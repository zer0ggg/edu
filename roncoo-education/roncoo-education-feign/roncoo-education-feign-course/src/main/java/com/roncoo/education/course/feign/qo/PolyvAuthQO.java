package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 视频授权播放回调
 */
@Data
@Accessors(chain = true)
public class PolyvAuthQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String vid;
    private String t;
    private String code;
    private String callback;
    private String secretkey;
}
