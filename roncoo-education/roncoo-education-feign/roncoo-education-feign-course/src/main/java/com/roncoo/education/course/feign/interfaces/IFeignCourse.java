package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.LiveCourseVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 课程信息
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourse {

    /**
     * 订单完成回调接口
     *
     * @param json
     * @return
     */
    @RequestMapping(value = "/course/order/complete", method = RequestMethod.POST)
    int orderComplete(@RequestBody String json);

    @RequestMapping(value = "/course/listForPage", method = RequestMethod.POST)
    Page<CourseVO> listForPage(@RequestBody CourseQO qo);

    @RequestMapping(value = "/course/save", method = RequestMethod.POST)
    int save(@RequestBody CourseQO qo);

    @RequestMapping(value = "/course/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/course/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseQO qo);

    @RequestMapping(value = "/course/get/{id}", method = RequestMethod.GET)
    CourseVO getById(@PathVariable(value = "id") Long id);

    /**
     * 专区使用
     *
     * @param id
     * @param statusId
     * @return
     */
    @RequestMapping(value = "/course/get/{id}/{statusId}", method = RequestMethod.GET)
    CourseVO getByIdAndStatusId(@PathVariable(value = "id") Long id, @PathVariable(value = "statusId") Integer statusId);

    /**
     * 根据id获得直播课程，包含直播时间,专区使用
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/course/live/get/{id}/{statusId}", method = RequestMethod.GET)
    LiveCourseVO getLiveCourseWithTimeByIdAndStatusId(@PathVariable(value = "id") Long id, @PathVariable(value = "statusId") Integer statusId);

    @RequestMapping(value = "/course/getByCourseId/{id}", method = RequestMethod.GET)
    CourseVO getByCourseId(@PathVariable(value = "id") Long id);

    /**
     * 根据课程id和课程类型查找课程信息
     *
     * @return
     */
    @RequestMapping(value = "/course/getByIdAndCourseCategory", method = RequestMethod.POST)
    CourseVO getByIdAndCourseCategory(@RequestBody CourseQO qo);

    /**
     * 一键导入es
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/course/addEs", method = RequestMethod.POST)
    boolean addEs(@RequestBody CourseQO qo);

    /**
     * 根据课程id和课程状态查找课程信息
     *
     * @return
     */
    @RequestMapping(value = "/course/getByCourseIdAndStatusId", method = RequestMethod.POST)
    CourseVO getByCourseIdAndStatusId(@RequestBody CourseQO qo);

    /**
     * 根据id集合获取课程信息
     *
     * @param courseQO
     * @return
     */
    @RequestMapping(value = "/course/listByIds", method = RequestMethod.POST)
    List<CourseVO> listByIds(@RequestBody CourseQO courseQO);
}
