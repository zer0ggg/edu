package com.roncoo.education.course.feign.qo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 直播结束
 *
 * @author LYQ
 */
@Data
public class TalkFunCallbackLiveStopQO implements Serializable {

    private static final long serialVersionUID = 2517099064711659996L;

    /**
     * 直播主题
     */
    private String title;

    /**
     * 直播记录ID
     */
    @JSONField(name = "liveid")
    private String liveId;

    /**
     * 主播账号
     */
    private String bid;

    /**
     * 第三方账号
     */
    private String thirdAccount;

    /**
     * 房间ID
     */
    @JSONField(name = "roomid")
    private String roomId;

    /**
     * 直播开始时间
     */
    private Date startTime;

    /**
     * 直播结束时间
     */
    private Date endTime;

    /**
     * 直播时长
     */
    private Integer duration;

    /**
     * 课程ID
     */
    @JSONField(name = "course_id")
    private Integer courseId;
}
