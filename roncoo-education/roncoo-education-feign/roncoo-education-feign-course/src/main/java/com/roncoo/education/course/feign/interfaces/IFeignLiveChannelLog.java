package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseLiveLogQO;
import com.roncoo.education.course.feign.vo.CourseLiveLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 直播记录表
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignLiveChannelLog {

    @RequestMapping(value = "/user/liveChannelLog/listForPage", method = RequestMethod.POST)
    Page<CourseLiveLogVO> listForPage(@RequestBody CourseLiveLogQO qo);

    @RequestMapping(value = "/user/liveChannelLog/save", method = RequestMethod.POST)
    int save(@RequestBody CourseLiveLogQO qo);

    @RequestMapping(value = "/user/liveChannelLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/liveChannelLog/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseLiveLogQO qo);

    @RequestMapping(value = "/user/liveChannelLog/get/{id}", method = RequestMethod.GET)
    CourseLiveLogVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据频道ID直播状态获取直播记录信息
     *
     * @param qo
     * @author kyh
     */
    @RequestMapping(value = "/user/liveChannelLog/getByChannelIdAndLiveStatus", method = RequestMethod.POST)
    CourseLiveLogVO getByChannelIdAndLiveStatus(@RequestBody CourseLiveLogQO qo);
}
