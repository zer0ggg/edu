package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignUserOrderCourseRef {

    @RequestMapping(value = "/user/userOrderCourseRef/listForPage", method = RequestMethod.POST)
    Page<UserOrderCourseRefVO> listForPage(@RequestBody UserOrderCourseRefQO qo);

    @RequestMapping(value = "/user/userOrderCourseRef/save", method = RequestMethod.POST)
    int save(@RequestBody UserOrderCourseRefQO qo);

    @RequestMapping(value = "/user/userOrderCourseRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/userOrderCourseRef/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserOrderCourseRefQO qo);

    @RequestMapping(value = "/user/userOrderCourseRef/get/{id}", method = RequestMethod.GET)
    UserOrderCourseRefVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据用户编号、课程id、课程类型查找课程关联
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/user/userOrderCourseRef/getByUserNoAndRefIdAndCourseType", method = RequestMethod.POST)
    UserOrderCourseRefVO getByUserNoAndRefIdAndCourseType(@RequestBody UserOrderCourseRefQO qo);

    /**
     * 根据用户编号、关联ID获取用户课程关联信息
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/user/userOrderCourseRef/getByUserNoAndRefId", method = RequestMethod.POST)
    UserOrderCourseRefVO getByUserNoAndRefId(@RequestBody UserOrderCourseRefQO qo);

    /**
     * 更新用户课程学习时长、进度
     *
     * @param userOrderCourseRefQO
     * @return
     */
    @RequestMapping(value = "/user/userOrderCourseRef/updateByUserNoAndRefId", method = RequestMethod.POST)
    int updateByUserNoAndRefId(@RequestBody UserOrderCourseRefQO userOrderCourseRefQO);
}
