package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.UserCollectionCourseQO;
import com.roncoo.education.course.feign.vo.UserCollectionCourseVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignUserCollectionCourse {

    @RequestMapping(value = "/user/userCollectionCourse/listForPage", method = RequestMethod.POST)
    Page<UserCollectionCourseVO> listForPage(@RequestBody UserCollectionCourseQO qo);

    @RequestMapping(value = "/user/userCollectionCourse/save", method = RequestMethod.POST)
    int save(@RequestBody UserCollectionCourseQO qo);

    @RequestMapping(value = "/user/userCollectionCourse/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/userCollectionCourse/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserCollectionCourseQO qo);

    @RequestMapping(value = "/user/userCollectionCourse/get/{id}", method = RequestMethod.GET)
    UserCollectionCourseVO getById(@PathVariable(value = "id") Long id);
}
