package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.ResourceRecommendQO;
import com.roncoo.education.course.feign.vo.ResourceRecommendVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * 文库推荐 接口
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@FeignClient(value = "service-course")
public interface IFeignResourceRecommend {

    @RequestMapping(value = "/resourceRecommend/listForPage", method = RequestMethod.POST)
    Page<ResourceRecommendVO> listForPage(@RequestBody ResourceRecommendQO qo);

    @RequestMapping(value = "/resourceRecommend/save", method = RequestMethod.POST)
    int save(@RequestBody ResourceRecommendQO qo);

    @RequestMapping(value = "/resourceRecommend/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/resourceRecommend/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ResourceRecommendQO qo);

    @RequestMapping(value = "/resourceRecommend/get/{id}", method = RequestMethod.GET)
    ResourceRecommendVO getById(@PathVariable(value = "id") Long id);

}
