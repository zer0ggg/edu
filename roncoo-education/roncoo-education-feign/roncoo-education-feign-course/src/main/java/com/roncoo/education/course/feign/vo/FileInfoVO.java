package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 文件信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class FileInfoVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 文件地址
	 */
	private String fileUrl;
	/**
	 * 文件类型(1:附件;2;图片;3:视频)
	 */
	private Integer fileType;
	/**
	 * 文件大小(B)
	 */
	private Long fileSize;

}
