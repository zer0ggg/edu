package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveDocumentAddQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLivePlaybackQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStartQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStopQO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 欢拓回调处理
 *
 * @author LYQ
 */
@FeignClient(value = "service-course")
public interface IFeignCallbackTalkFunLive {

    @PostMapping(value = "/callback/talk/fun/live/start")
    Boolean liveStart(@RequestBody TalkFunCallbackLiveStartQO liveStartQO);

    @PostMapping(value = "/callback/talk/fun/live/stop")
    Boolean liveStop(@RequestBody TalkFunCallbackLiveStopQO liveStopQO);

    @PostMapping(value = "/callback/talk/fun/live/playback")
    Boolean livePlayback(@RequestBody TalkFunCallbackLivePlaybackQO livePlaybackQO);

    @PostMapping(value = "/callback/talk/fun/live/document/add")
    Boolean liveDocumentAdd(@RequestBody TalkFunCallbackLiveDocumentAddQO liveDocumentAddQO);
}
