package com.roncoo.education.course.feign.qo;

import lombok.Data;

import java.io.Serializable;

/**
 * 上传文档
 *
 * @author LYQ
 */
@Data
public class TalkFunCallbackLiveDocumentAddQO implements Serializable {

    private static final long serialVersionUID = 2517099064711659996L;

    /**
     * 文档ID
     */
    private Integer id;

    /**
     * 文件后缀
     */
    private String ext;

    /**
     * 文档名称
     */
    private String name;

    /**
     * 文档大小
     */
    private Integer size;

    /**
     * 文档md5
     */
    private String md5;

}
