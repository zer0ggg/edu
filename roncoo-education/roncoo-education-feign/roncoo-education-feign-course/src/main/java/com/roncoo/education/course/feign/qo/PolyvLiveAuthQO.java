package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 保利威视，直播视频授权播放
 */
@Data
@Accessors(chain = true)
public class PolyvLiveAuthQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userid;
    private Long ts;
    private String token;
}
