package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 保利威视-直播状态回调
 */
@Data
@Accessors(chain = true)
public class PolyvLiveStatusQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String channelId;
    private String status;
    private String sessionId;
}
