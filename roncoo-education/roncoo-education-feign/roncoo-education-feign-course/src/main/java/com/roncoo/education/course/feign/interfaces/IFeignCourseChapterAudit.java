package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseChapterAuditQO;
import com.roncoo.education.course.feign.vo.CourseChapterAuditVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 章节信息-审核
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseChapterAudit {

    @RequestMapping(value = "/courseChapterAudit/listForPage", method = RequestMethod.POST)
    Page<CourseChapterAuditVO> listForPage(@RequestBody CourseChapterAuditQO qo);

    @RequestMapping(value = "/courseChapterAudit/save", method = RequestMethod.POST)
    int save(@RequestBody CourseChapterAuditQO qo);

    @RequestMapping(value = "/courseChapterAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/courseChapterAudit/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseChapterAuditQO qo);

    @RequestMapping(value = "/courseChapterAudit/get/{id}", method = RequestMethod.GET)
    CourseChapterAuditVO getById(@PathVariable(value = "id") Long id);

}
