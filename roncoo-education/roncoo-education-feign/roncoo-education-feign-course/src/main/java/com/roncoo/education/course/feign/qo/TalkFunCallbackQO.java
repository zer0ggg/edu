package com.roncoo.education.course.feign.qo;

import lombok.Data;

import java.io.Serializable;

/**
 * 回调事件通知
 *
 * @author LYQ
 */
@Data
public class TalkFunCallbackQO implements Serializable {

    private static final long serialVersionUID = -2635875615695114507L;

    /**
     * 合作方唯一标识码
     */
    private String openID;

    /**
     * 当前Unix时间戳
     */
    private String timestamp;

    /**
     * 调用的api接口名称
     */
    private String cmd;

    /**
     * 参数json格式字符串
     */
    private String params;

    /**
     * 协议版本号，统一 1.0
     */
    private String ver;

    /**
     * 欢拓根据以上参数与openToken进行生成
     */
    private String sign;
}
