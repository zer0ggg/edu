package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseCategoryQO;
import com.roncoo.education.course.feign.vo.CourseCategoryVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 课程分类
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseCategory {

    @RequestMapping(value = "/courseCategory/listForPage", method = RequestMethod.POST)
    Page<CourseCategoryVO> listForPage(@RequestBody CourseCategoryQO qo);

    @RequestMapping(value = "/courseCategory/save", method = RequestMethod.POST)
    int save(@RequestBody CourseCategoryQO qo);

    @RequestMapping(value = "/courseCategory/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/courseCategory/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseCategoryQO qo);

    @RequestMapping(value = "/courseCategory/get/{id}", method = RequestMethod.GET)
    CourseCategoryVO getById(@PathVariable(value = "id") Long id);

    /**
     * 更新状态
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/courseCategory/status", method = RequestMethod.PUT)
    int status(@RequestBody CourseCategoryQO qo);

    /**
     * 根据ID列出分类信息
     *
     * @author WY
     */
    @RequestMapping(value = "/courseCategory/listByParentId/{parentId}", method = RequestMethod.GET)
    List<CourseCategoryVO> listByParentId(@PathVariable(value = "parentId") Long parentId);

    /**
     * 根据层级列出分类信息
     *
     * @author WY
     */
    @RequestMapping(value = "/courseCategory/listByFloor/{floor}", method = RequestMethod.GET)
    List<CourseCategoryVO> listByFloor(@PathVariable(value = "floor") Integer floor);

    /**
     * 根据层级、父分类ID列出分类信息
     *
     * @author WY
     */
    @RequestMapping(value = "/courseCategory/listByFloorAndCategoryId", method = RequestMethod.POST)
    List<CourseCategoryVO> listByFloorAndCategoryId(@RequestBody CourseCategoryQO qo);

}
