package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务计划表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CrontabPlanVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;
    /**
     * 任务进度（1待执行，2执行中）
     */
    private Integer taskStatus;
    /**
     * 定时任务计划
     */
    private String taskCont;

}
