package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseChapterQO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 章节信息
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseChapter {

    @RequestMapping(value = "/courseChapter/listForPage", method = RequestMethod.POST)
    Page<CourseChapterVO> listForPage(@RequestBody CourseChapterQO qo);

    @RequestMapping(value = "/courseChapter/save", method = RequestMethod.POST)
    int save(@RequestBody CourseChapterQO qo);

    @RequestMapping(value = "/courseChapter/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/courseChapter/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseChapterQO qo);

    @RequestMapping(value = "/courseChapter/get/{id}", method = RequestMethod.GET)
    CourseChapterVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据id集合获取章节信息
     *
     * @param courseChapterQO
     * @return
     */
    @RequestMapping(value = "/courseChapter/listByIds", method = RequestMethod.POST)
    List<CourseChapterVO> listByIds(@RequestBody CourseChapterQO courseChapterQO);
}
