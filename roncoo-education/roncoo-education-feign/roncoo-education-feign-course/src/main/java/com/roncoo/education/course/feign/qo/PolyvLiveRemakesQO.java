package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 保利威视，重制课件完成回调通知接口
 */
@Data
@Accessors(chain = true)
public class PolyvLiveRemakesQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String channelId;
    private String title;
    private String remainDay;
    private String url;
    private String sessionId;
}
