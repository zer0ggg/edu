package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodAuditQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodAuditVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseChapterPeriodAudit {

    @RequestMapping(value = "/courseChapterPeriodAudit/listForPage", method = RequestMethod.POST)
    Page<CourseChapterPeriodAuditVO> listForPage(@RequestBody CourseChapterPeriodAuditQO qo);

    @RequestMapping(value = "/courseChapterPeriodAudit/save", method = RequestMethod.POST)
    int save(@RequestBody CourseChapterPeriodAuditQO qo);

    @RequestMapping(value = "/courseChapterPeriodAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/courseChapterPeriodAudit/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseChapterPeriodAuditQO qo);

    @RequestMapping(value = "/courseChapterPeriodAudit/get/{id}", method = RequestMethod.GET)
    CourseChapterPeriodAuditVO getById(@PathVariable(value = "id") Long id);

}
