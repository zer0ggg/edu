package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserOrderCourseRefQO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 当前页
	 */
	private int pageCurrent;
	/**
	 * 每页记录数
	 */
	private int pageSize;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1:正常，0:禁用)
	 */
	private Integer statusId;
	/**
	 * 讲师用户编号
	 */
	private Long lecturerUserNo;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 课程类型(1:普通课程;2:直播课程,3:试卷)
	 */
	private Integer courseCategory;
	/**
	 * 课程类型：1课程，2章节，3课时
	 */
	private Integer courseType;
	/**
	 * 课程ID
	 */
	private Long courseId;
	/**
	 * 关联ID
	 */
	private Long refId;
	/**
	 * 是否支付：1是，0:否
	 */
	private Integer isPay;
	/**
	 * 是否学习：1是，0:否
	 */
	private Integer isStudy;
	/**
	 * 订单号
	 */
	private Long orderNo;
	/**
	 * 过期时间
	 */
	private Date expireTime;
	/**
	 * 课程总时长
	 */
	private String courseLength;

	/**
	 * 学习时长
	 */
	private BigDecimal studyLength;
	/**
	 * 学习进度
	 */
	private String studyProcess;

	/**
	 * 课时视频时长
	 */
	private String videoLength;
	/**
	 * 最近学习课时ID
	 */
	private Long lastStudyPeriodId;
}
