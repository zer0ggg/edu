package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.FileInfoQO;
import com.roncoo.education.course.feign.vo.FileInfoVO;
import com.roncoo.education.course.feign.vo.UsedSpaceVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文件信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignFileInfo {

    @RequestMapping(value = "/user/fileInfo/listForPage", method = RequestMethod.POST)
    Page<FileInfoVO> listForPage(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/user/fileInfo/save", method = RequestMethod.POST)
    int save(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/user/fileInfo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/fileInfo/update", method = RequestMethod.PUT)
    int updateById(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/user/fileInfo/get/{id}", method = RequestMethod.GET)
    FileInfoVO getById(@PathVariable(value = "id") Long id);

    /**
     * 统计空间
     *
     * @return
     */
    @RequestMapping(value = "/course/fileInfo/usedSpace", method = RequestMethod.GET)
    UsedSpaceVO usedSpace();
}
