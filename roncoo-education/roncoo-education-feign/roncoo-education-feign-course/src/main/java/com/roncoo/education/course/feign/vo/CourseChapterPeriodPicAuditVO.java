package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 课时图片审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseChapterPeriodPicAuditVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 课程ID
	 */
	private Long courseId;
	/**
	 * 章节ID
	 */
	private Long chapterId;
	/**
	 * 课时ID
	 */
	private Long periodId;
	/**
	 * 图片ID
	 */
	private Long picId;
	/**
	 * 图片类型(1:试卷题目,2:试卷答案)
	 */
	private Integer picType;
	/**
	 * 图片地址
	 */
	private String picUrl;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 审核状态(0:待审核;1:审核通过;2:审核不通过)
	 */
	private Integer auditStatus;

}
