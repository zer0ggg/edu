package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 视频上传回调
 */
@Data
@Accessors(chain = true)
public class PolyvVideoQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String vid;

    private String sign;

    private String type;

    private String secretkey;

    private String state;
}
