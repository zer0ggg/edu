package com.roncoo.education.course.feign.qo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LYQ
 */
@Data
public class AliYunVideoAnalysisCompleteQO implements Serializable {

    private static final long serialVersionUID = 6453432279718494052L;

    /**
     * 事件产生时间，为UTC时间:yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 时间类型，固定为VideoAnalysisComplete
     */
    private String eventType;

    /**
     * 音视频ID
     */
    private String videoId;

    /**
     * 分析源文件结果状态，取值：success(成功)，fail(失败)
     */
    private String status;

    /**
     * 源文件宽 ，源片为纯音频不会该字段
     */
    private Long width;

    /**
     * 源文件高 ，源片为纯音频不会有该字段
     */
    private Long height;

    /**
     * 源文件时长 ，单位:秒
     */
    private Float duration;

    /**
     * 源文件码率 ，单位:Kbps
     */
    private String bitrate;

    /**
     * 源文件帧率 ，每秒多少帧，源片为纯音频不会有该字段
     */
    private String fps;

    /**
     * 源文件大小，单位: Byte(字节)
     */
    private Long size;

    /**
     * 如果分析源文件信息失败，会有该字段
     */
    private String errorCode;

    /**
     * 如果分析源文件信息失败，会有该字段
     */
    private String errorMessage;
}
