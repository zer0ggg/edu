package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 课程评论
 *
 * @author Quanf
 * @date 2020-09-03
 */
@Data
@Accessors(chain = true)
public class CourseCommentVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 被评论用户编号
     */
    private Long courseUserNo;

    /**
     * 被评论者昵称
     */
    private String courseNickname;

    /**
     * 评论者用户编号
     */
    private Long userNo;

    /**
     * 评论者昵称
     */
    private String nickname;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论者IP
     */
    private String userIp;

    /**
     * 评论者终端
     */
    private String userTerminal;

    /**
     * 课程分类(1点播,2直播,4文库)
     */
    private Integer courseCategory;
}
