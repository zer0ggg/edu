package com.roncoo.education.course.feign.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 课程信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class LiveCourseVO extends CourseVO {

	/**
	 * 直播开始时间
	 */
	@ApiModelProperty(value = "直播开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	/**
	 * 直播结束时间
	 */
	@ApiModelProperty(value = "直播结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

}
