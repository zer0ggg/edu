package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 保利威视，转存成功回调通知
 */
@Data
@Accessors(chain = true)
public class PolyvLiveSaveQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String channelId;
    private String vid;//转存后的视频vid
    private String title;
    private String duration;
    private String sessionId;
    private String videoId;//直播的直播存放的id。用于回放使用
}
