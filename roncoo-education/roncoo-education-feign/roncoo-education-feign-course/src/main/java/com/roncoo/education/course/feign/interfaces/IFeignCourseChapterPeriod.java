package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseChapterPeriod {

    @RequestMapping(value = "/courseChapterPeriod/listForPage", method = RequestMethod.POST)
    Page<CourseChapterPeriodVO> listForPage(@RequestBody CourseChapterPeriodQO qo);

    @RequestMapping(value = "/courseChapterPeriod/save", method = RequestMethod.POST)
    int save(@RequestBody CourseChapterPeriodQO qo);

    @RequestMapping(value = "/courseChapterPeriod/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/courseChapterPeriod/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseChapterPeriodQO qo);

    @RequestMapping(value = "/courseChapterPeriod/get/{id}", method = RequestMethod.GET)
    CourseChapterPeriodVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据课时id获取课时、章节、课程信息
     *
     * @return
     */
    @RequestMapping(value = "/courseChapterPeriod/getByCourseChapterPeriodId", method = RequestMethod.POST)
    CourseChapterPeriodVO getByCourseChapterPeriodId(@RequestBody CourseChapterPeriodQO qo);

    /**
     * 根据课程ID获取课时信息
     *
     * @return
     */
    @RequestMapping(value = "/courseChapterPeriod/listByCourseId", method = RequestMethod.POST)
    List<CourseChapterPeriodVO> ListByCourseId(@RequestBody CourseChapterPeriodQO qo);

    /**
     * 根据id集合获取课时信息
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/courseChapterPeriod/listByIds", method = RequestMethod.POST)
    List<CourseChapterPeriodVO> listByIds(@RequestBody CourseChapterPeriodQO qo);
}
