package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文库推荐
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@Data
@Accessors(chain = true)
public class ResourceRecommendVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 文库id
     */
    private Long resourceId;
    /**
     * 文库名称
     */
    private String resourceName;
}
