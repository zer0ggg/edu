package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 章节图片信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseChapterPicVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 课程id
	 */
	private Long courseId;
	/**
	 * 章节id
	 */
	private Long chapterId;
	/**
	 * 图片类型(1:试卷题目,2:试卷答案)
	 */
	private Integer picType;
	/**
	 * 图片地址
	 */
	private String picUrl;
	/**
	 * 标题
	 */
	private String title;

}
