package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 直播转点存定时任务
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CrontabPlanPolyvVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 频道号
     */
    private String channelId;
    /**
     * 回放
     */
    private String playback;
    /**
     * 课时名称
     */
    private String periodName;
    /**
     * 保利威视类型ID
     */
    private String polyvCategoryId;
    /**
     * 机构名称
     */
    private String orgName;
    /**
     * 失败次数
     */
    private Integer failCount;

}
