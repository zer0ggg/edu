package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CrontabPlanPolyvQO;
import com.roncoo.education.course.feign.vo.CrontabPlanPolyvVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 直播转点存定时任务
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCrontabPlanPolyv {

    @RequestMapping(value = "/crontabPlanPolyv/listForPage", method = RequestMethod.POST)
    Page<CrontabPlanPolyvVO> listForPage(@RequestBody CrontabPlanPolyvQO qo);

    @RequestMapping(value = "/crontabPlanPolyv/save", method = RequestMethod.POST)
    int save(@RequestBody CrontabPlanPolyvQO qo);

    @RequestMapping(value = "/crontabPlanPolyv/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/crontabPlanPolyv/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CrontabPlanPolyvQO qo);

    @RequestMapping(value = "/crontabPlanPolyv/get/{id}", method = RequestMethod.GET)
    CrontabPlanPolyvVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/crontabPlanPolyv/handleScheduledTasks", method = RequestMethod.POST)
    void handleScheduledTasks();
}
