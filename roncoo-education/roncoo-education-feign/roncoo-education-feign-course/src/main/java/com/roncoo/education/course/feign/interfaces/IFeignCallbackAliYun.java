package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.course.feign.qo.AliYunVideoAnalysisCompleteQO;
import com.roncoo.education.course.feign.qo.AliYunVideoUploadComplateQO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author LYQ
 */
@FeignClient(value = "service-course")
public interface IFeignCallbackAliYun {

    /**
     * 阿里云点播视频上传完成处理
     *
     * @param qo 回调参数
     * @return 回调结果
     */
    @PostMapping(value = "/callback/aliyun/video/upload/complete")
    Boolean uploadComplete(@RequestBody AliYunVideoUploadComplateQO qo);

    /**
     * 音视频分析完成
     *
     * @param qo 解析完成参数
     * @return 解析结果
     */
    @PostMapping(value = "/callback/aliyun/video/analysis/complete")
    Boolean videoAnalysisComplete(@RequestBody AliYunVideoAnalysisCompleteQO qo);
}
