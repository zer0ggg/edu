package com.roncoo.education.course.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 保利威视，回放生成回调通知
 */
@Data
@Accessors(chain = true)
public class PolyvLiveFileUrlQO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String channelId;
    private String fileId;
    private String fileUrl;
}
