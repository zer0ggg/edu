package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseCommentQO;
import com.roncoo.education.course.feign.vo.CourseCommentVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程评论 接口
 *
 * @author Quanf
 * @date 2020-09-03
 */
@FeignClient(value = "service-course")
public interface IFeignCourseComment{

    @RequestMapping(value = "/course/courseComment/listForPage", method = RequestMethod.POST)
    Page<CourseCommentVO> listForPage(@RequestBody CourseCommentQO qo);

    @RequestMapping(value = "/course/courseComment/save", method = RequestMethod.POST)
    int save(@RequestBody CourseCommentQO qo);

    @RequestMapping(value = "/course/courseComment/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/course/courseComment/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseCommentQO qo);

    @RequestMapping(value = "/course/courseComment/get/{id}", method = RequestMethod.GET)
    CourseCommentVO getById(@PathVariable(value = "id") Long id);

}
