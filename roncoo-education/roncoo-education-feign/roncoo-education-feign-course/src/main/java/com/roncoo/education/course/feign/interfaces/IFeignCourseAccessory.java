package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.qo.CourseAccessoryQO;
import com.roncoo.education.course.feign.vo.CourseAccessoryVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 附件信息
 *
 * @author wujing
 */
@FeignClient(value = "service-course")
public interface IFeignCourseAccessory {

    @RequestMapping(value = "/user/course/courseAccessory/listForPage", method = RequestMethod.POST)
    Page<CourseAccessoryVO> listForPage(@RequestBody CourseAccessoryQO qo);

    @RequestMapping(value = "/user/course/courseAccessory/save", method = RequestMethod.POST)
    int save(@RequestBody CourseAccessoryQO qo);

    @RequestMapping(value = "/user/course/courseAccessory/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/course/courseAccessory/update", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseAccessoryQO qo);

    @RequestMapping(value = "/user/course/courseAccessory/get/{id}", method = RequestMethod.GET)
    CourseAccessoryVO getById(@PathVariable(value = "id") Long id);

    /**
     * 统计下载数
     *
     * @return
     * @author kyh
     */
    @RequestMapping(value = "/user/course/courseAccessory/countDownload", method = RequestMethod.GET)
    CourseAccessoryVO countDownload();

    /**
     * 查找所有附件下载总数
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/user/course/courseAccessory/sumByAll", method = RequestMethod.POST)
    List<CourseAccessoryVO> sumByAll(@RequestBody CourseAccessoryQO qo);
}
