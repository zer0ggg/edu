/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * OSS空间使用统计
 * 
 */
@Data
@Accessors(chain = true)
public class UsedSpaceVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 机构所用总空间
	 */
	private String totalInOrgNo;

	/**
	 * 机构附件使用总空间
	 */
	private String totalInAccessory;

	/**
	 * 机构图片总空间
	 */
	private String totalInPicture;

	/**
	 * 机构视频总空间
	 */
	private String totalInVideo;

}
