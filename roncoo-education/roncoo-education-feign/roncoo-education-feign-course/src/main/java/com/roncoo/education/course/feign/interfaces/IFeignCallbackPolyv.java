package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.course.feign.qo.PolyvVideoQO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 保利威视频回调
 */
@FeignClient(value = "service-course")
public interface IFeignCallbackPolyv {

    /**
     * 保利威视，视频上传回调接口
     */
    @ApiOperation(value = "保利威视，视频上传回调接口", notes = "保利威视，视频上传回调接口")
    @RequestMapping(value = "/callback/polyv/video", method = RequestMethod.POST)
    String callbackPolyvVideo(@RequestBody PolyvVideoQO polyvVideoQO);

}
