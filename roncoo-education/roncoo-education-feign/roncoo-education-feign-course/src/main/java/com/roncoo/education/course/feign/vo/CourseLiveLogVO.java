package com.roncoo.education.course.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 直播记录表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseLiveLogVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 机构号
	 */
	private String orgNo;
	/**
	 * 讲师用户编号
	 */
	private Long lecturerUserNo;
	/**
	 * 课程id
	 */
	private Long courseId;
	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 章节id
	 */
	private Long chapterId;
	/**
	 * 章节名称
	 */
	private String chapterName;
	/**
	 * 课时id
	 */
	private Long periodId;
	/**
	 * 课时名称
	 */
	private String periodName;
	/**
	 * 频道号
	 */
	private String channelId;
	/**
	 * 开始时间
	 */
	private Date beginTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 直播状态(1:未开播,2:正在直播,3:待生成回放,4:待转存,5:结束)
	 */
	private Integer liveStatus;

}
