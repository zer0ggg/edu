package com.roncoo.education.course.feign.interfaces;

import com.roncoo.education.course.feign.qo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 保利威视频回调-直播
 */
@FeignClient(value = "service-course")
public interface IFeignCallbackPolyvLive {

    /**
     * 保利威视，视频授权播放回调接口
     *
     * @param polyvLiveStatusQO
     * @return 处理结果
     */
    @ApiOperation(value = "保利威视，直播状态回调接口", notes = "保利威视，直播状态回调接口")
    @RequestMapping(value = "/callbackPolyvLive/status", method = RequestMethod.POST)
    String callbackPolyvLiveStatus(@RequestBody PolyvLiveStatusQO polyvLiveStatusQO);


    @ApiOperation(value = "保利威视，视频授权播放接口", notes = "保利威视，视频授权播放接口")
    @RequestMapping(value = "/callbackPolyvLive/auth", method = RequestMethod.POST)
    String callbackPolyvLiveAuth(@RequestBody PolyvLiveAuthQO polyvLiveAuthQO);

    @ApiOperation(value = "保利威视，回放生成回调通知接口", notes = "保利威视，回放生成回调通知接口")
    @RequestMapping(value = "/callbackPolyvLive/url", method = RequestMethod.POST)
    String callbackLiveFileUrl(@RequestBody PolyvLiveFileUrlQO polyvLiveFileUrlQO);


    @ApiOperation(value = "保利威视，转存成功回调通知接口", notes = "保利威视，转存成功回调通知接口")
    @RequestMapping(value = "/callbackPolyvLive/save", method = RequestMethod.POST)
    String callbackLiveSave(@RequestBody PolyvLiveSaveQO polyvLiveSaveQO);


    @ApiOperation(value = "保利威视，重制课件完成回调通知接口", notes = "保利威视，重制课件完成回调通知接口")
    @RequestMapping(value = "/callbackPolyvLive/remakes", method = RequestMethod.POST)
    String callbackLiveRemakes(@RequestBody PolyvLiveRemakesQO polyvLiveRemakesQO);


}
