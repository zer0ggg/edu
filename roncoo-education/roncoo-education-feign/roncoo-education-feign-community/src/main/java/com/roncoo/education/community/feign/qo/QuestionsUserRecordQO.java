package com.roncoo.education.community.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsUserRecordQO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private int pageCurrent;
    /**
     * 每页记录数
     */
    private int pageSize;
    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 问题ID
     */
    private Long questionsId;
    /**
     * 用户操作类型(1收藏，2点赞)
     */
    private Integer opType;
    /**
     * 用户编号
     */
    private Long userNo;
    /**
     * 用户IP
     */
    private String userIp;
    /**
     * 用户终端
     */
    private String userTerminal;
}
