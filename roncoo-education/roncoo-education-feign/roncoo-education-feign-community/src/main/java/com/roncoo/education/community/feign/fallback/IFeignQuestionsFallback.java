package com.roncoo.education.community.feign.fallback;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignQuestions;
import com.roncoo.education.community.feign.qo.QuestionsQO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import org.springframework.stereotype.Component;

@Component
public class IFeignQuestionsFallback implements IFeignQuestions {

    @Override
    public Page<QuestionsVO> listForPage(QuestionsQO qo) {
        return null;
    }

    @Override
    public int save(QuestionsQO qo) {
        return 0;
    }

    @Override
    public int deleteById(Long id) {
        return 0;
    }

    @Override
    public int updateById(QuestionsQO qo) {
        return 0;
    }

    @Override
    public QuestionsVO getById(Long id) {
        return null;
    }

    @Override
    public int updateGoodComment(QuestionsQO qo) {
        return 0;
    }

    @Override
    public boolean addEs(QuestionsQO qo) {
        return false;
    }

    @Override
    public int updateStatusId(QuestionsQO qo) {
        return 0;
    }
}
