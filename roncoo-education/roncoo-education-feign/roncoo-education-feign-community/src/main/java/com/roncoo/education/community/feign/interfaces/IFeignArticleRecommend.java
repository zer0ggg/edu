package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.ArticleRecommendQO;
import com.roncoo.education.community.feign.vo.ArticleRecommendVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文章推荐
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignArticleRecommend {

    @RequestMapping(value = "/articleRecommend/listForPage", method = RequestMethod.POST)
    Page<ArticleRecommendVO> listForPage(@RequestBody ArticleRecommendQO qo);

    @RequestMapping(value = "/articleRecommend/save", method = RequestMethod.POST)
    int save(@RequestBody ArticleRecommendQO qo);

    @RequestMapping(value = "/articleRecommend/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/articleRecommend/update", method = RequestMethod.PUT)
    int updateById(@RequestBody ArticleRecommendQO qo);

    @RequestMapping(value = "/articleRecommend/get/{id}", method = RequestMethod.GET)
    ArticleRecommendVO getById(@PathVariable(value = "id") Long id);
}
