package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.fallback.IFeignQuestionsFallback;
import com.roncoo.education.community.feign.qo.QuestionsQO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 问答信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-community", fallback = IFeignQuestionsFallback.class)
public interface IFeignQuestions {

    @RequestMapping(value = "/questions/listForPage", method = RequestMethod.POST)
    Page<QuestionsVO> listForPage(@RequestBody QuestionsQO qo);

    @RequestMapping(value = "/questions/save", method = RequestMethod.POST)
    int save(@RequestBody QuestionsQO qo);

    @RequestMapping(value = "/questions/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/questions/update", method = RequestMethod.PUT)
    int updateById(@RequestBody QuestionsQO qo);

    @RequestMapping(value = "/questions/get/{id}", method = RequestMethod.GET)
    QuestionsVO getById(@PathVariable(value = "id") Long id);

    /**
     * 更新最佳问答
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/questions/updateGoodComment", method = RequestMethod.PUT)
    int updateGoodComment(@RequestBody QuestionsQO qo);

    /**
     * 一键导入ES
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/questions/addEs", method = RequestMethod.POST)
    boolean addEs(@RequestBody QuestionsQO qo);

    /**
     * 更新状态
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/questions/updateStatusId", method = RequestMethod.PUT)
    int updateStatusId(@RequestBody QuestionsQO qo);
}
