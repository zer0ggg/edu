package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.BlogUserRecordQO;
import com.roncoo.education.community.feign.vo.BlogUserRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 博客与用户关联表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignBlogUserRecord {

    @RequestMapping(value = "/blogUserRecord/listForPage", method = RequestMethod.POST)
    Page<BlogUserRecordVO> listForPage(@RequestBody BlogUserRecordQO qo);

    @RequestMapping(value = "/blogUserRecord/save", method = RequestMethod.POST)
    int save(@RequestBody BlogUserRecordQO qo);

    @RequestMapping(value = "/blogUserRecord/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/blogUserRecord/update", method = RequestMethod.PUT)
    int updateById(@RequestBody BlogUserRecordQO qo);

    @RequestMapping(value = "/blogUserRecord/get/{id}", method = RequestMethod.GET)
    BlogUserRecordVO getById(@PathVariable(value = "id") Long id);
}
