package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.ArticleZoneCategoryQO;
import com.roncoo.education.community.feign.vo.ArticleZoneCategoryVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignArticleZoneCategory {

    @RequestMapping(value = "/articleZoneCategory/listForPage", method = RequestMethod.POST)
    Page<ArticleZoneCategoryVO> listForPage(@RequestBody ArticleZoneCategoryQO qo);

    @RequestMapping(value = "/articleZoneCategory/save", method = RequestMethod.POST)
    int save(@RequestBody ArticleZoneCategoryQO qo);

    @RequestMapping(value = "/articleZoneCategory/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/articleZoneCategory/update", method = RequestMethod.PUT)
    int updateById(@RequestBody ArticleZoneCategoryQO qo);

    @RequestMapping(value = "/articleZoneCategory/get/{id}", method = RequestMethod.GET)
    ArticleZoneCategoryVO getById(@PathVariable(value = "id") Long id);
}
