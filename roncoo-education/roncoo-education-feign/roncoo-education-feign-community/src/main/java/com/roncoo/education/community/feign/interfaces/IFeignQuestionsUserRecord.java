package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.QuestionsUserRecordQO;
import com.roncoo.education.community.feign.vo.QuestionsUserRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignQuestionsUserRecord {

    @RequestMapping(value = "/questionsUserRecord/listForPage", method = RequestMethod.POST)
    Page<QuestionsUserRecordVO> listForPage(@RequestBody QuestionsUserRecordQO qo);

    @RequestMapping(value = "/questionsUserRecord/save", method = RequestMethod.POST)
    int save(@RequestBody QuestionsUserRecordQO qo);

    @RequestMapping(value = "/questionsUserRecord/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/questionsUserRecord/update", method = RequestMethod.PUT)
    int updateById(@RequestBody QuestionsUserRecordQO qo);

    @RequestMapping(value = "/questionsUserRecord/get/{id}", method = RequestMethod.GET)
    QuestionsUserRecordVO getById(@PathVariable(value = "id") Long id);
}
