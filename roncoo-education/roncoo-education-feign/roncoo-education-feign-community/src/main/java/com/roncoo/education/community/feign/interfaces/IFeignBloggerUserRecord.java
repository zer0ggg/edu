package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.BloggerUserRecordQO;
import com.roncoo.education.community.feign.vo.BloggerUserRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignBloggerUserRecord {

    @RequestMapping(value = "/bloggerUserRecord/listForPage", method = RequestMethod.POST)
    Page<BloggerUserRecordVO> listForPage(@RequestBody BloggerUserRecordQO qo);

    @RequestMapping(value = "/bloggerUserRecord/save", method = RequestMethod.POST)
    int save(@RequestBody BloggerUserRecordQO qo);

    @RequestMapping(value = "/bloggerUserRecord/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/bloggerUserRecord/update", method = RequestMethod.PUT)
    int updateById(@RequestBody BloggerUserRecordQO qo);

    @RequestMapping(value = "/bloggerUserRecord/get/{id}", method = RequestMethod.GET)
    BloggerUserRecordVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据用户编号与作者用户编号获取作者与用户关注关联信息
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/bloggerUserRecord/getByUserNoAndBloggerUserNo", method = RequestMethod.POST)
    BloggerUserRecordVO getByUserNoAndBloggerUserNo(@RequestBody BloggerUserRecordQO qo);
}
