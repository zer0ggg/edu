package com.roncoo.education.community.feign.fallback;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.qo.BlogQO;
import com.roncoo.education.community.feign.vo.BlogVO;
import org.springframework.stereotype.Component;

@Component
public class IFeignBlogFallback implements IFeignBlog {

    @Override
    public Page<BlogVO> listForPage(BlogQO qo) {
        return null;
    }

    @Override
    public int save(BlogQO qo) {
        return 0;
    }

    @Override
    public int deleteById(Long id) {
        return 0;
    }

    @Override
    public int updateById(BlogQO qo) {
        return 0;
    }

    @Override
    public BlogVO getById(Long id) {
        return null;
    }

    @Override
    public BlogVO getByIdAndStatusId(Long id, Integer statusId) {
        return null;
    }

    @Override
    public boolean addEs(BlogQO qo) {
        return false;
    }
}
