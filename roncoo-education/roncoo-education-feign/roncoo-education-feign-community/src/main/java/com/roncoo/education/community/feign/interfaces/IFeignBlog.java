package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.fallback.IFeignBlogFallback;
import com.roncoo.education.community.feign.qo.BlogQO;
import com.roncoo.education.community.feign.vo.BlogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 博客信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-community", fallback = IFeignBlogFallback.class)
public interface IFeignBlog {

    @RequestMapping(value = "/blog/listForPage", method = RequestMethod.POST)
    Page<BlogVO> listForPage(@RequestBody BlogQO qo);

    @RequestMapping(value = "/blog/save", method = RequestMethod.POST)
    int save(@RequestBody BlogQO qo);

    @RequestMapping(value = "/blog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/blog/update", method = RequestMethod.PUT)
    int updateById(@RequestBody BlogQO qo);

    @RequestMapping(value = "/blog/get/{id}", method = RequestMethod.GET)
    BlogVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/blog/get/{id}/{statusId}", method = RequestMethod.GET)
    BlogVO getByIdAndStatusId(@PathVariable(value = "id") Long id, @PathVariable(value = "statusId") Integer statusId);

    /**
     * 导入ES
     *
     * @param qo
     */
    @RequestMapping(value = "/blog/addEs", method = RequestMethod.POST)
    boolean addEs(@RequestBody BlogQO qo);
}
