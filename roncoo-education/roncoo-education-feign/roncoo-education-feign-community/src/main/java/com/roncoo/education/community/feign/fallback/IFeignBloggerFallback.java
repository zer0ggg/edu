package com.roncoo.education.community.feign.fallback;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlogger;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.community.feign.vo.BloggerVO;
import org.springframework.stereotype.Component;

@Component
public class IFeignBloggerFallback implements IFeignBlogger {

    @Override
    public Page<BloggerVO> listForPage(BloggerQO qo) {
        return null;
    }

    @Override
    public int save(BloggerQO qo) {
        return 0;
    }

    @Override
    public int deleteById(Long id) {
        return 0;
    }

    @Override
    public int updateById(BloggerQO qo) {
        return 0;
    }

    @Override
    public BloggerVO getById(Long id) {
        return null;
    }

    @Override
    public BloggerVO getByUserNo(BloggerQO qo) {
        return null;
    }
}
