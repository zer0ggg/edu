package com.roncoo.education.community.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleRecommendQO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private int pageCurrent;
    /**
     * 每页记录数
     */
    private int pageSize;
    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 文章id
     */
    private Long artcleId;
    /**
     * 文章类型(1:博客;2:资讯)
     */
    private Integer articleType;
    /**
     * 文章标题
     */
    private String title;

}
