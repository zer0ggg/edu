package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.ArticleZoneRefQO;
import com.roncoo.education.community.feign.vo.ArticleZoneRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignArticleZoneRef {

    @RequestMapping(value = "/articleZoneRef/listForPage", method = RequestMethod.POST)
    Page<ArticleZoneRefVO> listForPage(@RequestBody ArticleZoneRefQO qo);

    @RequestMapping(value = "/articleZoneRef/save", method = RequestMethod.POST)
    int save(@RequestBody ArticleZoneRefQO qo);

    @RequestMapping(value = "/articleZoneRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/articleZoneRef/update", method = RequestMethod.PUT)
    int updateById(@RequestBody ArticleZoneRefQO qo);

    @RequestMapping(value = "/articleZoneRef/get/{id}", method = RequestMethod.GET)
    ArticleZoneRefVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/articleZoneRef/remove/{ids}", method = RequestMethod.DELETE)
    int remove(@PathVariable(value = "ids") String ids);
}
