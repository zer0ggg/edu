package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.BlogCommentQO;
import com.roncoo.education.community.feign.vo.BlogCommentVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 博客评论表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignBlogComment {

    @RequestMapping(value = "/blogComment/listForPage", method = RequestMethod.POST)
    Page<BlogCommentVO> listForPage(@RequestBody BlogCommentQO qo);

    @RequestMapping(value = "/blogComment/save", method = RequestMethod.POST)
    int save(@RequestBody BlogCommentQO qo);

    @RequestMapping(value = "/blogComment/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/blogComment/update", method = RequestMethod.PUT)
    int updateById(@RequestBody BlogCommentQO qo);

    @RequestMapping(value = "/blogComment/get/{id}", method = RequestMethod.GET)
    BlogCommentVO getById(@PathVariable(value = "id") Long id);
}
