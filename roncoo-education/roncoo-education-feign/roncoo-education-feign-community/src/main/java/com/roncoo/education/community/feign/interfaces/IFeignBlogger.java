package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.fallback.IFeignBloggerFallback;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.community.feign.vo.BloggerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 博主信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-community", fallback = IFeignBloggerFallback.class)
public interface IFeignBlogger {

    @RequestMapping(value = "/blogger/listForPage", method = RequestMethod.POST)
    Page<BloggerVO> listForPage(@RequestBody BloggerQO qo);

    @RequestMapping(value = "/blogger/save", method = RequestMethod.POST)
    int save(@RequestBody BloggerQO qo);

    @RequestMapping(value = "/blogger/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/blogger/update", method = RequestMethod.PUT)
    int updateById(@RequestBody BloggerQO qo);

    @RequestMapping(value = "/blogger/get/{id}", method = RequestMethod.GET)
    BloggerVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据用户编号获取博主信息
     *
     * @param qo
     */
    @RequestMapping(value = "/blogger/getByUserNo", method = RequestMethod.POST)
    BloggerVO getByUserNo(@RequestBody BloggerQO qo);
}
