package com.roncoo.education.community.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博主信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class BloggerQO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private int pageCurrent;
    /**
     * 每页记录数
     */
    private int pageSize;
    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 用户编号
     */
    private Long userNo;
    /**
     * 用户手机
     */
    private String mobile;
    /**
     * 博主简介
     */
    private String introduction;
    /**
     * 粉丝人数
     */
    private Integer fansAccount;
    /**
     * 关注人数
     */
    private Integer attentionAcount;
    /**
     * 博客数量
     */
    private Integer weblogAccount;
}
