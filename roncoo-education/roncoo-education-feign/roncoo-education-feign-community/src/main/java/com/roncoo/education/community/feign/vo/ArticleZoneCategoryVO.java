package com.roncoo.education.community.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneCategoryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 文章专区名称
     */
    private String articleZoneName;
    /**
     * 专区描述
     */
    private String articleZoneDesc;
    /**
     * 显示平台(1:PC端显示;2:微信端展示)
     */
    private Integer platShow;

}
