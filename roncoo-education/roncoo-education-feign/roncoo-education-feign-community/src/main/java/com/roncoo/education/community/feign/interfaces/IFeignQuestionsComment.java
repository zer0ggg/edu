package com.roncoo.education.community.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.qo.QuestionsCommentQO;
import com.roncoo.education.community.feign.vo.QuestionsCommentVO;
import com.roncoo.education.community.feign.vo.QuestionsStatisticalVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 问答评论表
 *
 * @author wujing
 */
@FeignClient(value = "service-community")
public interface IFeignQuestionsComment {

    @RequestMapping(value = "/questionsComment/listForPage", method = RequestMethod.POST)
    Page<QuestionsCommentVO> listForPage(@RequestBody QuestionsCommentQO qo);

    @RequestMapping(value = "/questionsComment/save", method = RequestMethod.POST)
    int save(@RequestBody QuestionsCommentQO qo);

    @RequestMapping(value = "/questionsComment/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/questionsComment/update", method = RequestMethod.PUT)
    int updateById(@RequestBody QuestionsCommentQO qo);

    @RequestMapping(value = "/questionsComment/get/{id}", method = RequestMethod.GET)
    QuestionsCommentVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据用户编号统计问答数与评论数
     *
     * @param userNo
     * @return
     */
    @RequestMapping(value = "/questionsComment/statistical/userNo/{userNo}", method = RequestMethod.GET)
    QuestionsStatisticalVO statisticalByUserNo(@PathVariable(value = "userNo") Long userNo);
}
