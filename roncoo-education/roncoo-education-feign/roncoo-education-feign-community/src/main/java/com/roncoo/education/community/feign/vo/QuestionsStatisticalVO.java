package com.roncoo.education.community.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户统计问答与评论数
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsStatisticalVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 状态(1:正常，0:禁用)
	 */
	private Integer statusId;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 用户手机
	 */
	private String mobile;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 头像地址
	 */
	private String headImgUrl;
	/**
	 * 提问数量
	 */
	private Integer questionsAccount;
	/**
	 * 回答数量
	 */
	private Integer questionsResponseAccount;
}
