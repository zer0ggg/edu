package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamTitleAuditQO;
import com.roncoo.education.exam.feign.vo.ExamTitleAuditVO;
import com.roncoo.education.exam.feign.vo.ExamTitleGradeAuditVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷标题审核 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamTitleAudit {

    @RequestMapping(value = "/exam/examTitleAudit/listForPage", method = RequestMethod.POST)
    Page<ExamTitleAuditVO> listForPage(@RequestBody ExamTitleAuditQO qo);

    @RequestMapping(value = "/exam/examTitleAudit/save", method = RequestMethod.POST)
    int save(@RequestBody ExamTitleAuditQO qo);

    @RequestMapping(value = "/exam/examTitleAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examTitleAudit/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamTitleAuditQO qo);

    @RequestMapping(value = "/exam/examTitleAudit/get/{id}", method = RequestMethod.GET)
    ExamTitleAuditVO getById(@PathVariable(value = "id") Long id);

    /**
     * 获得标题及其试题（包含试题答案和分数），用于阅卷
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/exam/getByIdForGradeAudit/get/{id}", method = RequestMethod.GET)
    ExamTitleGradeAuditVO getByIdForGradeAudit(@PathVariable(value = "id") Long id);

}
