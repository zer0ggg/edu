package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户试卷下载
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class UserExamDownloadVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long Id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 文件格式（1：DOC; 2: DOCX）
     */
    private Integer fileFormat;

    /**
     * 下载类型（1：普通试卷题干带参考答案 ；2：考试试卷只有题干，不带参考答案）
     */
    private Integer downloadType;
}
