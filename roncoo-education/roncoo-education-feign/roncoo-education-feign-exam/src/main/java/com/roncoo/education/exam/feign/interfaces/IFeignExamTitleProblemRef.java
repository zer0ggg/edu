package com.roncoo.education.exam.feign.interfaces;


import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷标题题目关联 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamTitleProblemRef {

    @RequestMapping(value = "/exam/examTitleProblemRef/listForPage", method = RequestMethod.POST)
    Page<ExamTitleProblemRefVO> listForPage(@RequestBody ExamTitleProblemRefQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRef/save", method = RequestMethod.POST)
    int save(@RequestBody ExamTitleProblemRefQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examTitleProblemRef/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamTitleProblemRefQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRef/get/{id}", method = RequestMethod.GET)
    ExamTitleProblemRefVO getById(@PathVariable(value = "id") Long id);

}
