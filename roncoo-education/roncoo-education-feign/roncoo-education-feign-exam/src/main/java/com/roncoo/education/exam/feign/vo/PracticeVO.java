package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 练习信息
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
public class PracticeVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 科目id
     */
    private Long subjectId;

    /**
     * 题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)
     */
    private Integer problemType;

    /**
     * 难度id
     */
    private Long difficultyId;

    /**
     * 来源id
     */
    private Long sourceId;

    /**
     * 年份id
     */
    private Long yearId;

    /**
     * 试题数量
     */
    private Integer problemQuantity;

    /**
     * 地区
     */
    private String region;

    /**
     * 考点
     */
    private String emphasis;

    /**
     * 题类id
     */
    private Long topicId;

    /**
     * 年级id
     */
    private Long graId;

    /**
     * 考点id
     */
    private Long emphasisId;

    /**
     * 名称
     */
    private String name;
}
