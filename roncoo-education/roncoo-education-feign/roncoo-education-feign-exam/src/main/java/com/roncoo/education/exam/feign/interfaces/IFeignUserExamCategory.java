package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamCategoryQO;
import com.roncoo.education.exam.feign.vo.UserExamCategoryVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户试卷分类 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExamCategory {

    @RequestMapping(value = "/exam/userExamCategory/listForPage", method = RequestMethod.POST)
    Page<UserExamCategoryVO> listForPage(@RequestBody UserExamCategoryQO qo);

    @RequestMapping(value = "/exam/userExamCategory/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamCategoryQO qo);

    @RequestMapping(value = "/exam/userExamCategory/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExamCategory/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamCategoryQO qo);

    @RequestMapping(value = "/exam/userExamCategory/get/{id}", method = RequestMethod.GET)
    UserExamCategoryVO getById(@PathVariable(value = "id") Long id);

}
