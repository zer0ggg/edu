package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 题目练习关联
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
public class PracticeProblemRefVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 练习ID
     */
    private Long practiceId;

    /**
     * 题目父类ID
     */
    private Long problemParentId;

    /**
     * 题目ID
     */
    private Long problemId;
}
