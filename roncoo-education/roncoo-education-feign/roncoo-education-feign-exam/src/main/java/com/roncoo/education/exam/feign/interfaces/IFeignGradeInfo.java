package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeInfoQO;
import com.roncoo.education.exam.feign.vo.GradeInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeInfo {

    @RequestMapping(value = "/exam/gradeInfo/listForPage", method = RequestMethod.POST)
    Page<GradeInfoVO> listForPage(@RequestBody GradeInfoQO qo);

    @RequestMapping(value = "/exam/gradeInfo/save", method = RequestMethod.POST)
    int save(@RequestBody GradeInfoQO qo);

    @RequestMapping(value = "/exam/gradeInfo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeInfo/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeInfoQO qo);

    @RequestMapping(value = "/exam/gradeInfo/get/{id}", method = RequestMethod.GET)
    GradeInfoVO getById(@PathVariable(value = "id") Long id);

}
