package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户考试标题得分
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class UserExamTitleScoreVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 记录ID
     */
    private Long recordId;

    /**
     * 标题ID
     */
    private Long titleId;

    /**
     * 得分
     */
    private Integer score;

    /**
     * 系统评阅分值
     */
    private Integer sysAuditTotalScore;

    /**
     * 系统评阅得分
     */
    private Integer sysAuditScore;

    /**
     * 人工评阅分值
     */
    private Integer handworkAuditTotalScore;

    /**
     * 人工评阅得分
     */
    private Integer handworkAuditScore;
}
