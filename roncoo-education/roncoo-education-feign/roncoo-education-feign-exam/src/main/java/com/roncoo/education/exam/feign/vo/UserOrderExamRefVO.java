package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户订单试卷关联
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class UserOrderExamRefVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 订单编号
     */
    private Long orderNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 过期时间
     */
    private LocalDateTime expireTime;
}
