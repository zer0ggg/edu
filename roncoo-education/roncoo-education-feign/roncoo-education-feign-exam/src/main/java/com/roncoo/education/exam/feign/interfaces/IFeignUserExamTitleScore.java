package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamTitleScoreQO;
import com.roncoo.education.exam.feign.vo.UserExamTitleScoreVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户考试标题得分 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExamTitleScore {

    @RequestMapping(value = "/exam/userExamTitleScore/listForPage", method = RequestMethod.POST)
    Page<UserExamTitleScoreVO> listForPage(@RequestBody UserExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/userExamTitleScore/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/userExamTitleScore/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExamTitleScore/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/userExamTitleScore/get/{id}", method = RequestMethod.GET)
    UserExamTitleScoreVO getById(@PathVariable(value = "id") Long id);

}
