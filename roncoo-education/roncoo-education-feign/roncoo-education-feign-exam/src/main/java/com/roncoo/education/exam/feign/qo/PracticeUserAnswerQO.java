package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户考试答案
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
public class PracticeUserAnswerQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 记录ID
     */
    private Long recordId;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题ID
     */
    private Long titleId;

    /**
     * 题目父类ID
     */
    private Long problemParentId;

    /**
     * 题目ID
     */
    private Long problemId;

    /**
     * 题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)
     */
    private Integer problemType;

    /**
     * 回答内容
     */
    private String userAnswer;

    /**
     * 题目分值
     */
    private Integer problemScore;

    /**
     * 得分
     */
    private Integer score;

}
