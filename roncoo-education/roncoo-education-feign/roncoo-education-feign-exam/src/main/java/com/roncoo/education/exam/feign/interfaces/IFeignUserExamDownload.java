package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamDownloadQO;
import com.roncoo.education.exam.feign.vo.UserExamDownloadVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户试卷下载 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExamDownload {

    @RequestMapping(value = "/exam/userExamDownload/listForPage", method = RequestMethod.POST)
    Page<UserExamDownloadVO> listForPage(@RequestBody UserExamDownloadQO qo);

    @RequestMapping(value = "/exam/userExamDownload/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamDownloadQO qo);

    @RequestMapping(value = "/exam/userExamDownload/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExamDownload/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamDownloadQO qo);

    @RequestMapping(value = "/exam/userExamDownload/get/{id}", method = RequestMethod.GET)
    UserExamDownloadVO getById(@PathVariable(value = "id") Long id);

}
