package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeStudentQO;
import com.roncoo.education.exam.feign.vo.GradeStudentVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeStudent {

    @RequestMapping(value = "/exam/gradeStudent/listForPage", method = RequestMethod.POST)
    Page<GradeStudentVO> listForPage(@RequestBody GradeStudentQO qo);

    @RequestMapping(value = "/exam/gradeStudent/save", method = RequestMethod.POST)
    int save(@RequestBody GradeStudentQO qo);

    @RequestMapping(value = "/exam/gradeStudent/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeStudent/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeStudentQO qo);

    @RequestMapping(value = "/exam/gradeStudent/get/{id}", method = RequestMethod.GET)
    GradeStudentVO getById(@PathVariable(value = "id") Long id);

}
