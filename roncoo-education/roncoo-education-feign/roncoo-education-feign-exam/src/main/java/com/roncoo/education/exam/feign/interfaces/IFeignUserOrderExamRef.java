package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefSaveQO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 用户订单试卷关联 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserOrderExamRef {

    /**
     * 根据用户编号和试卷ID获取用户订单试卷关联
     *
     * @param qo 查询参数
     * @return 查询结果
     */
    @PostMapping(value = "/exam/userOrderExamRef/getByUserNoAndExamId")
    UserOrderExamRefVO getByUserNoAndExamId(@RequestBody UserOrderExamRefQO qo);

    /**
     * 保存用户订单试卷关联
     *
     * @param qo 保存参数
     * @return 保存结果
     */
    @PostMapping(value = "/exam/userOrderExamRef/save")
    int save(@RequestBody UserOrderExamRefSaveQO qo);

    /**
     * 删除用户订单试卷关联
     *
     * @param id id
     * @return 删除结果
     */
    @RequestMapping(value = "/exam/userOrderExamRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);
}
