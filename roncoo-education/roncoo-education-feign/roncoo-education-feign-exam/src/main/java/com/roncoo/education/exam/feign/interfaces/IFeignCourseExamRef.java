package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程试卷关联 接口
 *
 * @author wujing
 * @date 2020-10-21
 */
@FeignClient(value = "service-exam")
public interface IFeignCourseExamRef{

    @RequestMapping(value = "/exam/courseExamRef/listForPage", method = RequestMethod.POST)
    Page<CourseExamRefVO> listForPage(@RequestBody CourseExamRefQO qo);

    @RequestMapping(value = "/exam/courseExamRef/save", method = RequestMethod.POST)
    int save(@RequestBody CourseExamRefQO qo);

    @RequestMapping(value = "/exam/courseExamRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/courseExamRef/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseExamRefQO qo);

    @RequestMapping(value = "/exam/courseExamRef/get/{id}", method = RequestMethod.GET)
    CourseExamRefVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据关联ID、关联类型获取课程试卷关联信息（课程详情页使用）
     *
     * @param courseExamRefQO
     * @return
     */
    @RequestMapping(value = "/exam/courseExamRef/getByRefIdAndRefType", method = RequestMethod.POST)
    CourseExamRefVO getByRefIdAndRefType(@RequestBody CourseExamRefQO courseExamRefQO);

    /**
     * 根据关联ID、关联类型获取课程试卷关联信息（审核列表使用）
     *
     * @param courseExamRefQO
     * @return
     */
    @RequestMapping(value = "/exam/courseExamRef/getByRefIdAndRefTypeAudit", method = RequestMethod.POST)
    CourseExamRefVO getByRefIdAndRefTypeAudit(@RequestBody CourseExamRefQO courseExamRefQO);
}
