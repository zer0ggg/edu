package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷题目  班级评卷信息
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamProblemGradeAuditVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)
     */
    private Integer problemType;

    /**
     * 题目答案
     */
    private String problemAnswer;

}
