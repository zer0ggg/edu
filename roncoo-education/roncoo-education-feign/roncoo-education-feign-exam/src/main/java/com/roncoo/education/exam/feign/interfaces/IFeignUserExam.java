package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamQO;
import com.roncoo.education.exam.feign.vo.UserExamVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户试卷 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExam {

    @RequestMapping(value = "/exam/userExam/listForPage", method = RequestMethod.POST)
    Page<UserExamVO> listForPage(@RequestBody UserExamQO qo);

    @RequestMapping(value = "/exam/userExam/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamQO qo);

    @RequestMapping(value = "/exam/userExam/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExam/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamQO qo);

    @RequestMapping(value = "/exam/userExam/get/{id}", method = RequestMethod.GET)
    UserExamVO getById(@PathVariable(value = "id") Long id);

}
