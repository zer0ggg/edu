package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeStudentExamAnswerQO;
import com.roncoo.education.exam.feign.vo.GradeStudentExamAnswerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeStudentExamAnswer {

    @RequestMapping(value = "/exam/gradeStudentExamAnswer/listForPage", method = RequestMethod.POST)
    Page<GradeStudentExamAnswerVO> listForPage(@RequestBody GradeStudentExamAnswerQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamAnswer/save", method = RequestMethod.POST)
    int save(@RequestBody GradeStudentExamAnswerQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamAnswer/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeStudentExamAnswer/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeStudentExamAnswerQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamAnswer/get/{id}", method = RequestMethod.GET)
    GradeStudentExamAnswerVO getById(@PathVariable(value = "id") Long id);

}
