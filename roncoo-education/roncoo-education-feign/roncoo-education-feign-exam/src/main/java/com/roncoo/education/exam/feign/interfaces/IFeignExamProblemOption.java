package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamProblemOptionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemOptionVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷题目选项 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamProblemOption {

    @RequestMapping(value = "/exam/examProblemOption/listForPage", method = RequestMethod.POST)
    Page<ExamProblemOptionVO> listForPage(@RequestBody ExamProblemOptionQO qo);

    @RequestMapping(value = "/exam/examProblemOption/save", method = RequestMethod.POST)
    int save(@RequestBody ExamProblemOptionQO qo);

    @RequestMapping(value = "/exam/examProblemOption/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examProblemOption/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamProblemOptionQO qo);

    @RequestMapping(value = "/exam/examProblemOption/get/{id}", method = RequestMethod.GET)
    ExamProblemOptionVO getById(@PathVariable(value = "id") Long id);

}
