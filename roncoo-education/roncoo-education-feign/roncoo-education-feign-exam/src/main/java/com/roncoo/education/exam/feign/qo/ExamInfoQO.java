package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 试卷信息
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamInfoQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 试卷排序
     */
    private Integer examSort;

    /**
     * 试卷名称
     */
    private String examName;

    /**
     * 地区
     */
    private String region;

    /**
     * 年级id
     */
    private Long gradeId;

    /**
     * 科目id
     */
    private Long subjectId;

    /**
     * 年份id
     */
    private Long yearId;

    /**
     * 来源id
     */
    private Long sourceId;

    /**
     * 是否上架(1:上架，0:下架)
     */
    private Integer isPutaway;

    /**
     * 是否免费：1免费，0收费
     */
    private Integer isFree;

    /**
     * 优惠价
     */
    private BigDecimal fabPrice;

    /**
     * 原价
     */
    private BigDecimal orgPrice;

    /**
     * 答卷时间（分钟）
     */
    private Integer answerTime;

    /**
     * 总分
     */
    private Integer totalScore;

    /**
     * 试题数量
     */
    private Integer problemQuantity;

    /**
     * 描述
     */
    private String description;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 购买人数
     */
    private Integer countBuy;

    /**
     * 学习人数
     */
    private Integer studyCount;

    /**
     * 收藏人数
     */
    private Integer collectionCount;

    /**
     * 下载人数
     */
    private Integer downloadCount;

    /**
     * 个人分类id
     */
    private Long personalId;

}
