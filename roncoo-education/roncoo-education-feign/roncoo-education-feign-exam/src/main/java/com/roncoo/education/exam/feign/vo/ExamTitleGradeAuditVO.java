package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 试卷标题审核  班级阅卷用
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ExamTitleGradeAuditVO extends ExamTitleAuditVO{
    List<ExamProblemGradeAuditVO> problemList;

}
