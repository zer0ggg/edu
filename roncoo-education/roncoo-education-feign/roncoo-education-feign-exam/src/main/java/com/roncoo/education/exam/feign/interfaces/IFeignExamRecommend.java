package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamRecommendQO;
import com.roncoo.education.exam.feign.vo.ExamRecommendVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷推荐 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamRecommend {

    @RequestMapping(value = "/exam/examRecommend/listForPage", method = RequestMethod.POST)
    Page<ExamRecommendVO> listForPage(@RequestBody ExamRecommendQO qo);

    @RequestMapping(value = "/exam/examRecommend/save", method = RequestMethod.POST)
    int save(@RequestBody ExamRecommendQO qo);

    @RequestMapping(value = "/exam/examRecommend/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examRecommend/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamRecommendQO qo);

    @RequestMapping(value = "/exam/examRecommend/get/{id}", method = RequestMethod.GET)
    ExamRecommendVO getById(@PathVariable(value = "id") Long id);

}
