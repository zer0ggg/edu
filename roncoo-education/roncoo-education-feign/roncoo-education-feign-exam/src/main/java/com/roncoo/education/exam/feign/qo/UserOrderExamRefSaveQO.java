package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户订单试卷
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class UserOrderExamRefSaveQO implements Serializable {

    private static final long serialVersionUID = 6181712507840685891L;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 订单号
     */
    private Long orderNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 过期时间
     */
    private Date expireTime;
}
