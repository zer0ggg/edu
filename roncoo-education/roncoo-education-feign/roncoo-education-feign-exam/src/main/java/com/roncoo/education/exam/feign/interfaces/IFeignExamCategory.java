package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamCategoryQO;
import com.roncoo.education.exam.feign.vo.ExamCategoryVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷分类 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamCategory {

    @RequestMapping(value = "/exam/examCategory/listForPage", method = RequestMethod.POST)
    Page<ExamCategoryVO> listForPage(@RequestBody ExamCategoryQO qo);

    @RequestMapping(value = "/exam/examCategory/save", method = RequestMethod.POST)
    int save(@RequestBody ExamCategoryQO qo);

    @RequestMapping(value = "/exam/examCategory/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examCategory/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamCategoryQO qo);

    @RequestMapping(value = "/exam/examCategory/get/{id}", method = RequestMethod.GET)
    ExamCategoryVO getById(@PathVariable(value = "id") Long id);

}
