package com.roncoo.education.exam.feign.interfaces;


import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeExamQO;
import com.roncoo.education.exam.feign.vo.GradeExamVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeExam {

    @RequestMapping(value = "/exam/gradeExam/listForPage", method = RequestMethod.POST)
    Page<GradeExamVO> listForPage(@RequestBody GradeExamQO qo);

    @RequestMapping(value = "/exam/gradeExam/save", method = RequestMethod.POST)
    int save(@RequestBody GradeExamQO qo);

    @RequestMapping(value = "/exam/gradeExam/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeExam/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeExamQO qo);

    @RequestMapping(value = "/exam/gradeExam/get/{id}", method = RequestMethod.GET)
    GradeExamVO getById(@PathVariable(value = "id") Long id);

}
