package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeStudentExamTitleScoreQO;
import com.roncoo.education.exam.feign.vo.GradeStudentExamTitleScoreVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeStudentExamTitleScore {

    @RequestMapping(value = "/exam/gradeStudentExamTitleScore/listForPage", method = RequestMethod.POST)
    Page<GradeStudentExamTitleScoreVO> listForPage(@RequestBody GradeStudentExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamTitleScore/save", method = RequestMethod.POST)
    int save(@RequestBody GradeStudentExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamTitleScore/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeStudentExamTitleScore/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeStudentExamTitleScoreQO qo);

    @RequestMapping(value = "/exam/gradeStudentExamTitleScore/get/{id}", method = RequestMethod.GET)
    GradeStudentExamTitleScoreVO getById(@PathVariable(value = "id") Long id);

}
