package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeApplyRecordQO;
import com.roncoo.education.exam.feign.vo.GradeApplyRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeApplyRecord {

    @RequestMapping(value = "/exam/gradeApplyRecord/listForPage", method = RequestMethod.POST)
    Page<GradeApplyRecordVO> listForPage(@RequestBody GradeApplyRecordQO qo);

    @RequestMapping(value = "/exam/gradeApplyRecord/save", method = RequestMethod.POST)
    int save(@RequestBody GradeApplyRecordQO qo);

    @RequestMapping(value = "/exam/gradeApplyRecord/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeApplyRecord/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeApplyRecordQO qo);

    @RequestMapping(value = "/exam/gradeApplyRecord/get/{id}", method = RequestMethod.GET)
    GradeApplyRecordVO getById(@PathVariable(value = "id") Long id);

}
