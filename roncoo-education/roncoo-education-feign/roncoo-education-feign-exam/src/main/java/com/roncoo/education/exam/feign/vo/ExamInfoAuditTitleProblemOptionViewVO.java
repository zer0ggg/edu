package com.roncoo.education.exam.feign.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 试卷题目选项
 *
 * @author LYQ
 */
@Data
public class ExamInfoAuditTitleProblemOptionViewVO implements Serializable {

    private static final long serialVersionUID = 5766277595882098022L;

    /**
     *
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 题目ID
     */
    private Long problemId;

    /**
     * 选项内容
     */
    private String optionContent;
}
