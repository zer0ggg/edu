package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamProblemQO;
import com.roncoo.education.exam.feign.vo.ExamProblemVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷题目 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamProblem {

    @RequestMapping(value = "/exam/examProblem/listForPage", method = RequestMethod.POST)
    Page<ExamProblemVO> listForPage(@RequestBody ExamProblemQO qo);

    @RequestMapping(value = "/exam/examProblem/save", method = RequestMethod.POST)
    int save(@RequestBody ExamProblemQO qo);

    @RequestMapping(value = "/exam/examProblem/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examProblem/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamProblemQO qo);

    @RequestMapping(value = "/exam/examProblem/get/{id}", method = RequestMethod.GET)
    ExamProblemVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examProblem/updateByVideoId", method = RequestMethod.PUT)
    int updateByVideoId(@RequestBody ExamProblemQO examProblemQO);

    @RequestMapping(value = "/exam/examProblem/updateByVid", method = RequestMethod.PUT)
    int updateByVid(@RequestBody ExamProblemQO examProblemQO);
}
