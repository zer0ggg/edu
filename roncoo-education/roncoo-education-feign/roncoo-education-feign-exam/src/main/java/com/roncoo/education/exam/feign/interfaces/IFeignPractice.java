package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.PracticeQO;
import com.roncoo.education.exam.feign.vo.PracticeVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 练习信息 接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@FeignClient(value = "service-exam")
public interface IFeignPractice{

    @RequestMapping(value = "/exam/practice/listForPage", method = RequestMethod.POST)
    Page<PracticeVO> listForPage(@RequestBody PracticeQO qo);

    @RequestMapping(value = "/exam/practice/save", method = RequestMethod.POST)
    int save(@RequestBody PracticeQO qo);

    @RequestMapping(value = "/exam/practice/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/practice/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody PracticeQO qo);

    @RequestMapping(value = "/exam/practice/get/{id}", method = RequestMethod.GET)
    PracticeVO getById(@PathVariable(value = "id") Long id);

}
