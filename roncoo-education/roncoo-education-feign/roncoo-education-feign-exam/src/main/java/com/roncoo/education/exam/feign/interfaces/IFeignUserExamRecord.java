package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamRecordQO;
import com.roncoo.education.exam.feign.vo.UserExamRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户考试记录 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExamRecord {

    @RequestMapping(value = "/exam/userExamRecord/listForPage", method = RequestMethod.POST)
    Page<UserExamRecordVO> listForPage(@RequestBody UserExamRecordQO qo);

    @RequestMapping(value = "/exam/userExamRecord/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamRecordQO qo);

    @RequestMapping(value = "/exam/userExamRecord/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExamRecord/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamRecordQO qo);

    @RequestMapping(value = "/exam/userExamRecord/get/{id}", method = RequestMethod.GET)
    UserExamRecordVO getById(@PathVariable(value = "id") Long id);

}
