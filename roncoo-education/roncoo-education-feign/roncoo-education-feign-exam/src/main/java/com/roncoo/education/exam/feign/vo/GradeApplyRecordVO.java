package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
public class GradeApplyRecordVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 班级ID
     */
    private Long gradeId;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 申请描述
     */
    private String applyDescription;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 审核状态(1:待审核，2:同意，3:拒绝)
     */
    private Integer auditStatus;

    /**
     * 审核意见
     */
    private String auditOpinion;

    /**
     * 审核用户编号
     */
    private Long auditUserNo;
}
