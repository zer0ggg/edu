package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户订单试卷关联
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class UserOrderExamRefQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 试卷ID
     */
    private Long examId;

}
