package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.PracticeUserAnswerQO;
import com.roncoo.education.exam.feign.vo.PracticeUserAnswerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户考试答案 接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@FeignClient(value = "service-exam")
public interface IFeignPracticeUserAnswer{

    @RequestMapping(value = "/exam/practiceUserAnswer/listForPage", method = RequestMethod.POST)
    Page<PracticeUserAnswerVO> listForPage(@RequestBody PracticeUserAnswerQO qo);

    @RequestMapping(value = "/exam/practiceUserAnswer/save", method = RequestMethod.POST)
    int save(@RequestBody PracticeUserAnswerQO qo);

    @RequestMapping(value = "/exam/practiceUserAnswer/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/practiceUserAnswer/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody PracticeUserAnswerQO qo);

    @RequestMapping(value = "/exam/practiceUserAnswer/get/{id}", method = RequestMethod.GET)
    PracticeUserAnswerVO getById(@PathVariable(value = "id") Long id);

}
