package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 课程试卷关联
 *
 * @author wujing
 * @date 2020-10-21
 */
@Data
@Accessors(chain = true)
public class CourseExamRefVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 关联类型：1课程，2章节，3课时
     */
    private Integer refType;

    /**
     * 关联ID
     */
    private Long refId;

    /**
     * 试卷ID
     */
    private Long examId;
    /**
     * 试卷名称
     */
    private String examName;
}
