package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamVideoQO;
import com.roncoo.education.exam.feign.vo.ExamVideoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷视频 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamVideo {

    @RequestMapping(value = "/exam/examVideo/listForPage", method = RequestMethod.POST)
    Page<ExamVideoVO> listForPage(@RequestBody ExamVideoQO qo);

    @RequestMapping(value = "/exam/examVideo/save", method = RequestMethod.POST)
    int save(@RequestBody ExamVideoQO qo);

    @RequestMapping(value = "/exam/examVideo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examVideo/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamVideoQO qo);

    @RequestMapping(value = "/exam/examVideo/get/{id}", method = RequestMethod.GET)
    ExamVideoVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examVideo/getByVideoVid/{vid}", method = RequestMethod.GET)
    ExamVideoVO getByVideoVid(@PathVariable(value = "vid") String vid);

    @RequestMapping(value = "/exam/examVideo/updateByVid", method = RequestMethod.PUT)
    int updateByVid(@RequestBody ExamVideoQO examVideoQO);
}
