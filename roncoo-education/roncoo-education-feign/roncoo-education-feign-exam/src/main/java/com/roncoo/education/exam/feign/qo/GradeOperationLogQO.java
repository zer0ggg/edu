package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
public class GradeOperationLogQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 班级ID
     */
    private Long gradeId;

    /**
     * 操作员用户编号
     */
    private Long operatorUserNo;

    /**
     * 操作类型
     */
    private Integer operationType;

    /**
     * 操作内容
     */
    private String operationContent;

}
