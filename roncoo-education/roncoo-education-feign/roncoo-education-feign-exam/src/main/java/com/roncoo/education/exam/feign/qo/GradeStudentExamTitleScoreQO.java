package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
public class GradeStudentExamTitleScoreQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 关联ID
     */
    private Long relationId;

    /**
     * 班级ID
     */
    private Long gradeId;

    /**
     * 班级考试ID
     */
    private Long gradeExamId;

    /**
     * 学生ID
     */
    private Long studentId;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题ID
     */
    private Long titleId;

    /**
     * 得分
     */
    private Integer score;

    /**
     * 系统评阅总分
     */
    private Integer sysAuditTotalScore;

    /**
     * 系统评阅得分
     */
    private Integer sysAuditScore;

    /**
     * 讲师评阅总分
     */
    private Integer lecturerAuditTotalScore;

    /**
     * 讲师评阅得分
     */
    private Integer lecturerAuditScore;

}
