package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 试卷视频
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamVideoQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 题目ID
     */
    private Long problemId;

    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 视频状态(1待上传，2上传成功，3上传失败)
     */
    private Integer videoStatus;

    /**
     * 时长
     */
    private String videoLength;

    /**
     * 视频ID
     */
    private String videoVid;

    /**
     * 阿里云oss
     */
    private String videoOssId;

}
