package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeExamStudentRelationQO;
import com.roncoo.education.exam.feign.vo.GradeExamStudentRelationVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeExamStudentRelation {

    @RequestMapping(value = "/exam/gradeExamStudentRelation/listForPage", method = RequestMethod.POST)
    Page<GradeExamStudentRelationVO> listForPage(@RequestBody GradeExamStudentRelationQO qo);

    @RequestMapping(value = "/exam/gradeExamStudentRelation/save", method = RequestMethod.POST)
    int save(@RequestBody GradeExamStudentRelationQO qo);

    @RequestMapping(value = "/exam/gradeExamStudentRelation/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeExamStudentRelation/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeExamStudentRelationQO qo);

    @RequestMapping(value = "/exam/gradeExamStudentRelation/get/{id}", method = RequestMethod.GET)
    GradeExamStudentRelationVO getById(@PathVariable(value = "id") Long id);

}
