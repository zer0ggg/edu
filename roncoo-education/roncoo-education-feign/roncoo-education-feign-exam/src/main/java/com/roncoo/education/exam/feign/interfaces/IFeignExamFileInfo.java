package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.FileInfoQO;
import com.roncoo.education.exam.feign.vo.FileInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文件信息 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamFileInfo {

    @RequestMapping(value = "/exam/fileInfo/listForPage", method = RequestMethod.POST)
    Page<FileInfoVO> listForPage(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/exam/fileInfo/save", method = RequestMethod.POST)
    int save(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/exam/fileInfo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/fileInfo/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody FileInfoQO qo);

    @RequestMapping(value = "/exam/fileInfo/get/{id}", method = RequestMethod.GET)
    FileInfoVO getById(@PathVariable(value = "id") Long id);

}
