package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户考试记录
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class UserExamRecordQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 答卷时间
     */
    private Integer answerTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 总分
     */
    private Integer totalScore;

    /**
     * 得分
     */
    private Integer score;

    /**
     * 系统评阅分值
     */
    private Integer sysAuditTotalScore;

    /**
     * 系统评阅得分
     */
    private Integer sysAuditScore;

    /**
     * 人工评阅分值
     */
    private Integer handworkAuditTotalScore;

    /**
     * 人工评阅得分
     */
    private Integer handworkAuditScore;

    /**
     * 试卷状态(1:考试中、2:考试完成、3:评阅完成)
     */
    private Integer examStatus;

}
