package com.roncoo.education.exam.feign.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 试卷信息详情查看
 *
 * @author LYQ
 */
@Data
public class ExamInfoAuditViewVO implements Serializable {

    private static final long serialVersionUID = -3486583605646008127L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师编号
     */
    private Long lecturerUserNo;

    /**
     * 试卷排序
     */
    private Integer examSort;

    /**
     * 试卷名称
     */
    private String examName;

    /**
     * 地区
     */
    private String region;

    /**
     * 年级id
     */
    private Long gradeId;

    /**
     * 科目id
     */
    private Long subjectId;

    /**
     * 年份id
     */
    private Long yearId;

    /**
     * 来源id
     */
    private Long sourceId;

    /**
     * 是否上架(1:上架，0:下架)
     */
    private Integer isPutaway;

    /**
     * 是否免费：1免费，0收费
     */
    private Integer isFree;

    /**
     * 优惠价
     */
    private BigDecimal fabPrice;

    /**
     * 原价
     */
    private BigDecimal orgPrice;

    /**
     * 答卷时间（分钟）
     */
    private Integer answerTime;

    /**
     * 总分
     */
    private Integer totalScore;

    /**
     * 试题数量
     */
    private Integer problemQuantity;

    /**
     * 描述
     */
    private String description;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 审核状态(0:待审核,1:审核通过,2:审核不通过)
     */
    private Integer auditStatus;

    /**
     * 审核意见
     */
    private String auditOpinion;

    /**
     * 个人分类id
     */
    private Long personalId;

    /**
     * 提交审核
     */
    private Integer submitAudit;

    /**
     * 标题集合
     */
    private List<ExamInfoAuditTitleViewVO> titleList;
}
