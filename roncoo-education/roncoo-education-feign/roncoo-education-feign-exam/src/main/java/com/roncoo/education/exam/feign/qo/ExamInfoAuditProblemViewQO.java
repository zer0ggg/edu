package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class ExamInfoAuditProblemViewQO implements Serializable {

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题ID
     */
    private Long titleId;

    /**
     * 题目Id
     */
    private Long problemId;
}
