package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamInfoQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷信息 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamInfo {

    @RequestMapping(value = "/exam/examInfo/listForPage", method = RequestMethod.POST)
    Page<ExamInfoVO> listForPage(@RequestBody ExamInfoQO qo);

    @RequestMapping(value = "/exam/examInfo/save", method = RequestMethod.POST)
    int save(@RequestBody ExamInfoQO qo);

    @RequestMapping(value = "/exam/examInfo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examInfo/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamInfoQO qo);

    @RequestMapping(value = "/exam/examInfo/get/{id}", method = RequestMethod.GET)
    ExamInfoVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examInfo/get/{id}/{statusId}", method = RequestMethod.GET)
    ExamInfoVO getByIdAndStatusId(@PathVariable(value = "id") Long id, @PathVariable(value = "statusId") Integer statusId);

}
