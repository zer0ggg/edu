package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.PracticeProblemRefQO;
import com.roncoo.education.exam.feign.vo.PracticeProblemRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 题目练习关联 接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@FeignClient(value = "service-exam")
public interface IFeignPracticeProblemRef{

    @RequestMapping(value = "/exam/practiceProblemRef/listForPage", method = RequestMethod.POST)
    Page<PracticeProblemRefVO> listForPage(@RequestBody PracticeProblemRefQO qo);

    @RequestMapping(value = "/exam/practiceProblemRef/save", method = RequestMethod.POST)
    int save(@RequestBody PracticeProblemRefQO qo);

    @RequestMapping(value = "/exam/practiceProblemRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/practiceProblemRef/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody PracticeProblemRefQO qo);

    @RequestMapping(value = "/exam/practiceProblemRef/get/{id}", method = RequestMethod.GET)
    PracticeProblemRefVO getById(@PathVariable(value = "id") Long id);

}
