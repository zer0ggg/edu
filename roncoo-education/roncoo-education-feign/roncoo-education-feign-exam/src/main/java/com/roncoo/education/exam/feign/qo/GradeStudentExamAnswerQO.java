package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
public class GradeStudentExamAnswerQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 关联ID
     */
    private Long relationId;

    /**
     * 班级ID
     */
    private Long gradeId;

    /**
     * 班级考试ID
     */
    private Long gradeExamId;

    /**
     * 学生ID
     */
    private Long studentId;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题ID
     */
    private Long titleId;

    /**
     * 题目父类ID
     */
    private Long problemParentId;

    /**
     * 题目ID
     */
    private Long problemId;

    /**
     * 用户答案
     */
    private String userAnswer;

    /**
     * 题目分值
     */
    private Integer problemScore;

    /**
     * 得分
     */
    private Integer score;

    /**
     * 答案状态(1:待评阅，2:已评阅)
     */
    private Integer answerStatus;

    /**
     * 系统审核(1:未确定，2:系统审核，3:人工审核)
     */
    private Integer sysAudit;

    /**
     * 评阅用户编号
     */
    private Long auditUserNo;

}
