package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefAuditQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefAuditVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷标题题目关联审核 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamTitleProblemRefAudit {

    @RequestMapping(value = "/exam/examTitleProblemRefAudit/listForPage", method = RequestMethod.POST)
    Page<ExamTitleProblemRefAuditVO> listForPage(@RequestBody ExamTitleProblemRefAuditQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRefAudit/save", method = RequestMethod.POST)
    int save(@RequestBody ExamTitleProblemRefAuditQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRefAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examTitleProblemRefAudit/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamTitleProblemRefAuditQO qo);

    @RequestMapping(value = "/exam/examTitleProblemRefAudit/get/{id}", method = RequestMethod.GET)
    ExamTitleProblemRefAuditVO getById(@PathVariable(value = "id") Long id);

}
