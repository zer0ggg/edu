package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class ExamInfoAuditLecturerViewQO implements Serializable {

    /**
     * 试卷ID
     */
    private Long id;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;
}
