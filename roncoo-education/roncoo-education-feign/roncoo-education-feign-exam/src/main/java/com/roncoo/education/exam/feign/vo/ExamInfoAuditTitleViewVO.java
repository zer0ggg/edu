package com.roncoo.education.exam.feign.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 试卷标题信息
 *
 * @author LYQ
 */
@Data
public class ExamInfoAuditTitleViewVO implements Serializable {

    private static final long serialVersionUID = -8186668478943667737L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师用户编码
     */
    private Long lecturerUserNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题名称
     */
    private String titleName;

    /**
     * 标题类型
     */
    private Integer titleType;

    /**
     * 审核状态(0:待审核,1:审核通过,2:审核不通过)
     */
    private Integer auditStatus;

    /**
     * 题目集合
     */
    private List<ExamInfoAuditTitleProblemViewVO> problemList;

}
