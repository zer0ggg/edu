package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 试卷标题
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamTitleVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师用户编码
     */
    private Long lecturerUserNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题名称
     */
    private String titleName;

    /**
     * 标题类型
     */
    private Integer titleType;
}
