package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamProblemCollectionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemCollectionVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷题目收藏 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamProblemCollection {

    @RequestMapping(value = "/exam/examProblemCollection/listForPage", method = RequestMethod.POST)
    Page<ExamProblemCollectionVO> listForPage(@RequestBody ExamProblemCollectionQO qo);

    @RequestMapping(value = "/exam/examProblemCollection/save", method = RequestMethod.POST)
    int save(@RequestBody ExamProblemCollectionQO qo);

    @RequestMapping(value = "/exam/examProblemCollection/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examProblemCollection/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamProblemCollectionQO qo);

    @RequestMapping(value = "/exam/examProblemCollection/get/{id}", method = RequestMethod.GET)
    ExamProblemCollectionVO getById(@PathVariable(value = "id") Long id);

}
