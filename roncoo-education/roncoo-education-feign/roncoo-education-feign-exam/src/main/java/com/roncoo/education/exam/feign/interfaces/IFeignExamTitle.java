package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamTitleQO;
import com.roncoo.education.exam.feign.vo.ExamTitleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 试卷标题 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamTitle {

    @RequestMapping(value = "/exam/examTitle/listForPage", method = RequestMethod.POST)
    Page<ExamTitleVO> listForPage(@RequestBody ExamTitleQO qo);

    @RequestMapping(value = "/exam/examTitle/save", method = RequestMethod.POST)
    int save(@RequestBody ExamTitleQO qo);

    @RequestMapping(value = "/exam/examTitle/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/examTitle/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ExamTitleQO qo);

    @RequestMapping(value = "/exam/examTitle/get/{id}", method = RequestMethod.GET)
    ExamTitleVO getById(@PathVariable(value = "id") Long id);

}
