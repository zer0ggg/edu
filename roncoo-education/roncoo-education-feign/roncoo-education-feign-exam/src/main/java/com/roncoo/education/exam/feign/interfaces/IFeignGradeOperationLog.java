package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.GradeOperationLogQO;
import com.roncoo.education.exam.feign.vo.GradeOperationLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@FeignClient(value = "service-exam")
public interface IFeignGradeOperationLog {

    @RequestMapping(value = "/exam/gradeOperationLog/listForPage", method = RequestMethod.POST)
    Page<GradeOperationLogVO> listForPage(@RequestBody GradeOperationLogQO qo);

    @RequestMapping(value = "/exam/gradeOperationLog/save", method = RequestMethod.POST)
    int save(@RequestBody GradeOperationLogQO qo);

    @RequestMapping(value = "/exam/gradeOperationLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/gradeOperationLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody GradeOperationLogQO qo);

    @RequestMapping(value = "/exam/gradeOperationLog/get/{id}", method = RequestMethod.GET)
    GradeOperationLogVO getById(@PathVariable(value = "id") Long id);

}
