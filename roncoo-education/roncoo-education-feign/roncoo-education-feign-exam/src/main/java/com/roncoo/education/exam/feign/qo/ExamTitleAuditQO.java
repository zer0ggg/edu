package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 试卷标题审核
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamTitleAuditQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师用户编码
     */
    private Long lecturerUserNo;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 标题名称
     */
    private String titleName;

    /**
     * 标题类型
     */
    private Integer titleType;

    /**
     * 审核状态(0:待审核,1:审核通过,2:审核不通过)
     */
    private Integer auditStatus;

}
