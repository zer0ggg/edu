package com.roncoo.education.exam.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 试卷题目收藏
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamProblemCollectionVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效，0：无效)
     */
    private Integer statusId;

    /**
     * 用户编码
     */
    private Long userNo;

    /**
     * 收藏类型(4:试卷,5:题目)
     */
    private Integer collectionType;

    /**
     * 题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:材料题)
     */
    private Integer problemType;

    /**
     * 收藏ID
     */
    private Long collectionId;
}
