package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.UserExamAnswerQO;
import com.roncoo.education.exam.feign.vo.UserExamAnswerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户考试答案 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignUserExamAnswer {

    @RequestMapping(value = "/exam/userExamAnswer/listForPage", method = RequestMethod.POST)
    Page<UserExamAnswerVO> listForPage(@RequestBody UserExamAnswerQO qo);

    @RequestMapping(value = "/exam/userExamAnswer/save", method = RequestMethod.POST)
    int save(@RequestBody UserExamAnswerQO qo);

    @RequestMapping(value = "/exam/userExamAnswer/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/exam/userExamAnswer/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExamAnswerQO qo);

    @RequestMapping(value = "/exam/userExamAnswer/get/{id}", method = RequestMethod.GET)
    UserExamAnswerVO getById(@PathVariable(value = "id") Long id);

}
