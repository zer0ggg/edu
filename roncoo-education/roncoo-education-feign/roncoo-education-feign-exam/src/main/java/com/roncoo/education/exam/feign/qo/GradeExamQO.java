package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
public class GradeExamQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 班级ID
     */
    private Long gradeId;

    /**
     * 班级考试名称
     */
    private String gradeExamName;

    /**
     * 科目ID
     */
    private Long subjectId;

    /**
     * 试卷ID
     */
    private Long examId;

    /**
     * 开始时间
     */
    private LocalDateTime beginTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)
     */
    private Integer answerShow;

    /**
     * 答案展示时间
     */
    private LocalDateTime answerShowTime;

    /**
     * 评阅类型(1:不评阅，2:评阅)
     */
    private Integer auditType;

    /**
     * 补交状态(1:关闭，2:开启)
     */
    private Integer compensateStatus;

}
