package com.roncoo.education.exam.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 试卷题目
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
public class ExamProblemQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 讲师编号
     */
    private Long lecturerUserNo;

    /**
     * 地区
     */
    private String region;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)
     */
    private Integer problemType;

    /**
     * 题目内容
     */
    private String problemContent;

    /**
     * 题目答案
     */
    private String problemAnswer;

    /**
     * 解析
     */
    private String analysis;

    /**
     * 考点
     */
    private String emphasis;

    /**
     * 难度等级
     */
    private Integer examLevel;

    /**
     * 收藏人数
     */
    private Integer collectionCount;

    /**
     * 是否免费：1免费，0收费
     */
    private Integer isFree;

    /**
     * 分值
     */
    private Integer score;

    /**
     * 视频类型(1:解答视频,2:试题视频)
     */
    private Integer videoType;

    /**
     * 视频ID
     */
    private Long videoId;

    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 时长
     */
    private String videoLength;

    /**
     * 视频VID
     */
    private String videoVid;

    /**
     * 年级id
     */
    private Long gradeId;

    /**
     * 科目id
     */
    private Long subjectId;

    /**
     * 年份id
     */
    private Long yearId;

    /**
     * 来源id
     */
    private Long sourceId;

    /**
     * 难度id
     */
    private Long difficultyId;

    /**
     * 题类id
     */
    private Long topicId;

    /**
     * 个人分类id
     */
    private Long personalId;

}
