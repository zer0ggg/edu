package com.roncoo.education.exam.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditLecturerViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditProblemViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditQO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditProblemViewVO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditVO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditViewVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷信息审核 接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@FeignClient(value = "service-exam")
public interface IFeignExamInfoAudit {

    @PostMapping(value = "/exam/examInfoAudit/listForPage")
    Page<ExamInfoAuditVO> listForPage(@RequestBody ExamInfoAuditQO qo);

    @PostMapping(value = "/exam/examInfoAudit/save")
    int save(@RequestBody ExamInfoAuditQO qo);

    @DeleteMapping(value = "/exam/examInfoAudit/delete/{id}")
    int deleteById(@PathVariable(value = "id") Long id);

    @PutMapping(value = "/exam/examInfoAudit/updateById")
    int updateById(@RequestBody ExamInfoAuditQO qo);

    @GetMapping(value = "/exam/examInfoAudit/get/{id}")
    ExamInfoAuditVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据ID和讲师用户编号获取试卷审核信息
     *
     * @param qo 查询参数
     * @return 试卷审核信息
     */
    @GetMapping(value = "/exam/examInfoAudit/findIdAndLecturerUserNo")
    ExamInfoAuditVO getByIdAndLecturerUserNo(@RequestBody ExamInfoAuditLecturerViewQO qo);

    /**
     * 获取试题ID
     *
     * @param qo 查询参数
     * @return 试卷试题
     */
    @GetMapping(value = "/exam/examInfoAudit/problem")
    ExamInfoAuditProblemViewVO getExamProblem(@RequestBody ExamInfoAuditProblemViewQO qo);

    /**
     * 根据ID获取试卷详细信息
     *
     * @param id 试卷ID
     * @return 试卷审核信息
     */
    @GetMapping(value = "/exam/examInfoAudit/view/{id}")
    ExamInfoAuditViewVO getExamInfoById(@PathVariable(value = "id") Long id);
}
