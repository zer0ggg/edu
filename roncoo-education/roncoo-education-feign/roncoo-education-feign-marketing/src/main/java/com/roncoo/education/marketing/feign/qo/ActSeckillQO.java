package com.roncoo.education.marketing.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 课程秒杀活动
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActSeckillQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动Id
     */
    private Long actId;

    /**
     * 课程Id
     */
    private Long courseId;

    /**
     * 秒杀价
     */
    private BigDecimal seckillPrice;

    /**
     * 是否限购(1:是;0:否)
     */
    private Integer isLimit;

    /**
     * 限购人数
     */
    private Integer buyer;

    /**
     * 剩余数量
     */
    private Integer surplus;

}
