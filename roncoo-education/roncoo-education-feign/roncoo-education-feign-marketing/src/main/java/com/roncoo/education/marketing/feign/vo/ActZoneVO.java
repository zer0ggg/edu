package com.roncoo.education.marketing.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 活动专区
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActZoneVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动Id
     */
    private Long actId;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;

    /**
     * 专区标题
     */
    private String zoneTitle;

    private Date beginTime;

    private Date endTime;
}
