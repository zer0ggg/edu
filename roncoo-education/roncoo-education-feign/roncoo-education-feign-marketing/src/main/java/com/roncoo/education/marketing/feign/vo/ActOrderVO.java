package com.roncoo.education.marketing.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 活动信息表
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActOrderVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动Id（如果是优惠券就是优惠券ID，如果是秒杀该字段为秒杀ID）下单使用
     */
    private Long actTypeId;
    /**
     * 活动实付价格(下单使用)
     */
    private BigDecimal actPricePaid;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;
}
