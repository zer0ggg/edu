package com.roncoo.education.marketing.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActZoneCourseQO;
import com.roncoo.education.marketing.feign.vo.ActZoneCourseVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 活动专区课程 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignActZoneCourse {

    @RequestMapping(value = "/marketing/actZoneCourse/listForPage", method = RequestMethod.POST)
    Page<ActZoneCourseVO> listForPage(@RequestBody ActZoneCourseQO qo);

    @RequestMapping(value = "/marketing/actZoneCourse/save", method = RequestMethod.POST)
    int save(@RequestBody ActZoneCourseQO qo);

    @RequestMapping(value = "/marketing/actZoneCourse/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actZoneCourse/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActZoneCourseQO qo);

    @RequestMapping(value = "/marketing/actZoneCourse/get/{id}", method = RequestMethod.GET)
    ActZoneCourseVO getById(@PathVariable(value = "id") Long id);

    /**
     * 订单回调更新活动
     *
     * @param actZoneCourseQO
     */
    @RequestMapping(value = "/marketing/actZoneCourse/updateByOrder", method = RequestMethod.POST)
    int updateByOrder(@RequestBody ActZoneCourseQO actZoneCourseQO);
}
