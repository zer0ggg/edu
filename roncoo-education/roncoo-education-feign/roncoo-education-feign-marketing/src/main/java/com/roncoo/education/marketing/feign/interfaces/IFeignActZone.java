package com.roncoo.education.marketing.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActZoneQO;
import com.roncoo.education.marketing.feign.vo.ActZoneVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 活动专区 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignActZone {

    @RequestMapping(value = "/marketing/actZone/listForPage", method = RequestMethod.POST)
    Page<ActZoneVO> listForPage(@RequestBody ActZoneQO qo);

    @RequestMapping(value = "/marketing/actZone/save", method = RequestMethod.POST)
    int save(@RequestBody ActZoneQO qo);

    @RequestMapping(value = "/marketing/actZone/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actZone/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActZoneQO qo);

    @RequestMapping(value = "/marketing/actZone/get/{id}", method = RequestMethod.GET)
    ActZoneVO getById(@PathVariable(value = "id") Long id);

}
