package com.roncoo.education.marketing.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActSeckillQO;
import com.roncoo.education.marketing.feign.vo.ActSeckillVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程秒杀活动 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignActSeckill {

    @RequestMapping(value = "/marketing/actSeckill/listForPage", method = RequestMethod.POST)
    Page<ActSeckillVO> listForPage(@RequestBody ActSeckillQO qo);

    @RequestMapping(value = "/marketing/actSeckill/save", method = RequestMethod.POST)
    int save(@RequestBody ActSeckillQO qo);

    @RequestMapping(value = "/marketing/actSeckill/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actSeckill/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActSeckillQO qo);

    @RequestMapping(value = "/marketing/actSeckill/get/{id}", method = RequestMethod.GET)
    ActSeckillVO getById(@PathVariable(value = "id") Long id);

}
