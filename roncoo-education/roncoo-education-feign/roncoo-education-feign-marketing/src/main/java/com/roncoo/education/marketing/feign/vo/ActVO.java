package com.roncoo.education.marketing.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 活动信息表
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Data gmtCreate;

    /**
     * 修改时间
     */
    private Data gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;

    /**
     * 活动标题
     */
    private String actTitle;

    /**
     * 活动图片
     */
    private String actImg;

    /**
     * 活动开始时间
     */
    private Data beginTime;

    /**
     * 活动结束时间
     */
    private Data endTime;
    /**
     * 活动Id（如果是优惠券就是优惠券ID，如果是秒杀该字段为秒杀ID）下单使用
     */
    private Long actTypeId;
    /**
     * 活动实付价格(下单使用)
     */
    private BigDecimal actPricePaid;
}
