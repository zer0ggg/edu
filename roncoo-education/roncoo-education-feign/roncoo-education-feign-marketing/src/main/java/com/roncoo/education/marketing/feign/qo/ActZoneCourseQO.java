package com.roncoo.education.marketing.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 活动专区课程
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActZoneCourseQO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private int pageCurrent;
    /**
     * 每页记录数
     */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动Id
     */
    private Long actId;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;

    /**
     * 专区Id
     */
    private Long zoneId;

    /**
     * 课程Id
     */
    private Long courseId;

    /**
     * 课程封面
     */
    private String courseLogo;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 课程分类(1:点播;2:直播)
     */
    private Integer courseCategory;

    /**
     * 原价
     */
    private BigDecimal courseOriginal;

    /**
     * 优惠价
     */
    private BigDecimal courseDiscount;

    /**
     * 活动价格
     */
    private BigDecimal actPrice;
    /**
     * 活动ID（如果是优惠券就是优惠券ID，如果是秒杀该字段为秒杀ID）
     */
    private Long actTypeId;

}
