package com.roncoo.education.marketing.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActCouponQO;
import com.roncoo.education.marketing.feign.vo.ActCouponVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 优惠券活动 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignActCoupon {

    @RequestMapping(value = "/marketing/actCoupon/listForPage", method = RequestMethod.POST)
    Page<ActCouponVO> listForPage(@RequestBody ActCouponQO qo);

    @RequestMapping(value = "/marketing/actCoupon/save", method = RequestMethod.POST)
    int save(@RequestBody ActCouponQO qo);

    @RequestMapping(value = "/marketing/actCoupon/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actCoupon/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActCouponQO qo);

    @RequestMapping(value = "/marketing/actCoupon/get/{id}", method = RequestMethod.GET)
    ActCouponVO getById(@PathVariable(value = "id") Long id);

}
