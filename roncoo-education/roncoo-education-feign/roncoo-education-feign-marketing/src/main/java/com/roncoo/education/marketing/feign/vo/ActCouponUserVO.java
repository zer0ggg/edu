package com.roncoo.education.marketing.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 优惠券用户关联
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActCouponUserVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动Id
     */
    private Long actId;

    /**
     * 优惠券ID
     */
    private Long couponId;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 优惠价格
     */
    private BigDecimal couponPrice;

    /**
     * 过期时间
     */
    private LocalDateTime pastTime;

    /**
     * 是否使用（1使用，0未使用）
     */
    private Integer isUse;
}
