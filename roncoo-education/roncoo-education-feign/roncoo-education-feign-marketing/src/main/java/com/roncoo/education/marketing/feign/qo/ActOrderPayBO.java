package com.roncoo.education.marketing.feign.qo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 订单支付信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ActOrderPayBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "课程编号,试卷编号,会员主键")
    private Long productId;

    @ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
    private Integer payType;

    @ApiModelProperty(value = "购买渠道：1web")
    private Integer channelType;

    @ApiModelProperty(value = "用户备注")
    private String remarkCus;

    @ApiModelProperty(value = "产品类型：(1普通,2直播,3会员,4文库,5试卷)")
    private Integer productType;

    @ApiModelProperty(value = "活动id（目前只支持点播、直播、文库课程参与活动）")
    private Long actId;

    @ApiModelProperty(value = "推荐码")
    private String referralCode;
}
