package com.roncoo.education.marketing.feign.interfaces;


import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActCouponUserQO;
import com.roncoo.education.marketing.feign.vo.ActCouponUserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 优惠券用户关联 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignActCouponUser {

    @RequestMapping(value = "/marketing/actCouponUser/listForPage", method = RequestMethod.POST)
    Page<ActCouponUserVO> listForPage(@RequestBody ActCouponUserQO qo);

    @RequestMapping(value = "/marketing/actCouponUser/save", method = RequestMethod.POST)
    int save(@RequestBody ActCouponUserQO qo);

    @RequestMapping(value = "/marketing/actCouponUser/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actCouponUser/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActCouponUserQO qo);

    @RequestMapping(value = "/marketing/actCouponUser/get/{id}", method = RequestMethod.GET)
    ActCouponUserVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/actCouponUser/updateByIsUse", method = RequestMethod.PUT)
    int updateByIsUse(@RequestBody ActCouponUserQO actCouponUserQO);
}
