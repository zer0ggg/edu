package com.roncoo.education.marketing.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.qo.ActOrderPayBO;
import com.roncoo.education.marketing.feign.qo.ActQO;
import com.roncoo.education.marketing.feign.vo.ActOrderVO;
import com.roncoo.education.marketing.feign.vo.ActVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 活动信息表 接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@FeignClient(value = "service-marketing")
public interface IFeignAct {

    @RequestMapping(value = "/marketing/act/listForPage", method = RequestMethod.POST)
    Page<ActVO> listForPage(@RequestBody ActQO qo);

    @RequestMapping(value = "/marketing/act/save", method = RequestMethod.POST)
    int save(@RequestBody ActQO qo);

    @RequestMapping(value = "/marketing/act/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/marketing/act/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ActQO qo);

    @RequestMapping(value = "/marketing/act/get/{id}", method = RequestMethod.GET)
    ActVO getById(@PathVariable(value = "id") Long id);

    /**
     * 活动下单
     *
     * @param actOrderPayBO
     * @return
     */
    @RequestMapping(value = "/marketing/act/order", method = RequestMethod.POST)
    ActOrderVO order(@RequestBody ActOrderPayBO actOrderPayBO);
}
