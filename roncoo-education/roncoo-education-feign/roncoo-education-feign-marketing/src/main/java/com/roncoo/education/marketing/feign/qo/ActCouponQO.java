package com.roncoo.education.marketing.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 优惠券活动
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActCouponQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 发放卷总量
     */
    private Integer total;

    /**
     * 已使用数量
     */
    private Integer usedNum;

    /**
     * 领取数量
     */
    private Integer receiveNum;

    /**
     * 领取限制（0不限制，其他代表张数）
     */
    private Integer limitReceive;

    /**
     * 课程Id
     */
    private Long courseId;

    /**
     * 优惠价格
     */
    private BigDecimal couponPrice;

    /**
     * 活动Id
     */
    private Long actId;

}
