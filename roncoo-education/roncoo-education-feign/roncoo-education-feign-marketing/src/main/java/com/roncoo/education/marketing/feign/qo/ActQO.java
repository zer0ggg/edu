package com.roncoo.education.marketing.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 活动信息表
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
public class ActQO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private int pageCurrent;
    /**
     * 每页记录数
     */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Data gmtCreate;

    /**
     * 修改时间
     */
    private Data gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;

    /**
     * 活动标题
     */
    private String actTitle;

    /**
     * 活动图片
     */
    private String actImg;

    /**
     * 活动开始时间
     */
    private Data beginTime;

    /**
     * 活动结束时间
     */
    private Data endTime;
    /**
     * 产品ID
     */
    private Long productId;
    /**
     * 用户ID
     */
    private Long userNo;

    /**
     * 产品类型：(1普通,2直播,3会员,4文库,5试卷)
     */
    private Integer productType;

}
