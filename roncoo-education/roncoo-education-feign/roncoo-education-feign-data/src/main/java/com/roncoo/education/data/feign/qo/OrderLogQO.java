package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 订单日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class OrderLogQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 讲师名称
     */
    private String lecturerName;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 用户电话
     */
    private String mobile;

    /**
     * 订单号
     */
    private Long orderNo;
    /**
     * 产品ID
     */
    private Long productId;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)
     */
    private Integer productType;
    /**
     * 附加数据
     */
    private String attach;
    /**
     * 应付金额
     */
    private BigDecimal pricePayable;
    /**
     * 优惠金额
     */
    private BigDecimal priceDiscount;
    /**
     * 实付金额
     */
    private BigDecimal pricePaid;
    /**
     * 平台收入
     */
    private BigDecimal platformIncome;

    /**
     * 讲师收入
     */
    private BigDecimal lecturerIncome;

    /**
     * 代理收入
     */
    private BigDecimal agentIncome;

    /**
     * 交易类型：1线上支付，2线下支付
     */
    private Integer tradeType;

    /**
     * 支付方式：1微信支付，2支付宝支付
     */
    private Integer payType;

    /**
     * 购买渠道：（1: PC端；2: APP端；3:微信端； 4:手工绑定）
     */
    private Integer channelType;

    /**
     * 订单状态：1待支付，2成功支付，3支付失败，4已关闭
     */
    private Integer orderStatus;
    /**
     * 支付时间
     */
    private Date payTime;
    /**
     * 是否显示给讲师(1是，0否)
     */
    private Integer isShowLecturer;

    /**
     * 是否显示给用户看(1是，0否)
     */
    private Integer isShowUser;
    /**
     * 活动ID（如果是优惠券就是优惠券ID，如果是秒杀该字段为秒杀ID）
     */
    private Long actTypeId;
    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;

}
