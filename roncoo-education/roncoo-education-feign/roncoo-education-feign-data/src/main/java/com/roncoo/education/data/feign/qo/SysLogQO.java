package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 后台操作日志表
 *
 * @author wujing
 * @date 2020-06-09
 */
@Data
@Accessors(chain = true)
public class SysLogQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 操作人
     */
    private String loginName;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * IP地址
     */
    private String loginIp;

    /**
     * 用户操作
     */
    private String operation;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 请求路径
     */
    private String path;

    /**
     * 内容
     */
    private String content;

}
