//package com.roncoo.education.data.feign.config;
//
//import com.roncoo.education.common.core.base.BaseException;
//import com.roncoo.education.common.core.tools.JSUtil;
//import feign.Response;
//import feign.Util;
//import feign.codec.ErrorDecoder;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Configuration;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.util.HashMap;
//
//@Slf4j
//@Configuration
//public class FeignErrorDecoder implements ErrorDecoder {
//
//    @Override
//    public Exception decode(String configKey, Response response) {
//        String message = "系统异常";
//        if (null != response.body()) {
//            String result = "";
//            try {
//                result = Util.toString(response.body().asReader(Charset.defaultCharset()));
//                message = JSUtil.parseObject(result, HashMap.class).get("message").toString();
//            } catch (IOException e) {
//                log.error(result, e);
//            }
//        }
//        return new BaseException(message);
//    }
//}
