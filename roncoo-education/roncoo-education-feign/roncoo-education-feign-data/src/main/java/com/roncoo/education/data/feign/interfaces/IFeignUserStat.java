package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.UserStatQO;
import com.roncoo.education.data.feign.vo.UserStatVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户统计 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignUserStat {

    @RequestMapping(value = "/data/userStat/listForPage", method = RequestMethod.POST)
    Page<UserStatVO> listForPage(@RequestBody UserStatQO qo);

    @RequestMapping(value = "/data/userStat/save", method = RequestMethod.POST)
    int save(@RequestBody UserStatQO qo);

    @RequestMapping(value = "/data/userStat/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/userStat/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserStatQO qo);

    @RequestMapping(value = "/data/userStat/get/{id}", method = RequestMethod.GET)
    UserStatVO getById(@PathVariable(value = "id") Long id);

}
