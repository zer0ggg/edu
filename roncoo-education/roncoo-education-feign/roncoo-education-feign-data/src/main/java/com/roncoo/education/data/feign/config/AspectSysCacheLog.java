package com.roncoo.education.data.feign.config;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysCacheLog;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.ObjectMapUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 系统日志，切面处理类
 */
@Aspect
@Component
public class AspectSysCacheLog extends BaseBiz {

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Pointcut("@annotation(com.roncoo.education.common.core.base.SysCacheLog)")
    public void cacheLogPointCut() {

    }

    @AfterReturning(value = "cacheLogPointCut()", returning = "obj")
    public void doAfterReturning(JoinPoint joinPoint, Object obj) {
        Result<Object> result = (Result) obj;
        if (result.getCode().equals(ResultEnum.SUCCESS.getCode())) {
            Map<String, Object> map = ObjectMapUtil.Obj2Map(result.getData());
            if (map.get("id") != null) {
                MethodSignature signature = (MethodSignature) joinPoint.getSignature();
                SysCacheLog sysCachelog = signature.getMethod().getAnnotation(SysCacheLog.class);
                HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
                myRedisTemplate.setByJson(sysCachelog.key() + request.getHeader(Constants.TK.CURRENT_LOGIN_NAME) + map.get("id").toString(), map, Constants.Session.TIME_OUT, TimeUnit.MINUTES);
            }
        }
    }

}
