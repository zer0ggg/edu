package com.roncoo.education.data.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class UserStatVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDate gmtCreate;

    /**
     * 当天新增用户
     */
    private String newUserNum;

    /**
     * 当天登录人次
     */
    private String loginNum;

    /**
     * 当天登录人数(去重)
     */
    private String loginSum;
}
