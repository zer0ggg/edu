package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.OrderStatLecturerQO;
import com.roncoo.education.data.feign.vo.OrderStatLecturerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 订单讲师统计 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignOrderStatLecturer {

    @RequestMapping(value = "/data/orderStatLecturer/listForPage", method = RequestMethod.POST)
    Page<OrderStatLecturerVO> listForPage(@RequestBody OrderStatLecturerQO qo);

    @RequestMapping(value = "/data/orderStatLecturer/save", method = RequestMethod.POST)
    int save(@RequestBody OrderStatLecturerQO qo);

    @RequestMapping(value = "/data/orderStatLecturer/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/orderStatLecturer/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderStatLecturerQO qo);

    @RequestMapping(value = "/data/orderStatLecturer/get/{id}", method = RequestMethod.GET)
    OrderStatLecturerVO getById(@PathVariable(value = "id") Long id);

}
