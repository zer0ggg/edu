package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.OrderStatVipQO;
import com.roncoo.education.data.feign.vo.OrderStatVipVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 订单会员统计 接口
 *
 * @author wujing
 * @date 2020-05-25
 */
@FeignClient(value = "service-data")
public interface IFeignOrderStatVip {

    @RequestMapping(value = "/data/orderStatVip/listForPage", method = RequestMethod.POST)
    Page<OrderStatVipVO> listForPage(@RequestBody OrderStatVipQO qo);

    @RequestMapping(value = "/data/orderStatVip/save", method = RequestMethod.POST)
    int save(@RequestBody OrderStatVipQO qo);

    @RequestMapping(value = "/data/orderStatVip/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/orderStatVip/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderStatVipQO qo);

    @RequestMapping(value = "/data/orderStatVip/get/{id}", method = RequestMethod.GET)
    OrderStatVipVO getById(@PathVariable(value = "id") Long id);

}
