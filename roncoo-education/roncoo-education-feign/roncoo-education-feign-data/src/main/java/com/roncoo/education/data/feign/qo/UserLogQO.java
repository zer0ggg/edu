package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class UserLogQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 用户注册时间
     */
    private Date registerTime;

    /**
     * 登录类型(1:pc；2:安卓；3苹果；4微信小程序)
     */
    private Integer loginType;

    /**
     * 登录IP
     */
    private String loginIp;

    /**
     * 国家
     */
    private String country;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

}
