package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 课程日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class CourseLogQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 课程类型
     */
    private Integer courseCategory;

    /**
     * 课时ID
     */
    private Long periodId;

    /**
     * 课时名称
     */
    private String periodName;

    /**
     * 章节ID
     */
    private Long chapterId;

    /**
     * 章节名称
     */
    private String chapterName;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 视频VID
     */
    private String vid;

    /**
     * 课时时长
     */
    private String vidLength;

    /**
     * 观看时长
     */
    private String watchLength;

    /**
     * 观看进度
     */
    private String watckProgress;

    /**
     * 人脸对比结果
     */
    private String contrastResult;

    /**
     * 学习时长(当次学习的时长)
     */
    private String duration;

    /**
     * 学习总时长(每次叠加)
     */
    private String totalDuration;

    /**
     * 剩余人脸对比次数
     */
    private Integer residueContrastTotal;
}
