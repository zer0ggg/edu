package com.roncoo.education.data.feign.config;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.IPUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.ObjectMapUtil;
import com.roncoo.education.data.feign.interfaces.IFeignSysLog;
import com.roncoo.education.data.feign.qo.SysLogQO;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统日志，切面处理类
 */
@Aspect
@Component
public class AspectSysLog extends BaseBiz {

    /**
     * 记录最大程度
     */
    private final static int MAX_LENGTH = 5000;

    @Autowired
    private IFeignSysLog feignSysLog;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Pointcut("@annotation(com.roncoo.education.common.core.base.SysLog)")
    public void logPointCut() {
    }

    @Before("logPointCut()")
    public void saveSysLog(JoinPoint joinPoint) {
        SysLogQO qo = getSysLogQO();
        // 记录日志，异步执行
        CALLBACK_EXECUTOR.execute(() -> {
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            SysLog syslog = signature.getMethod().getAnnotation(SysLog.class);
            qo.setOperation(syslog.value());
            qo.setContent(JSUtil.toJSONString(joinPoint.getArgs()));// 请求的参数
            if (StringUtils.hasText(qo.getContent())) {
                if (syslog.isUpdate()) {
                    // 如果是修改或者编辑，比对变化
                    Map<String, Object> map1 = JSUtil.parseArray(qo.getContent(), HashMap.class).get(0); // 修改后的值
                    if (myRedisTemplate.hasKey(syslog.key() + qo.getLoginName() + map1.get("id").toString())) {
                        Map<String, Object> map2 = myRedisTemplate.getForJson(syslog.key() + qo.getLoginName() + map1.get("id").toString(), HashMap.class); // 修改前的值
                        myRedisTemplate.delete(syslog.key() + qo.getLoginName() + map1.get("id").toString());// 及时删除缓存
                        qo.setContent(ObjectMapUtil.contrast(map1, map2)); //进行对比
                    }
                }
                if (qo.getContent().length() > MAX_LENGTH) {
                    logger.warn("操作日志：{}", JSUtil.toJSONString(qo));
                    qo.setContent(qo.getContent().substring(0, MAX_LENGTH));
                }
            }
            feignSysLog.save(qo);
        });
    }

    private SysLogQO getSysLogQO() {
        SysLogQO qo = new SysLogQO();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        qo.setLoginName(request.getHeader(Constants.TK.CURRENT_LOGIN_NAME));
        qo.setLoginIp(IPUtil.getIpAddr(request));
        qo.setPath(request.getServletPath());
        qo.setMethod(request.getMethod());
        return qo;
    }

}
