package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.SysLogQO;
import com.roncoo.education.data.feign.vo.SysLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 后台操作日志表 接口
 *
 * @author wujing
 * @date 2020-06-09
 */
@FeignClient(value = "service-data")
public interface IFeignSysLog {

    @RequestMapping(value = "/data/sysLog/listForPage", method = RequestMethod.POST)
    Page<SysLogVO> listForPage(@RequestBody SysLogQO qo);

    @RequestMapping(value = "/data/sysLog/save", method = RequestMethod.POST)
    int save(@RequestBody SysLogQO qo);

    @RequestMapping(value = "/data/sysLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/sysLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysLogQO qo);

    @RequestMapping(value = "/data/sysLog/get/{id}", method = RequestMethod.GET)
    SysLogVO getById(@PathVariable(value = "id") Long id);

}
