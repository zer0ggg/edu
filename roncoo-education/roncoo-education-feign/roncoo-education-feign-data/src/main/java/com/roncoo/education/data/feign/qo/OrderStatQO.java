package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 订单统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class OrderStatQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     *
     */
    private Long id;

    /**
     *
     */
    private LocalDate gmtCreate;

    /**
     * 当天订单数
     */
    private Integer orderNum;

    /**
     * 当天收入
     */
    private BigDecimal orderIncome;

    /**
     * 当天利润
     */
    private BigDecimal orderProfit;

    /**
     * 当天购买用户
     */
    private Integer buyUser;

    /**
     * 当天购买新用户
     */
    private Integer buyNewUser;

}
