package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.CourseStatUserQO;
import com.roncoo.education.data.feign.vo.CourseStatUserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程用户统计 接口
 *
 * @author wujing
 * @date 2020-06-29
 */
@FeignClient(value = "service-data")
public interface IFeignCourseStatUser {

    @RequestMapping(value = "/data/courseStatUser/listForPage", method = RequestMethod.POST)
    Page<CourseStatUserVO> listForPage(@RequestBody CourseStatUserQO qo);

    @RequestMapping(value = "/data/courseStatUser/save", method = RequestMethod.POST)
    int save(@RequestBody CourseStatUserQO qo);

    @RequestMapping(value = "/data/courseStatUser/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/courseStatUser/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseStatUserQO qo);

    @RequestMapping(value = "/data/courseStatUser/get/{id}", method = RequestMethod.GET)
    CourseStatUserVO getById(@PathVariable(value = "id") Long id);

}
