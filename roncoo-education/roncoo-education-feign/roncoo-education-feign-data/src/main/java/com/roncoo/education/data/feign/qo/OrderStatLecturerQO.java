package com.roncoo.education.data.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 订单讲师统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class OrderStatLecturerQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDate gmtCreate;

    /**
     * 讲师用户编号
     */
    private Long lecturerUserNo;

    /**
     * 讲师名称
     */
    private String lecturerName;

    /**
     * 讲师日订单数
     */
    private Integer lecturerOrder;

    /**
     * 讲师日收入
     */
    private BigDecimal lecturerIncome;

}
