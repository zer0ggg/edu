package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.OrderStatCourserQO;
import com.roncoo.education.data.feign.vo.OrderStatCourserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 订单课程统计 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignOrderStatCourser {

    @RequestMapping(value = "/data/orderStatCourser/listForPage", method = RequestMethod.POST)
    Page<OrderStatCourserVO> listForPage(@RequestBody OrderStatCourserQO qo);

    @RequestMapping(value = "/data/orderStatCourser/save", method = RequestMethod.POST)
    int save(@RequestBody OrderStatCourserQO qo);

    @RequestMapping(value = "/data/orderStatCourser/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/orderStatCourser/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderStatCourserQO qo);

    @RequestMapping(value = "/data/orderStatCourser/get/{id}", method = RequestMethod.GET)
    OrderStatCourserVO getById(@PathVariable(value = "id") Long id);

}
