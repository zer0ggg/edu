package com.roncoo.education.data.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 课程用户统计
 *
 * @author wujing
 * @date 2020-06-29
 */
@Data
@Accessors(chain = true)
public class CourseStatUserVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDate gmtCreate;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 观看时长（秒）
     */
    private String watchLength;

    /**
     * 观看课程数(去重)
     */
    private Integer watchCourseSums;
}
