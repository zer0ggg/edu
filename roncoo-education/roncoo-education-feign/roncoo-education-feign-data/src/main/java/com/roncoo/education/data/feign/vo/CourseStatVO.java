package com.roncoo.education.data.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 课程统计
 *
 * @author wujing
 * @date 2020-06-29
 */
@Data
@Accessors(chain = true)
public class CourseStatVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 日期
     */
    private LocalDate gmtCreate;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 观看人次
     */
    private Integer views;

    /**
     * 观看人数(去重)
     */
    private Integer sums;

    /**
     * 课程总时长
     */
    private String courseLength;

    /**
     * 学习时长(秒)
     */
    private String studyLength;
}
