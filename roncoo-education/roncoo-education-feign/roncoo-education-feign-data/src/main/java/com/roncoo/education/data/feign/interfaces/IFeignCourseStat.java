package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.CourseStatQO;
import com.roncoo.education.data.feign.vo.CourseStatVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 课程统计 接口
 *
 * @author wujing
 * @date 2020-06-29
 */
@FeignClient(value = "service-data")
public interface IFeignCourseStat {

    @RequestMapping(value = "/data/courseStat/listForPage", method = RequestMethod.POST)
    Page<CourseStatVO> listForPage(@RequestBody CourseStatQO qo);

    @RequestMapping(value = "/data/courseStat/save", method = RequestMethod.POST)
    int save(@RequestBody CourseStatQO qo);

    @RequestMapping(value = "/data/courseStat/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/courseStat/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseStatQO qo);

    @RequestMapping(value = "/data/courseStat/get/{id}", method = RequestMethod.GET)
    CourseStatVO getById(@PathVariable(value = "id") Long id);

}
