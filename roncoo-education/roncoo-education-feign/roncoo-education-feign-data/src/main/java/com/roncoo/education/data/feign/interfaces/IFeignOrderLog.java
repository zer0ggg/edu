package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.OrderLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 订单日志 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignOrderLog {

    @RequestMapping(value = "/orderLog/listForPage", method = RequestMethod.POST)
    Page<OrderLogVO> listForPage(@RequestBody OrderLogQO qo);

    @RequestMapping(value = "/orderLog/save", method = RequestMethod.POST)
    int save(@RequestBody OrderLogQO qo);

    @RequestMapping(value = "/orderLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/orderLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderLogQO qo);

    @RequestMapping(value = "/orderLog/get/{id}", method = RequestMethod.GET)
    OrderLogVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据订单号获取订单日志信息
     *
     * @param orderNo
     * @return
     */
    @RequestMapping(value = "/orderLog/getByOrderNo/{orderNo}", method = RequestMethod.GET)
    OrderLogVO getByOrderNo(@PathVariable(value = "orderNo") Long orderNo);

    /**
     * 根据订单号更新订单日志
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/orderLog/updateByOrderNo", method = RequestMethod.PUT)
    int updateByOrderNo(@RequestBody OrderLogQO qo);

    /**
     * 根据用户编号更新手机号
     *
     * @param orderLogQO
     * @return
     */
    @RequestMapping(value = "/orderLog/updateByMobile", method = RequestMethod.PUT)
    int updateByMobile(@RequestBody OrderLogQO orderLogQO);

}
