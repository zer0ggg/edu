package com.roncoo.education.data.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 订单课程统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class OrderStatCourserVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDate gmtCreate;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 课程分类(1:普通课程;2:直播课程,3:题库,4文库，5:试卷)
     */
    private Integer courseCategory;

    /**
     * 课程数量
     */
    private Integer courseNum;

    /**
     * 课程收入
     */
    private BigDecimal courseIncome;

    /**
     * 课程利润
     */
    private BigDecimal courseProfit;
}
