package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.OrderStatQO;
import com.roncoo.education.data.feign.vo.OrderStatVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 订单统计 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignOrderStat {

    @RequestMapping(value = "/data/orderStat/listForPage", method = RequestMethod.POST)
    Page<OrderStatVO> listForPage(@RequestBody OrderStatQO qo);

    @RequestMapping(value = "/data/orderStat/save", method = RequestMethod.POST)
    int save(@RequestBody OrderStatQO qo);

    @RequestMapping(value = "/data/orderStat/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/orderStat/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderStatQO qo);

    @RequestMapping(value = "/data/orderStat/get/{id}", method = RequestMethod.GET)
    OrderStatVO getById(@PathVariable(value = "id") Long id);

}
