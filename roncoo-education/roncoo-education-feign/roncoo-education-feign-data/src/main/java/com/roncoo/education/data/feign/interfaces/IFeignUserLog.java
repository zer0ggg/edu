package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.UserLogQO;
import com.roncoo.education.data.feign.vo.UserLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户日志 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignUserLog {

    @RequestMapping(value = "/userLog/listForPage", method = RequestMethod.POST)
    Page<UserLogVO> listForPage(@RequestBody UserLogQO qo);

    @RequestMapping(value = "/userLog/save", method = RequestMethod.POST)
    int save(@RequestBody UserLogQO qo);

    @RequestMapping(value = "/userLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserLogQO qo);

    @RequestMapping(value = "/userLog/get/{id}", method = RequestMethod.GET)
    UserLogVO getById(@PathVariable(value = "id") Long id);

}
