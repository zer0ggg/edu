package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 课程日志 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignCourseLog {

    @RequestMapping(value = "/data/courseLog/listForPage", method = RequestMethod.POST)
    Page<CourseLogVO> listForPage(@RequestBody CourseLogQO qo);

    @RequestMapping(value = "/data/courseLog/save", method = RequestMethod.POST)
    int save(@RequestBody CourseLogQO qo);

    @RequestMapping(value = "/data/courseLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/courseLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody CourseLogQO qo);

    @RequestMapping(value = "/data/courseLog/get/{id}", method = RequestMethod.GET)
    CourseLogVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/courseLog/getByUserNoAndPeriodId", method = RequestMethod.POST)
    CourseLogVO getByUserNoAndPeriodId(@RequestBody CourseLogQO courseLogQO);

    /**
     * 更新时长
     *
     * @param courseLogQO
     * @return
     */
    @RequestMapping(value = "/data/courseLog/updateByCourseId", method = RequestMethod.PUT)
    int updateByCourseId(@RequestBody CourseLogQO courseLogQO);

    /**
     * 根据用户编号、课程ID获取学习记录
     *
     * @param record
     * @return
     */
    @RequestMapping(value = "/data/courseLog/listByUserNoAndCourseId", method = RequestMethod.POST)
    List<CourseLogVO> listByUserNoAndCourseId(@RequestBody CourseLogQO record);

    /**
     * 根据用户编号、课时id获取最新的学习记录
     *
     * @param courseLogQO
     * @return
     */
    @RequestMapping(value = "/data/courseLog/getByUserNoAndPeriodIdLatest", method = RequestMethod.POST)
    CourseLogVO getByUserNoAndPeriodIdLatest(@RequestBody CourseLogQO courseLogQO);

}
