package com.roncoo.education.data.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * 订单会员统计
 *
 * @author wujing
 * @date 2020-05-25
 */
@Data
@Accessors(chain = true)
public class OrderStatVipVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDate gmtCreate;

    /**
     * 订单数量
     */
    private Integer orderNum;

    /**
     * 当天收入
     */
    private BigDecimal orderIncome;
}
