package com.roncoo.education.data.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.qo.UserStatProvinceQO;
import com.roncoo.education.data.feign.vo.UserStatProvinceVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户省统计 接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@FeignClient(value = "service-data")
public interface IFeignUserStatProvince {

    @RequestMapping(value = "/data/userStatProvince/listForPage", method = RequestMethod.POST)
    Page<UserStatProvinceVO> listForPage(@RequestBody UserStatProvinceQO qo);

    @RequestMapping(value = "/data/userStatProvince/save", method = RequestMethod.POST)
    int save(@RequestBody UserStatProvinceQO qo);

    @RequestMapping(value = "/data/userStatProvince/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/data/userStatProvince/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserStatProvinceQO qo);

    @RequestMapping(value = "/data/userStatProvince/get/{id}", method = RequestMethod.GET)
    UserStatProvinceVO getById(@PathVariable(value = "id") Long id);

}
