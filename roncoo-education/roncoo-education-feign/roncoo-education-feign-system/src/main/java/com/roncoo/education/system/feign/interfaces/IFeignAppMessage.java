package com.roncoo.education.system.feign.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.AppMessageQO;
import com.roncoo.education.system.feign.vo.AppMessageVO;

/**
 * app推送 接口
 *
 * @author wujing
 * @date 2020-11-17
 */
@FeignClient(value = "service-system")
public interface IFeignAppMessage{

    @RequestMapping(value = "/system/appMessage/listForPage", method = RequestMethod.POST)
    Page<AppMessageVO> listForPage(@RequestBody AppMessageQO qo);

    @RequestMapping(value = "/system/appMessage/save", method = RequestMethod.POST)
    int save(@RequestBody AppMessageQO qo);

    @RequestMapping(value = "/system/appMessage/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/appMessage/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody AppMessageQO qo);

    @RequestMapping(value = "/system/appMessage/get/{id}", method = RequestMethod.GET)
    AppMessageVO getById(@PathVariable(value = "id") Long id);

}
