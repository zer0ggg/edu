package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.WebsiteNavArticleQO;
import com.roncoo.education.system.feign.vo.WebsiteNavArticleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 站点导航文章
 *
 * @author wuyun
 */
@FeignClient(value = "service-system")
public interface IFeignWebsiteNavArticle {

    @RequestMapping(value = "/websiteNavArticle/listForPage", method = RequestMethod.POST)
    Page<WebsiteNavArticleVO> listForPage(@RequestBody WebsiteNavArticleQO qo);

    @RequestMapping(value = "/websiteNavArticle/save", method = RequestMethod.POST)
    int save(@RequestBody WebsiteNavArticleQO qo);

    @RequestMapping(value = "/websiteNavArticle/deleteById", method = RequestMethod.DELETE)
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/websiteNavArticle/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody WebsiteNavArticleQO qo);

    @RequestMapping(value = "/websiteNavArticle/getById", method = RequestMethod.GET)
    WebsiteNavArticleVO getById(@RequestBody Long id);

    @RequestMapping(value = "/websiteNavArticle/getByNavId/{navId}", method = RequestMethod.GET)
    WebsiteNavArticleVO getByNavId(@PathVariable(value = "navId") Long navId);
}
