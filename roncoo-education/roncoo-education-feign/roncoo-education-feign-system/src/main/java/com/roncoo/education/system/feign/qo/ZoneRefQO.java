package com.roncoo.education.system.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 专区关联
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
public class ZoneRefQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 专区ID
     */
    private Long zoneId;

    /**
     * 位置(1电脑端，2微信端)
     */
    private Integer zoneLocation;

    /**
     * 关联ID
     */
    private Long courseId;

    /**
     * 标题
     */
    private String title;

    /**
     * 外部链接
     */
    private String url;

    /**
     * 图片
     */
    private String img;

}
