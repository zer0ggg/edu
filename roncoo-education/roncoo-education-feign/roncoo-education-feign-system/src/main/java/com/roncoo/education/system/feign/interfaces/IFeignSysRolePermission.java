package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysRolePermissionQO;
import com.roncoo.education.system.feign.vo.SysRolePermissionVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统角色权限 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysRolePermission {

    @RequestMapping(value = "/system/sysRolePermission/listForPage", method = RequestMethod.POST)
    Page<SysRolePermissionVO> listForPage(@RequestBody SysRolePermissionQO qo);

    @RequestMapping(value = "/system/sysRolePermission/save", method = RequestMethod.POST)
    int save(@RequestBody SysRolePermissionQO qo);

    @RequestMapping(value = "/system/sysRolePermission/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysRolePermission/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysRolePermissionQO qo);

    @RequestMapping(value = "/system/sysRolePermission/get/{id}", method = RequestMethod.GET)
    SysRolePermissionVO getById(@PathVariable(value = "id") Long id);

}
