package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.AdvQO;
import com.roncoo.education.system.feign.vo.AdvVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 广告信息
 *
 * @author wujing
 */
@FeignClient(value = "service-system")
public interface IFeignAdv {

    @RequestMapping(value = "/adv/listForPage", method = RequestMethod.POST)
    Page<AdvVO> listForPage(@RequestBody AdvQO qo);

    @RequestMapping(value = "/adv/save", method = RequestMethod.POST)
    int save(@RequestBody AdvQO qo);

    @RequestMapping(value = "/adv/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/adv/update", method = RequestMethod.PUT)
    int updateById(@RequestBody AdvQO qo);

    @RequestMapping(value = "/adv/get/{id}", method = RequestMethod.GET)
    AdvVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/adv/updateStautsId", method = RequestMethod.POST)
    int updateStautsId(@RequestBody AdvQO qo);
}
