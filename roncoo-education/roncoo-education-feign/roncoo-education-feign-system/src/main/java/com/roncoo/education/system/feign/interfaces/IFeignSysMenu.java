package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysMenuQO;
import com.roncoo.education.system.feign.vo.SysMenuVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统菜单 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysMenu {

    @RequestMapping(value = "/system/sysMenu/listForPage", method = RequestMethod.POST)
    Page<SysMenuVO> listForPage(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/system/sysMenu/save", method = RequestMethod.POST)
    int save(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/system/sysMenu/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysMenu/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/system/sysMenu/get/{id}", method = RequestMethod.GET)
    SysMenuVO getById(@PathVariable(value = "id") Long id);

}
