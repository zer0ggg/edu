package com.roncoo.education.system.feign.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 欢拓配置
 *
 * @author LYQ
 */
@Data
public class ConfigTalkFunVO implements Serializable {

    private static final long serialVersionUID = 1133747757275697037L;

    /**
     * 欢拓OpenId
     */
    private String talkFunOpenId;

    /**
     * 欢拓OpenToken
     */
    private String talkFunOpenToken;
}
