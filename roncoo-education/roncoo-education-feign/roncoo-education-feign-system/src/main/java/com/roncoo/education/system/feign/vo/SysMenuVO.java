package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统菜单
 *
 * @author wujing
 * @date 2020-04-29
 */
@Data
@Accessors(chain = true)
public class SysMenuVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单类型(0:目录;1菜单)
     */
    private Integer menuType;

    /**
     * 路由地址
     */
    private String routerUrl;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 是否外链(1:是 ;0:否)
     */
    private Integer isFrame;

    /**
     * 允许删除(1:允许;0:不允许)
     */
    private Integer allowDelete;
}
