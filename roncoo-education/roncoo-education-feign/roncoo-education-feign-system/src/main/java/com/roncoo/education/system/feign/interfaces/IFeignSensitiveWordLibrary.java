package com.roncoo.education.system.feign.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 敏感词库
 *
 * @author wujing
 */
@FeignClient(value = "service-system")
public interface IFeignSensitiveWordLibrary {

    @RequestMapping(value = "sensitive/word/library/list/all", method = RequestMethod.POST)
    List<String> listAll();
}
