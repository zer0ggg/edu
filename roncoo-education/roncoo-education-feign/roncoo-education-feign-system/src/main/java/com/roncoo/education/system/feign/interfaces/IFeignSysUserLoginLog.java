package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysUserLoginLogQO;
import com.roncoo.education.system.feign.vo.SysUserLoginLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统用户登录日志 接口
 *
 * @author LYQ
 * @date 2020-05-14
 */
@FeignClient(value = "service-system")
public interface IFeignSysUserLoginLog {

    @RequestMapping(value = "/system/sysUserLoginLog/listForPage", method = RequestMethod.POST)
    Page<SysUserLoginLogVO> listForPage(@RequestBody SysUserLoginLogQO qo);

    @RequestMapping(value = "/system/sysUserLoginLog/save", method = RequestMethod.POST)
    int save(@RequestBody SysUserLoginLogQO qo);

    @RequestMapping(value = "/system/sysUserLoginLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysUserLoginLog/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysUserLoginLogQO qo);

    @RequestMapping(value = "/system/sysUserLoginLog/get/{id}", method = RequestMethod.GET)
    SysUserLoginLogVO getById(@PathVariable(value = "id") Long id);

}
