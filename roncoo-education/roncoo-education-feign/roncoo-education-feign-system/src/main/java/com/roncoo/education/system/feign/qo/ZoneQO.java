package com.roncoo.education.system.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 专区
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
public class ZoneQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 名称
     */
    private String zoneName;

    /**
     * 专区分类(1:普通课程;2:直播课程,3文库，4:试卷，5:博客;6:资讯，7:自定义)
     */
    private Integer zoneCategory;

    /**
     * 描述
     */
    private String zoneDesc;

    /**
     * 位置(1电脑端，2微信端)
     */
    private Integer zoneLocation;

    /**
     * 展示方式(1横向，2纵向；3轮播)
     */
    private Integer showType;

}
