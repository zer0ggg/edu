package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统配置表
 *
 */
@Data
@Accessors(chain = true)
public class ConfigTencentVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer isFaceContras;
	private String tencentSecretKey;
	private String tencentSecretId;
	private String faceContrasBucketUrl;
	private String faceContrasBucketName;
	private String faceContrasPath;
}
