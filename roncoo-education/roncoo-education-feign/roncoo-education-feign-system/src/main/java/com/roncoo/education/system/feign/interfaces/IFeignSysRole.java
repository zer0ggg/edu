package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysRoleQO;
import com.roncoo.education.system.feign.vo.SysRoleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统角色 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysRole {

    @RequestMapping(value = "/system/sysRole/listForPage", method = RequestMethod.POST)
    Page<SysRoleVO> listForPage(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/system/sysRole/save", method = RequestMethod.POST)
    int save(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/system/sysRole/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysRole/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysRoleQO qo);

    @RequestMapping(value = "/system/sysRole/get/{id}", method = RequestMethod.GET)
    SysRoleVO getById(@PathVariable(value = "id") Long id);

}
