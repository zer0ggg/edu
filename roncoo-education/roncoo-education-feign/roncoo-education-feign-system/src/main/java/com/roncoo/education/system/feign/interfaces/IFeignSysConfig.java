package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysConfigQO;
import com.roncoo.education.system.feign.vo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 参数配置 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysConfig {

    @RequestMapping(value = "/system/sysConfig/listForPage", method = RequestMethod.POST)
    Page<SysConfigVO> listForPage(@RequestBody SysConfigQO qo);

    @RequestMapping(value = "/system/sysConfig/save", method = RequestMethod.POST)
    int save(@RequestBody SysConfigQO qo);

    @RequestMapping(value = "/system/sysConfig/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysConfig/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysConfigQO qo);

    @RequestMapping(value = "/system/sysConfig/get/{id}", method = RequestMethod.GET)
    SysConfigVO getById(@PathVariable(value = "id") Long id);

    /**
     * 从配置表获取站点信息
     *
     * @return 站点信息
     */
    @RequestMapping(value = "/system/sysConfig/getWebsite", method = RequestMethod.GET)
    ConfigWebsiteVO getWebsite();

    /**
     * 从配置表获得系统信息
     *
     * @return 系统信息
     */
    @RequestMapping(value = "/system/sysConfig/getSys", method = RequestMethod.POST)
    ConfigSysVO getSys();

    /**
     * 获取直播类型
     *
     * @return 直播类型
     */
    @GetMapping(value = "/system/sysConfig/live/platform")
    ConfigLivePlatformVO getLivePlatform();

    /**
     * 获取欢拓直播配置信息
     *
     * @return 欢拓直播配置信息
     */
    @GetMapping(value = "/system/sysConfig/talk/fun")
    ConfigTalkFunVO getTalkFun();

    /**
     * 从配置表获得保利威信息
     *
     * @return 保利威信息
     */
    @RequestMapping(value = "/system/sysConfig/getPolyv", method = RequestMethod.POST)
    ConfigPolyvVO getPolyv();

    /**
     * 从配置表获取阿里云信息
     *
     * @author Quanf
     */
    @RequestMapping(value = "/system/sysConfig/getAliyun", method = RequestMethod.GET)
    ConfigAliyunVO getAliyun();

    /**
     * 获取平台点播配置
     *
     * @return 平台点播配置
     */
    @GetMapping(value = "/system/sysConfig/getVod")
    ConfigVodVO getVod();

    /**
     * 获取阿里云点播配置
     *
     * @return 阿里云点播配置
     */
    @GetMapping(value = "/system/sysConfig/getAliYunVod")
    ConfigAliYunVodVO getAliYunVod();

    /**
     * 从配置表获得微信信息
     *
     * @return 微信配置
     */
    @RequestMapping(value = "/system/sysConfig/getWeixin", method = RequestMethod.POST)
    ConfigWeixinVO getWeixin();

    /**
     * 根据key值获取配置信息
     *
     * @param configKey 配置Key
     * @return 系统配置信息
     */
    @RequestMapping(value = "/system/sysConfig/getByConfigKey/{configKey}", method = RequestMethod.GET)
    SysConfigVO getByConfigKey(@PathVariable(value = "configKey") String configKey);

    /**
     * 从配置表获得腾讯信息
     *
     * @return 腾讯信息
     */
    @RequestMapping(value = "/system/sysConfig/getTencent", method = RequestMethod.POST)
    ConfigTencentVO getTencent();
}
