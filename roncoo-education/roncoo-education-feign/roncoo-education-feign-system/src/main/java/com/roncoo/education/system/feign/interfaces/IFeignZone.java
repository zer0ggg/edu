package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.ZoneQO;
import com.roncoo.education.system.feign.vo.ZoneVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 专区 接口
 *
 * @author wujing
 * @date 2020-05-23
 */
@FeignClient(value = "service-system")
public interface IFeignZone {

    @RequestMapping(value = "/system/zone/listForPage", method = RequestMethod.POST)
    Page<ZoneVO> listForPage(@RequestBody ZoneQO qo);

    @RequestMapping(value = "/system/zone/save", method = RequestMethod.POST)
    int save(@RequestBody ZoneQO qo);

    @RequestMapping(value = "/system/zone/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/zone/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ZoneQO qo);

    @RequestMapping(value = "/system/zone/get/{id}", method = RequestMethod.GET)
    ZoneVO getById(@PathVariable(value = "id") Long id);

}
