package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 系统配置表
 *
 * @author YZJ
 */
@Data
@Accessors(chain = true)
public class ConfigSysVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String domain;

    /**
     * writetoken
     */
    private String videoType;
    /**
     * 文件存储类型（1阿里云）
     */
    private Integer fileStorageType;
    /**
     * 讲师默认分成
     */
    private BigDecimal lecturerDefaultProportion;
    /**
     * 讲师默认最大分成
     */
    private BigDecimal lecturerMaxProportion;
    /**
     * 讲师代理默认分成
     */
    private BigDecimal agentDefaultProportion;
    /**
     * 讲师最低提现金额
     */
    private BigDecimal extractMinMoney;

}
