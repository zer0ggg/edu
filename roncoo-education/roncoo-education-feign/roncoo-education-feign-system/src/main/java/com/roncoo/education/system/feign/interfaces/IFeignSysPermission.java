package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysPermissionQO;
import com.roncoo.education.system.feign.vo.SysPermissionVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统权限 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysPermission {

    @RequestMapping(value = "/system/sysPermission/listForPage", method = RequestMethod.POST)
    Page<SysPermissionVO> listForPage(@RequestBody SysPermissionQO qo);

    @RequestMapping(value = "/system/sysPermission/save", method = RequestMethod.POST)
    int save(@RequestBody SysPermissionQO qo);

    @RequestMapping(value = "/system/sysPermission/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysPermission/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysPermissionQO qo);

    @RequestMapping(value = "/system/sysPermission/get/{id}", method = RequestMethod.GET)
    SysPermissionVO getById(@PathVariable(value = "id") Long id);

}
