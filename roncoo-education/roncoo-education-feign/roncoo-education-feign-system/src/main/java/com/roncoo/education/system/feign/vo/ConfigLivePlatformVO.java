package com.roncoo.education.system.feign.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 直播类型
 *
 * @author LYQ
 */
@Data
public class ConfigLivePlatformVO implements Serializable {

    private static final long serialVersionUID = -2467555748995103336L;

    /**
     * 直播平台
     */
    private Integer livePlatform;
}
