package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统用户登录日志
 *
 * @author LYQ
 * @date 2020-05-14
 */
@Data
@Accessors(chain = true)
public class SysUserLoginLogVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 备注
     */
    private String remark;

    /**
     * 登录账号
     */
    private String loginName;

    /**
     * 登录IP
     */
    private String loginIp;

    /**
     * 登录状态(1:成功、0:失败)
     */
    private Integer loginStatus;

    /**
     * 登录描述
     */
    private String loginDescription;
}
