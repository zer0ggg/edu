package com.roncoo.education.system.feign.vo;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * app推送
 *
 * @author wujing
 * @date 2020-11-17
 */
@Data
@Accessors(chain = true)
public class AppMessageVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 跳转分类(1:点播;2:直播,3文库,4试卷,;5:资讯)
     */
    private Integer jumpCategory;

    /**
     * 是否已发送(1是;0否)
     */
    private Integer isSend;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;
}
