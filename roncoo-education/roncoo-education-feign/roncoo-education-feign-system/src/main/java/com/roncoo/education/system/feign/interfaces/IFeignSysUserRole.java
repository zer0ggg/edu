package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysUserRoleQO;
import com.roncoo.education.system.feign.vo.SysUserRoleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统用户角色 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysUserRole {

    @RequestMapping(value = "/system/sysUserRole/listForPage", method = RequestMethod.POST)
    Page<SysUserRoleVO> listForPage(@RequestBody SysUserRoleQO qo);

    @RequestMapping(value = "/system/sysUserRole/save", method = RequestMethod.POST)
    int save(@RequestBody SysUserRoleQO qo);

    @RequestMapping(value = "/system/sysUserRole/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysUserRole/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysUserRoleQO qo);

    @RequestMapping(value = "/system/sysUserRole/get/{id}", method = RequestMethod.GET)
    SysUserRoleVO getById(@PathVariable(value = "id") Long id);

}
