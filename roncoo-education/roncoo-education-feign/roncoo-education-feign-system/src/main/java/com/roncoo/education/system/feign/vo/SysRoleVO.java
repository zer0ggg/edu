package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统角色
 *
 * @author wujing
 * @date 2020-04-29
 */
@Data
@Accessors(chain = true)
public class SysRoleVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态ID(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String roleValue;

    /**
     * 允许删除(1:允许;0:不允许)
     */
    private Integer allowDelete;
}
