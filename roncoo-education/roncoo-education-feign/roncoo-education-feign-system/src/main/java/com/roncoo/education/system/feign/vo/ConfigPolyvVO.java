package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统配置表
 *
 * @author YZJ
 */
@Data
@Accessors(chain = true)
public class ConfigPolyvVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String polyvUseid;
	private String polyvWritetoken;
	private String polyvReadtoken;
	private String polyvSecretkey;
	private String polyvAppID;
	private String polyvAppSecret;
	private String polyvLiveDomain;
	private String polyvLiveNotify;
}
