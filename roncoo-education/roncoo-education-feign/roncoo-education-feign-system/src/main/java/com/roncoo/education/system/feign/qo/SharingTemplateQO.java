package com.roncoo.education.system.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 分享图片模板
 *
 * @author wujing
 * @date 2020-07-16
 */
@Data
@Accessors(chain = true)
public class SharingTemplateQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1有效, 0无效)
     */
    private Integer statusId;

    /**
     * 模板URL
     */
    private String templateUrl;

    /**
     * 模板类型（1用户推荐；2：课程分享）
     */
    private Integer templateType;

    /**
     * 是否使用模板（1:是；0:否）
     */
    private Integer isUseTemplate;

    /**
     * x位置
     */
    private Integer picx;

    /**
     * y位置
     */
    private Integer picy;

}
