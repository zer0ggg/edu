package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SharingTemplateQO;
import com.roncoo.education.system.feign.vo.SharingTemplateVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 分享图片模板 接口
 *
 * @author wujing
 * @date 2020-07-16
 */
@FeignClient(value = "service-system")
public interface IFeignSharingTemplate {

    @RequestMapping(value = "/system/sharingTemplate/listForPage", method = RequestMethod.POST)
    Page<SharingTemplateVO> listForPage(@RequestBody SharingTemplateQO qo);

    @RequestMapping(value = "/system/sharingTemplate/save", method = RequestMethod.POST)
    int save(@RequestBody SharingTemplateQO qo);

    @RequestMapping(value = "/system/sharingTemplate/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sharingTemplate/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SharingTemplateQO qo);

    @RequestMapping(value = "/system/sharingTemplate/get/{id}", method = RequestMethod.GET)
    SharingTemplateVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据是否使用模板、模板类型获取模板
     *
     * @return
     */
    @RequestMapping(value = "/feign/system/sharingTemplate/getIsUseTemplateAndTemplateType", method = RequestMethod.POST)
    SharingTemplateVO getIsUseTemplateAndTemplateType(@RequestBody SharingTemplateQO qo);

}
