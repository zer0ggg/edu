package com.roncoo.education.system.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 广告信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AdvQO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 当前页
	 */
	private int pageCurrent;
	/**
	 * 每页记录数
	 */
	private int pageSize;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1:正常，0:禁用)
	 */
	private Integer statusId;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 广告标题
	 */
	private String advTitle;
	/**
	 * 广告链接
	 */
	private String advUrl;
	/**
	 * 广告图片
	 */
	private String advImg;
	/**
	 * 广告跳转方式
	 */
	private String advTarget;
	/**
	 * 广告位置(1首页轮播)
	 */
	private Integer advLocation;
	/**
	 * 开始时间
	 */
	private Date beginTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 位置(1电脑端，2微信端)
	 */
	private Integer platShow;

	private String beginTimeString;
	private String endTimeString;

	/**
	 * 课程ID
	 */
	private Long courseId;
	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 课程分类(1:普通课程;2:直播课程,4文库)
	 */
	private Integer courseCategory;

}
