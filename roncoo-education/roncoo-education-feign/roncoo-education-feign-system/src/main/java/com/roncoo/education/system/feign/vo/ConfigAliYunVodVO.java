package com.roncoo.education.system.feign.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 阿里云点播配置
 *
 * @author LYQ
 */
@Data
public class ConfigAliYunVodVO implements Serializable {

    private static final long serialVersionUID = -409671625075497299L;

    /**
     * 阿里云点播访问ID
     */
    private String aliYunVodAccessKeyId;

    /**
     * 阿里云点播访问秘钥
     */
    private String aliYunVodAccessKeySecret;

    /**
     * 阿里云点播回调通知地址
     */
    private String aliYunVodCallbackUrl;

    /**
     * 阿里云点播授权秘钥
     */
    private String aliYunVodAuthKey;

    /**
     * 阿里云点播数据服务秘钥
     */
    private String aliYunVodDataServiceKey;

    /**
     * 阿里云点播播放解密路径
     */
    private String aliYunVodPlayDecryptUrl;

    /**
     * 转码模板组ID
     */
    private String aliYunVodTranscodeTemplateGroupId;

    /**
     * 转码模板组ID--加密
     */
    private String aliYunVodTranscodeEncryptTemplateGroupId;

    /**
     * 阿里云点播水印ID
     */
    private String aliYunVodWatermarkId;
}
