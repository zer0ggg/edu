package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.ZoneRefQO;
import com.roncoo.education.system.feign.vo.ZoneRefVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 专区关联 接口
 *
 * @author wujing
 * @date 2020-05-23
 */
@FeignClient(value = "service-system")
public interface IFeignZoneRef {

    @RequestMapping(value = "/system/zoneRef/listForPage", method = RequestMethod.POST)
    Page<ZoneRefVO> listForPage(@RequestBody ZoneRefQO qo);

    @RequestMapping(value = "/system/zoneRef/save", method = RequestMethod.POST)
    int save(@RequestBody ZoneRefQO qo);

    @RequestMapping(value = "/system/zoneRef/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/zoneRef/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ZoneRefQO qo);

    @RequestMapping(value = "/system/zoneRef/get/{id}", method = RequestMethod.GET)
    ZoneRefVO getById(@PathVariable(value = "id") Long id);

}
