package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 系统配置表
 *
 * @author YZJ
 */
@Data
@Accessors(chain = true)
public class ConfigAliyunVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String aliyunAccessKeyId;
	private String aliyunAccessKeySecret;
	private String aliyunOssUrl;
	private String aliyunOssEndpoint;
	private String aliyunOssBucket;
	private String aliyunOSSBucketVideo;
	private String aliyunAppCode;
	private String aliyunSmsCode;
	private String aliyunSmsSignName;
}
