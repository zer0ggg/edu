package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.VipSetQO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 会员设置
 *
 * @author wujing
 */
@FeignClient(value = "service-system")
public interface IFeignVipSet {

    @RequestMapping(value = "/vipSet/listForPage", method = RequestMethod.POST)
    Page<VipSetVO> listForPage(@RequestBody VipSetQO qo);

    @RequestMapping(value = "/vipSet/save", method = RequestMethod.POST)
    int save(@RequestBody VipSetQO qo);

    @RequestMapping(value = "/vipSet/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/vipSet/update", method = RequestMethod.PUT)
    int updateById(@RequestBody VipSetQO qo);

    @RequestMapping(value = "/vipSet/get/{id}", method = RequestMethod.GET)
    VipSetVO getById(@PathVariable(value = "id") Long id);

    /**
     * 更新状态
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/vipSet/updateStatusId", method = RequestMethod.PUT)
    int updateStatusId(@RequestBody VipSetQO qo);

    /**
     * 查找该是否设置该会员类型
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/vipSet/getByVipTypeAndStatusId", method = RequestMethod.POST)
    VipSetVO getByVipTypeAndStatusId(@RequestBody VipSetQO qo);
}
