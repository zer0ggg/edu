package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 专区
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
public class ZoneVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 名称
     */
    private String zoneName;

    /**
     * 专区分类(1:普通课程;2:直播课程,3文库，4:试卷，5:博客;6:资讯，7:自定义)
     */
    private Integer zoneCategory;

    /**
     * 描述
     */
    private String zoneDesc;

    /**
     * 位置(1电脑端，2微信端)
     */
    private Integer zoneLocation;

    private Integer showType;


}
