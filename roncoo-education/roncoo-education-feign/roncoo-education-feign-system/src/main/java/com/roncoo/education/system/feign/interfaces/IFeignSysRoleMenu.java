package com.roncoo.education.system.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.qo.SysRoleMenuQO;
import com.roncoo.education.system.feign.vo.SysRoleMenuVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 系统角色菜单 接口
 *
 * @author wujing
 * @date 2020-04-29
 */
@FeignClient(value = "service-system")
public interface IFeignSysRoleMenu {

    @RequestMapping(value = "/system/sysRoleMenu/listForPage", method = RequestMethod.POST)
    Page<SysRoleMenuVO> listForPage(@RequestBody SysRoleMenuQO qo);

    @RequestMapping(value = "/system/sysRoleMenu/save", method = RequestMethod.POST)
    int save(@RequestBody SysRoleMenuQO qo);

    @RequestMapping(value = "/system/sysRoleMenu/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/system/sysRoleMenu/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody SysRoleMenuQO qo);

    @RequestMapping(value = "/system/sysRoleMenu/get/{id}", method = RequestMethod.GET)
    SysRoleMenuVO getById(@PathVariable(value = "id") Long id);

}
