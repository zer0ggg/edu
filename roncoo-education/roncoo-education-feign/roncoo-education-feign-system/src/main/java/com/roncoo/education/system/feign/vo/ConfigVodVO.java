package com.roncoo.education.system.feign.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 阿里云点播配置
 *
 * @author LYQ
 */
@Data
public class ConfigVodVO implements Serializable {

    private static final long serialVersionUID = -409671625075497299L;

    /**
     * 点播平台
     */
    private Integer vodPlatform;

    /**
     * 点播视频是否加密（0:不加密、1:加密）
     */
    private Integer vodVideoEncrypt;
}
