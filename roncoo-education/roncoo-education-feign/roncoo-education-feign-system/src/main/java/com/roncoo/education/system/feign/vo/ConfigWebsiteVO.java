package com.roncoo.education.system.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 站点信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class ConfigWebsiteVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String domain;
	private String ico;
	private String logo;
	private String title;
	/**
	 * 站点关键词
	 */
	private String keyword;
	/**
	 * 站点描述
	 */
	private String desc;
	/**
	 * 站点版权
	 */
	private String copyright;
	/**
	 * 备案号
	 */
	private String icp;
	/**
	 * 公安备案号
	 */
	private String prn;
	/**
	 * 站点微信
	 */
	private String wxCode;
	/**
	 * 小程序二维码
	 */
	private String minappCode;
	/**
	 * 站点微博
	 */
	private String wbCode;
	/**
	 * 是否开启统计
	 */
	private Integer isEnableStatistics;
	/**
	 * 统计代码
	 */
	private String statisticsCode;
	/**
	 * 是否显示客服信息
	 */
	private Integer isShowService;
	/**
	 * 客服信息1
	 */
	private String service1;
	/**
	 * 客服信息2
	 */
	private String service2;
	/**
	 * 用户协议
	 */
	private String userAgreement;
	/**
	 * 会员协议
	 */
	private String vipAgreement;
	/**
	 * 入驻协议
	 */
	private String recruitAgreement;
	/**
	 * 招募标题
	 */
	private String recruitTitle;
	/**
	 * 招募信息
	 */
	private String recruitInfo;

}
