package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.MsgTemplateQO;
import com.roncoo.education.user.feign.vo.MsgTemplateVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 消息模板
 *
 * @author wuyun
 */
@FeignClient(value = "service-user")
public interface IFeignMsgTemplate {

    @RequestMapping(value = "/msgTemplate/listForPage")
    Page<MsgTemplateVO> listForPage(@RequestBody MsgTemplateQO qo);

    @RequestMapping(value = "/msgTemplate/save")
    int save(@RequestBody MsgTemplateQO qo);

    @RequestMapping(value = "/msgTemplate/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/msgTemplate/updateById")
    int updateById(@RequestBody MsgTemplateQO qo);

    @RequestMapping(value = "/msgTemplate/getById")
    MsgTemplateVO getById(@RequestBody Long id);

}
