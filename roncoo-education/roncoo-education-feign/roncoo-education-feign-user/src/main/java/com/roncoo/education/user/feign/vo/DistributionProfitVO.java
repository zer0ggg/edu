package com.roncoo.education.user.feign.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 代理分润记录
 *
 * @author wujing
 * @date 2021-01-05
 */
@Data
@Accessors(chain = true)
public class DistributionProfitVO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 分销ID
     */
    private Long distributionId;

    /**
     * 分销用户编号
     */
    private Long userNo;

    /**
     * 分销父ID
     */
    private Long parentId;

    /**
     * 分销层级
     */
    private Integer floor;

    /**
     * 分润金额（1：推广；2：邀请）
     */
    private Integer profitType;

    /**
     * 分润比例
     */
    private BigDecimal profit;

    /**
     * 分润金额
     */
    private BigDecimal profitMoney;

    /**
     * 订单号
     */
    private Long orderNo;

    /**
     * 订单金额
     */
    private BigDecimal orderPrice;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)
     */
    private Integer productType;
}
