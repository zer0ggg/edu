package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserAccountQO;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户账户信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUserAccount {

    @RequestMapping(value = "/userAccount/listForPage", method = RequestMethod.POST)
    Page<UserAccountVO> listForPage(@RequestBody UserAccountQO qo);

    @RequestMapping(value = "/userAccount/save", method = RequestMethod.POST)
    int save(@RequestBody UserAccountQO qo);

    @RequestMapping(value = "/userAccount/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userAccount/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserAccountQO qo);

    @RequestMapping(value = "/userAccount/get/{id}", method = RequestMethod.GET)
    UserAccountVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据用户编号获取用户信息
     *
     * @param userNo
     * @return
     */
    @RequestMapping(value = "/userAccount/getByUserNo", method = RequestMethod.POST)
    UserAccountVO getByUserNo(@RequestBody UserAccountQO userAccountQO);

    /**
     * 更新账户的金额信息(必须注意传入的金额，金额代表要增加的数量)
     *
     * @param userAccountQO
     */
    @RequestMapping(value = "/userAccount/updateTotalIncomeByUserNo", method = RequestMethod.POST)
    void updateTotalIncomeByUserNo(@RequestBody UserAccountQO userAccountQO);
}
