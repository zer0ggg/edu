package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserRecommendedQO;
import com.roncoo.education.user.feign.vo.UserRecommendedVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户推荐信息 接口
 *
 * @author wujing
 * @date 2020-07-15
 */
@FeignClient(value = "service-user")
public interface IFeignUserRecommended {

    @RequestMapping(value = "/user/userRecommended/listForPage", method = RequestMethod.POST)
    Page<UserRecommendedVO> listForPage(@RequestBody UserRecommendedQO qo);

    @RequestMapping(value = "/user/userRecommended/save", method = RequestMethod.POST)
    int save(@RequestBody UserRecommendedQO qo);

    @RequestMapping(value = "/user/userRecommended/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/userRecommended/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody UserRecommendedQO qo);

    @RequestMapping(value = "/user/userRecommended/get/{id}", method = RequestMethod.GET)
    UserRecommendedVO getById(@PathVariable(value = "id") Long id);

}
