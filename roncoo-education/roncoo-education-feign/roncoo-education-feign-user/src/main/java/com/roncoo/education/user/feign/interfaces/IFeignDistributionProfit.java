package com.roncoo.education.user.feign.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.DistributionProfitQO;
import com.roncoo.education.user.feign.vo.DistributionProfitVO;

/**
 * 代理分润记录 接口
 *
 * @author wujing
 * @date 2021-01-05
 */
@FeignClient(value = "roncoo-education-service-user")
public interface IFeignDistributionProfit{

    @RequestMapping(value = "/user/distributionProfit/listForPage", method = RequestMethod.POST)
    Page<DistributionProfitVO> listForPage(@RequestBody DistributionProfitQO qo);

    @RequestMapping(value = "/user/distributionProfit/save", method = RequestMethod.POST)
    int save(@RequestBody DistributionProfitQO qo);

    @RequestMapping(value = "/user/distributionProfit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/distributionProfit/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody DistributionProfitQO qo);

    @RequestMapping(value = "/user/distributionProfit/get/{id}", method = RequestMethod.GET)
    DistributionProfitVO getById(@PathVariable(value = "id") Long id);

}
