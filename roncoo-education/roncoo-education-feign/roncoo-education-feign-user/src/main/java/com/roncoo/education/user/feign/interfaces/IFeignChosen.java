package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.ChosenQO;
import com.roncoo.education.user.feign.vo.ChosenVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 分销信息 接口
 *
 * @author wujing
 * @date 2020-06-17
 */
@FeignClient(value = "service-user")
public interface IFeignChosen {

    @RequestMapping(value = "/user/chosen/listForPage", method = RequestMethod.POST)
    Page<ChosenVO> listForPage(@RequestBody ChosenQO qo);

    @RequestMapping(value = "/user/chosen/save", method = RequestMethod.POST)
    int save(@RequestBody ChosenQO qo);

    @RequestMapping(value = "/user/chosen/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/chosen/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody ChosenQO qo);

    @RequestMapping(value = "/user/chosen/get/{id}", method = RequestMethod.GET)
    ChosenVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/chosen/getByCourseId/{courseId}", method = RequestMethod.GET)
    ChosenVO getByCourseId(@PathVariable(value = "courseId") Long courseId);

}
