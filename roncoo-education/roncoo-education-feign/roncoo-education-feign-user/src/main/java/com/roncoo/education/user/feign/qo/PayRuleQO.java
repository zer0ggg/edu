package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 支付路由表
 *
 * @author wujing
 * @date 2020-04-02
 */
@Data
@Accessors(chain = true)
public class PayRuleQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 支付渠道编码
     */
    private String payChannelCode;

    /**
     * 支付渠道名称
     */
    private String payChannelName;

    /**
     * 渠道优先等级
     */
    private Integer channelPriority;

    /**
     * 参考PayTypeEnum
     */
    private Integer payType;

}
