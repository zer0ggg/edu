package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.OrderEchartsQO;
import com.roncoo.education.user.feign.qo.OrderInfoQO;
import com.roncoo.education.user.feign.vo.CountIncomeVO;
import com.roncoo.education.user.feign.vo.OrderEchartsVO;
import com.roncoo.education.user.feign.vo.OrderInfoVO;
import com.roncoo.education.user.feign.vo.OrderReportVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 订单信息表
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignOrderInfo {

    @RequestMapping(value = "/orderInfo/listForPage", method = RequestMethod.POST)
    Page<OrderInfoVO> listForPage(@RequestBody OrderInfoQO qo);

    @RequestMapping(value = "/orderInfo/save", method = RequestMethod.POST)
    int save(@RequestBody OrderInfoQO qo);

    @RequestMapping(value = "/orderInfo/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/orderInfo/update", method = RequestMethod.PUT)
    int updateById(@RequestBody OrderInfoQO qo);

    @RequestMapping(value = "/orderInfo/get/{id}", method = RequestMethod.GET)
    OrderInfoVO getById(@PathVariable(value = "id") Long id);

    /**
     * 订单信息汇总（导出报表）
     *
     * @author YZJ
     */
    @RequestMapping(value = "/orderInfo/listForReport", method = RequestMethod.POST)
    List<OrderReportVO> listForReport(@RequestBody OrderInfoQO orderInfoQO);

    /**
     * 统计时间段的总订单数
     *
     * @param orderEchartsQO
     * @return
     * @author wuyun
     */
    @RequestMapping(value = "/orderInfo/sumByCountOrders", method = RequestMethod.POST)
    List<OrderEchartsVO> sumByCountOrders(@RequestBody OrderEchartsQO orderEchartsQO);

    /**
     * 统计时间段的总收入
     *
     * @param orderEchartsQO
     * @return
     * @author wuyun
     */
    @RequestMapping(value = "/orderInfo/sumByPayTime", method = RequestMethod.POST)
    List<OrderEchartsVO> sumByPayTime(@RequestBody OrderEchartsQO orderEchartsQO);

    /**
     * 统计订单收入情况
     *
     * @author wuyun
     */
    @RequestMapping(value = "/orderInfo/countIncome", method = RequestMethod.POST)
    CountIncomeVO countIncome(@RequestBody OrderInfoQO qo);

    /**
     * 根据订单号获取订单信息
     *
     * @return
     */
    @RequestMapping(value = "/orderInfo/getByOrderNo", method = RequestMethod.POST)
    OrderInfoVO getByOrderNo(@RequestBody OrderInfoQO qo);

    /**
     * 手工下单
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/orderInfo/manualOrder", method = RequestMethod.POST)
    int manualOrder(@RequestBody OrderInfoQO qo);

    /**
     * 根据订单号和课程ID获取订单信息
     *
     * @return
     */
    @RequestMapping(value = "/orderInfo/getByUserNoAndProductId", method = RequestMethod.POST)
    OrderInfoVO getByUserNoAndProductId(@RequestBody OrderInfoQO qo);

}
