package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 分销信息
 *
 * @author wujing
 * @date 2020-06-17
 */
@Data
@Accessors(chain = true)
public class ChosenQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreate;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 状态ID(1:正常;0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 课程ID
     */
    private Long courseId;

    /**
     * 分销比例
     */
    private BigDecimal chosenPercent;

    /**
     * 成功分销人数
     */
    private Integer chosenCount;

}
