package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class MsgQO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 当前页
	 */
	private int pageCurrent;
	/**
	 * 每页记录数
	 */
	private int pageSize;
	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1有效, 0无效)
	 */
	private Integer statusId;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 短信类型
	 */
	private Integer msgType;
	/**
	 * 短信标题
	 */
	private String msgTitle;
	/**
	 * 短信内容
	 */
	private String msgText;
	/**
	 * 是否定时发送（1是，0否）
	 */
	private Integer isTimeSend;
	/**
	 * 发送时间
	 */
	private Date sendTime;
	/**
	 * 是否已发送(1是;0否)
	 */
	private Integer isSend;
	/**
	 * 是否置顶(1是;0否)
	 */
	private Integer isTop;
	/**
	 * 后台备注
	 */
	private String backRemark;


	// 课程审核发通知消息讲师
	/**
	 * 讲师用户编号
	 */
	private Long lecturerUserNo;
	/**
	 * 课程ID
	 */
	private Long courseId;
	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 审核状态(0:待审核,1:审核通过,2:审核不通过)
	 */
	private Integer auditStatus;
	/**
	 * 课程审核意见
	 */
	private String auditOpinion;


}
