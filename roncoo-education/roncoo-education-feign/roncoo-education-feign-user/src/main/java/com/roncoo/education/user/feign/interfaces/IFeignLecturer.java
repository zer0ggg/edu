package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 讲师信息
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignLecturer {

    @RequestMapping(value = "/lecturer/listForPage", method = RequestMethod.POST)
    Page<LecturerVO> listForPage(@RequestBody LecturerQO qo);

    @RequestMapping(value = "/lecturer/save", method = RequestMethod.POST)
    int save(@RequestBody LecturerQO qo);

    @RequestMapping(value = "/lecturer/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/lecturer/update", method = RequestMethod.PUT)
    int updateById(@RequestBody LecturerQO qo);

    @RequestMapping(value = "/lecturer/get/{id}", method = RequestMethod.GET)
    LecturerVO getById(@PathVariable(value = "id") Long id);

    /***
     * 根据讲师用户编号查找讲师信息
     */
    @RequestMapping(value = "/lecturer/getByLecturerUserNo/{lecturerUserNo}", method = RequestMethod.GET)
    LecturerVO getByLecturerUserNo(@PathVariable(value = "lecturerUserNo") Long lecturerUserNo);

    /**
     * 列出所有讲师信息
     *
     * @author LHR
     */
    @RequestMapping(value = "/lecturer/listAllForLecturer", method = RequestMethod.POST)
    List<LecturerVO> listAllForLecturer();

    /**
     * 更新直播场景
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/lecturer/liveUpdateById", method = RequestMethod.PUT)
    int liveUpdateById(@RequestBody LecturerQO qo);

    /**
     * 根据手机号获取讲师信息
     *
     * @param lecturerMobile
     * @return
     */
    @RequestMapping(value = "/lecturer/getByLecturerMobile/{lecturerMobile}", method = RequestMethod.GET)
    LecturerVO getByLecturerMobile(@PathVariable(value = "lecturerMobile") String lecturerMobile);

    /**
     * 根据讲师编号集合获取讲师信息
     *
     * @param lecturerQO
     * @return
     */
    @RequestMapping(value = "/lecturer/listByLecturerUserNos", method = RequestMethod.POST)
    List<LecturerVO> listByLecturerUserNos(@RequestBody LecturerQO lecturerQO);
}
