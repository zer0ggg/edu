package com.roncoo.education.user.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserAccountExtractLogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 用户编号
     */
    private Long userNo;
    /**
     * 电话
     */
    private String phone;
    /**
     * 银行卡号
     */
    private String bankCardNo;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 银行支行名称
     */
    private String bankBranchName;
    /**
     * 银行开户名
     */
    private String bankUserName;
    /**
     * 银行身份证号
     */
    private String bankIdCardNo;
    /**
     * 提现状态（1申请中，2支付中，3确认中，4成功，5失败）
     */
    private Integer extractStatus;
    /**
     * 提现金额
     */
    private BigDecimal extractMoney;
    /**
     * 用户收入
     */
    private BigDecimal userIncome;
    /**
     * 备注
     */
    private String remark;

}
