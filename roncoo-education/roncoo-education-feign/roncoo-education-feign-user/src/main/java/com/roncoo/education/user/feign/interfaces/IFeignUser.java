package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserQO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户基本信息
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUser {

    @RequestMapping(value = "/user/listForPage", method = RequestMethod.POST)
    Page<UserVO> listForPage(@RequestBody UserQO qo);

    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    int save(@RequestBody UserQO qo);

    @RequestMapping(value = "/user/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserQO qo);

    @RequestMapping(value = "/user/get/{id}", method = RequestMethod.GET)
    UserVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/getByUserNo/{userNo}", method = RequestMethod.GET)
    UserVO getByUserNo(@PathVariable(value = "userNo") Long userNo);

    @RequestMapping(value = "/user/getByMobile/{mobile}", method = RequestMethod.GET)
    UserVO getByMobile(@PathVariable(value = "mobile") String mobile);

    @RequestMapping(value = "/user/updateUniconId/{userNo}", method = RequestMethod.DELETE)
    int updateUniconId(@PathVariable(value = "userNo") Long userNo);

}
