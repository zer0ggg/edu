package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserLogSmsQO;
import com.roncoo.education.user.feign.vo.UserLogSmsVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用户发送短信日志
 *
 * @author YZJ
 */
@FeignClient(value = "service-user")
public interface IFeignUserLogSms {

    @RequestMapping(value = "/userLogSms/listForPage")
    Page<UserLogSmsVO> listForPage(@RequestBody UserLogSmsQO qo);

    @RequestMapping(value = "/userLogSms/save")
    int save(@RequestBody UserLogSmsQO qo);

    @RequestMapping(value = "/userLogSms/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/userLogSms/updateById")
    int updateById(@RequestBody UserLogSmsQO qo);

    @RequestMapping(value = "/userLogSms/getById")
    UserLogSmsVO getById(@RequestBody Long id);

    /**
     * 用户发送短信日志
     *
     * @param qo
     * @return
     * @author YZJ
     */
    @RequestMapping(value = "/userLogSms/send")
    int send(@RequestBody UserLogSmsQO qo);

    /**
     * 统计短信发送
     *
     * @author kyh
     */
    @RequestMapping(value = "/userLogSms/statistical")
    UserLogSmsVO statistical();

}
