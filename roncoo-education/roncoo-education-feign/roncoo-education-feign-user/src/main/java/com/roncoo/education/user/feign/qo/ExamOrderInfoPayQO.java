/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.feign.qo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 试卷订单信息下单信息
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class ExamOrderInfoPayQO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "用户编号不能为空")
    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @NotNull(message = "讲师用户编号不能为空")
    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @NotNull(message = "试卷名称不能为空")
    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @NotNull(message = "支付方式不能为空")
    @ApiModelProperty(value = "支付方式：1微信支付，2:支付宝支付")
    private Integer payType;

    @NotNull(message = "购买渠道不能为空")
    @ApiModelProperty(value = "购买渠道：1:PC端，2:APP端，3:微信")
    private Integer channelType;

    @DecimalMin(value = "0", inclusive = false, message = "原价必须大于0")
    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @DecimalMin(value = "0", inclusive = false, message = "实付价格必须大于0")
    @ApiModelProperty(value = "实付价格")
    private BigDecimal paidPrice;

    @ApiModelProperty(value = "用户备注")
    private String remark;

}
