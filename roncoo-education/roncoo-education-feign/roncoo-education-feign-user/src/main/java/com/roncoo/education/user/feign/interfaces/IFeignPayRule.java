package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.PayRuleQO;
import com.roncoo.education.user.feign.vo.PayRuleVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 支付路由表 接口
 *
 * @author wujing
 * @date 2020-04-02
 */
@FeignClient(value = "service-user")
public interface IFeignPayRule {

    @RequestMapping(value = "/user/payRule/listForPage", method = RequestMethod.POST)
    Page<PayRuleVO> listForPage(@RequestBody PayRuleQO qo);

    @RequestMapping(value = "/user/payRule/save", method = RequestMethod.POST)
    int save(@RequestBody PayRuleQO qo);

    @RequestMapping(value = "/user/payRule/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/payRule/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody PayRuleQO qo);

    @RequestMapping(value = "/user/payRule/get/{id}", method = RequestMethod.GET)
    PayRuleVO getById(@PathVariable(value = "id") Long id);

}
