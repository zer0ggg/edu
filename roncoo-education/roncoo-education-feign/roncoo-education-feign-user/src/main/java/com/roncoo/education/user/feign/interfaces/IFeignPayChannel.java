package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.PayChannelQO;
import com.roncoo.education.user.feign.vo.PayChannelVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 银行渠道信息表 接口
 *
 * @author wujing
 * @date 2020-03-25
 */
@FeignClient(value = "service-user")
public interface IFeignPayChannel {

    @RequestMapping(value = "/user/payChannel/listForPage", method = RequestMethod.POST)
    Page<PayChannelVO> listForPage(@RequestBody PayChannelQO qo);

    @RequestMapping(value = "/user/payChannel/save", method = RequestMethod.POST)
    int save(@RequestBody PayChannelQO qo);

    @RequestMapping(value = "/user/payChannel/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/payChannel/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody PayChannelQO qo);

    @RequestMapping(value = "/user/payChannel/get/{id}", method = RequestMethod.GET)
    PayChannelVO getById(@PathVariable(value = "id") Long id);

}
