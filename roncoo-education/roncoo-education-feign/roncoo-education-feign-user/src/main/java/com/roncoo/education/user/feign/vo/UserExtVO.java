package com.roncoo.education.user.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserExtVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1:正常，0:禁用)
	 */
	private Integer statusId;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 用户类型(1用户，2讲师)
	 */
	private Integer userType;
	/**
	 * 用户手机
	 */
	private String mobile;
	/**
	 * 性别(1男，2女，3保密)
	 */
	private Integer sex;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 头像地址
	 */
	private String headImgUrl;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 是否是为会员(0:不是,1:是)
	 */
	private Integer isVip;
	/**
	 * 会员类型(1.年费，2.季度，3.月度)
	 */
	private Integer vipType;
	/**
	 * 是否是企业用户
	 */
	private Integer isEnterpriseUser;
	/**
	 * 过期时间
	 */
	private Date expireTime;
	/**
	 * 添加是否存在博主
	 */
	private Integer isExist;
	/**
	 * 是否绑定小程序微信小程序(1:是, 0:否)
	 */
	private Integer isBinding;

	/**
	 * 提问数量
	 */
	private Integer questionsAccount;
	/**
	 * 回答数量
	 */
	private Integer questionsResponseAccount;
	/**
	 * 推荐码
	 */
	private String referralCode;
	/**
	 * 二维码url
	 */
	private String codeUrl;
	/**
	 * 会员剩余时间
	 */
	private String surplus;
	/**
	 * 超级会员时间修改
	 */
	private String expireTimeString;

	private String idCardName;

	private String idCardNo;

	private String idCardFrontImg;

	private String idCardAfterImg;

	private String faceContrasImg;
}
