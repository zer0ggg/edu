/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.feign.qo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class OrderInfoPayQO implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
    private Integer payType;

    @ApiModelProperty(value = "购买渠道：1web，2pp，3微信")
    private Integer channelType;

    @ApiModelProperty(value = "用户备注")
    private String remark;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType = 1;

    @ApiModelProperty(value = "实付价格")
    private BigDecimal paidPrice;

}
