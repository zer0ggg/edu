package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.UserMsgVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@FeignClient(value = "service-user")
public interface IFeignUserMsg {

    @RequestMapping(value = "/userMsg/listForPage")
    Page<UserMsgVO> listForPage(@RequestBody UserMsgQO qo);

    @RequestMapping(value = "/userMsg/save")
    int save(@RequestBody UserMsgQO qo);

    @RequestMapping(value = "/userMsg/msg/save/batch")
    void batchSaveUserMsgAndMsg(@RequestBody List<UserMsgAndMsgSaveQO> qo);

    @RequestMapping(value = "/userMsg/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/userMsg/updateById")
    int updateById(@RequestBody UserMsgQO qo);

    @RequestMapping(value = "/userMsg/getById")
    UserMsgVO getById(@RequestBody Long id);

    /**
     * 定时器推送站内信
     *
     * @return
     * @author wuyun
     */
    @RequestMapping(value = "/userMsg/push")
    int push();

    /**
     * 手动推送站内信
     *
     * @return
     * @author wuyun
     */
    @RequestMapping(value = "/userMsg/pushByManual")
    int pushByManual(@RequestBody Long id);
}
