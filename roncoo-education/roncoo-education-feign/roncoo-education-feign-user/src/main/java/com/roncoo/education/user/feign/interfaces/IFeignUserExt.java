package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserExtEchartsQO;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserEchartsVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUserExt {

    @RequestMapping(value = "/userExt/listForPage", method = RequestMethod.POST)
    Page<UserExtVO> listForPage(@RequestBody UserExtQO qo);

    @RequestMapping(value = "/userExt/save", method = RequestMethod.POST)
    int save(@RequestBody UserExtQO qo);

    @RequestMapping(value = "/userExt/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userExt/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserExtQO qo);

    @RequestMapping(value = "/userExt/get/{id}", method = RequestMethod.GET)
    UserExtVO getById(@PathVariable(value = "id") Long id);

    /**
     * 根据userNo查找用户教育信息
     */
    @RequestMapping(value = "/userExt/get/userNo/{userNo}", method = RequestMethod.GET)
    UserExtVO getByUserNo(@PathVariable(value = "userNo") Long userNo);

    /**
     * 根据userNo查找用户教育信息
     */
    @RequestMapping(value = "/userExt/get/userNo/forCache/{userNo}", method = RequestMethod.GET)
    UserExtVO getByUserNoForCache(@PathVariable(value = "userNo") Long userNo);

    /**
     * 获取用户注册量
     *
     * @param userExtEchartsQO
     * @return
     * @author wuyun
     */
    @RequestMapping(value = "/userExt/sumByCounts", method = RequestMethod.POST)
    List<UserEchartsVO> sumByCounts(@RequestBody UserExtEchartsQO userExtEchartsQO);

    /**
     * 批量缓存用户信息,发送站内信用
     *
     * @author wuyun
     */
    @RequestMapping(value = "/userExt/cachUserForMsg")
    void cachUserForMsg();

    /**
     * 列出所有用户
     *
     * @return
     */
    @RequestMapping(value = "/userExt/listAllForUser", method = RequestMethod.POST)
    List<UserExtVO> listAllForUser();

    /**
     * 根据手机号获取用户信息
     *
     * @param userExtQO
     * @return
     */
    @RequestMapping(value = "/userExt/getByMobile", method = RequestMethod.POST)
    UserExtVO getByMobile(@RequestBody UserExtQO userExtQO);

    /**
     * 推荐码校验唯一性
     *
     * @param referralCode
     * @return
     */
    @RequestMapping(value = "/userExt/getByReferralCode/{referralCode}", method = RequestMethod.POST)
    UserExtVO getByReferralCode(@PathVariable(value = "referralCode") String referralCode);

    /**
     * 根据用户编号更新是否是企业用户
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/userExt/userIsEnterpriseUser", method = RequestMethod.POST)
    int userIsEnterpriseUser(@RequestBody UserExtQO qo);

    /**
     * 根据根据用户编号集合批量获取
     *
     * @param userExtQO
     * @return
     */
    @RequestMapping(value = "/userExt/listByUserNos", method = RequestMethod.POST)
    List<UserExtVO> listByUserNos(@RequestBody UserExtQO userExtQO);
}
