package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class UserMsgAndMsgSaveQO implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserMsgQO userMsg;

	private MsgQO msg;

}
