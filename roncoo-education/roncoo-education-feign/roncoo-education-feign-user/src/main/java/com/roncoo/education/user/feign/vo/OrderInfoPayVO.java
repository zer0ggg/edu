/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单支付
 *
 * @author forest
 */
@Data
@Accessors(chain = true)
public class OrderInfoPayVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orderNo;

	private String payMessage;

	private String productName;

	private BigDecimal price;

	private Integer payType;
}
