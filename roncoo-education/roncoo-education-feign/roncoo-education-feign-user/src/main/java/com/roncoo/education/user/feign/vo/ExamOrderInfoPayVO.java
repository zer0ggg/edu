/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.feign.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 试卷订单下单结果
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ExamOrderInfoPayVO", description = "试卷订单下单结果")
public class ExamOrderInfoPayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单编号")
    private Long orderNo;

    @ApiModelProperty(value = "支付信息")
    private String payMessage;

    @ApiModelProperty(value = "支付价格")
    private BigDecimal price;

    @ApiModelProperty(value = "支付类型")
    private Integer payType;
}
