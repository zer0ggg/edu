package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.LiveChannelQO;
import com.roncoo.education.user.feign.vo.LiveChannelVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * <p>
 * 直播频道表 接口
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@FeignClient(value = "service-user")
public interface IFeignLiveChannel {

    @RequestMapping(value = "/feign/user/liveChannel/listForPage", method = RequestMethod.POST)
    Page<LiveChannelVO> listForPage(@RequestBody LiveChannelQO qo);

    @RequestMapping(value = "/feign/user/liveChannel/save", method = RequestMethod.POST)
    int save(@RequestBody LiveChannelQO qo);

    @RequestMapping(value = "/feign/user/liveChannel/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/user/liveChannel/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody LiveChannelQO qo);

    @RequestMapping(value = "/feign/user/liveChannel/get/{id}", method = RequestMethod.GET)
    LiveChannelVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/feign/user/liveChannel/updateChannelIsUseAndPasswd", method = RequestMethod.POST)
    int updateChannelIsUseAndPasswd(@RequestBody LiveChannelQO qo);

    @RequestMapping(value = "/feign/user/liveChannel/listBySceneAndIsUse", method = RequestMethod.POST)
    List<LiveChannelVO> listBySceneAndIsUse(@RequestBody LiveChannelQO qo);

    @RequestMapping(value = "/feign/user/liveChannel/getByChannelId/{channelId}", method = RequestMethod.GET)
    LiveChannelVO getByChannelId(@PathVariable(value = "channelId") String channelId);
}
