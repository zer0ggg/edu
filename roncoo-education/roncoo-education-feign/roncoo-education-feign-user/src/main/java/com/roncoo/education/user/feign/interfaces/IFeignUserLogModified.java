package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserLogModifiedQO;
import com.roncoo.education.user.feign.vo.UserLogModifiedVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户修改日志
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUserLogModified {

    @RequestMapping(value = "/userLogModified/listForPage", method = RequestMethod.POST)
    Page<UserLogModifiedVO> listForPage(@RequestBody UserLogModifiedQO qo);

    @RequestMapping(value = "/userLogModified/save", method = RequestMethod.POST)
    int save(@RequestBody UserLogModifiedQO qo);

    @RequestMapping(value = "/userLogModified/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userLogModified/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserLogModifiedQO qo);

    @RequestMapping(value = "/userLogModified/get/{id}", method = RequestMethod.GET)
    UserLogModifiedVO getById(@PathVariable(value = "id") Long id);

}
