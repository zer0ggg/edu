package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserAccountExtractLogQO;
import com.roncoo.education.user.feign.vo.UserAccountExtractLogVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUserAccountExtractLog {

    @RequestMapping(value = "/userAccountExtractLog/listForPage", method = RequestMethod.POST)
    Page<UserAccountExtractLogVO> listForPage(@RequestBody UserAccountExtractLogQO qo);

    @RequestMapping(value = "/userAccountExtractLog/save", method = RequestMethod.POST)
    int save(@RequestBody UserAccountExtractLogQO qo);

    @RequestMapping(value = "/userAccountExtractLog/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userAccountExtractLog/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserAccountExtractLogQO qo);

    @RequestMapping(value = "/userAccountExtractLog/get/{id}", method = RequestMethod.GET)
    UserAccountExtractLogVO getById(@PathVariable(value = "id") Long id);
}
