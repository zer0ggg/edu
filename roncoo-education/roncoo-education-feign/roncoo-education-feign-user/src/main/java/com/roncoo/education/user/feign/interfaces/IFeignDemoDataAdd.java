package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.user.feign.qo.UserQO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Demo数据添加
 */
@FeignClient(value = "service-user")
public interface IFeignDemoDataAdd {

    @RequestMapping(value = "/demo/data/save", method = RequestMethod.POST)
    int save(@RequestBody UserQO qo);

}
