package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 直播频道表
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
public class LiveChannelQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 频道id
     */
    private String channelId;

    /**
     * 频道密码
     */
    private String channelPasswd;

    /**
     * 直播场景(alone:活动拍摄;ppt:三分屏)
     */
    private String scene;

    /**
     * 直播状态(1:未开播;2:直播中;3:待生成回放;4:待转存;5:直播结束)
     */
    private Integer liveStatus;

    /**
     * 是否被使用(0:未使用,1:已使用)
     */
    private Integer isUse;

}
