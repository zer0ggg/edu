package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.LecturerAuditQO;
import com.roncoo.education.user.feign.vo.LecturerAuditVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignLecturerAudit {

    @RequestMapping(value = "/lecturerAudit/listForPage", method = RequestMethod.POST)
    Page<LecturerAuditVO> listForPage(@RequestBody LecturerAuditQO qo);

    @RequestMapping(value = "/lecturerAudit/save", method = RequestMethod.POST)
    int save(@RequestBody LecturerAuditQO qo);

    @RequestMapping(value = "/lecturerAudit/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/lecturerAudit/update", method = RequestMethod.PUT)
    int updateById(@RequestBody LecturerAuditQO qo);

    @RequestMapping(value = "/lecturerAudit/get/{id}", method = RequestMethod.GET)
    LecturerAuditVO getById(@PathVariable(value = "id") Long id);

    /**
     * 讲师添加时候的手机号码校验
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/lecturerAudit/checkUserAndLecturer", method = RequestMethod.POST)
    LecturerAuditVO checkUserAndLecturer(LecturerAuditQO qo);

    /**
     * 根据讲师编号获取讲师信息
     *
     * @param lecturerUserNo
     * @return
     */
    @RequestMapping(value = "/lecturerAudit/getLecturerUserNo/{lecturerUserNo}", method = RequestMethod.GET)
    LecturerAuditVO getByLecturerUserNo(@PathVariable(value = "lecturerUserNo") Long lecturerUserNo);

}
