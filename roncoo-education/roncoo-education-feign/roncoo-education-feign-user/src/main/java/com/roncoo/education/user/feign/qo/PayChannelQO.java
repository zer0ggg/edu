package com.roncoo.education.user.feign.qo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 银行渠道信息表
 *
 * @author wujing
 * @date 2020-03-25
 */
@Data
@Accessors(chain = true)
public class PayChannelQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 支付渠道编码
     */
    private String payChannelCode;

    /**
     * 支付渠道名称
     */
    private String payChannelName;

    /**
     * 支付实现编码
     */
    private String payObjectCode;

    /**
     * 支付实现名称
     */
    private String payObjectName;

    /**
     * 支付号
     */
    private String payKey;

    /**
     * 支付密钥
     */
    private String paySecret;

    /**
     * 请求地址
     */
    private String requestUrl;

    /**
     * 查询地址
     */
    private String queryUrl;

    /**
     * 备用字段1
     */
    private String field1;

    /**
     * 备用字段2
     */
    private String field2;

    /**
     * 备用字段3
     */
    private String field3;

}
