package com.roncoo.education.user.feign.qo;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 分销信息
 *
 * @author wujing
 * @date 2020-12-29
 */
@Data
@Accessors(chain = true)
public class DistributionQO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * 当前页
    */
    private int pageCurrent;
    /**
    * 每页记录数
    */
    private int pageSize;

    /**
     * 主键
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 层级
     */
    private Integer floor;

    /**
     * 是否允许推广下级
     */
    private Integer isAllowInvitation;

    /**
     * 推广比例
     */
    private BigDecimal spreadProfit;

    /**
     * 邀请比例
     */
    private BigDecimal inviteProfit;

    /**
     * 备注
     */
    private String remark;

}
