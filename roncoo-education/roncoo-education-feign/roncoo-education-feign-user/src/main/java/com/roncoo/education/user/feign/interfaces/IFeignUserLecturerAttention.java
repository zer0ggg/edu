package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.UserLecturerAttentionQO;
import com.roncoo.education.user.feign.vo.UserLecturerAttentionVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@FeignClient(value = "service-user")
public interface IFeignUserLecturerAttention {

    @RequestMapping(value = "/userLecturerAttention/listForPage", method = RequestMethod.POST)
    Page<UserLecturerAttentionVO> listForPage(@RequestBody UserLecturerAttentionQO qo);

    @RequestMapping(value = "/userLecturerAttention/save", method = RequestMethod.POST)
    int save(@RequestBody UserLecturerAttentionQO qo);

    @RequestMapping(value = "/userLecturerAttention/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userLecturerAttention/update", method = RequestMethod.PUT)
    int updateById(@RequestBody UserLecturerAttentionQO qo);

    @RequestMapping(value = "/userLecturerAttention/get/{id}", method = RequestMethod.GET)
    UserLecturerAttentionVO getById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/userLecturerAttention/getByUserAndLecturerUserNo", method = RequestMethod.POST)
    UserLecturerAttentionVO getByUserAndLecturerUserNo(@RequestBody UserLecturerAttentionQO qo);
}
