package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Base;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BaseFeignUser extends Base {

	@Autowired
	private IFeignLecturer feignLecturer;
	@Autowired
	private IFeignUserExt feignUserExt;

	/**
	 * 列出所有的讲师
	 * 
	 * @return
	 */
	public List<LecturerVO> listAllLecturer() {
		return feignLecturer.listAllForLecturer();
	}

	/**
	 * 列出所有用户
	 * 
	 * @return
	 */
	public List<UserExtVO> listAllForUser() {
		return feignUserExt.listAllForUser();
	}

}
