package com.roncoo.education.user.feign.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.DistributionQO;
import com.roncoo.education.user.feign.vo.DistributionVO;

/**
 * 分销信息 接口
 *
 * @author wujing
 * @date 2020-12-29
 */
@FeignClient(value = "roncoo-education-service-user")
public interface IFeignDistribution{

    @RequestMapping(value = "/user/distribution/listForPage", method = RequestMethod.POST)
    Page<DistributionVO> listForPage(@RequestBody DistributionQO qo);

    @RequestMapping(value = "/user/distribution/save", method = RequestMethod.POST)
    int save(@RequestBody DistributionQO qo);

    @RequestMapping(value = "/user/distribution/delete/{id}", method = RequestMethod.DELETE)
    int deleteById(@PathVariable(value = "id") Long id);

    @RequestMapping(value = "/user/distribution/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody DistributionQO qo);

    @RequestMapping(value = "/user/distribution/get/{id}", method = RequestMethod.GET)
    DistributionVO getById(@PathVariable(value = "id") Long id);

}
