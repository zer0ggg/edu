package com.roncoo.education.user.feign.interfaces;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.vo.MsgVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@FeignClient(value = "service-user")
public interface IFeignMsg {

    @RequestMapping(value = "/msg/listForPage", method = RequestMethod.POST)
    Page<MsgVO> listForPage(@RequestBody MsgQO qo);

    @RequestMapping(value = "/msg/save", method = RequestMethod.POST)
    int save(@RequestBody MsgQO qo);

    @RequestMapping(value = "/msg/deleteById", method = RequestMethod.DELETE)
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/msg/updateById", method = RequestMethod.PUT)
    int updateById(@RequestBody MsgQO qo);

    @RequestMapping(value = "/msg/getById", method = RequestMethod.GET)
    MsgVO getById(@RequestBody Long id);

    @RequestMapping(value = "/msg/listByStatusIdAndIsSendAndIsTimeSendAndSendTime")
    List<MsgVO> listByStatusIdAndIsSendAndIsTimeSendAndSendTime();

    @RequestMapping(value = "/msg/msgSendLecturer", method = RequestMethod.POST)
    int msgSendLecturer(@RequestBody MsgQO qo);
}
