package com.roncoo.education.app.sba.config;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.domain.events.InstanceStatusChangedEvent;
import de.codecentric.boot.admin.server.domain.values.StatusInfo;
import de.codecentric.boot.admin.server.notify.AbstractStatusChangeNotifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

/**
 * 自定义通知
 */
@Configuration
public class CustomNotifier extends AbstractStatusChangeNotifier {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public CustomNotifier(InstanceRepository repository) {
        super(repository);
    }

    @Override
    protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {
        return Mono.fromRunnable(() -> {
            if (event instanceof InstanceStatusChangedEvent) {
                StatusInfo statusInfo = ((InstanceStatusChangedEvent) event).getStatusInfo();
                logger.info("statusInfo={}", statusInfo);
                logger.info("instance={}", instance);

                if (statusInfo.isUp()) {
                    // 应用上线
                }
                if (statusInfo.isOffline()) {
                    // 应用下线
                }
                if (statusInfo.isDown()) {
                    // 启动失败
                }
                if (statusInfo.isUnknown()) {
                    // 未知状态
                }

            }
        });
    }

}
