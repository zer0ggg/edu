package com.roncoo.education.app.gateway.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.StringUtils;

@Slf4j
public final class FilterUtil {

    private FilterUtil() {
    }

    /**
     * 校验uri里面的第二个斜杠的字符串
     *
     * @param uri
     * @param key
     * @return
     */
    public static boolean checkUri(String uri, String key) {
        return new StringBuilder("/").append(uri.split("/")[2]).toString().equals(key);
    }

    /**
     * 获取请求IP
     *
     * @param ip 访问请求
     * @return 访问IP地址
     */
    public static String getIpAddr(String ip) {
        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个ip值，第一个ip才是真实ip
            if (ip.contains(",")) {
                ip = ip.split(",")[0];
            }
        }
        return ip;
    }

    /**
     * 获取JWT解码信息
     *
     * @param request 访问请求
     * @return JWT解码信息
     */
    public static DecodedJWT getDecodedJWT(ServerHttpRequest request) {
        // 头部
        String token = request.getHeaders().getFirst(Constants.TK.TOKEN);
        // 路径
        if (StringUtils.isEmpty(token)) {
            token = request.getQueryParams().getFirst(Constants.TK.TOKEN);
        }
        // 检验token
        if (StringUtils.isEmpty(token)) {
            throw new BaseException(ResultEnum.TOKEN_PAST);
        }
        // 解析 token
        try {
            return JWTUtil.verify(token);
        } catch (Exception e) {
            log.error("token异常，token={}", token, e);
            throw new BaseException(ResultEnum.TOKEN_ERROR);
        }
    }

}
