package com.roncoo.education.app.gateway.common;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class RibbonConfigurer {

    /**
     * 使用nacos的策略（权重策略），默认为轮询
     *
     * @return
     */
    @Bean
    @Scope(value = "prototype")
    public IRule loadBalanceRule() {
        return new NacosRule();
    }

}
