package com.roncoo.education.app.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wujing
 */
@Slf4j
@Component
public class PcGlobalFilter implements GlobalFilter, Ordered {

    private static final String LOGIN_URL = "/system/pc/api/sys/user/login";
    private static final String ENUM_URL = "/system/pc/api/enum/list";

    /**
     * 下载功能
     */
    private static final List<String> DOWNLOAD_URL =
            Arrays.asList(
                    "/system/pc/sensitive/word/library/txt/import",
                    "/user/pc/user/ext/upload/excel",
                    "/exam/pc/exam/problem/upload/excel");

    /**
     * 不需要授权认证的接口
     */
    private static final List<String> EXCLUDE_AUTHORIZE_URL =
            Arrays.asList("/system/pc/sys/user/menu",
                    "/system/pc/sys/user/permission",
                    "/system/pc/upload/aliyun");

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 优先级，order越大，优先级越低
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 10;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String uri = request.getPath().value();
        if (!FilterUtil.checkUri(uri, "/pc")) {
            // 不是管理后台接口，不进行处理（管理后台的接口，必须带有/pc）
            return chain.filter(exchange);
        }

        if (LOGIN_URL.equals(uri)) {
            // 登录
            return chain.filter(exchange);
        }

        // 解析token
        DecodedJWT jwt = FilterUtil.getDecodedJWT(request);
        if (!Constants.Platform.ADMIN.equals(JWTUtil.getType(jwt))) {
            throw new BaseException("参数异常");
        }
        // 运营后台校验
        String loginName = getLoginNameByToken(uri, jwt);
        request.mutate().header(Constants.TK.CURRENT_LOGIN_NAME, loginName).header("HTTP_X_FORWARDED_FOR", FilterUtil.getIpAddr(exchange.getRequest().getHeaders().getFirst("X-Forwarded-For")));
        return chain.filter(exchange);
    }


    /**
     * 运营后台相关校验
     */
    private String getLoginNameByToken(String uri, DecodedJWT jwt) {
        // 校验token
        String loginName = JWTUtil.getLoginName(jwt);
        if (StringUtils.isEmpty(loginName)) {
            throw new BaseException(ResultEnum.TOKEN_ERROR);
        }
        // 禁止异地登录
        String redisToken = myRedisTemplate.get(RedisPreEnum.SYS_USER.getCode().concat(loginName));
        if (!jwt.getToken().equals(redisToken)) {
            throw new BaseException(ResultEnum.TOKEN_PAST);
        }

        // 校验用户权限
        checkAdminAuthorize(uri, loginName);

        // 刷新token缓存
        myRedisTemplate.set(RedisPreEnum.SYS_USER.getCode().concat(loginName), jwt.getToken(), Constants.Session.TIME_OUT, TimeUnit.MINUTES);
        return loginName;
    }

    /**
     * 校验运营后台权限
     *
     * @param loginName 登录名称
     */
    private void checkAdminAuthorize(String uri, String loginName) {
        // 判断是否需要认证
        String userAuthorizeStr = myRedisTemplate.get(RedisPreEnum.SYS_USER_PERMISSION.getCode().concat(String.valueOf(loginName)));
        if (!EXCLUDE_AUTHORIZE_URL.contains(uri) && !uri.startsWith(ENUM_URL)) {
            if (StrUtil.isBlank(userAuthorizeStr)) {
                throw new BaseException(ResultEnum.AUTHORIZE_ERROR);
            }

            // 获取用户拥有的权限
            List<String> userPermissionList = JSON.parseArray(userAuthorizeStr, String.class);

            // 当前路径需要的访问权限
            String uriPermission = uri.substring(1).replaceAll("/", ":");

            // 鉴权
            if (!userPermissionList.contains(uriPermission)) {
                throw new BaseException(ResultEnum.AUTHORIZE_SCARCITY);
            }
        }

        // 刷新权限时间
        myRedisTemplate.set(RedisPreEnum.SYS_USER_PERMISSION.getCode().concat(loginName), userAuthorizeStr, Constants.Session.TIME_OUT, TimeUnit.MINUTES);
    }


}
