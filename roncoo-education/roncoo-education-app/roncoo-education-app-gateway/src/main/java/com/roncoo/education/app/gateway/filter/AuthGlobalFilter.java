package com.roncoo.education.app.gateway.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

/**
 * @author wujing
 */
@Slf4j
@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 优先级，order越大，优先级越低
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 1;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String uri = request.getPath().value();
        if (!FilterUtil.checkUri(uri, "/auth")) {
            // /auth为需要鉴权的接口
            return chain.filter(exchange);
        }
        // 解析token
        DecodedJWT jwt = FilterUtil.getDecodedJWT(request);
        Long userNo = getUserNoByToken(jwt);
        request.mutate().header(Constants.TK.USER_NO, userNo.toString()).header("HTTP_X_FORWARDED_FOR", FilterUtil.getIpAddr(exchange.getRequest().getHeaders().getFirst("X-Forwarded-For")));
        return chain.filter(exchange);
    }

    /**
     * token 拦截功能
     */
    private Long getUserNoByToken(DecodedJWT jwt) {
        // 校验token
        if (null == jwt) {
            throw new BaseException(ResultEnum.TOKEN_ERROR);
        }
        Long userNo = JWTUtil.getUserNo(jwt);
        if (userNo <= 0) {
            throw new BaseException(ResultEnum.TOKEN_ERROR);
        }

        // 校验登录
        String type = JWTUtil.getType(jwt);
        if (Constants.Platform.PC.equals(type)) {
            // PC端请求
            // 单点登录处理，注意，登录的时候必须要放入缓存
            String tk = myRedisTemplate.get(type.concat(userNo.toString()));
            if (StringUtils.isEmpty(tk)) {
                throw new BaseException(ResultEnum.TOKEN_PAST);
            }
            // 禁止异地登录
            if (!jwt.getToken().equals(tk)) {
                // 不同则为不同的用户登录，这时候提示异地登录
                throw new BaseException(ResultEnum.REMOTE_ERROR);
            }
            // 更新时间，使token不过期
            myRedisTemplate.set(type.concat(userNo.toString()), jwt.getToken(), Constants.Session.TIME_OUT, TimeUnit.MINUTES);
        } else if (Constants.Platform.XCX.equals(type) || Constants.Platform.IOS.equals(type) || Constants.Platform.ANDROID.equals(type)) {
            // 小程序，iso，安卓端请求，判断是否登录过期，默认1个月
            if (!myRedisTemplate.hasKey(type.concat(userNo.toString()))) {
                throw new BaseException(ResultEnum.TOKEN_PAST);
            }
            // 更新时间，使token不过期
            myRedisTemplate.set(type.concat(userNo.toString()), jwt.getToken(), 30, TimeUnit.DAYS);
        } else {
            log.error("登录异常：未知类型={}", type);
            throw new BaseException(ResultEnum.TOKEN_ERROR);
        }
        return userNo;
    }

}
