package com.roncoo.education.common.aliyun;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 阿里云播放编码
 *
 * @author LYQ
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AliYunVodCode implements Serializable {

    private static final long serialVersionUID = 1817945121427607340L;

    /**
     * 用户编号
     */
    private Long userNo;

    /**
     * 课时id
     */
    private Long periodId;

    /**
     * 题目ID
     */
    private Long problemId;

    /**
     * 试卷ID
     */
    private Long examId;
}
