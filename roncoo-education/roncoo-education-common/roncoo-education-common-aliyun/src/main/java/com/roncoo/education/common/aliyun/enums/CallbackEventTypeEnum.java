package com.roncoo.education.common.aliyun.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 回调通知事件
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum CallbackEventTypeEnum {

    /**
     * 视频上传完成
     */
    FILE_UPLOAD_COMPLETE("FileUploadComplete", "视频上传完成"),

    /**
     * 图片上传完成
     */
    IMAGE_UPLOAD_COMPLETE("ImageUploadComplete", "图片上传完成"),

    /**
     * 视频单个清晰度转码完成
     */
    STREAM_TRANSCODE_COMPLETE("StreamTranscodeComplete", "视频单个清晰度转码完成"),

    /**
     * 视频全部清晰度转码完成
     */
    TRANSCODE_COMPLETE("TranscodeComplete", "视频全部清晰度转码完成"),

    /**
     * 视频截图完成
     */
    SNAPSHOT_COMPLETE("SnapshotComplete", "视频截图完成"),

    /**
     * 视频动图完成
     */
    DYNAMIC_IMAGE_COMPLETE("DynamicImageComplete", "视频动图完成"),

    /**
     * 直转点视频录制完成
     */
    ADD_LIVE_RECORD_VIDEO_COMPLETE("AddLiveRecordVideoComplete", "直转点视频录制完成"),

    /**
     * 直转点录制视频合成开始
     */
    LIVE_RECORD_VIDEO_COMPOSE_START("LiveRecordVideoComposeStart", "直转点录制视频合成开始"),

    /**
     * URL上传视频完成
     */
    UPLOAD_BY_URL_COMPLETE("UploadByURLComplete", "URL上传视频完成"),

    /**
     * 人工审核完成
     */
    CREATE_AUDIT_COMPLETE("CreateAuditComplete", "人工审核完成"),

    /**
     * 智能审核完成
     */
    AI_MEDIA_AUDIT_COMPLETE("AIMediaAuditComplete", "智能审核完成"),

    /**
     * 音视频分析完成
     */
    VIDEO_ANALYSIS_COMPLETE("VideoAnalysisComplete", "音视频分析完成"),

    /**
     * 视频DNA完成
     */
    AI_MEDIA_DNA_COMPLETE("AIMediaDNAComplete", "视频DNA完成"),

    /**
     * 多模态内容理解完成
     */
    AI_VIDEO_TAG_COMPLETE("AIVideoTagComplete", "多模态内容理解完成"),

    /**
     * 辅助媒资上传完成
     */
    ATTACHED_MEDIA_UPLOAD_COMPLETE("AttachedMediaUploadComplete", "辅助媒资上传完成"),

    /**
     * 媒体合成完成
     */
    PRODUCE_MEDIA_COMPLETE("ProduceMediaComplete", "媒体合成完成"),

    /**
     * 媒体删除完成
     */
    DELETE_MEDIA_COMPLETE("DeleteMediaComplete", "媒体删除完成"),

    /**
     * 媒资基础信息变更
     */
    MEDIA_BASE_CHANGE_COMPLETE("MediaBaseChangeComplete", "媒资基础信息变更");

    /**
     * 编码
     */
    private final String code;

    /**
     * 描述
     */
    private final String desc;
}
