package com.roncoo.education.common.aliyun;

import cn.hutool.crypto.SecureUtil;
import com.aliyun.vod.upload.impl.UploadImageImpl;
import com.aliyun.vod.upload.req.UploadImageRequest;
import com.aliyun.vod.upload.resp.UploadImageResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.kms.model.v20160120.GenerateDataKeyRequest;
import com.aliyuncs.kms.model.v20160120.GenerateDataKeyResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.vod.model.v20170321.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 阿里云点播工具类
 *
 * @author LYQ
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AliYunVodUtil {

    /**
     * 区域ID
     */
    private static final String REGION_ID = "cn-shanghai";


    /**
     * 获取视频上传地址和凭证
     *
     * @param request         请求参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 获取结果
     */
    public static CreateUploadVideoResponse createUploadVideo(CreateUploadVideoRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 刷新视频上传凭证
     *
     * @param videoId         视频ID
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 获取结果
     */
    public static RefreshUploadVideoResponse refreshUploadVideo(String videoId, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);

        RefreshUploadVideoRequest request = new RefreshUploadVideoRequest();
        request.setVideoId(videoId);

        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * URL批量拉取上传
     *
     * @param request         拉取参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 拉取结果
     */
    public static UploadMediaByURLResponse uploadMediaByUrl(UploadMediaByURLRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 获取视频播放地址
     *
     * @param request         获取参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 视频播放地址
     */
    public static GetPlayInfoResponse getPlayInfo(GetPlayInfoRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 查询视频转码摘要
     *
     * @param videoIds        视频ID，多个用英文逗号分隔，最多支持10个
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 转码摘要
     */
    public static GetTranscodeSummaryResponse getTranscodeSummary(String videoIds, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);

        GetTranscodeSummaryRequest request = new GetTranscodeSummaryRequest();
        request.setVideoIds(videoIds);

        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 提交媒体转码作业
     *
     * @param request         转码作业参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 提交结果
     */
    public static SubmitTranscodeJobsResponse submitTranscodeJobs(SubmitTranscodeJobsRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 获取辅助媒资上传地址和凭证
     *
     * @param request         获取参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 获取结果
     */
    public static CreateUploadAttachedMediaResponse createUploadAttachedMedia(CreateUploadAttachedMediaRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 创建数据秘钥--HLS加密时需要使用
     *
     * @param request         请求参数
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 数据秘钥
     */
    public static GenerateDataKeyResponse generateDataKey(GenerateDataKeyRequest request, String accessKeyId, String accessKeySecret) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        try {
            return client.getAcsResponse(request);
        } catch (ClientException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 回调参数验签
     *
     * @param request      回调请求
     * @param callbackUrl  回调地址
     * @param videoAuthKey 视频授权秘钥
     * @return 验签结果
     */
    public static Boolean verifyCallbackSign(HttpServletRequest request, String callbackUrl, String videoAuthKey) {
        String vodTimestamp = request.getHeader("X-VOD-TIMESTAMP");
        String sign = request.getHeader("X-VOD-SIGNATURE");

        // 签名
        String signParam = callbackUrl + "|" + vodTimestamp + "|" + videoAuthKey;
        String resultSign = SecureUtil.md5(signParam);

        // 验签
        return resultSign.equals(sign);
    }

    public static UploadImageResponse uploadWatermarkImage(String fileUrl, String accessKeyId, String accessKeySecret) {
        // 图片类型（必选）取值范围：default（默认)，cover（封面），watermark(水印)
        String imageType = "watermark";
        UploadImageRequest request = new UploadImageRequest(accessKeyId, accessKeySecret, imageType);

        /* 图片文件扩展名（可选）取值范围：png，jpg，jpeg */
        request.setImageExt("png");
        request.setInputStream(getImageStream(fileUrl));
        /* 图片标题（可选）长度不超过128个字节，UTF8编码 */
        //request.setTitle("图片标题");
        /* 图片标签（可选）单个标签不超过32字节，最多不超过16个标签，多个用逗号分隔，UTF8编码 */
        //request.setTags("标签1,标签2");
        /* 存储区域（可选）*/
        //request.setStorageLocation("out-4f3952f78c021*****013e7.oss-cn-shanghai.aliyuncs.com");
        /* 流式上传时，InputStream为必选，fileName为源文件名称，如:文件名称.png(可选)*/
        //request.setFileName("测试文件名称.png");
        /* 开启默认上传进度回调 */
        // request.setPrintProgress(true);
        /* 设置自定义上传进度回调 (必须继承 ProgressListener) */
        // request.setProgressListener(new PutObjectProgressListener());
        /* 点播服务接入点 */
        request.setApiRegionId(REGION_ID);
        /* ECS部署区域，如果与点播存储（OSS）区域相同，则自动使用内网上传文件至存储*/
        // request.setEcsRegionId("cn-shanghai");

        UploadImageImpl uploadImage = new UploadImageImpl();
        return uploadImage.upload(request);
    }

    /**
     * 初始化客户端
     *
     * @param accessKeyId     访问ID
     * @param accessKeySecret 访问秘钥
     * @return 访问客户端
     */
    private static DefaultAcsClient initVodClient(String accessKeyId, String accessKeySecret) {
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, accessKeyId, accessKeySecret);
        return new DefaultAcsClient(profile);
    }

    /**
     * 获取图片流
     *
     * @param url 图片路径
     * @return 图片流
     */
    private static InputStream getImageStream(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("GET");
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return connection.getInputStream();
            }
        } catch (IOException e) {
            log.error("获取水印图片失败, 图片路径{}", url);
            log.error("", e);
        }
        return null;
    }
}
