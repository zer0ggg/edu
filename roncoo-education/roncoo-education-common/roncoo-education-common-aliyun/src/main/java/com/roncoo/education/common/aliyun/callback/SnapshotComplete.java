package com.roncoo.education.common.aliyun.callback;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 视频截图完成
 * <p>
 * SubType=SpecifiedTime，表示通过 SubmitSnapshotJob 接口发起的截图
 *
 * @author LYQ
 */
public class SnapshotComplete implements Serializable {

    private static final long serialVersionUID = 7705939760740014270L;

    /**
     * 事件产生时间, 为UTC时间：yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 事件类型，固定为FileUploadComplete
     */
    private String eventType;

    /**
     * 视频ID
     */
    private String videoId;

    /**
     * 视频截图状态，取值：success(成功)，fail(失败)
     */
    private String status;

    /**
     * 截图子类型，取值:SpecifiedTime
     */
    private String subType;

    /**
     * 视频截图失败时，会有该字段表示出错代码
     */
    private String errorCode;

    /**
     * 视频截图失败时，会有该字段表示出错信息
     */
    private String errorMessage;

    /**
     * 封面图片地址，截图失败不会有该字段
     */
    private String coverUrl;

    /**
     * 用户自定义回调透传数据，详细请参考UserData
     */
    private String extend;

    /**
     * 截图列表
     */
    private List<String> snapshots;

    /**
     * 截图列表，截图失败不会有该字段
     */
    private List<SnapshotInfo> snapshotInfos;

    /**
     * 截图数据
     */
    @Data
    public static class SnapshotInfo {

        /**
         * 视频截图任务状态，取值：success(成功)，fail(失败)
         */
        private String status;

        /**
         * 截图类型：CoverSnapshot(封面截图)、NormalSnapshot(普通截图)、SpriteSnapshot(雪碧截图)
         */
        private String snapshotType;

        /**
         * 截图总数
         */
        private Long snapshotCount;

        /**
         * 截图名称格式，可使用OSS存储地址或CDN域名和该字段信息生成截图地址
         */
        private String snapshotFormat;

        /**
         * 截图地址规则，可根据规则生成截图地址 (推荐使用该字段生成截图地址)
         */
        private String snapshotRegular;

        /**
         * 截图列表
         */
        private List<String> snapshots;

        /**
         * 截图任务ID
         */
        private String jobId;
    }
}
