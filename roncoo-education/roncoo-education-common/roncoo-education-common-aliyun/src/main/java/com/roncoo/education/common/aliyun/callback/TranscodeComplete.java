package com.roncoo.education.common.aliyun.callback;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 视频转码完成
 *
 * @author LYQ
 */
@Data
public class TranscodeComplete implements Serializable {

    private static final long serialVersionUID = 2503965416715554525L;

    /**
     * 事件产生时间, 为UTC时间：yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 事件类型，固定为TranscodeComplete
     */
    private String eventType;

    /**
     * 视频ID
     */
    private String videoId;

    /**
     * 视频转码状态，只要有一路流转码成功即为成功，取值：success(成功)，fail(失败)
     */
    private String status;

    /**
     * 用户自定义回调透传数据
     */
    private String extend;

    /**
     * 视频流信息列表
     */
    private List<StreamInfo> streamInfos;

    /**
     * 视频流信息
     */
    @Data
    public static class StreamInfo {
        /**
         * 视频流转码状态，取值：success(成功)，fail(失败)
         */
        private String status;

        /**
         * 视频流码率，单位Kbps
         */
        private Integer bitrate;

        /**
         * 视频流清晰度定义, 取值：FD(流畅)，LD(标清)，SD(高清)，HD(超清)，OD(原画)，2K(2K)，4K(4K)，AUTO(自适应码流)
         */
        private String definition;

        /**
         * 视频流长度，单位秒
         */
        private Float duration;

        /**
         * 视频流是否加密流
         */
        private Boolean encrypt;

        /**
         * 视频流转码出错的时候，会有该字段表示出错代码
         */
        private String errorCode;

        /**
         * 视频流转码出错的时候，会有该字段表示出错信息
         */
        private String errorMessage;

        /**
         * 视频流的播放地址，不带鉴权的auth_key，如果开启了URL鉴权，则需要自己生成auth_key才能访问
         */
        private String fileUrl;

        /**
         * 视频流格式，取值：mp4, m3u8
         */
        private String format;

        /**
         * 视频流帧率，每秒多少帧
         */
        private String fps;

        /**
         * 视频流高度，单位px
         */
        private Long height;

        /**
         * 视频流大小，单位Byte
         */
        private Long size;

        /**
         * 视频流宽度，单位px
         */
        private Long width;

        /**
         * 转码作业ID
         */
        private String jobId;
    }
}
