package com.roncoo.education.common.aliyun.callback;

import lombok.Data;

import java.io.Serializable;

/**
 * 音视频分析完成
 * <p>
 * 视频点播会对上传完成的音视频源文件进行分析，完成后会产生此事件
 *
 * @author LYQ
 */
@Data
public class VideoAnalysisComplete implements Serializable {

    private static final long serialVersionUID = 3131744670398744843L;

    /**
     * 事件产生时间，为UTC时间:yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 时间类型，固定为VideoAnalysisComplete
     */
    private String eventType;

    /**
     * 音视频ID
     */
    private String videoId;

    /**
     * 分析源文件结果状态，取值：success(成功)，fail(失败)
     */
    private String status;

    /**
     * 源文件宽 ，源片为纯音频不会该字段
     */
    private Long width;

    /**
     * 源文件高 ，源片为纯音频不会有该字段
     */
    private Long height;

    /**
     * 源文件时长 ，单位:秒
     */
    private Float duration;

    /**
     * 源文件码率 ，单位:Kbps
     */
    private String bitrate;

    /**
     * 源文件帧率 ，每秒多少帧，源片为纯音频不会有该字段
     */
    private String fps;

    /**
     * 源文件大小，单位: Byte(字节)
     */
    private Long size;

    /**
     * 如果分析源文件信息失败，会有该字段
     */
    private String errorCode;

    /**
     * 如果分析源文件信息失败，会有该字段
     */
    private String errorMessage;
}
