package com.roncoo.education.common.aliyun.callback;

import lombok.Data;

import java.io.Serializable;

/**
 * 视频上传完成
 *
 * @author LYQ
 */
@Data
public class FileUploadComplete implements Serializable {

    private static final long serialVersionUID = 7531407852801353562L;

    /**
     * 事件产生时间, 为UTC时间：yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 事件类型，固定为FileUploadComplete
     */
    private String eventType;

    /**
     * 视频ID
     */
    private String videoId;

    /**
     * 上传的文件大小，单位：Byte(字节)
     */
    private Long size;

    /**
     * 上传文件的Url地址
     */
    private String fileUrl;
}
