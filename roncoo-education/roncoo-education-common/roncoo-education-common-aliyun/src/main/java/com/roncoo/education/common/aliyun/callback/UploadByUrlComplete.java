package com.roncoo.education.common.aliyun.callback;

import lombok.Data;

import java.io.Serializable;

/**
 * URL上传视频完成
 *
 * @author LYQ
 */
@Data
public class UploadByUrlComplete implements Serializable {

    private static final long serialVersionUID = -8630951410058149412L;

    /**
     * 事件产生时间, 为UTC时间：yyyy-MM-ddTHH:mm:ssZ
     */
    private String eventTime;

    /**
     * 事件类型，固定为UploadByURLComplete
     */
    private String eventType;

    /**
     * 视频ID，当视频拉取成功后会有该字段
     */
    private String videoId;

    /**
     * 上传任务ID
     */
    private String jobId;

    /**
     * 源文件URL地址
     */
    private String sourceURL;

    /**
     * 处理结果，取值：success(成功)，fail(失败)
     */
    private String status;

    /**
     * 失败错误码，当Status为fail时会有该字段
     */
    private String errorCode;

    /**
     * 失败错误信息，当Status为fail时会有该字段
     */
    private String errorMessage;


}
