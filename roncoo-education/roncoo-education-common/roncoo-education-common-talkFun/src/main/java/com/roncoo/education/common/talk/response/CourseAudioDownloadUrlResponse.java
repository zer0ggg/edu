package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 获取音频下载地址
 *
 * @author LYQ
 */
@Data
public class CourseAudioDownloadUrlResponse implements Serializable {

    private static final long serialVersionUID = -7302347892689705315L;

    /**
     * 音频下载地址
     */
    @JSONField(name = "urls")
    private String url;
}
