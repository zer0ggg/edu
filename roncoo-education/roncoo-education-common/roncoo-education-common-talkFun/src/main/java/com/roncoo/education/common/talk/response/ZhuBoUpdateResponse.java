package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LYQ
 */
@Data
public class ZhuBoUpdateResponse implements Serializable {

    private static final long serialVersionUID = -4575559915946039333L;

    /**
     * 欢拓主播账户ID
     */
    private String bid;

    /**
     * 	合作方id
     */
    @JSONField(name = "partner_id")
    private Integer partnerId;

    /**
     * 发起直播课程的合作方主播唯一账号或ID
     */
    private String thirdAccount;

    /**
     * 主播简介
     */
    private String intro;

    /**
     * 	头像：150x150
     */
    @JSONField(name = "p_150")
    private String p150;

    /**
     * 头像：40x40
     */
    @JSONField(name = "p_40")
    private String p40;
}
