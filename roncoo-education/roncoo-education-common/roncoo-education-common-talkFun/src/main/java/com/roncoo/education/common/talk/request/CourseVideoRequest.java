package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 获取直播回放视频
 *
 * @author LYQ
 */
@Data
public class CourseVideoRequest implements Serializable {

    private static final long serialVersionUID = 6588783155563084099L;

    /**
     * 直播id--必填
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 其它可选项--非必填
     */
    @JSONField(name = "options")
    private Option option;

    @Data
    public static class Option {

        /**
         * 是否返回视频下载地址：true 返回下载地址，false 返回观看地址
         */
        private Boolean download;

        /**
         * 是否获取额外信息：0 否，1 是
         */
        @JSONField(name = "getext")
        private Integer geText;
    }
}
