package com.roncoo.education.common.talk.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 更新主播信息
 *
 * @author LYQ
 */
@Data
public class ZhuBoUpdateRequest implements Serializable {

    private static final long serialVersionUID = 5626211244110184202L;

    /**
     * 发起直播课程的合作方主播唯一账号或ID--必填
     */
    private String account;

    /**
     * 主播昵称--必填
     */
    private String nickname;

    /**
     * 主播简介
     */
    private String intro;

    /**
     * 主播密码
     */
    private String password;
}
