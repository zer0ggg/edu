package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户授权
 *
 * @author LYQ
 */
@Data
public class CourseAccessResponse implements Serializable {

    private static final long serialVersionUID = -83372282342640960L;

    /**
     * 回放地址
     */
    private String playbackUrl;

    /**
     * 直播地址
     */
    private String liveUrl;

    /**
     * 直播视频外链地址
     */
    private String liveVideoUrl;

    /**
     * 用户的access_token
     */
    @JSONField(name = "access_token")
    private String accessToken;

    /**
     * 回放纯视频播放地址
     */
    private String playbackOutUrl;

    /**
     * 小程序web-view的直播或回放地址（未传miniprogramAppid参数时返回默认域名的直播或回放地址）
     */
    @JSONField(name = "miniprogramUrl")
    private String miniProgramUrl;

    /**
     * 手机端管理地址（role角色spadmin时返回）
     */
    private String adminMiniUrl;
}
