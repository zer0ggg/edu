package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 查询课程信息
 *
 * @author LYQ
 */
@Data
public class CourseGetResponse implements Serializable {

    private static final long serialVersionUID = 3044641482881225113L;

    /**
     * 课程id
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 合作方id
     */
    @JSONField(name = "partner_id")
    private Integer partnerId;

    /**
     * 课程名
     */
    @JSONField(name = "course_name")
    private String courseName;

    /**
     * 欢拓系统的主播id
     */
    private Integer bid;

    /**
     * 课程开始时间，时间戳，精确到秒，下同
     */
    @JSONField(name = "start_time")
    private Date startTime;

    /**
     * 课程结束时间
     */
    @JSONField(name = "end_time")
    private Date endTime;

    /**
     * 课程创建时间
     */
    @JSONField(name = "add_time")
    private Date addTime;

    /**
     * 状态： 0 正常，-1 已删除
     */
    private Integer status;

    /**
     * 直播开始时间
     */
    @JSONField(name = "live_stime")
    private Integer liveSTime;

    /**
     * 直播结束时间
     */
    @JSONField(name = "live_etime")
    private Integer liveETime;

    /**
     * 时长(秒)
     */
    private Integer duration;

    /**
     * 聊天总数
     */
    private Integer chatTotal;

    /**
     * 主播登录秘钥
     */
    @JSONField(name = "zhubo_key")
    private String zhuBoKey;

    /**
     * 助教登录秘钥
     */
    @JSONField(name = "admin_key")
    private String adminKey;

    /**
     * 学生登录秘钥
     */
    @JSONField(name = "user_key")
    private String userKey;

    /**
     * 问题总数
     */
    private Integer questionTotal;

    /**
     * 投票总数
     */
    private Integer voteTotal;

    /**
     * 鲜花总数
     */
    private Integer flowerTotal;

    /**
     * 抽奖总数
     */
    private Integer lotteryTotal;

    /**
     * 直播观看次数
     */
    private Integer livePv;

    /**
     * 回放观看次数
     */
    private Integer pbPv;

    /**
     * 直播观看人数
     */
    private Integer liveUv;

    /**
     * 回放观看人数
     */
    private Integer pbUv;

    /**
     * 主播信息
     */
    @JSONField(name = "zhubo")
    private ZhuBo zhuBo;

    /**
     * 直播状态：1 未开始；2 正在直播；3 已结束
     */
    private Integer liveStatus;

    /**
     * 机器人数量
     */
    private Integer robotTotal;

    /**
     * 是否生成回放，0为未生成，1为已生成
     */
    private Integer playback;

    /**
     * 回放地址
     */
    private String playbackUrl;

    /**
     * 回放大小(如果传了getsize)
     */
    private Integer filesize;

    /**
     * 直播类型。1: 教育直播，2: 生活直播
     */
    private Integer scenes;

    /**
     * 商品列表，scenes为2时返回
     */
    private List<Goods> goodsList;

    /**
     * 观众地址，scenes为2时返回
     */
    private String watchUrl;

    /**
     * 登陆地址，scenes为2时返回
     */
    private String loginUrl;

    /**
     * 主播信息
     */
    @Data
    public static class ZhuBo {

        /**
         * 欢拓系统的主播id
         */
        private String bid;

        /**
         * 合作方id
         */
        @JSONField(name = "partner_id")
        private String partnerId;

        /**
         * 发起直播课程的合作方主播唯一账号或ID
         */
        private String thirdAccount;

        /**
         * 主播昵称
         */
        private String nickname;

        /**
         * 主播介绍
         */
        private String intro;

        /**
         * 主播缩略图150x150
         */
        @JSONField(name = "p_150")
        private String p150;

        /**
         * 主播缩略图40x40
         */
        @JSONField(name = "p_40")
        private String p40;
    }

    /**
     * 商品列表
     */
    @Data
    public static class Goods {

        /**
         * 商品名称
         */
        private String name;

        /**
         * 商品图片地址
         */
        private String img;

        /**
         * 商品现价
         */
        private String price;

        /**
         * 商品原价
         */
        private String originalPrice;

        /**
         * 商品标签(特价:1, 限时:2, 新品:3, 钜惠:4, 秒杀:5)
         */
        private String tab;

        /**
         * 商品链接
         */
        private String url;

        /**
         * 商品是否上架(0 OR 1)
         */
        @JSONField(name = "putaway")
        private String putAway;

    }
}
