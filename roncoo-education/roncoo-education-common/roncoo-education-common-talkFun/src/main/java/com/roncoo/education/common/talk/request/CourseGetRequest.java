package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询课程信息
 *
 * @author LYQ
 */
@Data
public class CourseGetRequest implements Serializable {

    private static final long serialVersionUID = 8695724451549777728L;

    /**
     * 课程id--必填
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 返回的回放地址的过期时间，单位秒。默认 86400 秒--非必填
     */
    private Integer expire;

    /**
     * 附加参数--非必填
     */
    @JSONField(name = "options")
    private Option option;

    @Data
    public static class Option {

        /**
         * 是否获取回放大小：0 否，1 是
         */
        @JSONField(name = "getsize")
        private Integer getSize;
    }
}
