package com.roncoo.education.common.talk.callback;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 回调结果
 *
 * @author LYQ
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CallbackResult<T> implements Serializable {

    private static final long serialVersionUID = 5738087441866473967L;

    /**
     * 状态值：0表示操作成功；其它值表示失败
     */
    private Integer code;

    /**
     * 状态说明
     */
    private String msg;

    /**
     * 返回具体信息
     */
    private T data;

    public static <T> CallbackResult<T> success() {
        return new CallbackResult<>(0, "操作成功", null);
    }

    public static <T> CallbackResult<T> success(String msg) {
        return new CallbackResult<>(0, msg, null);
    }

    public static <T> CallbackResult<T> success(T data) {
        return new CallbackResult<>(0, "操作成功", data);
    }

    public static <T> CallbackResult<T> success(String msg, T data) {
        return new CallbackResult<>(0, msg, data);
    }

    public static <T> CallbackResult<T> error() {
        return new CallbackResult<>(999, "操作失败", null);
    }

    public static <T> CallbackResult<T> error(String msg) {
        return new CallbackResult<>(999, msg, null);
    }
}
