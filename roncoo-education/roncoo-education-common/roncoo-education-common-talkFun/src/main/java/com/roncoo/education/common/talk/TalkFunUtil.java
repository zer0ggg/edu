package com.roncoo.education.common.talk;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.common.talk.request.*;
import com.roncoo.education.common.talk.response.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author LYQ
 */
@Slf4j
public final class TalkFunUtil {

    private static final String REQUEST_URL = "http://api.talk-fun.com/portal.php";

    /**
     * 添加直播课程
     *
     * @param request   请求参数
     * @param openId    openId
     * @param openToken openToken
     * @return 添加直播课程响应
     */
    public static CourseAddResponse courseAdd(CourseAddRequest request, String openId, String openToken) {
        CommonResponse response = request("course.add", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), CourseAddResponse.class);
        } else if (StrUtil.isNotBlank(response.getMsg())) {
            throw new BaseException(response.getMsg());
        }
        return null;
    }

    /**
     * 更新课程信息
     *
     * @param request   请求参数
     * @param openId    openId
     * @param openToken openToken
     * @return 更新课程信息响应
     */
    public static Boolean courseUpdate(CourseUpdateRequest request, String openId, String openToken) {
        CommonResponse response = request("course.update", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        return response.isSuccess();
    }

    /**
     * 删除课程信息
     *
     * @param courseId  课程id
     * @param openId    openId
     * @param openToken openToken
     * @return 删除课程信息响应
     */
    public static Boolean courseDelete(Integer courseId, String openId, String openToken) {
        HashMap<Object, Object> paramMap = new HashMap<>();
        paramMap.put("course_id", courseId);

        CommonResponse response = request("course.delete", openId, openToken, JSON.toJSONString(paramMap), false, REQUEST_URL);
        return response.isSuccess();
    }

    /**
     * 获取直播器启动协议
     *
     * @param courseId  课程id
     * @param openId    openId
     * @param openToken openToken
     * @return 获取直播器启动协议响应
     */
    public static CourseLaunchResponse courseLaunch(Integer courseId, String openId, String openToken) {
        HashMap<Object, Object> paramMap = new HashMap<>();
        paramMap.put("course_id", courseId);

        CommonResponse response = request("course.launch", openId, openToken, JSON.toJSONString(paramMap), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), CourseLaunchResponse.class);
        }
        return null;
    }

    /**
     * 用户授权
     *
     * @param request   授权参数
     * @param openId    openId
     * @param openToken openToken
     * @return 用户授权响应
     */
    public static CourseAccessResponse courseAccess(CourseAccessRequest request, String openId, String openToken) {
        CommonResponse response = request("course.access", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), CourseAccessResponse.class);
        }
        return null;
    }

    /**
     * 获取音频下载地址
     *
     * @param courseId  课程Id
     * @param openId    openId
     * @param openToken openToken
     * @return 获取音频下载地址响应
     */
    public static CourseAudioDownloadUrlResponse courseAudioDownloadUrl(Integer courseId, String openId, String openToken) {
        HashMap<Object, Object> paramMap = new HashMap<>();
        paramMap.put("course_id", courseId);
        CommonResponse response = request("course.audio.download.url", openId, openToken, JSON.toJSONString(paramMap), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), CourseAudioDownloadUrlResponse.class);
        }
        return null;
    }


    /**
     * 获取直播回放地址
     *
     * @param request   回放参数
     * @param openId    openId
     * @param openToken openToken
     * @return 直播回放响应
     */
    public static CourseAccessPlaybackResponse courseAccessPlayback(CourseAccessPlaybackRequest request, String openId, String openToken) {
        CommonResponse response = request("course.access.playback", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            CourseAccessPlaybackResponse getResponse = JSON.parseObject(response.getData(), CourseAccessPlaybackResponse.class);
            return getResponse;
        }
        return null;
    }

    /**
     * 获取直播回放视频
     *
     * @param request   回放参数
     * @param openId    openId
     * @param openToken openToken
     * @return 直播回放响应
     */
    public static List<String> courseVideo(CourseVideoRequest request, String openId, String openToken) {
        CommonResponse response = request("course.video", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseArray(response.getData(), String.class);
        }
        return null;
    }

    /**
     * 查询课程信息
     *
     * @param request   查询参数
     * @param openId    openId
     * @param openToken openToken
     * @return 课程信息
     */
    public static CourseGetResponse courseGet(CourseGetRequest request, String openId, String openToken) {
        CommonResponse response = request("course.get", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            CourseGetResponse getResponse = JSON.parseObject(response.getData(), CourseGetResponse.class);
            // 对秒级时间处理
            JSONObject courseJson = JSON.parseObject(response.getData());
            getResponse.setStartTime(secondTimestampToDate(courseJson.getLong("start_time")));
            getResponse.setEndTime(secondTimestampToDate(courseJson.getLong("end_time")));
            getResponse.setAddTime(secondTimestampToDate(courseJson.getLong("add_time")));
            return getResponse;
        }
        return null;
    }

    /**
     * 查询课程推流地址
     *
     * @param courseId 课程ID
     */
    public static CoursePushRtmpUrlResponse coursePushRtmpUrl(Integer courseId, String openId, String openToken) {
        HashMap<Object, Object> paramMap = new HashMap<>();
        paramMap.put("course_id", courseId);

        CommonResponse response = request("course.pushRtmpUrl", openId, openToken, JSON.toJSONString(paramMap), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), CoursePushRtmpUrlResponse.class);
        }
        return null;
    }

    /**
     * 更新主播信息
     *
     * @param request   更新请求参数
     * @param openId    openId
     * @param openToken openToken
     * @return 更新结果
     */
    public static ZhuBoUpdateResponse zhuBoUpdate(ZhuBoUpdateRequest request, String openId, String openToken) {
        CommonResponse response = request("course.zhubo.update", openId, openToken, JSON.toJSONString(request), false, REQUEST_URL);
        if (response.isSuccess()) {
            return JSON.parseObject(response.getData(), ZhuBoUpdateResponse.class);
        }
        return null;
    }

    /**
     * 修改主播头像
     *
     * @param account   主播账号（第三方平台账号）
     * @param file      头像文件
     * @param openId    openId
     * @param openToken openToken
     * @return 修改结果
     */
    public static ZhuBoPortraitResponse zhuBoPortrait(String account, File file, String openId, String openToken) {
        HashMap<Object, Object> params = new HashMap<>();
        params.put("account", account);
        CommonResponse response = request("course.zhubo.portrait", openId, openToken, JSON.toJSONString(params), false, REQUEST_URL);

        if (!response.isSuccess()) {
            return null;
        }

        try {
            JSONObject resultData = JSON.parseObject(response.getData());
            Part[] parts = {
                    new FilePart(resultData.getString("field"), file)
            };
            CommonResponse uploadResult = doPost(resultData.getString("api"), parts);
            if (ObjectUtil.isNotNull(uploadResult) && uploadResult.isSuccess()) {
                return JSON.parseObject(uploadResult.getData(), ZhuBoPortraitResponse.class);
            }
            return null;
        } catch (FileNotFoundException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 获取课程登录页面（讲师/助教）
     *
     * @param courseId 课程ID
     * @return 登录页面
     */
    public static String getLoginUrl(Integer courseId) {
        return String.format("http://live.talk-fun.com/live/courseLogin.php?course_id=%s", courseId);
    }

    /**
     * 请求
     *
     * @param cmd         API名称
     * @param openId      openId
     * @param openToken   openToken
     * @param params      请求参数
     * @param postRequest 是否为POST请求
     * @param url         请求地址
     * @return 请求结果
     */
    private static CommonResponse request(String cmd, String openId, String openToken, String params, Boolean postRequest, String url) {
        try {
            Map<String, Object> paramMap = new HashMap<>(7);
            paramMap.put("openID", openId);
            paramMap.put("timestamp", System.currentTimeMillis() / 1000);
            paramMap.put("ver", "java.1.4");
            paramMap.put("format", "json");
            paramMap.put("cmd", cmd);
            paramMap.put("params", URLEncoder.encode(params, StandardCharsets.UTF_8.name()));
            // 签名
            paramMap.put("sign", sign(paramMap, openToken));

            String result;
            if (postRequest) {
                result = doPost(url, paramMap);
            } else {
                result = doGet(url, paramMap);
            }
            log.debug("请求返回报文：{}", result);
            return JSON.parseObject(result, CommonResponse.class);
        } catch (UnsupportedEncodingException e) {
            log.error("", e);
            return new CommonResponse();
        }
    }

    /**
     * POST请求
     *
     * @param url      请求地址
     * @param paramMap 请求参数
     * @return 请求结果
     */
    private static String doPost(String url, Map<String, Object> paramMap) {
        // 拼装请求参数
        Object[] array = paramMap.keySet().toArray();
        Part[] parts = new Part[array.length];
        try {
            for (int i = 0; i < array.length; i++) {
                String key = array[i].toString();

                String value = String.valueOf(paramMap.get(key));
                if (0 == value.indexOf("@")) {
                    File f = new File(value.substring(1));
                    parts[i] = new FilePart(key, f);
                } else {
                    parts[i] = new StringPart(key, value);
                }
            }
        } catch (FileNotFoundException e) {
            log.error("", e);
            return null;
        }

        PostMethod postMethod = new PostMethod(url);
        postMethod.addRequestHeader("User-Agent", "MT-JAVA-SDK");
        postMethod.setRequestEntity(
                new MultipartRequestEntity(parts, postMethod.getParams())
        );

        HttpClient httpClient = new HttpClient();
        try {
            httpClient.executeMethod(postMethod);

            InputStream jsonStr = postMethod.getResponseBodyAsStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            int i;
            while ((i = jsonStr.read()) != -1) {
                outputStream.write(i);
            }

            jsonStr.close();
            outputStream.close();
            postMethod.releaseConnection();

            return new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * POST请求
     *
     * @param url   请求地址
     * @param parts 请求参数
     * @return 请求结果
     */
    private static CommonResponse doPost(String url, Part[] parts) {
        PostMethod filePost = new PostMethod(url);
        filePost.addRequestHeader("User-Agent", "MT-JAVA-SDK");

        filePost.setRequestEntity(
                new MultipartRequestEntity(parts, filePost.getParams())
        );
        HttpClient client = new HttpClient();
        try {
            client.executeMethod(filePost);
            InputStream jsonStr;
            jsonStr = filePost.getResponseBodyAsStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            int i;
            while ((i = jsonStr.read()) != -1) {
                outputStream.write(i);
            }

            jsonStr.close();
            outputStream.close();
            filePost.releaseConnection();
            String result = new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
            log.debug("请求返回报文：{}", result);
            return JSON.parseObject(result, CommonResponse.class);
        } catch (IOException e) {
            log.error("", e);
            return null;

        }
    }

    /**
     * Get请求
     *
     * @param url      请求地址
     * @param paramMap 请求参数
     * @return 请求结果
     */
    private static String doGet(String url, Map<String, Object> paramMap) {
        // 拼接请求路径
        StringBuilder requestUrl = new StringBuilder(url);
        requestUrl.append("?");
        try {
            for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
                requestUrl.append(entry.getKey()).append("=").append(URLEncoder.encode(String.valueOf(entry.getValue()), StandardCharsets.UTF_8.name())).append("&");
            }
        } catch (UnsupportedEncodingException e) {
            log.error("", e);
        }
        requestUrl.deleteCharAt(requestUrl.length() - 1);

        GetMethod getMethod = new GetMethod(requestUrl.toString());
        getMethod.addRequestHeader("User-Agent", "MT-JAVA-SDK");

        //实例化httpClient
        HttpClient httpClient = new HttpClient();
        try {
            //执行
            httpClient.executeMethod(getMethod);

            InputStream jsonStr = getMethod.getResponseBodyAsStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            int i;
            while ((i = jsonStr.read()) != -1) {
                outputStream.write(i);
            }

            jsonStr.close();
            outputStream.close();
            getMethod.releaseConnection();

            return new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 签名
     *
     * @param paramMap  签名参数
     * @param openToken 签名token
     * @return 签名结果
     */
    public static String sign(Map<String, Object> paramMap, String openToken) {
        if (paramMap == null || paramMap.isEmpty()) {
            return null;
        }

        SortedMap<String, Object> sortedMap = new TreeMap<>(paramMap);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> entry : sortedMap.entrySet()) {
            if (!"sign".equals(entry.getKey())) {
                sb.append(entry.getKey()).append(entry.getValue());
            }
        }
        log.debug("签名原文：{}", sb.toString());
        sb.append(openToken);
        return MD5Util.MD5(sb.toString());
    }

    private static Date secondTimestampToDate(Long timestamp) {
        return new Date(timestamp * 1000L);
    }
}
