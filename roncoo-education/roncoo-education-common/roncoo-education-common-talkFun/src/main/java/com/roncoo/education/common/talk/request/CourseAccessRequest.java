package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户授权
 * 直播和回放的授权接口是分开的，不能用直播的地址或access_token进入回放，反之也不行
 *
 * @author LYQ
 */
@Data
public class CourseAccessRequest implements Serializable {

    private static final long serialVersionUID = -6936528601841496647L;

    /**
     * 课程ID--必填
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 接入方用户唯一ID。uid只能包含字母、数字、_、-、*、:，最长32位--必填
     */
    private String uid;

    /**
     * 用户昵称--必填
     */
    private String nickname;

    /**
     * 用户身份(user/admin/guest/watch，分别对应普通用户/管理员(助教)/游客/直播监课)--必填
     */
    private String role;

    /**
     * 有效期,默认:3600(单位:秒)--非必填
     */
    private Integer expire;

    /**
     * 其它可选项--非必填
     */
    @JSONField(name = "options")
    private Option option;

    @Data
    public static class Option {

        /**
         * 性别(1为男性，2为女性)
         */
        private Integer gender;

        /**
         * 头像链接地址(默认头像可不传，由欢拓来配置)
         */
        private String avatar;

        /**
         * 用户分组ID，范围 0-255，默认为0，表示不分组；不同分组的用户之间互相隔离(包括管理员)，看不到对方的聊天及在线情况
         */
        private Integer gid;

        /**
         * 是否使用https(true为使用，false为不使用)
         */
        private Boolean ssl;

        /**
         * 限制授权链接或token打开次数，为大于0数字，仅回放可用；默认0，为不限制
         */
        private Integer times;

        /**
         * 小程序ID，用来确定小程序的业务域名
         */
        @JSONField(name = "miniprogramAppid")
        private String miniProgramAppId;
    }
}
