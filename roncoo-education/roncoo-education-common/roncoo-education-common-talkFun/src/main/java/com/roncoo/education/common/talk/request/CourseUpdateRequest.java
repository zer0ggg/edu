package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LYQ
 */
@Data
public class CourseUpdateRequest implements Serializable {

    private static final long serialVersionUID = -4624519046858906112L;

    /**
     * 课程id--必填
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 接入方主播账号或ID或手机号等--必填
     */
    private String account;

    /**
     * 课程名称--必填
     */
    @JSONField(name = "course_name")
    private String courseName;

    /**
     * 课程开始时间--必填
     */
    @JSONField(name = "start_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 课程结束时间--必填
     */
    @JSONField(name = "end_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 主播的昵称--必填
     */
    private String nickname;

    /**
     * 主播的简介--非必填
     */
    private String accountIntro;

    /**
     * 其它可选参数--非必填
     */
    @JSONField(name = "options")
    private Option option;

    @Data
    public static class Option {

        /**
         * 是否开启弹幕：0 关闭，1 开启
         */
        private Integer barrage;

        /**
         * 虚拟用户数量
         */
        private Integer robotNumber;

        /**
         * 虚拟用户昵称类型：0使用真实姓名，1使用网络昵称
         */
        private Integer robotType;

        /**
         * 模式： 1 语音云，3 大班(默认)，5 小班, 6:大班互动)
         */
        @JSONField(name = "modetype")
        private Integer modeType;
    }
}
