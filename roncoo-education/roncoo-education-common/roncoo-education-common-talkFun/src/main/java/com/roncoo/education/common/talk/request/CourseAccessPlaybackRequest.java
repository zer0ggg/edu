package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 获取直播回放视频
 *
 * @author KYH
 */
@Data
public class CourseAccessPlaybackRequest implements Serializable {

    private static final long serialVersionUID = 6588783155563084099L;

    /**
     * 直播id--必填
     */
    @JSONField(name = "course_id")
    private Integer courseId;

    /**
     * 合作方系统内的用户的唯一ID--必填
     */
    @JSONField(name = "uid")
    private String uid;

    /**
     * 填入用户的昵称--必填
     */
    @JSONField(name = "nickname")
    private String nickname;
    /**
     * 用户角色--必填
     */
    @JSONField(name = "role")
    private String role;

    /**
     * 到期时间--必填
     */
    @JSONField(name = "expire")
    private String expire;

    /**
     * 其它可选项--非必填
     */
    @JSONField(name = "options")
    private Option option;

    @Data
    public static class Option {

        /**
         * 用户头像地址
         */
        @JSONField(name = "avatar")
        private Boolean avatar;

        /**
         * 不限制打开次数
         */
        @JSONField(name = "times")
        private Integer times = 0;
    }
}
