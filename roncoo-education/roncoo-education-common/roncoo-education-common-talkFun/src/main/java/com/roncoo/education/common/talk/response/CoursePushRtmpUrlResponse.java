package com.roncoo.education.common.talk.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询课程推流地址
 *
 * @author LYQ
 */
@Data
public class CoursePushRtmpUrlResponse implements Serializable {

    private static final long serialVersionUID = 7569048955688368593L;

    /**
     * 	视频宽高比
     */
    private String wh;

    /**
     * 外部推流地址
     */
    private String pushAddr;
}
