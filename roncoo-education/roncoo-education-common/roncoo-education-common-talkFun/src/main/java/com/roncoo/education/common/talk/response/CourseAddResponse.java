package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LYQ
 */
@Data
public class CourseAddResponse implements Serializable {

    private static final long serialVersionUID = 5809136323382934937L;

    /**
     * 合作方id
     */
    @JSONField(name = "partner_id")
    private Integer partnerId;

    /**
     * 欢拓系统的主播id
     */
    private Integer bid;

    /**
     * 课程名称
     */
    @JSONField(name = "course_name")
    private String courseName;

    /**
     * 开始时间戳
     */
    @JSONField(name = "start_time")
    private Date startTime;

    /**
     * 结束时间戳
     */
    @JSONField(name = "end_time")
    private Date endTime;

    /**
     * 主播登录秘钥
     */
    @JSONField(name = "zhubo_key")
    private String zhuBoKey;

    /**
     * 助教登录秘钥
     */
    @JSONField(name = "admin_key")
    private String adminKey;

    /**
     * 学生登录秘钥
     */
    @JSONField(name = "user_key")
    private String userKey;

    /**
     * 课程创建时间
     */
    @JSONField(name = "add_time")
    private Date addTime;

    /**
     * 课程id
     */
    @JSONField(name = "course_id")
    private Integer courseId;
}
