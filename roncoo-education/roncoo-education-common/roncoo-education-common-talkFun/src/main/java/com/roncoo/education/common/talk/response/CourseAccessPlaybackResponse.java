package com.roncoo.education.common.talk.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 获取直播回放视频
 *
 * @author KYH
 */
@Data
public class CourseAccessPlaybackResponse implements Serializable {

    private static final long serialVersionUID = 6588783155563084099L;

    /**
     * access_token 用于定制页面输出页面给JS调用
     */
    @JSONField(name = "access_token")
    private String accessToken;
    /**
     * 直接进入回放的地址
     */
    @JSONField(name = "playbackUrl")
    private String playbackUrl;



}
