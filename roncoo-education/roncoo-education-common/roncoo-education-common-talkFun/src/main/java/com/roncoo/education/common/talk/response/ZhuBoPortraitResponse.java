package com.roncoo.education.common.talk.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 修改主播头像
 *
 * @author LYQ
 */
@Data
public class ZhuBoPortraitResponse implements Serializable {

    private static final long serialVersionUID = 1117491015462525544L;

    /**
     * 主播ID
     */
    private String bid;

    /**
     * 图片路径(150_150、40_40)
     */
    private Map<String, Object> portraitUrl;
}
