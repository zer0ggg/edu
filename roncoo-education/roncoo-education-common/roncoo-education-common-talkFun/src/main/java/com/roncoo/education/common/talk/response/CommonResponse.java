package com.roncoo.education.common.talk.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用返回
 *
 * @author LYQ
 */
@Data
public class CommonResponse implements Serializable {

    private static final long serialVersionUID = -8149392694492686951L;

    /**
     * code=0为成功，其他值为失败
     */
    private Integer code;

    /**
     * 状态说明
     */
    private String msg;

    /**
     * 响应数据
     */
    private String data;

    /**
     * 判断是否成功
     *
     * @return 判断结果
     */
    public Boolean isSuccess() {
        return this.code == 0;
    }
}
