package com.roncoo.education.common.talk.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 获取直播器启动协议
 * 启动地址、协议有效期为14天，过期需要重新获取，最好是在主播操作的时候再获取，马上使用
 *
 * @author LYQ
 */
@Data
public class CourseLaunchResponse implements Serializable {

    private static final long serialVersionUID = 7898386333725642730L;

    /**
     * 登录页面地址
     */
    private String url;

    /**
     * 管理员登录页面地址
     */
    private String spUrl;

    /**
     * Windows启动协议，链接地址为协议内容，放在网页链接里面，点击可以启动直播器
     */
    private String protocol;

    /**
     * Mac启动协议，链接地址为协议内容，放在网页链接里面，点击可以启动直播器
     */
    private String protocolMac;

    /**
     * Win直播器下载地址
     */
    private String download;

    /**
     * Mac直播器下载地址
     */
    private String downloadMac;

    /**
     * 登录验证token，主播直播器终端（Windows,Mac,App,网页）验证自动登录用
     */
    private String token;

    /**
     * 管理员登录验证token，主播直播器终端（Windows,Mac,App,网页）验证自动登录用
     */
    private String spToken;
}
