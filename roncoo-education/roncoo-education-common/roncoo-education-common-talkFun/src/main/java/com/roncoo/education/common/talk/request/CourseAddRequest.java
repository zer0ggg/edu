package com.roncoo.education.common.talk.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 课程添加
 *
 * @author LYQ
 */
@Data
public class CourseAddRequest implements Serializable {

    private static final long serialVersionUID = 3852199646892577890L;

    /**
     * 课程名称--必填
     */
    @JSONField(name = "course_name")
    private String courseName;

    /**
     * 接入方主播账号或ID或手机号等，最长32位--必填
     */
    private String account;

    /**
     * 课程开始时间--必填
     */
    @JSONField(name = "start_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 课程结束时间--必填
     * (如果是伪直播，start_time等于end_time时，会自动根据关联信息的时长修改end_time，使伪直播时长等于关联对像的时长)
     */
    @JSONField(name = "end_time", format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 主播昵称--必填
     */
    private String nickname;

    /**
     * 主播简介--非必填
     */
    private String accountIntro;

    /**
     * 其他选项--非必填
     */
    @JSONField(name = "options")
    private Option option;

    /**
     * 其他选项
     */
    @Data
    public static class Option {

        /**
         * 部门ID
         */
        private String departmentId;

        /**
         * 是否开启弹幕：0 关闭，1 开启
         */
        private String barrage;

        /**
         * 虚拟用户数量
         */
        private Integer robotNumber;

        /**
         * 虚拟用户昵称类型：0使用真实姓名，1使用网络昵称
         */
        private Integer robotType;

        /**
         * 老师与助教是否显示虚拟用户：1不显示，0显示
         */
        private Integer adminNotShow;

        /**
         * 课程上课的模式： 1 语音云，3 大班(默认)，5 小班, 6:大班互动
         */
        @JSONField(name = "modetype")
        private Integer modeType;

        /**
         * 小班合流模式配置：1 多人模式，2 双人模式
         */
        private Integer streamMode;

        /**
         * 小班课类型： 1 1V1，2 1V6，3 1V16(默认)
         */
        private Integer smallType;

        /**
         * 是否将本次课程配置设为该主播的默认配置：1 设为该主播的默认配置，不设置为默认则不传
         */
        private Integer setDefault;

        /**
         * 主播密码，只有新主播生效。已经存在的主播，密码不会被修改
         */
        private String password;

        /**
         * 伪直播关联类型：5 关联普通回放（课程模式下一般用不到），9 关联课程回放，10 关联视频；
         */
        private String relateType;

        /**
         * relateId 关联的对应课程ID/视频ID，视频ID可以单个，也可以是多个(数组)
         */
        private List<String> relateId;

        /**
         * 直播类型。1: 教育直播，2: 生活直播。默认 1
         */
        private Integer scenes;
    }
}
