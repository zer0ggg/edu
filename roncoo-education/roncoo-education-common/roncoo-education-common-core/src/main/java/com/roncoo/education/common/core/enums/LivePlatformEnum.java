package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 直播类型
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum LivePlatformEnum {

    /**
     * 保利威
     */
    POLYV(1, "保利威"),

    /**
     * 欢拓
     */
    TALK_FUN(2, "欢拓");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取直播类型
     *
     * @param code 编码
     * @return 直播类型枚举
     */
    public static LivePlatformEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        LivePlatformEnum[] enums = LivePlatformEnum.values();
        for (LivePlatformEnum typeEnum : enums) {
            if (typeEnum.getCode().equals(code)) {
                return typeEnum;
            }
        }
        return null;
    }

    /**
     * 根据编码和默认编码获取直播平台
     *
     * @param codeList    编码集合
     * @param defaultCode 默认编码
     * @return 直播平台
     */
    public static List<Map<String, Object>> listByCodeAndDefaultCode(List<Integer> codeList, Integer defaultCode) {
        // 获取可用的直播平台
        List<LivePlatformEnum> enumList = Arrays.stream(LivePlatformEnum.values()).filter(item -> codeList.contains(item.getCode())).collect(Collectors.toList());
        if (enumList.isEmpty()) {
            return Collections.emptyList();
        }
        if (defaultCode != null) {
            enumList.sort((o1, o2) -> o1.getCode().equals(defaultCode) ? -1 : 0);
        }

        return enumList.stream().map(item -> {
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("code", item.getCode());
            resultMap.put("desc", item.getDesc());
            return resultMap;
        }).collect(Collectors.toList());
    }
}
