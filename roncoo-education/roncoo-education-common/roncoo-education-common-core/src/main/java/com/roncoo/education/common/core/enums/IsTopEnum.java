/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否置顶
 * 
 * @author Administrator
 *
 */
@Getter
@AllArgsConstructor
public enum IsTopEnum {

	YES(1, "置顶", "red"), NO(0, "不置顶", "");

	private Integer code;

	private String desc;

	private String color;

}
