/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum StatusIdEnum {

    /**
     * 正常
     */
    YES(1, "正常", ""),

    /**
     * 禁用
     */
    NO(0, "禁用", "red");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取状态枚举
     *
     * @param code 编码
     * @return 状态枚举
     */
    public static StatusIdEnum byCode(Integer code) {
        StatusIdEnum[] enums = StatusIdEnum.values();
        for (StatusIdEnum statusIdEnum : enums) {
            if (statusIdEnum.getCode().equals(code)) {
                return statusIdEnum;
            }
        }
        return null;
    }
}
