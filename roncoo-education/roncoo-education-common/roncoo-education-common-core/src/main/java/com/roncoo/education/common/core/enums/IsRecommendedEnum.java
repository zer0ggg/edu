package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否是推荐（1：是；0：否）
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum IsRecommendedEnum {

	YES(1, "开启"), NO(0, "关闭");

	private Integer code;

	private String desc;
}
