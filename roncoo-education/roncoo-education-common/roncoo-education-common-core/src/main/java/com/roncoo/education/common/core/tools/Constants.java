/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

/**
 * 常量工具类
 *
 * @author wujing
 */
public final class Constants {

    private Constants() {
    }

    public interface Session {
        public final static long TIME_OUT = 60; // 超时时间，单位：分钟
    }

    public interface TK {
        String USER_NO = "userNo";
        String CURRENT_LOGIN_NAME = "currentLoginName";
        String TOKEN = "token";
    }

    public interface Platform {
        /**
         * 管理后台登录标识
         */
        String ADMIN = "admin";
        /**
         * PC端登录标识
         */
        String PC = "pc";
        /**
         * 小程序端登录标识
         */
        String XCX = "xcx";
        /**
         * 安卓端登录标识
         */
        String ANDROID = "android";
        /**
         * 苹果端登录标识
         */
        String IOS = "ios";
    }

    /**
     * 日期类型
     *
     * @author wujing
     */
    public interface DATE {
        String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
        String YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
        String YYYY_MM_DD = "yyyy-MM-dd";
    }

    /**
     * 考试
     *
     * @author LYQ
     */
    public interface EXAM {
        /**
         * 答案分隔符
         */
        String PROBLEM_ANSWER_SEPARATION = "\\|";
    }

    /**
     * 超级管理员账号，固定值，社区资讯发布使用
     */
    public final static Long ADMIN_USER_NO = Long.valueOf("2018033111202589416");
    /**
     * vip购买显示名称
     */
    public final static String VIP_NAME = "VIP会员";
    /**
     * 验证码
     */
    public final static String SMS_CODE = "sms_code";
    /**
     * 冻结状态
     */
    public final static Integer FREEZE = 3;
    /**
     * 手机号检验
     */
    public final static String REGEX_MOBILE = "^1[0-9]{10}$";
}
