/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum VipTypeEnum {

	YEAR(1, "年度", 365, "red"), QUARTER(2, "季度", 92, "blue"), MONTH(3, "月度", 30, "green");

	private Integer code;

	private String desc;

	private Integer expireTime;

	private String color;

	public static VipTypeEnum getByExpireTime(Integer code) {
		VipTypeEnum tce = null;
		for (VipTypeEnum em : VipTypeEnum.values()) {
			if (em.getCode().equals(code)) {
				tce = em;
			}
		}
		if (tce == null) {
			return null;
		}
		return tce;
	}

}
