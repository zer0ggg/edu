/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 缓存前缀
 *
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum RedisPreEnum {
    // user
    USER_EXT("user_ext_", "用户信息-user_ext_"),
    SYS_CONFIG("sys_config_", "系统配置-sys_config_"),
    SENSITIVE_WORD_LIBRARY("sensitive_word_library_", "敏感词库-sensitive_word_library_"),

    /**
     * 运营后台--缓存操作员TOKEN
     */
    SYS_USER("sys_user_", "运营系统管理员权限-sys_user_"),

    /**
     * 运营后台--缓存操作员菜单
     */
    SYS_USER_MENU("sys_user_permission_", "运营系统管理员权限-sys_user_permission_"),

    /**
     * 运营后台--缓存操作员权限
     */
    SYS_USER_PERMISSION("sys_user_permission_", "运营系统管理员权限-sys_user_permission_"),

    /**
     * 运营后台--登录人机验证
     */
    SYS_USER_MAN_MACHINE_VERIFICATION("sys_user_man_machine_verification_", "运营系统登录人机验证-sys_user_man_machine_verification_"),

    SYS_USER_MAN_MACHINE_VERIFICATION_USED("sys_user_man_machine_verification_used_", "运营系统登录人机验证已使用-sys_user_man_machine_verification_used_"),

    SYS_MSG_SEND("sys_msg_send_", "发送站内信-sys_msg_send_"),

    SYS_MSG_SEND_NUM("sys_msg_send_num_", "发送站内信key数量-sys_msg_send_num_"),

    SYS_MSG("sys_msg_", "站内信-sys_msg"),

    LIVE_CHANNEL("live_channel_", "直播频道-live_channel"),

    ALI_YUN_VOD_PLAY("ali_yun_vod_play_", "阿里云点播播放-ali_yun_vod_play"),

    MINIAPP_ACCESS_TOKEN_KEY("ma_access_token_", "平台小程序--miniapp_access_token_"),

    MINIAPP_JSAPI_TICKET_KEY("ma_jsapi_ticket_", "平台小程序--miniapp_jsapi_ticket_"),

    MINIAPP_CARDAPI_TICKET_KEY("ma_cardapi_ticket_", "平台小程序--miniapp_cardapi_ticket_"),

    PERIOD_WATCK_PROGRESS("period_watck_progress_", "课时学习进度-period_watck_progress"),

    ADMINI_MENU("admini_menu_", "管理员菜单-admini_menu_"),

    ALI_YUN_UPLOAD_COMPLETE_HANDLE_LOCK("ALI_YUN_UPLOAD_COMPLETE_HANDLE_LOCK_", "阿里云上传完成处理锁-");


    private String code;

    private String desc;

}
