package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 模板类型（1:用户推荐；2:课程分享）
 *
 */
@Getter
@AllArgsConstructor
public enum TemplateTypeEnum {

	USER(1, "用户推荐"), COURSE(2, "课程分享");

	private Integer code;

	private String desc;

}
