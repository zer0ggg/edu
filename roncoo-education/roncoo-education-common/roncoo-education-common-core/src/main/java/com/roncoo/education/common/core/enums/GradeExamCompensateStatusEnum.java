package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级考试补交枚举
 *
 * @author LYQ
 */

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeExamCompensateStatusEnum {

    /**
     * 关闭
     */
    CLOSE(1, "关闭"),

    /**
     * 开启
     */
    OPEN(2, "开启");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    public static GradeExamCompensateStatusEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        GradeExamCompensateStatusEnum[] enums = GradeExamCompensateStatusEnum.values();
        for (GradeExamCompensateStatusEnum statusEnum : enums) {
            if (statusEnum.getCode().equals(code)) {
                return statusEnum;
            }
        }
        return null;
    }
}
