package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 移动端首页活动广告跳转链接
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum AdvUrlMEnum {

	COUPON("/pages/activity/coupon/coupon", "优惠券"), DISCOUNT("/pages/activity/discount/discount", "立减"), SECKILL("/pages/activity/spike/spike", "秒杀"), VPI("/pages/vip/vip", "会员");

	private String code;

	private String desc;

}
