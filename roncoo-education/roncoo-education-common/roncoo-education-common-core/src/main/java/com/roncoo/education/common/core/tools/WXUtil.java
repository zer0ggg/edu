/**
 * Copyright 2015-2016 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import cn.hutool.http.HttpUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;

/**
 * 微信接口工具类
 *
 * @author wujing
 */
public final class WXUtil {

	private static final Log logger = LogFactory.getLog(WXUtil.class);
	private static final String URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";

	private WXUtil() {
	}

	/**
	 * 获取openId
	 *
	 * @param code
	 * @param appId
	 * @param appSecret
	 * @return
	 */
	public static String getUniconId(String code, String appId, String appSecret) {
		String url = URL.replace("APPID", appId).replace("SECRET", appSecret).replace("JSCODE", code);
		String result = HttpUtil.get(url);
		HashMap<?, ?> map = JSUtil.parseObject(result, HashMap.class);
		Object obj = map.get("openid");
		if (obj == null) {
			logger.warn("获取失败，原因=" + result);
			return "";
		}
		return obj.toString();
	}

}
