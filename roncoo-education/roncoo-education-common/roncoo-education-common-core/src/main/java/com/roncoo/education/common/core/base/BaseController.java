/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.base;

import com.roncoo.education.common.core.tools.JSUtil;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Map;
import java.util.TreeMap;

/**
 * 控制基础类，所以controller都应该继承这个类
 *
 * @author wujing
 */
public class BaseController extends Base {

	public static final String TEXT_UTF8 = "text/html;charset=UTF-8";
	public static final String JSON_UTF8 = "application/json;charset=UTF-8";
	public static final String XML_UTF8 = "application/xml;charset=UTF-8";

	public static final int OK = 200;
	public static final int ER = 300;
	public static final int TO = 301;
	public static final boolean CLOSE = true;
	public static final boolean OPEN = false;

	/**
	 * 重定向
	 *
	 * @param format    格式字符串
	 * @param arguments 参数
	 * @return 格式化结果
	 */
	public static String redirect(String format, Object... arguments) {
		return "redirect:" + MessageFormat.format(format, arguments);
	}

	public static String getString(HttpServletRequest request) {
		DataInputStream in = null;
		try {
			in = new DataInputStream(request.getInputStream());
			byte[] buf = new byte[request.getContentLength()];
			in.readFully(buf);
			return new String(buf, StandardCharsets.UTF_8);
		} catch (Exception e) {
			return "";
		} finally {
			if (null != in) {
				try {
					// 关闭数据流
					in.close();
				} catch (IOException e) {
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static TreeMap<String, Object> getParamMap(HttpServletRequest request) {
		TreeMap<String, Object> paramMap = new TreeMap<>();
		Map<String, String[]> map = request.getParameterMap();
		for (String key : map.keySet()) {
			paramMap.put(key, map.get(key)[0]);
		}
		if (paramMap.isEmpty()) {
			return new TreeMap<>(JSUtil.parseObject(getString(request), TreeMap.class));
		}
		return paramMap;
	}

	/**
	 * 获取request
	 *
	 * @return 请求
	 */
	protected HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

}
