package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 模板类型（1:用户推荐；2:课程分享）
 *
 */
@Getter
@AllArgsConstructor
public enum MsgTemplateTypeEnum {

	COURSE(1, "课程审核"), ACTIVITY(2, "活动推送");

	private Integer code;

	private String desc;

}
