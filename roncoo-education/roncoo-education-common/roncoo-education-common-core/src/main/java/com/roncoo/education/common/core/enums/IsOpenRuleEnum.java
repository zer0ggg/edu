/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否开启支付路由
 *
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum IsOpenRuleEnum {

	YES(1, "是", ""), NO(0, "否", "red");

	private Integer code;

	private String desc;

	private String color;

}
