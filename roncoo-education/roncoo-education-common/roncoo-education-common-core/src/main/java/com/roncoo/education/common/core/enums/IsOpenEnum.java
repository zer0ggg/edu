/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否公开博客
 * 
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum IsOpenEnum {

	YES(1, "公开", ""), NO(0, "私密", "red");

	private Integer code;

	private String desc;

	private String color;

}
