/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程顾问分润类型
 *
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum AdviserProfitTypeEnum {

	COURSE(1, "课程订单"),
	USER(2, "会员订单");

	private Integer code;

	private String desc;
}
