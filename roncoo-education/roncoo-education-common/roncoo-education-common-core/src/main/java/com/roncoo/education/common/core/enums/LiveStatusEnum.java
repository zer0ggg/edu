/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否拥有直播功能
 * 
 * @author liaoh
 *
 */
@Getter
@AllArgsConstructor
public enum LiveStatusEnum {

	NOT(1, "未开播", ""),
	//
	NOW(2, "正在直播", "green"),
	//
	WAITCREATE(3, "待生成回放", "break"),

	WAITSAVE(4, "待转存", "blue"),

	END(5, "直播结束", "red");

	private Integer code;

	private String desc;

	private String color;

}
