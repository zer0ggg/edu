/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 平台展示枚举类
 *
 * @author Administrator
 *
 */
@Getter
@AllArgsConstructor
public enum PlatShowEnum {

	PC(1, "电脑端", ""), APP(2, "移动端","green");
	private Integer code;

	private String desc;

	private String color;

}
