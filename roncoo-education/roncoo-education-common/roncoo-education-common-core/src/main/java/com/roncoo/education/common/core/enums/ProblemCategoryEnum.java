package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 题目类型（1：年级；2：科目； 3：年份；4：来源；5：难度；6：题类；7：考点）图表使用
 */
@Getter
@AllArgsConstructor
public enum ProblemCategoryEnum {


    GRA(1, "年级"),
    SUBJECT(2, "科目"),
    YEAR(3, "年份"),
    SOURCE(4, "来源"),
    DIFFICULTY(5, "难度"),
    TOPIC(6, "题类"),
    EMPHASIS(7, "考点");
    private Integer code;

    private String desc;
}
