/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否叠加优惠(1可叠加;0不可叠加)
 *
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum ActTypeEnum {

    COUPON(1, "优惠券"), SECKILL(2, "秒杀");

    private Integer code;

    private String desc;

    public static ActTypeEnum getActType(int value) {
        for (ActTypeEnum actTypeEnum : ActTypeEnum.values()) {
            if (value == actTypeEnum.getCode()) {
                return actTypeEnum;
            }
        }
        return null;
    }
}
