package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:材料题；)
 */
@Getter
@AllArgsConstructor
public enum ProblemTypeEnum {

    THE_RADIO(1, "单选题", "blue"),
    MULTIPLE_CHOICE(2, "多选题", "blue"),
    ESTIMATE(3, "判断题", "blue"),
    GAP_FILLING(4, "填空题", "blue"),
    SHORT_ANSWER(5, "解答题", "blue"),
    MATERIAL(6, "组合题", "blue");

    private Integer code;

    private String desc;

    private String color;
}
