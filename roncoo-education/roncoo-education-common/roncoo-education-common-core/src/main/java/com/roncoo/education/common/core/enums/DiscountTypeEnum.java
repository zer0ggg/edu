/**
 * Copyright 2015-2017 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DiscountTypeEnum {

	REBATES(1, "打折"), MARKDOWN(2, "减价");

	private Integer code;

	private String desc;

}
