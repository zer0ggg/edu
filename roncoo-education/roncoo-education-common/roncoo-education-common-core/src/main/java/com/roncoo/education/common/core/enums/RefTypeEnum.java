package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 关联类型
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum RefTypeEnum {

	COURSE(1, "课程"), CHAPTER(2, "章节"), PERIOD(3, "课时");

	private Integer code;

	private String desc;
}
