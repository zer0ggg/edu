/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否已经发布
 * 
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum IsIssueEnum {

	YES(1, "已发布", "green"), NO(0, "未发布", "red");

	private Integer code;

	private String desc;

	private String color;
}
