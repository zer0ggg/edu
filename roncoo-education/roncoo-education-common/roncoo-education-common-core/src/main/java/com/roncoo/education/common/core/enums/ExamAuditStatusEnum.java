package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 考试评阅状态
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamAuditStatusEnum {


    /**
     * 未审核，初始化状态
     */
    NOT_AUDIT(1, "未评阅"),

    /**
     * 系统评阅
     */
    SYS_AUDIT(2, "系统评阅"),

    /**
     * 评阅中
     */
    AUDITING(3, "评阅中"),

    /**
     * 评阅完成
     */
    COMPLETE_AUDIT(4, "评阅完成");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    public static ExamAuditStatusEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        ExamAuditStatusEnum[] enums = ExamAuditStatusEnum.values();
        for (ExamAuditStatusEnum statusEnum : enums) {
            if (statusEnum.getCode().equals(code)) {
                return statusEnum;
            }
        }
        return null;
    }
}
