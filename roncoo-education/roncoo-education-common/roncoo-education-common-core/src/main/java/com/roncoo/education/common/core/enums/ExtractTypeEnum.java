package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 提现类型
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum ExtractTypeEnum {

	PROPORTION(1, "按比例"), EVERY(2, "按笔");

	private Integer code;

	private String desc;
}
