/**
 * Copyright 2015-2017 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NavEnum {

	LINK("/link", "自定义链接"), INDEX("/index", "首页"), COURSE("/list", "点播中心"), LIVE("/live", "直播中心"), RESOURCE("/resource", "文库中心"), EXAM("/exam", "试卷中心"), INFO("/info", "资讯中心"), BLOG("/blog", "博客中心"), QUESTIONS("/question", "知识问答"), SVIP("/vip", "超级会员"), RECRUIT("/recruit", "讲师招募");

	private String code;

	private String desc;

	public static NavEnum getByDesc(String code) {
		NavEnum tce = null;
		for (NavEnum em : NavEnum.values()) {
			if (em.getCode().equals(code)) {
				tce = em;
			}
		}
		if (tce == null) {
			return null;
		}
		return tce;
	}
}
