/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 适用范围类型(1全部课程可用;2关联课程可用)
 * 
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum RangeTypeEnum {

	ALL(1, "全部课程", "blue"), PART(2, "部分课程", "");

	private Integer code;

	private String desc;

	private String color;
}
