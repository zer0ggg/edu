package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * (1：超级会员； 2：普通用户；3：企业会员；4：企业用户)
 */
@Getter
@AllArgsConstructor
public enum UserVipTypeEnum {

    SVIP(1, "超级会员", "red"), COMMON(2, "普通用户", ""), ENTERPRISEVIP(3, "企业会员", "green"), ENTERPRISE_USER(4, "企业用户", "blue");

    private Integer code;

    private String desc;

    private String color;
}
