package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否提醒
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum IsNoticeEnum {

	YES(1, "提示", "green"), NO(0, "不提醒", "");

	private Integer code;

	private String desc;

	private String color;

}
