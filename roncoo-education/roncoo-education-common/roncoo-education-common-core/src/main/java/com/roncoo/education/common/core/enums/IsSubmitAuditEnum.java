/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum IsSubmitAuditEnum {

    /**
     * 提交
     */
    SUBMIT(1, "提交", ""),


    /**
     * 未提交
     */
    UNSUBMIT(0, "未提交", "green");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取是否提交审核枚举
     *
     * @param code 编码
     * @return 是否提交审核枚举
     */
    public static IsSubmitAuditEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        IsSubmitAuditEnum[] enums = IsSubmitAuditEnum.values();
        for (IsSubmitAuditEnum submitAuditEnum : enums) {
            if (submitAuditEnum.getCode().equals(code)) {
                return submitAuditEnum;
            }
        }
        return null;
    }

}
