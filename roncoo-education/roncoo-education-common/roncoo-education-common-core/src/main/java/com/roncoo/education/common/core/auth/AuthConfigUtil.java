/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.auth;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

@Slf4j
public class AuthConfigUtil {

	private AuthConfigUtil() {

	}

	/**
	 * 通过静态代码块读取上传文件的验证格式配置文件,静态代码块只执行一次(单例)
	 */
	private static final Properties PROPERTIES = new Properties();

	// 通过类装载器装载进来
	static {
		try {
			// 从类路径下读取属性文件
			PROPERTIES.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties"));
		} catch (IOException e) {
			log.error("读取失败", e);
		}
	}

	/**
	 * 根据key读取value
	 *
	 * @param keyName key
	 * @return
	 */
	public static String getProperty(String keyName) {
		return getProperty(keyName, "");
	}

	/**
	 * 根据key读取value，key为空，返回默认值
	 *
	 * @param keyName      key
	 * @param defaultValue 默认值
	 * @return
	 */
	public static String getProperty(String keyName, String defaultValue) {
		return PROPERTIES.getProperty(keyName, defaultValue).trim();
	}

	public static final String PAY_KEY = getProperty("payKey");
	public static final String PAY_SECRET = getProperty("paySecret");
	public static final String PRODUCT_TYPE = getProperty("productType");
	public static final String AUTH_URL = getProperty("authUrl");
}
