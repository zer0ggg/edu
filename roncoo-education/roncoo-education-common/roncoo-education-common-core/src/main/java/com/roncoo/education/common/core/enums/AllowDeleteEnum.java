/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 允许删除(1:允许;0:不允许)
 *
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum AllowDeleteEnum {

    ALLOW(1, "允许"), NOT_ALLOW(0, "不允许");

    private Integer code;

    private String desc;

}
