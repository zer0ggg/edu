/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否需要人脸对比(0:否，1:是)
 */
@Getter
@AllArgsConstructor
public enum IsFaceContrasEnum {

	YES(1, "需要"), NO(0, "不需要");

	private Integer code;

	private String desc;

}
