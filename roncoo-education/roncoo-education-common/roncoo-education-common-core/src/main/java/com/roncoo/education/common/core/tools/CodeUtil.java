/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import cn.hutool.core.util.RandomUtil;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;


/**
 * @author wujing
 */
public final class CodeUtil {

    private CodeUtil() {

    }

    /**
     * 图像宽度
     */
    private static final int WIDTH = 57;
    /**
     * 图像高度
     */
    private static final int HEIGHT = 21;

    public static String create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 定义输出格式
        response.setContentType("image/jpeg");
        ServletOutputStream out = response.getOutputStream();
        // 准备缓冲图像,不支持表单
        BufferedImage bimg = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        // 获取图形上下文环境
        Graphics gc = bimg.getGraphics();
        // 设定背景色并进行填充
        gc.setColor(getRandColor(200, 250));
        gc.fillRect(0, 0, WIDTH, HEIGHT);
        // 设置图形上下文环境字体
        gc.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        // 随机产生200条干扰线条，使图像中的认证码不易被其他分析程序探测到
        gc.setColor(getRandColor(160, 200));
        for (int i = 0; i < 200; i++) {

            int x1 = RandomUtil.randomInt(WIDTH);
            int y1 = RandomUtil.randomInt(HEIGHT);
            int x2 = RandomUtil.randomInt(15);
            int y2 = RandomUtil.randomInt(15);
            gc.drawLine(x1, y1, x1 + x2, y1 + y2);
        }
        // 随机产生100个干扰点，使图像中的验证码不易被其他分析程序探测到
        gc.setColor(getRandColor(120, 240));
        for (int i = 0; i < 100; i++) {
            int x = RandomUtil.randomInt(WIDTH);
            int y = RandomUtil.randomInt(HEIGHT);
            gc.drawOval(x, y, 0, 0);
        }

        // 随机产生4个数字的验证码
        StringBuilder rs = new StringBuilder();
        String rn;
        for (int i = 0; i < 4; i++) {
            rn = String.valueOf(RandomUtil.randomInt(10));
            rs.append(rn);
            gc.setColor(new Color(20 + RandomUtil.randomInt(110), 20 + RandomUtil.randomInt(110), 20 + RandomUtil.randomInt(110)));
            gc.drawString(rn, 13 * i + 1, 16);
        }

        // 释放图形上下文环境
        gc.dispose();
        request.getSession().setAttribute("randomCode", rs.toString());
        ImageIO.write(bimg, "jpeg", out);
        out.flush();
        out.close();
        return rs.toString();
    }

    private static Color getRandColor(int fc, int bc) {
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int red = fc + RandomUtil.randomInt(bc - fc);// 红
        int green = fc + RandomUtil.randomInt(bc - fc);// 绿
        int blue = fc + RandomUtil.randomInt(bc - fc);// 蓝
        return new Color(red, green, blue);
    }
}
