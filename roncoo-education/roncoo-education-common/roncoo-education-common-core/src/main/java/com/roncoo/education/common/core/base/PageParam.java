package com.roncoo.education.common.core.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页参数
 *
 * @author LYQ
 */
@Data
public class PageParam implements Serializable {

    private static final long serialVersionUID = -6047174855923324105L;

    /**
     * 当前页
     */
    private Integer pageCurrent = 1;

    /**
     * 每页条数
     */
    private Integer pageSize = 30;
}
