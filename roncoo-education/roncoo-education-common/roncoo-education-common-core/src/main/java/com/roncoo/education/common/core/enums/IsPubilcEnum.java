/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 试题是否公开(1:公开,0:不公开)
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum IsPubilcEnum {

    /**
     * 公开
     */
    YES(1, "公开", ""),

    /**
     * 不公开
     */
    NO(0, "不公开", "red");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取状态枚举
     *
     * @param code 编码
     * @return 状态枚举
     */
    public static IsPubilcEnum byCode(Integer code) {
        IsPubilcEnum[] enums = IsPubilcEnum.values();
        for (IsPubilcEnum statusIdEnum : enums) {
            if (statusIdEnum.getCode().equals(code)) {
                return statusIdEnum;
            }
        }
        return null;
    }
}
