package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IsUserAgentEnum {

	NO(1, "新用户，允许申请"), YES(2, "老用户，允许申请"), AGENTYES(3, "不允许申请");

	private Integer code;

	private String desc;

}
