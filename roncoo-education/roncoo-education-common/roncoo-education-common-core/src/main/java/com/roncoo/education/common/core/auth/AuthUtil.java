/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.auth;

import cn.hutool.json.JSONUtil;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.tools.HttpUtil;
import com.roncoo.education.common.core.tools.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.message.BasicHeader;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@Slf4j
public final class AuthUtil {

	private AuthUtil() {
	}

	/**
	 * @param bankcard 银行卡卡号
	 * @param idcard   身份证号
	 * @param mobile   手机号
	 * @param name     姓名
	 * @return
	 */
	public static AuthResult bankcard4c(String appcode, String bankcard, String idcard, String mobile, String name) {
		BasicHeader headers = new BasicHeader("Authorization", "APPCODE " + appcode);
		Map<String, Object> querys = new HashMap<>();
		querys.put("bankcard", bankcard);
		querys.put("idcard", idcard);
		querys.put("mobile", mobile);
		querys.put("name", name);
		AuthResult authResult = new AuthResult();
		try {
			String strign = HttpUtil.getHeadersForObjec(SystemUtil.HOST, headers, querys);
			Map<String, Object> resultMap;
			log.info("返回报文：{}", strign);
			resultMap = JSONUtil.parseObj(strign);
			if ("200".equals(String.valueOf(resultMap.get("code")))) {
				Map<String, Object> Map;
				Map = JSONUtil.parseObj(String.valueOf(resultMap.get("data")));
				authResult.setResultCode(String.valueOf(Map.get("result")));
				if (Map.get("result").equals(0)) {
					authResult.setStatus(true);
					authResult.setMessage("校验通过");
				} else {
					authResult.setMessage("校验不通过");
					authResult.setStatus(false);
				}
			} else {
				if ("Invalid id_number".equals(String.valueOf(resultMap.get("msg")))) {
					authResult.setMessage("请输入有效的身份证号码");
				} else if ("Invalid card_number".equals(String.valueOf(resultMap.get("msg")))) {
					authResult.setMessage("请输入有效的银行卡号");
				} else {
					authResult.setMessage(String.valueOf(resultMap.get("msg")));
					authResult.setResultCode(String.valueOf(resultMap.get("code")));
				}
				authResult.setStatus(false);
			}
			return authResult;
		} catch (Exception e) {
			log.error("银行卡校验失败！", e);
			authResult.setMessage("验签失败");
			authResult.setResultCode("99");
			authResult.setStatus(false);
			return authResult;
		}
	}

	/**
	 * 获取参数签名
	 *
	 * @param paramMap  签名参数
	 * @param paySecret 签名密钥
	 * @return
	 */
	public static String getSign(Map<String, Object> paramMap, String paySecret) {
		SortedMap<String, Object> smap = new TreeMap<String, Object>(paramMap);
		StringBuffer stringBuffer = new StringBuffer();
		for (Map.Entry<String, Object> m : smap.entrySet()) {
			Object value = m.getValue();
			if (value != null && StringUtils.isNotBlank(String.valueOf(value)) && !"sign".equals(m.getKey())) {
				stringBuffer.append(m.getKey()).append("=").append(m.getValue()).append("&");
			}
		}
		stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
		String argPreSign = stringBuffer.append("&paySecret=").append(paySecret).toString();
		return MD5Util.MD5(argPreSign).toUpperCase();
	}
}
