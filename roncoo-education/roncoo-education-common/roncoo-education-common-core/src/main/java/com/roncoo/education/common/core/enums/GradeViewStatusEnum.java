package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级考试查看状态
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeViewStatusEnum {

    /**
     * 未查看
     */
    NO(1, "未查看"),

    /**
     * 已查看
     */
    YES(2, "已查看"),

    /**
     * 已忽略
     */
    IGNORE(3, "已忽略");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
