/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 配置类型(0:文本;1:富文本,2图片)
 *
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum SysConfigTypeEnum {

    TEXT(1, "文本"), EDIT_TEXT(2, "富文本"), IMG(3, "图片"),SWITCH(4, "布尔类型"),ENUME(5, "枚举类型");

    private Integer code;

    private String desc;

}
