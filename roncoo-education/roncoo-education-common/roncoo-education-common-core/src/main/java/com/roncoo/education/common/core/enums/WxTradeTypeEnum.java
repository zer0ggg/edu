/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 微信支付交易类型trade_type
 *
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum WxTradeTypeEnum {

    JSAPI("JSAPI", "JSAPI支付（或小程序支付）"),
    APP("APP", "app支付"),
    MWEB("MWEB", "H5支付"),
    NATIVE("NATIVE", "Native支付");

    private String code;

    private String desc;

}
