package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级学生用户角色枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeStudentUserRoleEnum {

    /**
     * 普通用户
     */
    USER(1, "学员"),

    /**
     * 管理员，拥有部分讲师的操作权限
     */
    ADMIN(2, "管理员");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取班级学生用户角色枚举
     *
     * @param code 编码
     * @return 面积验证设置枚举
     */
    public static GradeStudentUserRoleEnum byCode(Integer code) {
        // 判空
        if (code == null) {
            return null;
        }

        // 匹配
        GradeStudentUserRoleEnum[] enums = GradeStudentUserRoleEnum.values();
        for (GradeStudentUserRoleEnum userRoleEnum : enums) {
            if (userRoleEnum.getCode().equals(code)) {
                return userRoleEnum;
            }
        }
        return null;
    }
}
