package com.roncoo.education.common.core.tools;

import com.roncoo.education.common.core.enums.IsVipEnum;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 检查会员
 *
 * @author LHR
 */
public final class VipCheckUtil {

	private VipCheckUtil() {
	}

	/**
	 * 用户是会员，在有效期内，价格为0则为true
	 *
	 * @param isVip
	 * @param expireTime
	 * @param fabPrice
	 * @return
	 * @author LHR
	 */
	public static boolean checkVipIsFree(Integer isVip, Date expireTime, BigDecimal fabPrice) {
        return IsVipEnum.YES.getCode().equals(isVip) && expireTime.after(new Date()) && fabPrice.compareTo(new BigDecimal(0)) == 0;
    }

	/**
	 * 用户是会员，在有效期内则为true
	 *
	 * @param isVip
	 * @param expireTime
	 * @param fabPrice
	 * @return
	 * @author LHR
	 */
	public static boolean checkIsVip(Integer isVip, Date expireTime) {
		if (IsVipEnum.YES.getCode().equals(isVip) && expireTime.after(new Date())) {
			return true;
		}
		return false;
	}

}
