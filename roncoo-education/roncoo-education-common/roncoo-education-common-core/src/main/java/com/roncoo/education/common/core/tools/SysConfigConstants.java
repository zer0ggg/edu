package com.roncoo.education.common.core.tools;

/**
 * 配置常量类
 */
public final class SysConfigConstants {

    private SysConfigConstants() {
    }


    /**
     * 直播重传设置(1开启,0不开启)
     */
    public static final String POLYV_LIVE_UPLOAD_SET = "polyvLiveUploadSet";

    /**
     * 短信签名
     */
    public static final String ALIYUN_SMS_SIGN_NAME = "aliyunSmsSignName";

    //前端跳转地址
    /**
     * 我的班级
     */
    public static final String EXAM_MYGRADE = "/account/grade/my";

    /**
     * 我的考试
     */
    public static final String EXAM_MYEXAMINATION = "/account/exam/myExamination";

    /**
     * 班级管理
     */
    public static final String EXAM_MYGRADE_TEACHER = "/account/grade/teacher";

    /**
     * 考试中心
     */
    public static final String EXAM = "/exam";

    /**
     * 试卷管理
     */
    public static final String EXAM_ADMIN = "/account/exam";

    /**
     * 站点域名
     */
    public static final String DOMAIN = "domain";

    /**
     * 保利威直播备份
     */
    public static final String POLYV_LIVE_BACKUP = "polyvLiveBackup";

    /**
     * 点播是否备份阿里云
     */
    public static final String IS_BACKUP_ALIYUN = "isBackupAliyun";

    /**
     * logo
     */
    public static final String LOGO = "logo";

    /**
     * 视频是否备份阿里云(1:备份,2:不备份)
     */
    public static final String ISBACKUPALI = "isBackupAli";

    /**
     * 是否需要人脸对比(0:否，1:是)
     */
    public static final String ISFACECONTRAS = "isFaceContras";

    /**
     * 是否需要统计(0:否，1:是)
     */
    public static final String ISENABLESTATISTICS = "isEnableStatistics";

    /**
     * 网关域名
     */
    public static final String GATEWAY_DOMAIN = "gatewayDomain";

    /**
     * 移动端域名
     */
    public static final String H5_DOMAIN = "h5Domain";
}
