package com.roncoo.education.common.core.tools;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 数字工具类
 *
 * @author LYQ
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class NumberUtil {
    /**
     * 中文数字
     */
    private static final String[] CHINESE_NUMBER = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"};
    /**
     * 中文数字单位
     */
    private static final String[] CHINESE_NUMBER_UNIT = {"", "十", "百", "千", "万", "十", "百", "千", "亿", "十", "百", "千"};

    /**
     * 字母数组
     */
    private static final String[] LETTER_LIST = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    /**
     * 数字转中文数字
     *
     * @param src 数字
     * @return 中文数字
     */
    public static String intToChineseNumber(int src) {
        String dst = "";
        int count = 0;
        while (src > 0) {
            dst = (CHINESE_NUMBER[src % 10] + CHINESE_NUMBER_UNIT[count]) + dst;
            src = src / 10;
            count++;
        }
        return dst.replaceAll("零[千百十]", "零").replaceAll("零+万", "万")
                .replaceAll("零+亿", "亿").replaceAll("亿万", "亿零")
                .replaceAll("零+", "零").replaceAll("零$", "");
    }

    /**
     * 数字转字母
     *
     * @param src 数字
     * @return 字母
     */
    public static String intToLetter(int src) {
        if (src < 1) {
            return "";
        }
        return LETTER_LIST[(src - 1) % LETTER_LIST.length];
    }
}
