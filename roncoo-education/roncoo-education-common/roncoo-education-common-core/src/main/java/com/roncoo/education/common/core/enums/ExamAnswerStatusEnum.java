package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 试卷用户答案状态
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamAnswerStatusEnum {

    /**
     * 待评阅
     */
    WAIT(1, "待评阅"),

    /**
     * 已评阅
     */
    COMPLETE(2, "已评阅");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
