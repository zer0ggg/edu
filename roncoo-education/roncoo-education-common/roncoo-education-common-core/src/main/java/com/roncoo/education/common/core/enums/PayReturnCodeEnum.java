package com.roncoo.education.common.core.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Quanf
 */
@AllArgsConstructor
@Getter
public enum PayReturnCodeEnum {

    SUCCESS("0000", "请求成功"),


    FAIL("9997", "请求失败");

    /**
     * 编码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

}
