/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;

/**
 * @author wujing
 */
public final class StrUtil {

	private StrUtil() {
	}

	public static String getSuffix(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1);
	}

	public static String getPrefix(String fileName) {
		return fileName.substring(0, fileName.lastIndexOf("."));
	}

	/**
	 * @return
	 */
	public static String getRandom(int bound) {
		return RandomUtil.randomNumbers(bound);
	}

	public static String get32UUID() {
		return IdUtil.simpleUUID();
	}

	/**
	 * 从头开始截取
	 * 
	 * @param str 字符串
	 * @param end 结束位置
	 * @return
	 * 
	 *         https://blog.csdn.net/xiaoxian8023/article/details/49834643
	 */
	public static String subStrStart(String str, int end) {
		return subStr(str, 0, end);
	}

	/**
	 * 截取字符串 （支持正向、反向选择）<br/>
	 * 
	 * @param str   待截取的字符串
	 * @param start 起始索引 ，>=0时，从start开始截取；<0时，从length-|start|开始截取
	 * @param end   结束索引 ，>=0时，从end结束截取；<0时，从length-|end|结束截取
	 * @return 返回截取的字符串
	 * @throws RuntimeException
	 */
	public static String subStr(String str, int start, int end) throws RuntimeException {
		if (str == null) {
			throw new NullPointerException("");
		}
		int len = str.length();
		int s = 0;// 记录起始索引
		int e = 0;// 记录结尾索引
		if (len < Math.abs(start)) {
			throw new StringIndexOutOfBoundsException("最大长度为" + len + "，索引超出范围为:" + (len - Math.abs(start)));
		} else if (start < 0) {
			s = len - Math.abs(start);
		} else if (start == 0) {
			s = 0;
		} else {// >=0
			s = start;
		}
		if (len < Math.abs(end)) {
			e = len;
		} else if (end < 0) {
			e = len - Math.abs(end);
		} else if (end == 0) {
			e = len;
		} else {// >=0
			e = end;
		}
		if (e < s) {
			throw new StringIndexOutOfBoundsException("截至索引小于起始索引:" + (e - s));
		}

		return str.substring(s, e);
	}
}
