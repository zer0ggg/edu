package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否显示给讲师功能
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum IsShowLecturerEnum {

	YES(1, "开启"), NO(0, "关闭");

	private Integer code;

	private String desc;

}
