/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultEnum {
	// 成功
	SUCCESS(200, "成功"),

	// token异常
	TOKEN_PAST(301, "登录过期"), TOKEN_ERROR(302, "token异常"),
	// 登录异常
	LOGIN_ERROR(303, "请重新登录"), REMOTE_ERROR(304, "异地登录"),
	// 鉴权异常
	AUTHORIZE_ERROR(305, "鉴权失败"), AUTHORIZE_SCARCITY(306, "权限不足"),

	// 课程异常，4开头
	COURSE_SAVE_FAIL(403, "添加失败"), COURSE_UPDATE_FAIL(404, "更新失败"), COURSE_DELETE_FAIL(405, "删除失败"),
	//
	SETTING_SORT(410, "设置排序失败"), COLLECTION(406, "已收藏"), USER_ADVICE(406, "保存建议失败,不能重复提建议"), PIC_DELETE(407, "该图片已被使用，请先删除"), COURSE_DISABLE(408, "课程不可用"),

	// 用户异常，5开头
	LECTURER_REQUISITION_REGISTERED(501, "申请失败！该手机没注册，请先注册账号"), LECTURER_REQUISITION_WAIT(502, "申请失败！该账号已提交申请入驻成为讲师，待审核中，在7个工作日内会有相关人员与您联系确认"), LECTURER_REQUISITION_YET(503, "申请失败！该账号已成为讲师，请直接登录"),
	//

	USER_SAVE_FAIL(504, "添加失败"), USER_UPDATE_FAIL(505, "更新失败"), USER_ATTENTION(506, "已关注"), USER_LOGIN_ERROR(513, "登录错误次数过多"), REAL_NAME(514, "需要上传实名信息"),

	// 博客异常 6开头
	BLOGGER_SAVE_FAIL(601, "用户的博主信息已经存在"), COMMUNITY_DELETE_FAIL(602, "删除失败"), COMMUNITY_UPDATE_FAIL(603, "更新失败"),

	// 试卷异常 7开头
	EXAM_SAVE_FAIL(701, "添加失败"), EXAM_UPDATE_FAIL(702, "更新失败"), EXAM_COLLECTION(703, "已收藏"), EXAM_COLLECTION_FAIL(704, "收藏失败"), EXAM_DELETE_FAIL(405, "删除失败"),

	// 支付 8开头
	REPEAT_PAY(801, "重复购买"),
	// 错误
	ERROR(999, "系统错误，请联系管理员");

	private Integer code;

	private String desc;

}
