package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级考试状态枚举
 *
 * @author LYQ
 */

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeExamStatusEnum {

    /**
     * 未考试
     */
    WAIT(1, "未考试"),

    /**
     * 考试中
     */
    NOT_OVER(2, "考试中"),

    /**
     * 考试结束
     */
    FINISH(3, "考试完成"),

    /**
     * 评阅中
     */
    AUDITING(4, "评阅中"),

    /**
     * 评阅完成
     */
    AUDIT_COMPLETE(5, "评阅完成");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
