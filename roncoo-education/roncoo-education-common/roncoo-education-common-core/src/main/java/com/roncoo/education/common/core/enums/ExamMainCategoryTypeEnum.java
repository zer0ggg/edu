/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 个人分类用 试题分类枚举类
 * @author forest
 *
 */
@Getter
@AllArgsConstructor
public enum ExamMainCategoryTypeEnum {

	SUBJECT(1, "试题", "red") , EXAM(2, "试卷", "");

	private Integer code;

	private String desc;

	private String color;


}
