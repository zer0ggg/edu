/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum AgentTypeEnum {

	AREA(1, "大代理"), LOCAL(2, "分销");

	private Integer code;

	private String desc;

}
