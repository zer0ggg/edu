/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否备份阿里云
 */
@Getter
@AllArgsConstructor
public enum IsBackupEnum {

    /**
     * 正常
     */
    YES(1, "备份", ""),

    /**
     * 禁用
     */
    NO(0, "不备份", "red");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取状态枚举
     *
     * @param code 编码
     * @return 状态枚举
     */
    public static IsBackupEnum byCode(Integer code) {
        IsBackupEnum[] enums = IsBackupEnum.values();
        for (IsBackupEnum statusIdEnum : enums) {
            if (statusIdEnum.getCode().equals(code)) {
                return statusIdEnum;
            }
        }
        return null;
    }
}
