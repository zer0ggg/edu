/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum ExamSortEnum {

    DEFAULT(0, "综合排序"),
    STUDY(1, "浏览次数"),
    DOWNLOAD(2, "下载次数");

    private Integer code;

    private String desc;

}
