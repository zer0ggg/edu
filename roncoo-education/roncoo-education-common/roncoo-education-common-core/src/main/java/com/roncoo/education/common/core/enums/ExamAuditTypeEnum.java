package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 评阅类型(1:不评阅，2:评阅)
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamAuditTypeEnum {

    /**
     * 考试中
     */
    NO_AUDIT(1, "不评阅"),

    /**
     * 考试结束
     */
    NEED_AUDIT(2, "须评阅");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取试卷评阅类型
     *
     * @param code 编码
     * @return 试卷评阅类型
     */
    public static ExamAuditTypeEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        ExamAuditTypeEnum[] enums = ExamAuditTypeEnum.values();
        for (ExamAuditTypeEnum typeEnum : enums) {
            if (typeEnum.getCode().equals(code)) {
                return typeEnum;
            }
        }
        return null;
    }
}
