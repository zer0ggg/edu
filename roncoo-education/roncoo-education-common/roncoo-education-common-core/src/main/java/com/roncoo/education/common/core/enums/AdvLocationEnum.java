package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 广告位置(1首页轮播图，2顶部活动,3资讯)
 *
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum AdvLocationEnum {

	SLIDESHOW(1, "首页轮播图"), TOP(2, "顶部活动"), MESSAGE(3, "首页资讯轮播图");

	private Integer code;

	private String desc;

}
