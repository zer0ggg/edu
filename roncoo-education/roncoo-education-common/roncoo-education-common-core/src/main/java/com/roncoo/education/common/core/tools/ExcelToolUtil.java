package com.roncoo.education.common.core.tools;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class ExcelToolUtil {
    public static final String EMPTY = "";
    public static final String POINT = ".";
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * 获得path的后缀名
     *
     * @param path 文件路径
     * @return 文件后缀名
     */
    public static String getPostfix(String path) {
        if (path == null || EMPTY.equals(path.trim())) {
            return EMPTY;
        }
        if (path.contains(POINT)) {
            return path.substring(path.lastIndexOf(POINT) + 1);
        }
        return EMPTY;
    }

    /**
     * 单元格格式
     *
     * @param hssfCell 单元格
     * @return 单元格数据（格式化后）
     */
    public static String getHValue(HSSFCell hssfCell) {
        if (CellType.BOOLEAN.equals(hssfCell.getCellTypeEnum())) {
            return String.valueOf(hssfCell.getBooleanCellValue());
        } else if (CellType.NUMERIC.equals(hssfCell.getCellTypeEnum())) {
            String cellValue;
            if (HSSFDateUtil.isCellDateFormatted(hssfCell)) {
                Date date = HSSFDateUtil.getJavaDate(hssfCell.getNumericCellValue());
                cellValue = sdf.format(date);
            } else {
                DecimalFormat df = new DecimalFormat("#.##");
                cellValue = df.format(hssfCell.getNumericCellValue());
                String strArr = cellValue.substring(cellValue.lastIndexOf(POINT) + 1);
                if ("00".equals(strArr)) {
                    cellValue = cellValue.substring(0, cellValue.lastIndexOf(POINT));
                }
            }
            return cellValue;
        } else {
            return String.valueOf(hssfCell.getStringCellValue());
        }
    }

    /**
     * 单元格格式
     *
     * @param xssfCell 单元格对象
     * @return 单元格数据（格式化后）
     */
    public static String getXValue(XSSFCell xssfCell) {
        String cellValue = "";
        if (xssfCell == null) {
            return cellValue;
        }
        if (CellType.BOOLEAN.equals(xssfCell.getCellTypeEnum())) {
            return String.valueOf(xssfCell.getBooleanCellValue());
        } else if (CellType.NUMERIC.equals(xssfCell.getCellTypeEnum())) {

            if (XSSFDateUtil.isCellDateFormatted(xssfCell)) {
                Date date = XSSFDateUtil.getJavaDate(xssfCell.getNumericCellValue());
                cellValue = sdf.format(date);
            } else {
                DecimalFormat df = new DecimalFormat("#.##");
                cellValue = df.format(xssfCell.getNumericCellValue());
                String strArr = cellValue.substring(cellValue.lastIndexOf(POINT) + 1);
                log.debug("+=====================+={}", xssfCell.getNumericCellValue());
                if (xssfCell.getCellStyle().getDataFormatString().contains("%")) {
                    cellValue = xssfCell.getNumericCellValue() * 100 + "%";
                }
                if ("00".equals(strArr) || "0".equals(strArr)) {
                    cellValue = cellValue.substring(0, cellValue.lastIndexOf(POINT));
                }
            }
            return cellValue;
        } else {
            return String.valueOf(xssfCell.getStringCellValue());
        }
    }

    /**
     * 自定义xssf日期工具类
     *
     * @author lp
     */
    static class XSSFDateUtil extends DateUtil {
        protected static int absoluteDay(Calendar cal, boolean use1904windowing) {
            return DateUtil.absoluteDay(cal, use1904windowing);
        }
    }
}
