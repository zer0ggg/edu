/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 移动端枚举类
 *
 * @author Administrator
 *
 */
@Getter
@AllArgsConstructor
public enum MobileTerminalCategoryEnum {

	APP(1, "小程序/App端","green"),H5(2, "H5移动端","green"),;

	private Integer code;

	private String desc;

	private String color;

}
