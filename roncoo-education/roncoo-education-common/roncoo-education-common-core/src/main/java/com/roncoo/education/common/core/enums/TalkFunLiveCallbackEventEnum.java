package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 欢拓回调事件
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TalkFunLiveCallbackEventEnum {

    /**
     * 直播开始
     */
    LIVE_START("live.start", "直播开始"),

    /**
     * 直播结束
     */
    LIVE_STOP("live.stop", "直播结束"),

    /**
     * 直播回放生成
     */
    LIVE_PLAYBACK("live.playback", "直播回放生成"),

    /**
     * 上传文档
     */
    LIVE_DOCUMENT_ADD("live.document.add", "上传文档");

    /**
     * 编码
     */
    private final String code;

    /**
     * 描述
     */
    private final String desc;

}
