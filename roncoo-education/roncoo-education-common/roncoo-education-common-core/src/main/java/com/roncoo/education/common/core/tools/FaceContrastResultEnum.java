package com.roncoo.education.common.core.tools;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 人脸识别返沪结果
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum FaceContrastResultEnum {

    SUCCESS("Success", "是本人"),
    ACTIONCLOSEEYE("FailedOperation.ActionCloseEye","未检测到闭眼动作。"),
    ACTIONFACECLOSE("FailedOperation.ActionFaceClose","脸离屏幕太近。"),
    ACTIONFACEFAR("FailedOperation.ActionFaceFar","脸离屏幕太远。"),
    ACTIONFACELEFT("FailedOperation.ActionFaceLeft","脸离屏幕太左。"),
    ACTIONFACERIGHT("FailedOperation.ActionFaceRight", "脸离屏幕太右。"),
    ACTIONFIRSTACTION("FailedOperation.ActionFirstAction","未检测到第一个动作。"),
    ACTIONLIGHTDARK("FailedOperation.ActionLightDark", "光线太暗。"),
    ACTIONLIGHTSTRONG("FailedOperation.ActionLightStrong", "光线太强。"),
    ACTIONNODETECTFACE("FailedOperation.ActionNodetectFace", "未能检测到完整人脸。"),
    ACTIONOPENMOUTH("FailedOperation.ActionOpenMouth", "未检测到张嘴动作。"),
    COMPAREFAIL("FailedOperation.CompareFail", "比对失败。"),
    COMPARELOWSIMILARITY("FailedOperation.CompareLowSimilarity", "比对相似度未达到通过标准。"),
    COMPARESYSTEMERROR("FailedOperation.CompareSystemError", "调用比对引擎接口出错。"),
    FILESAVEERROR("FailedOperation.FileSaveError", "文件存储失败，请稍后重试。"),
    IDFORMATERROR("FailedOperation.IdFormatError", "输入的身份证号有误。"),
    IDNAMEMISMATCH("FailedOperation.IdNameMisMatch", "姓名和身份证号不一致，请核实后重试。"),
    IDNOEXISTSYSTEM("FailedOperation.IdNoExistSystem", "库中无此号，请到户籍所在地进行核实。"),
    IDPHOTONOEXIST("FailedOperation.IdPhotoNoExist", "库中无此号照片，请到户籍所在地进行核实。"),
    IDPHOTOPOORQUALITY("FailedOperation.IdPhotoPoorQuality", "证件图片分辨率太低，请重新上传。"),
    IDPHOTOSYSTEMNOANSWER("FailedOperation.IdPhotoSystemNoanswer", "客户库自建库或认证中心返照失败，请稍后再试。"),
    LIFEPHOTODETECTFACES("FailedOperation.LifePhotoDetectFaces", "检测到多张人脸。"),
    LIFEPHOTODETECTFAKE("FailedOperation.LifePhotoDetectFake", "实人比对没通过。"),
    LIFEPHOTODETECTNOFACES("FailedOperation.LifePhotoDetectNoFaces", "未能检测到完整人脸。"),
    LIFEPHOTOPOORQUALITY("FailedOperation.LifePhotoPoorQuality", "传入图片分辨率太低，请重新上传。"),
    LIFEPHOTOSIZEERROR("FailedOperation.LifePhotoSizeError", "传入图片过大或过小。"),
    LIPFACEINCOMPLETE("FailedOperation.LipFaceIncomplete", "脸部未完整露出。"),
    LIPMOVESMALL("FailedOperation.LipMoveSmall", "嘴唇动作幅度过小。"),
    LIPNETFAILED("FailedOperation.LipNetFailed", "视频拉取失败，请重试。"),
    LIPSIZEERROR("FailedOperation.LipSizeError", "视频为空，或大小不合适，请控制录制时长在6s左右。"),
    LIPVIDEOINVALID("FailedOperation.LipVideoInvalid", "视频格式有误。"),
    LIPVIDEOQUAILITY("FailedOperation.LipVideoQuaility", "视频像素太低。"),
    LIPVOICEDETECT("FailedOperation.LipVoiceDetect", "未检测到声音。"),
    LIPVOICELOW("FailedOperation.LipVoiceLow", "视频声音太小。"),
    LIPVOICERECOGNIZE("FailedOperation.LipVoiceRecognize", "声音识别失败。"),
    LIVESSBESTFRAMEERROR("FailedOperation.LivessBestFrameError", "人脸检测失败，无法提取比对照。"),
    LIVESSDETECTFAIL("FailedOperation.LivessDetectFail", "活体检测没通过。"),
    LIVESSDETECTFAKE("FailedOperation.LivessDetectFake", "疑似非真人录制。"),
    LIVESSSYSTEMERROR("FailedOperation.LivessSystemError", "调用活体引擎接口出错。"),
    LIVESSUNKNOWNERROR("FailedOperation.LivessUnknownError", "视频实人检测没通过。"),
    NAMEFORMATERROR("FailedOperation.NameFormatError", "输入的姓名有误。"),
    SILENTDETECTFAIL("FailedOperation.SilentDetectFail", "实人检测失败。"),
    SILENTTHRESHOLD("FailedOperation.SilentThreshold", "实人检测未达到通过标准。"),
    SILENTTOOSHORT("FailedOperation.SilentTooShort", "视频录制时间过短，请录制2秒以上的视频。"),
    UNKNOWN("FailedOperation.UnKnown", "内部未知错误。"),
    INVALIDPARAMETER("InvalidParameter", "参数错误。"),
    INVALIDPARAMETERVALUE("InvalidParameterValue", "参数取值错误。"),
    UNAUTHORIZEDOPERATION("UnauthorizedOperation", "未授权操作。"),
    ARREARS("UnauthorizedOperation.Arrears", "帐号已欠费。"),
    NONAUTHORIZE("UnauthorizedOperation.NonAuthorize", "账号未实名。"),
    NONACTIVATED("UnauthorizedOperation.Nonactivated", "未开通服务。"),
    UNSUPPORTEDOPERATION("UnsupportedOperation", "操作不支持。");

    private String code;

    private String desc;

    public static FaceContrastResultEnum getContrastResult(String code) {
        log.warn("=++++++++++++{}", code);
        FaceContrastResultEnum tce = null;
        for (FaceContrastResultEnum em : FaceContrastResultEnum.values()) {
            if (em.getCode().equals(code)) {
                tce = em;
            }
        }
        if (tce == null) {
            return null;
        }
        return tce;
    }
}
