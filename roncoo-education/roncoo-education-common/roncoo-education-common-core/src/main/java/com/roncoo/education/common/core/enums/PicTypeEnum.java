package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 关联类型
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum PicTypeEnum {

	CAPTION(1, "试卷题目"), RESULT(2, "试卷答案");

	private Integer code;

	private String desc;
}
