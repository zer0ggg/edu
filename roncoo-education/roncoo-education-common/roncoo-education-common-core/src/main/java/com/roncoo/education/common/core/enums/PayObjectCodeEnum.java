/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 支付渠道编码
 *
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum PayObjectCodeEnum {

    WX_PAY_APP("官方-微信APP"),
    WX_PAY_H5("官方-微信H5"),
    WX_PAY_XCX("官方-微信小程序"),
    WX_PAY_SCAN("官方-微信扫码"),
    ALI_PAY_APP("官方-支付宝APP"),
    ALI_PAY_H5("官方-支付宝H5"),
    ALI_PAY_SCAN("官方-支付宝扫码"),
    RONCOO_PAY_WX_SCAN("龙果-微信扫码"),
    RONCOO_PAY_WX_H5("龙果-微信H5"),
    RONCOO_PAY_WX_XCX("龙果-微信小程序"),
    RONCOO_PAY_WX_APP("龙果-微信APP"),
    RONCOO_PAY_ALI_SCAN("龙果-支付宝扫码"),
    RONCOO_PAY_ALI_H5("龙果-支付宝H5"),
    RONCOO_PAY_ALI_APP("龙果-支付宝APP");

    private String desc;
}
