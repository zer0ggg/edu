package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否正确答案(1:正确;2:错误)"
 */
@Getter
@AllArgsConstructor
public enum IsCorrectEnum {

    YES(1, "正确"), NO(0, "错误");

    private Integer code;

    private String desc;
}
