package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 考试试题状态
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamProblemStatusEnum {

    /**
     * 错误，表示得分为0
     */
    NO(1, "错误"),

    /**
     * 正确，表示得分大于0
     */
    YES(2, "正确"),

    /**
     * 表示未审核
     */
    WAIT(3, "待评分");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
