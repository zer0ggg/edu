package com.roncoo.education.common.core.base;

import java.lang.annotation.*;

/**
 * 系统日志注解
 *
 * 注意：注解必须要在@ApiOperation下面，否则不生效
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

	String value() default "操作日志";

	/**
	 * 存入缓存的key前缀，避免key冲突
	 *
	 * @return
	 */
	String key() default "";

	/**
	 * 是否进行更新操作，会和@SysCacheLog的数据进行对比更新
	 * @return
	 */
	boolean isUpdate() default false;

}
