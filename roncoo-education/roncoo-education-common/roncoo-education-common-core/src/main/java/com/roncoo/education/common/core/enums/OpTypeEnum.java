/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户操作类型(1收藏，2点赞)
 * 
 */
@Getter
@AllArgsConstructor
public enum OpTypeEnum {

	COLLECTION(1, "收藏"), PRAISE(2, "点赞");

	private Integer code;

	private String desc;

}
