package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否存在封面(1,存在；0,不存在)
 * 
 */
@Getter
@AllArgsConstructor
public enum IsImgEnum {

	YES(1, "存在"), NO(0, "不存在");

	private Integer code;

	private String desc;
}
