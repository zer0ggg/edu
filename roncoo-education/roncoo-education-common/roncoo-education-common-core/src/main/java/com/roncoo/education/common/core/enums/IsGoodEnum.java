/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否存在最佳回答(1有，0无)
 * 
 * @author wuyun
 *
 */
@Getter
@AllArgsConstructor
public enum IsGoodEnum {

	YES(1, "有"), NO(0, "无");

	private Integer code;

	private String desc;

}
