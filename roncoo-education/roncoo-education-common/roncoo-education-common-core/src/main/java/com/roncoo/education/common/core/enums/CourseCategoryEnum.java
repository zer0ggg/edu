/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author LHR
 */
@Getter
@AllArgsConstructor
public enum CourseCategoryEnum {

    ORDINARY(1, "点播", ""), LIVE(2, "直播", "red"), RESOURCES(4, "文库", "blue");

    private Integer code;

    private String desc;

    private String color;
}
