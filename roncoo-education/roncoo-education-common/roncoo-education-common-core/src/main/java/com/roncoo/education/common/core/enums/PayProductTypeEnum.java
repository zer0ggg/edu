/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态码描述
 *
 * @author hugovon
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum PayProductTypeEnum {

    WEIXIN_T1("10000101", "微信T1扫码支付"),
    WEIXIN_T1_H5("10000201", "微信T1支付H5"),
    WEIXIN_T1_XCX("10000701", "微信T1小程序支付"),
    WEIXIN_T1_APP("10000401", "微信T1APP支付"),
    ZHIFUBAO_T1("20000301", "支付宝T1扫码支付"),
    ZHIFUBAO_T1_H5("20000201", "支付宝T1支付H5");

    private String code;

    private String desc;

}
