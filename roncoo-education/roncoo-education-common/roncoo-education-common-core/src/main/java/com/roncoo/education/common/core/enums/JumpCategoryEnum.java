/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 跳转分类(1:点播;2:直播,3文库,4试卷,5:资讯)
 *
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum JumpCategoryEnum {
    ORDINARY(1, "点播"),
    LIVE(2, "直播"),
    RESOURCE(3, "文库"),
    EXAM(4, "试卷"),
    INFOMATION(5, "资讯");

    private Integer code;

    private String desc;

}
