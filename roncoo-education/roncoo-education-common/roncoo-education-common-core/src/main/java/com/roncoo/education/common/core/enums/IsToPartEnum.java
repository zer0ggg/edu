/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 发送对象
 * 
 * @author Administrator
 *
 */
@Getter
@AllArgsConstructor
public enum IsToPartEnum {

	YES(1, "指定用户", "red"), NO(0, "所有用户", "");

	private Integer code;

	private String desc;

	private String color;

}
