package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 考试系统评阅
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamSysAuditEnum {

    /**
     * 未知，未评阅的为未知状态
     */
    UNKNOWN(1, "未知"),

    /**
     * 系统自动评阅
     */
    SYSTEM(2, "系统评阅"),

    /**
     * 需要人工镜像评阅
     */
    HANDWORK(3, "人工评阅");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
