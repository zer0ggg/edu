/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 */
@Getter
@AllArgsConstructor
public enum ZoneCategoryEnum {

	ORDINARY(1, "点播", ""), //
	LIVE(2, "直播", ""), //
	RESOURCE(4, "文库", ""), //
	//
	EXAM(5, "试卷", ""), //
	
	//
	Blog(7, "博客", ""), //
	INFOMATION(8, "资讯", ""), //
	DEFINITION(9, "其他", ""),//
	QUESTIONS(12, "问答", "");//

	private Integer code;

	private String desc;

	private String color;

}
