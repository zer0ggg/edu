/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum PayTypeEnum {

    // 微信
    WEIXIN(1, "微信扫码支付"), APP_WEIXIN(6, "微信APP支付"), XCX(5, "微信小程序支付"), H5_WEIXIN(9, "微信H5支付"),

    // 支付宝
    ALIPAY(2, "支付宝扫码支付"), APP_ALIPAY(7, "支付宝APP支付"), H5_ALIPAY(8, "支付宝H5支付"),

    // 线下支付
    MANUAL(4, "线下支付");

    private Integer code;

    private String desc;

    public static PayTypeEnum getByCode(int value) {
        for (PayTypeEnum enums : PayTypeEnum.values()) {
            if (value == enums.getCode()) {
                return enums;
            }
        }
        return null;
    }
}
