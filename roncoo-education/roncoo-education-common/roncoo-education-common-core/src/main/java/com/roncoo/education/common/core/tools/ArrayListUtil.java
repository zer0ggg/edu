/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 队列属性复制
 *
 * @author wujing
 */
@Slf4j
public final class ArrayListUtil {

    private ArrayListUtil() {
    }

    /**
     * 队列属性复制
     *
     * @param source 源数据
     * @param clazz  目标类
     * @return 目标数组
     */
    public static <T> List<T> copy(List<?> source, Class<T> clazz) {
        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }
        List<T> res = new ArrayList<>(source.size());
        for (Object o : source) {
            T t = null;
            try {
                t = clazz.newInstance();
                BeanUtils.copyProperties(o, t);
            } catch (Exception e) {
                log.error("请求失败", e);
            }
            res.add(t);
        }
        return res;
    }

}
