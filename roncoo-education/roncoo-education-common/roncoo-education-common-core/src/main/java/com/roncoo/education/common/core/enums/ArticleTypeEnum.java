/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 1:文章类型
 *
 */
@Getter
@AllArgsConstructor
public enum ArticleTypeEnum {

	BLOG(1, "博客"), INFORMATION(2, "资讯");

	private Integer code;

	private String desc;

    public static ArticleTypeEnum getByCode(int value) {
        for (ArticleTypeEnum articleTypeEnum : ArticleTypeEnum.values()) {
            if (value == articleTypeEnum.getCode()) {
                return articleTypeEnum;
            }
        }
        return null;
    }
}
