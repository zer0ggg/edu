/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 博客栏目(1最新文章，2热门文章，3推荐文章)
 * 
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum SectionsEnum {

	NEW(1, "最新文章"), HOT(2, "热门文章"), RECOMMEND(3, "推荐文章");

	private Integer code;

	private String desc;

}
