package com.roncoo.education.common.core.faceid;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.faceid.v20180301.FaceidClient;
import com.tencentcloudapi.faceid.v20180301.models.LivenessCompareRequest;
import com.tencentcloudapi.faceid.v20180301.models.LivenessCompareResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;
import sun.misc.BASE64Encoder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@Slf4j
public final class  FaceContrastUtil {

    private FaceContrastUtil() {
    }

    public static FaceContrastResult contrans(String secretId, String secretKey, String imageBase64, String videoBase64) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("faceid.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            FaceidClient client = new FaceidClient(cred, "ap-guangzhou", clientProfile);

            Map<String, Object> map = new HashedMap<>();
            map.put("Action", "LivenessCompare");//公共参数，本接口取值：LivenessCompare
            map.put("Version", "2018-03-01");//公共参数，本接口取值：2018-03-01
            map.put("Region", "ap-guangzhou");//地域列表
            map.put("ImageBase64", imageBase64);//用于人脸比对的照片，图片的BASE64值；BASE64编码后的图片数据大小不超过3M，仅支持jpg、png格式
            map.put("VideoBase64", videoBase64);//用于活体检测的视频，视频的BASE64值；BASE64编码后的大小不超过8M，支持mp4、avi、flv格式
            map.put("LivenessType", "SILENT");//活体检测类型，LIP为数字模式，ACTION为动作模式，SILENT为静默模式
            //map.put("ValidateData", "");//数字模式传参：数字验证码(1234)，需先调用接口获取数字验证码；动作模式传参：传动作顺序(2,1 or 1,2)，需先调用接口获取动作顺序；静默模式传参：空。
            Map<String, Integer> bestFrameMap = new HashedMap<>();
            bestFrameMap.put("BestFrameNum", 1);
            String optional = JSONUtil.toJsonStr(bestFrameMap);
            map.put("Optional", optional);//额外配置，传入JSON字符串。{"BestFrameNum": 2 //需要返回多张最佳截图，取值范围1-10 }
            LivenessCompareRequest req = LivenessCompareRequest.fromJsonString(JSONUtil.toJsonStr(map), LivenessCompareRequest.class);
            LivenessCompareResponse resp = client.LivenessCompare(req);
            return JSONObject.parseObject(LivenessCompareRequest.toJsonString(resp), FaceContrastResult.class);
            //返回结果{"BestFrameBase64":"","Sim":-1.0,"Result":"FailedOperation.SilentDetectFail","Description":"实人检测失败","RequestId":"d3a9b06a-1fc2-4462-ba75-45a17c8b276a"}
        } catch (TencentCloudSDKException e) {
            log.error(e.toString());
        }
        return null;
    }

    public static String getImageStr(String file) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        InputStream in = null;
        byte[] data = null;
        // 读取图片字节数组
        try {
            in = new FileInputStream(file);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);// 返回Base64编码过的字节数组字符串
    }

}
