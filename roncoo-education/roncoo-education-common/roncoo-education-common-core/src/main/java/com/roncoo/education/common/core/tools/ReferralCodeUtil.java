/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import java.security.SecureRandom;
import java.util.Random;

/**
 * 随机生成6位数字+大写字母的邀请码
 * 
 * @author wuyun
 */
public final class ReferralCodeUtil {
	
	private ReferralCodeUtil() {
	}

	// 生成随机数字和字母,
	public static String getStringRandom() {
		String val = "";
		Random random = new SecureRandom ();
		int length = 6;
		for (int i = 0; i < length; i++) {
			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 输出字母还是数字
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 输出是大写字母还是小写字母
				int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
				val += (char) (random.nextInt(26) + temp);
			} else if ("num".equalsIgnoreCase(charOrNum)) {
				val += String.valueOf(random.nextInt(10));
			}
		}
		return val.toUpperCase();
	}

}
