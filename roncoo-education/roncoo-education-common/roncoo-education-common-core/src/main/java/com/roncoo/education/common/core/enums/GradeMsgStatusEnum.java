package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级考试 常用站内信类型
 *
 * @author zkpc
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeMsgStatusEnum {

    APPLY(1, "申请"),

    APPROVAL(2, "同意"),

    REJECTION(3, "拒绝"),

    ADD(4, "加入"),

    QUIT(4, "退出"),

    KICK_OUT(4, "剔除"),

    SET_ADMIN(4, "设置管理员"),

    CANCEL_ADMIN(4, "取消管理员");
    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
