package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 收藏类型
 *
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum CollectionTypeEnum {

    COURSE(1, "课程"), CHAPTER(2, "章节"), PERIOD(3, "课时"), EXAM(4, "试卷"), TITLE(5, "题目");

    private Integer code;

	private String desc;
}
