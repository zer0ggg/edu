package com.roncoo.education.common.core.tools;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.style.Style0;

import java.util.ArrayList;
import java.util.List;

/**
 * app消息推送
 *
 * @author keyinghao
 */
public class AppPushUtil {

    // 定义常量, appId、appKey、masterSecret 采用本文档 "第二步 获取访问凭证 "中获得的应用配置

    private static String url = "http://sdk.open.api.igexin.com/apiex.htm";

    /**
     *
     * @param appId
     * @param appKey
     * @param masterSecret
     * @param title 标题
     * @param text 内容
     * @param transmissionContent 传递自定义消息、自定义消息，可以是json 字符串
     * @return
     */
    public static String AppPush(String appId ,String appKey, String masterSecret, String title, String text, String transmissionContent) {
        IGtPush push = new IGtPush(url, appKey, masterSecret);

        Style0 style = new Style0();
        // STEP2：设置推送标题、推送内容
        style.setTitle(title);
        style.setText(text);
        // 注释采用默认图标
        // style.setLogo("");  // 设置推送图标
        // STEP3：设置响铃、震动等推送效果
        // 设置响铃
        style.setRing(true);
        // 设置震动
        style.setVibrate(true);

        // STEP4：选择通知模板
        NotificationTemplate template = new NotificationTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setStyle(style);
        // 点击消息打开应用
        template.setTransmissionType(1);
        // 传递自定义消息
        // 自定义消息，可以是json 字符串
        template.setTransmissionContent(transmissionContent);

        // STEP5：定义"AppMessage"类型消息对象,设置推送消息有效期等推送参数
        List<String> appIds = new ArrayList<String>();
        appIds.add(appId);
        AppMessage message = new AppMessage();
        message.setData(template);
        message.setAppIdList(appIds);
        message.setOffline(true);
        // 时间单位为毫秒
        message.setOfflineExpireTime(1000 * 600);

        // STEP6：执行推送
        IPushResult ret = push.pushMessageToApp(message);
        return ret.getResponse().toString();
    }

}

