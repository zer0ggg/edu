package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 年级验证设置枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeVerificationSetEnum {

    /**
     * 添加班级时，不要要验证直接通过
     */
    ANYONE(1, "允许任何人加入"),

    /**
     * 加入班级时需要通过讲师或管理员审核才能加入
     */
    VERIFICATION(2, "加入时需要验证");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取年级验证设置枚举
     *
     * @param code 编码
     * @return 面积验证设置枚举
     */
    public static GradeVerificationSetEnum byCode(Integer code) {
        // 判空
        if (code == null) {
            return null;
        }

        // 匹配
        GradeVerificationSetEnum[] enums = GradeVerificationSetEnum.values();
        for (GradeVerificationSetEnum verificationSetEnum : enums) {
            if (verificationSetEnum.getCode().equals(code)) {
                return verificationSetEnum;
            }
        }
        return null;
    }
}
