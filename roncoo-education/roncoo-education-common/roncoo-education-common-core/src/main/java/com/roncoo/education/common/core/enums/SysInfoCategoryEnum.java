/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 配置信息分类（1站点信息，2系统信息，3阿里云信息，4视频信息，5微信信息，6小程序）
 *
 * @author zkpc
 */
@Getter
@AllArgsConstructor
public enum SysInfoCategoryEnum {

    Website(1, "站点信息"),
    SYS(2, "系统信息"),
    ALI_YUN(3, "阿里云"),
    Video(4, "视频"),
    WeChat(5, "微信"),
    XCX(5, "小程序");

    private Integer code;

    private String desc;

}
