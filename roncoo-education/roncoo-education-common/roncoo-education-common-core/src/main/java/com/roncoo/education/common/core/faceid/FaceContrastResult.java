/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.faceid;

import lombok.Data;

import java.io.Serializable;

@Data
public class FaceContrastResult implements Serializable {

	/**
	 * 验证通过后的视频最佳截图照片，照片为BASE64编码后的值，jpg格式。
	 */
	private String BestFrameBase64;
	/**
	 * 相似度，取值范围 [0.00, 100.00]。推荐相似度大于等于70时可判断为同一人，可根据具体场景自行调整阈值（阈值70的误通过率为千分之一，阈值80的误通过率是万分之一）。
	 */
	private Float Sim;
	/**
	 * 参考FaceContrastResultEnum
	 */
	private String Result;
	/**
	 * 业务结果描述。
	 */
	private String Description;
	/**
	 * 最佳截图列表，仅在配置了返回多张最佳截图时返回。注意：此字段可能返回 null，表示取不到有效值。
	 */
	private String BestFrameList;
	/**
	 * 唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
	 */
	private String RequestId;

	private static final long serialVersionUID = 1L;

}
