/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.base;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 基础类
 * 
 * @author wujing
 */
public class BaseBiz extends Base {

	protected static final ExecutorService CALLBACK_EXECUTOR = Executors.newFixedThreadPool(100);
	
}
