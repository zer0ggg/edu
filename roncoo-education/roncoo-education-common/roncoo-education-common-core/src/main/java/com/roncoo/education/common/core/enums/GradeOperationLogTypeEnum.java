package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级操作日志类型枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeOperationLogTypeEnum {

    /**
     * 创建班级
     */
    CREATE_GRADE(1, "创建班级"),

    /**
     * 班级成员主动退出班级
     */
    QUIT_GRADE(2, "退出班级"),

    /**
     * 讲师或者管理员移除班内成员
     */
    KICK_OUT_GRADE(3, "踢出班级"),

    /**
     * 讲师解散班级
     */
    DISBAND_GRADE(4, "解散班级"),

    /**
     * 修改班级
     */
    UPDATE_GRADE(5, "修改班级");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取班级操作日志类型枚举
     *
     * @param code 编码
     * @return 面积验证设置枚举
     */
    public static GradeOperationLogTypeEnum byCode(Integer code) {
        // 判空
        if (code == null) {
            return null;
        }

        // 匹配
        GradeOperationLogTypeEnum[] enums = GradeOperationLogTypeEnum.values();
        for (GradeOperationLogTypeEnum operationLogTypeEnum : enums) {
            if (operationLogTypeEnum.getCode().equals(code)) {
                return operationLogTypeEnum;
            }
        }
        return null;
    }
}
