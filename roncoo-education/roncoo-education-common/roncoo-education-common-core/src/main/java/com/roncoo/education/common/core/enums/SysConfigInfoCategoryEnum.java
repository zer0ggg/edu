/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 信息分类(0:站点信息;1:系统信息,第三方)
 *
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum SysConfigInfoCategoryEnum {

    WEBSITE(0, "站点信息"), SYS(1, "系统信息"), THIRD_PARTY(2, "第三方");

    private Integer code;

    private String desc;

}
