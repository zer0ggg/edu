package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级学生考试展示状态
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeStudentExamShowStatus {

    /**
     * 根据学生考试状态进行判断展示
     */
    NORMAL(1, "正常流程"),

    /**
     * 考试已经结束，但是可以补交
     */
    TIMEOUT_COMPENSATE(2, "超时可以补交"),

    /**
     * 考试已经结束，但是不可以补交
     */
    TIMEOUT_NO_COMPENSATE(3, "超时不可以补交");


    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
