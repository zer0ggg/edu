package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 客戶端类型（1:PC；2：安卓；3：IOS）
 */
@Getter
@AllArgsConstructor
public enum AlientTypeEnum {

    PC(1, "PC"), ANDROID(2, "安卓"), IOS(3, "IOS");

    private Integer code;

    private String desc;


}
