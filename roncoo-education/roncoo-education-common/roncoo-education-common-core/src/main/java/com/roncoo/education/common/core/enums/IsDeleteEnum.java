package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否可删除
 * 
 * @author kyh
 *
 */
@Getter
@AllArgsConstructor
public enum IsDeleteEnum {

	YES(1, "是"), NO(0, "否");

	private Integer code;

	private String desc;

}
