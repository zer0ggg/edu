package com.roncoo.education.common.core.enums;

public enum WxTradeStateEnum {
	SUCCESS("支付成功"), REFUND("转入退款"), NOTPAY("未支付"), CLOSED("已关闭"), USERPAYING("用户支付中"), PAYERROR("支付失败"),
	
	// 异步通知，业务结果可能返回
	FAIL("失败");

	private String desc;

	private WxTradeStateEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}
}
