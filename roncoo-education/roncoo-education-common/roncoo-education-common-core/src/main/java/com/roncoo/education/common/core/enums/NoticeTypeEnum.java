/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *提醒类型(1站内信,2微信,3短信) 可多选
 * 
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum NoticeTypeEnum {

	MSG(1, "站内信"), WECHAT(2, "微信"), SHORTMSG(3, "短信");

	private Integer code;

	private String desc;

	public static NoticeTypeEnum getNoticeType(int value) {
		for (NoticeTypeEnum actTypeEnum : NoticeTypeEnum.values()) {
			if (value == actTypeEnum.getCode()) {
				return actTypeEnum;
			}
		}
		return null;
	}
}
