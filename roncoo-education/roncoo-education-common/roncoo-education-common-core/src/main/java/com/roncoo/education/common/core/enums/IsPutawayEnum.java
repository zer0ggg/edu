/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author LHR
 */
@Getter
@AllArgsConstructor
public enum IsPutawayEnum {

    /**
     * 上架
     */
    YES(1, "上架", ""),

    /**
     * 下架
     */
    NO(0, "下架", "red");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取上下架枚举
     *
     * @param code 编码
     * @return 枚举
     */
    public static IsPutawayEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        IsPutawayEnum[] isPutawayEnums = IsPutawayEnum.values();
        for (IsPutawayEnum isPutawayEnum : isPutawayEnums) {
            if (isPutawayEnum.getCode().equals(code)) {
                return isPutawayEnum;
            }
        }
        return null;
    }
}
