package com.roncoo.education.common.core.tools;

import cn.hutool.core.util.URLUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
public final class DownloadUtil {

    private DownloadUtil() {
    }

    public static String download(String url, String filepath) {
        try {
            return downloadFile(url, filepath);
        } catch (IOException e) {
            log.error("下载异常", e);
        }
        return null;
    }

    /**
     * 下载文件到本地
     *
     * @param url      文件地址
     * @param filepath 存放路径
     * @return
     * @throws IOException
     */
    public static String downloadFile(String url, String filepath) throws IOException {
        // 设置缓冲区大小为10M
        int cache = 1024 * 1024 * 10;
        // 判断文件夹是否存在，不存在则创建
        File path = new File(filepath);
        if (!path.exists() && !path.isDirectory()) {
            path.mkdir();
        }
        InputStream is = null;
        FileOutputStream fileout = null;
        String endurl = URLUtil.decode(url, "utf-8");
        HttpClient client = HttpClients.createDefault();
        try {
            String query = URLUtil.url(endurl).getQuery();
            String filename = endurl.substring(url.lastIndexOf('/') + 1).replace("?" + query, ""); // 获取文件名

            File file = new File(filepath + filename);
            file.getParentFile().mkdirs();// 创建文件

            fileout = new FileOutputStream(file);
            byte[] buffer = new byte[cache];
            int ch = 0;
            is = client.execute(new HttpGet(endurl)).getEntity().getContent();
            while ((ch = is.read(buffer)) != -1) {
                fileout.write(buffer, 0, ch);
            }
            return filename;
        } catch (Exception e) {
            log.error("异常", e);
        } finally {
            if (is != null) {
                is.close();
            }
            if (fileout != null) {
                fileout.close();
            }
        }
        return null;
    }
}
