/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @author wujing
 */
public final class JWTUtil {

	protected static final Logger logger = LoggerFactory.getLogger(JWTUtil.class);

	private static final String TOKEN_SECRET = "eyJhbGciOiJIUzI1NiJ9";
	private static final String ISSUER = "RONCOO";
	public static final String USERNO = "userNo";
	public static final String TYPE = "type";
	public static final String LOGIN_NAME = "login_name";

	/**
	 * 1个月
	 */
	public static final Long DATE = 30 * 24 * 3600 * 1000L;


	/**
	 * @param userNo 用户编号
	 * @param date   过期时间
	 * @param type   类型
	 * @return token
	 */
	public static String create(Long userNo, Long date, String type) {
		try {
			return JWT.create().withIssuer(ISSUER).withClaim(USERNO, userNo.toString()).withClaim(TYPE, type).withExpiresAt(new Date(System.currentTimeMillis() + date)).sign(Algorithm.HMAC256(TOKEN_SECRET));
		} catch (IllegalArgumentException | JWTCreationException e) {
			logger.error("JWT生成失败", e);
			return "";
		}
	}

	/**
	 * @param loginName 用户编号/登录账号
	 * @param date      有效时间
	 * @param type      类型
	 * @return token
	 */
	public static String createByLoginName(String loginName, Long date, String type) {
		try {
			return JWT.create().withIssuer(ISSUER).withClaim(LOGIN_NAME, loginName).withClaim(TYPE, type).withExpiresAt(new Date(System.currentTimeMillis() + date)).sign(Algorithm.HMAC256(TOKEN_SECRET));
		} catch (IllegalArgumentException | JWTCreationException e) {
			logger.error("JWT生成失败", e);
			return "";
		}
	}

	/**
	 * 解码token
	 *
	 * @param token 令牌信息
	 * @return 解码信息
	 */
	public static DecodedJWT verify(String token) throws JWTVerificationException, IllegalArgumentException, UnsupportedEncodingException {
		return JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer(ISSUER).build().verify(token);
	}

	/**
	 * 获取用户编号
	 *
	 * @param decodedJWT 解码后JWT信息
	 * @return 用户编号
	 */
	public static Long getUserNo(DecodedJWT decodedJWT) {
		return Long.valueOf(decodedJWT.getClaim(USERNO).asString());
	}

	/**
	 * 获取登录名称
	 *
	 * @param decodedJWT 解码后JWT信息
	 * @return 登录名称
	 */
	public static String getLoginName(DecodedJWT decodedJWT) {
		return decodedJWT.getClaim(LOGIN_NAME).asString();
	}

	/**
	 * 获取类型
	 *
	 * @param decodedJWT 解码后JWT信息
	 * @return 类型
	 */
	public static String getType(DecodedJWT decodedJWT) {
		return decodedJWT.getClaim(TYPE).asString();
	}

}
