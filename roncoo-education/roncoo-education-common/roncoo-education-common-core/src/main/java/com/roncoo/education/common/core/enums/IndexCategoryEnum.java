/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 索引分类(1文章, 2课程,3博客,4问答)
 * 
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum IndexCategoryEnum {

	ARTICLE(1, "文章"), COURSE(2, "课程"), WEBLOG(3, "博客"), QUESTIONS(4, "问答");

	private Integer code;

	private String desc;

}
