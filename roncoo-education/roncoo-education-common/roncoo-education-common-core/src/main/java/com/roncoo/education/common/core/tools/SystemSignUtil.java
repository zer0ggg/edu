package com.roncoo.education.common.core.tools;

import cn.hutool.crypto.SecureUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

/**
 * 系统签名工具类
 *
 * @author LYQ
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class SystemSignUtil {

    /**
     * 签名
     *
     * @param param 待加密参数
     * @param salt  加密盐
     * @return 签名结果
     */
    public static String sign(String param, String salt) {
        return SecureUtil.sha256(param + salt);
    }

    /**
     * 校验签名
     *
     * @param param  待加密参数
     * @param salt   加密盐
     * @param target 校验目标
     * @return 校验结果
     */
    public static Boolean verifySign(String param, String salt, String target) {
        String sign = sign(param, salt);
        return sign.equals(target);
    }
}
