package com.roncoo.education.common.core.tools;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Bean校验工具
 *
 * @author LYQ
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BeanValidateUtil {

	/**
	 * 获取校验器
	 */
	private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	/**
	 * 校验请求对象
	 *
	 * @param t   请求对象
	 * @param <T> 请求对象类型
	 * @return 校验结果
	 */
	public static <T> ValidationResult validate(T t) {
		try {
			Set<ConstraintViolation<T>> constraintViolationSet = validator.validate(t);
			if (constraintViolationSet == null || constraintViolationSet.isEmpty()) {
				return new ValidationResult(true, null);
			}
			List<String> errMsgList = new ArrayList<>();
			for (ConstraintViolation<T> constraintViolation : constraintViolationSet) {
				errMsgList.add(constraintViolation.getMessage());
			}
			return new ValidationResult(false, errMsgList);
		} catch (Exception e) {
			return new ValidationResult(false, Collections.singletonList(e.getMessage()));
		}
	}

	/**
	 * 校验结果
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ValidationResult {
		/**
		 * 是否通过
		 */
		private boolean pass;

		/**
		 * 错误信息集合
		 */
		private List<String> errMsgList;

		public String getErrMsgString() {
			if (errMsgList == null || errMsgList.isEmpty()) {
				return null;
			}

			StringBuilder sb = new StringBuilder();
			for (String errMsg : errMsgList) {
				sb.append(errMsg).append(";");
			}
			sb.deleteCharAt(sb.length() - 1);
			return sb.toString();
		}
	}
}
