package com.roncoo.education.common.core.base;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.roncoo.education.common.core.enums.ResultEnum;

public class SentinelException extends BlockException {

  /** 异常码 */
  protected int code;

  protected String message;

  public SentinelException() {
    super(ResultEnum.ERROR.getDesc());

    this.code = ResultEnum.ERROR.getCode();
    this.message = ResultEnum.ERROR.getDesc();
  }

  public SentinelException(ResultEnum resultEnum) {
    super(resultEnum.getDesc());
    this.code = resultEnum.getCode();
    this.message = resultEnum.getDesc();
  }

  public SentinelException(String message) {
    super(message);
    this.code = ResultEnum.ERROR.getCode();
    this.message = message;
  }

  public SentinelException(int code, String message) {
    super(message);
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }
}
