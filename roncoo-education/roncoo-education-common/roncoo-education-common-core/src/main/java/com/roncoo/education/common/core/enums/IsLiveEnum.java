/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否拥有直播功能
 * 
 * @author liaoh
 *
 */
@Getter
@AllArgsConstructor
public enum IsLiveEnum {

	YES(1, "开启", "green"), NO(0, "关闭", "red");

	private Integer code;

	private String desc;

	private String color;
}
