package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ExamStatusEnum {

    /**
     * 考试中
     */
    NOT_OVER(1, "考试中"),

    /**
     * 考试结束
     */
    FINISH(2, "考试完成"),

    /**
     * 评阅完成
     */
    COMPLETE(3, "评阅完成");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;
}
