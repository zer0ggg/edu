/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 视频标签分类
 *
 */
@Getter
@AllArgsConstructor
public enum VideoTagEnum {

	COURSE("course", "录播课程"), LIVE_PLAYBACK("live_playback", "直播回放"), EXAM("exam", "试卷");

	private String code;

	private String desc;

	public static VideoTagEnum get(String code) {
		for (VideoTagEnum videoTagEnum : VideoTagEnum.values()) {
			if (code == videoTagEnum.getCode()) {
				return videoTagEnum;
			}
		}
		return null;
	}
}
