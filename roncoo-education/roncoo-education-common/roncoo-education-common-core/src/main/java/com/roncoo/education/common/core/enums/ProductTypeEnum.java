/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Quanf
 */
@Getter
@AllArgsConstructor
public enum ProductTypeEnum {

    ORDINARY(1, "点播", ""), LIVE(2, "直播", "red"), VIP(3, "会员", ""), RESOURCES(4, "文库", "blue"), EXAM(5, "试卷", "");

    private Integer code;

    private String desc;

    private String color;
}
