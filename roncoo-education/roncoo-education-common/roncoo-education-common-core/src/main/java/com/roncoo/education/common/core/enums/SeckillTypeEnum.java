/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum SeckillTypeEnum {

	PEOPLE(1, "限制人数"), TIME(2, "限制时间");

	private Integer code;

	private String desc;

}
