/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum WebsiteNavArticleEnum {

    /**
     * 正常
     */
    ARTICLE(1, "文章", ""),

    /**
     * 禁用
     */
    LINKURL(2, "外部链接", "red");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 显示颜色
     */
    private final String color;

    /**
     * 根据编码获取状态枚举
     *
     * @param code 编码
     * @return 状态枚举
     */
    public static WebsiteNavArticleEnum byCode(Integer code) {
        WebsiteNavArticleEnum[] enums = WebsiteNavArticleEnum.values();
        for (WebsiteNavArticleEnum statusIdEnum : enums) {
            if (statusIdEnum.getCode().equals(code)) {
                return statusIdEnum;
            }
        }
        return null;
    }
}
