/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 试题分类枚举类
 * @author forest
 *
 */
@Getter
@AllArgsConstructor
public enum ExamCategoryTypeEnum {

	SUBJECT(1, "试卷科目", "red"),
	YEAR(2, "试卷年份", "blue"),
	SOURCE(3, "试卷来源", "green"),
	DIFFICULTY(4, "试题难度", ""),
	CLASSIFICATION(5, "试题分类", ""),
	EMPHASIS(6, "考点分类", "");

	private Integer code;

	private String desc;

	private String color;


}
