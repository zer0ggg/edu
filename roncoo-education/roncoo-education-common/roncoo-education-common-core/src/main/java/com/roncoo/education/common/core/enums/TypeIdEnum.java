/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum TypeIdEnum {
    
	ORIGINAL(1, "原创"), RESHIPMENT(2, "转载");

    private Integer code;

    private String desc;

}
