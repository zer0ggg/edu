/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.config;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

/**
 * 配置文件读取工具类
 *
 * @author wujing
 */
@Slf4j
public final class SystemUtil {

    private SystemUtil() {
    }

    private static final Properties properties = new Properties();

    static {
        try {
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties"));
        } catch (IOException e) {
            log.error("配置文件读取失败", e);
        }
    }

    private static String getProperty(String keyName) {
        return properties.getProperty(keyName, "").trim();
    }

    public static final String VIDEO_PATH = getProperty("video_path").replace("${user.home}", System.getProperty("user.home"));
    public static final String PIC_PATH = getProperty("pic_path").replace("${user.home}", System.getProperty("user.home"));

    public static final String POLYV_UPLOAD_REMAKES = getProperty("polyv_upload_remakes").replace("${user.home}", System.getProperty("user.home"));
    public static final String POLYV_CALLBACK_REMAKES = getProperty("polyv_callback_remakes").replace("${user.home}", System.getProperty("user.home"));
    public static final String POLYV_UPLOAD_PARTITIONSIZE = getProperty("polyv_upload_partitionsize");
    public static final String POLYV_UPLOAD_THREADNUM = getProperty("polyv_upload_threadnum");

    public static final String WEB_LIVE_URL = "https://live.polyv.net/web-start/login?channelId=";
    public static final String POLYV_PLAYBACK_URL = "https://live.polyv.cn/watch/{channelId}?vid={vid}&userid={userid}&ts={ts}&sign={sign}";
    public static final String POLYV_GETCATAURL = "http://v.polyv.net/uc/services/rest?method=getCata&readtoken={READ_TOKEN}";
    public static final String POLYV_UPLOADVIDEO = "http://v.polyv.net/uc/services/rest?method=uploadfile";
    public static final String POLYV_CHANGECATAURL = "http://api.polyv.net/v2/video/{userid}/changeCata?vids={VIDS}&cataid={CATAID}&ptime={PTIME}&sign={SIGN}";
    public static final String POLYV_DELVIDEOBYID = "http://v.polyv.net/uc/services/rest?method=delVideoById&writetoken={WRITE_TOKEN}&vid={VID}";
    public static final String POLYV_DELETEVIDEO = "http://api.polyv.net/v2/video/{userid}/del-video";
    public static final String POLYV_QUESTION = "http://v.polyv.net/uc/services/rest";
    public static final String POLYV_GETTOKEN = "https://hls.videocc.net/service/v1/token";
    public static final String POLYV_LIVE_URL = "{domain}/watch/{channelId}?userid={userid}&ts={ts}&sign={sign}";

    public static final String HOST = "https://bankcard4c.shumaidata.com/bankcard4c";
}
