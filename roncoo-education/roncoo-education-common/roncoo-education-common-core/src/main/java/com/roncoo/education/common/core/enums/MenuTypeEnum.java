package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum {

    /**
     * 目录
     */
    DIRECTORY(0, "目录", true),

    /**
     * 菜单
     */
    MENU(1, "菜单", true),

    /**
     * 权限--不属于菜单类型，只在做菜单权限Tree展示时使用
     */
    PERMISSION(2, "权限", false);

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 是否展示（true：展示、false：不展示）--添加菜单时，根据该类型展示可添加数据
     */
    private final Boolean isShow;
}
