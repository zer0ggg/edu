package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否允许推广下级（1：允许，2：不允许）
 */
@Getter
@AllArgsConstructor
public enum IsAllowInvitationEnum {


    ALLOW(1, "允许"), NOT_ALLOW(2, "不允许");

    private Integer code;

    private String desc;

}
