/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否叠加优惠(1可叠加;0不可叠加)
 *
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum IsEnterpriseUserEnum {

    ENTERPRISE(1, "企业用户"), ORDINARY(0, "普通用户");

    private Integer code;

    private String desc;


}
