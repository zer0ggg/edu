/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum PayChannelEnum {

	RONCOO(1, "龙果支付"), OFFICIAL(2, "官方支付");

	private Integer code;

	private String desc;

}
