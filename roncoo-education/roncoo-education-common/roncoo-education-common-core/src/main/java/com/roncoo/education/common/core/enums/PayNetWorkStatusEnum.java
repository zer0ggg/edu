package com.roncoo.education.common.core.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通信状态枚举类
 *
 * @author Quanf
 */
@AllArgsConstructor
@Getter
public enum PayNetWorkStatusEnum {

    SUCCESS("请求成功"),

    FAILED("请求失败");

    /**
     * 描述
     */
    private String desc;

}
