/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.tools;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * http调用工具类
 *
 * @author wujing
 */
public final class HttpUtil {

	private static final String CHARSET_UTF_8 = "UTF-8";
	private static final int TIMEOUT = 10000;
	private static RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(15000).setConnectTimeout(15000).setConnectionRequestTimeout(15000).build();

	private HttpUtil() {
	}

	private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	private static SimpleClientHttpRequestFactory requestFactory = null;
	static {
		requestFactory = new SimpleClientHttpRequestFactory();
		requestFactory.setConnectTimeout(60000); // 连接超时时间，单位=毫秒
		requestFactory.setReadTimeout(60000); // 读取超时时间，单位=毫秒
	}
	private static RestTemplate restTemplate = new RestTemplate(requestFactory);

	public static JsonNode postForObject(String url, Map<String, Object> map) {
		logger.info("POST 请求， url={}，map={}", url, map.toString());
		return restTemplate.postForObject(url, map, JsonNode.class);
	}

	/**
	 *
	 * @param url
	 * @param param
	 * @return
	 */
	public static String postForPay(String url, Map<String, Object> param) {
		logger.info("POST 请求， url={}，map={}", url, param.toString());
		try {
			HttpPost httpPost = new HttpPost(url.trim());
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setConnectionRequestTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
			httpPost.setConfig(requestConfig);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			for (Map.Entry<String, Object> entry : param.entrySet()) {
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
			}
			StringEntity se = new UrlEncodedFormEntity(nvps, CHARSET_UTF_8);
			httpPost.setEntity(se);
			HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpPost);
			return EntityUtils.toString(httpResponse.getEntity(), CHARSET_UTF_8);
		} catch (Exception e) {
			logger.info("HTTP请求出错", e);
		}
		return "";
	}

	public static <T> T getForObject(String url, Map<String, Object> map, Class<T> clzss) {
		logger.info("GET 请求， url={}，map={}", url, map.toString());
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		return restTemplate.getForObject(url + "?" + sb, clzss);
	}

	public static String getHeadersForObjec(String url, BasicHeader header, Map<String, Object> map) {
		logger.info("GET 请求， url={}，headers={}， map={}", url, header.toString(), map.toString());
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		try {
			HttpGet httpGet = new HttpGet(url.trim() + "?" + sb);
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setConnectionRequestTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
			httpGet.setConfig(requestConfig);
			httpGet.setHeader(header);
			HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpGet);
			return EntityUtils.toString(httpResponse.getEntity(), CHARSET_UTF_8);
		} catch (Exception e) {
			logger.info("HTTP请求出错", e);
		}
		return "";
	}


	/**
	 *
	 * @param url
	 * @param param
	 * @return
	 */
	public static String post(String url, Map<String, Object> param) {
		logger.info("POST 请求， url={}，map={}", url, param.toString());
		try {
			HttpPost httpPost = new HttpPost(url.trim());
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(TIMEOUT).setConnectionRequestTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
			httpPost.setConfig(requestConfig);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			for (Map.Entry<String, Object> entry : param.entrySet()) {
				if (entry.getValue() != null) {
					nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
				}
			}
			StringEntity se = new UrlEncodedFormEntity(nvps, CHARSET_UTF_8);
			httpPost.setEntity(se);
			HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpPost);
			return EntityUtils.toString(httpResponse.getEntity(), CHARSET_UTF_8);
		} catch (Exception e) {
			logger.info("HTTP请求出错", e);
		}
		return "";
	}

	public static String post(String httpUrl, Map<String, Object> maps, String body) {
		StringBuilder url = new StringBuilder();
		url.append(httpUrl).append("?");
		for (Map.Entry<String, Object> map : maps.entrySet()) {
			url.append(map.getKey()).append("=").append(map.getValue()).append("&");
		}
		String urlStr = url.toString().substring(0, url.length() - 1);
		try {
			HttpPost httpPost = new HttpPost(urlStr);
			StringEntity entity = new StringEntity(body, Charset.forName("UTF-8"));
			httpPost.setEntity(entity);
			return sendHttpPost(httpPost);
		} catch (Exception e) {
			logger.info("HTTP请求出错", e);
			return null;
		}
	}

	/**
	 * @param httpUrl
	 * @param maps
	 * @param imgFile
	 * @return
	 * @author LHR
	 */
	public static String sendHttpPost(String httpUrl, Map<String, Object> maps, File file) {
		HttpPost httpPost = new HttpPost(httpUrl);// 创建httpPost
		MultipartEntityBuilder meBuilder = MultipartEntityBuilder.create();
		for (String key : maps.keySet()) {
			meBuilder.addPart(key, new StringBody(maps.get(key).toString(), ContentType.TEXT_PLAIN));
		}
		FileBody fileBody = new FileBody(file);
		meBuilder.addPart("imgfile", fileBody); // imgfile 图片对应参数名
		HttpEntity reqEntity = meBuilder.build();
		httpPost.setEntity(reqEntity);
		return sendHttpPost(httpPost);
	}

	private static String sendHttpPost(HttpPost httpPost) {
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		try {
			// 创建默认的httpClient实例.
			httpClient = HttpClients.createDefault();
			httpPost.setConfig(requestConfig);
			// 执行请求
			response = httpClient.execute(httpPost);
			return EntityUtils.toString(response.getEntity(), "UTF-8");
		} catch (Exception e) {
			logger.info("HTTP请求出错", e);
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					logger.info("HTTP请求出错", e);
				}
			}
			if (null != httpPost) {
				httpPost.releaseConnection();
			}
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.info("HTTP请求出错", e);
				}
			}
		}
		return "";
	}
}
