/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 直播状态
 * 
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum PolyvLiveStatusEnum {

	NOW("live", "正在直播"),
	
	END("end", "直播结束");

	private String code;

	private String desc;

}
