package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 答案展示类型枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeExamAnswerShowEnum {

    /**
     * 不展示
     */
    NO(1, "不展示"),

    /**
     * 交卷后马上展示
     */
    EXAM_SUBMIT(2, "交卷"),

    /**
     * 考试结束后才能展示
     */
    EXAM_COMPLETE(3, "考试完成"),

    /**
     * 自定义
     */
    CUSTOMIZE(4, "自定义");

    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据code获取班级考试答案展示
     *
     * @param code 编码
     * @return 班级考试答案展示
     */
    public static GradeExamAnswerShowEnum byCode(Integer code) {
        if (code == null) {
            return null;
        }

        GradeExamAnswerShowEnum[] enums = GradeExamAnswerShowEnum.values();
        for (GradeExamAnswerShowEnum showEnum : enums) {
            if (showEnum.getCode().equals(code)) {
                return showEnum;
            }
        }
        return null;
    }
}
