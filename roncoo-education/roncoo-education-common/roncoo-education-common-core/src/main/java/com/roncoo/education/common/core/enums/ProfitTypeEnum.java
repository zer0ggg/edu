package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 分润金额（1：推广；2：邀请）
 */
@Getter
@AllArgsConstructor
public enum ProfitTypeEnum {

    PROMOTE(1, "推广"), INVITATION(2, "邀请 ");

    private Integer code;

    private String desc;
}
