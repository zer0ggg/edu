package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录类型（1:PC；2：安卓；3：IOS; :微信小程序）
 */
@Getter
@AllArgsConstructor
public enum LognTypeEnum {

    PC(1, "PC"), ANDROID(2, "安卓"), IOS(3, "IOS"), XCX(4, "微信小程序");

    private Integer code;

    private String desc;
}
