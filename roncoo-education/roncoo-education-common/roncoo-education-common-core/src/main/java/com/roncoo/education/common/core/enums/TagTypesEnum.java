/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 博客类型标签（1公开，2私密，3草稿，4回收站）
 * 
 * @author wuyun
 *
 */
@Getter
@AllArgsConstructor
public enum TagTypesEnum {

	PUBLICITY(1, "公开"), PRIVACY(2, "私密"), DRAFT(3, "草稿"), TRASH(4, "回收站");

	private Integer code;

	private String desc;

}
