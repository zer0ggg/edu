/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.auth;

/**
 * @author lyq
 * @create 2017-10-11 16:26
 **/
public class AuthResult {

    private String resultCode;

    private String message;

    private boolean status;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AuthResult{" +
                "resultCode='" + resultCode + '\'' +
                ", message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
