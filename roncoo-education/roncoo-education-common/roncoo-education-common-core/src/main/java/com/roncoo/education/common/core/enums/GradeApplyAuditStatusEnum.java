package com.roncoo.education.common.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 班级申请审核状态枚举
 *
 * @author LYQ
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GradeApplyAuditStatusEnum {

    /**
     * 待审核，需要讲师或者管理员进行审核
     */
    WAIT(1, "待审核"),

    /**
     * 审核通过，加入班级
     */
    ASSENT(2, "同意"),

    /**
     * 审核失败，发送站内信
     */
    REFUSAL(3, "拒绝");
    /**
     * 编码
     */
    private final Integer code;

    /**
     * 描述
     */
    private final String desc;

    /**
     * 根据编码获取班级申请审核状态枚举
     *
     * @param code 编码
     * @return 面积验证设置枚举
     */
    public static GradeApplyAuditStatusEnum byCode(Integer code) {
        // 判空
        if (code == null) {
            return null;
        }

        // 匹配
        GradeApplyAuditStatusEnum[] enums = GradeApplyAuditStatusEnum.values();
        for (GradeApplyAuditStatusEnum applyAuditStatusEnum : enums) {
            if (applyAuditStatusEnum.getCode().equals(code)) {
                return applyAuditStatusEnum;
            }
        }
        return null;
    }
}
