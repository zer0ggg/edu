/**
 * Copyright 2019-现在 成都西交智汇大数据科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * 
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum CommentTypeEnum {


	MECOMMENT(1, "我评论别人的"), COMMENTME(2, "别人评论我的");

    private Integer code;

    private String desc;

}
