package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * PC端首页活动广告跳转链接
 * 
 * @author kyh
 */
@Getter
@AllArgsConstructor
public enum AdvUrlPEnum {

	COUPON("/sale/coupon", "优惠券"), DISCOUNT("/sale/discount", "立减"), SECKILL("/sale/spike", "秒杀");

	private String code;

	private String desc;

}
