/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 1:博客标签
 * 
 */
@Getter
@AllArgsConstructor
public enum LabelTypeEnum {

	WEBLOGLABEL(1, "博客"), QUESTIONS(2, "问答"), INFORMATION(3, "资讯");

	private Integer code;

	private String desc;
}
