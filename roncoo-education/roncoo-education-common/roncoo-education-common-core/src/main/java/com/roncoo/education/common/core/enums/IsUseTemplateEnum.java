/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否使用模板（1:是；0:否）
 * @author wuyun
 */
@Getter
@AllArgsConstructor
public enum IsUseTemplateEnum {

	YES(1, "使用"), NO(0, "未使用");

	private Integer code;

	private String desc;
}
