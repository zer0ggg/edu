package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IsWatchAccomplishEnum {

    NOT_ACCOMPLISH(1, "未完成"), ACCOMPLISH(2, "完成");

    private Integer code;

    private String desc;
}
