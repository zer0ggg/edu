package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Elasticsearch的Aliases别名
 * 
 * @author wuyun
 *
 */
@Getter
@AllArgsConstructor
public enum EsAliasesEnum {

	ARTICLE("ARTICLE", "文章"), COURSE("COURSE", "课程"), WEBLOG("WEBLOG", "博客"), QUESTIONS("QUESTIONS", "问答");

	private String code;

	private String desc;

}
