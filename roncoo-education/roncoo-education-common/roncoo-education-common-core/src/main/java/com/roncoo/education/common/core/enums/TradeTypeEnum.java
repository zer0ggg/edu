/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wujing
 */
@Getter
@AllArgsConstructor
public enum TradeTypeEnum {

    ONLINE(1, "线上支付"), OFFLINE(2, "线下支付");

    private Integer code;

    private String desc;

    public static TradeTypeEnum getByCode(int value) {
        for (TradeTypeEnum enums : TradeTypeEnum.values()) {
            if (value == enums.getCode()) {
                return enums;
            }
        }
        return null;
    }
}
