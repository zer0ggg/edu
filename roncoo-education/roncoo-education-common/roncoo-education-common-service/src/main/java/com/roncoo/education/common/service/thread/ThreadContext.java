package com.roncoo.education.common.service.thread;

public final class ThreadContext {

    private ThreadContext() {
    }

    private final static ThreadLocal<String> userNoLocal = new ThreadLocal<>();
    private final static ThreadLocal<String> loginNameLocal = new ThreadLocal<>();

    /**
     * 获取资源
     *
     * @return userId
     */
    public static Long userNo() {
        return Long.valueOf(userNoLocal.get());
    }

    /**
     * 添加资源
     *
     * @param val
     */
    public static void setUserNo(String val) {
        if (val == null) {
            removeUserId();
            return;
        }
        userNoLocal.set(val);
    }

    /**
     * 移除资源
     *
     * @return
     */
    public static void removeUserId() {
        userNoLocal.remove();
    }

    /**
     * 获取资源
     *
     * @return userId
     */
    public static String loginName() {
        return loginNameLocal.get();
    }

    /**
     * 添加资源
     *
     * @param val
     */
    public static void setloginName(String val) {
        if (val == null) {
            removeLoginName();
            return;
        }
        loginNameLocal.set(val);
    }

    /**
     * 移除资源
     *
     * @return
     */
    public static void removeLoginName() {
        loginNameLocal.remove();
    }

}
