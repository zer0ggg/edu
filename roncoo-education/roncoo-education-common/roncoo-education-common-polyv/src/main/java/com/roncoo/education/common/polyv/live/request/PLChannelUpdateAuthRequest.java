package com.roncoo.education.common.polyv.live.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设置频道观看条件
 * @author LHR
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PLChannelUpdateAuthRequest {

	/**
	 * 主要观看条件为1，次要观看条件为2
	 */
	protected int rank;
	/**
	 * 是否开启，Y为开启，N为关闭
	 */
	protected String enabled;
	/**
	 * 付费观看-pay，验证码观看-code，白名单观看-phone，登记观看-info，自定义授权观看-custom，外部授权-external
	 */
	protected String authType;
	/**
	 * SecretKey
	 */
	protected String externalKey;
	/**
	 * 自定义url
	 */
	protected String externalUri;
	/**
	 * 跳转地址
	 */
	protected String externalRedirectUri;
}
