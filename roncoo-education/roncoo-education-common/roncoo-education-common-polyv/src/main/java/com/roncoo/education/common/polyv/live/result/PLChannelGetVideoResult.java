package com.roncoo.education.common.polyv.live.result;

import lombok.Data;

/**
 * 直播频道信息获取请求响应对象类
 */
@Data
public class PLChannelGetVideoResult {

	/**
	 * <pre>
	 * 字段名：频道号
	 * 变量名：channelId
	 * 类型：int(11)
	 * </pre>
	 */
	protected int channelId;

	/**
	 * <pre>
	 * 字段名：录制文件地址
	 * 变量名：url
	 * 类型：String
	 * </pre>
	 */
	protected String url;

	/**
	 * 开始录制时间
	 */
	protected String startTime;
	
	/**
	 * 结束录制时间
	 */
	protected String endTime;
	
	/**
	 * 录制文件大小（单位：字节）
	 */
	protected String fileSize;
	
	/**
	 * 时长（单位：秒）
	 */
	protected String duration;
	
	/**
	 * 录制文件码率（单位：字节）
	 */
	protected String bitrate;
	
	/**
	 * 分辨率
	 */
	protected String resolution;
	
	/**
	 * 直播的场次ID
	 */
	protected String channelSessionId;
}
