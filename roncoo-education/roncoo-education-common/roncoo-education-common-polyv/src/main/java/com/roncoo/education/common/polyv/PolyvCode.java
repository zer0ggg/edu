/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.polyv;

import java.io.Serializable;

public class PolyvCode implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userNo;
	/**
	 * 课时id
	 */
	private Long periodId;
	/**
	 * 题目ID
	 */
	private Long problemId;

	/**
	 * 试卷ID
	 */
	private Long examId;

	public Long getUserNo() {
		return userNo;
	}

	public void setUserNo(Long userNo) {
		this.userNo = userNo;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}
}
