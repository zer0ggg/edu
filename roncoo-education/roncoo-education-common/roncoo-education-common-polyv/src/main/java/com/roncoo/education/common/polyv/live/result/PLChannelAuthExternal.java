package com.roncoo.education.common.polyv.live.result;

import lombok.Data;

/**
 * 直播频道信息获取请求响应对象类
 */
@Data
public class PLChannelAuthExternal {

	/**
	 * 频道号
	 */
	protected String channelId;

	/**
	 * secretKey
	 */
	protected String secretKey;
}
