package com.roncoo.education.common.polyv.live;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.tools.HttpUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.common.polyv.live.request.PLChannelConvertRequest;
import com.roncoo.education.common.polyv.live.request.PLChannelCreateRequest;
import com.roncoo.education.common.polyv.live.request.PLChannelUpdateAuthRequest;
import com.roncoo.education.common.polyv.live.result.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.map.HashedMap;

import java.io.File;
import java.util.*;

@Slf4j
public final class PLChannelUtil {

    private PLChannelUtil() {

    }

    public static String getSign(String secretKey, String userNo, Long ts) {
        String s = secretKey + userNo + secretKey + ts;
        return MD5Util.MD5(s);
    }

    /**
     * 获取频道号列表
     *
     * @return 频道号信息
     */
    public static List<String> listChannels(String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v1/users/{userId}/channels".replace("{userId}", "polyvLiveUserid");
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getJSONArray("result").toJavaList(String.class);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 根据频道号获取频道信息
     *
     * @param channelId
     * @return
     */
    public static PLChannelGetResult getChannel(String channelId, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/get".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getJSONObject("data").toJavaObject(PLChannelGetResult.class);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 创建频道号信息
     *
     * @param request
     * @return
     */
    public static PLChannelGetResult createChannel(PLChannelCreateRequest request, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/";
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("name", request.getName());
        map.put("channelPasswd", request.getChannelPasswd());
        map.put("courseId", request.getCourseId());
        map.put("autoPlay", request.getAutoPlay());
        map.put("playerColor", request.getPlayerColor());
        map.put("scene", request.getScene());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getJSONObject("data").toJavaObject(PLChannelGetResult.class);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 根据频道号修改频道名称
     *
     * @param channelId
     * @param newName
     * @return
     */
    public static Boolean updateChannel(String channelId, String newName, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/update".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("name", newName);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getBooleanValue("data");
        }
        log.error("获取失败，原因={}", js);
        return false;
    }

    /**
     * 修改频道详情
     *
     * @param channelId 频道号
     * @param field     要更新的字段名称：password 密码； scene 直播场景； maxViewer 最大同时在线人数
     * @param value     新的字段值，除设置无限制最大观看人数时可不提交，其他情况都为必填
     * @return
     * @author KYH
     */
    public static String channelIdUpdate(String channelId, String field, String value, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v3/channel/detail/update";
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("channelId", channelId);
        map.put("field", field);
        map.put("value", value);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 根据频道号删除频道
     *
     * @param channelId
     * @return
     */
    public static Boolean deleteChannel(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/delete".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getBooleanValue("data");
        }
        log.error("获取失败，原因={}", js);
        return false;
    }

    /**
     * 设置频道状态为直播中状态
     *
     * @param channelId 频道号
     * @return 处理结果
     */
    public static String liveChannel(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/live".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 设置频道状态为非直播状态
     *
     * @param channelId
     * @return
     */
    public static String endChannel(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/end".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 禁止频道号推流
     *
     * @param channelId
     * @return
     */
    public static String cutoffChannel(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v1/stream/{channelId}/cutoff".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("result");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 恢复直播频道推流
     *
     * @param channelId
     * @return
     */
    public static String resumeChannel(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/stream/{channelId}/resume".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 设置外部授权
     *
     * @param channelId
     * @param externalUri
     * @return
     */
    public static PLChannelAuthExternal authExternalChannel(int channelId, String externalUri, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelSetting/{userId}/auth-external".replace("{userId}", polyvLiveUserid);
        Map<String, Object> map = new HashedMap<>();
        map.put("channelId", channelId);
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("externalUri", externalUri);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return JSUtil.parseArray(js.getJSONArray("data").toString(), PLChannelAuthExternal.class).get(0);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 设置自定义授权地址
     *
     * @param channelId
     * @return
     */
    public static String authCustomChannel(String channelId, String customUri, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelSetting/{userId}/oauth-custom".replace("{userId}", "polyvLiveUserid");
        Map<String, Object> map = new HashedMap<>();
        map.put("channelId", channelId);
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("customUri", customUri);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 修改主持人姓名
     *
     * @param channelId
     * @return
     */
    public static String setPublisherChannel(String channelId, String publisher, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelSetting/{userId}/setPublisher".replace("{userId}", polyvLiveUserid);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("publisher", publisher);
        map.put("channelId", channelId);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 获取频道观看条件
     *
     * @param channelId
     * @return
     */
    public static PLChannelWatchAuthResult watchAuthChannel(String channelId, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "https://api.polyv.net/live/v2/channelSetting/{channelId}/watch-auth".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return JSUtil.parseArray(js.getJSONArray("data").toString(), PLChannelWatchAuthResult.class).get(0);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 设置播放器自定义url跑马灯
     *
     * @param channelId
     * @param marqueeRestrict
     * @return
     */
    public static String setDiyurlMarqueeChannel(String channelId, String diyurlmarqueeUri, String marqueeRestrict, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelRestrict/{channelId}/set-diyurl-marquee".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("diyurlmarqueeUri", diyurlmarqueeUri);
        map.put("marqueeRestrict", marqueeRestrict);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 设置播放器Logo
     *
     * @param channelId
     * @return
     */
    public static String setUpdateChannel(String channelId, String logoImage, String logoOpacity, String logoPosition, String logoHref, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/update".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("logoImage", logoImage);
        map.put("logoOpacity", logoOpacity);
        map.put("logoPosition", logoPosition);
        map.put("logoHref", logoHref);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 修改频道图标
     *
     * @param channelId
     * @return
     */
    public static String setCoverImgChannel(String channelId, File file, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelSetting/{channelId}/setCoverImg".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());

        // map.put("startXs", startXs);
        // map.put("startYs", startYs);
        // map.put("Widths", Widths);
        // map.put("Heights", Heights);

        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.sendHttpPost(url, map, file), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 修改频道密码
     *
     * @param channelId
     * @param passwd
     * @return
     * @author LHR
     */
    public static String passwdSetting(String channelId, String passwd, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{userId}/passwdSetting".replace("{userId}", polyvLiveUserid);
        Map<String, Object> map = new HashedMap<>();
        map.put("channelId", channelId);
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("passwd", passwd);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 直播转点存
     *
     * @param request
     * @return
     * @author LHR
     */
    public static String convert(PLChannelConvertRequest request, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channel/recordFile/{channelId}/convert".replace("{channelId}", request.getChannelId());
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("userId", polyvLiveUserid);
        map.put("fileUrl", request.getFileUrl());
        map.put("fileName", request.getFileName());
        // map.put("cataid", request.getCataid());
        map.put("cataname", request.getCataname());
        map.put("toPlayList", request.getToPlayList());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("status");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 异步批量转存录制文件到点播(没有时间限制)
     *
     * @param appId
     * @param appSecret
     * @param channelId
     * @param fileIds
     * @param fileName
     * @return
     */
    public static String listConvert(String appId, String appSecret, String channelId, String fileIds, String fileName) {
        String url = "http://api.polyv.net/live/v3/channel/record/convert";
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", appId);
        map.put("timestamp", System.currentTimeMillis());
        map.put("channelId", channelId);
        map.put("fileIds", fileIds);
        map.put("fileName", fileName);
        map.put("sign", getSign(appSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.warn("获取成功，报文={}", js);
            return js.getString("status");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 获取回放信息
     *
     * @param channelId
     * @return
     * @author LHR
     */
    public static List<PLChannelGetVideoResult> recordFiles(String channelId, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channels/{channelId}/recordFiles".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("userId", polyvLiveUserid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getJSONArray("result").toJavaList(PLChannelGetVideoResult.class);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 获取频道助教信息
     *
     * @param channelId
     * @return
     * @author liaoh
     */
    public static PLChannelGetAccounts accounts(String channelId, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "https://api.polyv.net/live/v2/channelAccount/{channelId}/accounts".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return JSUtil.parseArray(js.getJSONArray("data").toString(), PLChannelGetAccounts.class).get(0);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 设置是否使用通用设置
     *
     * @author LHR
     */
    public static String defaultSetting(int channelId, String globalEnabledType, String enabled, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v3/channel/common/update-global-enabled";
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("channelId", channelId);
        map.put("globalEnabledType", globalEnabledType);
        map.put("enabled", enabled);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 设置为当前频道的观看条件
     *
     * @return
     * @author LHR
     */
    public static String updateAuth(int channelId, PLChannelUpdateAuthRequest requst, String polyvLiveAppid, String polyvLiveAppSecret) {
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("channelId", channelId);
        map.put("sign", getSign(polyvLiveAppSecret, map));
        String url = "http://api.polyv.net/live/v3/channel/auth/update";
        Map<String, Object> map2 = new HashedMap<>();
        List<PLChannelUpdateAuthRequest> requstList = new ArrayList<>();
        requstList.add(requst);
        map2.put("authSettings", requstList);
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map, JSUtil.toJSONString(map2)), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 清除聊天室功能
     *
     * @return
     * @author LHR
     */
    public static String cleanChat(String channelId, String polyvLiveAppid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/chat/{channelId}/cleanChat".replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        JSONObject js = HttpUtil.getForObject(url, map, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 设置直播简介
     *
     * @param channelId
     * @param content
     * @return
     * @author LHR
     */
    public static String setMenu(String channelId, String content, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        String url = "http://api.polyv.net/live/v2/channelSetting/{userId}/{channelId}/set-menu".replace("{userId}", polyvLiveUserid).replace("{channelId}", channelId);
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", polyvLiveAppid);
        map.put("menuType", "desc");
        map.put("content", content);
        map.put("timestamp", System.currentTimeMillis());
        map.put("sign", getSign(polyvLiveAppSecret, map));
        String results = HttpUtil.post(url, map);
        JSONObject js = JSUtil.parseObject(results, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return "";
    }

    /**
     * 获取频道直播状态——live end
     *
     * @param channelId
     * @return
     */
    public static String getLiveStatus(String appId, String appSecret, String channelId) {
        String url = "http://api.polyv.net/live/v2/channels/live-status";
        Map<String, Object> map = new HashedMap<>();
        map.put("appId", appId);
        map.put("timestamp", System.currentTimeMillis());
        map.put("channelIds", channelId);
        map.put("sign", getSign(appSecret, map));
        JSONObject js = JSUtil.parseObject(HttpUtil.post(url, map), JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            List<PLChannelgetLiveStatus> list = js.getJSONArray("data").toJavaList(PLChannelgetLiveStatus.class);
            return list.get(0).getStatus();
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 获取登录开播地址
     *
     * @param channelId 频道ID
     * @return 登录开播地址
     */
    public static String getLoginUrl(String channelId) {
        return String.format("https://live.polyv.net/web-start/login?channelId=%s", channelId);
    }

    /**
     * 设置观看条件
     *
     * @param channelId          频道ID
     * @param externalUri        外部授权路径
     * @param polyvLiveAppid     直播APPID
     * @param polyvLiveUserid    直播用户ID
     * @param polyvLiveAppSecret 直播APP秘钥
     */
    public static void setAuth(Integer channelId, String externalUri, String polyvLiveAppid, String polyvLiveUserid, String polyvLiveAppSecret) {
        // 设置观看条件为外部授权
        PLChannelAuthExternal pLChannelAuthExternal = authExternalChannel(channelId, externalUri, polyvLiveAppid, polyvLiveUserid, polyvLiveAppSecret);
        if (ObjectUtil.isNull(pLChannelAuthExternal)) {
            throw new BaseException("设置观看条件失败");
        }

        // 设置外部授权观看条件
        PLChannelUpdateAuthRequest request = new PLChannelUpdateAuthRequest();
        request.setRank(1);
        request.setEnabled("Y");
        request.setAuthType("external");
        request.setExternalKey(pLChannelAuthExternal.getSecretKey());
        request.setExternalUri(externalUri);
        request.setExternalRedirectUri("");
        PLChannelUtil.updateAuth(channelId, request, polyvLiveAppid, polyvLiveAppSecret);

        // 关闭应用默认功能开关设置
        PLChannelUtil.defaultSetting(channelId, "switch", "N", polyvLiveAppid, polyvLiveAppSecret);
    }

    /**
     * 获取sign
     *
     * @param paramMap
     * @return
     */
    private static String getSign(String polyvLiveAppSecret, Map<String, Object> paramMap) {
        if (paramMap.isEmpty()) {
            return "";
        }
        SortedMap<String, Object> smap = new TreeMap<>(paramMap);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(polyvLiveAppSecret);
        for (Map.Entry<String, Object> m : smap.entrySet()) {
            if (m.getValue() != null) {
                stringBuilder.append(m.getKey()).append(m.getValue());
            }
        }
        stringBuilder.append(polyvLiveAppSecret);
        return MD5Util.MD5(stringBuilder.toString()).toUpperCase();
    }

}
