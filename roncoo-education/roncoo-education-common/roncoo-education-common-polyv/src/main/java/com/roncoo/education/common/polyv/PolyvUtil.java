/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.common.polyv;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.common.core.tools.SHA1Util;
import lombok.extern.slf4j.Slf4j;
import net.polyv.bean.vo.VideoInfo;
import net.polyv.callback.UploadCallBack;
import net.polyv.entry.PolyvUploadClient;
import net.polyv.enumeration.UploadErrorMsg;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 保利威视工具类
 *
 * @author wujing
 */
@Slf4j
public final class PolyvUtil {

    /**
     * 长度为12
     */
    private static final String KEY = "rc-education";
    private static final String CHARSET_UTF_8 = "UTF-8";

    private PolyvUtil() {
    }

    /**
     * 获取code
     *
     * @param polyvCode
     * @return
     */
    public static String getPolyvCode(PolyvCode polyvCode) {
        log.warn("获取codePolyvCode={}", JSONObject.toJSON(polyvCode));
        try {
            return URLUtil.encodeQuery(Base64.encode(SecureUtil.des(Base64.decode(KEY)).encrypt(JSUtil.toJSONString(polyvCode))), Charset.forName(CHARSET_UTF_8));
        } catch (Exception e) {
            log.error("保利威视，加密出错", e);
            return "";
        }
    }

    /**
     * code解密
     *
     * @param code
     * @return
     */
    public static PolyvCode decode(String code) {
        System.out.println(code);
        try {
            return JSUtil.parseObject(new String(SecureUtil.des(Base64.decode(KEY)).decrypt(Base64.decode(URLDecoder.decode(code, CHARSET_UTF_8)))), PolyvCode.class);
        } catch (Exception e) {
            log.error("保利威视，解密出错", e);
            return null;
        }
    }

    /**
     * h5获取播放sign
     *
     * @param bo
     * @param useId
     * @param secretKey
     * @return
     */
    @SuppressWarnings("unchecked")
    public static PolyvSignResult getSignForH5(PolyvSign bo, String useId, String secretKey) {
        // 根据时间戳、vid、密钥生成sign值
        String ts = String.valueOf(System.currentTimeMillis());

        // 获取播放token
        Map<String, Object> map = new HashedMap<>();
        map.put("userId", useId);
        map.put("videoId", bo.getVid());
        map.put("ts", ts);
        map.put("viewerIp", bo.getIp());
        map.put("viewerName", bo.getUserNo());
        map.put("extraParams", "HTML5");
        map.put("viewerId", bo.getUserNo());
        String concated = "extraParams" + map.get("extraParams") + "ts" + map.get("ts") + "userId" + map.get("userId") + "videoId" + map.get("videoId") + "viewerId" + map.get("viewerId") + "viewerIp" + map.get("viewerIp") + "viewerName" + map.get("viewerName");
        map.put("sign", MD5Util.MD5(secretKey + concated + secretKey).toUpperCase());
        String result = HttpUtil.post(SystemUtil.POLYV_GETTOKEN, map);
        Map<String, Object> resultMap = JSUtil.parseObject(result, HashMap.class);
        int code = Integer.parseInt(resultMap.get("code").toString());
        if (code != 200) {
            log.error("获取保利威播放sign值失败，返回参数={}", result);
            return null;
        }
        Map<String, Object> data = (Map<String, Object>) resultMap.get("data");
        if (data == null || data.isEmpty()) {
            return null;
        }
        PolyvSignResult dto = new PolyvSignResult();
        dto.setSign(MD5Util.MD5(secretKey + bo.getVid() + ts));
        dto.setTs(ts);
        dto.setToken(data.get("token").toString());
        return dto;
    }

    /**
     * 获取视频信息
     *
     * @param vid       视频ID
     * @param secretKey 秘钥
     * @param useId     用户ID
     * @return 上传文件返回
     */
    public static UploadFileResult getVideo(String vid, String secretKey, String useId) {
        Map<String, Object> param = new TreeMap<>();
        param.put("vid", vid);
        param.put("format", "json");
        // 当前13位毫秒级时间戳
        param.put("ptime", String.valueOf(System.currentTimeMillis()));
        StringBuilder signStr = new StringBuilder();
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            signStr.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        signStr.deleteCharAt(0);
        signStr.append(secretKey);
        param.put("sign", SHA1Util.getSign(signStr.toString()));
        String s = HttpUtil.post("http://api.polyv.net/v2/video/{userid}/get-video-msg".replace("{userid}", useId), param);
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> m = JSUtil.parseObject(s, HashMap.class);
            if ("200".equals(m.get("code").toString())) {
                // 成功
                return JSUtil.parseArray(JSUtil.toJSONString(m.get("data")), UploadFileResult.class).get(0);
            }
            log.error("获取视频失败，原因为：{}", s);
        } catch (Exception e) {
            log.error("获取视频失败异常，结果={}，参数={}", s, param);
        }
        return null;
    }

    /**
     * 上传视频 http://dev.polyv.net/2014/videoproduct/v-api/v-api-upload/uploadfile/
     *
     * @param file
     * @return
     */
    public static UploadFileResult uploadFile(File file, UploadFile uploadFile, String writeToken) {
        Map<String, Object> param = new HashMap<>();
        param.put("writetoken", writeToken);
        param.put("JSONRPC", "{\"title\": \"" + uploadFile.getTitle() + "\", \"tag\": \"" + uploadFile.getTag() + "\", \"desc\": \"" + uploadFile.getDesc() + "\"}");
        param.put("cataid", uploadFile.getCataid());
        if (StringUtils.hasText(uploadFile.getWatermark())) {
            param.put("watermark", uploadFile.getWatermark());
        }
        String result = postFile(SystemUtil.POLYV_UPLOADVIDEO, param, file);
        log.warn("保利威上传视频返回信息={}", result);
        try {
            JSONObject json = JSONObject.parseObject(result);
            if ("0".equals(json.getString("error"))) {
                return JSUtil.parseArray(json.getString("data"), UploadFileResult.class).get(0);
            }
        } catch (Exception e) {
            log.error(e.toString(), e);
        }
        log.error("保利威视，上传视频失败，原因={}", result);
        return null;
    }

    /**
     * 删除视频
     */
    public static String deleteFile(String vid, String useId, String secretKey) {
        SortedMap<String, String> paramMap = new TreeMap<>();
        // 用户ID
        paramMap.put("userid", useId);
        // 视频ID
        paramMap.put("vid", vid);
        // 当前13位毫秒级时间戳
        paramMap.put("ptime", String.valueOf(System.currentTimeMillis()));
        StringBuilder signStr = new StringBuilder();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            signStr.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        signStr.deleteCharAt(0);
        signStr.append(secretKey);
        String sign = SHA1Util.getSign(signStr.toString());
        paramMap.put("sign", sign);
        String url = SystemUtil.POLYV_DELETEVIDEO.replace("{userid}", useId);
        HttpPost httpPost = new HttpPost(url.trim());
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(10000).setConnectionRequestTimeout(3600000).setSocketTimeout(3600000).build();
        httpPost.setConfig(requestConfig);
        List<BasicNameValuePair> nvps = new ArrayList<>();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        StringEntity se;
        try {
            se = new UrlEncodedFormEntity(nvps, CHARSET_UTF_8);
            httpPost.setEntity(se);
            HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpPost);
            return EntityUtils.toString(httpResponse.getEntity(), CHARSET_UTF_8);
        } catch (Exception e) {
            log.error("保利威视，删除视频失败");
        }
        return null;
    }

    /**
     * 上传问题接口
     */
    public static QuestionResult uploadQuestion(Question question, String writeToken) {
        try {
            String url = SystemUtil.POLYV_QUESTION;
            HttpPost httpPost = new HttpPost(url.trim());
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(10000).setConnectionRequestTimeout(3600000).setSocketTimeout(3600000).build();
            httpPost.setConfig(requestConfig);
            JSONArray choices = new JSONArray();
            for (String value : question.getAnswerList()) {
                JSONObject answer = new JSONObject();
                answer.put("answer", value);
                choices.add(answer);
            }
            JSONObject rightAnswer = new JSONObject();
            rightAnswer.put("answer", question.getRightAnswer());
            rightAnswer.put("right_answer", question.getRight());
            choices.add(rightAnswer);
            List<BasicNameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("method", "saveExam"));
            nvps.add(new BasicNameValuePair("writetoken", writeToken));
            nvps.add(new BasicNameValuePair("vid", question.getVid()));
            nvps.add(new BasicNameValuePair("examId", question.getExamId()));
            nvps.add(new BasicNameValuePair("seconds", String.valueOf(question.getSeconds())));
            nvps.add(new BasicNameValuePair("question", question.getQuestion()));
            nvps.add(new BasicNameValuePair("choices", choices.toString()));
            nvps.add(new BasicNameValuePair("skip", String.valueOf(question.isSkip())));
            nvps.add(new BasicNameValuePair("answer", question.getAnswer()));
            nvps.add(new BasicNameValuePair("wrongShow", String.valueOf(question.getWrongShow())));
            nvps.add(new BasicNameValuePair("wrongTime", String.valueOf(question.getWrongTime())));
            StringEntity se = new UrlEncodedFormEntity(nvps, CHARSET_UTF_8);
            httpPost.setEntity(se);
            HttpResponse httpResponse = HttpClientBuilder.create().build().execute(httpPost);
            String resultStr = EntityUtils.toString(httpResponse.getEntity(), CHARSET_UTF_8);
            return JSONObject.parseObject(resultStr, QuestionResult.class);
        } catch (Exception e) {
            log.error("添加问题失败！");
        }
        return null;
    }

    /**
     * 获取视频观看完成度
     *
     * @param vId
     * @param viewerId
     * @param userid
     * @param secretKey
     * @return
     */
    public static String getEngagement(String vId, String viewerId, String userid, String secretKey) {
        String url = "https://api.polyv.net/v2/video/engagement/{userid}/get".replace("{userid}", userid);
        Map<String, Object> param = new TreeMap<>();
        param.put("userid", userid);
        // 当前13位毫秒级时间戳
        param.put("ptime", String.valueOf(System.currentTimeMillis()));
        param.put("vid", vId);
        param.put("viewerId", viewerId);
        StringBuilder signStr = new StringBuilder();
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            signStr.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        signStr = signStr.deleteCharAt(0);
        signStr.append(secretKey);
        param.put("sign", SHA1Util.getSign(signStr.toString()));
        JSONObject js = com.roncoo.education.common.core.tools.HttpUtil.getForObject(url, param, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getString("data");
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 获取视频打点信息
     *
     * @param vid
     * @param userid
     * @param secretKey
     * @return
     */
    public static PolyvGetKeyframeResult getKeyframe(String vid, String userid, String secretKey) {
        String url = "http://api.polyv.net/v2/video/{userid}/keyframe/{vid}".replace("{userid}", userid).replace("{vid}", vid);
        Map<String, Object> param = new TreeMap<>();
        param.put("userid", userid);
        // 当前13位毫秒级时间戳
        param.put("ptime", String.valueOf(System.currentTimeMillis()));
        param.put("vid", vid);
        StringBuilder signStr = new StringBuilder();
        for (Map.Entry<String, Object> entry : param.entrySet()) {
            signStr.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        signStr.deleteCharAt(0);
        signStr.append(secretKey);
        param.put("sign", SHA1Util.getSign(signStr.toString()));
        JSONObject js = com.roncoo.education.common.core.tools.HttpUtil.getForObject(url, param, JSONObject.class);
        if ("success".equalsIgnoreCase(js.getString("status"))) {
            log.info("获取成功，报文={}", js);
            return js.getJSONObject("data").toJavaObject(PolyvGetKeyframeResult.class);
        }
        log.error("获取失败，原因={}", js);
        return null;
    }

    /**
     * 获取sign
     *
     * @param paramMap
     * @return
     */
    public static String getSign(Map<String, Object> paramMap, String secretKey) {
        if (paramMap.isEmpty()) {
            return "";
        }
        SortedMap<String, Object> smap = new TreeMap<>(paramMap);
        StringBuilder stringBuffer = new StringBuilder();
        for (Map.Entry<String, Object> m : smap.entrySet()) {
            String value = String.valueOf(m.getValue());
            if (StringUtils.hasText(value)) {
                stringBuffer.append(m.getKey()).append("=").append(value).append("&");
            }
        }
        stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
        String argPreSign = stringBuffer.append("&paySecret=").append(secretKey).toString();
        return MD5Util.MD5(argPreSign).toUpperCase();
    }

    private static String postFile(String url, Map<String, Object> param, File file) {
        CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build();
        try {
            HttpPost httpPost = new HttpPost(url.trim());
            httpPost.setConfig(RequestConfig.custom().setConnectTimeout(10000).setConnectionRequestTimeout(3600000).setSocketTimeout(3600000).build());
            MultipartEntityBuilder entity = MultipartEntityBuilder.create();
            entity.addPart("Filedata", new FileBody(file));
            ContentType contentType = ContentType.create("text/plain", StandardCharsets.UTF_8);
            for (Map.Entry<String, Object> m : param.entrySet()) {
                entity.addPart(m.getKey(), new StringBody(m.getValue().toString(), contentType));
            }
            httpPost.setEntity(entity.build());
            return EntityUtils.toString(closeableHttpClient.execute(httpPost).getEntity(), CHARSET_UTF_8);
        } catch (Exception e) {
            log.error("保利威视，上传视频失败，原因", e);
        } finally {
            try {
                closeableHttpClient.close();
            } catch (IOException e) {
            }
        }
        return "";
    }

    /**
     * 使用sdk上传视频
     *
     * @param fileName
     * @param videoName
     * @param videoPath
     * @param videoTag
     * @param desc
     * @param userid
     * @param secretKey
     * @return
     */
    public static String sdkUploadFile(String fileName, String videoName, String videoPath, String videoTag, String desc, String userid, String secretKey) {
        PolyvUploadClient client = new PolyvUploadClient(userid, secretKey, Integer.parseInt(SystemUtil.POLYV_UPLOAD_PARTITIONSIZE), SystemUtil.VIDEO_PATH, Integer.parseInt(SystemUtil.POLYV_UPLOAD_THREADNUM));
        VideoInfo videoInfo = new VideoInfo();
        // 视频文件在服务器上的绝对路径，必须包含拓展名（必填）
        videoInfo.setFileLocation(videoPath.concat(fileName));
        // 视频标题（必填）
        videoInfo.setTitle(videoName);
        // 上传的分类目录，默认值：1，表示上传到默认分类（可选）
        videoInfo.setCataId(1L);
        //视频分类标签，用于区分视频类型，参考VideoTagEnum
        videoInfo.setTag(videoTag);
        // 视频描述（可选）
        videoInfo.setDescrib(desc);

        String videoVid;
        videoVid = client.uploadVideoParts(videoInfo, new UploadCallBack() {
            @Override
            public void start(String s) {
                log.warn("start=" + s);
            }

            @Override
            public void process(String s, long l, long l1) {
                log.warn("process=" + s + ",uploaded=" + l + ", total=" + l1);
            }

            @Override
            public void complete(String s) {
                log.warn("complete=" + s);
            }

            @Override
            public void success(String s) {
                log.warn("success=" + s);
            }

            @Override
            public void error(String s, UploadErrorMsg uploadErrorMsg) {
                log.warn("error=" + s + ", message=" + uploadErrorMsg.getMessage());
            }
        }, true);

        UploadFileResult uploadFileResult = getVideo(videoVid, secretKey, userid);
        if (ObjectUtils.isEmpty(uploadFileResult)) {
            log.warn("获取不到该视频上传信息,vid=" + videoVid);
            return null;
        }
        return videoVid;
    }

    /**
     * url上传视频
     *
     * @param uploadUrlFile
     * @return
     */
    public static Boolean uploadUrlFile(UploadUrlFile uploadUrlFile, String writeToken, String secretKey) {
        String url = "http://v.polyv.net/uc/services/rest?method=uploadUrlFile";
        Map<String, Object> map = new HashedMap<>();
        map.put("writetoken", writeToken);
        map.put("fileUrl", uploadUrlFile.getUrl());
        map.put("async", uploadUrlFile.getAsync());
        map.put("title", uploadUrlFile.getTitle());
        map.put("tag", uploadUrlFile.getTag());
        map.put("desc", uploadUrlFile.getDesc());
        map.put("cataid", uploadUrlFile.getCataid());
        map.put("watermark", uploadUrlFile.getWatermark());
        map.put("luping", 1);
        map.put("state", uploadUrlFile.getState());
        map.put("sign", getSign(map, secretKey));
        log.warn("url上传视频请求参数==={}", map);
        JSONObject json = JSONObject.parseObject(HttpUtil.post(url, map), JSONObject.class);
        log.warn("url上传视频返回参数==={}", json);
        if ("0".equals(json.getString("error"))) {
            return true;
        }
        log.error("获取失败，原因={}", json);
        return false;
    }

}
