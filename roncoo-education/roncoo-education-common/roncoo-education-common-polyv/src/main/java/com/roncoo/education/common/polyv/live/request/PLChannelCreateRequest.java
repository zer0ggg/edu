package com.roncoo.education.common.polyv.live.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 * POLYV创建频道对象类
 * </pre>
 *
 * @author HuangYF
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PLChannelCreateRequest {

	/**
	 * <pre>
	 * 字段名：频道名称
	 * 变量名：name
	 * 是否必填：是
	 * 类型：String(64)
	 * 示例值：POLYV
	 * 描述：创建频道时传入的频道名称
	 * </pre>
	 */
	protected String name;

	/**
	 * <pre>
	 * 字段名：频道密码
	 * 变量名：channelPasswd
	 * 是否必填：是
	 * 类型：String(64)
	 * 示例值：123456
	 * 描述：创建频道传入的频道密码
	 * </pre>
	 */
	protected String channelPasswd;

	/**
	 * <pre>
	 * 字段名：课程号
	 * 变量名：courseId
	 * 是否必填：否
	 * 类型：String(64)
	 * 示例值：course1
	 * 描述：创建频道时传入的课程Id标识
	 * </pre>
	 */
	protected String courseId;

	/**
	 * <pre>
	 * 字段名：自动播放标识
	 * 变量名：autoPlay
	 * 是否必填：否
	 * 类型：int(11)
	 * 示例值：0/1，默认1
	 * 描述：创建的频道是否自动播放
	 * </pre>
	 */
	protected Integer autoPlay;

	/**
	 * <pre>
	 * 字段名：播放器控制栏颜色，
	 * 变量名：playerColor
	 * 是否必填：否
	 * 类型：String(64)
	 * 示例值：默认：#666666
	 * 描述：创建频道的播放器控制栏颜色
	 * </pre>
	 */
	protected String playerColor;
	/**
	 * <pre>
	 * 字段名：直播场景 
	 * 变量名：scene 是否必填：否 
	 * 示例值：默认：alone活动拍摄 
	 * alone 活动拍摄, ppt 三分屏
	 * 描述：创建频道时直播场景默认活动拍摄
	 * </pre>
	 */
	protected String scene;

}
