package com.roncoo.education.common.polyv.live.request;

import lombok.Data;

/**
 * <pre>
 * POLYV直播API请求对象共用的参数存放类
 * </pre>
 *
 * @author HuangYF
 */
@Data
public abstract class PLBaseRequest {

	/**
	 * <pre>
	 * 字段名：账号应用ID
	 * 变量名：appId
	 * 是否必填：是
	 * 类型：String(32)
	 * 示例值：egzkiq28qv
	 * 描述：POLYV账号的APPID
	 * </pre>
	 */
	protected String appId;

	/**
	 * <pre>
	 * 字段名：加密串
	 * 变量名：appSecret
	 * 是否必填：是
	 * 类型：String(64)
	 * 示例值：4a5e6b18237d4766a1f61ad771ad5734
	 * 描述：接口加密辨识串
	 * </pre>
	 */
	protected String appSecret;

	/**
	 * <pre>
	 * 字段名：时间戳
	 * 变量名：timestamp
	 * 是否必填：是
	 * 类型：Long
	 * 示例值：1469777806
	 * 描述：当前系统时间戳
	 * </pre>
	 */
	protected Long timestamp;

	/**
	 * <pre>
	 * 字段名：签名
	 * 变量名：sign
	 * 是否必填：是
	 * 类型：String(32)
	 * 示例值：3C9F8A701D475DE50D501EDE8DFBE0F9
	 * 描述：签名，详见签名生成算法
	 * </pre>
	 */
	protected String sign;

}
