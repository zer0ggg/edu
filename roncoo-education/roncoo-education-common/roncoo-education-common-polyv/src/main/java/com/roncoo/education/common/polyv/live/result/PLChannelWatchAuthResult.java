package com.roncoo.education.common.polyv.live.result;

import lombok.Data;

/**
 * 直播频道信息获取请求响应对象类
 */
@Data
public class PLChannelWatchAuthResult {

	/**
	 * 	频道ID
	 */
	protected int channelId;

	/**
	 * 	用户ID
	 */
	protected int rank;
	
	/**
	 * （1为主要条件，2为次要条件）
	 */
	protected String userId;
	
	/**
	 * 是否开启全局设置（Y/N）
	 */
	protected String globalSettingEnabled;
	
	/**
	 * 是否开启观看条件(Y/N)
	 */
	protected String enabled;
	
	/**
	 * 观看条件类型(1. 无限制 none 2. 验证码观看 code 3. 付费观看 pay 4. 白名单观看 phone 5. 登记观看 info 6. 分享观看 wxshare 7. 自定义授权观看 custom 8. 外部授权观看 external)
	 */
	protected String authType;
	
	/**
	 * 外部授权观看的key
	 */
	protected String externalKey;
	
	/**
	 * 外部授权观看的接口地址
	 */
	protected String externalUri;
	
	/**
	 * 	外部授权观看，用户直接访问观看页时的跳转地址
	 */
	protected String externalRedirectUri;
}
