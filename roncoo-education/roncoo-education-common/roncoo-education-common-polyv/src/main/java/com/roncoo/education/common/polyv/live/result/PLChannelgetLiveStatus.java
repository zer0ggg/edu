package com.roncoo.education.common.polyv.live.result;

import lombok.Data;

@Data
public class PLChannelgetLiveStatus {

    /**
     * 频道号
     */
    protected String channelId;

    /**
     * 频道直播状态(live,end)
     */
    protected String status;
    /**
     * 场次id
     */
    protected String sessionId;
}
