package com.roncoo.education.common.polyv.live.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <pre>
 * POLYV创建频道对象类
 * </pre>
 *
 * @author HuangYF
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PLChannelConvertRequest {

	/**
	 * 频道号
	 */
	private String channelId;
	/**
	 * 直播系统登记的appId
	 */
	protected String appId;
	/**
	 * 当前13位毫秒级时间戳，3分钟内有效
	 */
	protected String timestamp;
	/**
	 * 直播账号ID
	 */
	protected String userId;
	/**
	 * 转存到录制文件地址（flv地址）
	 */
	protected String fileUrl;
	/**
	 * 转存后的点播视频名称
	 */
	protected String fileName;
	/**
	 * 目录id，不填默认或者填写错误即为默认分类
	 */
	protected String cataid;
	/**
	 * 目录名称，默认值为默认分类
	 */
	protected String cataname;
	/**
	 * 签名，32位大写MD5值
	 */
	protected String sign;
	/**
	 * 是否存放到回放列表默认为N ：不存放 Y：存放到回放列表
	 */
	protected String toPlayList;
}
