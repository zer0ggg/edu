package com.roncoo.education.common.polyv.live.result;

import lombok.Data;

/**
 * 获取频道下子频道信息
 * 
 * @author liaoh
 */
@Data
public class PLChannelGetAccounts {

	/**
	 * 助教ID
	 */
	protected String account;
	/**
	 * 助教密码
	 */
	protected String passwd;
}
