package net.polyv.enumeration;

/**
 * 视频上传状态
 */
public enum VideoUploadStatus {
    
    UPLOADING,SUCCESS,FAILED
}
