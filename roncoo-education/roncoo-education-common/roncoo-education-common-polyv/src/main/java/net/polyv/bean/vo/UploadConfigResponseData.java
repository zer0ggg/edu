package net.polyv.bean.vo;

import java.io.Serializable;

/**
 * 上传token接口响应体vo
 */
public class UploadConfigResponseData implements Serializable {
    
    private static final long serialVersionUID = 5981397967769448910L;
    private String dir;
    private String host;
    private String domain;
    private String encodedCallback;
    private String callback;
    private long remainSpace;
    private String vid;
    private String accessId;
    private String accessKey;
    private String token;
    private String Expiration;
    private String endpoint;
    private String bucketName;
    private long validityTime;
    
    public long getValidityTime() {
        return validityTime;
    }
    
    public void setValidityTime(long validityTime) {
        this.validityTime = validityTime;
    }
    
    public String getCallback() {
        return callback;
    }
    
    public void setCallback(String callback) {
        this.callback = callback;
    }
    
    public String getDir() {
        return dir;
    }
    
    public void setDir(String dir) {
        this.dir = dir;
    }
    
    public String getHost() {
        return host;
    }
    
    public void setHost(String host) {
        this.host = host;
    }
    
    public String getDomain() {
        return domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }
    
    public String getEncodedCallback() {
        return encodedCallback;
    }
    
    public void setEncodedCallback(String encodedCallback) {
        this.encodedCallback = encodedCallback;
    }
    
    public long getRemainSpace() {
        return remainSpace;
    }
    
    public void setRemainSpace(long remainSpace) {
        this.remainSpace = remainSpace;
    }
    
    public String getVid() {
        return vid;
    }
    
    public void setVid(String vid) {
        this.vid = vid;
    }
    
    public String getAccessId() {
        return accessId;
    }
    
    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }
    
    public String getAccessKey() {
        return accessKey;
    }
    
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }
    
    public String getToken() {
        return token;
    }
    
    public void setToken(String token) {
        this.token = token;
    }
    
    public String getExpiration() {
        return Expiration;
    }
    
    public void setExpiration(String expiration) {
        Expiration = expiration;
    }
    
    public String getEndpoint() {
        return endpoint;
    }
    
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
    
    public String getBucketName() {
        return bucketName;
    }
    
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
