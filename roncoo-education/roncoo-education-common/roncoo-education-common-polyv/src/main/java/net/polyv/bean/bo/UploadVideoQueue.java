package net.polyv.bean.bo;

import net.polyv.bean.vo.VideoInfo;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 上传视频队列
 */
public class UploadVideoQueue {

    List<VideoInfo> videoList;

    public UploadVideoQueue(){
        this.videoList = new ArrayList<>();
    }

    public List<VideoInfo> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<VideoInfo> videoList) {
        this.videoList = videoList;
    }

    /**
     * 添加视频任务
     * @param videoInfo
     */
    public void addVideo(VideoInfo videoInfo){
        this.videoList.add(videoInfo);
    }

    /**
     * 获取下一个任务
     * @return
     */
    public VideoInfo getNextVideo(){
        return CollectionUtils.isEmpty(videoList) ? null : videoList.get(0);
    }

    /**
     * 完成上传任务
     */
    public void finshVideoUpload(){
        if(CollectionUtils.isNotEmpty(videoList)){
            videoList.remove(videoList.get(0));
        }
    }
}
