package net.polyv.sample;

import net.polyv.bean.vo.VideoInfo;
import net.polyv.callback.UploadCallBack;
import net.polyv.entry.PolyvUploadClient;
import net.polyv.enumeration.UploadErrorMsg;

/**
 * 调用polyv 上传sdk的例子
 * 续传不能修改视频的标题，描述，分类，标签，分片大小
 */
public class UploadVideoSample {
    
    public static void main(String[] args) {
        /**
         * 传入polyv账号id，secretKey，分片大小（默认为1MB,大小限定为100KB~5GB），checkpoint文件夹路径，上传线程数（默认为5个）
         */
        PolyvUploadClient client = new PolyvUploadClient("<userid>", "<secretKey>", 100 * 1024, "checkpoint_location"
                , 5);
        
        VideoInfo videoInfo = new VideoInfo();
        videoInfo.setFileLocation("C:\\Users\\Lenovo\\Desktop\\video\\test.mp4");
        videoInfo.setCataId(1L);
        videoInfo.setDescrib("描述");
        System.out.println(client.uploadVideoParts(videoInfo, new UploadCallBack() {
            @Override
            public void start(String videoPoolId) {
                System.out.println("==================start=" + videoPoolId);
            }
            
            @Override
            public void process(String videoPoolId, long hasUploadBytes, long totalFileBytes) {
                System.out.println("==================process=" + videoPoolId + "---" + hasUploadBytes + "---" + totalFileBytes);
            }
            
            @Override
            public void complete(String videoPoolId) {
                System.out.println("==================complete=" + videoPoolId);
            }
            
            @Override
            public void success(String videoPoolId) {
                System.out.println("==================success=" + videoPoolId);
            }
            
            @Override
            public void error(String videoPoolId, UploadErrorMsg errorMsg) {
                System.out.println("==================error=" + videoPoolId + "--" + errorMsg.getMessage());
            }
        }, false));


//        // 视频2
            videoInfo = new VideoInfo();
            videoInfo.setFileLocation("C:\\Users\\Lenovo\\Desktop\\video\\3519015B46993F79688CAD07DFEE8FFE.mp4");
            videoInfo.setFileSize(873592915L);
            videoInfo.setTitle("video_title222");
            videoInfo.setTag("video_tag1");
            videoInfo.setCataId(1L);
            videoInfo.setDescrib("video_describ1");
            System.out.println(client.uploadVideoParts(videoInfo, new UploadCallBack() {
                @Override
                public void start(String videoPoolId) {
                    System.out.println("==================start=" + videoPoolId);
                }
                
                @Override
                public void process(String videoPoolId, long hasUploadBytes, long totalFileBytes) {
                    System.out.println("==================process=" + videoPoolId + "---" + hasUploadBytes + "---" + totalFileBytes);
                }
                
                @Override
                public void complete(String videoPoolId) {
                    System.out.println("==================complete=" + videoPoolId);
                }
                
                @Override
                public void success(String videoPoolId) {
                    System.out.println("==================success=" + videoPoolId);
                }
                
                @Override
                public void error(String videoPoolId, UploadErrorMsg errorMsg) {
                    System.out.println("==================error=" + videoPoolId + "--" + errorMsg.getMessage());
                }
            }, false));
    }
}
