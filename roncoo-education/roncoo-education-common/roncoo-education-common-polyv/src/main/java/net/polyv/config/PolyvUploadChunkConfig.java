package net.polyv.config;

import java.io.Serializable;

/**
 * polyv视频断点上传的配置，分片大小，checkpoint地址，回调函数等
 */
public class PolyvUploadChunkConfig implements Serializable {

    private static final long serialVersionUID = -7680111709310767459L;
    private static final String UPLOAD_TYPE = "java_sdk_chunk_v1";
    private String checkPointDir;
    private Long partitionSize;
    private String accessId;
    private String accessKey;
    private String token;
    private String expiration;
    private String endpoint;
    private String bucket;
    private String dir;
    private String domain;
    private int threadNum;
    private Long validityTime;

    public PolyvUploadChunkConfig(long partitionSize, String checkPointDir, int threadNum) {
        this.partitionSize = partitionSize;
        this.checkPointDir = checkPointDir;
        this.threadNum = threadNum;
    }

    public int getThreadNum() {
        return threadNum;
    }

    public void setThreadNum(int threadNum) {
        this.threadNum = threadNum;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public static String getUploadType() {
        return UPLOAD_TYPE;
    }

    public String getCheckPointDir() {
        return checkPointDir;
    }

    public void setCheckPointDir(String checkPointDir) {
        this.checkPointDir = checkPointDir;
    }

    public Long getPartitionSize() {
        return partitionSize;
    }

    public void setPartitionSize(Long partitionSize) {
        this.partitionSize = partitionSize;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Long getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(Long validityTime) {
        this.validityTime = validityTime;
    }
}
