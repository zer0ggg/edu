package net.polyv.bean.vo;

import net.polyv.config.PolyvUploadChunkConfig;

import java.io.Serializable;

/**
 * 视频信息
 */
public class VideoInfo implements Serializable {

    private static final long serialVersionUID = -3132455282961724783L;
    private String title;
    private Long fileSize;
    private String describ;
    private String tag;
    private Long cataId;
    private String fileName;
    private int luping;
    private int keepSource;
    private String videoPoolId;
    private String status;
    private String uploadType;
    private String fileLocation;
    private String callBack;
    private String checkpoint;
    private Long startTime;
    private String state;

    private PolyvUploadChunkConfig polyvUploadChunkConfig;

    public PolyvUploadChunkConfig getPolyvUploadChunkConfig() {
        return polyvUploadChunkConfig;
    }

    public void setPolyvUploadChunkConfig(PolyvUploadChunkConfig polyvUploadChunkConfig) {
        this.polyvUploadChunkConfig = polyvUploadChunkConfig;
    }

    public String getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(String checkpoint) {
        this.checkpoint = checkpoint;
    }

    public String getCallBack() {
        return callBack;
    }

    public void setCallBack(String callBack) {
        this.callBack = callBack;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getDescrib() {
        return describ;
    }

    public void setDescrib(String describ) {
        this.describ = describ;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getCataId() {
        return cataId;
    }

    public void setCataId(Long cataId) {
        this.cataId = cataId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getLuping() {
        return luping;
    }

    public void setLuping(int luping) {
        this.luping = luping;
    }

    public int getKeepSource() {
        return keepSource;
    }

    public void setKeepSource(int keepSource) {
        this.keepSource = keepSource;
    }

    public String getVideoPoolId() {
        return videoPoolId;
    }

    public void setVideoPoolId(String videoPoolId) {
        this.videoPoolId = videoPoolId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
