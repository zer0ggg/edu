package net.polyv.config;

import java.io.Serializable;

/**
 * polyv账号配置信息
 */
public class PolyvUserConfig implements Serializable {
    
    private static final long serialVersionUID = -2680272740563432489L;
    
    private String userId;
    private String secretKey;
    private String writeToken;
    private String readToken;
    
    public PolyvUserConfig(String userId, String secretKey, String writeToken, String readToken) {
        this.userId = userId;
        this.secretKey = secretKey;
        this.writeToken = writeToken;
        this.readToken = readToken;
    }
    
    public PolyvUserConfig(String userId, String secretKey) {
        this.userId = userId;
        this.secretKey = secretKey;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getSecretKey() {
        return secretKey;
    }
    
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    
    public String getWriteToken() {
        return writeToken;
    }
    
    public void setWriteToken(String writeToken) {
        this.writeToken = writeToken;
    }
    
    public String getReadToken() {
        return readToken;
    }
    
    public void setReadToken(String readToken) {
        this.readToken = readToken;
    }
}
