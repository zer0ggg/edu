package net.polyv.config;

import java.io.Serializable;

/**
 * TODO polyv视频直传的配置
 */
public class PolyvUploadDirectConfig implements Serializable {

    private static final long serialVersionUID = -3423126033424648504L;
    private static final String UPLOAD_TYPE = "java_sdk_v1";

    private String accessId;
    private String policy;
    private String signature;
    private String dir;
    private String host;
    private String bucket;
    private String expire;
    private String callback;

    public static String getUploadType() {
        return UPLOAD_TYPE;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }
}
