package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderLog implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long lecturerUserNo;

    private String lecturerName;

    private Long userNo;

    private String mobile;

    private Long orderNo;

    private Long productId;

    private String productName;

    private Integer productType;

    private String attach;

    private BigDecimal pricePayable;

    private BigDecimal priceDiscount;

    private BigDecimal pricePaid;

    private BigDecimal platformIncome;

    private BigDecimal lecturerIncome;

    private BigDecimal agentIncome;

    private Integer tradeType;

    private Integer payType;

    private Integer channelType;

    private Integer orderStatus;

    private Date payTime;

    private Integer isShowLecturer;

    private Integer isShowUser;

    private Long actTypeId;

    private Integer actType;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getLecturerUserNo() {
        return lecturerUserNo;
    }

    public void setLecturerUserNo(Long lecturerUserNo) {
        this.lecturerUserNo = lecturerUserNo;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName == null ? null : lecturerName.trim();
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach == null ? null : attach.trim();
    }

    public BigDecimal getPricePayable() {
        return pricePayable;
    }

    public void setPricePayable(BigDecimal pricePayable) {
        this.pricePayable = pricePayable;
    }

    public BigDecimal getPriceDiscount() {
        return priceDiscount;
    }

    public void setPriceDiscount(BigDecimal priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public BigDecimal getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(BigDecimal pricePaid) {
        this.pricePaid = pricePaid;
    }

    public BigDecimal getPlatformIncome() {
        return platformIncome;
    }

    public void setPlatformIncome(BigDecimal platformIncome) {
        this.platformIncome = platformIncome;
    }

    public BigDecimal getLecturerIncome() {
        return lecturerIncome;
    }

    public void setLecturerIncome(BigDecimal lecturerIncome) {
        this.lecturerIncome = lecturerIncome;
    }

    public BigDecimal getAgentIncome() {
        return agentIncome;
    }

    public void setAgentIncome(BigDecimal agentIncome) {
        this.agentIncome = agentIncome;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getChannelType() {
        return channelType;
    }

    public void setChannelType(Integer channelType) {
        this.channelType = channelType;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Integer getIsShowLecturer() {
        return isShowLecturer;
    }

    public void setIsShowLecturer(Integer isShowLecturer) {
        this.isShowLecturer = isShowLecturer;
    }

    public Integer getIsShowUser() {
        return isShowUser;
    }

    public void setIsShowUser(Integer isShowUser) {
        this.isShowUser = isShowUser;
    }

    public Long getActTypeId() {
        return actTypeId;
    }

    public void setActTypeId(Long actTypeId) {
        this.actTypeId = actTypeId;
    }

    public Integer getActType() {
        return actType;
    }

    public void setActType(Integer actType) {
        this.actType = actType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", lecturerUserNo=").append(lecturerUserNo);
        sb.append(", lecturerName=").append(lecturerName);
        sb.append(", userNo=").append(userNo);
        sb.append(", mobile=").append(mobile);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", productType=").append(productType);
        sb.append(", attach=").append(attach);
        sb.append(", pricePayable=").append(pricePayable);
        sb.append(", priceDiscount=").append(priceDiscount);
        sb.append(", pricePaid=").append(pricePaid);
        sb.append(", platformIncome=").append(platformIncome);
        sb.append(", lecturerIncome=").append(lecturerIncome);
        sb.append(", agentIncome=").append(agentIncome);
        sb.append(", tradeType=").append(tradeType);
        sb.append(", payType=").append(payType);
        sb.append(", channelType=").append(channelType);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", payTime=").append(payTime);
        sb.append(", isShowLecturer=").append(isShowLecturer);
        sb.append(", isShowUser=").append(isShowUser);
        sb.append(", actTypeId=").append(actTypeId);
        sb.append(", actType=").append(actType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}