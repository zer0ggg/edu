package com.roncoo.education.data.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseLogExportBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private String beginTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private String endTime;
}
