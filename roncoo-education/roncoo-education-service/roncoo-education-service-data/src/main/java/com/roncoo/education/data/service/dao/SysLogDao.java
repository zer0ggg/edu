package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLogExample;

/**
 * 后台操作日志表 服务类
 *
 * @author wujing
 * @date 2020-06-09
 */
public interface SysLogDao {

    /**
    * 保存后台操作日志表
    *
    * @param record 后台操作日志表
    * @return 影响记录数
    */
    int save(SysLog record);

    /**
    * 根据ID删除后台操作日志表
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 后台操作日志表
    * @return 影响记录数
    */
    int updateById(SysLog record);

    /**
    * 根据ID获取后台操作日志表
    *
    * @param id 主键ID
    * @return 后台操作日志表
    */
    SysLog getById(Long id);

    /**
    * 后台操作日志表--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<SysLog> listForPage(int pageCurrent, int pageSize, SysLogExample example);

}
