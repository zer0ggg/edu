package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.resp.OrderStatVipRESP;
import com.roncoo.education.data.service.pc.biz.PcOrderStatVipBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单会员统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/order/stat/vip")
@Api(value = "data-订单会员统计", tags = {"data-订单会员统计"})
public class PcOrderStatVipController {

    @Autowired
    private PcOrderStatVipBiz biz;

    @ApiOperation(value = "每天会员订单汇总列表", notes = "每天会员订单汇总列表")
    @PostMapping(value = "/list")
    public Result<OrderStatVipRESP> listByAll() {
        return biz.listByAll();
    }
}
