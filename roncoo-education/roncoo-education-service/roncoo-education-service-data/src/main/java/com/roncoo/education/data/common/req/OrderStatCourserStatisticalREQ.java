package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserStatisticalRESP", description="订单课程统计")
public class OrderStatCourserStatisticalREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:题库,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "开始日期")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束日期")
    private String endGmtCreate;

}
