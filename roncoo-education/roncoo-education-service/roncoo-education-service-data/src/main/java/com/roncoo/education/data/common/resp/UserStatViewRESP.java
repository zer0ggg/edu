package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserStatViewRESP", description="用户统计查看")
public class UserStatViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "当天新增用户")
    private String newUserNum;

    @ApiModelProperty(value = "当天登录人次")
    private String loginNum;

    @ApiModelProperty(value = "当天登录人数(去重)")
    private String loginSum;
}
