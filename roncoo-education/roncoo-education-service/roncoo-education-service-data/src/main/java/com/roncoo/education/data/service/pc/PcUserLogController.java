package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.UserLogListREQ;
import com.roncoo.education.data.common.resp.UserLogDaysStatRESP;
import com.roncoo.education.data.common.resp.UserLogListRESP;
import com.roncoo.education.data.service.pc.biz.PcUserLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/user/log")
@Api(value = "data-用户日志", tags = {"data-用户日志"})
public class PcUserLogController {

    @Autowired
    private PcUserLogBiz biz;

    @ApiOperation(value = "用户日志列表", notes = "用户日志列表")
    @PostMapping(value = "/list")
    public Result<Page<UserLogListRESP>> list(@RequestBody UserLogListREQ userLogListREQ) {
        return biz.list(userLogListREQ);
    }

    @ApiOperation(value = "获取当前时间与后七天的时间", notes = "获取当前时间与后七天的时间")
    @PostMapping(value = "/days/stat")
    public Result<UserLogDaysStatRESP> daysStat() {
        return biz.daysStat();
    }
}
