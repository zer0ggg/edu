package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserStatMapper {
    int countByExample(UserStatExample example);

    int deleteByExample(UserStatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserStat record);

    int insertSelective(UserStat record);

    List<UserStat> selectByExample(UserStatExample example);

    UserStat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserStat record, @Param("example") UserStatExample example);

    int updateByExample(@Param("record") UserStat record, @Param("example") UserStatExample example);

    int updateByPrimaryKeySelective(UserStat record);

    int updateByPrimaryKey(UserStat record);
}
