package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 订单统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatEditREQ", description="订单统计修改")
public class OrderStatEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    private LocalDate gmtCreate;

    @ApiModelProperty(value = "当天订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "当天收入")
    private BigDecimal orderIncome;

    @ApiModelProperty(value = "当天利润")
    private BigDecimal orderProfit;

    @ApiModelProperty(value = "当天购买用户")
    private Integer buyUser;

    @ApiModelProperty(value = "当天购买新用户")
    private Integer buyNewUser;
}
