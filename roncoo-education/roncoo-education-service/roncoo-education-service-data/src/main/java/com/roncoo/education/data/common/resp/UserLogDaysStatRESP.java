package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserLogDaysStatRESP", description="用户日志列表")
public class UserLogDaysStatRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "pc端访问数")
    private List<Integer> pcList;

    @ApiModelProperty(value = "微信小程序访问数")
    private List<Integer> xcxList;

    @ApiModelProperty(value = "时间列")
    private List<String> dataList;

}
