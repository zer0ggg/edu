package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CourseLogMapper {
    int countByExample(CourseLogExample example);

    int deleteByExample(CourseLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseLog record);

    int insertSelective(CourseLog record);

    List<CourseLog> selectByExample(CourseLogExample example);

    CourseLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseLog record, @Param("example") CourseLogExample example);

    int updateByExample(@Param("record") CourseLog record, @Param("example") CourseLogExample example);

    int updateByPrimaryKeySelective(CourseLog record);

    int updateByPrimaryKey(CourseLog record);
}