package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderLogREQ", description="订单日志")
public class OrderLogREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户电话")
    private String mobile;

    @ApiModelProperty(value = "订单号")
    private Long orderNo;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类(1:点播;2:直播,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "应付金额")
    private BigDecimal pricePayable;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal priceDiscount;

    @ApiModelProperty(value = "实付金额")
    private BigDecimal pricePaid;

    @ApiModelProperty(value = "平台收入")
    private BigDecimal platformProfit;

    @ApiModelProperty(value = "讲师收入")
    private BigDecimal lecturerProfit;

    @ApiModelProperty(value = "代理收入")
    private BigDecimal agentProfit;

    @ApiModelProperty(value = "交易类型：1线上支付，2线下支付")
    private Integer tradeType;

    @ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
    private Integer payType;

    @ApiModelProperty(value = "购买渠道：（1: PC端；2: APP端；3:微信端； 4:手工绑定）")
    private Integer channelType;

    @ApiModelProperty(value = "订单状态：1待支付，2成功支付，3支付失败，4已关闭")
    private Integer orderStatus;

    @ApiModelProperty(value = "是否显示给讲师(1是，0否)")
    private Integer isShowLecturer;

    @ApiModelProperty(value = "是否显示给用户看(1是，0否)")
    private Integer isShowUser;
}
