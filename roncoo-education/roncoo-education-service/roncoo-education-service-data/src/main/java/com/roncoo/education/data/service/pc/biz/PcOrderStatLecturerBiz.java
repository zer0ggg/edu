package com.roncoo.education.data.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsShowLecturerEnum;
import com.roncoo.education.common.core.enums.ProductTypeEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.req.OrderStatLecturerListREQ;
import com.roncoo.education.data.common.req.OrderStatLecturerStatisticalREQ;
import com.roncoo.education.data.common.req.OrderStatLecturerSummaryPageREQ;
import com.roncoo.education.data.common.resp.OrderStatLecturerListRESP;
import com.roncoo.education.data.common.resp.OrderStatLecturerTodayListRESP;
import com.roncoo.education.data.common.resp.OrderStatLecturerTodayRESP;
import com.roncoo.education.data.common.resp.OrderStatLecturserStatisticalRESP;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.OrderStatLecturerDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturer;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单讲师统计
 *
 * @author wujing
 */
@Component
public class PcOrderStatLecturerBiz extends BaseBiz {

    @Autowired
    private OrderStatLecturerDao dao;
    @Autowired
    private OrderLogDao orderLogDao;

    public Result<Page<OrderStatLecturerListRESP>> list(OrderStatLecturerListREQ req) {
        OrderStatLecturerExample example = new OrderStatLecturerExample();
        OrderStatLecturerExample.Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.like(req.getLecturerName()));
        }
        if (ObjectUtil.isNotNull(req.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(req.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (ObjectUtil.isNotNull(req.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.parseDate(req.getEndGmtCreate(), "yyyy-MM-dd"));
        }
        example.setOrderByClause(" id desc ");
        Page<OrderStatLecturer> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<OrderStatLecturerListRESP> respPage = PageUtil.transform(page, OrderStatLecturerListRESP.class);
        return Result.success(respPage);
    }

    public Result<OrderStatLecturerTodayRESP> today() {
        OrderStatLecturerTodayRESP resp = new OrderStatLecturerTodayRESP();
        Date date = new Date();
        String format = DateUtil.format(date);
        Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        // 列出当天讲师订单（去重）
        List<OrderLog> orderLogList  = orderLogDao.listByGmtCreateAndGroupByLecturerUserNoAndProductTypeAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), ProductTypeEnum.VIP.getCode(), IsShowLecturerEnum.YES.getCode());
        if (orderLogList.isEmpty()) {
            return Result.success(resp);
        }
        List<OrderStatLecturerTodayListRESP> list = new ArrayList<>();
        for (OrderLog orderLog : orderLogList) {
            OrderStatLecturerTodayListRESP listRESP = new OrderStatLecturerTodayListRESP();
            listRESP.setName(orderLog.getLecturerName());
            // 列出当天讲师订单数

            List<OrderLog> lecturerOrder  = orderLogDao.listByGmtCreateAndLecturerUserNoAndIsShowLecturer(startgmtCreate, endgmtCreate, orderLog.getLecturerUserNo(), IsShowLecturerEnum.YES.getCode());

            listRESP.setValue(lecturerOrder.size());
            // 当天统计讲师收益
            BigDecimal lecturerIncome = orderLogDao.lecturerIncomeByGmtCreateAndLecturerUserNoAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), orderLog.getLecturerUserNo(), IsShowLecturerEnum.YES.getCode());
            listRESP.setLecturerIncome(lecturerIncome);
            list.add(listRESP);
        }
        resp.setList(list);
        return Result.success(resp);
    }

    public Result<OrderStatLecturserStatisticalRESP> statistical(OrderStatLecturerStatisticalREQ req) {
        OrderStatLecturserStatisticalRESP resp = new OrderStatLecturserStatisticalRESP();

        // 获取最新10条订单信息
        List<OrderStatLecturer> list = dao.listByShowCount(req.getShowCount(), req.getLecturerName(), req.getBeginGmtCreate(), req.getEndGmtCreate());
        List<String> lecturerName = new ArrayList<>();
        List<String> lecturerOrder = new ArrayList<>();
        List<String> lecturerIncome = new ArrayList<>();
        for (OrderStatLecturer orderStatLecturer:  list) {
            lecturerName.add(orderStatLecturer.getLecturerName());
            lecturerOrder.add(String.valueOf(orderStatLecturer.getLecturerOrder()));
            lecturerIncome.add(String.valueOf(orderStatLecturer.getLecturerIncome()));
        }
        resp.setList(lecturerName);
        resp.setLecturerOrderList(lecturerOrder);
        resp.setLecturerIncomeList(lecturerIncome);
        return Result.success(resp);
    }

    public Result<Page<OrderStatLecturerListRESP>> summaryList(OrderStatLecturerSummaryPageREQ req) {
        Page<OrderStatCourser> page = dao.summaryList(req.getLecturerName(), req.getBeginGmtCreate(), req.getEndGmtCreate(), req.getPageCurrent(),req.getPageSize());
        Page<OrderStatLecturerListRESP> respPage = PageUtil.transform(page, OrderStatLecturerListRESP.class);
        return Result.success(respPage);
    }
}
