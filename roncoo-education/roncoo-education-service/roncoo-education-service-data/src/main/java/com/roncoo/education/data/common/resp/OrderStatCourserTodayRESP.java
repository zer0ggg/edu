package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserTodayRESP", description="订单课程统计")
public class OrderStatCourserTodayRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程订单")
    private List<OrderStatCourserTodayListRESP> list = new ArrayList<>();
}
