package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.CourseStatUserQO;
import com.roncoo.education.data.feign.vo.CourseStatUserVO;
import com.roncoo.education.data.service.dao.CourseStatUserDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程用户统计
 *
 * @author wujing
 */
@Component
public class FeignCourseStatUserBiz extends BaseBiz {

    @Autowired
    private CourseStatUserDao dao;

	public Page<CourseStatUserVO> listForPage(CourseStatUserQO qo) {
	    CourseStatUserExample example = new CourseStatUserExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<CourseStatUser> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseStatUserVO.class);
	}

	public int save(CourseStatUserQO qo) {
		CourseStatUser record = BeanUtil.copyProperties(qo, CourseStatUser.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseStatUserVO getById(Long id) {
		CourseStatUser record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseStatUserVO.class);
	}

	public int updateById(CourseStatUserQO qo) {
		CourseStatUser record = BeanUtil.copyProperties(qo, CourseStatUser.class);
		return dao.updateById(record);
	}

}
