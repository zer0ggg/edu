package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单日志 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface OrderLogDao {

    int save(OrderLog record);

    int deleteById(Long id);

    int updateById(OrderLog record);

    OrderLog getById(Long id);

    Page<OrderLog> listForPage(int pageCurrent, int pageSize, OrderLogExample example);

    /**
     * 列出所有当天订单数
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    List<OrderLog> listByGmtCreateAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Integer IsShowLecturer);
    /**
     * 列出当天订单数
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param productId
     * @return
     */
    List<OrderLog> listByGmtCreateAndProductIdAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Long productId, Integer IsShowLecturer);


    /**
     * 当天订单金额
     * @param startgmtCreate
     * @param endgmtCreate
     * @param productId
     * @return
     */
    OrderLog orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Long productId, Integer isShowLecturer);

    /**
     * 当天购买用户(去重）
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    Integer buyUserByGmtCreateAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer isShowLecturer);

    /**
     * 列出当天课程订单 （去重）会员订单除外
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param productType  vip
     * @return
     */
    List<OrderLog> listByGmtCreateAndNotProductTypeAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer productType, Integer isShowLecturer);
    /**
     * 列出当天会员订单 （去重）
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    int orderNumByGmtCreateAndProductType(Date startgmtCreate, Date endgmtCreate, Integer productType);

    /**
     * 列出当天讲师订单（去重）
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    List<OrderLog> listByGmtCreateAndGroupByLecturerUserNoAndProductTypeAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer productType, Integer isShowLecturer);

    /**
     * 列出当前讲师订单数
     * @param startgmtCreate
     * @param endgmtCreate
     * @param lecturerUserNo
     * @return
     */
    List<OrderLog> listByGmtCreateAndLecturerUserNoAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Long lecturerUserNo, Integer isShowLecturer);

    /**
     * 统计当天收益
     * @param startgmtCreate
     * @param endgmtCreate
     * @param lecturerUserNo
     * @return
     */
    BigDecimal lecturerIncomeByGmtCreateAndLecturerUserNoAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Long lecturerUserNo, Integer isShowLecturer);
    /**
     * 根据订单号获取订单日志信息
     *
     * @param orderNo
     * @return
     */
    OrderLog getByOrderNo(Long orderNo);
    /**
     * 根据支付类型、时间统计订单数
     * @param payType
     * @return
     */
    Integer sumByPayTypeAndGmtCreate(Integer payType, String startgmtCreate, String endgmtCreate);

    /**
     * 统计当天收益 会员订单
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param productType
     * @return
     */
    BigDecimal orderIncomeByGmtCreateAndProductType(String startgmtCreate, String endgmtCreate, Integer productType);

    /**
     * 根据订单号更新订单日志
     * @param record
     * @return
     */
    int updateByOrderNo(OrderLog record);

    /**
     * 根据用户编号获取用户订单信息记录
     *
     * @param userNo
     * @return
     */
    List<OrderLog> listByUserNo(Long userNo);
    /**
     * 根据用户编号更新手机号
     *
     * @param userNo
     * @param mobile
     *
     * @return
     */
    int updateByMobile(Long userNo, String mobile);
}
