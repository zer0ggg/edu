package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.OrderStatVipDao;
import com.roncoo.education.data.service.dao.impl.mapper.OrderStatVipMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVip;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVipExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 订单会员统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-23
 */
@Repository
public class OrderStatVipDaoImpl implements OrderStatVipDao {

    @Autowired
    private OrderStatVipMapper mapper;

    @Override
    public int save(OrderStatVip record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderStatVip record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderStatVip getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<OrderStatVip> listForPage(int pageCurrent, int pageSize, OrderStatVipExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent((int)count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage((int)count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderStatVip>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public OrderStatVip getByGmtCreate(Date gmtCreate) {
        OrderStatVipExample example = new OrderStatVipExample();
        OrderStatVipExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(gmtCreate);
        List<OrderStatVip> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<OrderStatVip> listByAll() {
        OrderStatVipExample example = new OrderStatVipExample();
        OrderStatVipExample.Criteria c = example.createCriteria();
        return this.mapper.selectByExample(example);
    }


}
