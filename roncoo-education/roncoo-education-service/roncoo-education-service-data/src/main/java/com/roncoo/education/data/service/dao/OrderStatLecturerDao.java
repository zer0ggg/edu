package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturer;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample;

import java.util.Date;
import java.util.List;

/**
 * 订单讲师统计 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface OrderStatLecturerDao {

    int save(OrderStatLecturer record);

    int deleteById(Long id);

    int updateById(OrderStatLecturer record);

    OrderStatLecturer getById(Long id);

    Page<OrderStatLecturer> listForPage(int pageCurrent, int pageSize, OrderStatLecturerExample example);

    /**
     * 获取讲师当天统计信息
     *
     * @param lecturerUserNo
     * @param gmtCreate
     * @return
     */
    OrderStatLecturer getByLecturerUserNoAndGmtCreate(Long lecturerUserNo, Date gmtCreate);

    /**
     * 获取讲师订单统计信息
     * @return
     */
    List<OrderStatLecturer> listByShowCount(int showCount, String lecturerName, String beginGmtCreate, String endGmtCreate);

    /**
     * 汇总列表
     *
     * @param lecturerName
     * @param beginGmtCreate
     * @param endGmtCreate
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    Page<OrderStatCourser> summaryList(String lecturerName, String beginGmtCreate, String endGmtCreate, int pageCurrent, int pageSize);
}
