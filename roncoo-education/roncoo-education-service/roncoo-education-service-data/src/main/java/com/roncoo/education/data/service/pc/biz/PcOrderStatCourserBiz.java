package com.roncoo.education.data.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsShowLecturerEnum;
import com.roncoo.education.common.core.enums.ProductTypeEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.req.OrderStatCourserListREQ;
import com.roncoo.education.data.common.req.OrderStatCourserStatisticalREQ;
import com.roncoo.education.data.common.resp.OrderStatCourserListRESP;
import com.roncoo.education.data.common.resp.OrderStatCourserStatisticalRESP;
import com.roncoo.education.data.common.resp.OrderStatCourserTodayListRESP;
import com.roncoo.education.data.common.resp.OrderStatCourserTodayRESP;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.OrderStatCourserDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单课程统计
 *
 * @author wujing
 */
@Component
public class PcOrderStatCourserBiz extends BaseBiz {

    @Autowired
    private OrderStatCourserDao dao;
    @Autowired
    private OrderLogDao orderLogDao;
    /**
    * 订单课程统计列表
    *
    * @return 订单课程统计分页查询结果
    */
    public Result<Page<OrderStatCourserListRESP>> list(OrderStatCourserListREQ req) {
        OrderStatCourserExample example = new OrderStatCourserExample();
        OrderStatCourserExample.Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(req.getCourseName()));
        }
        if (req.getCourseCategory() != null) {
            c.andCourseCategoryEqualTo(req.getCourseCategory());
        }
        if (ObjectUtil.isNotNull(req.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(req.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (ObjectUtil.isNotNull(req.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.parseDate(req.getEndGmtCreate(), "yyyy-MM-dd"));
        }
        example.setOrderByClause(" id desc ");
        Page<OrderStatCourser> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<OrderStatCourserListRESP> respPage = PageUtil.transform(page, OrderStatCourserListRESP.class);
        return Result.success(respPage);
    }


    public Result<OrderStatCourserTodayRESP> today() {
        OrderStatCourserTodayRESP resp = new OrderStatCourserTodayRESP();
        Date date = new Date();
        String format = DateUtil.format(date);
        Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        // 列出当天课程订单（去重）
        List<OrderLog> orderLogList  = orderLogDao.listByGmtCreateAndNotProductTypeAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), ProductTypeEnum.VIP.getCode(), IsShowLecturerEnum.YES.getCode());
        if (orderLogList.isEmpty()) {
            return Result.success(resp);
        }
        List<OrderStatCourserTodayListRESP> list = new ArrayList<>();
        for (OrderLog orderLog : orderLogList) {
            OrderStatCourserTodayListRESP listRESP = new OrderStatCourserTodayListRESP();
            listRESP.setName(orderLog.getProductName());
            // 列出当天课程订单数
            List<OrderLog> courseNum  = orderLogDao.listByGmtCreateAndProductIdAndIsShowLecturer(startgmtCreate, endgmtCreate, orderLog.getProductId(), IsShowLecturerEnum.YES.getCode());
            listRESP.setValue(courseNum.size());
            // 当天订单课程金额
            OrderLog price  = orderLogDao.orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), orderLog.getProductId(), IsShowLecturerEnum.YES.getCode());
            listRESP.setCourseIncome(price.getPricePaid());
            listRESP.setCourseProfit(price.getPlatformIncome());
            list.add(listRESP);
        }
        resp.setList(list);
        return Result.success(resp);
    }

    public Result<OrderStatCourserStatisticalRESP> statistical(OrderStatCourserStatisticalREQ req) {
        int showCount = 10;
        OrderStatCourserStatisticalRESP resp = new OrderStatCourserStatisticalRESP();
        List<OrderStatCourser> list = dao.listByShowCount(showCount, req.getCourseName(), req.getCourseCategory(), req.getBeginGmtCreate(), req.getEndGmtCreate());
        List<String> courseName = new ArrayList<>();
        List<String> courseNum = new ArrayList<>();
        List<String> courseIncome = new ArrayList<>();
        List<String> courseProfit = new ArrayList<>();
        for (OrderStatCourser orderStatCourser:  list) {
            courseName.add(orderStatCourser.getCourseName());
            //标题过长，截取
            if (list.size() > 6 && StrUtil.length(orderStatCourser.getCourseName()) > 6) {
                courseName.add(orderStatCourser.getCourseName().substring(0, 6));
            }
            courseNum.add(String.valueOf(orderStatCourser.getCourseNum()));
            courseIncome.add(String.valueOf(orderStatCourser.getCourseIncome()));
            courseProfit.add(String.valueOf(orderStatCourser.getCourseProfit()));
        }
        resp.setList(courseName);
        resp.setCourseNumList(courseNum);
        resp.setCourseIncomeList(courseIncome);
        resp.setCourseProfitList(courseProfit);
        return Result.success(resp);
    }
}
