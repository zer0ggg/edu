package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CourseLogExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public CourseLogExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNull() {
            addCriterion("course_category is null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNotNull() {
            addCriterion("course_category is not null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryEqualTo(Integer value) {
            addCriterion("course_category =", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotEqualTo(Integer value) {
            addCriterion("course_category <>", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThan(Integer value) {
            addCriterion("course_category >", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_category >=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThan(Integer value) {
            addCriterion("course_category <", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThanOrEqualTo(Integer value) {
            addCriterion("course_category <=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIn(List<Integer> values) {
            addCriterion("course_category in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotIn(List<Integer> values) {
            addCriterion("course_category not in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryBetween(Integer value1, Integer value2) {
            addCriterion("course_category between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotBetween(Integer value1, Integer value2) {
            addCriterion("course_category not between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andPeriodIdIsNull() {
            addCriterion("period_id is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIdIsNotNull() {
            addCriterion("period_id is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodIdEqualTo(Long value) {
            addCriterion("period_id =", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdNotEqualTo(Long value) {
            addCriterion("period_id <>", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdGreaterThan(Long value) {
            addCriterion("period_id >", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdGreaterThanOrEqualTo(Long value) {
            addCriterion("period_id >=", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdLessThan(Long value) {
            addCriterion("period_id <", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdLessThanOrEqualTo(Long value) {
            addCriterion("period_id <=", value, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdIn(List<Long> values) {
            addCriterion("period_id in", values, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdNotIn(List<Long> values) {
            addCriterion("period_id not in", values, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdBetween(Long value1, Long value2) {
            addCriterion("period_id between", value1, value2, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodIdNotBetween(Long value1, Long value2) {
            addCriterion("period_id not between", value1, value2, "periodId");
            return (Criteria) this;
        }

        public Criteria andPeriodNameIsNull() {
            addCriterion("period_name is null");
            return (Criteria) this;
        }

        public Criteria andPeriodNameIsNotNull() {
            addCriterion("period_name is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodNameEqualTo(String value) {
            addCriterion("period_name =", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameNotEqualTo(String value) {
            addCriterion("period_name <>", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameGreaterThan(String value) {
            addCriterion("period_name >", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameGreaterThanOrEqualTo(String value) {
            addCriterion("period_name >=", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameLessThan(String value) {
            addCriterion("period_name <", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameLessThanOrEqualTo(String value) {
            addCriterion("period_name <=", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameLike(String value) {
            addCriterion("period_name like", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameNotLike(String value) {
            addCriterion("period_name not like", value, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameIn(List<String> values) {
            addCriterion("period_name in", values, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameNotIn(List<String> values) {
            addCriterion("period_name not in", values, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameBetween(String value1, String value2) {
            addCriterion("period_name between", value1, value2, "periodName");
            return (Criteria) this;
        }

        public Criteria andPeriodNameNotBetween(String value1, String value2) {
            addCriterion("period_name not between", value1, value2, "periodName");
            return (Criteria) this;
        }

        public Criteria andChapterIdIsNull() {
            addCriterion("chapter_id is null");
            return (Criteria) this;
        }

        public Criteria andChapterIdIsNotNull() {
            addCriterion("chapter_id is not null");
            return (Criteria) this;
        }

        public Criteria andChapterIdEqualTo(Long value) {
            addCriterion("chapter_id =", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdNotEqualTo(Long value) {
            addCriterion("chapter_id <>", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdGreaterThan(Long value) {
            addCriterion("chapter_id >", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdGreaterThanOrEqualTo(Long value) {
            addCriterion("chapter_id >=", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdLessThan(Long value) {
            addCriterion("chapter_id <", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdLessThanOrEqualTo(Long value) {
            addCriterion("chapter_id <=", value, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdIn(List<Long> values) {
            addCriterion("chapter_id in", values, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdNotIn(List<Long> values) {
            addCriterion("chapter_id not in", values, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdBetween(Long value1, Long value2) {
            addCriterion("chapter_id between", value1, value2, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterIdNotBetween(Long value1, Long value2) {
            addCriterion("chapter_id not between", value1, value2, "chapterId");
            return (Criteria) this;
        }

        public Criteria andChapterNameIsNull() {
            addCriterion("chapter_name is null");
            return (Criteria) this;
        }

        public Criteria andChapterNameIsNotNull() {
            addCriterion("chapter_name is not null");
            return (Criteria) this;
        }

        public Criteria andChapterNameEqualTo(String value) {
            addCriterion("chapter_name =", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameNotEqualTo(String value) {
            addCriterion("chapter_name <>", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameGreaterThan(String value) {
            addCriterion("chapter_name >", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameGreaterThanOrEqualTo(String value) {
            addCriterion("chapter_name >=", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameLessThan(String value) {
            addCriterion("chapter_name <", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameLessThanOrEqualTo(String value) {
            addCriterion("chapter_name <=", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameLike(String value) {
            addCriterion("chapter_name like", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameNotLike(String value) {
            addCriterion("chapter_name not like", value, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameIn(List<String> values) {
            addCriterion("chapter_name in", values, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameNotIn(List<String> values) {
            addCriterion("chapter_name not in", values, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameBetween(String value1, String value2) {
            addCriterion("chapter_name between", value1, value2, "chapterName");
            return (Criteria) this;
        }

        public Criteria andChapterNameNotBetween(String value1, String value2) {
            addCriterion("chapter_name not between", value1, value2, "chapterName");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNull() {
            addCriterion("course_name is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNotNull() {
            addCriterion("course_name is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEqualTo(String value) {
            addCriterion("course_name =", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotEqualTo(String value) {
            addCriterion("course_name <>", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThan(String value) {
            addCriterion("course_name >", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThanOrEqualTo(String value) {
            addCriterion("course_name >=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThan(String value) {
            addCriterion("course_name <", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThanOrEqualTo(String value) {
            addCriterion("course_name <=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLike(String value) {
            addCriterion("course_name like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotLike(String value) {
            addCriterion("course_name not like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameIn(List<String> values) {
            addCriterion("course_name in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotIn(List<String> values) {
            addCriterion("course_name not in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameBetween(String value1, String value2) {
            addCriterion("course_name between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotBetween(String value1, String value2) {
            addCriterion("course_name not between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andVidIsNull() {
            addCriterion("vid is null");
            return (Criteria) this;
        }

        public Criteria andVidIsNotNull() {
            addCriterion("vid is not null");
            return (Criteria) this;
        }

        public Criteria andVidEqualTo(String value) {
            addCriterion("vid =", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidNotEqualTo(String value) {
            addCriterion("vid <>", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidGreaterThan(String value) {
            addCriterion("vid >", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidGreaterThanOrEqualTo(String value) {
            addCriterion("vid >=", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidLessThan(String value) {
            addCriterion("vid <", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidLessThanOrEqualTo(String value) {
            addCriterion("vid <=", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidLike(String value) {
            addCriterion("vid like", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidNotLike(String value) {
            addCriterion("vid not like", value, "vid");
            return (Criteria) this;
        }

        public Criteria andVidIn(List<String> values) {
            addCriterion("vid in", values, "vid");
            return (Criteria) this;
        }

        public Criteria andVidNotIn(List<String> values) {
            addCriterion("vid not in", values, "vid");
            return (Criteria) this;
        }

        public Criteria andVidBetween(String value1, String value2) {
            addCriterion("vid between", value1, value2, "vid");
            return (Criteria) this;
        }

        public Criteria andVidNotBetween(String value1, String value2) {
            addCriterion("vid not between", value1, value2, "vid");
            return (Criteria) this;
        }

        public Criteria andVidLengthIsNull() {
            addCriterion("vid_length is null");
            return (Criteria) this;
        }

        public Criteria andVidLengthIsNotNull() {
            addCriterion("vid_length is not null");
            return (Criteria) this;
        }

        public Criteria andVidLengthEqualTo(String value) {
            addCriterion("vid_length =", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthNotEqualTo(String value) {
            addCriterion("vid_length <>", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthGreaterThan(String value) {
            addCriterion("vid_length >", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthGreaterThanOrEqualTo(String value) {
            addCriterion("vid_length >=", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthLessThan(String value) {
            addCriterion("vid_length <", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthLessThanOrEqualTo(String value) {
            addCriterion("vid_length <=", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthLike(String value) {
            addCriterion("vid_length like", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthNotLike(String value) {
            addCriterion("vid_length not like", value, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthIn(List<String> values) {
            addCriterion("vid_length in", values, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthNotIn(List<String> values) {
            addCriterion("vid_length not in", values, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthBetween(String value1, String value2) {
            addCriterion("vid_length between", value1, value2, "vidLength");
            return (Criteria) this;
        }

        public Criteria andVidLengthNotBetween(String value1, String value2) {
            addCriterion("vid_length not between", value1, value2, "vidLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthIsNull() {
            addCriterion("watch_length is null");
            return (Criteria) this;
        }

        public Criteria andWatchLengthIsNotNull() {
            addCriterion("watch_length is not null");
            return (Criteria) this;
        }

        public Criteria andWatchLengthEqualTo(BigDecimal value) {
            addCriterion("watch_length =", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthNotEqualTo(BigDecimal value) {
            addCriterion("watch_length <>", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthGreaterThan(BigDecimal value) {
            addCriterion("watch_length >", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("watch_length >=", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthLessThan(BigDecimal value) {
            addCriterion("watch_length <", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("watch_length <=", value, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthIn(List<BigDecimal> values) {
            addCriterion("watch_length in", values, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthNotIn(List<BigDecimal> values) {
            addCriterion("watch_length not in", values, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("watch_length between", value1, value2, "watchLength");
            return (Criteria) this;
        }

        public Criteria andWatchLengthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("watch_length not between", value1, value2, "watchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthIsNull() {
            addCriterion("biggest_watch_length is null");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthIsNotNull() {
            addCriterion("biggest_watch_length is not null");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthEqualTo(BigDecimal value) {
            addCriterion("biggest_watch_length =", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthNotEqualTo(BigDecimal value) {
            addCriterion("biggest_watch_length <>", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthGreaterThan(BigDecimal value) {
            addCriterion("biggest_watch_length >", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("biggest_watch_length >=", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthLessThan(BigDecimal value) {
            addCriterion("biggest_watch_length <", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("biggest_watch_length <=", value, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthIn(List<BigDecimal> values) {
            addCriterion("biggest_watch_length in", values, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthNotIn(List<BigDecimal> values) {
            addCriterion("biggest_watch_length not in", values, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("biggest_watch_length between", value1, value2, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andBiggestWatchLengthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("biggest_watch_length not between", value1, value2, "biggestWatchLength");
            return (Criteria) this;
        }

        public Criteria andWatckProgressIsNull() {
            addCriterion("watck_progress is null");
            return (Criteria) this;
        }

        public Criteria andWatckProgressIsNotNull() {
            addCriterion("watck_progress is not null");
            return (Criteria) this;
        }

        public Criteria andWatckProgressEqualTo(Integer value) {
            addCriterion("watck_progress =", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressNotEqualTo(Integer value) {
            addCriterion("watck_progress <>", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressGreaterThan(Integer value) {
            addCriterion("watck_progress >", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressGreaterThanOrEqualTo(Integer value) {
            addCriterion("watck_progress >=", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressLessThan(Integer value) {
            addCriterion("watck_progress <", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressLessThanOrEqualTo(Integer value) {
            addCriterion("watck_progress <=", value, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressIn(List<Integer> values) {
            addCriterion("watck_progress in", values, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressNotIn(List<Integer> values) {
            addCriterion("watck_progress not in", values, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressBetween(Integer value1, Integer value2) {
            addCriterion("watck_progress between", value1, value2, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andWatckProgressNotBetween(Integer value1, Integer value2) {
            addCriterion("watck_progress not between", value1, value2, "watckProgress");
            return (Criteria) this;
        }

        public Criteria andDurationIsNull() {
            addCriterion("duration is null");
            return (Criteria) this;
        }

        public Criteria andDurationIsNotNull() {
            addCriterion("duration is not null");
            return (Criteria) this;
        }

        public Criteria andDurationEqualTo(BigDecimal value) {
            addCriterion("duration =", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotEqualTo(BigDecimal value) {
            addCriterion("duration <>", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThan(BigDecimal value) {
            addCriterion("duration >", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("duration >=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThan(BigDecimal value) {
            addCriterion("duration <", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("duration <=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationIn(List<BigDecimal> values) {
            addCriterion("duration in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotIn(List<BigDecimal> values) {
            addCriterion("duration not in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("duration between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("duration not between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationIsNull() {
            addCriterion("total_duration is null");
            return (Criteria) this;
        }

        public Criteria andTotalDurationIsNotNull() {
            addCriterion("total_duration is not null");
            return (Criteria) this;
        }

        public Criteria andTotalDurationEqualTo(BigDecimal value) {
            addCriterion("total_duration =", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationNotEqualTo(BigDecimal value) {
            addCriterion("total_duration <>", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationGreaterThan(BigDecimal value) {
            addCriterion("total_duration >", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_duration >=", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationLessThan(BigDecimal value) {
            addCriterion("total_duration <", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_duration <=", value, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationIn(List<BigDecimal> values) {
            addCriterion("total_duration in", values, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationNotIn(List<BigDecimal> values) {
            addCriterion("total_duration not in", values, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_duration between", value1, value2, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andTotalDurationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_duration not between", value1, value2, "totalDuration");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalIsNull() {
            addCriterion("residue_contrast_total is null");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalIsNotNull() {
            addCriterion("residue_contrast_total is not null");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalEqualTo(Integer value) {
            addCriterion("residue_contrast_total =", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalNotEqualTo(Integer value) {
            addCriterion("residue_contrast_total <>", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalGreaterThan(Integer value) {
            addCriterion("residue_contrast_total >", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalGreaterThanOrEqualTo(Integer value) {
            addCriterion("residue_contrast_total >=", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalLessThan(Integer value) {
            addCriterion("residue_contrast_total <", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalLessThanOrEqualTo(Integer value) {
            addCriterion("residue_contrast_total <=", value, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalIn(List<Integer> values) {
            addCriterion("residue_contrast_total in", values, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalNotIn(List<Integer> values) {
            addCriterion("residue_contrast_total not in", values, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalBetween(Integer value1, Integer value2) {
            addCriterion("residue_contrast_total between", value1, value2, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andResidueContrastTotalNotBetween(Integer value1, Integer value2) {
            addCriterion("residue_contrast_total not between", value1, value2, "residueContrastTotal");
            return (Criteria) this;
        }

        public Criteria andContrastResultIsNull() {
            addCriterion("contrast_result is null");
            return (Criteria) this;
        }

        public Criteria andContrastResultIsNotNull() {
            addCriterion("contrast_result is not null");
            return (Criteria) this;
        }

        public Criteria andContrastResultEqualTo(String value) {
            addCriterion("contrast_result =", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultNotEqualTo(String value) {
            addCriterion("contrast_result <>", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultGreaterThan(String value) {
            addCriterion("contrast_result >", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultGreaterThanOrEqualTo(String value) {
            addCriterion("contrast_result >=", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultLessThan(String value) {
            addCriterion("contrast_result <", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultLessThanOrEqualTo(String value) {
            addCriterion("contrast_result <=", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultLike(String value) {
            addCriterion("contrast_result like", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultNotLike(String value) {
            addCriterion("contrast_result not like", value, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultIn(List<String> values) {
            addCriterion("contrast_result in", values, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultNotIn(List<String> values) {
            addCriterion("contrast_result not in", values, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultBetween(String value1, String value2) {
            addCriterion("contrast_result between", value1, value2, "contrastResult");
            return (Criteria) this;
        }

        public Criteria andContrastResultNotBetween(String value1, String value2) {
            addCriterion("contrast_result not between", value1, value2, "contrastResult");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}