package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserStatisticalRESP", description="订单课程统计")
public class OrderStatCourserStatisticalRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private List<String> list = new ArrayList<>();

    @ApiModelProperty(value = "订单数数量")
    private List<String> courseNumList = new ArrayList<>();

    @ApiModelProperty(value = "课程收入")
    private List<String> courseIncomeList = new ArrayList<>();

    @ApiModelProperty(value = "课程利润")
    private List<String> courseProfitList = new ArrayList<>();
}
