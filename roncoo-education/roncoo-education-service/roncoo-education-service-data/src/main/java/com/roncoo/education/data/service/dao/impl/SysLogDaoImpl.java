package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.SysLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.SysLogMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 后台操作日志表 服务实现类
 *
 * @author wujing
 * @date 2020-06-09
 */
@Repository
public class SysLogDaoImpl implements SysLogDao {

    @Autowired
    private SysLogMapper mapper;

    @Override
    public int save(SysLog record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysLog record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysLog getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysLog> listForPage(int pageCurrent, int pageSize, SysLogExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


}
