package com.roncoo.education.data.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 课程日志
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseLogDTO", description="课程日志")
public class CourseLogPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "课程类型")
    private Integer courseCategory;

    @ApiModelProperty(value = "课时ID")
    private Long periodId;

    @ApiModelProperty(value = "课时名称")
    private String periodName;

    @ApiModelProperty(value = "章节ID")
    private Long chapterId;

    @ApiModelProperty(value = "章节名称")
    private String chapterName;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "视频VID")
    private String vid;

    @ApiModelProperty(value = "课时时长")
    private String vidLength;

    @ApiModelProperty(value = "观看时长")
    private BigDecimal biggestWatchLength;

    @ApiModelProperty(value = "观看时长(HH:mm:ss)")
    private String stringWatchLength;

    @ApiModelProperty(value = "观看进度")
    private Integer watckProgress;
}
