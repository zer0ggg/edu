package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.CourseStatQO;
import com.roncoo.education.data.feign.vo.CourseStatVO;
import com.roncoo.education.data.service.dao.CourseStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程统计
 *
 * @author wujing
 */
@Component
public class FeignCourseStatBiz extends BaseBiz {

    @Autowired
    private CourseStatDao dao;

	public Page<CourseStatVO> listForPage(CourseStatQO qo) {
	    CourseStatExample example = new CourseStatExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<CourseStat> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseStatVO.class);
	}

	public int save(CourseStatQO qo) {
		CourseStat record = BeanUtil.copyProperties(qo, CourseStat.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseStatVO getById(Long id) {
		CourseStat record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseStatVO.class);
	}

	public int updateById(CourseStatQO qo) {
		CourseStat record = BeanUtil.copyProperties(qo, CourseStat.class);
		return dao.updateById(record);
	}

}
