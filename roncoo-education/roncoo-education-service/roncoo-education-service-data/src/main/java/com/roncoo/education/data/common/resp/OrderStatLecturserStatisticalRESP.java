package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单讲师统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatLecturserStatisticalRESP", description="订单讲师统计")
public class OrderStatLecturserStatisticalRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private List<String> list = new ArrayList<>();

    @ApiModelProperty(value = "订单数数量")
    private List<String> lecturerOrderList = new ArrayList<>();

    @ApiModelProperty(value = "讲师收入")
    private List<String> lecturerIncomeList = new ArrayList<>();
}
