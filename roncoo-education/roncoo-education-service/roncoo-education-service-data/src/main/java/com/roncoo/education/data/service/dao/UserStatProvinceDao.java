package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvince;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvinceExample;

import java.util.Date;
import java.util.List;

/**
 * 用户省统计 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface UserStatProvinceDao {

    int save(UserStatProvince record);

    int deleteById(Long id);

    int updateById(UserStatProvince record);

    UserStatProvince getById(Long id);

    Page<UserStatProvince> listForPage(int pageCurrent, int pageSize, UserStatProvinceExample example);

    /**
     * 获取当天省登录统计记录
     * @param gmtCreate
     * @param province
     * @return
     */
    UserStatProvince getByGmtCreateAndProvince(Date gmtCreate, String province);

    /**
     * 获取当天省登录统计记录
     *
     * @return
     */
    List<UserStatProvince> listByGmtCreate(Date gmtCreate);
}
