package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.OrderStatCourserListREQ;
import com.roncoo.education.data.common.req.OrderStatCourserStatisticalREQ;
import com.roncoo.education.data.common.resp.OrderStatCourserListRESP;
import com.roncoo.education.data.common.resp.OrderStatCourserStatisticalRESP;
import com.roncoo.education.data.common.resp.OrderStatCourserTodayRESP;
import com.roncoo.education.data.service.pc.biz.PcOrderStatCourserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单课程统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/order/stat/courser")
@Api(value = "data-订单课程统计", tags = {"data-订单课程统计"})
public class PcOrderStatCourserController {

    @Autowired
    private PcOrderStatCourserBiz biz;

    @ApiOperation(value = "订单课程统计列表", notes = "订单课程统计列表")
    @PostMapping(value = "/list")
    public Result<Page<OrderStatCourserListRESP>> list(@RequestBody OrderStatCourserListREQ orderStatCourserListREQ) {
        return biz.list(orderStatCourserListREQ);
    }

    @ApiOperation(value = "订单课程当天统计", notes = "订单课程当天统计")
    @PostMapping(value = "/today")
    public Result<OrderStatCourserTodayRESP> today() {
        return biz.today();
    }


    @ApiOperation(value = "订单课程统计", notes = "订单课程统计")
    @PostMapping(value = "/statistical")
    public Result<OrderStatCourserStatisticalRESP> statistical(@RequestBody OrderStatCourserStatisticalREQ orderStatCourserStatisticalREQ) {
        return biz.statistical(orderStatCourserStatisticalREQ);
    }

}
