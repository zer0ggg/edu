package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.PayTypeEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.req.OrderLogListREQ;
import com.roncoo.education.data.common.resp.OrderLogDaysStatRESP;
import com.roncoo.education.data.common.resp.OrderLogListRESP;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 订单日志
 *
 * @author wujing
 */
@Component
public class PcOrderLogBiz extends BaseBiz {

    @Autowired
    private OrderLogDao dao;

    /**
    * 订单日志列表
    *
    * @param orderLogListREQ 订单日志分页查询参数
    * @return 订单日志分页查询结果
    */
    public Result<Page<OrderLogListRESP>> list(OrderLogListREQ orderLogListREQ) {
        OrderLogExample example = new OrderLogExample();
        Criteria c = example.createCriteria();
        if (orderLogListREQ.getOrderNo() != null) {
            c.andOrderNoEqualTo(orderLogListREQ.getOrderNo());
        }
        if (orderLogListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(orderLogListREQ.getUserNo());
        }
        if (StringUtils.hasText(orderLogListREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(orderLogListREQ.getMobile()));
        }
        if (StringUtils.hasText(orderLogListREQ.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.like(orderLogListREQ.getLecturerName()));
        }
        if (StringUtils.hasText(orderLogListREQ.getProductName())) {
            c.andProductNameLike(PageUtil.like(orderLogListREQ.getProductName()));
        }
        if (orderLogListREQ.getProductType() != null) {
            c.andProductTypeEqualTo(orderLogListREQ.getProductType());
        }
        if (orderLogListREQ.getTradeType() != null) {
            c.andTradeTypeEqualTo(orderLogListREQ.getTradeType());
        }
        if (orderLogListREQ.getPayType() != null) {
            c.andPayTypeEqualTo(orderLogListREQ.getPayType());
        }
        if (orderLogListREQ.getChannelType() != null) {
            c.andChannelTypeEqualTo(orderLogListREQ.getChannelType());
        }
        if (orderLogListREQ.getOrderStatus() != null) {
            c.andOrderStatusEqualTo(orderLogListREQ.getOrderStatus());
        }
        example.setOrderByClause(" id desc ");
        Page<OrderLog> page = dao.listForPage(orderLogListREQ.getPageCurrent(), orderLogListREQ.getPageSize(), example);
        Page<OrderLogListRESP> respPage = PageUtil.transform(page, OrderLogListRESP.class);
        return Result.success(respPage);
    }

    public Result<OrderLogDaysStatRESP> daysStat() {
        OrderLogDaysStatRESP resp = new OrderLogDaysStatRESP();
        resp.setDataList(payTime());
        List<Integer> weixinList = new ArrayList<>(); // 微信扫码支付
        List<Integer> xcxList = new ArrayList<>(); // 微信小程序支付订单数
        List<Integer> alipayList = new ArrayList<>(); // 支付宝扫码支付订单数
        List<Integer> manualList = new ArrayList<>(); // 线下支付订单数
        for (String date : resp.getDataList()) {

            Date startgmtCreate = DateUtil.parseDate(date + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            Date endgmtCreate = DateUtil.parseDate(date + " 23:59:59", "yyyy-MM-dd HH:mm:ss");

            // 微信扫码支付
            Integer weixin = dao.sumByPayTypeAndGmtCreate(PayTypeEnum.WEIXIN.getCode(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            // 微信小程序支付
            Integer xcx = dao.sumByPayTypeAndGmtCreate(PayTypeEnum.XCX.getCode(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            // 支付宝扫码支付
            Integer alipay = dao.sumByPayTypeAndGmtCreate(PayTypeEnum.ALIPAY.getCode(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            // 线下支付
            Integer manual = dao.sumByPayTypeAndGmtCreate(PayTypeEnum.MANUAL.getCode(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            weixinList.add(weixin);
            xcxList.add(xcx);
            alipayList.add(alipay);
            manualList.add(manual);
        }
        resp.setWeixinList(weixinList);
        resp.setXcxList(xcxList);
        resp.setAlipayList(alipayList);
        resp.setManualList(manualList);
        return Result.success(resp);

    }

    /**
     * 获取当前时间与后十天的时间
     *
     * @author kyh
     * @return
     */
    private List<String> payTime() {
        List<String> xdata = new ArrayList<>();
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(DateUtil.parseDate(DateUtil.format(DateUtil.addDate(new Date(), -9)), "yyyy-MM-dd"));
        tempStart.add(Calendar.DAY_OF_YEAR, 0);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(DateUtil.parseDate(DateUtil.format(new Date()), "yyyy-MM-dd"));
        tempEnd.add(Calendar.DAY_OF_YEAR, 1);
        while (tempStart.before(tempEnd)) {
            xdata.add(DateUtil.formatDate(tempStart.getTime()));
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return xdata;
    }
}
