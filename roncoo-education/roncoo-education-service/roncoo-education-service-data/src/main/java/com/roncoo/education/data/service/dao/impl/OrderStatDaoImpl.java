package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.OrderStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.OrderStatMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 订单统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class OrderStatDaoImpl implements OrderStatDao {

    @Autowired
    private OrderStatMapper mapper;

    @Override
    public int save(OrderStat record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderStat record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderStat getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<OrderStat> listForPage(int pageCurrent, int pageSize, OrderStatExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderStat>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public OrderStat getByGmtCreate(Date gmtCreate) {
        OrderStatExample example = new OrderStatExample();
        OrderStatExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(gmtCreate);
        List<OrderStat> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<OrderStat> listByAll() {
        OrderStatExample example = new OrderStatExample();
        return this.mapper.selectByExample(example);
    }


}
