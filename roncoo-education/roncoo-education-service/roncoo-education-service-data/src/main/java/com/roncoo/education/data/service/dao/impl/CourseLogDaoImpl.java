package com.roncoo.education.data.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.data.common.bo.CourseLogPageBO;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.CourseLogMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程日志 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Slf4j
@Repository
public class CourseLogDaoImpl extends AbstractBaseJdbc implements CourseLogDao {

    @Autowired
    private CourseLogMapper mapper;

    @Override
    public int save(CourseLog record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseLog record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseLog getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseLog> listForPage(int pageCurrent, int pageSize, CourseLogExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<CourseLog>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @SysLog
    @Override
    public CourseLog getByUserNoAndPeriodId(Long userNo, Long periodId) {
        CourseLogExample example = new CourseLogExample();
        CourseLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andPeriodIdEqualTo(periodId);
        List<CourseLog> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public BigDecimal studyLengthByUserNoAndCourseId(Long userNo, Long courseId) {
        String sql = "SELECT SUM(a.biggest_watch_length) FROM (SELECT period_id, MAX(biggest_watch_length) as biggest_watch_length FROM course_log where user_no = ? and course_id = ? group by period_id ) a";
        return jdbcTemplate.queryForObject(sql, BigDecimal.class, userNo, courseId);
    }

    @Override
    public List<CourseLog> getByPeriodId(Long periodId) {
        CourseLogExample example = new CourseLogExample();
        CourseLogExample.Criteria c = example.createCriteria();
        c.andPeriodIdEqualTo(periodId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<CourseLog> listByUserNoAndCourseId(Long userNo, Long courseId) {
        CourseLogExample example = new CourseLogExample();
        CourseLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andCourseIdEqualTo(courseId);
        example.setOrderByClause(" gmt_create desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<CourseLog> listByGmtCreate(String startgmtCreate, String endgmtCreate) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct course_id as courseId, course_name as courseName from course_log where 1=1 ");
        sql.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        sql.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        List<CourseLog> resultList = queryForObjectList(sql.toString(), CourseLog.class);
        return resultList;
    }

    @Override
    public Integer viewsByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) as count from course_log where 1");
        builder.append(" and course_id = '").append(courseId).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        Integer count = 0;
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public Integer sumsByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct user_no) as count from course_log where 1");
        builder.append(" and course_id = '").append(courseId).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        Integer count = 0;
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public String studyLengthByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(watch_length) as watchLength from course_log where 1");
        builder.append(" and course_id = '").append(courseId).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        String count = "0";
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("watchLength"))) {
            count = String.valueOf(map.get("watchLength"));
        }
        return count;
    }

    @Override
    public List<CourseLog> listUserNoByGmtCreate(String startgmtCreate, String endgmtCreate) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct user_no as userNo, mobile as mobile from course_log where 1=1 ");
        sql.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        sql.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        List<CourseLog> resultList = queryForObjectList(sql.toString(), CourseLog.class);
        return resultList;
    }

    @Override
    public String watchLengthByUserNoAndGmtCreate(Long userNo, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(watch_length) as watchLength from course_log where 1");
        builder.append(" and user_no = '").append(userNo).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        String count = "0";
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("watchLength"))) {
            count = String.valueOf(map.get("watchLength"));
        }
        return count;
    }

    @Override
    public Integer watchCourseSumsByUserNoAndGmtCreate(Long userNo, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct course_id) as count from course_log where 1");
        builder.append(" and user_no = '").append(userNo).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        Integer count = 0;
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public CourseLog getByUserNoAndPeriodIdLatest(Long userNo, Long periodId) {
        CourseLogExample example = new CourseLogExample();
        CourseLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andPeriodIdEqualTo(periodId);
        example.setOrderByClause(" id desc ");
        List<CourseLog> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<CourseLog> listByUserNoAndTimeGroupByPeriodId(Long userNo, String beginTime, String endTime) {
        StringBuilder sql = new StringBuilder();
        //sql.append("SELECT * FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '?' AND gmt_create >= '?' and gmt_create <= '?'  GROUP BY period_id )) ORDER BY id DESC");
        sql.append("SELECT * FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '").append(userNo).append("' ");
        if (StringUtils.hasText(beginTime) && StringUtils.hasText(endTime)) {
            sql.append("AND gmt_create >= '").append(beginTime.concat(" 00:00:00")).append("' ").append("AND gmt_create <= '").append(endTime.concat(" 23:59:59")).append("' ");
        }
        sql.append("GROUP BY period_id )) ORDER BY id DESC");
        List<CourseLog> resultList = queryForObjectList(sql.toString(), CourseLog.class);
        return resultList;
    }
    @Override
    public Page<CourseLog> page(Long userNo, Long courseId, String periodName, String beginTime, String endTime, int pageCurrent, int pageSize) {
        StringBuilder sql = new StringBuilder();
        //sql.append("SELECT * FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '?' AND gmt_create >= '?' and gmt_create <= '?'  GROUP BY period_id )) ORDER BY id DESC");
        sql.append("SELECT * FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '").append(userNo).append("' ");
        if (StringUtils.hasText(beginTime) && StringUtils.hasText(endTime)) {
            sql.append("AND gmt_create >= '").append(beginTime.concat(" 00:00:00")).append("' ").append("AND gmt_create <= '").append(endTime.concat(" 23:59:59")).append("' ");
        }
        if (courseId != null) {
            sql.append("AND course_id >= '").append(courseId).append("' ");
        }
        if (StringUtils.hasText(periodName)) {
            sql.append("AND period_name like '").append(PageUtil.like(periodName)).append("' ");
        }
        sql.append("GROUP BY period_id )) ORDER BY id DESC ");
        sql.append("LIMIT ").append(pageCurrent - 1).append(",").append(pageSize);
        List<CourseLog> list = queryForObjectList(sql.toString(), CourseLog.class);
        Integer totalCount = pageTotal(userNo, courseId, beginTime, endTime);
        int size = PageUtil.checkPageSize(pageSize);
        int current = SqlUtil.checkPageCurrent(totalCount, pageSize, pageCurrent);
        int totalPage = SqlUtil.countTotalPage(totalCount, pageSize);
        Page<CourseLog> page = new Page<>(totalCount, totalPage, current, size, list);
        return page;
    }

    @Override
    public String getByUserNoAndPeriodIdAndBiggestWatckProgress(Long userNo, Long periodId) {
        StringBuilder builder = new StringBuilder();
        builder.append("select MAX(CAST(watck_progress as decimal(15,5))) As watckProgress FROM course_log where 1");
        if (userNo != null) {
            builder.append(" and user_no = '").append(userNo).append("'");
        }
        if (periodId != null) {
            builder.append(" and period_id = '").append(periodId).append("'");
        }
        String count = "0";
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("watckProgress"))) {
            count = String.valueOf(map.get("watckProgress"));
        }
        return count;
    }


    private Integer pageTotal(Long userNo, Long courseId, String beginTime, String endTime) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT count(*) as count FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '").append(userNo).append("' ");
        if (StringUtils.hasText(beginTime) && StringUtils.hasText(endTime)) {
            sql.append("AND gmt_create >= '").append(beginTime.concat(" 00:00:00")).append("' ").append("AND gmt_create <= '").append(endTime.concat(" 23:59:59")).append("' ");
        }
        if (courseId != null) {
            sql.append("AND course_id >= '").append(courseId).append("' ");
        }
        sql.append("GROUP BY period_id )) ORDER BY id DESC ");
        Integer count = 0;
        Map<String, Object> map = jdbcTemplate.queryForMap(sql.toString());
        if (!StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public List<CourseLog> listByUserNoAndCourseIdByPeriodId(Long userNo, Long courseId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM course_log WHERE (id IN ( SELECT MAX(id) FROM course_log WHERE user_no = '").append(userNo).append("' ");
        sql.append(" and course_id = '").append(courseId).append("'");
        sql.append("GROUP BY period_id )) ORDER BY id DESC");
        List<CourseLog> resultList = queryForObjectList(sql.toString(), CourseLog.class);
        return resultList;
    }


    @Override
    public List<CourseLog> listAll() {
        CourseLogExample example = new CourseLogExample();
        CourseLogExample.Criteria c = example.createCriteria();
        example.setOrderByClause(" gmt_create desc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public BigDecimal getByUserNoAndPeriodIdAndBiggestWatchLength(Long userNo, Long periodId) {
        StringBuilder builder = new StringBuilder();
        builder.append("select MAX(biggest_watch_length) As biggestWatchLength FROM course_log where 1");
        if (userNo != null) {
            builder.append(" and user_no = '").append(userNo).append("'");
        }
        if (periodId != null) {
            builder.append(" and period_id = '").append(periodId).append("'");
        }
        BigDecimal count = BigDecimal.ZERO;
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("biggestWatchLength"))) {
            count = new BigDecimal(String.valueOf(map.get("biggestWatchLength")));
        }
        return count;
    }

}
