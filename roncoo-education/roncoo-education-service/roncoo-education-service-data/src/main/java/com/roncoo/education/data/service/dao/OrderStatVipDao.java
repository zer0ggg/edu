package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVip;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVipExample;

import java.util.Date;
import java.util.List;

/**
 * 订单会员统计 服务类
 *
 * @author wujing
 * @date 2020-05-25
 */
public interface OrderStatVipDao {

    int save(OrderStatVip record);

    int deleteById(Long id);

    int updateById(OrderStatVip record);

    OrderStatVip getById(Long id);

    Page<OrderStatVip> listForPage(int pageCurrent, int pageSize, OrderStatVipExample example);

    /**
     * 查找当天会员订单统计信息
     *
     * @param gmtCreate
     * @return
     */
    OrderStatVip getByGmtCreate(Date gmtCreate);

    /**
     * 查找会员订单统计信息
     *
     * @return
     */
    List<OrderStatVip> listByAll();
}
