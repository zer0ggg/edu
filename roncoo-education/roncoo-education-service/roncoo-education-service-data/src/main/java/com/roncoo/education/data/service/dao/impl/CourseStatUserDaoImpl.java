package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.CourseStatUserDao;
import com.roncoo.education.data.service.dao.impl.mapper.CourseStatUserMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 课程用户统计 服务实现类
 *
 * @author wujing
 * @date 2020-06-29
 */
@Repository
public class CourseStatUserDaoImpl implements CourseStatUserDao {

    @Autowired
    private CourseStatUserMapper mapper;

    @Override
    public int save(CourseStatUser record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseStatUser record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseStatUser getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseStatUser> listForPage(int pageCurrent, int pageSize, CourseStatUserExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public CourseStatUser getByByUserNoAndGmtCreate(Long userNo, Date gmtCreate) {
        CourseStatUserExample example = new CourseStatUserExample();
        CourseStatUserExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andGmtCreateEqualTo(gmtCreate);
        List<CourseStatUser> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }


}
