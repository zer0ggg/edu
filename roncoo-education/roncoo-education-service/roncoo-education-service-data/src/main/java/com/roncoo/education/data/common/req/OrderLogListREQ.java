package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderLogListREQ", description="订单日志列表")
public class OrderLogListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户电话")
    private String mobile;

    @ApiModelProperty(value = "订单号")
    private Long orderNo;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;

    @ApiModelProperty(value = "附加数据")
    private String attach;

    @ApiModelProperty(value = "应付金额")
    private BigDecimal pricePayable;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal priceDiscount;

    @ApiModelProperty(value = "实付金额")
    private BigDecimal pricePaid;

    @ApiModelProperty(value = "平台收入")
    private BigDecimal platformIncome;

    @ApiModelProperty(value = "讲师收入")
    private BigDecimal lecturerIncome;

    @ApiModelProperty(value = "代理收入")
    private BigDecimal agentIncome;

    @ApiModelProperty(value = "交易类型：1线上支付，2线下支付")
    private Integer tradeType;

    @ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
    private Integer payType;

    @ApiModelProperty(value = "购买渠道：（1: PC端；2: APP端；3:微信端； 4:手工绑定）")
    private Integer channelType;

    @ApiModelProperty(value = "订单状态：1待支付，2成功支付，3支付失败，4已关闭")
    private Integer orderStatus;

    @ApiModelProperty(value = "是否显示给讲师(1是，0否)")
    private Integer isShowLecturer;

    @ApiModelProperty(value = "是否显示给用户看(1是，0否)")
    private Integer isShowUser;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
