package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.SysLogListREQ;
import com.roncoo.education.data.common.resp.SysLogListRESP;
import com.roncoo.education.data.service.pc.biz.PcSysLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 后台操作日志表 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/sys/log")
@Api(value = "data-后台操作日志表", tags = {"data-后台操作日志表"})
public class PcSysLogController {

    @Autowired
    private PcSysLogBiz biz;

    @ApiOperation(value = "后台操作日志列表", notes = "后台操作日志列表")
    @PostMapping(value = "/list")
    public Result<Page<SysLogListRESP>> list(@RequestBody SysLogListREQ sysLogListREQ) {
        return biz.list(sysLogListREQ);
    }

}
