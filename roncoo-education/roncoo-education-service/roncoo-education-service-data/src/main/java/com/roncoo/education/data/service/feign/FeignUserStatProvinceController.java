package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignUserStatProvince;
import com.roncoo.education.data.feign.qo.UserStatProvinceQO;
import com.roncoo.education.data.feign.vo.UserStatProvinceVO;
import com.roncoo.education.data.service.feign.biz.FeignUserStatProvinceBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户省统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignUserStatProvinceController extends BaseController implements IFeignUserStatProvince{

    @Autowired
    private FeignUserStatProvinceBiz biz;

	@Override
	public Page<UserStatProvinceVO> listForPage(@RequestBody UserStatProvinceQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserStatProvinceQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserStatProvinceQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserStatProvinceVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
