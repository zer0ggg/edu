// package com.roncoo.education.data.common;
//
// import com.roncoo.education.common.core.base.BaseController;
// import com.roncoo.education.common.core.base.BaseException;
// import com.roncoo.education.common.core.base.Result;
// import feign.FeignException;
// import feign.FeignException.FeignServerException;
// import org.springframework.http.HttpStatus;
// import org.springframework.web.bind.annotation.ExceptionHandler;
// import org.springframework.web.bind.annotation.ResponseStatus;
// import org.springframework.web.bind.annotation.RestControllerAdvice;
//
/// ** @author wujing */
// @RestControllerAdvice(basePackages = {"com.roncoo.education.data.service.feign"})
// public class FeignHandlerException extends BaseController {
//
//  @ExceptionHandler({BaseException.class})
//  @ResponseStatus(HttpStatus.OK)
//  public FeignException processException(BaseException e) {
//    logger.error("BaseException", e);
//    return new FeignServerException(200, e.getMessage(), null, e.getMessage().getBytes());
//  }
//
//  @ExceptionHandler({Exception.class})
//  @ResponseStatus(HttpStatus.OK)
//  public Result<String> processException(Exception e) {
//    logger.error("Exception", e);
//    return Result.error("服务繁忙，请重试");
//  }
// }
