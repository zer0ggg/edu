package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.UserStatProvinceDao;
import com.roncoo.education.data.service.dao.impl.mapper.UserStatProvinceMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvince;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvinceExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 用户省统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class UserStatProvinceDaoImpl implements UserStatProvinceDao {

    @Autowired
    private UserStatProvinceMapper mapper;

    @Override
    public int save(UserStatProvince record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserStatProvince record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserStatProvince getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<UserStatProvince> listForPage(int pageCurrent, int pageSize, UserStatProvinceExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserStatProvince>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserStatProvince getByGmtCreateAndProvince(Date gmtCreate, String province) {
        UserStatProvinceExample example = new UserStatProvinceExample();
        UserStatProvinceExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(gmtCreate);
        c.andProvinceEqualTo(province);
        List<UserStatProvince> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<UserStatProvince> listByGmtCreate(Date gmtCreate) {
        UserStatProvinceExample example = new UserStatProvinceExample();
        UserStatProvinceExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(gmtCreate);
        return this.mapper.selectByExample(example);
    }
}
