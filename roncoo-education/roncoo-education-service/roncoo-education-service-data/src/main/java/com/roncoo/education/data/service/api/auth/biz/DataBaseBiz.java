package com.roncoo.education.data.service.api.auth.biz;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBaseBiz extends BaseBiz {

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public UserExtVO getUserExtVO() {
        UserExtVO result = myRedisTemplate.getForJson(RedisPreEnum.USER_EXT.getCode() + ThreadContext.userNo(), UserExtVO.class);
        if (null != result) {
            return result;
        }
        return feignUserExt.getByUserNoForCache(ThreadContext.userNo());
    }

}
