package com.roncoo.education.data.service.api.auth;

import com.itextpdf.text.DocumentException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.bo.AuthCourseLogExportBO;
import com.roncoo.education.data.common.bo.CourseLogPageBO;
import com.roncoo.education.data.common.bo.CourseLogProgressBO;
import com.roncoo.education.data.common.bo.CourseLogSaveBO;
import com.roncoo.education.data.common.dto.CourseLogPageDTO;
import com.roncoo.education.data.common.dto.CourseLogProgressDTO;
import com.roncoo.education.data.service.api.auth.biz.AuthCourseLogBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

/**
 * 课程日志 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/data/auth/course/log")
public class AuthCourseLogController {

    @Autowired
    private AuthCourseLogBiz biz;

    @ApiOperation(value = "课程日志添加", notes = "课程日志添加")
    @PostMapping(value = "/save")
    public Result<Integer> save(@RequestBody @Valid CourseLogSaveBO courseLogSaveBO) {
        return biz.save(courseLogSaveBO);
    }

    @ApiOperation(value = "用户列出学习记录", notes = "用户列出学习记录")
    @PostMapping(value = "/list")
    public Result<Page<CourseLogPageDTO>> list(@RequestBody CourseLogPageBO courseLogPageBO) {
        return biz.list(courseLogPageBO);
    }

    /**
     * 学习记录导出
     *
     * @param response
     * @param bo
     * @throws IOException
     */
    @ApiOperation(value = "学习记录导出", notes = "学习记录导出")
    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public void export(@Valid AuthCourseLogExportBO bo, HttpServletResponse response) throws IOException, DocumentException {
        biz.export(response, bo);
    }

    @ApiOperation(value = "用户课时学习进度集合", notes = "用户课时进度集合")
    @PostMapping(value = "/course/progress")
    public Result<CourseLogProgressDTO> periodProgressList(@RequestBody CourseLogProgressBO courseLogProgressBO) {
        return biz.periodProgressList(courseLogProgressBO);
    }

}
