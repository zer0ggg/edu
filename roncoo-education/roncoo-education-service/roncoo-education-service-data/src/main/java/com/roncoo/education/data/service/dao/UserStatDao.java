package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatExample;

import java.util.Date;
import java.util.List;

/**
 * 用户统计 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface UserStatDao {

    int save(UserStat record);

    int deleteById(Long id);

    int updateById(UserStat record);

    UserStat getById(Long id);

    Page<UserStat> listForPage(int pageCurrent, int pageSize, UserStatExample example);

    /**
     * 获取当天时间
     *
     * @param date
     * @return
     */
    UserStat getByGmtCreate(Date date);

    /**
     * 列出用户信息統計
     * @return
     */
    List<UserStat> listByAll();
}
