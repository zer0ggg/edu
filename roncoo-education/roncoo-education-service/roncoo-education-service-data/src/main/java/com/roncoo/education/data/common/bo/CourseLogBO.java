package com.roncoo.education.data.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 课程日志
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseLogBO", description = "课程日志")
public class CourseLogBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当次开始学习时间")
    private String startStudyData;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "课时ID")
    private Long periodId;

    @ApiModelProperty(value = "观看时长")
    private BigDecimal watchLength;

    @ApiModelProperty(value = "观看结束时间点（当次历史最大时间点)")
    private BigDecimal biggestWatchLength;

    @ApiModelProperty(value = "时间")
    private String data;
}
