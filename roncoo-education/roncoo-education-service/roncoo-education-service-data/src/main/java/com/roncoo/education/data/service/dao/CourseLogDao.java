package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.common.bo.CourseLogPageBO;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample;

import java.math.BigDecimal;
import java.util.List;

/**
 * 课程日志 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface CourseLogDao {

    int save(CourseLog record);

    int deleteById(Long id);

    int updateById(CourseLog record);

    CourseLog getById(Long id);

    Page<CourseLog> listForPage(int pageCurrent, int pageSize, CourseLogExample example);

    CourseLog getByUserNoAndPeriodId(Long userNo, Long periodId);

    /**
     * 汇总学习时长
     *
     * @param userNo
     * @param courseId
     * @return
     */
    BigDecimal studyLengthByUserNoAndCourseId(Long userNo, Long courseId);

    /**
     * 根据课时编号获取学习记录
     *
     * @param periodId
     * @return
     */
    List<CourseLog> getByPeriodId(Long periodId);

    /**
     * 根据用户编号、课程ID获取学习记录
     *
     * @param userNo
     * @param courseId
     * @return
     */
    List<CourseLog> listByUserNoAndCourseId(Long userNo, Long courseId);

    /**
     * 获取当天课程学习记录（去重）
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    List<CourseLog> listByGmtCreate(String startgmtCreate, String endgmtCreate);

    /**
     * 根据课程ID汇总当天课程学习观看人次
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param courseId
     * @return
     */
    Integer viewsByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate);

    /**
     * 根据课程ID获取当天课程学习观看人数(去重)
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param courseId
     * @return
     */
    Integer sumsByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate);

    /**
     * 根据课程ID获取当天课程学习学习时长(秒)
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param courseId
     * @return
     */
    String studyLengthByCourseIdAndGmtCreate(Long courseId, String startgmtCreate, String endgmtCreate);

    /**
     * 列出当天学习用户，去重
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    List<CourseLog> listUserNoByGmtCreate(String startgmtCreate, String endgmtCreate);

    /**
     * 根据用户编号获取当天课程学习学习时长(秒)
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param userNo
     * @return
     */
    String watchLengthByUserNoAndGmtCreate(Long userNo, String startgmtCreate, String endgmtCreate);

    /**
     * 根据用户编号获取当天学习课程数
     *
     * @param startgmtCreate
     * @param endgmtCreate
     * @param userNo
     * @return
     */
    Integer watchCourseSumsByUserNoAndGmtCreate(Long userNo, String startgmtCreate, String endgmtCreate);

    /**
     * 根据用户编号、课时id获取最新的学习记录
     *
     * @param userNo
     * @param periodId
     * @return
     */
    CourseLog getByUserNoAndPeriodIdLatest(Long userNo, Long periodId);

    /**
     * 根据用户编号，时间段查询，课时id去重的最新一条数据
     *
     * @param userNo
     * @param beginTime
     * @param endTime
     * @return
     */
    List<CourseLog> listByUserNoAndTimeGroupByPeriodId(Long userNo, String beginTime, String endTime);

    /**
     * 用户学习记录分页查询--课时id去重
     *
     * @return
     */
    Page<CourseLog> page(Long userNo,Long courseId, String periodName, String beginTime, String endTime, int pageCurrent, int pageSize);

    /**
     * 获取最大学习进度
     *
     * @param userNo
     * @param periodId
     * @return
     */
    String getByUserNoAndPeriodIdAndBiggestWatckProgress(Long userNo, Long periodId);

    /**
     * 根据用户编号, 课时id去重的最新一条数据
     *
     * @param userNo
     * @param courseId
     * @return
     */
    List<CourseLog> listByUserNoAndCourseIdByPeriodId(Long userNo, Long courseId);

    /**
     * 列出当天学习用户，去重
     *
     * @return
     */
    List<CourseLog> listAll();

    /**
     * 获取观看结束时间点（当次历史最大时间点)
     *
     * @param userNo
     * @param periodId
     * @return
     */
    BigDecimal getByUserNoAndPeriodIdAndBiggestWatchLength(Long userNo, Long periodId);
}
