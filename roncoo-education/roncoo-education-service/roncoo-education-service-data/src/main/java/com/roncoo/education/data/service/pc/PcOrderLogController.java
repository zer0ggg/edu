package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.data.common.req.OrderLogListREQ;
import com.roncoo.education.data.common.resp.OrderLogDaysStatRESP;
import com.roncoo.education.data.common.resp.OrderLogListRESP;
import com.roncoo.education.data.service.pc.biz.PcOrderLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/order/log")
@Api(value = "data-订单日志", tags = {"data-订单日志"})
public class PcOrderLogController {

    @Autowired
    private PcOrderLogBiz biz;

    @ApiOperation(value = "订单日志列表", notes = "订单日志列表")
    @PostMapping(value = "/list")
    @SysLog(value = "订单日志列表")
    public Result<Page<OrderLogListRESP>> list(@RequestBody OrderLogListREQ orderLogListREQ) {
        return biz.list(orderLogListREQ);
    }

    @ApiOperation(value = "获取当前时间与后十天的时间", notes = "获取当前时间与后十天的时间")
    @PostMapping(value = "/days/stat")
    public Result<OrderLogDaysStatRESP> daysStat() {
        return biz.daysStat();
    }
}
