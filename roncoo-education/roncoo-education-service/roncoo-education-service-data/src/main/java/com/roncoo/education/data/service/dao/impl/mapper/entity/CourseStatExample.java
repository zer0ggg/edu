package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class CourseStatExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public CourseStatExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterionForJDBCDate("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterionForJDBCDate("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterionForJDBCDate("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterionForJDBCDate("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterionForJDBCDate("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterionForJDBCDate("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNull() {
            addCriterion("course_name is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameIsNotNull() {
            addCriterion("course_name is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEqualTo(String value) {
            addCriterion("course_name =", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotEqualTo(String value) {
            addCriterion("course_name <>", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThan(String value) {
            addCriterion("course_name >", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameGreaterThanOrEqualTo(String value) {
            addCriterion("course_name >=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThan(String value) {
            addCriterion("course_name <", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLessThanOrEqualTo(String value) {
            addCriterion("course_name <=", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameLike(String value) {
            addCriterion("course_name like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotLike(String value) {
            addCriterion("course_name not like", value, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameIn(List<String> values) {
            addCriterion("course_name in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotIn(List<String> values) {
            addCriterion("course_name not in", values, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameBetween(String value1, String value2) {
            addCriterion("course_name between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseNameNotBetween(String value1, String value2) {
            addCriterion("course_name not between", value1, value2, "courseName");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNull() {
            addCriterion("course_category is null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNotNull() {
            addCriterion("course_category is not null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryEqualTo(Integer value) {
            addCriterion("course_category =", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotEqualTo(Integer value) {
            addCriterion("course_category <>", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThan(Integer value) {
            addCriterion("course_category >", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_category >=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThan(Integer value) {
            addCriterion("course_category <", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThanOrEqualTo(Integer value) {
            addCriterion("course_category <=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIn(List<Integer> values) {
            addCriterion("course_category in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotIn(List<Integer> values) {
            addCriterion("course_category not in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryBetween(Integer value1, Integer value2) {
            addCriterion("course_category between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotBetween(Integer value1, Integer value2) {
            addCriterion("course_category not between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andViewsIsNull() {
            addCriterion("views is null");
            return (Criteria) this;
        }

        public Criteria andViewsIsNotNull() {
            addCriterion("views is not null");
            return (Criteria) this;
        }

        public Criteria andViewsEqualTo(Integer value) {
            addCriterion("views =", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsNotEqualTo(Integer value) {
            addCriterion("views <>", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsGreaterThan(Integer value) {
            addCriterion("views >", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsGreaterThanOrEqualTo(Integer value) {
            addCriterion("views >=", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsLessThan(Integer value) {
            addCriterion("views <", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsLessThanOrEqualTo(Integer value) {
            addCriterion("views <=", value, "views");
            return (Criteria) this;
        }

        public Criteria andViewsIn(List<Integer> values) {
            addCriterion("views in", values, "views");
            return (Criteria) this;
        }

        public Criteria andViewsNotIn(List<Integer> values) {
            addCriterion("views not in", values, "views");
            return (Criteria) this;
        }

        public Criteria andViewsBetween(Integer value1, Integer value2) {
            addCriterion("views between", value1, value2, "views");
            return (Criteria) this;
        }

        public Criteria andViewsNotBetween(Integer value1, Integer value2) {
            addCriterion("views not between", value1, value2, "views");
            return (Criteria) this;
        }

        public Criteria andSumsIsNull() {
            addCriterion("sums is null");
            return (Criteria) this;
        }

        public Criteria andSumsIsNotNull() {
            addCriterion("sums is not null");
            return (Criteria) this;
        }

        public Criteria andSumsEqualTo(Integer value) {
            addCriterion("sums =", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsNotEqualTo(Integer value) {
            addCriterion("sums <>", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsGreaterThan(Integer value) {
            addCriterion("sums >", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsGreaterThanOrEqualTo(Integer value) {
            addCriterion("sums >=", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsLessThan(Integer value) {
            addCriterion("sums <", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsLessThanOrEqualTo(Integer value) {
            addCriterion("sums <=", value, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsIn(List<Integer> values) {
            addCriterion("sums in", values, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsNotIn(List<Integer> values) {
            addCriterion("sums not in", values, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsBetween(Integer value1, Integer value2) {
            addCriterion("sums between", value1, value2, "sums");
            return (Criteria) this;
        }

        public Criteria andSumsNotBetween(Integer value1, Integer value2) {
            addCriterion("sums not between", value1, value2, "sums");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIsNull() {
            addCriterion("course_length is null");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIsNotNull() {
            addCriterion("course_length is not null");
            return (Criteria) this;
        }

        public Criteria andCourseLengthEqualTo(String value) {
            addCriterion("course_length =", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotEqualTo(String value) {
            addCriterion("course_length <>", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthGreaterThan(String value) {
            addCriterion("course_length >", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthGreaterThanOrEqualTo(String value) {
            addCriterion("course_length >=", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLessThan(String value) {
            addCriterion("course_length <", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLessThanOrEqualTo(String value) {
            addCriterion("course_length <=", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLike(String value) {
            addCriterion("course_length like", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotLike(String value) {
            addCriterion("course_length not like", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIn(List<String> values) {
            addCriterion("course_length in", values, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotIn(List<String> values) {
            addCriterion("course_length not in", values, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthBetween(String value1, String value2) {
            addCriterion("course_length between", value1, value2, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotBetween(String value1, String value2) {
            addCriterion("course_length not between", value1, value2, "courseLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIsNull() {
            addCriterion("study_length is null");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIsNotNull() {
            addCriterion("study_length is not null");
            return (Criteria) this;
        }

        public Criteria andStudyLengthEqualTo(String value) {
            addCriterion("study_length =", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotEqualTo(String value) {
            addCriterion("study_length <>", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthGreaterThan(String value) {
            addCriterion("study_length >", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthGreaterThanOrEqualTo(String value) {
            addCriterion("study_length >=", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthLessThan(String value) {
            addCriterion("study_length <", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthLessThanOrEqualTo(String value) {
            addCriterion("study_length <=", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthLike(String value) {
            addCriterion("study_length like", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotLike(String value) {
            addCriterion("study_length not like", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIn(List<String> values) {
            addCriterion("study_length in", values, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotIn(List<String> values) {
            addCriterion("study_length not in", values, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthBetween(String value1, String value2) {
            addCriterion("study_length between", value1, value2, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotBetween(String value1, String value2) {
            addCriterion("study_length not between", value1, value2, "studyLength");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}