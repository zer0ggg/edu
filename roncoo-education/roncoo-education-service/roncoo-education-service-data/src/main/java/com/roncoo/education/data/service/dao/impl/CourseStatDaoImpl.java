package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.CourseStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.CourseStatMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 课程统计 服务实现类
 *
 * @author wujing
 * @date 2020-06-29
 */
@Repository
public class CourseStatDaoImpl implements CourseStatDao {

    @Autowired
    private CourseStatMapper mapper;

    @Override
    public int save(CourseStat record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseStat record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseStat getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseStat> listForPage(int pageCurrent, int pageSize, CourseStatExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public CourseStat getByByCourseIdAndGmtCreate(Long courseId, Date gmtCreate) {
        CourseStatExample example = new CourseStatExample();
        CourseStatExample.Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(courseId);
        c.andGmtCreateEqualTo(gmtCreate);
        List<CourseStat> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }


}
