package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatListRESP", description="订单统计列表")
public class OrderStatListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "当天订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "当天收入")
    private BigDecimal orderIncome;

    @ApiModelProperty(value = "当天利润")
    private BigDecimal orderProfit;

    @ApiModelProperty(value = "当天购买用户")
    private Integer buyUser;

    @ApiModelProperty(value = "当天购买新用户")
    private Integer buyNewUser;
}
