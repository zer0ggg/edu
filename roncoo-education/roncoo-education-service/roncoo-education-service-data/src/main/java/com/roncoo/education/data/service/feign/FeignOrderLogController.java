package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.OrderLogVO;
import com.roncoo.education.data.service.feign.biz.FeignOrderLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignOrderLogController extends BaseController implements IFeignOrderLog {

  @Autowired private FeignOrderLogBiz biz;

  @Override
  public Page<OrderLogVO> listForPage(@RequestBody OrderLogQO qo) {
    return biz.listForPage(qo);
  }

  @Override
  public int save(@RequestBody OrderLogQO qo) {
    return biz.save(qo);
  }

  @Override
  public int deleteById(@PathVariable(value = "id") Long id) {
    return biz.deleteById(id);
  }

  @Override
  public int updateById(@RequestBody OrderLogQO qo) {
    return biz.updateById(qo);
  }

  @Override
  public OrderLogVO getById(@PathVariable(value = "id") Long id) {
    return biz.getById(id);
  }

  @Override
  public OrderLogVO getByOrderNo(@PathVariable(value = "orderNo") Long orderNo) {
    return biz.getByOrderNo(orderNo);
  }

  @Override
  public int updateByOrderNo(@RequestBody OrderLogQO qo) {
    return biz.updateByOrderNo(qo);
  }

  @Override
  public int updateByMobile(@RequestBody OrderLogQO orderLogQO) {
    return biz.updateByMobile(orderLogQO);
  }
}
