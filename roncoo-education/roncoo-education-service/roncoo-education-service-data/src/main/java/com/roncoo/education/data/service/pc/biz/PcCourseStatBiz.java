package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.CourseStatListREQ;
import com.roncoo.education.data.common.resp.CourseStatListRESP;
import com.roncoo.education.data.service.dao.CourseStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 课程统计
 *
 * @author wujing
 */
@Component
public class PcCourseStatBiz extends BaseBiz {

    @Autowired
    private CourseStatDao dao;

    /**
     * 课程统计列表
     *
     * @param req 课程统计分页查询参数
     * @return 课程统计分页查询结果
     */
    public Result<Page<CourseStatListRESP>> list(CourseStatListREQ req) {
        CourseStatExample example = new CourseStatExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(req.getCourseName()));
        }
        example.setOrderByClause(" id desc ");
        Page<CourseStat> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseStatListRESP> respPage = PageUtil.transform(page, CourseStatListRESP.class);
        for (CourseStatListRESP resp : respPage.getList()) {
            if (StringUtils.hasText(resp.getStudyLength()) && resp.getStudyLength().contains(".")) {
                int indexOf = resp.getStudyLength().indexOf(".");
                resp.setStudyLength(cn.hutool.core.date.DateUtil.secondToTime(Integer.parseInt(resp.getStudyLength().substring(0, indexOf))));
            }
        }
        return Result.success(respPage);
    }

}
