package com.roncoo.education.data.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.dto.CourseLogPageDTO;
import com.roncoo.education.data.common.req.CourseLogListREQ;
import com.roncoo.education.data.common.req.CourseLogPeriodPageREQ;
import com.roncoo.education.data.common.resp.CourseLogListRESP;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 课程日志
 *
 * @author wujing
 */
@Component
public class PcCourseLogBiz extends BaseBiz {

    @Autowired
    private CourseLogDao dao;

    /**
     * 课程日志列表
     *
     * @param courseLogListREQ 课程日志分页查询参数
     * @return 课程日志分页查询结果
     */
    public Result<Page<CourseLogListRESP>> list(CourseLogListREQ courseLogListREQ) {
        CourseLogExample example = new CourseLogExample();
        Criteria c = example.createCriteria();
        if (courseLogListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(courseLogListREQ.getUserNo());
        }
        if (StringUtils.hasText(courseLogListREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(courseLogListREQ.getMobile()));
        }
        if (StringUtils.hasText(courseLogListREQ.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(courseLogListREQ.getCourseName()));
        }
        if (StringUtils.hasText(courseLogListREQ.getChapterName())) {
            c.andChapterNameLike(PageUtil.like(courseLogListREQ.getChapterName()));
        }
        if (StringUtils.hasText(courseLogListREQ.getPeriodName())) {
            c.andPeriodNameLike(PageUtil.like(courseLogListREQ.getPeriodName()));
        }
        example.setOrderByClause(" id desc ");
        Page<CourseLog> page = dao.listForPage(courseLogListREQ.getPageCurrent(), courseLogListREQ.getPageSize(), example);
        Page<CourseLogListRESP> respPage = PageUtil.transform(page, CourseLogListRESP.class);
        for (CourseLogListRESP resp : respPage.getList()) {
            if (StringUtils.isEmpty(resp.getWatckProgress())) {
                resp.setWatckProgress(0);
            }
            if (resp.getBiggestWatchLength() != null) {
                resp.setStringBiggestWatchLength(cn.hutool.core.date.DateUtil.secondToTime(resp.getBiggestWatchLength().intValue()));
            } else {
                resp.setStringBiggestWatchLength("00:00:00");
            }
        }
        return Result.success(respPage);
    }

    public Result<Page<CourseLogListRESP>> periodList(CourseLogPeriodPageREQ req) {
        Page<CourseLog> page = dao.page(req.getUserNo(), req.getCourseId(), req.getPeriodName(), null, null, req.getPageCurrent(), req.getPageSize());
        Page<CourseLogListRESP> respPage = PageUtil.transform(page, CourseLogListRESP.class);
        for (CourseLogListRESP resp : respPage.getList()) {
            CourseLog courseLog = dao.getByUserNoAndPeriodIdLatest(resp.getUserNo(), resp.getPeriodId());
            if (ObjectUtil.isNotNull(courseLog) && courseLog.getGmtCreate() != null) {
                resp.setGmtCreate(courseLog.getGmtCreate());
            }
            if (resp.getBiggestWatchLength() != null) {
                resp.setStringBiggestWatchLength(cn.hutool.core.date.DateUtil.secondToTime(resp.getBiggestWatchLength().intValue()));
            } else {
                resp.setStringBiggestWatchLength("00:00:00");
            }
        }
        return Result.success(respPage);
    }
}
