package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.OrderLogMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单日志 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class OrderLogDaoImpl extends AbstractBaseJdbc implements OrderLogDao {

    @Autowired
    private OrderLogMapper mapper;

    @Override
    public int save(OrderLog record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderLog record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderLog getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<OrderLog> listForPage(int pageCurrent, int pageSize, OrderLogExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderLog>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


    @Override
    public List<OrderLog> listByGmtCreateAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Integer isShowLecturer) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andIsShowLecturerEqualTo(isShowLecturer);
        c.andOrderStatusEqualTo(OrderStatusEnum.SUCCESS.getCode());
        if (startgmtCreate != null) {
            c.andGmtCreateGreaterThanOrEqualTo(startgmtCreate);
        }
        if (endgmtCreate != null) {
            c.andGmtCreateLessThanOrEqualTo(endgmtCreate);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<OrderLog> listByGmtCreateAndProductIdAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Long productId, Integer IsShowLecturer) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andProductIdEqualTo(productId);
        c.andIsShowLecturerEqualTo(IsShowLecturer);
        c.andOrderStatusEqualTo(OrderStatusEnum.SUCCESS.getCode());
        c.andGmtCreateGreaterThanOrEqualTo(startgmtCreate);
        c.andGmtCreateLessThanOrEqualTo(endgmtCreate);
        return this.mapper.selectByExample(example);
    }

    @Override
    public OrderLog orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Long productId, Integer isShowLecturer) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(price_paid) as pricePaid, sum(platform_income) as platformIncome from order_log where 1");
        if (startgmtCreate != null) {
            builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        }
        if (endgmtCreate != null) {
            builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        }
        if (productId != 0L) {
            builder.append(" and product_id = '").append(productId).append("'");
        }
        builder.append(" and order_status = '").append(OrderStatusEnum.SUCCESS.getCode()).append("'");
        builder.append(" and is_show_lecturer = '").append(isShowLecturer).append("'");
        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        OrderLog orderLog = new OrderLog();
        BigDecimal pricePaid = BigDecimal.valueOf(0);
        BigDecimal platformIncome = BigDecimal.valueOf(0);
        if (map != null && !StringUtils.isEmpty(map.get("pricePaid"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("pricePaid")));
            pricePaid = bd.setScale(2, RoundingMode.DOWN);
        }
        if (map != null && !StringUtils.isEmpty(map.get("platformIncome"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("platformIncome")));
            platformIncome = bd.setScale(2, RoundingMode.DOWN);
        }
        orderLog.setPricePaid(pricePaid);
        orderLog.setPlatformIncome(platformIncome);
        return orderLog;
    }

    @Override
    public Integer buyUserByGmtCreateAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer isShowLecturer) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct user_no) as count from order_log where 1 ");
        if (startgmtCreate != null) {
            builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        }
        if (endgmtCreate != null) {
            builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        }
        builder.append(" and is_show_lecturer = '").append(isShowLecturer).append("'");
        String sql = builder.toString();
        Integer count = 0;
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public List<OrderLog> listByGmtCreateAndNotProductTypeAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer productType, Integer isShowLecturer) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct product_id, product_name, product_type from order_log where 1=1 ");
        sql.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        sql.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        sql.append(" and product_type != '").append(productType).append("'");
        sql.append(" and is_show_lecturer = '").append(isShowLecturer).append("'");
        List<OrderLog> resultList = jdbcTemplate.query(sql.toString(), new RowMapper<OrderLog>() {
            @Override
            public OrderLog mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                OrderLog orderLog = new OrderLog();
                orderLog.setProductId(resultSet.getLong("product_id"));
                orderLog.setProductName(resultSet.getString("product_name"));
                orderLog.setProductType(resultSet.getInt("product_type"));
                return orderLog;
            }
        });
        return resultList;
    }

    @Override

    public List<OrderLog> listByGmtCreateAndGroupByLecturerUserNoAndProductTypeAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Integer productType, Integer isShowLecturer) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct(lecturer_user_no), lecturer_name from order_log where 1=1");
        sql.append(" and product_type != '").append(productType).append("'");
        sql.append(" and is_show_lecturer = '").append(isShowLecturer).append("'");
        sql.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        sql.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        List<OrderLog> resultList = jdbcTemplate.query(sql.toString(), new RowMapper<OrderLog>() {
            @Override
            public OrderLog mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                OrderLog orderLog = new OrderLog();
                orderLog.setLecturerUserNo(resultSet.getLong("lecturer_user_no"));
                orderLog.setLecturerName(resultSet.getString("lecturer_name"));
                return orderLog;
            }
        });
        return resultList;
    }

    @Override
    public List<OrderLog> listByGmtCreateAndLecturerUserNoAndIsShowLecturer(Date startgmtCreate, Date endgmtCreate, Long lecturerUserNo, Integer isShowLecturer) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(lecturerUserNo);
        c.andIsShowLecturerEqualTo(isShowLecturer);
        c.andOrderStatusEqualTo(OrderStatusEnum.SUCCESS.getCode());
        c.andGmtCreateGreaterThanOrEqualTo(startgmtCreate);
        c.andGmtCreateLessThanOrEqualTo(endgmtCreate);
        return this.mapper.selectByExample(example);
    }

    @Override
    public BigDecimal lecturerIncomeByGmtCreateAndLecturerUserNoAndIsShowLecturer(String startgmtCreate, String endgmtCreate, Long lecturerUserNo, Integer isShowLecturer) {
        StringBuilder builder = new StringBuilder();
        builder.append("select distinct(lecturer_user_no), sum(lecturer_income) lecturerIncome from order_log where");
        builder.append(" lecturer_user_no = '").append(lecturerUserNo).append("'");
        builder.append(" and order_status = '").append(OrderStatusEnum.SUCCESS.getCode()).append("'");
        builder.append(" and is_show_lecturer = '").append(isShowLecturer).append("'");
        builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");

        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        BigDecimal lecturerIncome = BigDecimal.valueOf(0);
        if (!StringUtils.isEmpty(map.get("lecturerIncome"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("lecturerIncome")));
            lecturerIncome = bd.setScale(2, RoundingMode.DOWN);
        }
        return lecturerIncome;
    }

    @Override
    public OrderLog getByOrderNo(Long orderNo) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andOrderNoEqualTo(orderNo);
        List<OrderLog> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public Integer sumByPayTypeAndGmtCreate(Integer payType, String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct user_no) as count from order_log where");
        builder.append(" gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        builder.append(" and pay_type = '").append(payType).append("'");
        builder.append(" and order_status = '").append(OrderStatusEnum.SUCCESS.getCode()).append("'");
        String sql = builder.toString();
        Integer count = 0;
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public int orderNumByGmtCreateAndProductType(Date startgmtCreate, Date endgmtCreate, Integer productType) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andGmtCreateGreaterThanOrEqualTo(startgmtCreate);
        c.andGmtCreateLessThanOrEqualTo(endgmtCreate);
        c.andPayTypeEqualTo(productType);
        c.andOrderStatusEqualTo(OrderStatusEnum.SUCCESS.getCode());
        return this.mapper.countByExample(example);
    }


    @Override
    public BigDecimal orderIncomeByGmtCreateAndProductType(String startgmtCreate, String endgmtCreate, Integer productType) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(platform_income) as platformIncome from order_log where ");
        builder.append(" gmt_create >= '").append(startgmtCreate).append("'");
        builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        builder.append(" and product_type <= '").append(productType).append("'");
        builder.append(" and order_status = '").append(OrderStatusEnum.SUCCESS.getCode()).append("'");
        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        BigDecimal platformProfit = BigDecimal.valueOf(0);
        if (!StringUtils.isEmpty(map.get("platformIncome"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("platformIncome")));
            platformProfit = bd.setScale(2, RoundingMode.DOWN);
        }
        return platformProfit;
    }

    @Override
    public int updateByOrderNo(OrderLog record) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andOrderNoEqualTo(record.getOrderNo());
        return this.mapper.updateByExampleSelective(record, example);
    }

    @Override
    public List<OrderLog> listByUserNo(Long userNo) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int updateByMobile(Long userNo, String mobile) {
        OrderLogExample example = new OrderLogExample();
        OrderLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        OrderLog orderInfo = new OrderLog();
        orderInfo.setUserNo(userNo);
        orderInfo.setMobile(mobile);
        return this.mapper.updateByExampleSelective(orderInfo, example);
    }

}
