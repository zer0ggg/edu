package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatRESP", description="订单统计")
public class OrderStatRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "每天订单汇总")
    private List<OrderStatListRESP> list;

}
