package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单讲师统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatLecturerTodayRESP", description="订单讲师统计")
public class OrderStatLecturerTodayRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单讲师统计")
    private List<OrderStatLecturerTodayListRESP> list = new ArrayList<>();

}