package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.OrderStatVipQO;
import com.roncoo.education.data.feign.vo.OrderStatVipVO;
import com.roncoo.education.data.service.dao.OrderStatVipDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVip;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVipExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVipExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单会员统计
 *
 * @author wujing
 */
@Component
public class FeignOrderStatVipBiz extends BaseBiz {

    @Autowired
    private OrderStatVipDao dao;

	public Page<OrderStatVipVO> listForPage(OrderStatVipQO qo) {
	    OrderStatVipExample example = new OrderStatVipExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<OrderStatVip> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, OrderStatVipVO.class);
	}

	public int save(OrderStatVipQO qo) {
		OrderStatVip record = BeanUtil.copyProperties(qo, OrderStatVip.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public OrderStatVipVO getById(Long id) {
		OrderStatVip record = dao.getById(id);
		return BeanUtil.copyProperties(record, OrderStatVipVO.class);
	}

	public int updateById(OrderStatVipQO qo) {
		OrderStatVip record = BeanUtil.copyProperties(qo, OrderStatVip.class);
		return dao.updateById(record);
	}

}
