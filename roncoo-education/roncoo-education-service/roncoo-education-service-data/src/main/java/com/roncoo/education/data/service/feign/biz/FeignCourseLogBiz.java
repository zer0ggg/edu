package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriod;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 课程日志
 *
 * @author wujing
 */
@Component
public class FeignCourseLogBiz extends BaseBiz {
    @Autowired
    private IFeignCourseChapterPeriod feignCourseChapterPeriod;

    @Autowired
    private CourseLogDao dao;

    public Page<CourseLogVO> listForPage(CourseLogQO qo) {
        CourseLogExample example = new CourseLogExample();
        Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<CourseLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, CourseLogVO.class);
    }

    public int save(CourseLogQO qo) {
        CourseLog record = BeanUtil.copyProperties(qo, CourseLog.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public CourseLogVO getById(Long id) {
        CourseLog record = dao.getById(id);
        return BeanUtil.copyProperties(record, CourseLogVO.class);
    }

    public int updateById(CourseLogQO qo) {
        CourseLog record = BeanUtil.copyProperties(qo, CourseLog.class);
        return dao.updateById(record);
    }

    public CourseLogVO getByUserNoAndPeriodId(CourseLogQO courseLogQO) {
        CourseLog record = dao.getByUserNoAndPeriodId(courseLogQO.getUserNo(), courseLogQO.getPeriodId());
        return BeanUtil.copyProperties(record, CourseLogVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public int updateByCourseId(CourseLogQO courseLogQO) {
        CourseChapterPeriodQO qo = new CourseChapterPeriodQO();
        qo.setCourseId(courseLogQO.getCourseId());
        List<CourseChapterPeriodVO> list = feignCourseChapterPeriod.ListByCourseId(qo);
        for (CourseChapterPeriodVO vo : list) {
            List<CourseLog> record = dao.getByPeriodId(vo.getId());
            for (CourseLog courseLog : record) {
                if (StringUtils.hasText(vo.getVideoLength()) && vo.getVideoLength().equals(courseLog.getVidLength())) {
                    courseLog.setVidLength(vo.getVideoLength());
                    /*BigDecimal watchLength = new BigDecimal(courseLog.getWatchLength());
                    BigDecimal videoLength = new BigDecimal(DateUtil.getSecond(courseLog.getVidLength()));
                    BigDecimal watckProgress = watchLength.divide(videoLength, 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100L));
                    double f1 = watckProgress.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                    int i = (int) Math.round(f1);
                    courseLog.setWatckProgress(String.valueOf(i));*/
                    dao.updateById(courseLog);
                }
            }
        }
        return 1;

    }

    public List<CourseLogVO> listByUserNoAndCourseId(CourseLogQO record) {
        List<CourseLog> list = dao.listByUserNoAndCourseId(record.getUserNo(), record.getCourseId());
        return ArrayListUtil.copy(list, CourseLogVO.class);
    }

    public CourseLogVO getByUserNoAndPeriodIdLatest(CourseLogQO qo) {
        return BeanUtil.copyProperties(dao.getByUserNoAndPeriodIdLatest(qo.getUserNo(), qo.getPeriodId()), CourseLogVO.class);
    }
}
