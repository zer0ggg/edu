package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignOrderStatCourser;
import com.roncoo.education.data.feign.qo.OrderStatCourserQO;
import com.roncoo.education.data.feign.vo.OrderStatCourserVO;
import com.roncoo.education.data.service.feign.biz.FeignOrderStatCourserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单课程统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignOrderStatCourserController extends BaseController implements IFeignOrderStatCourser{

    @Autowired
    private FeignOrderStatCourserBiz biz;

	@Override
	public Page<OrderStatCourserVO> listForPage(@RequestBody OrderStatCourserQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody OrderStatCourserQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody OrderStatCourserQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public OrderStatCourserVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
