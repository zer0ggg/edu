package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.OrderStatLecturerQO;
import com.roncoo.education.data.feign.vo.OrderStatLecturerVO;
import com.roncoo.education.data.service.dao.OrderStatLecturerDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturer;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单讲师统计
 *
 * @author wujing
 */
@Component
public class FeignOrderStatLecturerBiz extends BaseBiz {

    @Autowired
    private OrderStatLecturerDao dao;

	public Page<OrderStatLecturerVO> listForPage(OrderStatLecturerQO qo) {
	    OrderStatLecturerExample example = new OrderStatLecturerExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<OrderStatLecturer> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, OrderStatLecturerVO.class);
	}

	public int save(OrderStatLecturerQO qo) {
		OrderStatLecturer record = BeanUtil.copyProperties(qo, OrderStatLecturer.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public OrderStatLecturerVO getById(Long id) {
		OrderStatLecturer record = dao.getById(id);
		return BeanUtil.copyProperties(record, OrderStatLecturerVO.class);
	}

	public int updateById(OrderStatLecturerQO qo) {
		OrderStatLecturer record = BeanUtil.copyProperties(qo, OrderStatLecturer.class);
		return dao.updateById(record);
	}

}
