package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserSummaryPageREQ", description="订单课程统计列表")
public class OrderStatCourserSummaryPageREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:题库,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "开始日期")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束日期")
    private String endGmtCreate;
    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
