package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.data.service.dao.OrderStatCourserDao;
import com.roncoo.education.data.service.dao.impl.mapper.OrderStatCourserMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 订单课程统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class OrderStatCourserDaoImpl extends AbstractBaseJdbc implements OrderStatCourserDao {

    @Autowired
    private OrderStatCourserMapper mapper;

    @Override
    public int save(OrderStatCourser record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderStatCourser record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderStatCourser getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<OrderStatCourser> listForPage(int pageCurrent, int pageSize, OrderStatCourserExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderStatCourser>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public OrderStatCourser getByGmtCreateAndCourseId(Date date, Long courseId) {
        OrderStatCourserExample example = new OrderStatCourserExample();
        OrderStatCourserExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(date);
        c.andCourseIdEqualTo(courseId);
        List<OrderStatCourser> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    @Override
    public List<OrderStatCourser> listByShowCount(int showCount, String courseName, Integer courseCategory, String beginGmtCreate, String endGmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select course_name as courseName, sum(course_num) as courseNum, sum(course_income) as courseIncome, sum(course_profit) as courseProfit from order_stat_courser where 1");
        if (StringUtils.hasText(courseName)) {
            courseName = PageUtil.like(courseName);
            builder.append(" and course_name like '").append(courseName).append("'");
        }
        if (courseCategory != null) {
            builder.append(" and course_category = '").append(courseCategory).append("'");
        }
        if (beginGmtCreate != null) {
            builder.append(" and gmt_create >= '").append(beginGmtCreate).append("'");
        }
        if (endGmtCreate != null) {
            builder.append(" and gmt_create <= '").append(endGmtCreate).append("'");
        }
        builder.append(" group by course_name order by courseProfit desc limit 0,").append(showCount);
        return queryForObjectList(builder.toString(), OrderStatCourser.class);
    }

    @Override
    public Page<OrderStatCourser> summaryList(String courseName, Integer courseCategory, String beginGmtCreate, String endGmtCreate, int pageCurrent, int pageSize) {
        StringBuilder builder = new StringBuilder();
        builder.append("select course_name as courseName from order_stat_courser where 1");
        if (StringUtils.hasText(courseName)) {
            courseName = PageUtil.like(courseName);
            builder.append(" and course_name like '").append(courseName).append("'");
        }
        if (courseCategory != null) {
            builder.append(" and course_category = '").append(courseCategory).append("'");
        }
        if (beginGmtCreate != null) {
            builder.append(" and gmt_create >= '").append(beginGmtCreate).append("'");
        }
        if (endGmtCreate != null) {
            builder.append(" and gmt_create <= '").append(endGmtCreate).append("'");
        }
        builder.append(" order by course_profit desc ");

        return queryForPage(builder.toString(), pageCurrent, pageSize, OrderStatCourser.class);
    }
}
