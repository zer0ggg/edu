package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.data.service.dao.OrderStatLecturerDao;
import com.roncoo.education.data.service.dao.impl.mapper.OrderStatLecturerMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturer;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 订单讲师统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class OrderStatLecturerDaoImpl extends AbstractBaseJdbc implements OrderStatLecturerDao {

    @Autowired
    private OrderStatLecturerMapper mapper;

    @Override
    public int save(OrderStatLecturer record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderStatLecturer record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderStatLecturer getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<OrderStatLecturer> listForPage(int pageCurrent, int pageSize, OrderStatLecturerExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderStatLecturer>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public OrderStatLecturer getByLecturerUserNoAndGmtCreate(Long lecturerUserNo, Date gmtCreate) {
        OrderStatLecturerExample example = new OrderStatLecturerExample();
        OrderStatLecturerExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(lecturerUserNo);
        c.andGmtCreateEqualTo(gmtCreate);
        List<OrderStatLecturer> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<OrderStatLecturer> listByShowCount(int showCount, String lecturerName, String beginGmtCreate, String endGmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select lecturer_name as lecturerName, sum(lecturer_order) as lecturerOrder, sum(lecturer_income) as lecturerIncome from order_stat_lecturer where 1");
        if (StringUtils.hasText(lecturerName)) {
            lecturerName = PageUtil.like(lecturerName);
            builder.append(" and lecturer_name like '").append(lecturerName).append("'");
        }
        if (beginGmtCreate != null) {
            builder.append(" and gmt_create >= '").append(beginGmtCreate).append("'");
        }
        if (endGmtCreate != null) {
            builder.append(" and gmt_create <= '").append(endGmtCreate).append("'");
        }
        builder.append(" group by lecturer_name order by lecturerIncome desc, id desc limit 0,").append(showCount);
        return queryForObjectList(builder.toString(), OrderStatLecturer.class);
    }

    @Override
    public Page<OrderStatCourser> summaryList(String lecturerName, String beginGmtCreate, String endGmtCreate, int pageCurrent, int pageSize) {
        StringBuilder builder = new StringBuilder();
        builder.append("select lecturer_name as lecturerName, sum(lecturer_order) as lecturerOrder, sum(lecturer_income) as lecturerIncome from order_stat_courser where 1");
        if (StringUtils.hasText(lecturerName)) {
            lecturerName = PageUtil.like(lecturerName);
            builder.append(" and lecturer_name like '").append(lecturerName).append("'");
        }
        if (beginGmtCreate != null) {
            builder.append(" and gmt_create >= '").append(DateUtil.parseDate(beginGmtCreate, "yyyy-MM-dd")).append("'");
        }
        if (endGmtCreate != null) {
            builder.append(" and gmt_create <= '").append(DateUtil.parseDate(endGmtCreate, "yyyy-MM-dd")).append("'");
        }
        builder.append(" group by lecturer_name order by lecturerIncome desc");
        return queryForPage(builder.toString(), pageCurrent, pageSize, OrderStatCourser.class);
    }
}
