package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.resp.UserStatRESP;
import com.roncoo.education.data.common.resp.UserStatViewRESP;
import com.roncoo.education.data.service.pc.biz.PcUserStatBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/user/stat")
@Api(value = "data-用户统计", tags = {"data-用户统计"})
public class PcUserStatController {

    @Autowired
    private PcUserStatBiz biz;

    @ApiOperation(value = "当天用户登录汇总", notes = "当天用户登录汇总")
    @PostMapping(value = "/view")
    public Result<UserStatViewRESP> view() {
        return biz.view();
    }

    @ApiOperation(value = "每天汇总登录列表", notes = "每天汇总登录列表")
    @PostMapping(value = "/list")
    public Result<UserStatRESP> listByAll() {
        return biz.listByAll();
    }

}
