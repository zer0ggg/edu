package com.roncoo.education.data.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.IsUseEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.OrderLogVO;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLogExample.Criteria;
import com.roncoo.education.marketing.feign.interfaces.IFeignActCouponUser;
import com.roncoo.education.marketing.feign.interfaces.IFeignActZoneCourse;
import com.roncoo.education.marketing.feign.qo.ActCouponUserQO;
import com.roncoo.education.marketing.feign.qo.ActZoneCourseQO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单日志
 *
 * @author wujing
 */
@Component
public class FeignOrderLogBiz extends BaseBiz {

  @Autowired private OrderLogDao dao;

  @Autowired private IFeignActCouponUser feignActCouponUser;
  @Autowired private IFeignActZoneCourse feignActZoneCourse;

  public Page<OrderLogVO> listForPage(OrderLogQO qo) {
    OrderLogExample example = new OrderLogExample();
    Criteria c = example.createCriteria();
    example.setOrderByClause(" id desc ");
    Page<OrderLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
    return PageUtil.transform(page, OrderLogVO.class);
  }

  public int save(OrderLogQO qo) {
    OrderLog record = BeanUtil.copyProperties(qo, OrderLog.class);
    return dao.save(record);
  }

  public int deleteById(Long id) {
    return dao.deleteById(id);
  }

  public OrderLogVO getById(Long id) {
    OrderLog record = dao.getById(id);
    return BeanUtil.copyProperties(record, OrderLogVO.class);
  }

  public int updateById(OrderLogQO qo) {
    OrderLog record = BeanUtil.copyProperties(qo, OrderLog.class);
    int results = dao.updateById(record);
    if (record.getActTypeId() != null && ActTypeEnum.COUPON.getCode().equals(record.getActType())) {
      ActCouponUserQO actCouponUserQO = new ActCouponUserQO();
      actCouponUserQO.setCouponId(record.getActTypeId());
      actCouponUserQO.setIsUse(IsUseEnum.NO.getCode());
      feignActCouponUser.updateByIsUse(actCouponUserQO);
    }
    return results;
  }

  public OrderLogVO getByOrderNo(Long orderNo) {
    OrderLog record = dao.getByOrderNo(orderNo);
    return BeanUtil.copyProperties(record, OrderLogVO.class);
  }

  public int updateByOrderNo(OrderLogQO qo) {
    OrderLog orderLog = dao.getByOrderNo(qo.getOrderNo());
    OrderLog record = BeanUtil.copyProperties(qo, OrderLog.class);
    int results = dao.updateByOrderNo(record);
    if (orderLog.getActTypeId() != null && orderLog.getActType() != null) {
      ActZoneCourseQO actZoneCourseQO = new ActZoneCourseQO();
      actZoneCourseQO.setActTypeId(orderLog.getActTypeId());
      actZoneCourseQO.setActType(orderLog.getActType());
      feignActZoneCourse.updateByOrder(actZoneCourseQO);
    }
    return results;
  }

  public int updateByMobile(OrderLogQO orderLogQO) {
    List<OrderLog> orderInfo = dao.listByUserNo(orderLogQO.getUserNo());
    if (CollectionUtil.isNotEmpty(orderInfo)) {
      dao.updateByMobile(orderLogQO.getUserNo(), orderLogQO.getMobile());
    }
    return 1;
  }
}
