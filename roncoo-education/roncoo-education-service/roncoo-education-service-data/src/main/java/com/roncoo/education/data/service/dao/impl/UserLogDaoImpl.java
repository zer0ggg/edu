package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.data.service.dao.UserLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.UserLogMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户日志 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class UserLogDaoImpl extends AbstractBaseJdbc implements UserLogDao {

    @Autowired
    private UserLogMapper mapper;

    @Override
    public int save(UserLog record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserLog record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserLog getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserLog> listForPage(int pageCurrent, int pageSize, UserLogExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserLog>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<UserLog> loginNumByGmtCreateAndProvince(Date startgmtCreate, Date endgmtCreate, String province) {
        UserLogExample example = new UserLogExample();
        UserLogExample.Criteria c = example.createCriteria();
        if (startgmtCreate != null) {
            c.andGmtCreateGreaterThanOrEqualTo(startgmtCreate);
        }
        if (endgmtCreate != null) {
            c.andGmtCreateLessThanOrEqualTo(endgmtCreate);
        }
        if (StringUtils.hasText(province)) {
            c.andProvinceEqualTo(province);
        }
        return this.mapper.selectByExample(example);
    }


    @Override
    public Integer loginSumByGmtCreateAndProvince(String startgmtCreate, String endgmtCreate, String province) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct user_no) as count from user_log where 1");
        if (StringUtils.hasText(startgmtCreate)) {
            builder.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        }
        if (StringUtils.hasText(endgmtCreate)) {
            builder.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        }
        if (StringUtils.hasText(province)) {
            builder.append(" and province = '").append(province).append("'");
        }
        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        Integer count = 0;
        if (!StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }

    @Override
    public Integer newUserNumByGmtCreate(String startgmtCreate, String endgmtCreate) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(distinct user_no) as count from user_log where 1");
        if (StringUtils.hasText(startgmtCreate)) {
            builder.append(" and register_time >= '").append(startgmtCreate).append("'");
        }
        if (StringUtils.hasText(endgmtCreate)) {
            builder.append(" and register_time <= '").append(endgmtCreate).append("'");
        }
        String sql = builder.toString();
        Integer newUserNum = 0;
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        if (!StringUtils.isEmpty(map.get("count"))) {
            newUserNum = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return newUserNum;
    }

    /**
     * 统计当天省登录
     */
    @Override
    public List<String> listByGmtCreateProvince(String startgmtCreate, String endgmtCreate) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct province from user_log where 1=1");
        sql.append(" and gmt_create >= '").append(startgmtCreate).append("'");
        sql.append(" and gmt_create <= '").append(endgmtCreate).append("'");
        List<String> resultList = jdbcTemplate.query(sql.toString(), new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                return resultSet.getString("province");
            }
        });
        return resultList;
    }
    /**
     * 统计时间段内
     */
    @Override
    public Integer sumByGmtCreate(Integer lognType, String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) as count from user_log where");
        builder.append(" login_type = ").append(lognType);
        builder.append(" and gmt_create >= '").append(date).append(" 00:00:00'");
        builder.append(" and gmt_create <= '").append(date).append(" 23:59:59'");
        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        Integer count = 0;
        if (!StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }
        return count;
    }


}
