package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvince;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvinceExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserStatProvinceMapper {
    int countByExample(UserStatProvinceExample example);

    int deleteByExample(UserStatProvinceExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserStatProvince record);

    int insertSelective(UserStatProvince record);

    List<UserStatProvince> selectByExample(UserStatProvinceExample example);

    UserStatProvince selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserStatProvince record, @Param("example") UserStatProvinceExample example);

    int updateByExample(@Param("record") UserStatProvince record, @Param("example") UserStatProvinceExample example);

    int updateByPrimaryKeySelective(UserStatProvince record);

    int updateByPrimaryKey(UserStatProvince record);
}
