package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignOrderStatLecturer;
import com.roncoo.education.data.feign.qo.OrderStatLecturerQO;
import com.roncoo.education.data.feign.vo.OrderStatLecturerVO;
import com.roncoo.education.data.service.feign.biz.FeignOrderStatLecturerBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单讲师统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignOrderStatLecturerController extends BaseController implements IFeignOrderStatLecturer{

    @Autowired
    private FeignOrderStatLecturerBiz biz;

	@Override
	public Page<OrderStatLecturerVO> listForPage(@RequestBody OrderStatLecturerQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody OrderStatLecturerQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody OrderStatLecturerQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public OrderStatLecturerVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
