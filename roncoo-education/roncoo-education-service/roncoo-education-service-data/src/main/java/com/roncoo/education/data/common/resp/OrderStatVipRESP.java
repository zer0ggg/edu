package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单会员统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatVipRESP", description="订单会员统计")
public class OrderStatVipRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单会员统计")
    private List<OrderStatVipListRESP> list;
}
