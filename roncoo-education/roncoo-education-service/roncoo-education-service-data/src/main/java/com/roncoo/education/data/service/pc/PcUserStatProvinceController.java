package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.resp.UserStatProvinceListRESP;
import com.roncoo.education.data.service.pc.biz.PcUserStatProvinceBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户省统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/user/stat/province")
@Api(value = "data-用户省统计", tags = {"data-用户省统计"})
public class PcUserStatProvinceController {

    @Autowired
    private PcUserStatProvinceBiz biz;

    @ApiOperation(value = "用户省统计列表", notes = "用户省统计列表")
    @PostMapping(value = "/list")
    public Result<UserStatProvinceListRESP> listByGmtCreate() {
        return biz.listByGmtCreate();
    }
}
