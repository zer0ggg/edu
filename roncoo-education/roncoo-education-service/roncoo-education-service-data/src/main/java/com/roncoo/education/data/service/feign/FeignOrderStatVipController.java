package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignOrderStatVip;
import com.roncoo.education.data.feign.qo.OrderStatVipQO;
import com.roncoo.education.data.feign.vo.OrderStatVipVO;
import com.roncoo.education.data.service.feign.biz.FeignOrderStatVipBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单会员统计
 *
 * @author wujing
 * @date 2020-05-25
 */
@RestController
public class FeignOrderStatVipController extends BaseController implements IFeignOrderStatVip{

    @Autowired
    private FeignOrderStatVipBiz biz;

	@Override
	public Page<OrderStatVipVO> listForPage(@RequestBody OrderStatVipQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody OrderStatVipQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody OrderStatVipQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public OrderStatVipVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
