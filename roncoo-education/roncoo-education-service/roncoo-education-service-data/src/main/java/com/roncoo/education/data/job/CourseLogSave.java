package com.roncoo.education.data.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.enums.CourseTypeEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapter;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriod;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.data.common.bo.CourseLogBO;
import com.roncoo.education.data.job.biz.CourseLogBiz;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 定时任务-用户学习记录
 *
 * @author wuyun
 */
@Slf4j
@Component
public class CourseLogSave extends IJobHandler {


    @Autowired
    private MyRedisTemplate myRedisTemplate;
    @Autowired
    private CourseLogBiz courseLogBiz;

    @Override
    @XxlJob(value = "courseLogSave")
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始执行[用户学习记录]");
        try {
            // 模糊查询课时学习进度前缀缓存keys
            Set<String> keys = myRedisTemplate.keys(RedisPreEnum.PERIOD_WATCK_PROGRESS.getCode() + "*");
            if (!keys.isEmpty()) {
                List<CourseLogBO> list = this.myRedisTemplate.multiGetKeys(keys, CourseLogBO.class);
                if (CollectionUtil.isNotEmpty(list)) {
                    courseLogBiz.saveCourseLog(list);
                }
            }

        } catch (Exception e) {
            log.error("定时任务-用户学习记录-执行出错", e);
            return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[用户学习记录]--处理异常");
        }
        XxlJobLogger.log("结束执行[用户学习记录]");
        return SUCCESS;
    }
}
