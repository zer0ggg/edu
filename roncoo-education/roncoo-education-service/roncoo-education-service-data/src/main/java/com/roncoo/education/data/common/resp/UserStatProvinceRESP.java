package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户省统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserStatProvinceRESP", description="用户省统计")
public class UserStatProvinceRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "省")
    private String name;

    @ApiModelProperty(value = "当天登录人数")
    private Integer value;

    @ApiModelProperty(value = "当天登录人次")
    private String loginNum;

    @ApiModelProperty(value = "当天登录人数(去重)")
    private String loginSum;
}
