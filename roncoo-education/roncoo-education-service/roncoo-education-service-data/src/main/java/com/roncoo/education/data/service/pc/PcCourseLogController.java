package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.CourseLogListREQ;
import com.roncoo.education.data.common.req.CourseLogPeriodPageREQ;
import com.roncoo.education.data.common.resp.CourseLogListRESP;
import com.roncoo.education.data.service.pc.biz.PcCourseLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 课程日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/course/log")
@Api(value = "data-课程日志", tags = {"data-课程日志"})
public class PcCourseLogController {

    @Autowired
    private PcCourseLogBiz biz;

    @ApiOperation(value = "课程日志列表", notes = "课程日志列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseLogListRESP>> list(@RequestBody CourseLogListREQ courseLogListREQ) {
        return biz.list(courseLogListREQ);
    }

    @ApiOperation(value = "课程日志列表", notes = "课程日志列表")
    @PostMapping(value = "/period/list")
    public Result<Page<CourseLogListRESP>> periodList(@RequestBody @Valid CourseLogPeriodPageREQ courseLogPeriodPageREQ) {
        return biz.periodList(courseLogPeriodPageREQ);
    }
}
