package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignUserLog;
import com.roncoo.education.data.feign.qo.UserLogQO;
import com.roncoo.education.data.feign.vo.UserLogVO;
import com.roncoo.education.data.service.feign.biz.FeignUserLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignUserLogController extends BaseController implements IFeignUserLog{

    @Autowired
    private FeignUserLogBiz biz;

	@Override
	public Page<UserLogVO> listForPage(@RequestBody UserLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserLogQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserLogQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserLogVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
