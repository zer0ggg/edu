package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatViewRESP", description="订单统计查看")
public class OrderStatViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "当天订单数")
    private Integer orderNum = 0;

    @ApiModelProperty(value = "当天收入")
    private BigDecimal orderIncome = BigDecimal.ZERO;

    @ApiModelProperty(value = "当天利润")
    private BigDecimal orderProfit = BigDecimal.ZERO;

    @ApiModelProperty(value = "当天购买用户")
    private Integer buyUser = 0;

    @ApiModelProperty(value = "当天购买新用户")
    private Integer buyNewUser = 0;
}
