package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.data.common.resp.OrderStatVipListRESP;
import com.roncoo.education.data.common.resp.OrderStatVipRESP;
import com.roncoo.education.data.service.dao.OrderStatVipDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单会员统计
 *
 * @author wujing
 */
@Component
public class PcOrderStatVipBiz extends BaseBiz {

    @Autowired
    private OrderStatVipDao dao;

    /**
    * 订单会员统计列表
    *
    * @return 订单会员统计分页查询结果
    */
    public Result<OrderStatVipRESP> listByAll() {
        OrderStatVipRESP resp = new OrderStatVipRESP();
        List<OrderStatVip> list = dao.listByAll();
        resp.setList(ArrayListUtil.copy(list, OrderStatVipListRESP.class));
        return Result.success(resp);
    }
}
