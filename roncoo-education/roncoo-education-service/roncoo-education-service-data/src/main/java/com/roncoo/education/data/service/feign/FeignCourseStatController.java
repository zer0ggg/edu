package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignCourseStat;
import com.roncoo.education.data.feign.qo.CourseStatQO;
import com.roncoo.education.data.feign.vo.CourseStatVO;
import com.roncoo.education.data.service.feign.biz.FeignCourseStatBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程统计
 *
 * @author wujing
 * @date 2020-06-29
 */
@RestController
public class FeignCourseStatController extends BaseController implements IFeignCourseStat{

    @Autowired
    private FeignCourseStatBiz biz;

	@Override
	public Page<CourseStatVO> listForPage(@RequestBody CourseStatQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseStatQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseStatQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public CourseStatVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
