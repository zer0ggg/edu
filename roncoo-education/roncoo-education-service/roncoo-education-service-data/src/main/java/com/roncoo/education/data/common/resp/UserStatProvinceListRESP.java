package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户省统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserStatProvinceListRESP", description="用户省统计列表")
public class UserStatProvinceListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户省统计列出信息")
    private List<UserStatProvinceRESP> list = new ArrayList<>();
}
