package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.UserLogQO;
import com.roncoo.education.data.feign.vo.UserLogVO;
import com.roncoo.education.data.service.dao.UserLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户日志
 *
 * @author wujing
 */
@Component
public class FeignUserLogBiz extends BaseBiz {

    @Autowired
    private UserLogDao dao;

	public Page<UserLogVO> listForPage(UserLogQO qo) {
	    UserLogExample example = new UserLogExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserLogVO.class);
	}

	public int save(UserLogQO qo) {
		UserLog record = BeanUtil.copyProperties(qo, UserLog.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserLogVO getById(Long id) {
		UserLog record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserLogVO.class);
	}

	public int updateById(UserLogQO qo) {
		UserLog record = BeanUtil.copyProperties(qo, UserLog.class);
		return dao.updateById(record);
	}

}
