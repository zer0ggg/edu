package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.CourseStatListREQ;
import com.roncoo.education.data.common.resp.CourseStatListRESP;
import com.roncoo.education.data.service.pc.biz.PcCourseStatBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/course/stat")
@Api(value = "data-课程统计", tags = {"data-课程统计"})
public class PcCourseStatController {

    @Autowired
    private PcCourseStatBiz biz;

    @ApiOperation(value = "课程统计列表", notes = "课程统计列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseStatListRESP>> list(@RequestBody CourseStatListREQ courseStatListREQ) {
        return biz.list(courseStatListREQ);
    }
}
