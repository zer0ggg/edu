package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.CourseStatUserListREQ;
import com.roncoo.education.data.common.resp.CourseStatUserListRESP;
import com.roncoo.education.data.service.pc.biz.PcCourseStatUserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程用户统计 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-课程用户统计")
@RestController
@RequestMapping("/data/pc/course/stat/user")
public class PcCourseStatUserController {

    @Autowired
    private PcCourseStatUserBiz biz;

    @ApiOperation(value = "课程用户统计列表", notes = "课程用户统计列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseStatUserListRESP>> list(@RequestBody CourseStatUserListREQ courseStatUserListREQ) {
        return biz.list(courseStatUserListREQ);
    }
}
