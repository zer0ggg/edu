package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderVipLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderVipLogExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderVipLogMapper {
    int countByExample(OrderVipLogExample example);

    int deleteByExample(OrderVipLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderVipLog record);

    int insertSelective(OrderVipLog record);

    List<OrderVipLog> selectByExample(OrderVipLogExample example);

    OrderVipLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderVipLog record, @Param("example") OrderVipLogExample example);

    int updateByExample(@Param("record") OrderVipLog record, @Param("example") OrderVipLogExample example);

    int updateByPrimaryKeySelective(OrderVipLog record);

    int updateByPrimaryKey(OrderVipLog record);
}
