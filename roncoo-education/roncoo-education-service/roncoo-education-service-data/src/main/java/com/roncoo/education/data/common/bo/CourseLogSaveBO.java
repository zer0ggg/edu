package com.roncoo.education.data.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 课程日志
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseLogBO", description = "课程日志")
public class CourseLogSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课时ID")
    @NotNull(message = "课时ID不能为空")
    private Long periodId;

    /**
     * 小数点6位
     */
    @ApiModelProperty(value = "观看时长(秒)")
    @NotNull(message = "观看时长不能为空")
    private BigDecimal watchLength;
}
