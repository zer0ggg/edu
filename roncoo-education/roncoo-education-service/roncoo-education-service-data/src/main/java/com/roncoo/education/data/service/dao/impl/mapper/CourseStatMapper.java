package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseStatMapper {
    int countByExample(CourseStatExample example);

    int deleteByExample(CourseStatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseStat record);

    int insertSelective(CourseStat record);

    List<CourseStat> selectByExample(CourseStatExample example);

    CourseStat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseStat record, @Param("example") CourseStatExample example);

    int updateByExample(@Param("record") CourseStat record, @Param("example") CourseStatExample example);

    int updateByPrimaryKeySelective(CourseStat record);

    int updateByPrimaryKey(CourseStat record);
}
