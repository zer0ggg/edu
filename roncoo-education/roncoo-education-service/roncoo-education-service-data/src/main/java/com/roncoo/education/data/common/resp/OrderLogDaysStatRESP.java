package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单日志列表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderLogDaysStatRESP", description="订单日志列表")
public class OrderLogDaysStatRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "微信扫码支付")
    private List<Integer> weixinList;

    @ApiModelProperty(value = "微信小程序支付订单数")
    private List<Integer> xcxList;

    @ApiModelProperty(value = "支付宝扫码支付订单数")
    private List<Integer> alipayList;

    @ApiModelProperty(value = "线下支付订单数")
    private List<Integer> manualList;

    @ApiModelProperty(value = "时间列")
    private List<String> dataList;

}
