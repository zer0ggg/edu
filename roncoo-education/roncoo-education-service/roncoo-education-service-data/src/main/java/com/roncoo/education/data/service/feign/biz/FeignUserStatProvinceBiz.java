package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.UserStatProvinceQO;
import com.roncoo.education.data.feign.vo.UserStatProvinceVO;
import com.roncoo.education.data.service.dao.UserStatProvinceDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvince;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvinceExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvinceExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户省统计
 *
 * @author wujing
 */
@Component
public class FeignUserStatProvinceBiz extends BaseBiz {

    @Autowired
    private UserStatProvinceDao dao;

	public Page<UserStatProvinceVO> listForPage(UserStatProvinceQO qo) {
	    UserStatProvinceExample example = new UserStatProvinceExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserStatProvince> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserStatProvinceVO.class);
	}

	public int save(UserStatProvinceQO qo) {
		UserStatProvince record = BeanUtil.copyProperties(qo, UserStatProvince.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserStatProvinceVO getById(Long id) {
		UserStatProvince record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserStatProvinceVO.class);
	}

	public int updateById(UserStatProvinceQO qo) {
		UserStatProvince record = BeanUtil.copyProperties(qo, UserStatProvince.class);
		return dao.updateById(record);
	}

}
