package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsShowLecturerEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.resp.OrderStatListRESP;
import com.roncoo.education.data.common.resp.OrderStatRESP;
import com.roncoo.education.data.common.resp.OrderStatViewRESP;
import com.roncoo.education.data.service.dao.OrderLogDao;
import com.roncoo.education.data.service.dao.OrderStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 订单统计
 *
 * @author wujing
 */
@Component
public class PcOrderStatBiz extends BaseBiz {

    @Autowired
    private OrderLogDao orderLogDao;
    @Autowired
    private OrderStatDao dao;

    /**
    * 订单统计列表
    *
    * @return 订单统计分页查询结果
    */
    public Result<OrderStatRESP> list() {
        OrderStatRESP resp = new OrderStatRESP();
        List<OrderStat> list =  dao.listByAll();
        resp.setList(ArrayListUtil.copy(list, OrderStatListRESP.class));
        return Result.success(resp);
    }

    public Result<OrderStatViewRESP> view() {
        OrderStatViewRESP resp = new OrderStatViewRESP();
        Date date = new Date();
        String format = DateUtil.format(date);
        Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");

        List<OrderLog> list  = orderLogDao.listByGmtCreateAndIsShowLecturer(startgmtCreate, endgmtCreate, IsShowLecturerEnum.YES.getCode());
        // 当天订单金额
        OrderLog price = orderLogDao.orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), 0L, IsShowLecturerEnum.YES.getCode());
        // 当天购买用户(去重）
        Integer buyUser  = orderLogDao.buyUserByGmtCreateAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), IsShowLecturerEnum.YES.getCode());
        // 获取当天记录
        resp.setGmtCreate(DateUtil.parseDate(format));
        resp.setOrderNum(list.size());
        resp.setOrderIncome(price.getPricePaid());
        resp.setOrderProfit(price.getPlatformIncome());
        resp.setBuyUser(buyUser);
        return Result.success(resp);
    }
}
