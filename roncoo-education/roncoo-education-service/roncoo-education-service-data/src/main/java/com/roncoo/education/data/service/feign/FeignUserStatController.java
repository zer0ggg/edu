package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignUserStat;
import com.roncoo.education.data.feign.qo.UserStatQO;
import com.roncoo.education.data.feign.vo.UserStatVO;
import com.roncoo.education.data.service.feign.biz.FeignUserStatBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignUserStatController extends BaseController implements IFeignUserStat{

    @Autowired
    private FeignUserStatBiz biz;

	@Override
	public Page<UserStatVO> listForPage(@RequestBody UserStatQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserStatQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserStatQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserStatVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
