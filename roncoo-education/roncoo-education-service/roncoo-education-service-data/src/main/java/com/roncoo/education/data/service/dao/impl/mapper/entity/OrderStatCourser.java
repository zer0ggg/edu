package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderStatCourser implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long courseId;

    private String courseName;

    private Integer courseCategory;

    private Integer courseNum;

    private BigDecimal courseIncome;

    private BigDecimal courseProfit;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public Integer getCourseCategory() {
        return courseCategory;
    }

    public void setCourseCategory(Integer courseCategory) {
        this.courseCategory = courseCategory;
    }

    public Integer getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(Integer courseNum) {
        this.courseNum = courseNum;
    }

    public BigDecimal getCourseIncome() {
        return courseIncome;
    }

    public void setCourseIncome(BigDecimal courseIncome) {
        this.courseIncome = courseIncome;
    }

    public BigDecimal getCourseProfit() {
        return courseProfit;
    }

    public void setCourseProfit(BigDecimal courseProfit) {
        this.courseProfit = courseProfit;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", courseId=").append(courseId);
        sb.append(", courseName=").append(courseName);
        sb.append(", courseCategory=").append(courseCategory);
        sb.append(", courseNum=").append(courseNum);
        sb.append(", courseIncome=").append(courseIncome);
        sb.append(", courseProfit=").append(courseProfit);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}