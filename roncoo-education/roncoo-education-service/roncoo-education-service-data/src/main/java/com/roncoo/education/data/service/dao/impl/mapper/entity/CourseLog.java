package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CourseLog implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long userNo;

    private String mobile;

    private Integer courseCategory;

    private Long periodId;

    private String periodName;

    private Long chapterId;

    private String chapterName;

    private Long courseId;

    private String courseName;

    private String vid;

    private String vidLength;

    private BigDecimal watchLength;

    private BigDecimal biggestWatchLength;

    private Integer watckProgress;

    private BigDecimal duration;

    private BigDecimal totalDuration;

    private Integer residueContrastTotal;

    private String contrastResult;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getCourseCategory() {
        return courseCategory;
    }

    public void setCourseCategory(Integer courseCategory) {
        this.courseCategory = courseCategory;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public void setPeriodId(Long periodId) {
        this.periodId = periodId;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName == null ? null : periodName.trim();
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName == null ? null : chapterName.trim();
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid == null ? null : vid.trim();
    }

    public String getVidLength() {
        return vidLength;
    }

    public void setVidLength(String vidLength) {
        this.vidLength = vidLength == null ? null : vidLength.trim();
    }

    public BigDecimal getWatchLength() {
        return watchLength;
    }

    public void setWatchLength(BigDecimal watchLength) {
        this.watchLength = watchLength;
    }

    public BigDecimal getBiggestWatchLength() {
        return biggestWatchLength;
    }

    public void setBiggestWatchLength(BigDecimal biggestWatchLength) {
        this.biggestWatchLength = biggestWatchLength;
    }

    public Integer getWatckProgress() {
        return watckProgress;
    }

    public void setWatckProgress(Integer watckProgress) {
        this.watckProgress = watckProgress;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public BigDecimal getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(BigDecimal totalDuration) {
        this.totalDuration = totalDuration;
    }

    public Integer getResidueContrastTotal() {
        return residueContrastTotal;
    }

    public void setResidueContrastTotal(Integer residueContrastTotal) {
        this.residueContrastTotal = residueContrastTotal;
    }

    public String getContrastResult() {
        return contrastResult;
    }

    public void setContrastResult(String contrastResult) {
        this.contrastResult = contrastResult == null ? null : contrastResult.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", userNo=").append(userNo);
        sb.append(", mobile=").append(mobile);
        sb.append(", courseCategory=").append(courseCategory);
        sb.append(", periodId=").append(periodId);
        sb.append(", periodName=").append(periodName);
        sb.append(", chapterId=").append(chapterId);
        sb.append(", chapterName=").append(chapterName);
        sb.append(", courseId=").append(courseId);
        sb.append(", courseName=").append(courseName);
        sb.append(", vid=").append(vid);
        sb.append(", vidLength=").append(vidLength);
        sb.append(", watchLength=").append(watchLength);
        sb.append(", biggestWatchLength=").append(biggestWatchLength);
        sb.append(", watckProgress=").append(watckProgress);
        sb.append(", duration=").append(duration);
        sb.append(", totalDuration=").append(totalDuration);
        sb.append(", residueContrastTotal=").append(residueContrastTotal);
        sb.append(", contrastResult=").append(contrastResult);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}