package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 课程用户统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseStatUserListRESP", description="课程用户统计列表")
public class CourseStatUserListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "观看时长（秒）")
    private String watchLength;

    @ApiModelProperty(value = "观看课程数(去重)")
    private Integer watchCourseSums;
}
