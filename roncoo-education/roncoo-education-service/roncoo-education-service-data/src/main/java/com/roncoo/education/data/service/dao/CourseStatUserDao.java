package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample;

import java.util.Date;

/**
 * 课程用户统计 服务类
 *
 * @author wujing
 * @date 2020-06-29
 */
public interface CourseStatUserDao {

    /**
    * 保存课程用户统计
    *
    * @param record 课程用户统计
    * @return 影响记录数
    */
    int save(CourseStatUser record);

    /**
    * 根据ID删除课程用户统计
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 课程用户统计
    * @return 影响记录数
    */
    int updateById(CourseStatUser record);

    /**
    * 根据ID获取课程用户统计
    *
    * @param id 主键ID
    * @return 课程用户统计
    */
    CourseStatUser getById(Long id);

    /**
    * 课程用户统计--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<CourseStatUser> listForPage(int pageCurrent, int pageSize, CourseStatUserExample example);

    /**
     * 根据用户编号、时间获取用户学习汇总信息
     *
     * @param userNo
     * @param gmtCreate
     * @return
     */
    CourseStatUser getByByUserNoAndGmtCreate(Long userNo, Date gmtCreate);
}
