package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseStatSaveREQ", description="课程统计添加")
public class CourseStatSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "日期")
    private LocalDate gmtCreate;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "观看人次")
    private Integer views;

    @ApiModelProperty(value = "观看人数(去重)")
    private Integer sums;

    @ApiModelProperty(value = "课程总时长")
    private String courseLength;

    @ApiModelProperty(value = "学习时长(秒)")
    private String studyLength;
}
