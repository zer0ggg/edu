package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatExample;

import java.util.Date;
import java.util.List;

/**
 * 订单统计 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface OrderStatDao {

    int save(OrderStat record);

    int deleteById(Long id);

    int updateById(OrderStat record);

    OrderStat getById(Long id);

    Page<OrderStat> listForPage(int pageCurrent, int pageSize, OrderStatExample example);

    /**
     * 获取当天订单统计
     *
     * @param gmtCreate
     * @return
     */
    OrderStat getByGmtCreate(Date gmtCreate);

    /**
     * 列出获取每天订单统计
     *
     * @return
     */
    List<OrderStat> listByAll();
}
