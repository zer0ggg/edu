package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.LognTypeEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.req.UserLogListREQ;
import com.roncoo.education.data.common.resp.UserLogDaysStatRESP;
import com.roncoo.education.data.common.resp.UserLogListRESP;
import com.roncoo.education.data.service.dao.UserLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 用户日志
 *
 * @author wujing
 */
@Component
public class PcUserLogBiz extends BaseBiz {

    @Autowired
    private UserLogDao dao;

    /**
    * 用户日志列表
    *
    * @param userLogListREQ 用户日志分页查询参数
    * @return 用户日志分页查询结果
    */
    public Result<Page<UserLogListRESP>> list(UserLogListREQ userLogListREQ) {
        UserLogExample example = new UserLogExample();
        Criteria c = example.createCriteria();
        if (userLogListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(userLogListREQ.getUserNo());
        }
        if (StringUtils.hasText(userLogListREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(userLogListREQ.getMobile()));
        }
        if (userLogListREQ.getLoginType() != null) {
            c.andLoginTypeEqualTo(userLogListREQ.getLoginType());
        }
        if (StringUtils.hasText(userLogListREQ.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(userLogListREQ.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(userLogListREQ.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(userLogListREQ.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        // 注册时间
        if (StringUtils.hasText(userLogListREQ.getBeginRegisterTime())) {
            c.andRegisterTimeGreaterThanOrEqualTo(DateUtil.parseDate(userLogListREQ.getBeginRegisterTime(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(userLogListREQ.getEndRegisterTime())) {
            c.andRegisterTimeLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(userLogListREQ.getEndRegisterTime(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" id desc ");
        Page<UserLog> page = dao.listForPage(userLogListREQ.getPageCurrent(), userLogListREQ.getPageSize(), example);
        Page<UserLogListRESP> respPage = PageUtil.transform(page, UserLogListRESP.class);
        return Result.success(respPage);
    }

    public Result<UserLogDaysStatRESP> daysStat() {
        UserLogDaysStatRESP resp = new UserLogDaysStatRESP();
        resp.setDataList(payTime());
        List<Integer> pcList = new ArrayList<>();
        List<Integer> xcxList = new ArrayList<>();
        for (String date : resp.getDataList()) {
            Integer pc = dao.sumByGmtCreate(LognTypeEnum.PC.getCode(), date);
            pcList.add(pc);
            Integer xcx = dao.sumByGmtCreate(LognTypeEnum.XCX.getCode(), date);
            xcxList.add(xcx);
        }
        resp.setPcList(pcList);
        resp.setXcxList(xcxList);
        return Result.success(resp);
    }

    /**
     * 获取当前时间与后7天的时间
     *
     * @author kyh
     * @return
     */
    private List<String> payTime() {
        List<String> xdata = new ArrayList<>();
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(DateUtil.parseDate(DateUtil.format(DateUtil.addDate(new Date(), -6)), "yyyy-MM-dd"));
        tempStart.add(Calendar.DAY_OF_YEAR, 0);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(DateUtil.parseDate(DateUtil.format(new Date()), "yyyy-MM-dd"));
        tempEnd.add(Calendar.DAY_OF_YEAR, 1);
        while (tempStart.before(tempEnd)) {
            xdata.add(DateUtil.formatDate(tempStart.getTime()));
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return xdata;
    }
}
