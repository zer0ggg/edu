package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 订单讲师统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatLecturerStatisticalREQ", description="订单讲师统计")
public class OrderStatLecturerStatisticalREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;
    /**
     * 获取条数
     */
    @ApiModelProperty(value = "获取条数")
    private int showCount = 10;

    @ApiModelProperty(value = "开始日期")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束日期")
    private String endGmtCreate;

}
