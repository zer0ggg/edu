package com.roncoo.education.data.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.data.service.dao.UserStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.UserStatMapper;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 用户统计 服务实现类
 *
 * @author wujing
 * @date 2020-05-20
 */
@Repository
public class UserStatDaoImpl implements UserStatDao {

    @Autowired
    private UserStatMapper mapper;

    @Override
    public int save(UserStat record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserStat record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserStat getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<UserStat> listForPage(int pageCurrent, int pageSize, UserStatExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserStat>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserStat getByGmtCreate(Date gmtCreate) {
        UserStatExample example = new UserStatExample();
        UserStatExample.Criteria c = example.createCriteria();
        c.andGmtCreateEqualTo(gmtCreate);
        List<UserStat> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<UserStat> listByAll() {
        UserStatExample example = new UserStatExample();
        UserStatExample.Criteria c = example.createCriteria();
        return this.mapper.selectByExample(example);
    }

}
