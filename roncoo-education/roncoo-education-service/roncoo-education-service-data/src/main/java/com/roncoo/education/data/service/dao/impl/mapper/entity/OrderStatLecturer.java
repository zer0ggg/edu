package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderStatLecturer implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long lecturerUserNo;

    private String lecturerName;

    private Integer lecturerOrder;

    private BigDecimal lecturerIncome;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getLecturerUserNo() {
        return lecturerUserNo;
    }

    public void setLecturerUserNo(Long lecturerUserNo) {
        this.lecturerUserNo = lecturerUserNo;
    }

    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName == null ? null : lecturerName.trim();
    }

    public Integer getLecturerOrder() {
        return lecturerOrder;
    }

    public void setLecturerOrder(Integer lecturerOrder) {
        this.lecturerOrder = lecturerOrder;
    }

    public BigDecimal getLecturerIncome() {
        return lecturerIncome;
    }

    public void setLecturerIncome(BigDecimal lecturerIncome) {
        this.lecturerIncome = lecturerIncome;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", lecturerUserNo=").append(lecturerUserNo);
        sb.append(", lecturerName=").append(lecturerName);
        sb.append(", lecturerOrder=").append(lecturerOrder);
        sb.append(", lecturerIncome=").append(lecturerIncome);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}