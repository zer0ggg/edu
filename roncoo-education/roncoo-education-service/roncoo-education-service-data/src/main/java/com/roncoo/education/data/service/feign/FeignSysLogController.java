package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignSysLog;
import com.roncoo.education.data.feign.qo.SysLogQO;
import com.roncoo.education.data.feign.vo.SysLogVO;
import com.roncoo.education.data.service.feign.biz.FeignSysLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 后台操作日志表
 *
 * @author wujing
 * @date 2020-06-09
 */
@RestController
public class FeignSysLogController extends BaseController implements IFeignSysLog{

    @Autowired
    private FeignSysLogBiz biz;

	@Override
	public Page<SysLogVO> listForPage(@RequestBody SysLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysLogQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysLogQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysLogVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
