package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.UserStatQO;
import com.roncoo.education.data.feign.vo.UserStatVO;
import com.roncoo.education.data.service.dao.UserStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户统计
 *
 * @author wujing
 */
@Component
public class FeignUserStatBiz extends BaseBiz {

    @Autowired
    private UserStatDao dao;

	public Page<UserStatVO> listForPage(UserStatQO qo) {
	    UserStatExample example = new UserStatExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserStat> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserStatVO.class);
	}

	public int save(UserStatQO qo) {
		UserStat record = BeanUtil.copyProperties(qo, UserStat.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserStatVO getById(Long id) {
		UserStat record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserStatVO.class);
	}

	public int updateById(UserStatQO qo) {
		UserStat record = BeanUtil.copyProperties(qo, UserStat.class);
		return dao.updateById(record);
	}

}
