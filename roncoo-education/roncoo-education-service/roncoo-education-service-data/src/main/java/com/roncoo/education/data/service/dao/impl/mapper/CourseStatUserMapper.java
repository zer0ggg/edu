package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseStatUserMapper {
    int countByExample(CourseStatUserExample example);

    int deleteByExample(CourseStatUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseStatUser record);

    int insertSelective(CourseStatUser record);

    List<CourseStatUser> selectByExample(CourseStatUserExample example);

    CourseStatUser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseStatUser record, @Param("example") CourseStatUserExample example);

    int updateByExample(@Param("record") CourseStatUser record, @Param("example") CourseStatUserExample example);

    int updateByPrimaryKeySelective(CourseStatUser record);

    int updateByPrimaryKey(CourseStatUser record);
}
