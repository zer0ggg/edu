package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample;

import java.util.Date;
import java.util.List;

/**
 * 订单课程统计 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface OrderStatCourserDao {

    int save(OrderStatCourser record);

    int deleteById(Long id);

    int updateById(OrderStatCourser record);

    OrderStatCourser getById(Long id);

    Page<OrderStatCourser> listForPage(int pageCurrent, int pageSize, OrderStatCourserExample example);

    /**
     * 根据课程ID获取当天课程统计
     * @param date
     * @param courseId
     * @return
     */
    OrderStatCourser getByGmtCreateAndCourseId(Date date, Long courseId);

    /**
     * 根据显示数列出指定条数的课程订单
     * @param showCount
     * @param courseName
     * @param courseCategory
     * @param beginGmtCreate
     * @param endGmtCreate
     * @return
     */
    List<OrderStatCourser> listByShowCount(int showCount, String courseName, Integer courseCategory, String beginGmtCreate, String endGmtCreate);

    /**
     * 汇总列表
     *
     * @param courseName
     * @param courseCategory
     * @param beginGmtCreate
     * @param endGmtCreate
     * @param pageCurrent
     * @param pageSize
     * @return
     */
    Page<OrderStatCourser> summaryList(String courseName, Integer courseCategory, String beginGmtCreate, String endGmtCreate, int pageCurrent, int pageSize);
}
