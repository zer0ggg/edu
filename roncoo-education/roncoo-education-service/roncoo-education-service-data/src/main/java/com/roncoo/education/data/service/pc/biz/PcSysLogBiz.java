package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.SysLogListREQ;
import com.roncoo.education.data.common.resp.SysLogListRESP;
import com.roncoo.education.data.service.dao.SysLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLogExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.SysLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 后台操作日志表
 *
 * @author wujing
 */
@Component
public class PcSysLogBiz extends BaseBiz {

	@Autowired
	private SysLogDao dao;

	/**
	 * 后台操作日志表列表
	 *
	 * @param sysLogListREQ 后台操作日志表分页查询参数
	 * @return 后台操作日志表分页查询结果
	 */
	public Result<Page<SysLogListRESP>> list(SysLogListREQ sysLogListREQ) {
		SysLogExample example = new SysLogExample();
		Criteria c = example.createCriteria();
		if (StringUtils.hasText(sysLogListREQ.getLoginName())) {
			c.andLoginNameLike(PageUtil.like(sysLogListREQ.getLoginName()));
		}
		if (StringUtils.hasText(sysLogListREQ.getLoginIp())) {
			c.andLoginIpLike(PageUtil.like(sysLogListREQ.getLoginIp()));
		}
		example.setOrderByClause(" id desc ");
		Page<SysLog> page = dao.listForPage(sysLogListREQ.getPageCurrent(), sysLogListREQ.getPageSize(), example);
		Page<SysLogListRESP> respPage = PageUtil.transform(page, SysLogListRESP.class);
		return Result.success(respPage);
	}
}
