package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.OrderStatCourserQO;
import com.roncoo.education.data.feign.vo.OrderStatCourserVO;
import com.roncoo.education.data.service.dao.OrderStatCourserDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单课程统计
 *
 * @author wujing
 */
@Component
public class FeignOrderStatCourserBiz extends BaseBiz {

    @Autowired
    private OrderStatCourserDao dao;

	public Page<OrderStatCourserVO> listForPage(OrderStatCourserQO qo) {
	    OrderStatCourserExample example = new OrderStatCourserExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<OrderStatCourser> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, OrderStatCourserVO.class);
	}

	public int save(OrderStatCourserQO qo) {
		OrderStatCourser record = BeanUtil.copyProperties(qo, OrderStatCourser.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public OrderStatCourserVO getById(Long id) {
		OrderStatCourser record = dao.getById(id);
		return BeanUtil.copyProperties(record, OrderStatCourserVO.class);
	}

	public int updateById(OrderStatCourserQO qo) {
		OrderStatCourser record = BeanUtil.copyProperties(qo, OrderStatCourser.class);
		return dao.updateById(record);
	}

}
