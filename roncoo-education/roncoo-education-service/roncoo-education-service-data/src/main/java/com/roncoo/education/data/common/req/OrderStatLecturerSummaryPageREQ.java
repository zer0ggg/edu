package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 订单讲师统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatLecturerListREQ", description="订单讲师统计列表")
public class OrderStatLecturerSummaryPageREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "开始日期")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束日期")
    private String endGmtCreate;
    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
