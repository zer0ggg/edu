package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLogExample;

import java.util.Date;
import java.util.List;

/**
 * 用户日志 服务类
 *
 * @author wujing
 * @date 2020-05-20
 */
public interface UserLogDao {

    int save(UserLog record);

    int deleteById(Long id);

    int updateById(UserLog record);

    UserLog getById(Long id);

    Page<UserLog> listForPage(int pageCurrent, int pageSize, UserLogExample example);

    /**
     * 获取当天登录人数(去重)
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    Integer loginSumByGmtCreateAndProvince(String startgmtCreate, String endgmtCreate, String province);

    /**
     * 列出当天登录人次
     * @return
     */
    List<UserLog> loginNumByGmtCreateAndProvince(Date startgmtCreate, Date endgmtCreate, String province);
    /**
     * 当天新增用户
     *
     * @return
     */
    Integer newUserNumByGmtCreate(String startgmtCreate, String endgmtCreate);

    /**
     * 列出当天登录的省份
     * @param startgmtCreate
     * @param endgmtCreate
     * @return
     */
    List<String> listByGmtCreateProvince(String startgmtCreate, String endgmtCreate);

    Integer sumByGmtCreate(Integer lognType, String date);

}
