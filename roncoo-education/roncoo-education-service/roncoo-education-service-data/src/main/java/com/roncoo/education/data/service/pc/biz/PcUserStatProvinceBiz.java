package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.resp.UserStatProvinceListRESP;
import com.roncoo.education.data.common.resp.UserStatProvinceRESP;
import com.roncoo.education.data.service.dao.UserLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用户省统计
 *
 * @author wujing
 */
@Component
public class PcUserStatProvinceBiz extends BaseBiz {

    @Autowired
    private UserLogDao userLogDao;

    /**
    * 用户省统计列表
    *
    * @return 用户省统计分页查询结果
    */
    public Result<UserStatProvinceListRESP> listByGmtCreate() {
        UserStatProvinceListRESP listRESP = new UserStatProvinceListRESP();
        Date date = new Date();
        String format = DateUtil.format(date);
        Date startGmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endGmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        // 列出当天登录的省份
        List<String> provinceList  = userLogDao.listByGmtCreateProvince(DateUtil.getDateTime(startGmtCreate), DateUtil.getDateTime(endGmtCreate));
        if (provinceList.isEmpty()) {
            return Result.success(listRESP);
        }
        List<UserStatProvinceRESP> userStatProvinceRESPList = new ArrayList<>();
        for (String province: provinceList) {
            UserStatProvinceRESP resp = new UserStatProvinceRESP();
            // 当天、省份登录人数(去重)
            Integer loginSum  = userLogDao.loginSumByGmtCreateAndProvince(DateUtil.getDateTime(startGmtCreate), DateUtil.getDateTime(endGmtCreate), province);
            resp.setName(province);
            resp.setValue(loginSum);
            userStatProvinceRESPList.add(resp);
        }
        listRESP.setList(userStatProvinceRESPList);
        return Result.success(listRESP);
    }
}
