package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignCourseStatUser;
import com.roncoo.education.data.feign.qo.CourseStatUserQO;
import com.roncoo.education.data.feign.vo.CourseStatUserVO;
import com.roncoo.education.data.service.feign.biz.FeignCourseStatUserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程用户统计
 *
 * @author wujing
 * @date 2020-06-29
 */
@RestController
public class FeignCourseStatUserController extends BaseController implements IFeignCourseStatUser{

    @Autowired
    private FeignCourseStatUserBiz biz;

	@Override
	public Page<CourseStatUserVO> listForPage(@RequestBody CourseStatUserQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseStatUserQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseStatUserQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public CourseStatUserVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
