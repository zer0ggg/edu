package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单讲师统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatLecturerTodayListRESP", description="订单讲师统计")
public class OrderStatLecturerTodayListRESP implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "讲师名称")
    private String name;

    @ApiModelProperty(value = "讲师日订单数")
    private Integer value = 0;

    @ApiModelProperty(value = "讲师日收入")
    private BigDecimal lecturerIncome = BigDecimal.ZERO;

}