package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单会员统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatVipListRESP", description="订单会员统计列表")
public class OrderStatVipListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当天时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreate;

    @ApiModelProperty(value = "订单数量")
    private Integer orderNum;

    @ApiModelProperty(value = "当天收入")
    private BigDecimal orderIncome;
}
