package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class CourseStatUser implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long userNo;

    private String mobile;

    private String watchLength;

    private Integer watchCourseSums;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getWatchLength() {
        return watchLength;
    }

    public void setWatchLength(String watchLength) {
        this.watchLength = watchLength == null ? null : watchLength.trim();
    }

    public Integer getWatchCourseSums() {
        return watchCourseSums;
    }

    public void setWatchCourseSums(Integer watchCourseSums) {
        this.watchCourseSums = watchCourseSums;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", userNo=").append(userNo);
        sb.append(", mobile=").append(mobile);
        sb.append(", watchLength=").append(watchLength);
        sb.append(", watchCourseSums=").append(watchCourseSums);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}