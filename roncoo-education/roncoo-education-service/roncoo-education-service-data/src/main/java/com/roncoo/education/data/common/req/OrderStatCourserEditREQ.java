package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserEditREQ", description="订单课程统计修改")
public class OrderStatCourserEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDate gmtCreate;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:题库,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程数量")
    private Integer courseNum;

    @ApiModelProperty(value = "课程收入")
    private BigDecimal courseIncome;

    @ApiModelProperty(value = "课程利润")
    private BigDecimal courseProfit;
}
