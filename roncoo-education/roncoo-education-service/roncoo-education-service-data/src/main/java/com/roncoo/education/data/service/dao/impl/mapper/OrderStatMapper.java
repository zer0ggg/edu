package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderStatMapper {
    int countByExample(OrderStatExample example);

    int deleteByExample(OrderStatExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderStat record);

    int insertSelective(OrderStat record);

    List<OrderStat> selectByExample(OrderStatExample example);

    OrderStat selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderStat record, @Param("example") OrderStatExample example);

    int updateByExample(@Param("record") OrderStat record, @Param("example") OrderStatExample example);

    int updateByPrimaryKeySelective(OrderStat record);

    int updateByPrimaryKey(OrderStat record);
}
