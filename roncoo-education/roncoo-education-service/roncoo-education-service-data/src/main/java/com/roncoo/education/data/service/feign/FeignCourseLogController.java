package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import com.roncoo.education.data.service.feign.biz.FeignCourseLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 课程日志
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignCourseLogController extends BaseController implements IFeignCourseLog{

    @Autowired
    private FeignCourseLogBiz biz;

	@Override
	public Page<CourseLogVO> listForPage(@RequestBody CourseLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseLogQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseLogQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public CourseLogVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

    @Override
    public CourseLogVO getByUserNoAndPeriodId(@RequestBody CourseLogQO courseLogQO) {
        return biz.getByUserNoAndPeriodId(courseLogQO);
    }

    @Override
    public int updateByCourseId(@RequestBody CourseLogQO courseLogQO) {
        return biz.updateByCourseId(courseLogQO);
    }

    @Override
    public List<CourseLogVO> listByUserNoAndCourseId(@RequestBody CourseLogQO record) {
		return biz.listByUserNoAndCourseId(record);
    }

	@Override
	public CourseLogVO getByUserNoAndPeriodIdLatest(@RequestBody CourseLogQO courseLogQO) {
		return biz.getByUserNoAndPeriodIdLatest(courseLogQO);
	}

}
