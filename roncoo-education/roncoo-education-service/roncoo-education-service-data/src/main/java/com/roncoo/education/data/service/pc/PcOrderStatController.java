package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.resp.OrderStatRESP;
import com.roncoo.education.data.common.resp.OrderStatViewRESP;
import com.roncoo.education.data.service.pc.biz.PcOrderStatBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/order/stat")
@Api(value = "data-订单统计", tags = {"data-订单统计"})
public class PcOrderStatController {

    @Autowired
    private PcOrderStatBiz biz;

    @ApiOperation(value = "当天订单汇总", notes = "当天订单汇总")
    @PostMapping(value = "/view")
    public Result<OrderStatViewRESP> view() {
        return biz.view();
    }

    @ApiOperation(value = "每天订单汇总列表", notes = "每天订单汇总列表")
    @PostMapping(value = "/list")
    public Result<OrderStatRESP> list() {
        return biz.list();
    }
}
