package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class CourseStat implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Long courseId;

    private String courseName;

    private Integer courseCategory;

    private Integer views;

    private Integer sums;

    private String courseLength;

    private String studyLength;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public Integer getCourseCategory() {
        return courseCategory;
    }

    public void setCourseCategory(Integer courseCategory) {
        this.courseCategory = courseCategory;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getSums() {
        return sums;
    }

    public void setSums(Integer sums) {
        this.sums = sums;
    }

    public String getCourseLength() {
        return courseLength;
    }

    public void setCourseLength(String courseLength) {
        this.courseLength = courseLength == null ? null : courseLength.trim();
    }

    public String getStudyLength() {
        return studyLength;
    }

    public void setStudyLength(String studyLength) {
        this.studyLength = studyLength == null ? null : studyLength.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", courseId=").append(courseId);
        sb.append(", courseName=").append(courseName);
        sb.append(", courseCategory=").append(courseCategory);
        sb.append(", views=").append(views);
        sb.append(", sums=").append(sums);
        sb.append(", courseLength=").append(courseLength);
        sb.append(", studyLength=").append(studyLength);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}