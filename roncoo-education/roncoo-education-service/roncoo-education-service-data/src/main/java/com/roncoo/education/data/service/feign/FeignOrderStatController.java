package com.roncoo.education.data.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.feign.interfaces.IFeignOrderStat;
import com.roncoo.education.data.feign.qo.OrderStatQO;
import com.roncoo.education.data.feign.vo.OrderStatVO;
import com.roncoo.education.data.service.feign.biz.FeignOrderStatBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单统计
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
public class FeignOrderStatController extends BaseController implements IFeignOrderStat{

    @Autowired
    private FeignOrderStatBiz biz;

	@Override
	public Page<OrderStatVO> listForPage(@RequestBody OrderStatQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody OrderStatQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody OrderStatQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public OrderStatVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
