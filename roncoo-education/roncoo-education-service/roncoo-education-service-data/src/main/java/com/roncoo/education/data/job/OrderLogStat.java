package com.roncoo.education.data.job;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.enums.IsShowLecturerEnum;
import com.roncoo.education.common.core.enums.ProductTypeEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.service.dao.*;
import com.roncoo.education.data.service.dao.impl.mapper.entity.*;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 定时任务-	订单汇总统计：建议每天凌晨执行
 *
 * @author wuyun
 */
@Slf4j
@Component
public class OrderLogStat extends IJobHandler {

    @Autowired
	private OrderLogDao orderLogDao;
    @Autowired
	private OrderStatDao orderStatDao;
    @Autowired
	private OrderStatCourserDao orderStatCourserDao;
    @Autowired
	private OrderStatLecturerDao orderStatLecturerDao;
	@Autowired
	private OrderStatVipDao orderStatVipDao;


	@Override
	@XxlJob(value = "orderJob")
	public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始执行[统计当天订单]");
		try {
			Date date = DateUtil.addDate(new Date(), -1);
			handleScheduledTasks(date);
			orderStatCourser(date);
			orderStatLecturer(date);
			handleScheduledTasksVip(date);
		} catch (Exception e) {
			log.error("定时任务-统计当天订单-执行出错", e);
			return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[统计当天订单]--处理异常");
		}
        XxlJobLogger.log("结束执行[统计当天订单]");
		return SUCCESS;
	}

	/**
	 * 订单统计
	 * @return
	 */
	private void handleScheduledTasks(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		// 列出当天订单数
		List<OrderLog> list  = orderLogDao.listByGmtCreateAndIsShowLecturer(startgmtCreate, endgmtCreate, IsShowLecturerEnum.YES.getCode());
		// 当天订单金额
		OrderLog price = orderLogDao.orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), 0L, IsShowLecturerEnum.YES.getCode());
		// 当天购买用户(去重）
		Integer buyUser  = orderLogDao.buyUserByGmtCreateAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), IsShowLecturerEnum.YES.getCode());
		// 获取当天记录
		OrderStat orderStat = orderStatDao.getByGmtCreate(DateUtil.parseDate(format));

		OrderStat record = new OrderStat();
		record.setOrderNum(list.size());
		record.setOrderIncome(price.getPricePaid());
		record.setOrderProfit(price.getPlatformIncome());
		record.setBuyNewUser(buyUser);
		// 如果当天不存在该课程记录, 新添加
		if (ObjectUtil.isNull(orderStat)) {
			record.setGmtCreate(DateUtil.parseDate(format));
			orderStatDao.save(record);
		} else {
			record.setId(orderStat.getId());
			orderStatDao.updateById(orderStat);
		}
		return;
	}
	/**
	 * 订单课程统计
	 */
	private void orderStatCourser(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		// 列出当天课程订单（去重）
		List<OrderLog> list  = orderLogDao.listByGmtCreateAndNotProductTypeAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), ProductTypeEnum.VIP.getCode(), IsShowLecturerEnum.YES.getCode());
		if (list.isEmpty()) {
			return;
		}
		for (OrderLog orderLog : list) {
			// 列出当天课程订单数
			List<OrderLog> courseNum  = orderLogDao.listByGmtCreateAndProductIdAndIsShowLecturer(startgmtCreate, endgmtCreate, orderLog.getProductId(), IsShowLecturerEnum.YES.getCode());
			// 当天订单课程金额
			OrderLog price  = orderLogDao.orderIncomeSumByGmtCreateAndProductIdAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), orderLog.getProductId(), IsShowLecturerEnum.YES.getCode());
			// 列出当天订单数
			OrderStatCourser orderStatCourser = orderStatCourserDao.getByGmtCreateAndCourseId(DateUtil.parseDate(format), orderLog.getProductId());

			OrderStatCourser record = new OrderStatCourser();
			record.setCourseNum(courseNum.size());
			record.setCourseIncome(price.getPricePaid());
			record.setCourseProfit(price.getPlatformIncome());
			// 如果当天不存在该课程记录, 新添加
			if (ObjectUtil.isNull(orderStatCourser)) {
				record.setGmtCreate(DateUtil.parseDate(format));
				record.setCourseId(orderLog.getProductId());
				record.setCourseName(orderLog.getProductName());
				record.setCourseCategory(orderLog.getProductType());
				orderStatCourserDao.save(record);
			} else {
				record.setId(orderStatCourser.getId());
				orderStatCourserDao.updateById(record);
			}
		}
		return;
	}

	/**
	 * 订单讲师统计
	 * @return
	 */
	private void orderStatLecturer(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		// 列出当天讲师订单（去重）
		List<OrderLog> list  = orderLogDao.listByGmtCreateAndGroupByLecturerUserNoAndProductTypeAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), ProductTypeEnum.VIP.getCode(), IsShowLecturerEnum.YES.getCode());
		if (list.isEmpty() || list.size() == 0) {
			return;
		}
		for (OrderLog orderLog : list) {
			// 列出当天讲师订单数
			List<OrderLog> lecturerOrder  = orderLogDao.listByGmtCreateAndLecturerUserNoAndIsShowLecturer(startgmtCreate, endgmtCreate, orderLog.getLecturerUserNo(), IsShowLecturerEnum.YES.getCode());
			// 当天统计讲师收益
			BigDecimal lecturerIncome = orderLogDao.lecturerIncomeByGmtCreateAndLecturerUserNoAndIsShowLecturer(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), orderLog.getLecturerUserNo(), IsShowLecturerEnum.YES.getCode());
			// 获取当天讲师统计记录
			OrderStatLecturer orderStatLecturer = orderStatLecturerDao.getByLecturerUserNoAndGmtCreate(orderLog.getLecturerUserNo(), DateUtil.parseDate(format));

			OrderStatLecturer record = new OrderStatLecturer();
			record.setLecturerName(orderLog.getLecturerName());
			record.setLecturerOrder(lecturerOrder.size());
			record.setLecturerIncome(lecturerIncome);
			// 如果当天不存在该讲师记录新添加
			if (ObjectUtil.isNull(orderStatLecturer)) {
				record.setGmtCreate(DateUtil.parseDate(format));
				record.setLecturerUserNo(orderLog.getLecturerUserNo());
				orderStatLecturerDao.save(record);
			} else {
				record.setId(orderStatLecturer.getId());
				orderStatLecturerDao.updateById(record);
			}
		}
		return;
	}

	/**
	 * 会员订单
	 *
	 * @return
	 */
	private void handleScheduledTasksVip(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		/**
		 * 列出当天订单数
		 */
		int orderNum = orderLogDao.orderNumByGmtCreateAndProductType(startgmtCreate, endgmtCreate, ProductTypeEnum.VIP.getCode());
		/**
		 * 当天收入
		 */
		BigDecimal orderIncome = orderLogDao.orderIncomeByGmtCreateAndProductType(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), ProductTypeEnum.VIP.getCode());

		OrderStatVip orderStatVip = orderStatVipDao.getByGmtCreate(DateUtil.parseDate(format));
		OrderStatVip record = new OrderStatVip();
		record.setOrderNum(orderNum);
		record.setOrderIncome(orderIncome);

		if (ObjectUtil.isNull(orderStatVip)) {
			record.setGmtCreate(DateUtil.parseDate(format));
			orderStatVipDao.save(record);
		} else {
			record.setId(orderStatVip.getId());
			orderStatVipDao.updateById(record);
		}
		return;
	}

}
