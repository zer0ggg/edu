package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatCourserExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderStatCourserMapper {
    int countByExample(OrderStatCourserExample example);

    int deleteByExample(OrderStatCourserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderStatCourser record);

    int insertSelective(OrderStatCourser record);

    List<OrderStatCourser> selectByExample(OrderStatCourserExample example);

    OrderStatCourser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderStatCourser record, @Param("example") OrderStatCourserExample example);

    int updateByExample(@Param("record") OrderStatCourser record, @Param("example") OrderStatCourserExample example);

    int updateByPrimaryKeySelective(OrderStatCourser record);

    int updateByPrimaryKey(OrderStatCourser record);
}
