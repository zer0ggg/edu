package com.roncoo.education.data.job.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.CourseTypeEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapter;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriod;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.qo.CourseChapterQO;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.data.common.bo.CourseLogBO;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CourseLogBiz extends BaseBiz {

    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignCourseChapter feignCourseChapter;
    @Autowired
    private IFeignCourseChapterPeriod feignCourseChapterPeriod;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;

    @Autowired
    private CourseLogDao courseLogDao;


    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Transactional(rollbackFor = Exception.class)
    public Integer saveCourseLog(List<CourseLogBO> list) {
        Set<Long> periodIds = list.stream().map(CourseLogBO::getPeriodId).collect(Collectors.toSet());
        CourseChapterPeriodQO qo = new CourseChapterPeriodQO();
        qo.setIds(new ArrayList<>(periodIds));
        // 根据课时ID集合获取课时信息
        List<CourseChapterPeriodVO> periodlist = feignCourseChapterPeriod.listByIds(qo);
        if (CollectionUtil.isEmpty(periodlist)) {
            return 0;
        }
        Map<Long, CourseChapterPeriodVO> periodlistMap = periodlist.stream().collect(Collectors.toMap(CourseChapterPeriodVO::getId, item -> item));
        // 批量获取课程信息
        Set<Long> courseIds = periodlist.stream().map(CourseChapterPeriodVO::getCourseId).collect(Collectors.toSet());
        CourseQO courseQO = new CourseQO();
        courseQO.setListId(new ArrayList<>(courseIds));
        // 根据课程ID集合获取课程信息
        List<CourseVO> courseVOList = feignCourse.listByIds(courseQO);
        if (CollectionUtil.isEmpty(courseVOList)) {
            return 0;
        }
        Map<Long, CourseVO> courseVOListMap = courseVOList.stream().collect(Collectors.toMap(CourseVO::getId, item -> item));
        // 批量获取章节信息
        Set<Long> chapterIds = periodlist.stream().map(CourseChapterPeriodVO::getChapterId).collect(Collectors.toSet());
        CourseChapterQO courseChapterQO = new CourseChapterQO();
        courseChapterQO.setIds(new ArrayList<>(chapterIds));
        // 根据章节ID集合获取章节信息
        List<CourseChapterVO> courseChapterVOList = feignCourseChapter.listByIds(courseChapterQO);
        if (CollectionUtil.isEmpty(courseChapterVOList)) {
            return 0;
        }
        Map<Long, CourseChapterVO> courseChapterVOListMap = courseChapterVOList.stream().collect(Collectors.toMap(CourseChapterVO::getId, item -> item));
        for (CourseLogBO courseLogBO : list) {
            String key = RedisPreEnum.PERIOD_WATCK_PROGRESS.getCode().concat(courseLogBO.getUserNo().toString() + courseLogBO.getPeriodId().toString());
            // 获取课时信息
            CourseChapterPeriodVO courseChapterPeriodVO = periodlistMap.get(courseLogBO.getPeriodId());
            if (courseChapterPeriodVO == null) {
                myRedisTemplate.delete(key);
            }

            // 最后更新时间
            Date endgmtCreate = DateUtil.parseDate(courseLogBO.getData(), "yyyy-MM-dd HH:mm:ss");
            long cha = System.currentTimeMillis() - endgmtCreate.getTime();
            if (cha < 10000) {
                continue;
            }

            // 超过10秒没更新，则存库
            CourseLog record = new CourseLog();
            CourseLog courseLog = courseLogDao.getByUserNoAndPeriodIdLatest(courseLogBO.getUserNo(), courseLogBO.getPeriodId());
            if (ObjectUtil.isNotEmpty(courseLog)) {
                record.setResidueContrastTotal(courseLog.getResidueContrastTotal());
            }
            record.setGmtCreate(endgmtCreate);
            record.setUserNo(courseLogBO.getUserNo());
            record.setMobile(courseLogBO.getMobile());
            record.setCourseId(courseChapterPeriodVO.getCourseId());
            record.setChapterId(courseChapterPeriodVO.getChapterId());
            record.setPeriodId(courseLogBO.getPeriodId());
            record.setPeriodName(courseChapterPeriodVO.getPeriodName());
            // 课时视频vid
            record.setVid(courseChapterPeriodVO.getVideoVid());
            // 课时视频时长
            record.setVidLength(courseChapterPeriodVO.getVideoLength());
            // 观看结束时间点
            record.setWatchLength(courseLogBO.getWatchLength());
            // 观看结束时间点（当次历史最大时间点)
            record.setBiggestWatchLength(courseLogBO.getBiggestWatchLength());

            // 处理课时学习总时长-开始
            Long interval = getaLong(courseLogBO);
            BigDecimal duration = BigDecimal.ZERO;
            if (interval != null) {
                duration = BigDecimal.valueOf(interval);
            }
            // 学习时长(当次学习使用的时长)
            record.setDuration(duration);
            if (ObjectUtil.isNotNull(courseLog)) {
                // 课时学习总时长(每次叠加)
                record.setTotalDuration(courseLog.getTotalDuration().add(record.getDuration()));
            } else {
                // 课时学习总时长
                record.setTotalDuration(record.getDuration());
            }
            // 处理课时学习总时长状态-结束
            CourseVO course = courseVOListMap.get(courseChapterPeriodVO.getCourseId());
            if (ObjectUtil.isNotNull(course)) {
                record.setCourseName(course.getCourseName());
                record.setCourseCategory(course.getCourseCategory());
            }
            CourseChapterVO courseChapterVO = courseChapterVOListMap.get(courseChapterPeriodVO.getChapterId());
            if (ObjectUtil.isNotNull(courseChapterVO)) {
                record.setChapterName(courseChapterVO.getChapterName());
            }
            // 处理历史最大时间点-开始
            BigDecimal biggestWatckLength = courseLogDao.getByUserNoAndPeriodIdAndBiggestWatchLength(courseLogBO.getUserNo(), courseLogBO.getPeriodId());
            if (biggestWatckLength != null) {
                // 比较当次历史最大结束时间点 是否比历史最大结束时间点
                if (courseLogBO.getBiggestWatchLength().compareTo(biggestWatckLength) == 1) {
                    // 观看结束时间点（当次历史最大时间点)
                    record.setBiggestWatchLength(courseLogBO.getBiggestWatchLength());
                } else {
                    record.setBiggestWatchLength(biggestWatckLength);
                }
            } else {
                record.setBiggestWatchLength(courseLogBO.getBiggestWatchLength());
            }
            // 处理历史最大时间点-结束

            // 处理学习进度-开始
            // 转换成秒计算进度（保留小数点后两位）
            // 课时视频时长
            BigDecimal videoLength = new BigDecimal(DateUtil.getSecond(courseChapterPeriodVO.getVideoLength()));
            // 防止学习时长大于课时视频时长
            if (record.getBiggestWatchLength().compareTo(videoLength) == 1) {
                record.setBiggestWatchLength(videoLength);
            }
            // 防止学习时长大于课时视频时长
            if (record.getWatchLength().compareTo(videoLength) == 1) {
                record.setWatchLength(videoLength);
            }
            // 如果学习时间距离结束时间小于5秒
            // 课时学习进度为100%
            if (videoLength.subtract(record.getBiggestWatchLength()).intValue() < 5) {
                record.setWatckProgress(100);
                // 设置历史最大时间为课时时长
                record.setBiggestWatchLength(videoLength);
            } else {
                //离结束大于5秒
                BigDecimal watchProgress = record.getBiggestWatchLength().divide(videoLength, 2, BigDecimal.ROUND_HALF_UP);
                if (watchProgress.compareTo(BigDecimal.valueOf(0.95)) >= 0) {
                    watchProgress = record.getBiggestWatchLength().divide(videoLength, 2, BigDecimal.ROUND_DOWN);
                }
                //观看进度(观看总进度)
                watchProgress = watchProgress.multiply(BigDecimal.valueOf(100));
                record.setWatckProgress(Integer.valueOf(watchProgress.intValue()));
                if (record.getWatckProgress() > 100) {
                    record.setWatckProgress(100);
                }
            }

            // 处理学习进度-结束
            courseLogDao.save(record);

            // 处理课程学习时长-学习进度
            BigDecimal studyLength = courseLogDao.studyLengthByUserNoAndCourseId(record.getUserNo(), courseChapterPeriodVO.getCourseId());
            UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();
            userOrderCourseRefQO.setUserNo(record.getUserNo());
            userOrderCourseRefQO.setRefId(courseChapterPeriodVO.getCourseId());
            userOrderCourseRefQO.setLastStudyPeriodId(courseChapterPeriodVO.getId());
            userOrderCourseRefQO.setStudyLength(studyLength);
            userOrderCourseRefQO.setCourseType(CourseTypeEnum.COURSE.getCode());
            feignUserOrderCourseRef.updateByUserNoAndRefId(userOrderCourseRefQO);
            // 刪除缓存
            myRedisTemplate.delete(key);
        }
        return 1;
    }

    private Long getaLong(CourseLogBO courseLogBO) {
        Date a = DateUtil.parseDate(courseLogBO.getStartStudyData(), "yyyy-MM-dd HH:mm:ss");
        Date b = DateUtil.parseDate(courseLogBO.getData(), "yyyy-MM-dd HH:mm:ss");
        return (b.getTime() - a.getTime()) / 1000;
    }

    private CourseChapterPeriodVO getCourseChapterPeriodVO(CourseLogBO courseLogBO) {
        CourseChapterPeriodQO qo = new CourseChapterPeriodQO();
        qo.setId(courseLogBO.getPeriodId());
        return feignCourseChapterPeriod.getByCourseChapterPeriodId(qo);
    }
}
