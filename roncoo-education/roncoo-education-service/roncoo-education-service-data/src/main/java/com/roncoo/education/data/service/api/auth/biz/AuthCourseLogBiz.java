package com.roncoo.education.data.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.data.common.bo.*;
import com.roncoo.education.data.common.dto.CourseLogPageDTO;
import com.roncoo.education.data.common.dto.CourseLogProgressDTO;
import com.roncoo.education.data.common.dto.CourseLogProgressListDTO;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 课程日志
 *
 * @author wujing
 */
@Slf4j
@Component
public class AuthCourseLogBiz extends DataBaseBiz {

    @Autowired
    private CourseLogDao dao;
    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    private static final String FONT_PATH = "/opt/simsun.ttf";
   // private static final String IMAGE = "/opt/zhuoyi.jpg";

    public Result<Integer> save(CourseLogSaveBO courseLogSaveBO) {
        if (courseLogSaveBO.getPeriodId() == null) {
            return Result.error("课时ID不能为空");
        }
        if (StringUtils.isEmpty(courseLogSaveBO.getWatchLength())) {
            return Result.error("课时视频观看时长不能为空");
        }

        CourseLogBO courseLogBO = new CourseLogBO();
        courseLogBO.setUserNo(ThreadContext.userNo());

        courseLogBO.setPeriodId(courseLogSaveBO.getPeriodId());
        courseLogBO.setWatchLength(courseLogSaveBO.getWatchLength());
        courseLogBO.setData(DateUtil.getDateTime(new Date()));

        String key = RedisPreEnum.PERIOD_WATCK_PROGRESS.getCode().concat(courseLogBO.getUserNo().toString() + courseLogSaveBO.getPeriodId().toString());
        if (!myRedisTemplate.hasKey(key)) {
            // 第一次进入
            UserExtVO userExtVO = getUserExtVO();
            if (ObjectUtil.isNull(userExtVO)) {
                return Result.error("找不到用户信息");
            }
            courseLogBO.setMobile(userExtVO.getMobile());
            courseLogBO.setBiggestWatchLength(courseLogBO.getWatchLength());
            courseLogBO.setStartStudyData(DateUtil.getDateTime(new Date()));
            myRedisTemplate.setByJson(key, courseLogBO, 1, TimeUnit.HOURS);
            return Result.success(1);
        }

        // 当次观看非第一次进入
        CourseLogBO hashMap = myRedisTemplate.getForJson(key, CourseLogBO.class);
        courseLogBO.setMobile(hashMap.getMobile());
        // 首次观看时间
        courseLogBO.setStartStudyData(hashMap.getStartStudyData());
        if (courseLogBO.getWatchLength().compareTo(hashMap.getBiggestWatchLength()) == -1) {
            // 当前观看时长 < 上次观看时长
            courseLogBO.setBiggestWatchLength(hashMap.getBiggestWatchLength());
        } else {
            // 当前观看时长 > 上次观看时长
            courseLogBO.setBiggestWatchLength(courseLogBO.getWatchLength());
        }
        myRedisTemplate.setByJson(key, courseLogBO, 1, TimeUnit.HOURS);
        return Result.success(1);
    }

    public Result<Page<CourseLogPageDTO>> list(CourseLogPageBO bo) {
        Page<CourseLog> page = dao.page(ThreadContext.userNo(), null, null, bo.getBeginTime(), bo.getEndTime(), bo.getPageCurrent(), bo.getPageSize());
        Page<CourseLogPageDTO> respPage = PageUtil.transform(page, CourseLogPageDTO.class);
        for (CourseLogPageDTO resp : respPage.getList()) {
            CourseLog courseLog = dao.getByUserNoAndPeriodIdLatest(resp.getUserNo(), resp.getPeriodId());
            if (ObjectUtil.isNotNull(courseLog) && courseLog.getGmtCreate() != null) {
                resp.setGmtCreate(courseLog.getGmtCreate());
            }
            if (StringUtils.isEmpty(resp.getWatckProgress())) {
                resp.setWatckProgress(0);
            }
            if (resp.getBiggestWatchLength() != null) {
                resp.setStringWatchLength(cn.hutool.core.date.DateUtil.secondToTime(resp.getBiggestWatchLength().intValue()));
            } else {
                resp.setStringWatchLength("00:00:00");
            }
        }
        return Result.success(respPage);
    }

    public void export(HttpServletResponse response, AuthCourseLogExportBO bo) throws IOException, DocumentException {
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("获取不到用户信息");
        }
        try {
            // 设置强制下载不打开
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            // 设置文件名
            String name;
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                name = userExtVO.getMobile();
            } else {
                name = userExtVO.getNickname();
            }
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(name + "学习记录", "utf-8") + ".pdf");
        } catch (UnsupportedEncodingException e) {
            logger.error("导出文件失败", e);
        }
        exportPDF(response, bo, userExtVO);
    }

    private void exportPDF(HttpServletResponse response, AuthCourseLogExportBO bo, UserExtVO userExtVO) throws IOException, DocumentException {
        List<CourseLog> list = dao.listByUserNoAndTimeGroupByPeriodId(ThreadContext.userNo(), bo.getBeginTime(), bo.getEndTime());
        //1.打开文档并设置基本属性
        Document document = new Document();
        //2.通过流将pdf实例写出到浏览器
        PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
        writer.setViewerPreferences(PdfWriter.PageModeUseThumbs);
        writer.setPageSize(PageSize.A4);

        // 首先打开文档
        document.open();

        //标题自提
        Font f = FontFactory.getFont(FONT_PATH, BaseFont.IDENTITY_H, 27);
        String name = "";
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            name = userExtVO.getMobile();
        } else {
            name = userExtVO.getNickname();
        }
        Paragraph p = new Paragraph(name + "学习记录清单", f);
        p.setAlignment(Element.ALIGN_CENTER);
        document.add(p);

        Font f1 = FontFactory.getFont(FONT_PATH, BaseFont.IDENTITY_H, 15);
        Paragraph p1 = new Paragraph(name + "学习记录清单—打印时间:" + DateUtil.format(new DateTime(), "yyyy-MM-dd HH:mm:ss"), f1);
        p1.setAlignment(Element.ALIGN_LEFT);
        p1.setSpacingBefore(12f);
        document.add(p1);

        Paragraph p2 = new Paragraph("身份证号:" + userExtVO.getIdCardNo(), f1);
        p2.setAlignment(Element.ALIGN_LEFT);
        p2.setSpacingBefore(11f);
        document.add(p2);

        //设置表格字体
        Font f2 = FontFactory.getFont(FONT_PATH, BaseFont.IDENTITY_H);
        // 添加表格，4列
        PdfPTable table = new PdfPTable(6);
        // 设置表格宽度比例为%100
        table.setWidthPercentage(100);
        // 设置表格的宽度
        table.setTotalWidth(500);
        table.setSpacingBefore(14f);
        // 也可以每列分别设置宽度
        table.setTotalWidth(new float[]{225, 125, 55, 55, 52, 68});
        // 设置表格默认为无边框
        table.getDefaultCell().setBorder(0);

   /*     //插入图片
        byte[] bt = FileUtils.readFileToByteArray(new File(IMAGE));
        Image image = Image.getInstance(bt);
        int x = 25;
        int y = 16;
        image.setAbsolutePosition(x + document.leftMargin(), PageSize.A4.getHeight() - y - image.getHeight() - document.topMargin());
        document.add(image);*/

        //设置表格头部
        PdfPCell cell1 = new PdfPCell(new Paragraph("课程名称", f2));
        table.addCell(cell1);
        PdfPCell cell2 = new PdfPCell(new Paragraph("学习内容", f2));
        table.addCell(cell2);
        PdfPCell cell4 = new PdfPCell(new Paragraph("课时时长", f2));
        table.addCell(cell4);
        PdfPCell cell3 = new PdfPCell(new Paragraph("学习时长", f2));
        table.addCell(cell3);
        PdfPCell cell11 = new PdfPCell(new Paragraph("学习进度", f2));
        table.addCell(cell11);
        PdfPCell cell5 = new PdfPCell(new Paragraph("最近学习时间", f2));
        table.addCell(cell5);

        if (CollectionUtil.isEmpty(list)) {
            document.close();
            writer.close();
            return;
        }

        for (CourseLog log : list) {
            PdfPCell cell6 = new PdfPCell(new Paragraph(log.getCourseName(), f2));
            table.addCell(cell6);
            PdfPCell cell7 = new PdfPCell(new Paragraph(log.getPeriodName(), f2));
            table.addCell(cell7);
            PdfPCell cell9;
            if (!org.apache.commons.lang.StringUtils.isEmpty(log.getVidLength())) {
                cell9 = new PdfPCell(new Paragraph(log.getVidLength()));
                //PdfPCell cell9 = new PdfPCell(new Paragraph(log.getVidLength().replaceFirst(":", "时").replaceFirst(":", "分").concat("秒").replace("00时", "").replace("00分", "").replace("00时", ""), f2));
            } else {
                cell9 = new PdfPCell(new Paragraph("", f2));
            }
            table.addCell(cell9);

            /*if (StringUtils.hasText(log.getDuration())){
                if (log.getDuration().contains(".")) {
                    int indexOf = log.getDuration().indexOf(".");
                    log.setDuration(cn.hutool.core.date.DateUtil.secondToTime(Integer.valueOf(log.getDuration().substring(0, indexOf))));
                }
                if (log.getDuration() == "0") {
                    PdfPCell cell8 = new PdfPCell(new Paragraph("", f2));
                    table.addCell(cell8);
                }else {
                    String time = cn.hutool.core.date.DateUtil.formatBetween(Long.valueOf(log.getDuration()), BetweenFormater.Level.SECOND);
                    PdfPCell cell8 = new PdfPCell(new Paragraph(time, f2));
                    table.addCell(cell8);
                }
            }else */
            if (StringUtils.hasText(log.getVidLength())) {
                BigDecimal progress = new BigDecimal(log.getWatckProgress());
                int duration = cn.hutool.core.date.DateUtil.timeToSecond(log.getVidLength());
                BigDecimal studyLength = BigDecimal.valueOf(duration).multiply(progress.divide(BigDecimal.valueOf(100)));
                String length = cn.hutool.core.date.DateUtil.secondToTime(studyLength.intValue());
                PdfPCell cell8 = new PdfPCell(new Paragraph(length));
                //PdfPCell cell8 = new PdfPCell(new Paragraph(length.replaceFirst(":", "时").replaceFirst(":", "分").concat("秒").replace("00时", "").replace("00分", "").replace("00时", ""), f2));
                table.addCell(cell8);
            } else {
                PdfPCell cell8 = new PdfPCell(new Paragraph("", f2));
                table.addCell(cell8);
            }

            PdfPCell cell12;
            if (log.getWatckProgress() == null) {
                cell12 = new PdfPCell(new Paragraph("", f2));
            } else {
                if (log.getWatckProgress() > 100) {
                    log.setWatckProgress(100);
                }
                cell12 = new PdfPCell(new Paragraph(String.valueOf(log.getWatckProgress()).concat("%"), f2));
            }
            table.addCell(cell12);

            CourseLog courseLog = dao.getByUserNoAndPeriodIdLatest(log.getUserNo(), log.getPeriodId());
            if (ObjectUtil.isNotNull(courseLog) && courseLog.getGmtCreate() != null) {
                PdfPCell cell10 = new PdfPCell(new Paragraph(DateUtil.format(courseLog.getGmtCreate(), "yyyy-MM-dd"), f2));
                table.addCell(cell10);
            } else {
                PdfPCell cell10 = new PdfPCell(new Paragraph(DateUtil.format(log.getGmtCreate(), "yyyy-MM-dd"), f2));
                table.addCell(cell10);
            }
        }
        document.add(table);
        document.close();
        writer.close();
    }

    public Result<CourseLogProgressDTO> periodProgressList(CourseLogProgressBO bo) {
        CourseLogProgressDTO dto = new CourseLogProgressDTO();
        if (bo.getCourseId() == null) {
            return Result.error("课时ID不能为空");
        }
        List<CourseLog> list = dao.listByUserNoAndCourseIdByPeriodId(ThreadContext.userNo(), bo.getCourseId());
        if (CollectionUtil.isEmpty(list)) {
            return Result.success(dto);
        }
        List<CourseLogProgressListDTO> listDTO = ArrayListUtil.copy(list, CourseLogProgressListDTO.class);
        for (CourseLogProgressListDTO dto1 : listDTO) {
            if (null != dto1.getWatckProgress() && dto1.getWatckProgress() > 100) {
                dto1.setWatckProgress(100);
            }
        }
        dto.setList(listDTO);
        return Result.success(dto);
    }
}
