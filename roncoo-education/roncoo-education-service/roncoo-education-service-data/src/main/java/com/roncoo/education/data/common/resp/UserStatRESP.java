package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserStatRESP", description="用户统计")
public class UserStatRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户统计集合")
    private List<UserStatListRESP> list;
}
