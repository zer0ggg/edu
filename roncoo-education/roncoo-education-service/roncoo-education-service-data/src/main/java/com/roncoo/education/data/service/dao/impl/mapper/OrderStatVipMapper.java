package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVip;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatVipExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderStatVipMapper {
    int countByExample(OrderStatVipExample example);

    int deleteByExample(OrderStatVipExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderStatVip record);

    int insertSelective(OrderStatVip record);

    List<OrderStatVip> selectByExample(OrderStatVipExample example);

    OrderStatVip selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderStatVip record, @Param("example") OrderStatVipExample example);

    int updateByExample(@Param("record") OrderStatVip record, @Param("example") OrderStatVipExample example);

    int updateByPrimaryKeySelective(OrderStatVip record);

    int updateByPrimaryKey(OrderStatVip record);
}
