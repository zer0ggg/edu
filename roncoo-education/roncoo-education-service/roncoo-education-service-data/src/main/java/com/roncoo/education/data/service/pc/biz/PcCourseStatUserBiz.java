package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.CourseStatUserListREQ;
import com.roncoo.education.data.common.resp.CourseStatUserListRESP;
import com.roncoo.education.data.service.dao.CourseStatUserDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 课程用户统计
 *
 * @author wujing
 */
@Component
public class PcCourseStatUserBiz extends BaseBiz {

    @Autowired
    private CourseStatUserDao dao;

    /**
    * 课程用户统计列表
    *
    * @param req 课程用户统计分页查询参数
    * @return 课程用户统计分页查询结果
    */
    public Result<Page<CourseStatUserListRESP>> list(CourseStatUserListREQ req) {
        CourseStatUserExample example = new CourseStatUserExample();
        Criteria c = example.createCriteria();
        if (req.getUserNo() != null) {
            c.andUserNoEqualTo(req.getUserNo());
        }
        if (StringUtils.hasText(req.getMobile())) {
            c.andMobileLike(PageUtil.like(req.getMobile()));
        }
        Page<CourseStatUser> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseStatUserListRESP> respPage = PageUtil.transform(page, CourseStatUserListRESP.class);
        for (CourseStatUserListRESP resp : respPage.getList()) {
            if (StringUtils.hasText(resp.getWatchLength()) && resp.getWatchLength().contains(".")) {
                int indexOf = resp.getWatchLength().indexOf(".");
                resp.setWatchLength(cn.hutool.core.date.DateUtil.secondToTime(Integer.parseInt(resp.getWatchLength().substring(0, indexOf))));
            }
        }
        return Result.success(respPage);
    }
}
