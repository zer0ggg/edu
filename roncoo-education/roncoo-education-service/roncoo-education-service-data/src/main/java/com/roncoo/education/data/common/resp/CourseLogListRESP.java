package com.roncoo.education.data.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 课程日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseLogListRESP", description="课程日志列表")
public class CourseLogListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "学习时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "用户编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userNo;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "课程类型")
    private Integer courseCategory;

    @ApiModelProperty(value = "课时ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long periodId;

    @ApiModelProperty(value = "课时名称")
    private String periodName;

    @ApiModelProperty(value = "章节ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long chapterId;

    @ApiModelProperty(value = "章节名称")
    private String chapterName;

    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "视频VID")
    private String vid;

    @ApiModelProperty(value = "课时时长")
    private String vidLength;

    @ApiModelProperty(value = "观看时长")
    private BigDecimal watchLength;

    @ApiModelProperty(value = "历史最大观看时长，格式：s")
    private BigDecimal biggestWatchLength;

    @ApiModelProperty(value = "历史最大观看时长，格式：")
    private String stringBiggestWatchLength;

    @ApiModelProperty(value = "观看总进度")
    private Integer watckProgress = 0;

    @ApiModelProperty(value = "人脸对比结果")
    private String contrastResult;
}
