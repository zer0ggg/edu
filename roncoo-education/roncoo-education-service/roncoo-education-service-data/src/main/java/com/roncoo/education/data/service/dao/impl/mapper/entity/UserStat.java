package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class UserStat implements Serializable {
    private Long id;

    private Date gmtCreate;

    private String newUserNum;

    private String loginNum;

    private String loginSum;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getNewUserNum() {
        return newUserNum;
    }

    public void setNewUserNum(String newUserNum) {
        this.newUserNum = newUserNum == null ? null : newUserNum.trim();
    }

    public String getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(String loginNum) {
        this.loginNum = loginNum == null ? null : loginNum.trim();
    }

    public String getLoginSum() {
        return loginSum;
    }

    public void setLoginSum(String loginSum) {
        this.loginSum = loginSum == null ? null : loginSum.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", newUserNum=").append(newUserNum);
        sb.append(", loginNum=").append(loginNum);
        sb.append(", loginSum=").append(loginSum);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}