package com.roncoo.education.data.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="OrderStatCourserTodayListRESP", description="订单课程统计")
public class OrderStatCourserTodayListRESP implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "课程名称")
    private String name;

    @ApiModelProperty(value = "课程数量")
    private Integer value = 0;

    @ApiModelProperty(value = "课程数量")
    private Integer courseNum = 0;

    @ApiModelProperty(value = "课程收入")
    private BigDecimal courseIncome = BigDecimal.ZERO;

    @ApiModelProperty(value = "课程利润")
    private BigDecimal courseProfit = BigDecimal.ZERO;
}
