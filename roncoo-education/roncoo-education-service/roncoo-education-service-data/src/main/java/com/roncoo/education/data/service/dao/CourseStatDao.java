package com.roncoo.education.data.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatExample;

import java.util.Date;

/**
 * 课程统计 服务类
 *
 * @author wujing
 * @date 2020-06-29
 */
public interface CourseStatDao {

    /**
    * 保存课程统计
    *
    * @param record 课程统计
    * @return 影响记录数
    */
    int save(CourseStat record);

    /**
    * 根据ID删除课程统计
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 课程统计
    * @return 影响记录数
    */
    int updateById(CourseStat record);

    /**
    * 根据ID获取课程统计
    *
    * @param id 主键ID
    * @return 课程统计
    */
    CourseStat getById(Long id);

    /**
    * 课程统计--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<CourseStat> listForPage(int pageCurrent, int pageSize, CourseStatExample example);

    /**
     * 根据课程ID、时间获取汇总信息
     *
     * @param courseId
     * @param gmtCreate
     * @return
     */
    CourseStat getByByCourseIdAndGmtCreate(Long courseId, Date gmtCreate);
}
