package com.roncoo.education.data.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.common.resp.UserStatListRESP;
import com.roncoo.education.data.common.resp.UserStatRESP;
import com.roncoo.education.data.common.resp.UserStatViewRESP;
import com.roncoo.education.data.service.dao.UserLogDao;
import com.roncoo.education.data.service.dao.UserStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 用户统计
 *
 * @author wujing
 */
@Component
public class PcUserStatBiz extends BaseBiz {
    @Autowired
    private UserStatDao dao;
    @Autowired
    private UserLogDao userLogDao;


    public Result<UserStatViewRESP> view() {
        UserStatViewRESP resp = new UserStatViewRESP();
        Date date = new Date();
        String format = DateUtil.format(date);
        Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        // 当天登录人数(去重)
        Integer loginSum  = userLogDao.loginSumByGmtCreateAndProvince(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), "");
        // 列出当天登录人次
        List<UserLog> loginNum  = userLogDao.loginNumByGmtCreateAndProvince(startgmtCreate, endgmtCreate, "");
        // 当天新增用户
        Integer newUserNum  = userLogDao.newUserNumByGmtCreate(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));

        resp.setGmtCreate(DateUtil.parseDate(format));
        resp.setLoginSum(String.valueOf(loginSum));
        resp.setLoginNum(String.valueOf(loginNum.size()));
        resp.setNewUserNum(String.valueOf(newUserNum));
        return Result.success(resp);
    }
    /**
     * 列出所有统计
     *
     * @return
     */
    public Result<UserStatRESP> listByAll() {
        UserStatRESP resp = new UserStatRESP();
        List<UserStat> list = dao.listByAll();
        resp.setList(ArrayListUtil.copy(list, UserStatListRESP.class));
        return Result.success(resp);
    }
}
