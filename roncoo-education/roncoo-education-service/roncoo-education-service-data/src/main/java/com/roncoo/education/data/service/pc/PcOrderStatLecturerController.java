package com.roncoo.education.data.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.data.common.req.OrderStatLecturerListREQ;
import com.roncoo.education.data.common.req.OrderStatLecturerStatisticalREQ;
import com.roncoo.education.data.common.req.OrderStatLecturerSummaryPageREQ;
import com.roncoo.education.data.common.resp.OrderStatLecturerListRESP;
import com.roncoo.education.data.common.resp.OrderStatLecturerTodayRESP;
import com.roncoo.education.data.common.resp.OrderStatLecturserStatisticalRESP;
import com.roncoo.education.data.service.pc.biz.PcOrderStatLecturerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单讲师统计 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/data/pc/order/stat/lecturer")
@Api(value = "data-订单讲师统计", tags = {"data-订单讲师统计"})
public class PcOrderStatLecturerController {

    @Autowired
    private PcOrderStatLecturerBiz biz;

    @ApiOperation(value = "订单讲师统计列表", notes = "订单讲师统计列表")
    @PostMapping(value = "/list")
    public Result<Page<OrderStatLecturerListRESP>> list(@RequestBody OrderStatLecturerListREQ orderStatLecturerListREQ) {
        return biz.list(orderStatLecturerListREQ);
    }

    @ApiOperation(value = "订单讲师当天统计", notes = "订单讲师当天统计")
    @PostMapping(value = "/today")
    public Result<OrderStatLecturerTodayRESP> today() {
        return biz.today();
    }


    @ApiOperation(value = "订单讲师统计", notes = "订单课程统计")
    @PostMapping(value = "/statistical")
    public Result<OrderStatLecturserStatisticalRESP> statistical(@RequestBody OrderStatLecturerStatisticalREQ req) {
        return biz.statistical(req);
    }


    @ApiOperation(value = "订单课程统计汇总列表", notes = "订单课程统计列表")
    @PostMapping(value = "/summary/list")
    public Result<Page<OrderStatLecturerListRESP>> summaryList(@RequestBody OrderStatLecturerSummaryPageREQ orderStatLecturerSummaryPageREQ) {
        return biz.summaryList(orderStatLecturerSummaryPageREQ);
    }

}
