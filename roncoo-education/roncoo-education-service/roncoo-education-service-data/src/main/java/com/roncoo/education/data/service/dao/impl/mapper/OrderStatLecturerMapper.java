package com.roncoo.education.data.service.dao.impl.mapper;

import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturer;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatLecturerExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderStatLecturerMapper {
    int countByExample(OrderStatLecturerExample example);

    int deleteByExample(OrderStatLecturerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OrderStatLecturer record);

    int insertSelective(OrderStatLecturer record);

    List<OrderStatLecturer> selectByExample(OrderStatLecturerExample example);

    OrderStatLecturer selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OrderStatLecturer record, @Param("example") OrderStatLecturerExample example);

    int updateByExample(@Param("record") OrderStatLecturer record, @Param("example") OrderStatLecturerExample example);

    int updateByPrimaryKeySelective(OrderStatLecturer record);

    int updateByPrimaryKey(OrderStatLecturer record);
}
