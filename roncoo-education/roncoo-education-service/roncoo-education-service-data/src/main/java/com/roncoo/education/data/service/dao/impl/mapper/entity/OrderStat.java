package com.roncoo.education.data.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OrderStat implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Integer orderNum;

    private BigDecimal orderIncome;

    private BigDecimal orderProfit;

    private Integer buyUser;

    private Integer buyNewUser;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public BigDecimal getOrderIncome() {
        return orderIncome;
    }

    public void setOrderIncome(BigDecimal orderIncome) {
        this.orderIncome = orderIncome;
    }

    public BigDecimal getOrderProfit() {
        return orderProfit;
    }

    public void setOrderProfit(BigDecimal orderProfit) {
        this.orderProfit = orderProfit;
    }

    public Integer getBuyUser() {
        return buyUser;
    }

    public void setBuyUser(Integer buyUser) {
        this.buyUser = buyUser;
    }

    public Integer getBuyNewUser() {
        return buyNewUser;
    }

    public void setBuyNewUser(Integer buyNewUser) {
        this.buyNewUser = buyNewUser;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", orderNum=").append(orderNum);
        sb.append(", orderIncome=").append(orderIncome);
        sb.append(", orderProfit=").append(orderProfit);
        sb.append(", buyUser=").append(buyUser);
        sb.append(", buyNewUser=").append(buyNewUser);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}