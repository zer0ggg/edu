package com.roncoo.education.data.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 课程统计
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseStatListREQ", description="课程统计列表")
public class CourseStatListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "日期")
    private LocalDate gmtCreate;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "观看人次")
    private Integer views;

    @ApiModelProperty(value = "观看人数(去重)")
    private Integer sums;

    @ApiModelProperty(value = "课程总时长")
    private String courseLength;

    @ApiModelProperty(value = "学习时长(秒)")
    private String studyLength;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
