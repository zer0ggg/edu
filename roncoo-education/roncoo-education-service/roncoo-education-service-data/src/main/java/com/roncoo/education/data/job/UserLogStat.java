package com.roncoo.education.data.job;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.data.service.dao.UserLogDao;
import com.roncoo.education.data.service.dao.UserStatDao;
import com.roncoo.education.data.service.dao.UserStatProvinceDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.UserStatProvince;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 定时任务-用户登录统计：建议每天凌晨执行
 *
 * @author wuyun
 */
@Slf4j
@Component
public class UserLogStat extends IJobHandler {

    @Autowired
	private UserLogDao userLogDao;
	@Autowired
	private UserStatDao userStatDao;
	@Autowired
	private UserStatProvinceDao userStatProvinceDao;

	@Override
	@XxlJob(value = "userLogJob")
	public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始执行[统计当天登录]");
		try {
			Date date = DateUtil.addDate(new Date(), -1);
			handleScheduledTasks(date);
			// 根据省份统计
			handleScheduledTasksUserStatProvince(date);
		} catch (Exception e) {
			log.error("定时任务-统计当天登录-执行出错", e);
			return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[统计当天登录]--处理异常");
		}
        XxlJobLogger.log("结束执行[统计当天登录]");
		return SUCCESS;
	}

	private void handleScheduledTasks(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");

		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		// 当天登录人数(去重)
		Integer loginSum  = userLogDao.loginSumByGmtCreateAndProvince(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), "");
		// 列出当天登录人次
		List<UserLog> loginNum  = userLogDao.loginNumByGmtCreateAndProvince(startgmtCreate, endgmtCreate, "");
		// 当天新增用户
		Integer newUserNum  = userLogDao.newUserNumByGmtCreate(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
		// 获取当天记录
		UserStat userStat = userStatDao.getByGmtCreate(DateUtil.parseDate(format));

		UserStat record = new UserStat();
		record.setLoginNum(String.valueOf(loginNum.size()));
		record.setLoginSum(String.valueOf(loginSum));
		record.setNewUserNum(String.valueOf(newUserNum));
		// 如果当天不存在该记录, 新添加
		if (ObjectUtil.isNull(userStat)) {
			record.setGmtCreate(DateUtil.parseDate(format));
			userStatDao.save(record);
		} else {
			record.setId(userStat.getId());
			userStatDao.updateById(record);
		}
		return;
	}

	private void handleScheduledTasksUserStatProvince(Date date) {
		String format = DateUtil.format(date);
		Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		// 列出当天登录的省份
		List<String> provinceList  = userLogDao.listByGmtCreateProvince(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
		if (provinceList.isEmpty()) {
			return;
		}
		for (String province: provinceList) {
			// 当天、省份登录人数(去重)
			Integer loginSum  = userLogDao.loginSumByGmtCreateAndProvince(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate), province);
			// 列出当天、省份登录人次
			List<UserLog> loginNum  = userLogDao.loginNumByGmtCreateAndProvince(startgmtCreate, endgmtCreate, province);
			// 获取当天省份记录
			UserStatProvince userStatProvince = userStatProvinceDao.getByGmtCreateAndProvince(DateUtil.parseDate(format), province);

			UserStatProvince record = new UserStatProvince();
			record.setProvince(province);
			record.setLoginNum(String.valueOf(loginNum.size()));
			record.setLoginSum(String.valueOf(loginSum));
			// 如果当天不存在该记录, 新添加
			if (ObjectUtil.isNull(userStatProvince)) {
				record.setGmtCreate(DateUtil.parseDate(format));
				userStatProvinceDao.save(record);
			} else {
				record.setId(userStatProvince.getId());
				userStatProvinceDao.updateById(record);
			}
		}
		return;
	}

}
