package com.roncoo.education.data.job;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.data.service.dao.CourseLogDao;
import com.roncoo.education.data.service.dao.CourseStatDao;
import com.roncoo.education.data.service.dao.CourseStatUserDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseLog;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.CourseStatUser;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 定时任务-学习汇总统计：建议每天凌晨执行
 *
 * @author wuyun
 */
@Slf4j
@Component
public class CourseStatLog extends IJobHandler {
    @Autowired
    private CourseLogDao courseLogDao;
    @Autowired
    private CourseStatDao courseStatDao;
    @Autowired
    private CourseStatUserDao courseStatUserDao;

    @Autowired
    private IFeignCourse feignCourse;

    @Override
    @XxlJob(value = "courseStatLog")
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始执行[用户学习记录]");
        try {
            handleScheduledTasks();
        } catch (Exception e) {
            log.error("定时任务-用户学习记录-执行出错", e);
            return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[用户学习记录]--处理异常");
        }
        XxlJobLogger.log("结束执行[用户学习记录]");
        return SUCCESS;
    }

    private void handleScheduledTasks() {
        Date date = DateUtil.addDate(new Date(), -1);
        String format = DateUtil.format(date);
        Date startgmtCreate = DateUtil.parseDate(format + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endgmtCreate = DateUtil.parseDate(format + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
        // 获取当天课程学习记录（去重）
        List<CourseLog> list = courseLogDao.listByGmtCreate(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
        for (CourseLog courseLog : list) {
            CourseVO vo = feignCourse.getByCourseId(courseLog.getCourseId());
            if (ObjectUtil.isNotNull(vo)) {
                // 根据课程ID汇总当天课程学习观看人次
                Integer views = courseLogDao.viewsByCourseIdAndGmtCreate(courseLog.getCourseId(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
                // 根据课程ID获取当天课程学习观看人数(去重)
                Integer sums = courseLogDao.sumsByCourseIdAndGmtCreate(courseLog.getCourseId(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
                // 根据课程ID获取当天课程学习学习时长(秒)
                String studyLength = courseLogDao.studyLengthByCourseIdAndGmtCreate(courseLog.getCourseId(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
                CourseStat record = new CourseStat();
                record.setCourseName(courseLog.getCourseName());
                record.setViews(views);
                record.setSums(sums);
                record.setCourseLength(vo.getVideoLength());
                record.setStudyLength(studyLength);
                // 根据课程ID、时间获取汇总信息
                CourseStat courseStat = courseStatDao.getByByCourseIdAndGmtCreate(courseLog.getCourseId(), DateUtil.parseDate(format));
                if (ObjectUtil.isNotNull(courseStat)) {
                    record.setId(courseStat.getId());
                    courseStatDao.updateById(record);
                } else {
                    record.setGmtCreate(DateUtil.parseDate(format));
                    record.setCourseId(courseLog.getCourseId());
                    record.setCourseCategory(vo.getCourseCategory());
                    courseStatDao.save(record);
                }
            }
        }
        // 列出当天学习用户，去重
        List<CourseLog> listUserNo = courseLogDao.listUserNoByGmtCreate(DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
        for (CourseLog courseLog : listUserNo) {
            // 根据用户编号获取当天课程学习学习时长(秒)
            String watchLength = courseLogDao.watchLengthByUserNoAndGmtCreate(courseLog.getUserNo(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            // 根据用户编号获取当天学习课程数
            Integer watchCourseSums = courseLogDao.watchCourseSumsByUserNoAndGmtCreate(courseLog.getUserNo(), DateUtil.getDateTime(startgmtCreate), DateUtil.getDateTime(endgmtCreate));
            // 根据用户编号、时间获取用户学习汇总信息
            CourseStatUser courseStatUser = courseStatUserDao.getByByUserNoAndGmtCreate(courseLog.getUserNo(), DateUtil.parseDate(format));
            CourseStatUser record = new CourseStatUser();
            record.setWatchLength(watchLength);
            record.setWatchCourseSums(watchCourseSums);
            if (ObjectUtil.isNotNull(courseStatUser)) {
                record.setId(courseStatUser.getId());
                courseStatUserDao.updateById(record);
            } else {
                record.setGmtCreate(DateUtil.parseDate(format));
                record.setUserNo(courseLog.getUserNo());
                record.setMobile(courseLog.getMobile());
                courseStatUserDao.save(record);
            }
        }
    }
}
