package com.roncoo.education.data.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 课程日志
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseLogProgressListDTO", description = "课程日志")
public class CourseLogProgressListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课时ID")
    private Long periodId;

    @ApiModelProperty(value = "观看时长")
    private BigDecimal watchLength;

    @ApiModelProperty(value = "观看进度（string类型）")
    private Integer watckProgress;
}
