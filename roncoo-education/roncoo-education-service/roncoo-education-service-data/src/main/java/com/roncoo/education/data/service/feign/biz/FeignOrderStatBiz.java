package com.roncoo.education.data.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.qo.OrderStatQO;
import com.roncoo.education.data.feign.vo.OrderStatVO;
import com.roncoo.education.data.service.dao.OrderStatDao;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStat;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatExample;
import com.roncoo.education.data.service.dao.impl.mapper.entity.OrderStatExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单统计
 *
 * @author wujing
 */
@Component
public class FeignOrderStatBiz extends BaseBiz {

    @Autowired
    private OrderStatDao dao;

	public Page<OrderStatVO> listForPage(OrderStatQO qo) {
	    OrderStatExample example = new OrderStatExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<OrderStat> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, OrderStatVO.class);
	}

	public int save(OrderStatQO qo) {
		OrderStat record = BeanUtil.copyProperties(qo, OrderStat.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public OrderStatVO getById(Long id) {
		OrderStat record = dao.getById(id);
		return BeanUtil.copyProperties(record, OrderStatVO.class);
	}

	public int updateById(OrderStatQO qo) {
		OrderStat record = BeanUtil.copyProperties(qo, OrderStat.class);
		return dao.updateById(record);
	}

}
