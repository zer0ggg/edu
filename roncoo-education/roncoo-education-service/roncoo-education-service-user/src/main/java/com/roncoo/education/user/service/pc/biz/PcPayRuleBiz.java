package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.PayRuleEditREQ;
import com.roncoo.education.user.common.req.PayRuleListREQ;
import com.roncoo.education.user.common.req.PayRuleSaveREQ;
import com.roncoo.education.user.common.req.PayRuleUpdateStatusREQ;
import com.roncoo.education.user.common.resp.PayRuleListRESP;
import com.roncoo.education.user.common.resp.PayRuleViewRESP;
import com.roncoo.education.user.service.dao.PayRuleDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 支付路由
 *
 * @author wujing
 */
@Component
public class PcPayRuleBiz extends BaseBiz {

    @Autowired
    private PayRuleDao dao;

    /**
     * 支付路由列表
     *
     * @param payRuleListREQ 支付路由分页查询参数
     * @return 支付路由分页查询结果
     */
    public Result<Page<PayRuleListRESP>> list(PayRuleListREQ payRuleListREQ) {
        PayRuleExample example = new PayRuleExample();
        Criteria c = example.createCriteria();
        if (StrUtil.isNotEmpty(payRuleListREQ.getPayChannelName())) {
            c.andPayChannelNameLike(PageUtil.like(payRuleListREQ.getPayChannelName()));
        }
        example.setOrderByClause("channel_priority asc, id desc");
        Page<PayRule> page = dao.listForPage(payRuleListREQ.getPageCurrent(), payRuleListREQ.getPageSize(), example);
        Page<PayRuleListRESP> respPage = PageUtil.transform(page, PayRuleListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 支付路由添加
     *
     * @param payRuleSaveREQ 支付路由
     * @return 添加结果
     */
    public Result<String> save(PayRuleSaveREQ payRuleSaveREQ) {
        PayRule record = BeanUtil.copyProperties(payRuleSaveREQ, PayRule.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 支付路由查看
     *
     * @param id 主键ID
     * @return 支付路由
     */
    public Result<PayRuleViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), PayRuleViewRESP.class));
    }


    /**
     * 支付路由修改
     *
     * @param payRuleEditREQ 支付路由修改对象
     * @return 修改结果
     */
    public Result<String> edit(PayRuleEditREQ payRuleEditREQ) {
        PayRule record = BeanUtil.copyProperties(payRuleEditREQ, PayRule.class);
        record.setGmtModified(new Date());
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 支付路由删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 修改状态
     *
     * @param payRuleUpdateStatusREQ 支付路由修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(PayRuleUpdateStatusREQ payRuleUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(payRuleUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(payRuleUpdateStatusREQ.getId()))) {
            return Result.error("路由不存在");
        }
        PayRule record = BeanUtil.copyProperties(payRuleUpdateStatusREQ, PayRule.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
