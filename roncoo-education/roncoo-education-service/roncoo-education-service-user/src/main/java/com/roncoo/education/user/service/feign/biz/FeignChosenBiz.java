package com.roncoo.education.user.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.ChosenQO;
import com.roncoo.education.user.feign.vo.ChosenVO;
import com.roncoo.education.user.service.dao.ChosenDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Chosen;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分销信息
 *
 * @author wujing
 */
@Component
public class FeignChosenBiz extends BaseBiz {

    @Autowired
    private ChosenDao dao;

    public Page<ChosenVO> listForPage(ChosenQO qo) {
        ChosenExample example = new ChosenExample();
        Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<Chosen> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, ChosenVO.class);
    }

    public int save(ChosenQO qo) {
        Chosen record = BeanUtil.copyProperties(qo, Chosen.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public ChosenVO getById(Long id) {
        Chosen record = dao.getById(id);
        return BeanUtil.copyProperties(record, ChosenVO.class);
    }

    public int updateById(ChosenQO qo) {
        Chosen record = BeanUtil.copyProperties(qo, Chosen.class);
        return dao.updateById(record);
    }

    public ChosenVO getByCourseId(Long courseId) {
        Chosen record = dao.getByCourseId(courseId);
        return BeanUtil.copyProperties(record, ChosenVO.class);
    }
}
