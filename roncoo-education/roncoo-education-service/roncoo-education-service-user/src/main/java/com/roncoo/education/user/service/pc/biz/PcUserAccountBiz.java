package com.roncoo.education.user.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.UserAccountEditREQ;
import com.roncoo.education.user.common.req.UserAccountListREQ;
import com.roncoo.education.user.common.req.UserAccountSaveREQ;
import com.roncoo.education.user.common.resp.UserAccountListRESP;
import com.roncoo.education.user.common.resp.UserAccountViewRESP;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户账户信息
 *
 * @author wujing
 */
@Component
public class PcUserAccountBiz extends BaseBiz {

    @Autowired
    private UserAccountDao dao;

    /**
    * 用户账户信息列表
    *
    * @param userAccountListREQ 用户账户信息分页查询参数
    * @return 用户账户信息分页查询结果
    */
    public Result<Page<UserAccountListRESP>> list(UserAccountListREQ userAccountListREQ) {
        UserAccountExample example = new UserAccountExample();
        Criteria c = example.createCriteria();
        Page<UserAccount> page = dao.listForPage(userAccountListREQ.getPageCurrent(), userAccountListREQ.getPageSize(), example);
        Page<UserAccountListRESP> respPage = PageUtil.transform(page, UserAccountListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 用户账户信息添加
    *
    * @param userAccountSaveREQ 用户账户信息
    * @return 添加结果
    */
    public Result<String> save(UserAccountSaveREQ userAccountSaveREQ) {
        UserAccount record = BeanUtil.copyProperties(userAccountSaveREQ, UserAccount.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 用户账户信息查看
    *
    * @param id 主键ID
    * @return 用户账户信息
    */
    public Result<UserAccountViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserAccountViewRESP.class));
    }


    /**
    * 用户账户信息修改
    *
    * @param userAccountEditREQ 用户账户信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(UserAccountEditREQ userAccountEditREQ) {
        UserAccount record = BeanUtil.copyProperties(userAccountEditREQ, UserAccount.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 用户账户信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
