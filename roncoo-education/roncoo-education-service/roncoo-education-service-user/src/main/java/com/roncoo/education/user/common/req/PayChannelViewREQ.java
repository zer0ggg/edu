package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 银行渠道信息表
 * </p>
 *
 * @author wujing
 * @date 2020-03-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayChannelViewREQ", description="银行渠道信息表")
public class PayChannelViewREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

}
