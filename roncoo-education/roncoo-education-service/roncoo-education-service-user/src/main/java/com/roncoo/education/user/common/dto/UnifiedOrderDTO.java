/**
 * Copyright 2015-2016 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 支付申请返回实体类
 * </p>
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UnifiedOrderDTO对象", description = "支付申请返回实体类")
public class UnifiedOrderDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "响应码")
    private String resultCode;

    @ApiModelProperty(value = "支付请求信息，响应码为'0000'时非空，扫码支付返回支付链接")
    private String payMessage;

    @ApiModelProperty(value = "渠道编号")
    private String payChannelCode;

    @ApiModelProperty(value = "渠道名称")
    private String payChannelName;
}
