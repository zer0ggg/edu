package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class AuthOrderInfoListDTO implements Serializable {

	private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date gmtCreate;

	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long userNo;

	@ApiModelProperty(value = "订单号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long orderNo;

	@ApiModelProperty(value = "产品ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long productId;

	@ApiModelProperty(value = "课程封面")
	private String courseLogo;

	@ApiModelProperty(value = "产品名称")
	private String productName;

	@ApiModelProperty(value = "实付金额")
	@JsonSerialize(using = ToStringSerializer.class)
	private BigDecimal pricePaid;

	@ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
	private Integer payType;

	@ApiModelProperty(value = "订单状态：1待支付，2成功支付，3支付失败，4已关闭")
	private Integer orderStatus;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;
}
