/**
 * Copyright 2015-2016 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserXcxLoginBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;

	private Long timestamp;

	private String sign;

	/**
	 * 客戶端类型（1:PC；2：安卓；3：IOS）
	 */
	@ApiModelProperty(value = "客戶端类型（1:PC；2：安卓；3：IOS; 4:微信小程序）", required = true)
	private Integer clientType = 4;
	/**
	 * 登录IP
	 */
	@ApiModelProperty(value = "登录IP", required = false)
	private String loginIp;
	/**
	 * 国家
	 */
	@ApiModelProperty(value = "国家", required = false)
	private String country = "中国";
	/**
	 * 省
	 */
	@ApiModelProperty(value = "省", required = false)
	private String province;
	/**
	 * 市
	 */
	@ApiModelProperty(value = "市", required = false)
	private String city;
	/**
	 * 浏览器
	 */
	@ApiModelProperty(value = "浏览器", required = false)
	private String browser;
	/**
	 * 操作系统
	 */
	@ApiModelProperty(value = "操作系统", required = false)
	private String os;


}
