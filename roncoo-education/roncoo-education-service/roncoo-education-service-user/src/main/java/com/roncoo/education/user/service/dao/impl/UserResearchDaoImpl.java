package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserResearchDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserResearchMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearch;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearchExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户调研信息 服务实现类
 *
 * @author wujing
 * @date 2020-10-21
 */
@Repository
public class UserResearchDaoImpl implements UserResearchDao {

    @Autowired
    private UserResearchMapper mapper;

    @Override
    public int save(UserResearch record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserResearch record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserResearch getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserResearch> listForPage(int pageCurrent, int pageSize, UserResearchExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserResearch getByUserNo(Long userNo) {
        UserResearchExample example = new UserResearchExample();
        UserResearchExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        List<UserResearch> list = this.mapper.selectByExampleWithBLOBs(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }


}
