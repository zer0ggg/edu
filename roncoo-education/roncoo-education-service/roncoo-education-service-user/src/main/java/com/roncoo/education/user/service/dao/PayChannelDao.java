package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample;

import java.util.List;

/**
 * 银行渠道信息表 服务类
 *
 * @author wujing
 * @date 2020-03-25
 */
public interface PayChannelDao {

    int save(PayChannel record);

    int deleteById(Long id);

    int updateById(PayChannel record);

    PayChannel getById(Long id);

    Page<PayChannel> listForPage(int pageCurrent, int pageSize, PayChannelExample example);

    PayChannel getByPayChannelCode(String payChannelCode);

    List<PayChannel> listUsable();

}
