package com.roncoo.education.user.common.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销信息
 * </p>
 *
 * @author wujing
 * @date 2020-12-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionDTO", description="分销信息")
public class DistributionDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "层级")
    private Integer floor;

    @ApiModelProperty(value = "是否允许推广下级")
    private Integer isAllowInvitation;

    @ApiModelProperty(value = "推广比例")
    private BigDecimal spreadProfit;

    @ApiModelProperty(value = "邀请比例")
    private BigDecimal inviteProfit;

    @ApiModelProperty(value = "邀请数")
    private Integer agentCount;

    @ApiModelProperty(value = "备注")
    private String remark;
}
