package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsUseTemplateEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.ReferralCodeUtil;
import com.roncoo.education.user.common.req.MsgTemplateEditREQ;
import com.roncoo.education.user.common.req.MsgTemplateListREQ;
import com.roncoo.education.user.common.req.MsgTemplateSaveREQ;
import com.roncoo.education.user.common.resp.MsgTemplateListRESP;
import com.roncoo.education.user.common.resp.MsgTemplateViewRESP;
import com.roncoo.education.user.service.dao.MsgTemplateDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplate;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplateExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplateExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消息模板
 *
 * @author wujing
 */
@Component
public class PcMsgTemplateBiz extends BaseBiz {

    protected static final String MSG_ = "msg_";

    @Autowired
    private MsgTemplateDao dao;

    /**
     * 消息模板列表
     *
     * @param msgTemplateListREQ 消息模板分页查询参数
     * @return 消息模板分页查询结果
     */
    public Result<Page<MsgTemplateListRESP>> list(MsgTemplateListREQ msgTemplateListREQ) {
        MsgTemplateExample example = new MsgTemplateExample();
        Criteria c = example.createCriteria();
        Page<MsgTemplate> page = dao.listForPage(msgTemplateListREQ.getPageCurrent(), msgTemplateListREQ.getPageSize(), example);
        Page<MsgTemplateListRESP> respPage = PageUtil.transform(page, MsgTemplateListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 消息模板添加
     *
     * @param req 消息模板
     * @return 添加结果
     */
    public Result<String> save(MsgTemplateSaveREQ req) {
        if (req.getMsgTemplateType() == null) {
            return Result.error("请选择消息模板类型");
        }
        if (req.getIsUseTemplate() == null) {
            return Result.error("请选择是否使用消息模板");
        }
        MsgTemplate record = BeanUtil.copyProperties(req, MsgTemplate.class);
        List<MsgTemplate> list = dao.listByMsgTemplateType(req.getMsgTemplateType());
        if (CollectionUtil.isNotEmpty(list)) {
            record.setTemplateCode(list.get(0).getTemplateCode());
        } else {
            record.setTemplateCode(check(MSG_ + ReferralCodeUtil.getStringRandom()));
        }

        if (IsUseTemplateEnum.YES.getCode().equals(req.getIsUseTemplate())) {
            // 如果使用新添加模板更新原来的模板为未使用
            MsgTemplate  msgTemplate = dao.getByMsgTemplateTypeAndIsUseTemplate(req.getMsgTemplateType(), IsUseTemplateEnum.YES.getCode());
            if (ObjectUtil.isNotNull(msgTemplate)) {
                msgTemplate.setIsUseTemplate(IsUseTemplateEnum.NO.getCode());
                dao.updateById(msgTemplate);
            }
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 模板ID校不同类型唯一性
     */
    private String check(String referralCode) {
        // 查询生成的推荐码在数据库是否存在，不存在就返回，存在重新生成再次判断
        MsgTemplate msgTemplate = dao.getByTemplateCode(MSG_ + referralCode);
        if (ObjectUtil.isNull(msgTemplate)) {
            return referralCode;
        }

        ReferralCodeUtil.getStringRandom();
        return check(MSG_ + ReferralCodeUtil.getStringRandom());
    }

    /**
     * 消息模板查看
     *
     * @param id 主键ID
     * @return 消息模板
     */
    public Result<MsgTemplateViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), MsgTemplateViewRESP.class));
    }


    /**
     * 消息模板修改
     *
     * @param req 消息模板修改对象
     * @return 修改结果
     */
    public Result<String> edit(MsgTemplateEditREQ req) {
        MsgTemplate record = BeanUtil.copyProperties(req, MsgTemplate.class);
        if (req.getIsUseTemplate() !=null && IsUseTemplateEnum.YES.getCode().equals(req.getIsUseTemplate())) {
            // 如果使用新添加模板更新原来的模板为未使用
            MsgTemplate  msgTemplate = dao.getByMsgTemplateTypeAndIsUseTemplate(req.getMsgTemplateType(), IsUseTemplateEnum.YES.getCode());
            if (ObjectUtil.isNotNull(msgTemplate)) {
                msgTemplate.setIsUseTemplate(IsUseTemplateEnum.NO.getCode());
                dao.updateById(msgTemplate);
            }
        }
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 消息模板删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
