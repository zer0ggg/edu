package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户账户提现日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserAccountExtractLogSaveREQ", description="用户账户提现日志添加")
public class UserAccountExtractLogSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "银行卡号")
    private String bankCardNo;

    @ApiModelProperty(value = "银行名称")
    private String bankName;

    @ApiModelProperty(value = "银行支行名称")
    private String bankBranchName;

    @ApiModelProperty(value = "银行开户名")
    private String bankUserName;

    @ApiModelProperty(value = "银行身份证号")
    private String bankIdCardNo;

    @ApiModelProperty(value = "提现状态（1申请中，2支付中，3确认中，4成功，5失败）")
    private Integer extractStatus;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal extractMoney;

    @ApiModelProperty(value = "用户收入")
    private BigDecimal userIncome;

    @ApiModelProperty(value = "备注")
    private String remark;
}
