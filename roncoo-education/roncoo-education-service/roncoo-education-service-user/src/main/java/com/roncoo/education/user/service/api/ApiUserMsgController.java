package com.roncoo.education.user.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.user.service.api.biz.ApiUserMsgBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站内信用户记录表
 */
@RestController
@RequestMapping(value = "/user/api/user/msg")
public class ApiUserMsgController extends BaseController {

	@Autowired
	private ApiUserMsgBiz biz;

	/**
	 * 用户查看站内信内容（小程序使用）
	 */
	@ApiOperation(value = "用户查看站内信内容（小程序使用）", notes = "用户查看站内信内容（小程序使用）")
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable(value = "id") Long id) {
		return biz.view(id);
	}
}
