package com.roncoo.education.user.service.feign.biz;


import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.user.feign.qo.UserQO;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.Transient;
import java.math.BigDecimal;
import java.util.Date;

@Component
public class FeignDemoDataAddBiz {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private OrderInfoDao orderInfoDao;


    @Transient
    public int save(UserQO qo) {
        User user = new User();
        user.setMobile(qo.getMobile());
        user.setUserNo(qo.getUserNo());
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + "123456"));
        userDao.save(user);

        // 用户其他信息
        UserExt userExt = new UserExt();
        userExt.setUserNo(user.getUserNo());
        userExt.setUserType(UserTypeEnum.USER.getCode());
        userExt.setMobile(user.getMobile());
        userExtDao.save(userExt);

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUserNo(qo.getUserNo());
        orderInfo.setMobile(qo.getMobile());
        orderInfo.setRegisterTime(new Date());
        orderInfo.setLecturerUserNo(Long.valueOf("2020060514034606"));
        orderInfo.setLecturerName("测试");
        orderInfo.setProductId(Long.valueOf("1273525324477759489"));
        orderInfo.setProductName("软考直播");
        orderInfo.setProductType(ProductTypeEnum.ORDINARY.getCode());
        orderInfo.setPricePayable(BigDecimal.valueOf(10)); //应付金额
        orderInfo.setPricePaid(BigDecimal.valueOf(0)); // 实付金额
        orderInfo.setPriceDiscount(BigDecimal.valueOf(10)); // 优惠金额

        orderInfo.setPlatformIncome(BigDecimal.valueOf(0));
        orderInfo.setLecturerIncome(BigDecimal.valueOf(10));
        orderInfo.setOrderNo(qo.getOrderNo());
        orderInfo.setPlatformIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setIsShowUser(IsShowUserEnum.YES.getCode());
        orderInfo.setTradeType(TradeTypeEnum.ONLINE.getCode());
        orderInfo.setPayType(PayTypeEnum.ALIPAY.getCode());
        orderInfo.setChannelType(ChannelTypeEnum.PC.getCode());
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfo.setSerialNumber(qo.getSerialNumber());
        return orderInfoDao.save(orderInfo);
    }
}
