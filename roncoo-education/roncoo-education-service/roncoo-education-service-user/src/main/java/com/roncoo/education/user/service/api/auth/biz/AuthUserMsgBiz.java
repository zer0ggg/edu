package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsDefaultEnum;
import com.roncoo.education.common.core.enums.IsReadEnum;
import com.roncoo.education.common.core.enums.IsSendEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.MsgReadBO;
import com.roncoo.education.user.common.bo.MsgViewAllBO;
import com.roncoo.education.user.common.bo.MsgViewBO;
import com.roncoo.education.user.common.bo.UserMsgBO;
import com.roncoo.education.user.common.dto.MsgDTO;
import com.roncoo.education.user.common.dto.MsgReadDTO;
import com.roncoo.education.user.common.dto.UserMsgDTO;
import com.roncoo.education.user.service.dao.MsgDao;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Msg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@Component
public class AuthUserMsgBiz {

    @Autowired
    private UserMsgDao dao;
    @Autowired
    private MsgDao msgDao;

    /**
     * 站内信分页列表接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<Page<UserMsgDTO>> list(UserMsgBO bo) {
        UserMsgExample example = new UserMsgExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        if (bo.getIsRead() != null) {
            c.andIsReadEqualTo(bo.getIsRead());
        }
        if (bo.getIsRead() == null) {
            c.andIsSendEqualTo(IsSendEnum.YES.getCode());
        }
        example.setOrderByClause(" is_top desc, status_id desc, sort asc, id desc ");
        Page<UserMsg> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(page, UserMsgDTO.class));
    }

    /**
     * 用户查看站内信内容
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<MsgDTO> readMsg(MsgViewBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        UserMsg record = dao.getById(bo.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("找不到站内信息");
        }
        if (!record.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("找不到站内信息");
        }
        // 未阅读，则刷新阅读状态
        if (IsDefaultEnum.NO.getCode().equals(record.getIsRead())) {
            record.setIsRead(IsDefaultEnum.YES.getCode());
            dao.updateById(record);
        }
        // 返回消息实体
        Msg msg = msgDao.getById(record.getMsgId());
        if (ObjectUtil.isNull(msg)) {
            return Result.error("查询msg有误");
        }
        return Result.success(BeanUtil.copyProperties(msg, MsgDTO.class));
    }

    public Result<String> readAllMsg(MsgViewAllBO bo) {
        dao.readAllByUserNo(ThreadContext.userNo());
        return Result.success("操作成功");
    }

    /**
     * 用户站内信未读条数
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<MsgReadDTO> getNumOfUnReadMsg(MsgReadBO bo) {
        int num = dao.countByUserNoAndIsReadAndIsSend(ThreadContext.userNo(), IsReadEnum.NO.getCode(), IsSendEnum.YES.getCode());
        MsgReadDTO dto = new MsgReadDTO();
        dto.setNum(num);
        return Result.success(dto);
    }

}
