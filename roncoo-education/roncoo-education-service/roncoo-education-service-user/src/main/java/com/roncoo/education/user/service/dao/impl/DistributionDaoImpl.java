package com.roncoo.education.user.service.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.DistributionDao;
import com.roncoo.education.user.service.dao.impl.mapper.DistributionMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Distribution;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample;

import java.util.List;

/**
 * 分销信息 服务实现类
 *
 * @author wujing
 * @date 2020-12-29
 */
@Repository
public class DistributionDaoImpl implements DistributionDao {

    @Autowired
    private DistributionMapper mapper;

    @Override
    public int save(Distribution record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Distribution record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Distribution getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Distribution> listForPage(int pageCurrent, int pageSize, DistributionExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<Distribution> listByFloor(Integer floor) {
        DistributionExample example = new DistributionExample();
        DistributionExample.Criteria c = example.createCriteria();
        c.andFloorNotEqualTo(floor);
        return this.mapper.selectByExample(example);
    }

    @Override
    public Distribution getByUserNo(Long userNo) {
        DistributionExample example = new DistributionExample();
        DistributionExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        List<Distribution> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<Distribution> listByParentId(Long parentId) {
        DistributionExample example = new DistributionExample();
        DistributionExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<Distribution> listByIds(List<Long> ids) {
        DistributionExample example = new DistributionExample();
        DistributionExample.Criteria c = example.createCriteria();
        c.andIdIn(ids);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<Distribution> listByUserNos(List<Long> userNos) {
        DistributionExample example = new DistributionExample();
        DistributionExample.Criteria c = example.createCriteria();
        c.andUserNoIn(userNos);
        return this.mapper.selectByExample(example);
    }

}
