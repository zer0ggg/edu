package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 支付路由
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayRuleSaveREQ", description="支付路由添加")
public class PayRuleSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "支付渠道编码")
    private String payChannelCode;

    @ApiModelProperty(value = "支付渠道名称")
    private String payChannelName;

    @ApiModelProperty(value = "渠道优先等级")
    private Integer channelPriority;

    @ApiModelProperty(value = "参考PayTypeEnum")
    private Integer payType;
}
