package com.roncoo.education.user.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.user.service.dao.UserResearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户调研信息
 *
 * @author wujing
 */
@Component
public class ApiUserResearchBiz extends BaseBiz {

    @Autowired
    private UserResearchDao dao;

}
