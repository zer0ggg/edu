/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户基本信息
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class UserLoginPasswordBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 手机号码
	 */
	@ApiModelProperty(value = "手机号", required = true)
	private String mobile;
	/**
	 * 登录密码
	 */
	@ApiModelProperty(value = "密码", required = true)
	private String password;
	/**
	 * 客戶端类型（1:PC；2：安卓；3：IOS）
	 */
	@ApiModelProperty(value = "客戶端类型（1:PC；2：安卓；3：IOS）", required = true)
	private Integer clientType = 1;
	/**
	 * 登录IP
	 */
	@ApiModelProperty(value = "登录IP", required = false)
	private String loginIp;
	/**
	 * 国家
	 */
	@ApiModelProperty(value = "国家", required = false)
	private String country = "中国";

	/**
	 * 省
	 */
	@ApiModelProperty(value = "省", required = false)
	private String province;
	/**
	 * 市
	 */
	@ApiModelProperty(value = "市", required = false)
	private String city;
	/**
	 * 浏览器
	 */
	@ApiModelProperty(value = "浏览器", required = false)
	private String browser;
	/**
	 * 操作系统
	 */
	@ApiModelProperty(value = "操作系统", required = false)
	private String os;

}
