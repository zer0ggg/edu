package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserExtDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long userNo;
	/**
	 * 用户类型(1用户，2讲师)
	 */
	@ApiModelProperty(value = "用户类型(1用户，2讲师)")
	private Integer userType;
	/**
	 * 手机号码
	 */
	@ApiModelProperty(value = "手机号码")
	private String mobile;
	/**
	 * 性别
	 */
	@ApiModelProperty(value = "性别")
	private Integer sex;
	/**
	 * 年龄
	 */
	@ApiModelProperty(value = "年龄")
	private Integer age;
	/**
	 * 昵称
	 */
	@ApiModelProperty(value = "昵称")
	private String nickname;
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;
	/**
	 * 是否是会员（0：不是，1：是）
	 */
	@ApiModelProperty(value = "是否是会员（0：不是，1：是）")
	private Integer isVip;
	/**
	 * 会员类型(1.年费，2.季度，3.月度)
	 */
	@ApiModelProperty(value = "会员类型(1.年费，2.季度，3.月度)")
	private Integer vipType;
	/**
	 * 过期时间
	 */
	@ApiModelProperty(value = "过期时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date expireTime;
	/**
	 * 推荐码
	 */
	@ApiModelProperty(value = "推荐码")
	private String referralCode;
	/**
	 * 二维码
	 */
	@ApiModelProperty(value = "二维码")
	private String codeUrl;
	/**
	 * 是否是推荐（1：是；0：否）
	 */
	@ApiModelProperty(value = "是否是推荐（1：是；0：否）")
	private Integer isRecommended;
	/**
	 * 推荐二维码url
	 */
	@ApiModelProperty(value = "推荐二维码url")
	private String recommendedCodeUrl;
	/**
	 * 用戶小程序唯一标识
	 */
	private String uniconId;
	/**
	 * 是否允许直播重新上传视频(0:否,1:是)
	 */
	private String isAllowLiveUploadVideo = "0";
	/**
	 * 身份证姓名
	 */
	@ApiModelProperty(value = "身份证姓名")
	private String idCardName;
	/**
	 * 身份证号
	 */
	@ApiModelProperty(value = "身份证号")
	private String idCardNo;
	/**
	 * 身份证正面
	 */
	@ApiModelProperty(value = "身份证正面")
	private String idCardFrontImg;
	/**
	 * 身份证反面
	 */
	@ApiModelProperty(value = "身份证反面")
	private String idCardAfterImg;
	/**
	 * 人脸对比图
	 */
	@ApiModelProperty(value = "人脸对比图")
	private String faceContrasImg;
}
