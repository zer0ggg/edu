package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.AuthSharingBO;
import com.roncoo.education.user.common.bo.AuthUserLogoutBO;
import com.roncoo.education.user.common.bo.auth.*;
import com.roncoo.education.user.common.dto.auth.AuthUserExtDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserExtBiz;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/user/auth/user/ext")
public class AuthUserExtController extends BaseController {

	@Autowired
	private AuthUserExtBiz biz;

	/**
	 * 用户信息查看接口
	 */
	@ApiOperation(value = "用户信息查看接口", notes = "根据userNo获取用户信息接口")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthUserExtDTO> view(@RequestBody AuthUserExtViewBO authUserExtViewBO) {
		return biz.view(authUserExtViewBO);
	}

	/**
	 * 用户信息更新接口
	 */
	@ApiOperation(value = "用户信息更新接口", notes = "用户信息更新接口")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<AuthUserExtDTO> update(@RequestBody AuthUserExtBO authUserExtBO) {
		return biz.update(authUserExtBO);
	}

	/**
	 * 获取小程序推荐码
	 */
	@ApiOperation(value = "获取小程序推荐码", notes = "获取小程序推荐码")
	@RequestMapping(value = "/referral/code", method = RequestMethod.POST)
	public Result<String> referralCode(@RequestBody AuthUserExtReferralCodeBO bo) {
		return biz.referralCode(bo);
	}

	/**
	 * 分享图片生成接口(小程序使用)
	 */
	@ApiOperation(value = "分享图片生成接口(小程序使用)", notes = "分享图片生成接口(小程序使用)")
	@RequestMapping(value = "/sharing/recommended", method = RequestMethod.POST)
	public Result<String> sharingRecommended(@RequestBody AuthSharingBO bo) {
		return biz.sharingRecommended(bo);
	}

	/**
	 * 用户注销
	 */
	@ApiOperation(value = "用户注销", notes = "用户注销")
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public Result<String> logout(@RequestBody AuthUserLogoutBO bo) {
		return biz.logout(bo);
	}

	/**
	 * 人脸对比
	 */
	@ApiOperation(value = "人脸对比", notes = "人脸对比")
	@RequestMapping(value = "/face/contras", method = RequestMethod.POST)
	public Result<String> faceContras(@RequestBody AuthFaceContrasBO authFaceContrasBO) throws Exception {
		return biz.faceContras(authFaceContrasBO);
	}

	/**
	 * pc端跳转小程序人脸对比二维码
	 */
	@ApiOperation(value = "pc端跳转小程序人脸对比二维码", notes = "pc端跳转小程序人脸对比二维码")
	@RequestMapping(value = "/face/contras/code", method = RequestMethod.POST)
	public Result<String> faceContrasCode(@RequestBody AuthFaceContrasCodeBO authFaceContrasCodeBO) throws WxErrorException {
		return biz.faceContrasCode(authFaceContrasCodeBO);
	}

	/**
	 * 人脸对比是否通过校验
	 * @param authFaceContrasPassBO
	 * @return
	 */
	@ApiOperation(value = "人脸对比是否通过校验——Pc端使用", notes = "人脸对比是否通过校验——Pc端使用")
	@RequestMapping(value = "/face/contras/pass", method = RequestMethod.POST)
	public Result<String> faceContrasPass(@RequestBody AuthFaceContrasPassBO authFaceContrasPassBO) {
		return biz.faceContrasPass(authFaceContrasPassBO);
	}

	/**
	 * pc端跳未进行人脸对比标记失败
	 */
	@ApiOperation(value = "pc端跳未进行人脸对比标记失败", notes = "pc端跳未进行人脸对比标记失败")
	@RequestMapping(value = "/face/contras/fail", method = RequestMethod.POST)
	public Result<String> faceContrasFail(@RequestBody AuthFaceContrasFailBO authFaceContrasFailBO) {
		return biz.faceContrasFail(authFaceContrasFailBO);
	}
}
