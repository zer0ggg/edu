package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.DistributionProfitEditREQ;
import com.roncoo.education.user.common.req.DistributionProfitListREQ;
import com.roncoo.education.user.common.req.DistributionProfitSaveREQ;
import com.roncoo.education.user.common.resp.DistributionProfitListRESP;
import com.roncoo.education.user.common.resp.DistributionProfitViewRESP;
import com.roncoo.education.user.service.pc.biz.PcDistributionProfitBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 代理分润记录 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-代理分润记录")
@RestController
@RequestMapping("/user/pc/distribution/profit")
public class PcDistributionProfitController {

    @Autowired
    private PcDistributionProfitBiz biz;

    @ApiOperation(value = "代理分润记录列表", notes = "代理分润记录列表")
    @PostMapping(value = "/list")
    public Result<Page<DistributionProfitListRESP>> list(@RequestBody DistributionProfitListREQ distributionProfitListREQ) {
        return biz.list(distributionProfitListREQ);
    }

    @ApiOperation(value = "代理分润记录添加", notes = "代理分润记录添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody DistributionProfitSaveREQ distributionProfitSaveREQ) {
        return biz.save(distributionProfitSaveREQ);
    }

    @ApiOperation(value = "代理分润记录查看", notes = "代理分润记录查看")
    @GetMapping(value = "/view")
    public Result<DistributionProfitViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "代理分润记录修改", notes = "代理分润记录修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody DistributionProfitEditREQ distributionProfitEditREQ) {
        return biz.edit(distributionProfitEditREQ);
    }

    @ApiOperation(value = "代理分润记录删除", notes = "代理分润记录删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
