package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthOrderInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "下单时间", required = true)
	private Date gmtCreate;

	@ApiModelProperty(value = "订单编号", required = true)
	@JsonSerialize(using = ToStringSerializer.class)
	private Long orderNo;

	@ApiModelProperty(value = "实际支付金额", required = true)
	private BigDecimal pricePaid;

	@ApiModelProperty(value = "支付方式(1:微信支付，2:支付宝支付)", required = true)
	private Integer payType;

	@ApiModelProperty(value = "订单状态(1:待支付，2:支付成功，3:支付失败，4:已关闭)", required = true)
	private Integer orderStatus;

	@ApiModelProperty(value = "支付时间", required = true)
	private Date payTime;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;
}
