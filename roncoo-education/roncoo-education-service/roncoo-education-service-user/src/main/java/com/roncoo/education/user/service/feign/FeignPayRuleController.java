package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignPayRule;
import com.roncoo.education.user.feign.qo.PayRuleQO;
import com.roncoo.education.user.feign.vo.PayRuleVO;
import com.roncoo.education.user.service.feign.biz.FeignPayRuleBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付路由表
 *
 * @author wujing
 * @date 2020-04-02
 */
@RestController
public class FeignPayRuleController extends BaseController implements IFeignPayRule{

    @Autowired
    private FeignPayRuleBiz biz;

	@Override
	public Page<PayRuleVO> listForPage(@RequestBody PayRuleQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody PayRuleQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody PayRuleQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public PayRuleVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
