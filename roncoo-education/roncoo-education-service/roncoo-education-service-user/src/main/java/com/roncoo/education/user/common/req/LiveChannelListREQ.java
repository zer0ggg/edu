package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value = "LiveChannelListREQ", description = "频道列表")
public class LiveChannelListREQ  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:有效;0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "频道id")
    private String channelId;

    @ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏)")
    private String scene;

    @ApiModelProperty(value = "直播状态(1:未开播;2:直播中;3:待生成回放;4:待转存;5:直播结束)")
    private Integer liveStatus;

    @ApiModelProperty(value = "是否被使用(0:未使用,1:已使用)")
    private Integer isUse;

    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    @ApiModelProperty(value = "每页记录数")
    private int pageSize = 20;
}
