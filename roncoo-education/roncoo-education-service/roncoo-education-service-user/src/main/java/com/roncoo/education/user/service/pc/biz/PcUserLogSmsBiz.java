package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.aliyuncs.exceptions.ClientException;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.IsSuccessEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliyunVO;
import com.roncoo.education.user.common.req.UserLogSmsEditREQ;
import com.roncoo.education.user.common.req.UserLogSmsListREQ;
import com.roncoo.education.user.common.req.UserLogSmsSaveREQ;
import com.roncoo.education.user.common.resp.UserLogSmsListRESP;
import com.roncoo.education.user.common.resp.UserLogSmsViewRESP;
import com.roncoo.education.user.feign.vo.UserLogSmsVO;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserLogSmsDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSms;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 用户发送短信日志
 *
 * @author wujing
 */
@Component
public class PcUserLogSmsBiz extends BaseBiz {

    @Autowired
    private UserLogSmsDao dao;

    @Autowired
    private UserDao userDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Autowired
    private IFeignSysConfig feignSysConfig;

    /**
     * 用户发送短信日志列表
     *
     * @param userLogSmsListREQ 用户发送短信日志分页查询参数
     * @return 用户发送短信日志分页查询结果
     */
    public Result<Page<UserLogSmsListRESP>> list(UserLogSmsListREQ userLogSmsListREQ) {
        UserLogSmsExample example = new UserLogSmsExample();
        Criteria c = example.createCriteria();
        // 手机号
        if (ObjectUtil.isNotNull(userLogSmsListREQ.getMobile())) {
            c.andMobileLike(SqlUtil.like(userLogSmsListREQ.getMobile()));
        }
        // 发送时间
        if (StringUtils.hasText(userLogSmsListREQ.getBeginSendTime())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(userLogSmsListREQ.getBeginSendTime(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(userLogSmsListREQ.getEndSendTime())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(userLogSmsListREQ.getEndSendTime(), "yyyy-MM-dd"), 1));
        }
        // 发送状态
        if (ObjectUtil.isNotEmpty(userLogSmsListREQ.getIsSuccess())) {
            c.andIsSuccessEqualTo(userLogSmsListREQ.getIsSuccess());
        }
        example.setOrderByClause(" id desc ");
        Page<UserLogSms> page = dao.listForPage(userLogSmsListREQ.getPageCurrent(), userLogSmsListREQ.getPageSize(), example);
        Page<UserLogSmsListRESP> respPage = PageUtil.transform(page, UserLogSmsListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 用户发送短信统计
     *
     * @return 统计结果
     */
    public Result<UserLogSmsVO> countIncome() {
        UserLogSmsVO vo = new UserLogSmsVO();

        // 查找总发送数
        List<UserLogSms> total = dao.listByAll();
        if (ObjectUtil.isNotNull(total)) {
            vo.setTotal(total.size());
        } else {
            vo.setTotal(0);
        }
        // 查找成功数
        List<UserLogSms> succeedCount = dao.listByIsSuccess(IsSuccessEnum.SUCCESS.getCode());
        if (ObjectUtil.isNotNull(succeedCount)) {
            vo.setSucceedCount(succeedCount.size());
        } else {
            vo.setSucceedCount(0);
        }
        // 查找失败数
        List<UserLogSms> failCount = dao.listByIsSuccess(IsSuccessEnum.FAIL.getCode());
        if (ObjectUtil.isNotNull(failCount)) {
            vo.setFailCount(failCount.size());
        } else {
            vo.setFailCount(0);
        }
        return Result.success(vo);
    }


    /**
     * 用户发送短信日志添加
     *
     * @param userLogSmsSaveREQ 用户发送短信日志
     * @return 添加结果
     */
    public Result<String> save(UserLogSmsSaveREQ userLogSmsSaveREQ) {
        UserLogSms record = BeanUtil.copyProperties(userLogSmsSaveREQ, UserLogSms.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户发送短信日志查看
     *
     * @param id 主键ID
     * @return 用户发送短信日志
     */
    public Result<UserLogSmsViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserLogSmsViewRESP.class));
    }


    /**
     * 用户发送短信日志修改
     *
     * @param userLogSmsEditREQ 用户发送短信日志修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserLogSmsEditREQ userLogSmsEditREQ) {
        UserLogSms record = BeanUtil.copyProperties(userLogSmsEditREQ, UserLogSms.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户发送短信日志删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }


    /**
     * 发送验证码
     *
     * @param mobile 手机号
     * @return 结果
     */
    public Result<String> send(String mobile) {
        if (StringUtils.isEmpty(mobile)) {
            throw new BaseException("手机号不能为空");
        }
        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(mobile).matches()) {
            throw new BaseException("手机号码格式不正确");
        }
        User user = userDao.getByMobile(mobile);
        if (ObjectUtil.isNull(user)) {
            throw new BaseException("用户不存在");
        }
        if (myRedisTemplate.hasKey(Constants.SMS_CODE + mobile)) {
            throw new BaseException("操作过于频繁，请稍后重试（不少于5分钟）");
        }
        ConfigAliyunVO sys = feignSysConfig.getAliyun();
        if (ObjectUtil.isNull(sys)) {
            throw new BaseException("未配置系统配置表");
        }
        if (StringUtils.isEmpty(sys.getAliyunAccessKeyId()) || StringUtils.isEmpty(sys.getAliyunAccessKeySecret())) {
            throw new BaseException("aliyunAccessKeyId或aliyunAccessKeySecret未配置");
        }
        if (StringUtils.isEmpty(sys.getAliyunSmsCode()) || StringUtils.isEmpty(sys.getAliyunSmsSignName())) {
            throw new BaseException("smsCode或signName未配置");
        }
        // 创建日志实例
        UserLogSms userLogSms = new UserLogSms();
        userLogSms.setMobile(mobile);
        userLogSms.setTemplate(sys.getAliyunSmsCode());
        // 随机生成验证码
        userLogSms.setSmsCode(RandomUtil.randomNumbers(6));
        try {
            // 发送验证码
            boolean result = AliyunUtil.sendMsg(mobile, userLogSms.getSmsCode(), BeanUtil.copyProperties(sys, Aliyun.class));
            if (result) {
                // 成功发送，验证码存入缓存：5分钟有效
                myRedisTemplate.set(Constants.SMS_CODE + mobile, userLogSms.getSmsCode(), 5, TimeUnit.MINUTES);
                userLogSms.setIsSuccess(IsSuccessEnum.SUCCESS.getCode());
                dao.save(userLogSms);
                return Result.success("成功发送，5分钟有效");
            }
            // 发送失败
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            dao.save(userLogSms);
            throw new BaseException("发送失败");
        } catch (ClientException e) {
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            dao.save(userLogSms);
            throw new BaseException("发送失败，原因={" + e.getErrMsg() + "}");
        }
    }
}
