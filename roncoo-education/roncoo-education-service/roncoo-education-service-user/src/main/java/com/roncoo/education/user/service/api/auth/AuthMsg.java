package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.MsgViewBO;
import com.roncoo.education.user.common.dto.MsgDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthMsgBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@RestController
@RequestMapping(value = "/user/auth/msg")
public class AuthMsg extends BaseController {

    @Autowired
    private AuthMsgBiz biz;

    /**
     * 课站内信详情
     */
    @ApiOperation(value = "站内信详情", notes = "站内信详情接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<MsgDTO> view(@RequestBody MsgViewBO MsgViewBO) {
        return biz.view(MsgViewBO);
    }
}
