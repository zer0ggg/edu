package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.PayRuleDao;
import com.roncoo.education.user.service.dao.impl.mapper.PayRuleMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 支付路由表 服务实现类
 *
 * @author wujing
 * @date 2020-04-02
 */
@Repository
public class PayRuleDaoImpl implements PayRuleDao {

    @Autowired
    private PayRuleMapper mapper;

    @Override
    public int save(PayRule record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(PayRule record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public PayRule getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<PayRule> listForPage(int pageCurrent, int pageSize, PayRuleExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent((int) count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage((int) count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<PayRule>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<PayRule> listByPayTypeAndStatusIdAndPriorityDesc(Integer payType, Integer statusId) {
        PayRuleExample example = new PayRuleExample();
        example.setOrderByClause("channel_priority asc");
        Criteria c = example.createCriteria();
        c.andPayTypeEqualTo(payType);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public PayRule getByPayChannelCode(String payChannelCode) {
        PayRuleExample example = new PayRuleExample();
        Criteria c = example.createCriteria();
        c.andPayChannelCodeEqualTo(payChannelCode);
        List<PayRule> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

}
