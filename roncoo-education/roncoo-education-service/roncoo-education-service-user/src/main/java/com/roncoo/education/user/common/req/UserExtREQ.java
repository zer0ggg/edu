package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户教育信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExtREQ", description="用户教育信息")
public class UserExtREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户类型(1用户，2讲师，3代理，4讲师（代理）)")
    private Integer userType;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "性别(1男，2女，3保密)")
    private Integer sex;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "头像地址")
    private String headImgUrl;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否是会员（0：不是，1:是）")
    private Integer isVip;

    @ApiModelProperty(value = "会员类型(1.年费，2.季度，3.月度)")
    private Integer vipType;

    @ApiModelProperty(value = "是否是企业用户（1：是；0：否）")
    private Integer isEnterpriseUser;

    @ApiModelProperty(value = "过期时间")
    private Date expireTime;

    @ApiModelProperty(value = "推荐码")
    private String referralCode;

    @ApiModelProperty(value = "二维码url")
    private String codeUrl;
}
