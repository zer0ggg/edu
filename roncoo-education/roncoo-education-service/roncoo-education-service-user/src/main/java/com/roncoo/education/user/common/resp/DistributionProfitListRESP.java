package com.roncoo.education.user.common.resp;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代理分润记录
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionProfitListRESP", description="代理分润记录列表")
public class DistributionProfitListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "分销ID")
    private Long distributionId;

    @ApiModelProperty(value = "分销用户编号")
    private Long userNo;

    @ApiModelProperty(value = "分销用户手机号")
    private String mobile;

    @ApiModelProperty(value = "分销父ID")
    private Long parentId;

    @ApiModelProperty(value = "分销父用户手机号")
    private String parentMobile;

    @ApiModelProperty(value = "分销层级")
    private Integer floor;

    @ApiModelProperty(value = "分润金额（1：推广；2：邀请）")
    private Integer profitType;

    @ApiModelProperty(value = "分润比例")
    private BigDecimal profit;

    @ApiModelProperty(value = "分润金额")
    private BigDecimal profitMoney;

    @ApiModelProperty(value = "订单号")
    private Long orderNo;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;
}
