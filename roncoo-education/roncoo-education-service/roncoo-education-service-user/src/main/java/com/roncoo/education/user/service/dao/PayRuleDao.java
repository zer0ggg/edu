package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample;

import java.util.List;

/**
 * 支付路由表 服务类
 *
 * @author wujing
 * @date 2020-04-02
 */
public interface PayRuleDao {

    int save(PayRule record);

    int deleteById(Long id);

    int updateById(PayRule record);

    PayRule getById(Long id);

    Page<PayRule> listForPage(int pageCurrent, int pageSize, PayRuleExample example);

    /**
     * 根据支付类型，状态，优先级倒序获取路由
     *
     * @param payType
     * @param statusId
     * @return
     */
    List<PayRule> listByPayTypeAndStatusIdAndPriorityDesc(Integer payType, Integer statusId);

    PayRule getByPayChannelCode(String payChannelCode);
}
