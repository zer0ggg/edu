package com.roncoo.education.user.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class LecturerViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "讲师编号")
	private Long lecturerUserNo;

	@ApiModelProperty(value = "讲师名称")
	private String lecturerName;

	@ApiModelProperty(value = "电话")
	private String lecturerMobile;

	@ApiModelProperty(value = "头像")
	private String headImgUrl;

	@ApiModelProperty(value = "邮箱")
	private String lecturerEmail;

	@ApiModelProperty(value = "职位")
	private String position;

	@ApiModelProperty(value = "简介")
	private String introduce;

	@ApiModelProperty(value = "是否拥有直播功能（0：否，1: 是）")
	private Integer isLive;

	@ApiModelProperty(value = "直播推流地址")
	private String pushFlowUrl;

	@ApiModelProperty(value = "直播状态(1:未开始;2:直播中;3:直播结束)")
    private Integer liveStatus;
}
