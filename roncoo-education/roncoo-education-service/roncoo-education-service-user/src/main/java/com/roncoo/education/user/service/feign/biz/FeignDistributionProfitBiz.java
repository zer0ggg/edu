package com.roncoo.education.user.service.feign.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.DistributionProfitQO;
import com.roncoo.education.user.feign.vo.DistributionProfitVO;
import com.roncoo.education.user.service.dao.DistributionProfitDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample.Criteria;

/**
 * 代理分润记录
 *
 * @author wujing
 */
@Component
public class FeignDistributionProfitBiz extends BaseBiz {

    @Autowired
    private DistributionProfitDao dao;

	public Page<DistributionProfitVO> listForPage(DistributionProfitQO qo) {
	    DistributionProfitExample example = new DistributionProfitExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<DistributionProfit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, DistributionProfitVO.class);
	}

	public int save(DistributionProfitQO qo) {
		DistributionProfit record = BeanUtil.copyProperties(qo, DistributionProfit.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public DistributionProfitVO getById(Long id) {
		DistributionProfit record = dao.getById(id);
		return BeanUtil.copyProperties(record, DistributionProfitVO.class);
	}

	public int updateById(DistributionProfitQO qo) {
		DistributionProfit record = BeanUtil.copyProperties(qo, DistributionProfit.class);
		return dao.updateById(record);
	}

}
