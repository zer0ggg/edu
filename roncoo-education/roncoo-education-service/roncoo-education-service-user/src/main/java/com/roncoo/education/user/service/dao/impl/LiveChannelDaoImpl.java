package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.LiveChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.LiveChannelMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannelExample;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 直播频道表 服务实现类
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@Repository
public class LiveChannelDaoImpl implements LiveChannelDao {

    @Autowired
    private LiveChannelMapper mapper;

    @Override
    public Page<LiveChannel> listForPage(int pageCurrent, int pageSize, LiveChannelExample example) {
        long count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent((int)count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage((int)count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<LiveChannel>((int)count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int save(LiveChannel record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(LiveChannel record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public LiveChannel getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public LiveChannel getByChannel(String channelId) {
        LiveChannelExample example = new LiveChannelExample();
        LiveChannelExample.Criteria criteria = example.createCriteria();
        criteria.andChannelIdEqualTo(channelId);
        List<LiveChannel> list = this.mapper.selectByExample(example);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<LiveChannel> listBySceneAndIsUse(String scene, Integer isUse) {
        LiveChannelExample example = new LiveChannelExample();
        LiveChannelExample.Criteria criteria = example.createCriteria();
        criteria.andSceneEqualTo(scene);
        criteria.andIsUseEqualTo(isUse);
        List<LiveChannel> list = this.mapper.selectByExample(example);
        return list;
    }
}
