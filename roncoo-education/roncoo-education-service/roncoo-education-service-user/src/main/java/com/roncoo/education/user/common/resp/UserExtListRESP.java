package com.roncoo.education.user.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户教育信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserExtListRESP", description = "用户教育信息列表")
public class UserExtListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户类型(1用户，2讲师，3代理，4讲师（代理）)")
    private Integer userType;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "是否是会员（0：不是，1:是）")
    private Integer isVip;

    @ApiModelProperty(value = "会员类型(1.年费，2.季度，3.月度)")
    private Integer vipType;

    @ApiModelProperty(value = "是否绑定小程序微信小程序(1:是, 0:否)")
    private Integer isBinding;

    @ApiModelProperty(value = "过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireTime;

    @ApiModelProperty(value = "是否是推荐（1：是；0：否）")
    private Integer isRecommended;

    @ApiModelProperty(value = "推荐码")
    private String referralCode;

}
