package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户教育信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserExtUpdateStatusREQ", description = "用户教育信息状态修改")
public class UserExtUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @ApiModelProperty(value = "状态ID(1:正常;0:禁用)", required = true)
    private Integer statusId;

    @ApiModelProperty(value = "是否为会员(0不是,1是)", required = true)
    private Integer isVip;
}
