package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author YZJ
 */
@Data
@Accessors(chain = true)
public class AuthOrderInfoForChartsBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "讲师用户编号", required = true)
    private Long lecturerUserNo;

    @ApiModelProperty(value = "开始时间", required = false)
	private String beginCreate;

    @ApiModelProperty(value = "结束时间", required = false)
	private String endCreate;

	@ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
	private Integer productType;
}
