package com.roncoo.education.user.service.pc.biz;

import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.LoginStatusEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.JWTUtil;
import com.roncoo.education.user.common.req.UserLoginPasswordREQ;
import com.roncoo.education.user.common.resp.UserLoginRESP;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserLogLoginDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

@Component
public class PcUserLoginBiz {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserLogLoginDao userLogLoginDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Result<UserLoginRESP> loginPassword(UserLoginPasswordREQ req) {
        if (StringUtils.isEmpty(req.getMobile())) {
            return Result.error("手机号不能为空");
        }
        if (StringUtils.isEmpty(req.getPassword())) {
            return Result.error("密码不能为空");
        }

        // 密码错误次数校验

        // 用户校验
        User user = userDao.getByMobile(req.getMobile());
        if (null == user) {
            return Result.error("账号或者密码不正确");
        }
        // 密码校验
        if (!DigestUtil.sha1Hex(user.getMobileSalt() + req.getPassword()).equals(user.getMobilePsw())) {
            loginLog(user.getUserNo(), LoginStatusEnum.FAIL, req.getIp());
            // 放入缓存，错误次数+1
            return Result.error("账号或者密码不正确");
        }
        // 获取用户菜单权限
//        List<String> menuList = feignSysMenu.listByUserAndMenu(user.getUserNo());
//        if (CollectionUtil.isEmpty(menuList)) {
//            return Result.error("没权限！");
//        }

        // 登录日志
        loginLog(user.getUserNo(), LoginStatusEnum.SUCCESS, req.getIp());

        // 用户菜单存入缓存, 时间

        myRedisTemplate.set(RedisPreEnum.ADMINI_MENU.getCode().concat(user.getUserNo().toString()), JSUtil.toJSONString("获取用户菜单权限"), 1, TimeUnit.DAYS);

        UserLoginRESP dto = new UserLoginRESP();
        dto.setUserNo(user.getUserNo());
        dto.setMobile(user.getMobile());
        dto.setToken(JWTUtil.create(user.getUserNo(), JWTUtil.DATE, Constants.Platform.ADMIN));

        // 登录成功，存入缓存，单点登录使用
//        cacheRedis.set(dto.getUserNo().toString(), dto.getToken(), 1, TimeUnit.DAYS);

        return Result.success(dto);
    }

    private void loginLog(Long userNo, LoginStatusEnum status, String ip) {
        UserLogLogin record = new UserLogLogin();
        record.setUserNo(userNo);
        record.setLoginStatus(status.getCode());
        record.setLoginIp(ip);
        userLogLoginDao.save(record);
    }
}
