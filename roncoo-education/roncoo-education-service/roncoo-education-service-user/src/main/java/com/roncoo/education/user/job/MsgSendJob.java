package com.roncoo.education.user.job;

import com.roncoo.education.user.service.feign.biz.FeignMsgBiz;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 站内信-定时发送
 *
 * @author wuyun
 */
@Slf4j
@Component
public class MsgSendJob extends IJobHandler {
    @Autowired
    private FeignMsgBiz biz;

    @Override
    @XxlJob(value = "msgSendJob")
    public ReturnT<String> execute(String param) {
        XxlJobLogger.log("开始执行[发送站内信]");
        try {
            biz.push();
        } catch (Exception e) {
            log.error("站内信-定时发送-执行出错", e);
            return new ReturnT<>(IJobHandler.FAIL.getCode(), "执行[发送站内信]--处理异常");
        }
        log.warn("站内信-定时发送-任务完成");
        XxlJobLogger.log("结束执行[发送站内信]");
        return SUCCESS;
    }
}
