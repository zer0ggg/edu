package com.roncoo.education.user.common.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 异步通知
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayNotifyDTO", description = "异步通知")
public class PayNotifyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "是否验签通过")
    private boolean isVerify;

    @ApiModelProperty(value = "订单状态[OrderStatusEnum]")
    private int orderStatus;

    @ApiModelProperty(value = "成功后回写字符串")
    private String responseStr;

}
