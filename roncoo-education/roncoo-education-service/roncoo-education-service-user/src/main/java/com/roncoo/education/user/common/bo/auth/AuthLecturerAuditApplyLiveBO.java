package com.roncoo.education.user.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthLecturerAuditApplyLiveBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
