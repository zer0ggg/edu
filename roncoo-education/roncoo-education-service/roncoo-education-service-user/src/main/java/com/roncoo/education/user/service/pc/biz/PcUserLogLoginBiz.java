package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.UserLogLoginEditREQ;
import com.roncoo.education.user.common.req.UserLogLoginListREQ;
import com.roncoo.education.user.common.req.UserLogLoginSaveREQ;
import com.roncoo.education.user.common.resp.UserLogLoginListRESP;
import com.roncoo.education.user.common.resp.UserLogLoginViewRESP;
import com.roncoo.education.user.service.dao.UserLogLoginDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLogin;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLoginExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLoginExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户错误登录日志
 *
 * @author wujing
 */
@Component
public class PcUserLogLoginBiz extends BaseBiz {

    @Autowired
    private UserLogLoginDao dao;

    /**
    * 用户错误登录日志列表
    *
    * @param userLogLoginListREQ 用户错误登录日志分页查询参数
    * @return 用户错误登录日志分页查询结果
    */
    public Result<Page<UserLogLoginListRESP>> list(UserLogLoginListREQ userLogLoginListREQ) {
        UserLogLoginExample example = new UserLogLoginExample();
        Criteria c = example.createCriteria();
        // 用户编号
        if (ObjectUtil.isNotEmpty(userLogLoginListREQ.getUserNo())){
            c.andUserNoEqualTo(userLogLoginListREQ.getUserNo());
        }
        example.setOrderByClause(" id desc ");
        Page<UserLogLogin> page = dao.listForPage(userLogLoginListREQ.getPageCurrent(), userLogLoginListREQ.getPageSize(), example);
        Page<UserLogLoginListRESP> respPage = PageUtil.transform(page, UserLogLoginListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 用户错误登录日志添加
    *
    * @param userLogLoginSaveREQ 用户错误登录日志
    * @return 添加结果
    */
    public Result<String> save(UserLogLoginSaveREQ userLogLoginSaveREQ) {
        UserLogLogin record = BeanUtil.copyProperties(userLogLoginSaveREQ, UserLogLogin.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 用户错误登录日志查看
    *
    * @param id 主键ID
    * @return 用户错误登录日志
    */
    public Result<UserLogLoginViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserLogLoginViewRESP.class));
    }


    /**
    * 用户错误登录日志修改
    *
    * @param userLogLoginEditREQ 用户错误登录日志修改对象
    * @return 修改结果
    */
    public Result<String> edit(UserLogLoginEditREQ userLogLoginEditREQ) {
        UserLogLogin record = BeanUtil.copyProperties(userLogLoginEditREQ, UserLogLogin.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 用户错误登录日志删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
