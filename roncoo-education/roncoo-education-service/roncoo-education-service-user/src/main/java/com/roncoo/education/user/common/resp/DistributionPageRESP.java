package com.roncoo.education.user.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分销信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionPageRESP", description="分销信息列表")
public class DistributionPageRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户手机号")
    private String mobile;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "层级")
    private Integer floor;

    @ApiModelProperty(value = "是否允许推广下级")
    private Integer isAllowInvitation;

    @ApiModelProperty(value = "推广比例")
    private BigDecimal spreadProfit;

    @ApiModelProperty(value = "邀请比例")
    private BigDecimal inviteProfit;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "收益")
    private BigDecimal earnings;

    @ApiModelProperty(value = "下级分销信息")
    private List<DistributionPageRESP> childrenList = new ArrayList<>();
}
