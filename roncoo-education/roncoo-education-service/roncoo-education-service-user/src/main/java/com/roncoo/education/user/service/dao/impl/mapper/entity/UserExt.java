package com.roncoo.education.user.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class UserExt implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Long userNo;

    private Integer userType;

    private Integer agentType;

    private String mobile;

    private Integer sex;

    private Integer age;

    private String nickname;

    private String headImgUrl;

    private Integer isVip;

    private Integer vipType;

    private Date expireTime;

    private String referralCode;

    private String codeUrl;

    private Integer isRecommended;

    private String recommendedCodeUrl;

    private String remark;

    private String idCardName;

    private String idCardNo;

    private String idCardFrontImg;

    private String idCardAfterImg;

    private String faceContrasImg;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getAgentType() {
        return agentType;
    }

    public void setAgentType(Integer agentType) {
        this.agentType = agentType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl == null ? null : headImgUrl.trim();
    }

    public Integer getIsVip() {
        return isVip;
    }

    public void setIsVip(Integer isVip) {
        this.isVip = isVip;
    }

    public Integer getVipType() {
        return vipType;
    }

    public void setVipType(Integer vipType) {
        this.vipType = vipType;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode == null ? null : referralCode.trim();
    }

    public String getCodeUrl() {
        return codeUrl;
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl == null ? null : codeUrl.trim();
    }

    public Integer getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(Integer isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getRecommendedCodeUrl() {
        return recommendedCodeUrl;
    }

    public void setRecommendedCodeUrl(String recommendedCodeUrl) {
        this.recommendedCodeUrl = recommendedCodeUrl == null ? null : recommendedCodeUrl.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName == null ? null : idCardName.trim();
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo == null ? null : idCardNo.trim();
    }

    public String getIdCardFrontImg() {
        return idCardFrontImg;
    }

    public void setIdCardFrontImg(String idCardFrontImg) {
        this.idCardFrontImg = idCardFrontImg == null ? null : idCardFrontImg.trim();
    }

    public String getIdCardAfterImg() {
        return idCardAfterImg;
    }

    public void setIdCardAfterImg(String idCardAfterImg) {
        this.idCardAfterImg = idCardAfterImg == null ? null : idCardAfterImg.trim();
    }

    public String getFaceContrasImg() {
        return faceContrasImg;
    }

    public void setFaceContrasImg(String faceContrasImg) {
        this.faceContrasImg = faceContrasImg == null ? null : faceContrasImg.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", userNo=").append(userNo);
        sb.append(", userType=").append(userType);
        sb.append(", agentType=").append(agentType);
        sb.append(", mobile=").append(mobile);
        sb.append(", sex=").append(sex);
        sb.append(", age=").append(age);
        sb.append(", nickname=").append(nickname);
        sb.append(", headImgUrl=").append(headImgUrl);
        sb.append(", isVip=").append(isVip);
        sb.append(", vipType=").append(vipType);
        sb.append(", expireTime=").append(expireTime);
        sb.append(", referralCode=").append(referralCode);
        sb.append(", codeUrl=").append(codeUrl);
        sb.append(", isRecommended=").append(isRecommended);
        sb.append(", recommendedCodeUrl=").append(recommendedCodeUrl);
        sb.append(", remark=").append(remark);
        sb.append(", idCardName=").append(idCardName);
        sb.append(", idCardNo=").append(idCardNo);
        sb.append(", idCardFrontImg=").append(idCardFrontImg);
        sb.append(", idCardAfterImg=").append(idCardAfterImg);
        sb.append(", faceContrasImg=").append(faceContrasImg);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}