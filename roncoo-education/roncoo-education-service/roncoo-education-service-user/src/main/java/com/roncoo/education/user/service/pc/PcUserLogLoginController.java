package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserLogLoginEditREQ;
import com.roncoo.education.user.common.req.UserLogLoginListREQ;
import com.roncoo.education.user.common.req.UserLogLoginSaveREQ;
import com.roncoo.education.user.common.resp.UserLogLoginListRESP;
import com.roncoo.education.user.common.resp.UserLogLoginViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserLogLoginBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户错误登录日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/log/login")
@Api(value = "user-用户错误登录日志", tags = {"user-用户错误登录日志"})
public class PcUserLogLoginController {

    @Autowired
    private PcUserLogLoginBiz biz;

    @ApiOperation(value = "用户错误登录日志列表", notes = "用户错误登录日志列表")
    @PostMapping(value = "/list")
    public Result<Page<UserLogLoginListRESP>> list(@RequestBody UserLogLoginListREQ userLogLoginListREQ) {
        return biz.list(userLogLoginListREQ);
    }

    @ApiOperation(value = "用户错误登录日志添加", notes = "用户错误登录日志添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserLogLoginSaveREQ userLogLoginSaveREQ) {
        return biz.save(userLogLoginSaveREQ);
    }

    @ApiOperation(value = "用户错误登录日志查看", notes = "用户错误登录日志查看")
    @GetMapping(value = "/view")
    public Result<UserLogLoginViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户错误登录日志修改", notes = "用户错误登录日志修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserLogLoginEditREQ userLogLoginEditREQ) {
        return biz.edit(userLogLoginEditREQ);
    }

    @ApiOperation(value = "用户错误登录日志删除", notes = "用户错误登录日志删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
