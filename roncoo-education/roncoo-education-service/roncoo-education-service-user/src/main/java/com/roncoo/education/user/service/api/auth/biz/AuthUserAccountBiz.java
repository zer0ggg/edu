package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.auth.AuthResult;
import com.roncoo.education.common.core.auth.AuthUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.PrivacyUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountBankBO;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountViewBO;
import com.roncoo.education.user.common.dto.auth.AuthUserAccountViewDTO;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;

/**
 * 用户账户信息表
 *
 * @author wujing
 */
@Component
public class AuthUserAccountBiz {
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Result<AuthUserAccountViewDTO> view(AuthUserAccountViewBO bo) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户信息异常");
        }
        UserAccount userAccount = userAccountDao.getByUserNo(userExt.getUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            // 没账户信息，创建一个，兼容之前的业务逻辑
            userAccount = new UserAccount();
            userAccount.setUserNo(userExt.getUserNo());
            userAccount.setTotalIncome(BigDecimal.valueOf(0));
            userAccount.setHistoryMoney(BigDecimal.valueOf(0));
            userAccount.setEnableBalances(BigDecimal.valueOf(0));
            userAccount.setFreezeBalances(BigDecimal.valueOf(0));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.save(userAccount);
        }
        AuthUserAccountViewDTO dto = BeanUtil.copyProperties(userAccount, AuthUserAccountViewDTO.class);
        if (!StringUtils.isEmpty(dto.getBankCardNo())) {
            dto.setBankCardNo(PrivacyUtil.encryptBankAcct(dto.getBankCardNo()));
        }
        if (StringUtils.hasText(dto.getBankIdCardNo())) {
            dto.setBankIdCardNo(PrivacyUtil.encryptIdCard(dto.getBankIdCardNo()));
        }
        return Result.success(dto);
    }

    /**
     * 更新用户银行卡信息
     *
     * @param bo
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> updateBackInfo(AuthUserAccountBankBO bo) {
        if (StringUtils.isEmpty(bo.getBankCardNo())) {
            return Result.error("输入的银行卡号码不能为空!");
        }
        if (StringUtils.isEmpty(bo.getBankName())) {
            return Result.error("输入的开户行名称不能为空!");
        }
        if (StringUtils.isEmpty(bo.getBankBranchName())) {
            return Result.error("输入的开户支行名不能为空!");
        }
        if (StringUtils.isEmpty(bo.getBankUserName())) {
            return Result.error("输入的开户名不能为空!");
        }
        if (StringUtils.isEmpty(bo.getBankIdCardNo())) {
            return Result.error("输入的身份证号码不能为空!");
        }
        if (StringUtils.isEmpty(bo.getSmsCode())) {
            return Result.error("输入的验证码不能为空!");
        }
        if (StringUtils.isEmpty(bo.getBankMobile())) {
            return Result.error("手机号码不能为空!");
        }

        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userAccount) && StatusIdEnum.YES.getCode().equals(userAccount.getStatusId())) {
            return Result.error("找不到账户信息");
        }
        String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + bo.getBankMobile());
        if (StringUtils.isEmpty(redisSmsCode)) {
            return Result.error("验证码已失效!");
        }

        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey("aliyunAppCode");
        if (ObjectUtil.isNull(sysConfigVO)) {
            return Result.error("请配置银行卡4要素验证C版AppCode, KEY=aliyunAppCode");
        }
        // 银行卡4要素验证C版
        AuthResult authResult = AuthUtil.bankcard4c(sysConfigVO.getConfigValue(), bo.getBankCardNo(), bo.getBankIdCardNo(), bo.getBankMobile(), bo.getBankUserName());
        // 鉴权失败
        if (!authResult.isStatus()) {
            return Result.error(authResult.getMessage());
        }
        UserAccount account = BeanUtil.copyProperties(bo, UserAccount.class);
        account.setId(userAccount.getId());
        int resultNum = userAccountDao.updateById(account);
        if (resultNum < 1) {
            return Result.error(ResultEnum.USER_UPDATE_FAIL.getDesc());
        }
        return Result.success(resultNum);
    }

}
