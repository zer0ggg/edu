package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAudit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Component
public class FeignLecturerBiz extends BaseBiz {

	@Autowired
	private LecturerDao dao;
	@Autowired
	private LecturerAuditDao lecturerAuditDao;
	@Autowired
	private UserAccountDao userAccountDao;

	public Page<LecturerVO> listForPage(LecturerQO qo) {
		LecturerExample example = new LecturerExample();
		Criteria c = example.createCriteria();
		if (StringUtils.isNotBlank(qo.getLecturerMobile())) {
			c.andLecturerMobileEqualTo(qo.getLecturerMobile());
		}
		if (StringUtils.isNotEmpty(qo.getLecturerName())) {
			c.andLecturerNameLike(PageUtil.like(qo.getLecturerName()));
		}
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<Lecturer> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, LecturerVO.class);
	}

	public int save(LecturerQO qo) {
		Lecturer record = BeanUtil.copyProperties(qo, Lecturer.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public LecturerVO getById(Long id) {
		Lecturer record = dao.getById(id);
		LecturerVO vo = BeanUtil.copyProperties(record, LecturerVO.class);
		// 查找账户信息
		UserAccount userAccount = userAccountDao.getByUserNo(vo.getLecturerUserNo());
		if (ObjectUtil.isNull(userAccount)) {
			userAccount = new UserAccount();
			userAccount.setUserNo(vo.getLecturerUserNo());
			userAccount.setTotalIncome(BigDecimal.ZERO);
			userAccount.setHistoryMoney(BigDecimal.ZERO);
			userAccount.setEnableBalances(BigDecimal.ZERO);
			userAccount.setFreezeBalances(BigDecimal.ZERO);
			userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
			userAccountDao.save(userAccount);
		}
		vo.setUserAccountVO(BeanUtil.copyProperties(userAccount, UserAccountVO.class));
		return vo;
	}

	@Transactional(rollbackFor = Exception.class)
	public int updateById(LecturerQO qo) {
		Lecturer record = BeanUtil.copyProperties(qo, Lecturer.class);
		if (qo.getLecturerProportion() != null) {
			record.setLecturerProportion(qo.getLecturerProportion().divide(BigDecimal.valueOf(100)));
		}
		int lecturerNum = dao.updateById(record);
		if (lecturerNum < 1) {
			throw new BaseException("讲师信息表更新失败");
		}
		return lecturerAuditDao.updateById(BeanUtil.copyProperties(record, LecturerAudit.class));
	}

	/**
	 * 根据讲师用户编号查找讲师信息
	 *
	 * @param lecturerUserNo
	 * @return
	 */
	public LecturerVO getByLecturerUserNo(Long lecturerUserNo) {
		if (lecturerUserNo == null) {
			throw new BaseException("传入的讲师用户编号不能为空");
		}
		Lecturer record = dao.getByLecturerUserNoWithBLOBs(lecturerUserNo);
		if (ObjectUtil.isNull(record)) {
			throw new BaseException("找不到讲师信息");
		}
		LecturerVO vo = BeanUtil.copyProperties(record, LecturerVO.class);
		// 查找账户信息
		UserAccount userAccount = userAccountDao.getByUserNo(vo.getLecturerUserNo());
		if (ObjectUtil.isNull(userAccount)) {
			userAccount = new UserAccount();
			userAccount.setUserNo(vo.getLecturerUserNo());
			userAccount.setTotalIncome(BigDecimal.valueOf(0));
			userAccount.setHistoryMoney(BigDecimal.valueOf(0));
			userAccount.setEnableBalances(BigDecimal.valueOf(0));
			userAccount.setFreezeBalances(BigDecimal.valueOf(0));
			userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
			userAccountDao.save(userAccount);
		}
		vo.setUserAccountVO(BeanUtil.copyProperties(userAccount, UserAccountVO.class));
		return vo;
	}

	/**
	 * 列出所有讲师信息
	 *
	 * @author WY
	 */
	public List<LecturerVO> listAllForLecturer() {
		List<Lecturer> lecturerList = dao.listByStatusId(StatusIdEnum.YES.getCode());
		return PageUtil.copyList(lecturerList, LecturerVO.class);
	}

	public int liveUpdateById(LecturerQO qo) {
		Lecturer lecturer = BeanUtil.copyProperties(qo, Lecturer.class);
		return dao.updateById(lecturer);
	}

    public LecturerVO getByLecturerMobile(String lecturerMobile) {
		Lecturer lecturer = dao.getByLecturerMobile(lecturerMobile);
		return BeanUtil.copyProperties(lecturer, LecturerVO.class);
    }
	public List<LecturerVO> listByLecturerUserNos(LecturerQO lecturerQO) {
		List<Lecturer> lecturerList = dao.listByLecturerUserNos(lecturerQO.getLecturerUserNos());
		return PageUtil.copyList(lecturerList, LecturerVO.class);
	}
}
