package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 订单支付信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthOrderPayBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程编号,试卷编号,会员主键", required = true)
    private Long productId;

    @ApiModelProperty(value = "支付方式：(1:微信扫码支付；2:支付宝扫码支付；4:线下支付；5:微信小程序支付；6:微信APP支付；7：支付宝APP支付；8:支付宝H5支付；9:微信H5支付)", required = true)
    private Integer payType;

    @ApiModelProperty(value = "购买渠道：1web", required = true)
    private Integer channelType;

    @ApiModelProperty(value = "用户备注")
    private String remarkCus;

    @ApiModelProperty(value = "产品类型：(1普通,2直播,3会员,4文库,5试卷)", required = true)
    private Integer productType;

    @ApiModelProperty(value = "活动id（目前只支持点播、直播、文库课程参与活动）", required = true)
    private Long actId;

    @ApiModelProperty(value = "推荐码")
    private String referralCode;


}
