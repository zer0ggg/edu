package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefSaveQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.interfaces.IFeignVipSet;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.user.common.bo.PayOrderBO;
import com.roncoo.education.user.common.dto.PayOrderDTO;
import com.roncoo.education.user.common.pay.OrderFacade;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.common.req.*;
import com.roncoo.education.user.common.resp.OrderInfoCallbackUserRESP;
import com.roncoo.education.user.common.resp.OrderInfoListRESP;
import com.roncoo.education.user.common.resp.OrderInfoViewRESP;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample.Criteria;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 订单信息
 *
 * @author wujing
 */
@Component
public class PcOrderInfoBiz extends BaseBiz {

    /**
     * 内存中缓存记录行数
     */
    private static final Integer REPORT_CACHE_NUM = 100;
    private static final Integer NUM_PER_PAGE = 1000;
    private static final Integer REPORT_MAX_TOTAL = 100000;
    private static final Integer SHEET_MAX_NUM = 60000;

    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignVipSet feignVipSet;
    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignUserOrderExamRef feignUserOrderExamRef;
    @Autowired
    private IFeignOrderLog feignOrderLog;

    @Autowired
    private OrderInfoDao dao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private PayChannelDao payChannelDao;
    @Autowired
    private UserAccountDao userAccountDao;


    @Autowired
    private OrderFacade orderFacade;
    @Autowired
    private Map<String, PayFacade> payFacades;
    @Autowired
    protected HttpServletResponse response;

    /**
     * 订单信息列表
     *
     * @param orderInfoListREQ 订单信息分页查询参数
     * @return 订单信息分页查询结果
     */
    public Result<Page<OrderInfoListRESP>> list(OrderInfoListREQ orderInfoListREQ) {
        logger.info("请求参数{}", orderInfoListREQ);
        OrderInfoExample example = getPageExample(orderInfoListREQ);
        Page<OrderInfo> page = dao.listForPage(orderInfoListREQ.getPageCurrent(), orderInfoListREQ.getPageSize(), example);
        Page<OrderInfoListRESP> respPage = PageUtil.transform(page, OrderInfoListRESP.class);
        return Result.success(respPage);
    }

    /**
     * 分页，导出共用查询条件
     *
     * @param orderInfoListREQ 订单信息分页查询参数
     * @return 查询条件
     */
    private OrderInfoExample getPageExample(OrderInfoListREQ orderInfoListREQ) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        if (orderInfoListREQ.getOrderNo() != null) {
            c.andOrderNoEqualTo(orderInfoListREQ.getOrderNo());
        }
        if (orderInfoListREQ.getTradeType() != null) {
            c.andTradeTypeEqualTo(orderInfoListREQ.getTradeType());
        }
        if (orderInfoListREQ.getPayType() != null) {
            c.andPayTypeEqualTo(orderInfoListREQ.getPayType());
        }
        if (orderInfoListREQ.getChannelType() != null) {
            c.andChannelTypeEqualTo(orderInfoListREQ.getChannelType());
        }
        if (orderInfoListREQ.getOrderStatus() != null) {
            c.andOrderStatusEqualTo(orderInfoListREQ.getOrderStatus());
        } else {
            c.andOrderStatusNotEqualTo(OrderStatusEnum.CLOSE.getCode());
        }
        // 用户手机号
        if (StringUtils.hasText(orderInfoListREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(orderInfoListREQ.getMobile()));
        }
        // 讲师编号
        if (orderInfoListREQ.getLecturerUserNo() != null) {
            c.andLecturerUserNoEqualTo(orderInfoListREQ.getLecturerUserNo());
        }
        // 讲师名称
        if (StringUtils.hasText(orderInfoListREQ.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.like(orderInfoListREQ.getLecturerName()));
        }
        // 课程ID
        if (orderInfoListREQ.getProductId() != null) {
            c.andProductIdEqualTo(orderInfoListREQ.getProductId());
        }
        // 课程名称
        if (StringUtils.hasText(orderInfoListREQ.getProductName())) {
            c.andProductNameLike(PageUtil.like(orderInfoListREQ.getProductName()));
        }
        // 后台备注
        if (StringUtils.hasText(orderInfoListREQ.getRemark())) {
            c.andRemarkLike(PageUtil.like(orderInfoListREQ.getRemark()));
        }
        // 课程分类
        if (orderInfoListREQ.getProductType() != null) {
            c.andProductTypeEqualTo(orderInfoListREQ.getProductType());
        }

        if (ObjectUtil.isNotNull(orderInfoListREQ.getBeginPayTime())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(orderInfoListREQ.getBeginPayTime(), "yyyy-MM-dd"));
        }
        if (ObjectUtil.isNotNull(orderInfoListREQ.getEndPayTime())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(orderInfoListREQ.getEndPayTime(), "yyyy-MM-dd"), 1));

        }
        example.setOrderByClause(" id desc ");
        return example;
    }

    /**
     * 导出
     *
     * @param orderInfoListREQ 订单信息分页查询参数
     */
    public void export(OrderInfoListREQ orderInfoListREQ) {
        logger.info("导出报表");
        logger.info("请求参数{}", orderInfoListREQ);
        try {
            response.setCharacterEncoding("utf-8");
            String fileName = "订单信息" + DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN);
            logger.info("导出订单交易信息！");
            String[] names = {"下单时间", "订单编号", "产品名称", "讲师名称", "用户手机", "交易流水号", "订单金额", "订单状态", "支付方式", "交易类型", "交易渠道", "支付时间", "平台收入", "讲师收入"};// 表头
            Integer[] widths = {10, 10, 8, 6, 6, 10, 6, 6, 8, 6, 6, 10, 6, 6};// 列宽
            int rowIndex = 0;// 当前sheet行数

            // 创建一个excel文件
            final SXSSFWorkbook workbook = new SXSSFWorkbook(REPORT_CACHE_NUM);
            SXSSFSheet sheet = workbook.createSheet("1表");

            // 创建第一行
            SXSSFRow row = sheet.createRow(rowIndex);
            // 创建sheet头部
            int nameLength = names.length;
            for (int i = 0; i < nameLength; i++) {
                row.createCell(i).setCellValue(names[i]);
                sheet.setColumnWidth(i, widths[i] * 512);
            }


            // 第一次获取数据
            boolean isGo = true;
            orderInfoListREQ.setPageSize(NUM_PER_PAGE);
            Page<OrderInfo> page = null;
            do {
                try {
                    OrderInfoExample example = getPageExample(orderInfoListREQ);
                    page = dao.listForPage(orderInfoListREQ.getPageCurrent(), orderInfoListREQ.getPageSize(), example);
                } catch (Exception e) {
                    logger.info("获取订单信息失败!", e);
                }

                // 第一次获取数据判空
                if (orderInfoListREQ.getPageCurrent() <= 1) {
                    boolean isError = false;
                    if (StringUtils.isEmpty(page) || page.getList() == null || page.getList().isEmpty()) {
                        logger.error("没有需要导出的订单信息!");
                        isError = true;
                        sheet.createRow(++rowIndex).createCell(0).setCellValue("没有需要导出的订单信息!");
                    }
                    // 判断中记录数是否大于最大限额
                    assert page != null;
                    if (REPORT_MAX_TOTAL.compareTo(page.getTotalCount()) < 0) {
                        logger.error("选中的订单信息据大于" + REPORT_MAX_TOTAL + "条");
                        isError = true;
                        sheet.createRow(++rowIndex).createCell(0).setCellValue("选中的订单信息据大于" + REPORT_MAX_TOTAL + "条");
                    }
                    if (isError) {

                        writeExcel(response, workbook, fileName);
                    }
                }

                assert page != null;
                int totalCount = page.getList().size();
                List<OrderInfo> resultList = page.getList();
                for (int i = 0; i < totalCount; i++) {
                    row = sheet.createRow(++rowIndex);
                    OrderInfo orderInfo = resultList.get(i);
                    row.createCell(0).setCellValue(DateUtil.format(orderInfo.getGmtCreate(), DatePattern.NORM_DATETIME_PATTERN));// 下单时间
                    row.createCell(1).setCellValue(orderInfo.getOrderNo().toString());// 订单编号
                    row.createCell(2).setCellValue(orderInfo.getProductName());// 产品名称
                    row.createCell(3).setCellValue(orderInfo.getLecturerName());// 讲师名称
                    row.createCell(4).setCellValue(orderInfo.getMobile());// 用户手机
                    row.createCell(5).setCellValue(orderInfo.getSerialNumber());// 交易流水号
                    row.createCell(6).setCellValue(orderInfo.getPricePaid().doubleValue());// 订单金额
                    row.createCell(7).setCellValue(Objects.requireNonNull(OrderStatusEnum.getByCode(orderInfo.getOrderStatus())).getDesc());// 订单状态
                    row.createCell(8).setCellValue(Objects.requireNonNull(PayTypeEnum.getByCode(orderInfo.getPayType())).getDesc());// 支付方式
                    row.createCell(9).setCellValue(Objects.requireNonNull(TradeTypeEnum.getByCode(orderInfo.getTradeType())).getDesc());// 交易类型
                    row.createCell(10).setCellValue(Objects.requireNonNull(ChannelTypeEnum.getByCode(orderInfo.getTradeType())).getDesc());// 交易渠道
                    row.createCell(11).setCellValue(DateUtil.format(orderInfo.getPayTime(), DatePattern.NORM_DATETIME_PATTERN));// 支付时间
                    row.createCell(12).setCellValue(orderInfo.getPlatformIncome().doubleValue());// 平台收入
                    row.createCell(13).setCellValue(orderInfo.getLecturerIncome().doubleValue());// 讲师收入

                    // 每当行数达到设置的值就刷新数据到硬盘,以清理内存
                    if (rowIndex % REPORT_CACHE_NUM == 0) {
                        try {
                            sheet.flushRows();
                        } catch (IOException e) {
                            logger.error("订单信息导出,刷新数据到硬盘失败！");
                        }
                    }

                    // 当记录达到每页的sheet记录数大小，创建新的sheet
                    if (rowIndex % SHEET_MAX_NUM == 0) {
                        logger.info("订单信息导出，创建新sheet:{}", workbook.getNumberOfSheets() + 1);
                        sheet = workbook.createSheet((workbook.getNumberOfSheets() + 1) + "表");
                        rowIndex = 0;
                        row = sheet.createRow(rowIndex);
                        // 创建sheet头部
                        nameLength = names.length;
                        for (int j = 0; j < nameLength; j++) {
                            row.createCell(j).setCellValue(names[j]);
                            sheet.setColumnWidth(j, widths[j] * 512);
                        }
                    }
                }

                if (page.getPageCurrent() >= page.getTotalPage()) {
                    isGo = false;
                }
                orderInfoListREQ.setPageCurrent(page.getPageCurrent() + 1);
            } while (isGo);
            writeExcel(response, workbook, fileName);
        } catch (Exception e) {
            logger.error("错误信息", e);
        }
    }

    /**
     * 写入文件
     */
    private void writeExcel(HttpServletResponse response, SXSSFWorkbook workbook, String fileName) {
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");// 设置强制下载不打开

        // 设置文件名
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes(), StandardCharsets.ISO_8859_1) + ".xlsx");

        ServletOutputStream outputStream = null;
        // 写入数据
        try {
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("日志导出失败！", e);
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                logger.error("日志导出失败", e);
            }
        }
    }


    /**
     * 订单信息添加
     *
     * @param orderInfoSaveREQ 订单信息
     * @return 添加结果
     */
    public Result<String> save(OrderInfoSaveREQ orderInfoSaveREQ) {
        OrderInfo record = BeanUtil.copyProperties(orderInfoSaveREQ, OrderInfo.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 订单信息查看
     *
     * @param id 主键ID
     * @return 订单信息
     */
    public Result<OrderInfoViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), OrderInfoViewRESP.class));
    }


    /**
     * 订单信息修改
     *
     * @param orderInfoEditREQ 订单信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(OrderInfoEditREQ orderInfoEditREQ) {
        OrderInfo record = BeanUtil.copyProperties(orderInfoEditREQ, OrderInfo.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 订单信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 获取订单信息查找带回用户信息
     *
     * @param orderInfoCallbackUserREQ 分页查询参数
     * @return 分页结果
     */
    public Result<Page<OrderInfoCallbackUserRESP>> userPage(OrderInfoCallbackUserREQ orderInfoCallbackUserREQ) {
        UserExtExample example = new UserExtExample();
        UserExtExample.Criteria c = example.createCriteria();
        if (StrUtil.isNotBlank(orderInfoCallbackUserREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(orderInfoCallbackUserREQ.getMobile()));
        }
        example.setOrderByClause("id desc");
        Page<UserExt> userExtPage = userExtDao.listForPage(orderInfoCallbackUserREQ.getPageCurrent(), orderInfoCallbackUserREQ.getPageSize(), example);
        return Result.success(PageUtil.transform(userExtPage, OrderInfoCallbackUserRESP.class));
    }

    /**
     * 根据Id解绑会员
     *
     * @param id 订单id
     * @return 处理结果
     */
    @GlobalTransactional(rollbackFor = Exception.class)
    public Result<String> orderUnbind(Long id) {
        if (StringUtils.isEmpty(id)) {
            throw new BaseException("传入的订单ID不能为空");
        }
        OrderInfo orderInfo = dao.getById(id);
        if (ObjectUtil.isNull(orderInfo)) {
            throw new BaseException("根据传入的订单ID没找到对应的订单信息");
        }
        orderInfo.setOrderStatus(OrderStatusEnum.UNBIND.getCode());
        dao.updateById(orderInfo);

        if (ProductTypeEnum.ORDINARY.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.LIVE.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.RESOURCES.getCode().equals(orderInfo.getProductType())) {
            // 根据用户编号和用户订单关联编号查找用户订单课程关联信息
            UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();
            userOrderCourseRefQO.setUserNo(orderInfo.getUserNo());
            userOrderCourseRefQO.setRefId(orderInfo.getProductId());
            UserOrderCourseRefVO userOrderCourse = feignUserOrderCourseRef.getByUserNoAndRefId(userOrderCourseRefQO);
            if (ObjectUtil.isNull(userOrderCourse)) {
                throw new BaseException("没找到用户订单课程关联关系信息!");
            }
            userOrderCourse.setIsPay(IsPayEnum.NO.getCode());
            feignUserOrderCourseRef.updateById(BeanUtil.copyProperties(userOrderCourse, UserOrderCourseRefQO.class));
            return Result.success("课程订单解绑成功");
        }

        if (ProductTypeEnum.VIP.getCode().equals(orderInfo.getProductType())) {
            // 根据用户编号和用户机构号查找用户教育信息
            UserExt userExt = userExtDao.getByUserNo(orderInfo.getUserNo());
            if (ObjectUtil.isNull(userExt)) {
                throw new BaseException("根据传入的用户编号没找到对应的用户教育信息");
            }
            // 根据订单信息，减去对应会员时间
            VipSetVO vipSetVO = feignVipSet.getById(orderInfo.getProductId());
            if (ObjectUtil.isNull(vipSetVO)) {
                throw new BaseException("找不到会员设置");
            }
            userExt.setExpireTime(com.roncoo.education.common.core.tools.DateUtil.subDate(userExt.getExpireTime(), Objects.requireNonNull(VipTypeEnum.getByExpireTime(vipSetVO.getSetType())).getExpireTime()));
            userExt.setIsVip(IsVipEnum.NO.getCode());
            if (userExtDao.updateById(userExt) < 0) {
                throw new BaseException("会员订单解绑失败");
            }
            return Result.success("会员订单解绑成功");
        }

        if (ProductTypeEnum.EXAM.getCode().equals(orderInfo.getProductType())) {
            // 如果用户购买过，不作处理，需要人工进行退款处理
            UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
            userOrderExamRefQO.setUserNo(orderInfo.getUserNo());
            userOrderExamRefQO.setExamId(orderInfo.getProductId());
            UserOrderExamRefVO userOrderExamRefVO = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
            if (ObjectUtil.isNull(userOrderExamRefVO)) {
                throw new BaseException("找不到试卷关联信息");
            }
            feignUserOrderExamRef.deleteById(userOrderExamRefVO.getId());
            return Result.success("试卷订单解绑成功");
        }
        return Result.error("没有匹配的产品类型");
    }

    /**
     * TODO 需要分布式事务
     *
     * @param orderInfoManualREQ 订单手工录单参数
     * @return 录单结果
     */
    @GlobalTransactional(rollbackFor = Exception.class)
    public Result<String> manualOrder(OrderInfoManualREQ orderInfoManualREQ) {
        if (ObjectUtil.isNull(orderInfoManualREQ.getProductId())) {
            return Result.error("请选择产品类型");
        }
        if (ObjectUtil.isNull(orderInfoManualREQ.getProductId())) {
            return Result.error("请选择产品");
        }
        if (ObjectUtil.isNull(orderInfoManualREQ.getUserNo())) {
            return Result.error("请选择用户");
        }
        // 点播、直播、文库
        if (ProductTypeEnum.ORDINARY.getCode().equals(orderInfoManualREQ.getProductType())) {
            return manualCourse(orderInfoManualREQ);
        }
        // 试卷
        if (ProductTypeEnum.EXAM.getCode().equals(orderInfoManualREQ.getProductType())) {
            return manualExam(orderInfoManualREQ);
        }
        // 会员
        if (ProductTypeEnum.VIP.getCode().equals(orderInfoManualREQ.getProductType())) {
            return manualVip(orderInfoManualREQ);
        }
        return Result.error("产品类型有误");
    }

    /**
     * 手工录单，会员
     *
     * @param orderInfoManualREQ 订单手工录单参数
     * @return 录单结果
     */
    public Result<String> manualVip(OrderInfoManualREQ orderInfoManualREQ) {
        VipSetVO vipSetVO = feignVipSet.getById(orderInfoManualREQ.getProductId());
        if (ObjectUtil.isNull(vipSetVO)) {
            return Result.error("找不到会员设置");
        }
        // 查找用户信息
        UserExt userExt = userExtDao.getByUserNo(orderInfoManualREQ.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("找不到该用户");
        }

        // 插入订单信息
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUserNo(orderInfoManualREQ.getUserNo());
        orderInfo.setRemark(orderInfoManualREQ.getRemark());
        orderInfo.setLecturerUserNo(0L);
        orderInfo.setLecturerName("购买会员");
        orderInfo.setProductId(vipSetVO.getId());
        String goodsName = Constants.VIP_NAME;
        // 支付显示，"领课网络-VIP会员"
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.ALIYUN_SMS_SIGN_NAME);
        if (ObjectUtil.isNotEmpty(sysConfigVO) && ObjectUtil.isNotEmpty(sysConfigVO.getConfigValue())) {
            goodsName = sysConfigVO.getConfigValue() + "-" + Constants.VIP_NAME;
        }
        orderInfo.setProductName(goodsName);
        orderInfo.setProductType(ProductTypeEnum.VIP.getCode());
        orderInfo.setPricePayable(vipSetVO.getOrgPrice());
        orderInfo.setPricePaid(vipSetVO.getFabPrice());
        orderInfo.setMobile(userExt.getMobile());
        orderInfo.setRegisterTime(userExt.getGmtCreate());
        orderInfo.setIsShowLecturer(orderInfoManualREQ.getIsShowLecturer());
        orderInfo.setIsShowUser(orderInfoManualREQ.getIsShowUser());
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        // 平台分成
        BigDecimal platformProfit = orderInfo.getPricePaid().subtract(orderInfo.getLecturerIncome()).setScale(2, RoundingMode.DOWN);
        orderInfo.setPlatformIncome(platformProfit);
        saveOrderInfoForManual(orderInfo);

        // 更新用户信息
        if (userExt.getExpireTime() != null && VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            // 如果是会员，且在有效期内为续费
            userExt.setExpireTime(com.roncoo.education.common.core.tools.DateUtil.addDate(userExt.getExpireTime(), Objects.requireNonNull(VipTypeEnum.getByExpireTime(vipSetVO.getSetType())).getExpireTime()));
        } else {
            userExt.setExpireTime(com.roncoo.education.common.core.tools.DateUtil.addDate(new Date(), Objects.requireNonNull(VipTypeEnum.getByExpireTime(vipSetVO.getSetType())).getExpireTime()));
            userExt.setIsVip(IsVipEnum.YES.getCode());
            userExt.setVipType(vipSetVO.getSetType());
        }
        if (userExtDao.updateById(userExt) < 0) {
            throw new BaseException("录单失败");
        }
        return Result.success("录单成功");
    }

    /**
     * 手工录单，试卷
     *
     * @param orderInfoManualREQ 订单手工录单参数
     * @return 录单结果
     */
    public Result<String> manualExam(OrderInfoManualREQ orderInfoManualREQ) {
        // 根据课程No查找试卷信息
        ExamInfoVO examInfoVO = feignExamInfo.getById(orderInfoManualREQ.getProductId());
        if (StringUtils.isEmpty(examInfoVO)) {
            return Result.error("没找到对应的试卷信息!");
        }

        // 如果用户购买过，不作处理
        UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
        userOrderExamRefQO.setUserNo(orderInfoManualREQ.getUserNo());
        userOrderExamRefQO.setExamId(orderInfoManualREQ.getProductId());
        UserOrderExamRefVO userOrderExamRefVO = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
        if (ObjectUtil.isNotNull(userOrderExamRefVO)) {
            return Result.error("已购买该试卷!");
        }

        // 获取讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(examInfoVO.getLecturerUserNo());
        if (StringUtils.isEmpty(lecturer)) {
            return Result.error("根据课程的讲师编号,没找到对应的讲师信息!");
        }
        // 根据用户编号和机构编号查找用户信息
        UserExt userExt = userExtDao.getByUserNo(orderInfoManualREQ.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("根据传入的userNo没找到对应的用户信息!");
        }

        // 插入订单信息
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUserNo(orderInfoManualREQ.getUserNo());
        orderInfo.setRemark(orderInfoManualREQ.getRemark());
        orderInfo.setLecturerUserNo(examInfoVO.getLecturerUserNo());
        orderInfo.setProductId(examInfoVO.getId());
        orderInfo.setProductName(examInfoVO.getExamName());
        orderInfo.setProductType(ProductTypeEnum.EXAM.getCode());
        orderInfo.setPricePayable(examInfoVO.getOrgPrice());
        orderInfo.setPricePaid(examInfoVO.getFabPrice());
        orderInfo.setLecturerName(lecturer.getLecturerName());
        orderInfo.setMobile(userExt.getMobile());
        orderInfo.setRegisterTime(userExt.getGmtCreate());

        orderInfo.setIsShowLecturer(orderInfoManualREQ.getIsShowLecturer());
        orderInfo.setIsShowUser(orderInfoManualREQ.getIsShowUser());
        orderInfo.setPlatformIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        // 讲师账户加款
        if (IsShowLecturerEnum.YES.getCode().equals(orderInfoManualREQ.getIsShowLecturer())) {
            // 根据讲师编号和机构编号查找讲师账户信息
            UserAccount userAccount = userAccountDao.getByUserNo(examInfoVO.getLecturerUserNo());
            if (ObjectUtil.isNull(userAccount)) {
                return Result.error("找不到讲师账户信息!");
            }
            String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
            if (!userAccount.getSign().equals(sign)) {
                logger.error("账户异常 , sign不正确, 用户编号={}", userAccount.getUserNo());
                return Result.error("讲师账户异常!");
            }

            // 计算讲师分润
            BigDecimal lecturerProfit = orderInfo.getPricePaid().multiply(lecturer.getLecturerProportion()).setScale(2, RoundingMode.DOWN);
            // 讲师收入 = 订单价格x讲师分成比例
            orderInfo.setLecturerIncome(lecturerProfit);

            // 更新讲师账户信息
            userAccount.setTotalIncome(orderInfo.getLecturerIncome().add(userAccount.getEnableBalances()));
            userAccount.setEnableBalances(orderInfo.getLecturerIncome().add(userAccount.getEnableBalances()));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.updateByUserNo(userAccount);

            // 平台分成
            BigDecimal platformProfit = orderInfo.getPricePaid().subtract(orderInfo.getLecturerIncome()).setScale(2, RoundingMode.DOWN);
            orderInfo.setPlatformIncome(platformProfit);
        }
        saveOrderInfoForManual(orderInfo);

        // 绑定试卷
        UserOrderExamRefSaveQO userOrderExamRefSaveQO = new UserOrderExamRefSaveQO();
        userOrderExamRefSaveQO.setLecturerUserNo(orderInfo.getLecturerUserNo());
        userOrderExamRefSaveQO.setUserNo(orderInfo.getUserNo());
        userOrderExamRefSaveQO.setOrderNo(orderInfo.getOrderNo());
        userOrderExamRefSaveQO.setExamId(orderInfo.getProductId());
        feignUserOrderExamRef.save(userOrderExamRefSaveQO);
        return Result.success("录单完成");
    }

    /**
     * 手工录单，课程
     *
     * @param orderInfoManualREQ 订单手工录单参数
     * @return 录单结果
     */
    public Result<String> manualCourse(OrderInfoManualREQ orderInfoManualREQ) {
        // 判断所要购买的课程是否已经购买
        UserOrderCourseRefQO refQO = new UserOrderCourseRefQO();
        refQO.setUserNo(orderInfoManualREQ.getUserNo());
        refQO.setRefId(orderInfoManualREQ.getProductId());
        UserOrderCourseRefVO userOrderCourseRef = feignUserOrderCourseRef.getByUserNoAndRefId(refQO);
        if (!checkUserOrderCourseRef(userOrderCourseRef)) {
            return Result.error("已经购买该课程");
        }
        // 课程信息
        CourseVO course = feignCourse.getById(orderInfoManualREQ.getProductId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }

        // 获取讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(course.getLecturerUserNo());
        if (StringUtils.isEmpty(lecturer)) {
            return Result.error("根据课程的讲师编号,没找到对应的讲师信息!");
        }
        // 根据用户编号和机构编号查找用户信息
        UserExt userExt = userExtDao.getByUserNo(orderInfoManualREQ.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("根据传入的userNo没找到对应的用户信息!");
        }

        // 插入订单信息
        OrderInfo orderInfo = new OrderInfo();
        // 如果不是会员实付金额为课程的原价
        orderInfo.setPricePayable(course.getCourseOriginal());// 应付金额
        orderInfo.setPricePaid(course.getCourseOriginal());// 实付金额
        orderInfo.setPriceDiscount(BigDecimal.valueOf(0));// 优惠金额
        // 如果该用户为会员，且在有效期内
        if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            orderInfo.setPricePayable(course.getCourseOriginal());// 应付金额
            // 优惠金额=课程原价减去优惠价
            orderInfo.setPriceDiscount(course.getCourseOriginal().subtract(course.getCourseDiscount()));
            // 则实付金额为优惠价
            orderInfo.setPricePaid(course.getCourseDiscount());// 实付金额
        }
        orderInfo.setUserNo(orderInfoManualREQ.getUserNo());
        orderInfo.setRemark(orderInfoManualREQ.getRemark());
        orderInfo.setLecturerUserNo(course.getLecturerUserNo());
        orderInfo.setProductId(course.getId());
        orderInfo.setProductName(course.getCourseName());
        orderInfo.setProductType(course.getCourseCategory());
        orderInfo.setLecturerName(lecturer.getLecturerName());
        orderInfo.setMobile(userExt.getMobile());
        orderInfo.setRegisterTime(userExt.getGmtCreate());
        orderInfo.setIsShowLecturer(orderInfoManualREQ.getIsShowLecturer());
        orderInfo.setIsShowUser(orderInfoManualREQ.getIsShowUser());
        orderInfo.setPlatformIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        // 显示给讲师，加讲师账户
        if (IsShowLecturerEnum.YES.getCode().equals(orderInfoManualREQ.getIsShowLecturer())) {
            // 根据讲师编号和机构编号查找讲师账户信息
            UserAccount userAccount = userAccountDao.getByUserNo(course.getLecturerUserNo());
            if (ObjectUtil.isNull(userAccount)) {
                return Result.error("找不到讲师账户信息!");
            }
            String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
            if (!userAccount.getSign().equals(sign)) {
                logger.error("账户异常 , sign不正确, 用户编号={}", userAccount.getUserNo());
                return Result.error("讲师账户异常!");
            }
            // 计算讲师分润
            BigDecimal lecturerProfit = orderInfo.getPricePaid().multiply(lecturer.getLecturerProportion()).setScale(2, RoundingMode.DOWN);
            // 讲师收入 = 订单价格x讲师分成比例
            orderInfo.setLecturerIncome(lecturerProfit);
            // 更新讲师账户信息
            userAccount.setTotalIncome(orderInfo.getLecturerIncome().add(userAccount.getEnableBalances()));
            userAccount.setEnableBalances(orderInfo.getLecturerIncome().add(userAccount.getEnableBalances()));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.updateById(userAccount);
            // 平台分成
            BigDecimal platformProfit = orderInfo.getPricePaid().subtract(orderInfo.getLecturerIncome()).setScale(2, RoundingMode.DOWN);
            orderInfo.setPlatformIncome(platformProfit);
        }
        saveOrderInfoForManual(orderInfo);

        if (ObjectUtil.isNull(userOrderCourseRef)) {
            // 如果原来的课程用户订单关系信息为空,则插入新的用户课程订单信息关系
            saveUserOrderRefForManual(orderInfo.getOrderNo(), orderInfoManualREQ.getUserNo(), course);
        } else {
            // 不为空则更新
            UserOrderCourseRefQO ref = new UserOrderCourseRefQO();
            ref.setId(userOrderCourseRef.getId());
            ref.setIsPay(IsPayEnum.YES.getCode());
            ref.setExpireTime(com.roncoo.education.common.core.tools.DateUtil.addYear(new Date(), 1));
            feignUserOrderCourseRef.updateById(ref);
        }
        return Result.success("手工录单成功");
    }

    /**
     * 判断课程是否已经支付
     *
     * @param userOrderCourseRefVO 用户订单课程关联
     * @return 判断结果
     */
    private boolean checkUserOrderCourseRef(UserOrderCourseRefVO userOrderCourseRefVO) {
        // 判断课程是否在有效期内
        if (ObjectUtil.isNull(userOrderCourseRefVO) || IsPayEnum.NO.getCode().equals(userOrderCourseRefVO.getIsPay())) {
            return true;
        }
        return userOrderCourseRefVO.getExpireTime().before(new Date());
    }

    /**
     * 创建并保存订单信息
     *
     * @param orderInfo 订单手工录单参数
     * @return 订单信息
     */
    private void saveOrderInfoForManual(OrderInfo orderInfo) {
        orderInfo.setOrderNo(NOUtil.getOrderNo());
        orderInfo.setSerialNumber(NOUtil.getSerialNumber());
        orderInfo.setTradeType(TradeTypeEnum.OFFLINE.getCode());
        orderInfo.setPayType(PayTypeEnum.MANUAL.getCode());
        orderInfo.setChannelType(ChannelTypeEnum.MANUAL.getCode());
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfo.setPayTime(new Date());
        dao.save(orderInfo);
        feignOrderLog.save(BeanUtil.copyProperties(orderInfo, OrderLogQO.class));
    }

    /**
     * 保存用户订单课程关联关系
     *
     * @param orderNo  订单号
     * @param userNo   用户编号
     * @param courseVO 课程信息
     */
    private void saveUserOrderRefForManual(Long orderNo, Long userNo, CourseVO courseVO) {
        UserOrderCourseRefQO userOrderRef = new UserOrderCourseRefQO();
        userOrderRef.setLecturerUserNo(courseVO.getLecturerUserNo());
        userOrderRef.setUserNo(userNo);
        userOrderRef.setCourseId(courseVO.getId());
        userOrderRef.setRefId(courseVO.getId());
        userOrderRef.setCourseType(CourseTypeEnum.COURSE.getCode());
        userOrderRef.setCourseCategory(courseVO.getCourseCategory());
        userOrderRef.setIsPay(IsPayEnum.YES.getCode());
        userOrderRef.setIsStudy(IsStudyEnum.YES.getCode());
        userOrderRef.setOrderNo(orderNo);
        userOrderRef.setExpireTime(com.roncoo.education.common.core.tools.DateUtil.addYear(new Date(), 1));
        feignUserOrderCourseRef.save(userOrderRef);
    }

    /**
     * 查单
     *
     * @param orderNo 订单编号
     * @return 返回
     */
    public Result<String> orderQuery(Long orderNo) {
        // 根据订单号查找订单信息
        OrderInfo orderInfo = dao.getByOrderNo(orderNo);
        if (ObjectUtil.isNull(orderInfo)) {
            return Result.error("找不到对应的订单信息");
        }
        // 如果已经处理过,不用再处理
        if (OrderStatusEnum.SUCCESS.getCode().equals(orderInfo.getOrderStatus())) {
            return Result.error("已经处理过,不用再处理");
        }
        // 获取支付配置
        if (ObjectUtil.isNull(orderInfo.getPayChannelCode())) {
            return Result.error("找不到对应的支付配置");
        }
        PayChannel payChannel = payChannelDao.getByPayChannelCode(orderInfo.getPayChannelCode());
        if (ObjectUtil.isNull(payChannel)) {
            return Result.error("支付配置不可用");
        }
        // 调用支付查询接口
        PayFacade payFacade = payFacades.get(payChannel.getPayObjectCode());
        PayOrderBO payOrderBO = new PayOrderBO();
        payOrderBO.setOrderNo(String.valueOf(orderInfo.getSerialNumber()));
        payOrderBO.setPayChannelCode(orderInfo.getPayChannelCode());
        PayOrderDTO payOrderDTO = payFacade.order(payOrderBO);
        // 该渠道未对接查询接口
        if (ObjectUtil.isNull(payOrderDTO)) {
            return Result.error("未对接查询接口");
        }

        logger.debug("订单信息{}开始处理{}", orderInfo.getSerialNumber(), payOrderDTO.getOrderStatus());
        if (orderFacade.orderInfoComplete(orderInfo.getSerialNumber(), payOrderDTO.getOrderStatus())) {
            return Result.success("订单处理完成");
        }
        return Result.success("请稍后查询");
    }

    /**
     * 改价
     *
     * @param orderInfoUpdatePriceREQ 订单信息改价
     * @return 处理结果
     */
    public Result<String> updatePrice(OrderInfoUpdatePriceREQ orderInfoUpdatePriceREQ) {
        OrderInfo record = BeanUtil.copyProperties(orderInfoUpdatePriceREQ, OrderInfo.class);
        if (dao.updateById(record) > 0) {
            return Result.success("改价成功");
        }
        return Result.error("改价失败");
    }
}
