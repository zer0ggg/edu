package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.*;
import com.roncoo.education.user.common.resp.LecturerListRESP;
import com.roncoo.education.user.common.resp.LecturerViewRESP;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.pc.biz.PcLecturerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 讲师信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/lecturer")
@Api(value = "user-讲师信息", tags = {"user-讲师信息"})
public class PcLecturerController {

    @Autowired
    private PcLecturerBiz biz;

    @ApiOperation(value = "讲师信息列表", notes = "讲师信息列表")
    @PostMapping(value = "/list")
    public Result<Page<LecturerListRESP>> list(@RequestBody LecturerListREQ lecturerListREQ) {
        return biz.list(lecturerListREQ);
    }

    @ApiOperation(value = "讲师信息添加", notes = "讲师信息添加")
    @PostMapping(value = "/save")
    @SysLog(value = "讲师信息添加")
    public Result<String> save(@RequestBody LecturerSaveREQ lecturerSaveREQ) {
        return biz.save(lecturerSaveREQ);
    }

    @ApiOperation(value = "讲师信息查看", notes = "讲师信息查看")
    @GetMapping(value = "/view")
    public Result<LecturerViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "讲师信息修改", notes = "讲师信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "讲师信息修改")
    public Result<String> edit(@RequestBody LecturerEditREQ lecturerEditREQ) {
        return biz.edit(BeanUtil.copyProperties(lecturerEditREQ, Lecturer.class));
    }

    @SysLog(value = "讲师状态修改")
    @ApiOperation(value = "讲师状态修改", notes = "讲师状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatusId(@RequestBody LecturerUpdateStatusIdREQ lecturerUpdateStatusIdREQ) {
        return biz.lecturerStatus(lecturerUpdateStatusIdREQ);
    }

    @ApiOperation(value = "设置讲师分成", notes = "设置讲师分成")
    @PutMapping(value = "/proportion/set")
    public Result<String> setLecturerProportion(@RequestBody LecturerProportionSetREQ lecturerProportionSetREQ) {
        return biz.setLecturerProportion(lecturerProportionSetREQ);
    }

    @ApiOperation(value = "讲师信息删除", notes = "讲师信息删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "讲师信息删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "跳转讲师中心", notes = "跳转讲师中心")
    @GetMapping(value = "/direct")
    public Result<String> redirectLecturerCenter(@RequestParam Long lecturerUserNo) {
        return biz.redirectLecturerCenter(lecturerUserNo);
    }
}
