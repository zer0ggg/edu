package com.roncoo.education.user.service.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignDistribution;
import com.roncoo.education.user.feign.qo.DistributionQO;
import com.roncoo.education.user.feign.vo.DistributionVO;
import com.roncoo.education.user.service.feign.biz.FeignDistributionBiz;

/**
 * 分销信息
 *
 * @author wujing
 * @date 2020-12-29
 */
@RestController
public class FeignDistributionController extends BaseController implements IFeignDistribution{

    @Autowired
    private FeignDistributionBiz biz;

	@Override
	public Page<DistributionVO> listForPage(@RequestBody DistributionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody DistributionQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody DistributionQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public DistributionVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
