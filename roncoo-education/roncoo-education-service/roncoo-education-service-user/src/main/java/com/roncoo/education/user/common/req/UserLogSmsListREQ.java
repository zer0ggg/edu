package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户发送短信日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserLogSmsListREQ", description="用户发送短信日志列表")
public class UserLogSmsListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "是否发送成功(1发送成功，0:发送失败)")
    private Integer isSuccess;

    @ApiModelProperty(value = "开始发送时间")
    private String beginSendTime;

    @ApiModelProperty(value = "结束发送时间")
    private String endSendTime;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
