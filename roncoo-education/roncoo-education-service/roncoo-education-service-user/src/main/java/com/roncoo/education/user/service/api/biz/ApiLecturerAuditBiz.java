package com.roncoo.education.user.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AuditStatusEnum;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigSysVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.common.bo.auth.AuthLecturerAuditSaveBO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAudit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.regex.Pattern;

@Component
public class ApiLecturerAuditBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private LecturerAuditDao lecturerAuditDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserAccountDao userAccountDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 讲师申请接口
     *
     * @param bo
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthLecturerAuditSaveBO bo) {
        if (StringUtils.isEmpty(bo.getLecturerMobile())) {
            return Result.error("lecturerMobile不能为空");
        }
        // 手机号去空处理
        bo.setLecturerMobile(bo.getLecturerMobile().trim());

        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(bo.getLecturerMobile()).matches()) {
            return Result.error("手机号码格式不正确");
        }

        ConfigSysVO sysVO = feignSysConfig.getSys();
        if (ObjectUtil.isNull(sysVO)) {
            return Result.error("找不到系统配置信息");
        }

        // 根据手机号与正常状态获取用户信息(讲师用户信息)
        UserExt userExt = userExtDao.getByMobile(bo.getLecturerMobile());
        if (ObjectUtil.isNull(userExt)) {
            // 新用户
            if (StringUtils.isEmpty(bo.getCode())) {
                return Result.error("验证码不能为空");
            }
            // 验证码校验
            String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + bo.getLecturerMobile());
            if (StringUtils.isEmpty(redisSmsCode)) {
                return Result.error("验证码过期，请重新获取");
            }
            if (!redisSmsCode.equals(bo.getCode())) {
                return Result.error("验证码不正确,重新输入");
            }
            // 密码校验
            if (!bo.getMobilePsw().equals(bo.getRepassword())) {
                return Result.error("密码不一致");
            }

            // 注册用户
            userExt = register(bo);

            // 判断账户信息是否存在,不存在添加账户信息
            UserAccount userAccount = userAccountDao.getByUserNo(userExt.getUserNo());
            if (ObjectUtil.isNull(userAccount)) {
                userAccount = new UserAccount();
                userAccount.setUserNo(userExt.getUserNo());
                userAccount.setTotalIncome(BigDecimal.ZERO);
                userAccount.setHistoryMoney(BigDecimal.ZERO);
                userAccount.setEnableBalances(BigDecimal.ZERO);
                userAccount.setFreezeBalances(BigDecimal.ZERO);
                userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
                userAccountDao.save(userAccount);
            }

            // 添加讲师-只是添加审核表
            addLecturerInfo(bo, userExt, sysVO.getLecturerDefaultProportion());
            return Result.success(1);
        }

        // 老用户
        LecturerAudit lecturerAudit = lecturerAuditDao.getByLecturerUserNo(userExt.getUserNo());
        if (ObjectUtil.isNotNull(lecturerAudit) && AuditStatusEnum.SUCCESS.getCode().equals(lecturerAudit.getAuditStatus())) {
            return Result.error("该账号已申请入驻成为讲师，请更换账号重试");
        }
        if (ObjectUtil.isNull(lecturerAudit)) {
            // 添加讲师-只是添加审核表，需要审核
            addLecturerInfo(bo, userExt, sysVO.getLecturerDefaultProportion());
        } else {
            lecturerAudit = BeanUtil.copyProperties(bo, LecturerAudit.class);
            lecturerAudit.setLecturerUserNo(userExt.getUserNo());
            lecturerAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
            lecturerAuditDao.updateById(lecturerAudit);
        }
        return Result.success(1);
    }

    /**
     * 插入讲师信息
     *
     * @param bo                        讲师信息
     * @param userExt                   用户教育信息
     * @param lecturerDefaultProportion 讲师默认配置
     * @return 保存结果
     */
    private int addLecturerInfo(AuthLecturerAuditSaveBO bo, UserExt userExt, BigDecimal lecturerDefaultProportion) {
        // 插入讲师信息
        LecturerAudit infoAudit = BeanUtil.copyProperties(bo, LecturerAudit.class);
        infoAudit.setLecturerUserNo(userExt.getUserNo());
        infoAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        infoAudit.setLecturerProportion(lecturerDefaultProportion);
        infoAudit.setId(IdWorker.getId());
        return lecturerAuditDao.save(infoAudit);
    }

    /**
     * 添加用户信息
     */
    private UserExt register(AuthLecturerAuditSaveBO authLecturerAuditSaveBO) {
        // 用户基本信息
        User user = new User();
        user.setUserNo(NOUtil.getUserNo());
        user.setMobile(authLecturerAuditSaveBO.getLecturerMobile());
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + authLecturerAuditSaveBO.getMobilePsw()));
        int userNum = userDao.save(user);
        if (userNum < 1) {
            throw new BaseException("用户信息表新增失败");
        }

        // 用户账户信息
        UserAccount userAccount = new UserAccount();
        userAccount.setUserNo(user.getUserNo());
        userAccount.setTotalIncome(BigDecimal.ZERO);
        userAccount.setHistoryMoney(BigDecimal.ZERO);
        userAccount.setEnableBalances(BigDecimal.ZERO);
        userAccount.setFreezeBalances(BigDecimal.ZERO);
        userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
        userAccountDao.save(userAccount);

        // 用户教育信息
        UserExt userExt = new UserExt();
        userExt.setUserNo(user.getUserNo());
        userExt.setMobile(user.getMobile());
        userExt.setNickname(authLecturerAuditSaveBO.getLecturerName());
        // 用户默认头像
        SysConfigVO sysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        if (StringUtils.isEmpty(sysVO.getConfigValue())) {
            throw new BaseException("未设置平台域名");
        }
        userExt.setHeadImgUrl(sysVO.getConfigValue() + "/friend.jpg");
        int userExtNum = userExtDao.save(userExt);
        if (userExtNum < 1) {
            throw new BaseException("用户教育信息表新增失败");
        }

        return userExt;
    }

}
