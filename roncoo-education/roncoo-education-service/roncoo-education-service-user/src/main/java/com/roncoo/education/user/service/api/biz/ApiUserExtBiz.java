package com.roncoo.education.user.service.api.biz;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.service.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class ApiUserExtBiz extends BaseBiz {
    @Autowired
    private UserDao userDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 根据状态，角色获取可用的用户信息的集合
     *
     * @author wuyun
     */
    public void cacheUserForMsg() {
        int pageSize = 1000;
        Page<UserExtMsgVO> page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), 1, pageSize);
        // 缓存key条数
        set(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), page.getTotalPage(), 120);
        // 缓存用户
        for (int i = 1; i < page.getTotalPage() + 1; i++) {
            page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), i, pageSize);
            // 缓存，2个小时
            set(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, page.getList(), 120);
        }
    }

    /**
     * 单位分钟
     */
    private <T> T set(String key, T t, long time) {
        if (t != null) {
            myRedisTemplate.set(key, JSUtil.toJSONString(t), time, TimeUnit.MINUTES);
        }
        return t;
    }
}
