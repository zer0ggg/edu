package com.roncoo.education.user.common.pay.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.roncoo.education.common.core.base.Base;
import com.roncoo.education.common.core.enums.PayNetWorkStatusEnum;
import com.roncoo.education.common.core.enums.WxTradeTypeEnum;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.bo.PayOrderBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.common.dto.PayOrderDTO;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.common.pay.util.WxPayUtil;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 官方微信小程序
 *
 * @author Quanf
 * 2020/3/31 17:59
 */
@Service("WX_PAY_XCX")
public class WxPayXcxImpl extends Base implements PayFacade {

    @Autowired
    private PayChannelDao payChannelDao;

    @Override
    public PayDTO pay(PayBO payBO) {
        logger.info("官方微信小程序:{}", payBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(payBO.getPayChannelCode());
        PayDTO payDTO = new PayDTO();
        payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.FAILED);
        Map<String, String> wxPayResult = WxPayUtil.wxPay(payChannel, payBO, WxTradeTypeEnum.JSAPI.getCode());
        logger.debug("官方微信小程序:{}", wxPayResult);
        try {
            if (ObjectUtil.isNotNull(wxPayResult)) {
                if ("SUCCESS".equals(wxPayResult.get("return_code")) && "SUCCESS".equals(wxPayResult.get("result_code"))) {
                    payDTO.setPayMessage(JSONUtil.toJsonPrettyStr(wxPayResult));
                    payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.SUCCESS);
                }
            }
        } catch (Exception e) {
            logger.error("微信官方支付失败，报文={}", wxPayResult);
        }
        return payDTO;
    }

    @Override
    public PayNotifyDTO verify(PayNotifyBO notifyBO) {
        logger.debug("官方微信小程序支付通知:[{}]", notifyBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(notifyBO.getPayChannelCode());
        return WxPayUtil.checkNotify(notifyBO, payChannel);
    }

    @Override
    public PayOrderDTO order(PayOrderBO orderBO) {
        logger.debug("官方微信小程序查询:[{}]", orderBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(orderBO.getPayChannelCode());
        PayOrderDTO payOrderDTO = new PayOrderDTO();
        int orderStatus = WxPayUtil.orderQuery(orderBO.getOrderNo(), payChannel);
        payOrderDTO.setOrderStatus(orderStatus);
        return payOrderDTO;
    }
}
