package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.user.feign.interfaces.IFeignDemoDataAdd;
import com.roncoo.education.user.feign.qo.UserQO;
import com.roncoo.education.user.service.feign.biz.FeignDemoDataAddBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignDemoDataAddController extends BaseController implements IFeignDemoDataAdd {

    @Autowired
    private FeignDemoDataAddBiz biz;

    @Override
    public int save(@RequestBody UserQO qo) {
        return biz.save(qo);
    }
}
