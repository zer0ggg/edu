package com.roncoo.education.user.service.pc;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.user.common.req.ChannelUpdateStatusIdREQ;
import com.roncoo.education.user.common.req.LiveChannelDeleteREQ;
import com.roncoo.education.user.common.req.LiveChannelListREQ;
import com.roncoo.education.user.common.resp.LiveChannelListRESP;
import com.roncoo.education.user.service.pc.biz.PcLiveChannelBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 直播频道表 UserApi接口
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/user/pc/liveChannel")
public class PcLiveChannelController {

    @Autowired
    private PcLiveChannelBiz biz;

    @ApiOperation(value = "频道信息列表", notes = "频道信息列表")
    @PostMapping(value = "/list")
    public Result<Page<LiveChannelListRESP>> list(@RequestBody LiveChannelListREQ liveChannelListREQ) {
        return biz.list(liveChannelListREQ);
    }

    @ApiOperation(value = "频道删除", notes = "频道删除")
    @PostMapping(value = "/delete")
    @SysLog(value = "频道删除")
    public Result<String> list(@RequestBody LiveChannelDeleteREQ liveChannelDeleteREQ) {
        return biz.delete(liveChannelDeleteREQ);
    }

    @ApiOperation(value = "频道状态修改", notes = "频道状态修改")
    @PutMapping(value = "/update/status")
    @SysLog(value = "频道状态修改")
    public Result<String> updateStatusId(@RequestBody ChannelUpdateStatusIdREQ ChannelUpdateStatusIdREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(ChannelUpdateStatusIdREQ.getStatusId()))) {
            return Result.error("状态不正确");
        }
        return biz.updateStatusId(ChannelUpdateStatusIdREQ);
    }
}
