package com.roncoo.education.user.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import com.roncoo.education.user.service.api.biz.ApiDistributionBiz;
/**
 * 分销信息 Api接口
 *
 * @author wujing
 * @date 2020-12-29
 */
@Api(tags = "API-分销信息")
@RestController
@RequestMapping("/user/api/distribution")
public class ApiDistributionController {

    @Autowired
    private ApiDistributionBiz biz;

}
