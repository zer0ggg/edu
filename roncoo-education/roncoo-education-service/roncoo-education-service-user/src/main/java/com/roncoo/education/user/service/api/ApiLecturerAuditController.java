package com.roncoo.education.user.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthLecturerAuditSaveBO;
import com.roncoo.education.user.service.api.biz.ApiLecturerAuditBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 讲师审核
 */
@RestController
@RequestMapping(value = "/user/api/lecturer/audit")
public class ApiLecturerAuditController extends BaseController {

    @Autowired
    private ApiLecturerAuditBiz biz;

    /**
     * 讲师申请接口
     *
     * @param authLecturerAuditSaveBO
     */
    @ApiOperation(value = "讲师申请接口", notes = "保存讲师信息接口,需要审核")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result<Integer> save(@RequestBody AuthLecturerAuditSaveBO authLecturerAuditSaveBO) {
        return biz.save(authLecturerAuditSaveBO);
    }

}
