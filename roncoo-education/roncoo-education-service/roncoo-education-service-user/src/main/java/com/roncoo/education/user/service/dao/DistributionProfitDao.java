package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample;

import java.util.List;

/**
 * 代理分润记录 服务类
 *
 * @author wujing
 * @date 2021-01-05
 */
public interface DistributionProfitDao {

    /**
     * 保存代理分润记录
     *
     * @param record 代理分润记录
     * @return 影响记录数
     */
    int save(DistributionProfit record);

    /**
     * 根据ID删除代理分润记录
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 代理分润记录
     * @return 影响记录数
     */
    int updateById(DistributionProfit record);

    /**
     * 根据ID获取代理分润记录
     *
     * @param id 主键ID
     * @return 代理分润记录
     */
    DistributionProfit getById(Long id);

    /**
     * 代理分润记录--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<DistributionProfit> listForPage(int pageCurrent, int pageSize, DistributionProfitExample example);

}
