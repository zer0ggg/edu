package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserLecturerAttentionMapper {
    int countByExample(UserLecturerAttentionExample example);

    int deleteByExample(UserLecturerAttentionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserLecturerAttention record);

    int insertSelective(UserLecturerAttention record);

    List<UserLecturerAttention> selectByExample(UserLecturerAttentionExample example);

    UserLecturerAttention selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserLecturerAttention record, @Param("example") UserLecturerAttentionExample example);

    int updateByExample(@Param("record") UserLecturerAttention record, @Param("example") UserLecturerAttentionExample example);

    int updateByPrimaryKeySelective(UserLecturerAttention record);

    int updateByPrimaryKey(UserLecturerAttention record);
}
