package com.roncoo.education.user.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.PayChannelQO;
import com.roncoo.education.user.feign.vo.PayChannelVO;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 银行渠道信息表
 *
 * @author wujing
 */
@Component
public class FeignPayChannelBiz extends BaseBiz {

    @Autowired
    private PayChannelDao dao;

	public Page<PayChannelVO> listForPage(PayChannelQO qo) {
	    PayChannelExample example = new PayChannelExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<PayChannel> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, PayChannelVO.class);
	}

	public int save(PayChannelQO qo) {
		PayChannel record = BeanUtil.copyProperties(qo, PayChannel.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public PayChannelVO getById(Long id) {
		PayChannel record = dao.getById(id);
		return BeanUtil.copyProperties(record, PayChannelVO.class);
	}

	public int updateById(PayChannelQO qo) {
		PayChannel record = BeanUtil.copyProperties(qo, PayChannel.class);
		return dao.updateById(record);
	}

}
