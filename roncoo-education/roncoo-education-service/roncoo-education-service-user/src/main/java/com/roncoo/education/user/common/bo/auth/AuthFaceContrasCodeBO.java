package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 人脸对比信息
 */
@Data
@Accessors(chain = true)
public class AuthFaceContrasCodeBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课时id
     */
    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;
    /**
     * 跳转页面
     */
    @ApiModelProperty(value = "跳转页面")
    private String page;
}
