package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.user.common.req.UserMsgEditREQ;
import com.roncoo.education.user.common.req.UserMsgListREQ;
import com.roncoo.education.user.common.req.UserMsgSaveREQ;
import com.roncoo.education.user.common.resp.UserMsgListRESP;
import com.roncoo.education.user.common.resp.UserMsgViewRESP;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 站内信用户记录
 *
 * @author wujing
 */
@Component
public class PcUserMsgBiz extends BaseBiz {

    @Autowired
    private UserMsgDao dao;

    /**
     * 站内信用户记录列表
     *
     * @param req 站内信用户记录分页查询参数
     * @return 站内信用户记录分页查询结果
     */
    public Result<Page<UserMsgListRESP>> list(UserMsgListREQ req) {
        UserMsgExample example = new UserMsgExample();
        Criteria c = example.createCriteria();
        // 用户手机
        if (ObjectUtil.isNotEmpty(req.getMobile())) {
            c.andMobileLike(SqlUtil.like(req.getMobile()));
        }
        // 短信标题
        if (ObjectUtil.isNotEmpty(req.getMsgTitle())) {
            c.andMsgTitleLike(SqlUtil.like(req.getMsgTitle()));
        }
        // 是否阅读
        if (ObjectUtil.isNotEmpty(req.getIsRead())) {
            c.andIsReadEqualTo(req.getIsRead());
        }
        // 是否已经发送
        if (ObjectUtil.isNotEmpty(req.getIsSend())) {
            c.andIsSendEqualTo(req.getIsSend());
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<UserMsg> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<UserMsgListRESP> respPage = PageUtil.transform(page, UserMsgListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 站内信用户记录添加
     *
     * @param userMsgSaveREQ 站内信用户记录
     * @return 添加结果
     */
    public Result<String> save(UserMsgSaveREQ userMsgSaveREQ) {
        UserMsg record = BeanUtil.copyProperties(userMsgSaveREQ, UserMsg.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 站内信用户记录查看
     *
     * @param id 主键ID
     * @return 站内信用户记录
     */
    public Result<UserMsgViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserMsgViewRESP.class));
    }


    /**
     * 站内信用户记录修改
     *
     * @param userMsgEditREQ 站内信用户记录修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserMsgEditREQ userMsgEditREQ) {
        UserMsg record = BeanUtil.copyProperties(userMsgEditREQ, UserMsg.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 站内信用户记录删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
