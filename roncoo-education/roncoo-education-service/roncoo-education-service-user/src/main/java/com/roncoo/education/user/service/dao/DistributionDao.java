package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Distribution;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample;

import java.util.ArrayList;
import java.util.List;

/**
 * 分销信息 服务类
 *
 * @author wujing
 * @date 2020-12-29
 */
public interface DistributionDao {

    /**
     * 保存分销信息
     *
     * @param record 分销信息
     * @return 影响记录数
     */
    int save(Distribution record);

    /**
     * 根据ID删除分销信息
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 分销信息
     * @return 影响记录数
     */
    int updateById(Distribution record);

    /**
     * 根据ID获取分销信息
     *
     * @param id 主键ID
     * @return 分销信息
     */
    Distribution getById(Long id);

    /**
     * 分销信息--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<Distribution> listForPage(int pageCurrent, int pageSize, DistributionExample example);

    /**
     * 根据层级获取分销集合
     *
     * @param floor
     * @return
     */
    List<Distribution> listByFloor(Integer floor);

    /**
     * 根据用户编号获取用户分销信息
     *
     * @param userNo
     * @return
     */
    Distribution getByUserNo(Long userNo);

    /**
     * 根据父id获取用户分销信息
     *
     * @param parentId
     * @return
     */
    List<Distribution> listByParentId(Long parentId);

    /**
     * 根据id集合获取分销信息
     *
     * @param ids
     * @return
     */
    List<Distribution> listByIds(List<Long> ids);

    /**
     *
     * @param userNos
     * @return
     */
    List<Distribution> listByUserNos(List<Long> userNos);
}
