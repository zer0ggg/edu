package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.UserLecturerAttentionEditREQ;
import com.roncoo.education.user.common.req.UserLecturerAttentionListREQ;
import com.roncoo.education.user.common.req.UserLecturerAttentionSaveREQ;
import com.roncoo.education.user.common.resp.UserLecturerAttentionListRESP;
import com.roncoo.education.user.common.resp.UserLecturerAttentionViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserLecturerAttentionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户关注讲师 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/lecturer/attention")
@Api(value = "user-用户关注讲师", tags = {"user-用户关注讲师"})
public class PcUserLecturerAttentionController {

    @Autowired
    private PcUserLecturerAttentionBiz biz;

    @ApiOperation(value = "用户关注讲师列表", notes = "用户关注讲师列表")
    @PostMapping(value = "/list")
    public Result<Page<UserLecturerAttentionListRESP>> list(@RequestBody UserLecturerAttentionListREQ userLecturerAttentionListREQ) {
        return biz.list(userLecturerAttentionListREQ);
    }

    @ApiOperation(value = "用户关注讲师添加", notes = "用户关注讲师添加")
    @PostMapping(value = "/save")
    @SysLog(value = "用户关注讲师添加")
    public Result<String> save(@RequestBody UserLecturerAttentionSaveREQ userLecturerAttentionSaveREQ) {
        return biz.save(userLecturerAttentionSaveREQ);
    }

    @ApiOperation(value = "用户关注讲师查看", notes = "用户关注讲师查看")
    @GetMapping(value = "/view")
    public Result<UserLecturerAttentionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "用户关注讲师修改", notes = "用户关注讲师修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "用户关注讲师修改")
    public Result<String> edit(@RequestBody UserLecturerAttentionEditREQ userLecturerAttentionEditREQ) {
        return biz.edit(userLecturerAttentionEditREQ);
    }

    @ApiOperation(value = "用户关注讲师删除", notes = "用户关注讲师删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "用户关注讲师删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
