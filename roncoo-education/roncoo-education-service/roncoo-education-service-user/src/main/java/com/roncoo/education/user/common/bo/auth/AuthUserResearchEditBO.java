package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户调研信息
 * </p>
 *
 * @author wujing
 * @date 2020-10-21
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthUserResearchEditBO", description="用户调研信息")
public class AuthUserResearchEditBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "内容")
    private String content;
}
