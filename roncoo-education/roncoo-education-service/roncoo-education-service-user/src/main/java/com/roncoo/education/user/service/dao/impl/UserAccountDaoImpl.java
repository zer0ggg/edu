package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserAccountMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample.Criteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class UserAccountDaoImpl implements UserAccountDao {
    @Autowired
    private UserAccountMapper userAccountMapper;

    @Override
    public int save(UserAccount record) {
        record.setId(IdWorker.getId());
        return this.userAccountMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.userAccountMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserAccount record) {
        return this.userAccountMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(UserAccount record, UserAccountExample example) {
        return this.userAccountMapper.updateByExampleSelective(record, example);
    }

    @Override
    public UserAccount getById(Long id) {
        return this.userAccountMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserAccount> listForPage(int pageCurrent, int pageSize, UserAccountExample example) {
        int count = this.userAccountMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserAccount>(count, totalPage, pageCurrent, pageSize, this.userAccountMapper.selectByExample(example));
    }

    @Override
    public UserAccount getByUserNo(Long userNo) {
        UserAccountExample example = new UserAccountExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        List<UserAccount> list = this.userAccountMapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public UserAccount getByUserNoAndCheck(Long userNo) {
        UserAccount userAccount = getByUserNo(userNo);
        if (ObjectUtil.isNull(userAccount)) {
            log.debug("找不到账户信息!");
            return null;
        }
        if (!StatusIdEnum.YES.getCode().equals(userAccount.getStatusId())) {
            log.debug("账户不可用!");
            return null;
        }
        // 判断sign
        String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
        if (!userAccount.getSign().equals(sign)) {
            log.error("账户异常 , sign不正确, 用户编号={}", userAccount.getUserNo());
            return null;
        }
        return userAccount;
    }

    @Override
    public int updateByUserNo(UserAccount record) {
        UserAccountExample example = new UserAccountExample();
        example.createCriteria().andUserNoEqualTo(record.getUserNo());
        return this.userAccountMapper.updateByExampleSelective(record, example);
    }

    @Override
    public UserAccount getByUserNoAndStatusId(Long userNo, Integer statusId) {
        UserAccountExample example = new UserAccountExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andStatusIdEqualTo(statusId);
        List<UserAccount> list = this.userAccountMapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }
}
