package com.roncoo.education.user.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户教育信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthUserExtViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
