package com.roncoo.education.user.common.req;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 分销信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionListREQ", description="分销信息列表")
public class DistributionListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "是否允许推广下级")
    private Integer isAllowInvitation;

    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
