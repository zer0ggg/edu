package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserRecommended;
import com.roncoo.education.user.feign.qo.UserRecommendedQO;
import com.roncoo.education.user.feign.vo.UserRecommendedVO;
import com.roncoo.education.user.service.feign.biz.FeignUserRecommendedBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户推荐信息
 *
 * @author wujing
 * @date 2020-07-15
 */
@RestController
public class FeignUserRecommendedController extends BaseController implements IFeignUserRecommended{

    @Autowired
    private FeignUserRecommendedBiz biz;

	@Override
	public Page<UserRecommendedVO> listForPage(@RequestBody UserRecommendedQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserRecommendedQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserRecommendedQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserRecommendedVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
