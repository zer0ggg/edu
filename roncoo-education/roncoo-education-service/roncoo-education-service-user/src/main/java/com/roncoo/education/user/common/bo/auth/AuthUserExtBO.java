package com.roncoo.education.user.common.bo.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserExtBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键", required = true)
    private Long id;
    /**
     * 性别(1男，2女，3保密)
     */
    @ApiModelProperty(value = "性别(1男，2女，3保密)", required = false)
    private Integer sex;
    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄", required = false)
    private Integer age;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称", required = false)
    private String nickname;
    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址", required = false)
    private String headImgUrl;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", required = false)
    private String remark;
    /**
     * 是否是推荐（1：是；0：否）
     */
    @ApiModelProperty(value = "是否是推荐（1：是；0：否）")
    private Integer isRecommended;

    /**
     * 身份证姓名
     */
    @ApiModelProperty(value = "身份证姓名", required = false)
    private String idCardName;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码", required = false)
    private String idCardNo;

    /**
     * 身份证正面
     */
    @ApiModelProperty(value = "身份证正面", required = false)
    private String idCardFrontImg;

    /**
     * 身份证反面
     */
    @ApiModelProperty(value = "身份证反面", required = false)
    private String idCardAfterImg;
}
