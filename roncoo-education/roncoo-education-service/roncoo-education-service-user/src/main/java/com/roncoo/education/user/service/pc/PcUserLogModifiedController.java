package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserLogModifiedEditREQ;
import com.roncoo.education.user.common.req.UserLogModifiedListREQ;
import com.roncoo.education.user.common.req.UserLogModifiedSaveREQ;
import com.roncoo.education.user.common.resp.UserLogModifiedListRESP;
import com.roncoo.education.user.common.resp.UserLogModifiedViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserLogModifiedBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户修改日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/log/modified")
@Api(value = "user-用户修改日志", tags = {"user-用户修改日志"})
public class PcUserLogModifiedController {

    @Autowired
    private PcUserLogModifiedBiz biz;

    @ApiOperation(value = "用户修改日志列表", notes = "用户修改日志列表")
    @PostMapping(value = "/list")
    public Result<Page<UserLogModifiedListRESP>> list(@RequestBody UserLogModifiedListREQ userLogModifiedListREQ) {
        return biz.list(userLogModifiedListREQ);
    }

    @ApiOperation(value = "用户修改日志添加", notes = "用户修改日志添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserLogModifiedSaveREQ userLogModifiedSaveREQ) {
        return biz.save(userLogModifiedSaveREQ);
    }

    @ApiOperation(value = "用户修改日志查看", notes = "用户修改日志查看")
    @GetMapping(value = "/view")
    public Result<UserLogModifiedViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户修改日志修改", notes = "用户修改日志修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserLogModifiedEditREQ userLogModifiedEditREQ) {
        return biz.edit(userLogModifiedEditREQ);
    }

    @ApiOperation(value = "用户修改日志删除", notes = "用户修改日志删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
