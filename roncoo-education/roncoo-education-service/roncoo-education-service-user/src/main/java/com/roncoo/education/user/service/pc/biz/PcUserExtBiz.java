package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.community.feign.interfaces.IFeignBlogger;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigSysVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.common.req.UserExtEditREQ;
import com.roncoo.education.user.common.req.UserExtListREQ;
import com.roncoo.education.user.common.req.UserExtUpdateStatusREQ;
import com.roncoo.education.user.common.resp.UserExtListRESP;
import com.roncoo.education.user.common.resp.UserExtViewRESP;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExtExample.Criteria;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Component
public class PcUserExtBiz extends BaseBiz {


    /**
     * 内存中缓存记录行数
     */
    private static final Integer REPORT_CACHE_NUM = 100;
    private static final Integer NUM_PER_PAGE = 1000;
    private static final Integer REPORT_MAX_TOTAL = 100000;
    private static final Integer SHEET_MAX_NUM = 60000;


    @Autowired
    private UserExtDao dao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserLogLoginDao userLogLoginDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private LecturerAuditDao lecturerAuditDao;
    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private IFeignBlogger feignBlogger;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private MyRedisTemplate myRedisTemplate;
    @Autowired
    protected HttpServletResponse response;

    /**
     * 用户教育信息列表
     *
     * @param userExtListREQ 用户教育信息分页查询参数
     * @return 用户教育信息分页查询结果
     */
    public Result<Page<UserExtListRESP>> list(UserExtListREQ userExtListREQ) {
        UserExtExample example = getPageExample(userExtListREQ);
        Criteria c = example.createCriteria();
        Page<UserExt> page = dao.listForPage(userExtListREQ.getPageCurrent(), userExtListREQ.getPageSize(), example);
        Page<UserExtListRESP> respPage = PageUtil.transform(page, UserExtListRESP.class);
        for (UserExtListRESP resp : respPage.getList()) {
            User user = userDao.getByUserNo(resp.getUserNo());
            if (!ObjectUtil.isNull(user)) {
                if (StringUtils.hasText(user.getUniconId())) {
                    resp.setIsBinding(1);
                } else {
                    resp.setIsBinding(0);
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 用户教育信息查看
     *
     * @param userNo 用户编号
     * @return 用户教育信息
     */
    public Result<UserExtViewRESP> view(Long userNo) {
        return Result.success(BeanUtil.copyProperties(dao.getByUserNo(userNo), UserExtViewRESP.class));
    }

    /**
     * 用户教育信息修改
     *
     * @param userExtEditREQ 用户教育信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserExtEditREQ userExtEditREQ) {
        UserExt record = BeanUtil.copyProperties(userExtEditREQ, UserExt.class);
        if (userExtEditREQ.getExpireTime() != null) {
            record.setExpireTime(DateUtil.parse(userExtEditREQ.getExpireTime()));
        }
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户教育信息删除
     *
     * @param userNo 用户编号
     * @return 删除结果
     */
    public Result<String> resetError(Long userNo) {
        // 根据用户编号获取用户当天登录错误
        Date startgmtCreate = DateUtil.parseDate(DateUtil.format(new Date()) + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        List<UserLogLogin> list = userLogLoginDao.listByUserAndGmtCreateAndLoginStatus(userNo, startgmtCreate, LoginStatusEnum.FAIL.getCode());
        if (CollectionUtil.isEmpty(list)) {
            return Result.success("重置成功");
        }
        for (UserLogLogin userLogLogin : list) {
            userLogLoginDao.deleteById(userLogLogin.getId());
        }
        return Result.success("重置成功");
    }

    /**
     * 缓存用户信息--发送站内信使用
     */
    public void cacheUserForMsg() {
        int pageSize = 1000;
        Page<UserExtMsgVO> page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), 1, pageSize);
        // 缓存key条数
        set(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), page.getTotalPage(), 120);
        // 缓存用户
        for (int i = 1; i < page.getTotalPage() + 1; i++) {
            page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), i, pageSize);
            // 缓存，2个小时
            set(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, page.getList(), 120);
        }
    }

    /**
     * 单位分钟
     */
    private <T> T set(String key, T t, Integer time) {
        if (t != null) {
            myRedisTemplate.set(key, JSUtil.toJSONString(t), time, TimeUnit.MINUTES);
        }
        return t;
    }

    /**
     * 修改状态
     *
     * @param userExtUpdateStatusREQ 用户教育信息修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(UserExtUpdateStatusREQ userExtUpdateStatusREQ) {
        if (ObjectUtil.isNull(dao.getById(userExtUpdateStatusREQ.getId()))) {
            return Result.error("用户教育信息不存在");
        }
        UserExt record = BeanUtil.copyProperties(userExtUpdateStatusREQ, UserExt.class);
        if (dao.updateById(record) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }


    /**
     * @param userExtListREQ
     * @return 条件查询
     */
    public UserExtExample getPageExample(UserExtListREQ userExtListREQ) {

        UserExtExample example = new UserExtExample();
        Criteria c = example.createCriteria();
        // 用户编号
        if (userExtListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(userExtListREQ.getUserNo());
        }
        // 用户手机
        if (StringUtils.hasText(userExtListREQ.getMobile())) {
            c.andMobileLike(PageUtil.like(userExtListREQ.getMobile()));
        }
        // 是否为会员
        if (userExtListREQ.getIsVip() != null) {
            c.andIsVipEqualTo(userExtListREQ.getIsVip());
        }
        // 状态
        if (userExtListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(userExtListREQ.getStatusId());
        }
        // 昵称
        if (StringUtils.hasText(userExtListREQ.getNickname())) {
            c.andNicknameLike(PageUtil.like(userExtListREQ.getNickname()));
        }
        // 是否推荐
        if (userExtListREQ.getIsRecommended() != null) {
            c.andIsRecommendedEqualTo(userExtListREQ.getIsRecommended());
        }
        if (StringUtils.hasText(userExtListREQ.getBeginRegisterTime())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(userExtListREQ.getBeginRegisterTime(), "yyyy-MM-dd"));
        }
        // 结束时间
        if (StringUtils.hasText(userExtListREQ.getEndRegisterTime())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(userExtListREQ.getEndRegisterTime(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" id desc ");
        return example;
    }

    /**
     * 用户教育信息导出信息
     */
    public void export(UserExtListREQ userExtListREQ) {

        logger.info("导出报表");
        logger.info("导出参数：{}", userExtListREQ);
        try {

            String fileName = "用户教育信息" + DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN);
            String[] names = {"用户编号", "手机号", "昵称", "用户角色", "用户类型", "状态", "注册时间"};
            Integer[] widths = {10, 10, 5, 5, 5, 5, 10};
            //当前sheet行数
            int rowIndex = 0;

            final SXSSFWorkbook workbook = new SXSSFWorkbook();
            SXSSFSheet sheet = workbook.createSheet("1表");

            //创建第一行
            SXSSFRow row = sheet.createRow(rowIndex);
            //创建头部
            int nameLength = names.length;
            for (int i = 0; i < nameLength; i++) {
                row.createCell(i).setCellValue(names[i]);
                sheet.setColumnWidth(i, widths[i] * 512);
            }

            //第一次获取数据
            boolean isGo = true;
            userExtListREQ.setPageSize(NUM_PER_PAGE);
            Page<UserExt> page = null;
            do {
                try {
                    UserExtExample example = getPageExample(userExtListREQ);
                    page = dao.listForPage(userExtListREQ.getPageCurrent(), userExtListREQ.getPageSize(), example);
                } catch (Exception e) {
                    logger.info("获取用户教育信息失败！", e);
                }
                // 第一次获取数据判空
                if (userExtListREQ.getPageCurrent() <= 1) {
                    boolean isError = false;
                    if (StringUtils.isEmpty(page) || page.getList() == null || page.getList().isEmpty()) {
                        logger.error("没有需要导出的用户教育信息!");
                        isError = true;
                        sheet.createRow(++rowIndex).createCell(0).setCellValue("没有需要导出的用户教育信息!");
                    }
                    // 判断中记录数是否大于最大限额
                    assert page != null;
                    if (REPORT_MAX_TOTAL.compareTo(page.getTotalCount()) < 0) {
                        logger.error("选中的用户教育信息据大于" + REPORT_MAX_TOTAL + "条");
                        isError = true;
                        sheet.createRow(++rowIndex).createCell(0).setCellValue("选中的用户教育信息据大于" + REPORT_MAX_TOTAL + "条");
                    }
                    if (isError) {
                        writeExcel(response, workbook, fileName);
                    }
                }

                assert page != null;
                int totalCount = page.getList().size();
                List<UserExt> resultList = page.getList();
                String StringUserTypeEnum;   //用户角色 接受枚举用户
                String StringStatusIdEnum;   //状态
                String StringIsVipEnum;  //用户类型

                for (int i = 0; i < totalCount; i++) {
                    row = sheet.createRow(++rowIndex);
                    UserExt userExt = resultList.get(i);
                    //判断用户角色
                    if (UserTypeEnum.USER.getCode().equals(userExt.getUserType())) {
                        StringUserTypeEnum = UserTypeEnum.USER.getDesc();
                    } else {
                        StringUserTypeEnum = UserTypeEnum.LECTURER.getDesc();
                    }
                    //判断 用户状态
                    if (StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
                        StringStatusIdEnum = StatusIdEnum.YES.getDesc();
                    } else {
                        StringStatusIdEnum = StatusIdEnum.NO.getDesc();
                    }
                    //判断用户类型
                    if (IsVipEnum.NO.getCode().equals(userExt.getIsVip())) {
                        StringIsVipEnum = IsVipEnum.NO.getDesc();
                    } else {
                        StringIsVipEnum = IsVipEnum.YES.getDesc();
                    }
                    //用户编号
                    row.createCell(0).setCellValue(userExt.getUserNo().toString());
                    //手机号
                    row.createCell(1).setCellValue(userExt.getMobile());
                    //昵称
                    row.createCell(2).setCellValue(userExt.getNickname());
                    //用户角色
                    row.createCell(3).setCellValue(StringUserTypeEnum);
                    //用户类型
                    row.createCell(4).setCellValue(StringIsVipEnum);
                    //状态
                    row.createCell(5).setCellValue(StringStatusIdEnum);
                    //创建时间
                    row.createCell(6).setCellValue(DateUtil.format(userExt.getGmtCreate(), DatePattern.NORM_DATETIME_PATTERN));

                    // 每当行数达到设置的值就刷新数据到硬盘,以清理内存
                    if (rowIndex % REPORT_CACHE_NUM == 0) {
                        try {
                            sheet.flushRows();
                        } catch (IOException e) {
                            logger.error("用户教育信息导出,刷新数据到硬盘失败！");
                        }
                    }

                    // 当记录达到每页的sheet记录数大小，创建新的sheet
                    if (rowIndex % SHEET_MAX_NUM == 0) {
                        logger.info("用户教育信息导出，创建新sheet:{}", workbook.getNumberOfSheets() + 1);
                        sheet = workbook.createSheet((workbook.getNumberOfSheets() + 1) + "表");
                        rowIndex = 0;
                        row = sheet.createRow(rowIndex);
                        // 创建sheet头部
                        nameLength = names.length;
                        for (int j = 0; j < nameLength; j++) {
                            row.createCell(j).setCellValue(names[j]);
                            sheet.setColumnWidth(j, widths[j] * 512);
                        }
                    }
                }
                if (page.getPageCurrent() >= page.getTotalPage()) {
                    isGo = false;
                }
                userExtListREQ.setPageCurrent(page.getPageCurrent() + 1);
            } while (isGo);
            writeExcel(response, workbook, fileName);
        } catch (Exception e) {
            logger.error("错误信息", e);
        }
    }

    /**
     * 写入文件
     */
    private void writeExcel(HttpServletResponse response, SXSSFWorkbook workbook, String fileName) {
        // 设置强制下载不打开
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");

        // 设置文件名
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes(), StandardCharsets.ISO_8859_1) + ".xlsx");

        ServletOutputStream outputStream = null;
        // 写入数据
        try {
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("日志导出失败！", e);
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                logger.error("日志导出失败", e);
            }
        }
    }

    /**
     * 批量导入用户
     *
     * @param
     * @param
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> uploadExcel(MultipartFile file) {
        XSSFWorkbook wookbook = null;
        try {
            wookbook = new XSSFWorkbook(file.getInputStream());
            XSSFSheet sheet = wookbook.getSheetAt(0);
            // 		指的行数，一共有多少行+
            int rows = sheet.getLastRowNum();
            for (int i = 1; i <= rows; i++) {
                // 读取左上端单元格
                XSSFRow row = sheet.getRow(i);
                // 行不为空
                String mobile = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 手机号
                if (StringUtils.isEmpty(mobile)) {
                    continue;
                }

                String nickname = ExcelToolUtil.getXValue(row.getCell(1)).trim();// 用户昵称
                if (StringUtils.isEmpty(nickname)) {
                    continue;
                }
                String mobilePsw = ExcelToolUtil.getXValue(row.getCell(2)).trim();// 登录密码
                if (StringUtils.isEmpty(mobilePsw)) {
                    continue;
                }
                String userType = ExcelToolUtil.getXValue(row.getCell(4)).trim();// 用户类型(1用户，2讲师)
                if (StringUtils.isEmpty(userType)) {
                    continue;
                }
                // 输入规范不正确跳出不添加
                if (!UserTypeEnum.USER.getCode().equals(Integer.valueOf(userType)) && !UserTypeEnum.LECTURER.getCode().equals(Integer.valueOf(userType))) {
                    continue;
                }

                // 用户其他信息
                UserExt userExt = dao.getByMobile(mobile);
                UserExt ext = new UserExt();
                ext.setUserType(Integer.valueOf(userType));
                ext.setMobile(mobile);
                ext.setNickname(nickname);
                // 备注
                String remark = ExcelToolUtil.getXValue(row.getCell(3)).trim();
                if (StringUtils.hasText(remark)) {
                    ext.setRemark(remark);
                }
                // 用户未注册、新增
                if (ObjectUtil.isNull(userExt)) {
                    User user = register(mobile, mobilePsw);
                    // 用户默认头像
                    SysConfigVO SysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
                    if (org.springframework.util.StringUtils.isEmpty(SysVO.getConfigValue())) {
                        logger.error("未设置平台域名");
                        continue;
                    }
                    ext.setHeadImgUrl(SysVO.getConfigValue() + "/friend.jpg");
                    ext.setUserNo(user.getUserNo());
                    dao.save(ext);
                } else {
                    ext.setId(userExt.getId());
                    dao.updateById(ext);
                    ext.setUserNo(userExt.getUserNo());
                }
                if (UserTypeEnum.LECTURER.getCode().equals(userType)) {
                    lecturer(ext, row);
                }
            }
            return Result.success("导入成功");
        } catch (IOException e) {
            logger.error("导入失败, {}", e);
            return Result.error("导入失败");
        }
    }

    private void lecturer(UserExt userExt, XSSFRow row) {
        ConfigSysVO sysVO = feignSysConfig.getSys();
        if (ObjectUtil.isNull(sysVO)) {
            return;
        }
        // 讲师简介
        String introduce = ExcelToolUtil.getXValue(row.getCell(5)).trim();
        // 讲师邮箱
        String lecturerEmail = ExcelToolUtil.getXValue(row.getCell(6)).trim();
        LecturerAudit infoAudit = lecturerAuditDao.getByLecturerUserNo(userExt.getUserNo());
        if (ObjectUtil.isNull(infoAudit)) {
            infoAudit = new LecturerAudit();
            infoAudit.setIntroduce(introduce);
            infoAudit.setLecturerEmail(lecturerEmail);
            infoAudit.setLecturerName(userExt.getNickname());
            infoAudit.setLecturerUserNo(userExt.getUserNo());
            infoAudit.setAuditStatus(AuditStatusEnum.SUCCESS.getCode());
            infoAudit.setLecturerProportion(sysVO.getLecturerDefaultProportion());
            infoAudit.setId(IdWorker.getId());
            lecturerAuditDao.save(infoAudit);
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(userExt.getUserNo());
            if (ObjectUtil.isNull(lecturer)) {
                lecturer = BeanUtil.copyProperties(infoAudit, Lecturer.class);
                lecturerDao.save(lecturer);

                userExt.setUserType(UserTypeEnum.LECTURER.getCode());
                dao.updateById(userExt);
            }
        }
    }

    private User register(String mobile, String password) {
        // 用户基本信息
        User user = new User();
        user.setUserNo(NOUtil.getUserNo());
        user.setMobile(mobile);
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + password));
        userDao.save(user);

        UserAccount userAccount = new UserAccount();
        userAccount.setUserNo(user.getUserNo());
        userAccount.setTotalIncome(BigDecimal.valueOf(0));
        userAccount.setHistoryMoney(BigDecimal.valueOf(0));
        userAccount.setEnableBalances(BigDecimal.valueOf(0));
        userAccount.setFreezeBalances(BigDecimal.valueOf(0));
        userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
        userAccountDao.save(userAccount);

        // 添加博主信息
        BloggerQO bloggerQO = new BloggerQO();
        bloggerQO.setUserNo(user.getUserNo());
        bloggerQO.setMobile(user.getMobile());
        feignBlogger.save(bloggerQO);
        return user;
    }

}

