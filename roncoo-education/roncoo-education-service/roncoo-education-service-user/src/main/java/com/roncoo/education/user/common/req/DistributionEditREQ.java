package com.roncoo.education.user.common.req;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 分销信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionEditREQ", description="分销信息修改")
public class DistributionEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "是否允许推广下级")
    private Integer isAllowInvitation;

    @ApiModelProperty(value = "推广比例")
    @NotNull(message = "请输入推广比例")
    private BigDecimal spreadProfit;

    @ApiModelProperty(value = "邀请比例")
    @NotNull(message = "请输入邀请比例")
    private BigDecimal inviteProfit;

    @ApiModelProperty(value = "备注")
    private String remark;
}
