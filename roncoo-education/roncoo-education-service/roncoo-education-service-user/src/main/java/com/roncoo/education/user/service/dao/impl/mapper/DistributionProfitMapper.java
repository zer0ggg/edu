package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DistributionProfitMapper {
    int countByExample(DistributionProfitExample example);

    int deleteByExample(DistributionProfitExample example);

    int deleteByPrimaryKey(Long id);

    int insert(DistributionProfit record);

    int insertSelective(DistributionProfit record);

    List<DistributionProfit> selectByExample(DistributionProfitExample example);

    DistributionProfit selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") DistributionProfit record, @Param("example") DistributionProfitExample example);

    int updateByExample(@Param("record") DistributionProfit record, @Param("example") DistributionProfitExample example);

    int updateByPrimaryKeySelective(DistributionProfit record);

    int updateByPrimaryKey(DistributionProfit record);
}