package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单支付产品
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthOrderPayProductBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "应付金额")
    private BigDecimal pricePayable;

    @ApiModelProperty(value = "实付金额")
    private BigDecimal pricePaid;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal priceDiscount;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "注册时间")
    private Date registerTime;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "附加信息")
    private String attach;
    /**
     * 活动ID（如果是优惠券就是优惠券ID，如果是秒杀该字段为秒杀ID）
     */
    private Long actTypeId;
    /**
     * 活动类型(1优惠券,2秒杀)
     */
    private Integer actType;
}
