package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthUserRecommendedPageBO;
import com.roncoo.education.user.common.dto.auth.AuthUserRecommendedPageDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserRecommendedBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户推荐信息 UserApi接口
 *
 * @author wujing
 * @date 2020-07-15
 */
@Api(tags = "API-AUTH-用户推荐信息")
@RestController
@RequestMapping("/user/auth/user/recommended")
public class AuthUserRecommendedController {

    @Autowired
    private AuthUserRecommendedBiz biz;

    /**
     * 用户推荐人信息列出接口
     */
    @ApiOperation(value = "用户推荐人信息列出接口", notes = "用户推荐人信息列出接口")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<Page<AuthUserRecommendedPageDTO>> listForPage(@RequestBody AuthUserRecommendedPageBO authUserRecommendedPageBO) {
        return biz.listForPage(authUserRecommendedPageBO);
    }

}
