package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.ReferralCodeUtil;
import com.roncoo.education.user.feign.qo.UserQO;
import com.roncoo.education.user.feign.vo.UserVO;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户基本信息
 *
 * @author wujing
 */
@Component
public class FeignUserBiz {

    @Autowired
    private UserDao dao;
    @Autowired
    private UserExtDao userExtDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<UserVO> listForPage(UserQO qo) {
        UserExample example = new UserExample();
        Criteria criteria = example.createCriteria();
        if (StringUtils.isNotEmpty(qo.getMobile())) {
            criteria.andMobileLike(PageUtil.like(qo.getMobile()));
        }
        example.setOrderByClause(" id desc ");
        Page<User> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<UserVO> listForPage = PageUtil.transform(page, UserVO.class);
        for (UserVO vo : listForPage.getList()) {
            UserExt userExt = userExtDao.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExt)) {
                vo.setNickname(userExt.getNickname());
            }
        }
        return listForPage;
    }

    public int save(UserQO qo) {
        User record = BeanUtil.copyProperties(qo, User.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public UserVO getById(Long id) {
        User record = dao.getById(id);
        return BeanUtil.copyProperties(record, UserVO.class);
    }

    public int updateById(UserQO qo) {
        User record = BeanUtil.copyProperties(qo, User.class);
        return dao.updateById(record);
    }

    public UserVO getByMobile(String mobile) {
        User record = dao.getByMobile(mobile);
        return BeanUtil.copyProperties(record, UserVO.class);
    }

    public UserVO getByUserNo(Long userNo) {
        User record = dao.getByUserNo(userNo);
        return BeanUtil.copyProperties(record, UserVO.class);
    }

    /**
     * 小程序解除绑定
     *
     * @param userNo
     * @return
     */
    public int updateUniconId(Long userNo) {
        User record = dao.getByUserNo(userNo);
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("找不到用户信息");
        }
        record.setUniconId("");
        // 解除绑定，清空该用户token
        if (myRedisTemplate.hasKey(Constants.Platform.XCX.concat(record.getUserNo().toString()))) {
            myRedisTemplate.delete(Constants.Platform.XCX.concat(record.getUserNo().toString()));
        }
        return dao.updateById(record);
    }

    /**
     * 推荐码校验唯一性
     *
     * @author wuyun
     */
    @SuppressWarnings("unused")
    private String referralCode(String referralCode) {
        // 查询生成的推荐码在数据库是否存在，不存在就返回，存在重新生成再次判断
        UserExt userExt = userExtDao.getByReferralCode(referralCode);
        if (ObjectUtil.isNull(userExt)) {
            return referralCode;
        }
        return referralCode(ReferralCodeUtil.getStringRandom());
    }

}
