package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.URLUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.UserTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.JWTUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigSysVO;
import com.roncoo.education.user.common.req.LecturerListREQ;
import com.roncoo.education.user.common.req.LecturerProportionSetREQ;
import com.roncoo.education.user.common.req.LecturerSaveREQ;
import com.roncoo.education.user.common.req.LecturerUpdateStatusIdREQ;
import com.roncoo.education.user.common.resp.LecturerListRESP;
import com.roncoo.education.user.common.resp.LecturerViewRESP;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Component
public class PcLecturerBiz extends BaseBiz {

    @Autowired
    private LecturerDao dao;
    @Autowired
    private LecturerAuditDao lecturerAuditDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 讲师信息列表
     *
     * @param lecturerListREQ 讲师信息分页查询参数
     * @return 讲师信息分页查询结果
     */
    public Result<Page<LecturerListRESP>> list(LecturerListREQ lecturerListREQ) {
        LecturerExample example = new LecturerExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotBlank(lecturerListREQ.getLecturerMobile())) {
            c.andLecturerMobileEqualTo(lecturerListREQ.getLecturerMobile());
        }
        if (StringUtils.isNotEmpty(lecturerListREQ.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.like(lecturerListREQ.getLecturerName()));
        }
        if (lecturerListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(lecturerListREQ.getStatusId());
        }
        example.setOrderByClause(" sort asc, id desc ");
        Page<Lecturer> page = dao.listForPage(lecturerListREQ.getPageCurrent(), lecturerListREQ.getPageSize(), example);
        Page<LecturerListRESP> respPage = PageUtil.transform(page, LecturerListRESP.class);
        return Result.success(respPage);
    }

    /**
     * 讲师信息添加
     *
     * @param lecturerSaveREQ 讲师信息
     * @return 添加结果
     */
    public Result<String> save(LecturerSaveREQ lecturerSaveREQ) {
        Lecturer record = BeanUtil.copyProperties(lecturerSaveREQ, Lecturer.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 讲师信息查看
     *
     * @param id 主键ID
     * @return 讲师信息
     */
    public Result<LecturerViewRESP> view(Long id) {
        LecturerViewRESP resp = BeanUtil.copyProperties(dao.getById(id), LecturerViewRESP.class);
        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(resp.getLecturerUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            userAccount = new UserAccount();
            userAccount.setUserNo(resp.getLecturerUserNo());
            userAccount.setTotalIncome(BigDecimal.ZERO);
            userAccount.setHistoryMoney(BigDecimal.ZERO);
            userAccount.setEnableBalances(BigDecimal.ZERO);
            userAccount.setFreezeBalances(BigDecimal.ZERO);
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.save(userAccount);
        }
        resp.setUserAccountVO(BeanUtil.copyProperties(userAccount, UserAccountVO.class));
        return Result.success(resp);
    }

    /**
     * 讲师状态
     *
     * @param lecturerUpdateStatusIdREQ
     * @return
     */
    public Result<String> lecturerStatus(LecturerUpdateStatusIdREQ lecturerUpdateStatusIdREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(lecturerUpdateStatusIdREQ.getStatusId()))) {
            return Result.error("讲师状态不正确");
        }
        Lecturer lecturer = dao.getById(lecturerUpdateStatusIdREQ.getId());
        UserExt userExt = userExtDao.getByUserNo(lecturer.getLecturerUserNo());
        // 禁用讲师，变为普通用户
        if (StatusIdEnum.NO.getCode().equals(lecturerUpdateStatusIdREQ.getStatusId())) {
            userExt.setUserType(UserTypeEnum.USER.getCode());
            userExtDao.updateById(userExt);
        }
        // 禁用讲师，变为讲师用户
        if (StatusIdEnum.YES.getCode().equals(lecturerUpdateStatusIdREQ.getStatusId())) {
            userExt.setUserType(UserTypeEnum.LECTURER.getCode());
            userExtDao.updateById(userExt);
        }
        return edit(BeanUtil.copyProperties(lecturerUpdateStatusIdREQ, Lecturer.class));
    }

    /**
     * 讲师信息修改
     *
     * @param record 讲师信息修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> edit(Lecturer record) {
        int lecturerNum = dao.updateById(record);
        if (lecturerNum < 1) {
            throw new BaseException("讲师信息表更新失败");
        }
        int lecturerAuditNum = lecturerAuditDao.updateById(BeanUtil.copyProperties(record, LecturerAudit.class));
        if (lecturerAuditNum < 1) {
            throw new BaseException("讲师信息表更新失败");
        }
        return Result.success("更新成功");
    }

    /**
     * 设置讲师分成
     *
     * @param lecturerProportionSetREQ
     * @return
     */
    public Result<String> setLecturerProportion(LecturerProportionSetREQ lecturerProportionSetREQ) {
        if (ObjectUtil.isNull(lecturerProportionSetREQ.getId())) {
            return Result.error("讲师ID不能为空");
        }
        if (ObjectUtil.isNull(lecturerProportionSetREQ.getLecturerProportion())) {
            return Result.error("讲师分成不能为空");
        }
        if (BigDecimal.ZERO.compareTo(lecturerProportionSetREQ.getLecturerProportion()) > 0) {
            return Result.error("讲师分成不能小于零");
        }
        ConfigSysVO sysVO = feignSysConfig.getSys();
        if (ObjectUtil.isNull(sysVO)) {
            return Result.error("获取系统配置失败");
        }
        if (ObjectUtil.isNull(sysVO.getLecturerMaxProportion())) {
            return Result.error("获取讲师默认最大分成失败");
        }
        BigDecimal maxProportion = sysVO.getLecturerMaxProportion().multiply(BigDecimal.valueOf(100));
        if (maxProportion.compareTo(lecturerProportionSetREQ.getLecturerProportion()) < 0) {
            return Result.error("讲师分成不能大于最大值" + maxProportion);
        }
        if (BigDecimal.valueOf(100L).compareTo(lecturerProportionSetREQ.getLecturerProportion()) < 0) {
            return Result.error("讲师分成不能大于100");
        }
        lecturerProportionSetREQ.setLecturerProportion(lecturerProportionSetREQ.getLecturerProportion().divide(BigDecimal.valueOf(100)));
        return edit(BeanUtil.copyProperties(lecturerProportionSetREQ, Lecturer.class));
    }

    /**
     * 讲师信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 跳转讲师中心
     *
     * @param lecturerUserNo 讲师用户编号
     * @return 访问地址
     */
    public Result<String> redirectLecturerCenter(Long lecturerUserNo) {
        ConfigSysVO sysVO = feignSysConfig.getSys();
        if (org.springframework.util.StringUtils.isEmpty(sysVO.getDomain())) {
            return Result.error("请设置平台域名");
        }
        String token = JWTUtil.create(lecturerUserNo, JWTUtil.DATE, Constants.Platform.PC);
        // 登录成功，存入缓存，单点登录使用
        myRedisTemplate.set(Constants.Platform.PC.concat(lecturerUserNo.toString()), token, 1, TimeUnit.DAYS);
        return Result.success(sysVO.getDomain() + "/login?t=" + URLUtil.encode(token, StandardCharsets.UTF_8));
    }
}
