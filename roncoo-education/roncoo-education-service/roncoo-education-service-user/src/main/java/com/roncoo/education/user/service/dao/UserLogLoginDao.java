package com.roncoo.education.user.service.dao;

import cn.hutool.core.date.DateTime;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLogin;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLoginExample;

import java.util.Date;
import java.util.List;

public interface UserLogLoginDao {
    int save(UserLogLogin record);

    int deleteById(Long id);

    int updateById(UserLogLogin record);

    UserLogLogin getById(Long id);

    Page<UserLogLogin> listForPage(int pageCurrent, int pageSize, UserLogLoginExample example);

    /**
     * 获取当天该用户该状态的登录信息
     *
     * @param userNo
     * @param loginStatus
     * @return
     */
    Integer listByUserNoAndLonginStatus(Long userNo, Integer loginStatus);

    /**
     * 根据用户编号、时间、登录状态获得集合
     *
     * @param userNo
     * @param beginOfDay
     * @param endOfDay
     * @param loginStatus
     * @return
     */
    List<UserLogLogin> listByUserAndGmtCreateAndLoginStatus(Long userNo, Date beginOfDay, Integer loginStatus);
}
