package com.roncoo.education.user.service.feign.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.DistributionQO;
import com.roncoo.education.user.feign.vo.DistributionVO;
import com.roncoo.education.user.service.dao.DistributionDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Distribution;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample.Criteria;

/**
 * 分销信息
 *
 * @author wujing
 */
@Component
public class FeignDistributionBiz extends BaseBiz {

    @Autowired
    private DistributionDao dao;

	public Page<DistributionVO> listForPage(DistributionQO qo) {
	    DistributionExample example = new DistributionExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<Distribution> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, DistributionVO.class);
	}

	public int save(DistributionQO qo) {
		Distribution record = BeanUtil.copyProperties(qo, Distribution.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public DistributionVO getById(Long id) {
		Distribution record = dao.getById(id);
		return BeanUtil.copyProperties(record, DistributionVO.class);
	}

	public int updateById(DistributionQO qo) {
		Distribution record = BeanUtil.copyProperties(qo, Distribution.class);
		return dao.updateById(record);
	}

}
