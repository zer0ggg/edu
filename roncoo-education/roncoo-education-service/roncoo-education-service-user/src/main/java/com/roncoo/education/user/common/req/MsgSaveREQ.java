package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 站内信息
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "MsgSaveREQ", description = "站内信息添加")
public class MsgSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "短信标题", required = true)
    private String msgTitle;

    @ApiModelProperty(value = "短信内容", required = true)
    private String msgText;

    @ApiModelProperty(value = "是否定时发送（1是，0否）", required = true)
    private Integer isTimeSend;

    @ApiModelProperty(value = "发送时间")
    private Date sendTime;

    @ApiModelProperty(value = "是否置顶(1是;0否)", required = true)
    private Integer isTop;

    @ApiModelProperty(value = "是否发给部分用户(1是;0否)")
    private Integer isToPart;

    @ApiModelProperty(value = "部分用户的编号字符串，形式userNo1,userNo2")
    private String userNos;

}
