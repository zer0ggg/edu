package com.roncoo.education.user.common.pay.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.core.enums.DateFormatEnum;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.common.core.tools.HttpUtil;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 龙果支付工具
 *
 * @author Quanf 2020/4/2 11:35
 */
public class RoncooPayUtil {

    /**
     * 商品名称特殊字符过滤掉的正则表达式
     */
    private static final Pattern PRODUCT_NAME_PATTERN = Pattern.compile("[`~!@#$%^&*()+=|{}':;',_\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]");

    private static final Logger logger = LoggerFactory.getLogger(RoncooPayUtil.class);

    public static String pay(PayChannel payChannel, PayBO payBO, String payProductType) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("payKey", payChannel.getPayKey());
        paramMap.put("orderPrice", payBO.getOrderAmount());
        paramMap.put("outTradeNo", payBO.getOrderNo());
        paramMap.put("productType", payProductType);
        paramMap.put("orderTime", new SimpleDateFormat(DateFormatEnum.YYYYMMDDHHMMSS.getDateFormat()).format(new Date()));
        // 把商品名中的特殊字符过滤掉
        String productName = payBO.getGoodsName();
        productName = PRODUCT_NAME_PATTERN.matcher(productName).replaceAll("").trim();
        if (productName.length() > 15) {
            paramMap.put("productName", productName.substring(0, 15));
        } else {
            paramMap.put("productName", productName);
        }
        HttpServletRequest req = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        // 下单IP
        paramMap.put("orderIp", req.getHeader("HTTP_X_FORWARDED_FOR"));
        // 页面通知返回url
        paramMap.put("returnUrl", String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
        paramMap.put("notifyUrl", String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
        if (StringUtils.hasText(payBO.getOpenId())) {
            paramMap.put("openId", payBO.getOpenId());
        }
        paramMap.put("remark", payBO.getOrderNo());
        String sign = getSign(paramMap, payChannel.getPaySecret());
        paramMap.put("sign", sign);
        logger.debug("请求参数:{}", paramMap);
        String url = payChannel.getRequestUrl();
        logger.debug("请求地址:{}", url);
        String resp = HttpUtil.postForPay(url, paramMap);
        logger.debug("返回参数:{}", resp);
        return resp;
    }

    public static PayNotifyDTO verify(PayNotifyBO notifyBO, String paySecret) {
        Map<String, Object> paramMap = JSONObject.parseObject(notifyBO.getBody());
        logger.debug("龙果支付转换:[{}]", paramMap);
        PayNotifyDTO payNotifyDTO = new PayNotifyDTO();
        payNotifyDTO.setOrderNo(String.valueOf(paramMap.get("outTradeNo")));
        payNotifyDTO.setVerify(false);// 签名状态
        payNotifyDTO.setOrderStatus(OrderStatusEnum.WAIT.getCode());
        if (isRightSign(paramMap, paySecret, paramMap.get("sign").toString())) {
            logger.debug("龙果支付通知结果，验签成功!");
            payNotifyDTO.setVerify(true);
            String orderStatus = String.valueOf(paramMap.get("tradeStatus"));
            if ("FAILED".equals(orderStatus)) {
                payNotifyDTO.setOrderStatus(OrderStatusEnum.FAIL.getCode());
            }
            if ("SUCCESS".equals(orderStatus)) {
                payNotifyDTO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
            }
            payNotifyDTO.setResponseStr("SUCCESS");
        }
        return payNotifyDTO;
    }

    private static String getSign(Map<String, Object> paramMap, String paySecret) {
        if (paramMap.isEmpty()) {
            return "";
        }
        SortedMap<String, Object> smap = new TreeMap<>(paramMap);
        StringBuilder stringBuffer = new StringBuilder();
        for (Map.Entry<String, Object> m : smap.entrySet()) {
            String value = String.valueOf(m.getValue());
            if (StringUtils.hasText(value)) {
                stringBuffer.append(m.getKey()).append("=").append(value).append("&");
            }
        }
        stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
        logger.debug("签名原文:{}", stringBuffer.toString());
        String argPreSign = stringBuffer.append("&paySecret=").append(paySecret).toString();
        return MD5Util.MD5(argPreSign).toUpperCase();
    }

    /**
     * 验证商户签名
     *
     * @param paramMap  签名参数
     * @param paySecret 签名私钥
     * @param signStr   原始签名密文
     * @return 验签结果
     */
    public static boolean isRightSign(Map<String, Object> paramMap, String paySecret, String signStr) {
        if (!StringUtils.hasText(signStr)) {
            return false;
        }
        paramMap.remove("sign");
        String sign = getSign(paramMap, paySecret);
        return signStr.equals(sign);
    }


    /**
     * 订单查询
     *
     * @param outTradeNo 订单号
     * @param payChannel 支付配置
     * @return 订单状态
     */
    public static int orderQuery(String outTradeNo, PayChannel payChannel) {
        int orderStatus = OrderStatusEnum.WAIT.getCode();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("payKey", payChannel.getPayKey());
        paramMap.put("outTradeNo", outTradeNo);
        String sign = getSign(paramMap, payChannel.getPaySecret());
        paramMap.put("sign", sign);
        logger.debug("请求参数:{}", paramMap);
        String url = payChannel.getQueryUrl();
        logger.debug("请求地址:{}", url);
        String resp = HttpUtil.postForPay(url, paramMap);
        logger.debug("返回参数:{}", resp);
        if (StrUtil.isEmpty(resp)) {
            logger.info("订单查询结果为空");
            return orderStatus;
        }
        JSONObject jsonObject = JSON.parseObject(resp);
        Object resultCode = jsonObject.get("resultCode");
        if ("0000".equals(resultCode.toString())) {
            logger.info("龙果支付订单查询，通讯成功！");
            // 龙果支付状态
            String status = jsonObject.getString("orderStatus");
            if ("SUCCESS".equals(status)) {
                //交易成功
                orderStatus = OrderStatusEnum.SUCCESS.getCode();
            } else if ("FAILED".equals(status)) {
                //支付失败
                orderStatus = OrderStatusEnum.FAIL.getCode();
            } else if ("WAITING_PAYMENT".equals(status)) {
                //等待支付
                orderStatus = OrderStatusEnum.WAIT.getCode();
            } else if ("FINISH".equals(status)) {
                //交易完成
                orderStatus = OrderStatusEnum.SUCCESS.getCode();
            }
        }
        return orderStatus;
    }

}
