package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户账户信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserAccountListREQ", description="用户账户信息列表")
public class UserAccountListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "总收入")
    private BigDecimal totalIncome;

    @ApiModelProperty(value = "已提金额")
    private BigDecimal historyMoney;

    @ApiModelProperty(value = "可提余额")
    private BigDecimal enableBalances;

    @ApiModelProperty(value = "冻结金额")
    private BigDecimal freezeBalances;

    @ApiModelProperty(value = "sign")
    private String sign;

    @ApiModelProperty(value = "银行卡号")
    private String bankCardNo;

    @ApiModelProperty(value = "银行名称")
    private String bankName;

    @ApiModelProperty(value = "银行支行名称")
    private String bankBranchName;

    @ApiModelProperty(value = "银行开户名")
    private String bankUserName;

    @ApiModelProperty(value = "银行身份证号")
    private String bankIdCardNo;

    @ApiModelProperty(value = "银行卡手机号")
    private String bankMobile;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
