package com.roncoo.education.user.common.pay;


import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.bo.PayOrderBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.common.dto.PayOrderDTO;

/**
 * 统一支付
 *
 * @author Quanf
 */
public interface PayFacade {

    /**
     * 初始化支付,通过该方法获取支付链接.
     */
    PayDTO pay(PayBO payBO);

    /**
     * 异步回调
     */
    PayNotifyDTO verify(PayNotifyBO notifyBO);

    /**
     * 订单查询
     */
    PayOrderDTO order(PayOrderBO orderBO);

}
