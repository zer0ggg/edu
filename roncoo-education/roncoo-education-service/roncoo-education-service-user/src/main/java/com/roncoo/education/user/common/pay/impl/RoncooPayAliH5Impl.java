package com.roncoo.education.user.common.pay.impl;

import com.roncoo.education.common.core.base.Base;
import com.roncoo.education.common.core.enums.PayNetWorkStatusEnum;
import com.roncoo.education.common.core.enums.PayProductTypeEnum;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.bo.PayOrderBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.common.dto.PayOrderDTO;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.common.pay.util.RoncooPayUtil;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 龙果支付宝H5
 *
 * @author Quanf
 * 2020/3/31 17:59
 */
@Service("RONCOO_PAY_ALI_H5")
public class RoncooPayAliH5Impl extends Base implements PayFacade {

    @Autowired
    private PayChannelDao payChannelDao;

    @Override
    public PayDTO pay(PayBO payBO) {
        logger.info("龙果支付阿里H5:{}", payBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(payBO.getPayChannelCode());
        PayDTO payDTO = new PayDTO();
        payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.FAILED);
        String resp = RoncooPayUtil.pay(payChannel, payBO, PayProductTypeEnum.ZHIFUBAO_T1_H5.getCode());
        try {
            Map<String, String> res = JSUtil.parseObject(resp, HashMap.class);
            logger.debug("RoncooPay返回转换:{}", res);
            if ("0000".equals(res.get("resultCode"))) {
                payDTO.setPayMessage(res.get("payMessage"));
                payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.SUCCESS);
            }
        } catch (Exception e) {
            logger.error("RoncooPay请求失败:{}", resp);
        }
        return payDTO;
    }

    @Override
    public PayNotifyDTO verify(PayNotifyBO notifyBO) {
        logger.debug("龙果支付阿里H5通知:[{}]", notifyBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(notifyBO.getPayChannelCode());
        return RoncooPayUtil.verify(notifyBO, payChannel.getPaySecret());
    }

    @Override
    public PayOrderDTO order(PayOrderBO orderBO) {
        logger.debug("龙果支付阿里H5查询:[{}]", orderBO);
        PayOrderDTO payOrderDTO = new PayOrderDTO();
        PayChannel payChannel = payChannelDao.getByPayChannelCode(orderBO.getPayChannelCode());
        int orderStatus = RoncooPayUtil.orderQuery(orderBO.getOrderNo(), payChannel);
        payOrderDTO.setOrderStatus(orderStatus);
        return payOrderDTO;
    }
}
