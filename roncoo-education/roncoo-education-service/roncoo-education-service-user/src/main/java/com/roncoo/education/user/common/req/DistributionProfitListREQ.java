package com.roncoo.education.user.common.req;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代理分润记录
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "DistributionProfitListREQ", description = "代理分润记录列表")
public class DistributionProfitListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分销用户手机号")
    private String mobile;

    @ApiModelProperty(value = "分销父用户手机号")
    private String parentMobile;

    @ApiModelProperty(value = "分润金额（1：推广；2：邀请）")
    private Integer profitType;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
