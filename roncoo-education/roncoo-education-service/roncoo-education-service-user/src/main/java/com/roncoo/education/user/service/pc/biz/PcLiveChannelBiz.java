package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsUseEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.user.common.req.ChannelUpdateStatusIdREQ;
import com.roncoo.education.user.common.req.LiveChannelDeleteREQ;
import com.roncoo.education.user.common.req.LiveChannelListREQ;
import com.roncoo.education.user.common.resp.LiveChannelListRESP;
import com.roncoo.education.user.service.dao.LiveChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannelExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 直播频道表
 *
 * @author LHR
 */
@Component
public class PcLiveChannelBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private LiveChannelDao dao;

    public Result<Page<LiveChannelListRESP>> list(LiveChannelListREQ liveChannelListREQ) {
        LiveChannelExample example = new LiveChannelExample();
        LiveChannelExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(liveChannelListREQ.getChannelId())) {
            criteria.andChannelIdLike(PageUtil.like(liveChannelListREQ.getChannelId()));
        }
        if (!StringUtils.isEmpty(liveChannelListREQ.getIsUse())) {
            criteria.andIsUseEqualTo(liveChannelListREQ.getIsUse());
        }
        if (!StringUtils.isEmpty(liveChannelListREQ.getLiveStatus())) {
            criteria.andLiveStatusEqualTo(liveChannelListREQ.getLiveStatus());
        }
        if (!StringUtils.isEmpty(liveChannelListREQ.getScene())) {
            criteria.andSceneEqualTo(liveChannelListREQ.getScene());
        }
        if (!StringUtils.isEmpty(liveChannelListREQ.getStatusId())) {
            criteria.andStatusIdEqualTo(liveChannelListREQ.getStatusId());
        }else {
            criteria.andStatusIdNotEqualTo(Constants.FREEZE);
        }
        example.getOrderByClause(" status_id desc, id desc");
        Page<LiveChannel> page = dao.listForPage(liveChannelListREQ.getPageCurrent(), liveChannelListREQ.getPageSize(), example);
        return Result.success(PageUtil.transform(page, LiveChannelListRESP.class));
    }

    public Result<String> delete(LiveChannelDeleteREQ liveChannelDeleteREQ) {
        if (StringUtils.isEmpty(liveChannelDeleteREQ.getId())) {
            return Result.error("id不能为空!");
        }
        LiveChannel liveChannel = dao.getById(liveChannelDeleteREQ.getId());
        if (ObjectUtil.isEmpty(liveChannel)) {
            return Result.error("获取不到该频道信息!");
        }
        if (IsUseEnum.YES.getCode().equals(liveChannel.getIsUse())) {
            return Result.error("该频道已被使用，暂不能删除!");
        }
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || com.alibaba.druid.util.StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || com.alibaba.druid.util.StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
        }
        int result = dao.deleteById(liveChannel.getId());
        if (result > 0 ) {
            PLChannelUtil.deleteChannel(liveChannel.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvAppSecret());
            return Result.success("删除成功!");
        }
        return Result.error("删除失败!");
    }

    public Result<String> updateStatusId(ChannelUpdateStatusIdREQ channelUpdateStatusIdREQ) {
        int result = dao.updateById(BeanUtil.copyProperties(channelUpdateStatusIdREQ, LiveChannel.class));
        if (result > 0) {
            return Result.success("修改成功!");
        }
        return Result.error("修改失败!");
    }
}
