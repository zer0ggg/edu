package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单信息表
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class AuthOrderInfoListForLecturerDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单号")
	@JsonSerialize(using = ToStringSerializer.class)
    private Long orderNo;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;

    @ApiModelProperty(value = "支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @ApiModelProperty(value = "优惠价")
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal priceDiscount;

    @ApiModelProperty(value = "实付金额")
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal pricePaid;

    @ApiModelProperty(value = "讲师收入")
    @JsonSerialize(using = ToStringSerializer.class)
    private BigDecimal lecturerIncome;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "手机号（脱敏处理）")
    private String phone;

}
