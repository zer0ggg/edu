package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignChosen;
import com.roncoo.education.user.feign.qo.ChosenQO;
import com.roncoo.education.user.feign.vo.ChosenVO;
import com.roncoo.education.user.service.feign.biz.FeignChosenBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分销信息
 *
 * @author wujing
 * @date 2020-06-17
 */
@RestController
public class FeignChosenController extends BaseController implements IFeignChosen{

    @Autowired
    private FeignChosenBiz biz;

	@Override
	public Page<ChosenVO> listForPage(@RequestBody ChosenQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ChosenQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ChosenQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ChosenVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

    @Override
    public ChosenVO getByCourseId(@PathVariable(value = "courseId") Long courseId) {
        return biz.getByCourseId(courseId);
    }
}
