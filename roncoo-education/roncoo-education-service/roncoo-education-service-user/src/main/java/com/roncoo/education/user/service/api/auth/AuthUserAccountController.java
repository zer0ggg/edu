package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountBankBO;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountViewBO;
import com.roncoo.education.user.common.dto.auth.AuthUserAccountViewDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserAccountBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户账户信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/user/auth/user/account")
public class AuthUserAccountController {

	@Autowired
	private AuthUserAccountBiz biz;

	/**
	 * 账户信息详情接口
	 *
	 * @param authUserAccountViewBO
	 */
	@ApiOperation(value = "账户信息查看接口", notes = "账户信息详情接口")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthUserAccountViewDTO> view(@RequestBody AuthUserAccountViewBO authUserAccountViewBO) {
		return biz.view(authUserAccountViewBO);
	}

	/**
	 * 更新用户银行卡信息接口
	 *
	 * @param athUserAccountBankBO
	 */
	@ApiOperation(value = "更新用户银行卡信息", notes = "更新用户银行卡信息接口")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<Integer> updateBackInfo(@RequestBody AuthUserAccountBankBO athUserAccountBankBO) {
		return biz.updateBackInfo(athUserAccountBankBO);
	}

}
