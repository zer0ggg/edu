package com.roncoo.education.user.common.resp;

import com.roncoo.education.user.feign.vo.UserAccountVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 讲师信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="LecturerViewRESP", description="讲师信息查看")
public class LecturerViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "讲师手机")
    private String lecturerMobile;

    @ApiModelProperty(value = "讲师邮箱")
    private String lecturerEmail;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "头像")
    private String headImgUrl;

    @ApiModelProperty(value = "简介")
    private String introduce;

    @ApiModelProperty(value = "讲师分成比例")
    private BigDecimal lecturerProportion;

    @ApiModelProperty(value = "是否拥有直播功能（0：否，1: 是）")
    private Integer isLive;

    @ApiModelProperty(value = "频道id")
    private String channelId;

    @ApiModelProperty(value = "频道密码")
    private String channelPasswd;

    @ApiModelProperty(value = "直播推流地址")
    private String pushFlowUrl;

    @ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏)")
    private String scene;

    @ApiModelProperty(value = "直播状态(1:未开始;2:直播中;3:直播结束)")
    private Integer liveStatus;

    @ApiModelProperty(value = "账户信息")
    private UserAccountVO userAccountVO;

    @ApiModelProperty(value = "三分屏频道id")
    private String pptChannelId;

    @ApiModelProperty(value = "三分屏频道密码")
    private String pptChannelPasswd;
}
