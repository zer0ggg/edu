package com.roncoo.education.user.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 订单查询
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayOrderBO", description = "订单查询")
public class PayOrderBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "渠道编号")
    private String payChannelCode;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

}
