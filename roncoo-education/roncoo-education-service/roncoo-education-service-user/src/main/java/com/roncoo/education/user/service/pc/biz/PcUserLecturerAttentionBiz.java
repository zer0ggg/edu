package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.UserLecturerAttentionEditREQ;
import com.roncoo.education.user.common.req.UserLecturerAttentionListREQ;
import com.roncoo.education.user.common.req.UserLecturerAttentionSaveREQ;
import com.roncoo.education.user.common.resp.UserLecturerAttentionListRESP;
import com.roncoo.education.user.common.resp.UserLecturerAttentionViewRESP;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserLecturerAttentionDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户关注讲师
 *
 * @author wujing
 */
@Component
public class PcUserLecturerAttentionBiz extends BaseBiz {

    @Autowired
    private UserLecturerAttentionDao dao;

    @Autowired
    private UserExtDao userExtDao;

    @Autowired
    private LecturerDao lecturerDao;

    /**
     * 用户关注讲师列表
     *
     * @param userLecturerAttentionListREQ 用户关注讲师分页查询参数
     * @return 用户关注讲师分页查询结果
     */
    public Result<Page<UserLecturerAttentionListRESP>> list(UserLecturerAttentionListREQ userLecturerAttentionListREQ) {
        UserLecturerAttentionExample example = new UserLecturerAttentionExample();
        Criteria c = example.createCriteria();
        // 用户编号
        if (ObjectUtil.isNotNull(userLecturerAttentionListREQ.getUserNo())) {
            c.andUserNoEqualTo(userLecturerAttentionListREQ.getUserNo());
        }
        // 讲师编号
        if (ObjectUtil.isNotNull(userLecturerAttentionListREQ.getLecturerMobile())) {
            UserExt userExt = userExtDao.getByMobile(userLecturerAttentionListREQ.getLecturerMobile());
            if (ObjectUtil.isNotNull(userExt) && StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
                c.andLecturerUserNoEqualTo(userExt.getUserNo());
            }
        }
        example.setOrderByClause(" id desc ");
        Page<UserLecturerAttention> page = dao.listForPage(userLecturerAttentionListREQ.getPageCurrent(), userLecturerAttentionListREQ.getPageSize(), example);
        Page<UserLecturerAttentionListRESP> respPage = PageUtil.transform(page, UserLecturerAttentionListRESP.class);
        for (UserLecturerAttentionListRESP vo : respPage.getList()) {
            UserExt userExt = userExtDao.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExt) && StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
                vo.setUserMobile(userExt.getMobile());
            }
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(vo.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturer) && StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                vo.setLecturerMobile(lecturer.getLecturerMobile());
                vo.setLecturerName(lecturer.getLecturerName());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 用户关注讲师添加
     *
     * @param userLecturerAttentionSaveREQ 用户关注讲师
     * @return 添加结果
     */
    public Result<String> save(UserLecturerAttentionSaveREQ userLecturerAttentionSaveREQ) {
        UserLecturerAttention record = BeanUtil.copyProperties(userLecturerAttentionSaveREQ, UserLecturerAttention.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户关注讲师查看
     *
     * @param id 主键ID
     * @return 用户关注讲师
     */
    public Result<UserLecturerAttentionViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserLecturerAttentionViewRESP.class));
    }


    /**
     * 用户关注讲师修改
     *
     * @param userLecturerAttentionEditREQ 用户关注讲师修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserLecturerAttentionEditREQ userLecturerAttentionEditREQ) {
        UserLecturerAttention record = BeanUtil.copyProperties(userLecturerAttentionEditREQ, UserLecturerAttention.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户关注讲师删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
