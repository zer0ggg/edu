package com.roncoo.education.user.common.pay;

import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.dto.UnifiedOrderDTO;

/**
 * 统一支付
 *
 * @author Quanf
 */
public interface UnifiedOrder {
    /**
     * 统一支付接口
     */
    UnifiedOrderDTO unifiedorder(PayBO payBO);
}
