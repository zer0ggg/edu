package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMsgMapper {
    int countByExample(UserMsgExample example);

    int deleteByExample(UserMsgExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserMsg record);

    int insertSelective(UserMsg record);

    List<UserMsg> selectByExample(UserMsgExample example);

    UserMsg selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserMsg record, @Param("example") UserMsgExample example);

    int updateByExample(@Param("record") UserMsg record, @Param("example") UserMsgExample example);

    int updateByPrimaryKeySelective(UserMsg record);

    int updateByPrimaryKey(UserMsg record);

    /**
     * 根据用户编号，将所有消息置为已阅读
     * @param userNo
     * @return
     */
    int readAllByUserNo(Long userNo);
}
