package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PayRuleMapper {
    int countByExample(PayRuleExample example);

    int deleteByExample(PayRuleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PayRule record);

    int insertSelective(PayRule record);

    List<PayRule> selectByExample(PayRuleExample example);

    PayRule selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PayRule record, @Param("example") PayRuleExample example);

    int updateByExample(@Param("record") PayRule record, @Param("example") PayRuleExample example);

    int updateByPrimaryKeySelective(PayRule record);

    int updateByPrimaryKey(PayRule record);
}
