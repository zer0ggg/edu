package com.roncoo.education.user.common.pay.impl;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.PayNetWorkStatusEnum;
import com.roncoo.education.common.core.enums.PayReturnCodeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.dto.UnifiedOrderDTO;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.common.pay.UnifiedOrder;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.PayRuleDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 支付
 *
 * @author Quanf
 */
@Component
public class PayImpl extends BaseBiz implements UnifiedOrder {

    @Autowired
    private PayChannelDao payChannelDao;
    @Autowired
    private PayRuleDao payRuleDao;

    @Autowired
    private Map<String, PayFacade> payFacades;

    /**
     * 统一支付接口
     *
     * @param payBO 支付申请请求实体类
     * @return 支付信息
     */
    @Override
    public UnifiedOrderDTO unifiedorder(PayBO payBO) {
        logger.info("支付请求:{}", payBO);

        //验证请求参数
        verifyPayCnpParam(payBO);

        //请求第三方获取支付链接
        UnifiedOrderDTO dto = new UnifiedOrderDTO();
        dto.setResultCode(PayReturnCodeEnum.FAIL.getCode());
        //获取可用路由
        List<PayRule> resultList = payRuleDao.listByPayTypeAndStatusIdAndPriorityDesc(payBO.getPayType(), StatusIdEnum.YES.getCode());
        if (resultList == null || resultList.isEmpty()) {
            logger.error("没有符合要求的支付路由, 支付类型={}", payBO.getPayType());
            return dto;
        }
        logger.error("查询结果:返回符合要求的支付路由记录:[{}]", resultList.size());
        for (PayRule payRule : resultList) {
            // 判断渠道
            PayChannel payChannel = payChannelDao.getByPayChannelCode(payRule.getPayChannelCode());
            if (!StatusIdEnum.YES.getCode().equals(payChannel.getStatusId())) {
                logger.error("渠道:[{}]不可用", payChannel.getPayChannelName());
                continue;
            }
            dto = request(payBO, payChannel);
            dto.setPayChannelCode(payRule.getPayChannelCode());
            dto.setPayChannelName(payRule.getPayChannelName());
            if (PayReturnCodeEnum.SUCCESS.getCode().equals(dto.getResultCode())) {
                return dto;
            }
        }
        return dto;
    }


    /**
     * 请求支付通道获取支付链接
     *
     * @param payBO 请求参数
     * @return 支付链接
     */
    private UnifiedOrderDTO request(PayBO payBO, PayChannel payChannel) {
        UnifiedOrderDTO dto = new UnifiedOrderDTO();
        dto.setResultCode(PayReturnCodeEnum.FAIL.getCode());
        try {
            payBO.setPayChannelCode(payChannel.getPayChannelCode());
            long startTime = System.currentTimeMillis();
            // 请求支付通道获取支付链接
            PayFacade cnpPayFacade = payFacades.get(payChannel.getPayObjectCode());
            PayDTO payDTO = cnpPayFacade.pay(payBO);
            Long subTime = System.currentTimeMillis() - startTime;
            logger.info("请求花费时间[{}]毫秒", subTime);
            if (payDTO != null && PayNetWorkStatusEnum.SUCCESS.name().equals(payDTO.getPayNetWorkStatusEnum().name())) {
                logger.info("请求支付通道{}成功", payChannel.getPayChannelCode());
                dto.setResultCode(PayReturnCodeEnum.SUCCESS.getCode());
                dto.setPayMessage(payDTO.getPayMessage());
                return dto;
            }
        } catch (Exception e) {
            logger.error("请求支付通道异常", e);
        }
        logger.info("请求上游通道{}失败", payChannel.getPayChannelCode());
        return dto;
    }

    /**
     * 校验支付传入参数
     *
     * @param payBO 支付申请请求实体
     */
    private void verifyPayCnpParam(PayBO payBO) {
        if (StringUtils.isEmpty(payBO.getOrderAmount())) {
            throw new BaseException("订单金额[orderAmount]不能为空");
        }
        if (StringUtils.isEmpty(payBO.getOrderNo())) {
            throw new BaseException("订单号[orderNo]不能为空");
        }
        if (String.valueOf(payBO.getOrderNo()).length() > 20) {
            throw new BaseException("订单号[orderNo]长度最大20位");
        }
        if (StringUtils.isEmpty(payBO.getGoodsName())) {
            throw new BaseException("商品名称[goodsName]不能为空");
        }
        if (payBO.getGoodsName().length() > 200) {
            throw new BaseException("商品名称[goodsName]长度最大200位");
        }
        if (StringUtils.isEmpty(payBO.getPayType())) {
            throw new BaseException("支付类型编码[payType]不能为空");
        }
        if (StringUtils.isEmpty(payBO.getNotifyUrl())) {
            throw new BaseException("异步回调地址[notifyUrl]不能为空");
        }
    }
}
