package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.user.feign.qo.UserAccountQO;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 用户账户信息表
 *
 * @author wujing
 */
@Component
public class FeignUserAccountBiz extends BaseBiz {

	@Autowired
	private UserAccountDao dao;

	public Page<UserAccountVO> listForPage(UserAccountQO qo) {
		UserAccountExample example = new UserAccountExample();
		example.setOrderByClause(" id desc ");
		Page<UserAccount> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserAccountVO.class);
	}

	public int save(UserAccountQO qo) {
		UserAccount record = BeanUtil.copyProperties(qo, UserAccount.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserAccountVO getById(Long id) {
		UserAccount record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserAccountVO.class);
	}

	public int updateById(UserAccountQO qo) {
		UserAccount record = BeanUtil.copyProperties(qo, UserAccount.class);
		return dao.updateById(record);
	}

	public UserAccountVO getByUserNo(UserAccountQO qo) {
		UserAccount record = dao.getByUserNo(qo.getUserNo());
		return BeanUtil.copyProperties(record, UserAccountVO.class);
	}

	public void updateTotalIncomeByUserNo(UserAccountQO userAccountQO) {
		UserAccount userAccount = dao.getByUserNo(userAccountQO.getUserNo());
		if (ObjectUtil.isNull(userAccount)) {
			userAccount = new UserAccount();
			userAccount.setUserNo(userAccountQO.getUserNo());
			userAccount.setTotalIncome(BigDecimal.valueOf(0));
			userAccount.setHistoryMoney(BigDecimal.valueOf(0));
			userAccount.setEnableBalances(BigDecimal.valueOf(0));
			userAccount.setFreezeBalances(BigDecimal.valueOf(0));
			userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
			dao.save(userAccount);
		}
		String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
		if (userAccount.getSign().equals(sign)) {
			userAccount.setTotalIncome(userAccount.getTotalIncome().add(userAccountQO.getEnableBalances()));
			userAccount.setEnableBalances(userAccount.getEnableBalances().add(userAccountQO.getEnableBalances()));
			userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
			dao.updateByUserNo(userAccount);
		} else {
			logger.error("账户异常 , sign不正确, 用户编号={}", userAccount.getUserNo());
			throw new BaseException("账户异常");
		}
	}

}
