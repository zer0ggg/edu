package com.roncoo.education.user.common.pay;

public interface OrderFacade {

    /**
     * 订单信息完成处理
     *
     * @param serialNumber 流水号
     * @param orderStatus  订单状态
     * @return 处理结果
     */
    Boolean orderInfoComplete(Long serialNumber, Integer orderStatus);

}
