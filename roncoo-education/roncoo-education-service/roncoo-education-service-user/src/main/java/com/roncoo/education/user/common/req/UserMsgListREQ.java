package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 站内信用户记录
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserMsgListREQ", description="站内信用户记录列表")
public class UserMsgListREQ implements Serializable {

    private static final long serialVersionUID = 1L;



    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "短信标题")
    private String msgTitle;

    @ApiModelProperty(value = "是否阅读(1是;0否)")
    private Integer isRead;

    @ApiModelProperty(value = "是否已发送(1是;0否)")
    private Integer isSend;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
