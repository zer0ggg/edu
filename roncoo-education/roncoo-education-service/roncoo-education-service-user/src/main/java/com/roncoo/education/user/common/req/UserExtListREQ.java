package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户教育信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserExtListREQ", description = "用户教育信息列表")
public class UserExtListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "是否是会员（0：不是，1:是）")
    private Integer isVip;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "开始注册时间")
    private String beginRegisterTime;

    @ApiModelProperty(value = "结束注册时间")
    private String endRegisterTime;

    /**
     * 是否是推荐（1：是；0：否）
     */
    private Integer isRecommended;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
