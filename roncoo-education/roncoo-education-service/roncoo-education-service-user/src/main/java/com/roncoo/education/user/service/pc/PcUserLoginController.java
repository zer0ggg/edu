package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserLoginPasswordREQ;
import com.roncoo.education.user.common.resp.UserLoginRESP;
import com.roncoo.education.user.service.pc.biz.PcUserLoginBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 后台用户接口
 */
@RestController
@RequestMapping(value = "/user/pc/api/user/login")
@Api(value = "user-登录", tags = {"user-登录"})
public class PcUserLoginController {

	@Autowired
	private PcUserLoginBiz biz;

	/**
	 * 用户密码登录接口
	 */
	@ApiOperation(value = "用户密码登录接口", notes = "用户密码登录")
	@RequestMapping(value = "/password", method = RequestMethod.POST)
	public Result<UserLoginRESP> loginPassword(@RequestBody UserLoginPasswordREQ userLoginPasswordREQ) {
		return biz.loginPassword(userLoginPasswordREQ);
	}

}
