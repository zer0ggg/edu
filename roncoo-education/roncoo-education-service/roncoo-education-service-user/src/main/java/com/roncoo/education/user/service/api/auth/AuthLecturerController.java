package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.LecturerViewBO;
import com.roncoo.education.user.common.dto.auth.AuthLecturerViewDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthLecturerBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/user/auth/lecturer")
public class AuthLecturerController extends BaseController {

	@Autowired
	private AuthLecturerBiz biz;

	/**
	 * 讲师信息查看接口
	 *
	 * @param lecturerViewBO
	 * @author wuyun
	 */
	@ApiOperation(value = "讲师查看接口", notes = "根据讲师用户编号查看讲师信息")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthLecturerViewDTO> view(@RequestBody LecturerViewBO lecturerViewBO) {
		return biz.view(lecturerViewBO);
	}

}
