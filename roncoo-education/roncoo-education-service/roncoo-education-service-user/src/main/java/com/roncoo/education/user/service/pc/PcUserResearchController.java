package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserResearchEditREQ;
import com.roncoo.education.user.common.req.UserResearchListREQ;
import com.roncoo.education.user.common.req.UserResearchSaveREQ;
import com.roncoo.education.user.common.resp.UserResearchListRESP;
import com.roncoo.education.user.common.resp.UserResearchViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserResearchBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户调研信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户调研信息")
@RestController
@RequestMapping("/user/pc/user/research")
public class PcUserResearchController {

    @Autowired
    private PcUserResearchBiz biz;

    @ApiOperation(value = "用户调研信息列表", notes = "用户调研信息列表")
    @PostMapping(value = "/list")
    public Result<Page<UserResearchListRESP>> list(@RequestBody UserResearchListREQ userResearchListREQ) {
        return biz.list(userResearchListREQ);
    }

    @ApiOperation(value = "用户调研信息添加", notes = "用户调研信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserResearchSaveREQ userResearchSaveREQ) {
        return biz.save(userResearchSaveREQ);
    }

    @ApiOperation(value = "用户调研信息查看", notes = "用户调研信息查看")
    @GetMapping(value = "/view")
    public Result<UserResearchViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户调研信息修改", notes = "用户调研信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserResearchEditREQ userResearchEditREQ) {
        return biz.edit(userResearchEditREQ);
    }

    @ApiOperation(value = "用户调研信息删除", notes = "用户调研信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
