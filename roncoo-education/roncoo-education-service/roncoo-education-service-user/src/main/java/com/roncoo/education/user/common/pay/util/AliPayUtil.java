package com.roncoo.education.user.common.pay.util;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.roncoo.education.common.core.enums.AliPayTradeStatusEnum;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class AliPayUtil {

    private static final Logger logger = LoggerFactory.getLogger(AliPayUtil.class);

    /**
     * 支付宝扫码支付
     *
     * @param payChannel 支付配置
     * @param payBO      下单参数
     * @return 支付内容
     */
    public static AlipayTradePrecreateResponse scanPay(PayChannel payChannel, PayBO payBO) {
        DecimalFormat df = new DecimalFormat("#0.00");
        logger.info("支付宝扫码支付:商户支付订单号:[{}],支付金额:[{}元],订单标题:[{}]", payBO.getOrderNo(), df.format(payBO.getOrderAmount()), payBO.getGoodsName());
        try {
            String timeoutExpress = "30m";// 该笔订单允许的最晚付款时间，逾期将关闭交易
            AlipayClient alipayClient = new DefaultAlipayClient(payChannel.getRequestUrl(), payChannel.getPayKey(), payChannel.getPaySecret(), "json", "utf-8", payChannel.getField1(), "RSA2");
            AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
            SortedMap<String, Object> bizContentMap = new TreeMap<>();
            bizContentMap.put("out_trade_no", payBO.getOrderNo());
            bizContentMap.put("total_amount", df.format(payBO.getOrderAmount()));
            bizContentMap.put("subject", payBO.getGoodsName());
            bizContentMap.put("timeout_express", timeoutExpress);
            String bizContent = JSONUtil.toJsonPrettyStr(bizContentMap);
            request.setBizContent(bizContent);
            request.setNotifyUrl(String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
            logger.info("支付宝扫码请求参数:[{}]", request);
            AlipayTradePrecreateResponse response = alipayClient.execute(request);
            logger.info("支付宝扫码返回结果:[{}]", response);
            if (response.isSuccess()) {
                logger.info("支付宝扫码支付调用成功!返回二维码链接:[{}]", response.getQrCode());
            } else {
                logger.info("支付宝扫码支付调用失败!错误信息:[{} {}]", response.getMsg(), response.getSubCode());
            }
            return response;
        } catch (AlipayApiException e) {
            logger.error("支付宝扫码支付异常信息，参数:{}", e.getErrMsg(), e);
            return null;
        }
    }

    /**
     * APP支付
     * 商户APP 集成支付宝提供的 SDK，通过 SDK 跳转到支付宝中完成支付，
     * 支付完后跳回商户 APP内，展示支付结果。
     *
     * @param payChannel 支付配置
     * @param payBO      支付参数
     * @return 支付信息
     */
    public static AlipayTradeAppPayResponse appPay(PayChannel payChannel, PayBO payBO) {
        DecimalFormat df = new DecimalFormat("#0.00");
        logger.info("支付宝APP支付:商户支付订单号:[{}],支付金额:[{}元],订单标题:[{}]", payBO.getOrderNo(), df.format(payBO.getOrderAmount()), payBO.getGoodsName());
        try {
            String timeoutExpress = "30m";// 该笔订单允许的最晚付款时间，逾期将关闭交易
            AlipayClient alipayClient = new DefaultAlipayClient(payChannel.getRequestUrl(), payChannel.getPayKey(), payChannel.getPaySecret(), "json", "utf-8", payChannel.getField1(), "RSA2");
            AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
            SortedMap<String, Object> bizContentMap = new TreeMap<>();
            bizContentMap.put("out_trade_no", payBO.getOrderNo());
            bizContentMap.put("total_amount", df.format(payBO.getOrderAmount()));
            bizContentMap.put("subject", payBO.getGoodsName());
            bizContentMap.put("timeout_express", timeoutExpress);
            String bizContent = JSONUtil.toJsonPrettyStr(bizContentMap);
            request.setBizContent(bizContent);
            request.setNotifyUrl(String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
            logger.info("支付宝APP请求参数:[{}]", request);
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            logger.info("支付宝APP返回结果:[{}]", response);
            if (response.isSuccess()) {
                logger.info("支付宝APP支付调用成功!返回信息:[{}]", response.getBody());
            } else {
                logger.info("支付宝APP支付调用失败!错误信息:[{} {}]", response.getMsg(), response.getSubCode());
            }
            return response;
        } catch (AlipayApiException e) {
            logger.error("支付宝APP支付异常信息，参数:{}", e.getErrMsg(), e);
            return null;
        }
    }

    /**
     * H5支付
     * 本支付产品适用于商户网页应用中，买家在商家手机网站进行支付，
     * 通过浏览器唤起支付宝客户端进行付款，实现和App支付相同的支付体验；
     * 在没有安装支付宝客户端的情况下，可以继续使用网页完成支付。
     *
     * @param payChannel 支付配置
     * @param payBO      下单参数
     * @return 支付信息
     */
    public static AlipayTradeWapPayResponse H5Pay(PayChannel payChannel, PayBO payBO) {
        DecimalFormat df = new DecimalFormat("#0.00");
        logger.info("支付宝H5支付:商户支付订单号:[{}],支付金额:[{}元],订单标题:[{}]", payBO.getOrderNo(), df.format(payBO.getOrderAmount()), payBO.getGoodsName());
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(payChannel.getRequestUrl(), payChannel.getPayKey(), payChannel.getPaySecret(), "json", "utf-8", payChannel.getField1(), "RSA2");
            AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("out_trade_no", payBO.getOrderNo());
            jsonObject.put("total_amount", df.format(payBO.getOrderAmount()));
            jsonObject.put("subject", payBO.getGoodsName());
            jsonObject.put("product_code", "QUICK_WAP_PAY");
            request.setReturnUrl(payBO.getReturnUrl());
            request.setNotifyUrl(String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
            request.setBizContent(jsonObject.toJSONString());
            logger.info("支付宝H5请求参数:[{}]", request);
            AlipayTradeWapPayResponse response = alipayClient.pageExecute(request);
            logger.info("支付宝H5返回结果:[{}]", response);
            if (response.isSuccess()) {
                logger.info("支付宝H5支付调用成功!返回信息:[{}]", response.getBody());
            } else {
                logger.info("支付宝H5支付调用失败!错误信息:[{} {}]", response.getMsg(), response.getSubCode());
            }
            return response;
        } catch (AlipayApiException e) {
            logger.error("支付宝APP支付异常信息，参数:{}", e.getErrMsg(), e);
            return null;
        }
    }

    /**
     * 支付订单查询
     *
     * @param outTradeNo 商户订单号
     * @return 订单状态
     */
    public static int orderQuery(String outTradeNo, PayChannel payChannel) {
        logger.info("支付宝订单查询,订单号:[{}]", outTradeNo);
        int orderStatus = OrderStatusEnum.WAIT.getCode();
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(payChannel.getQueryUrl(), payChannel.getPayKey(), payChannel.getPaySecret(), "json", "utf-8", payChannel.getField1(), "RSA2");
            AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();

            SortedMap<String, Object> bizContentMap = new TreeMap<>();
            bizContentMap.put("out_trade_no", outTradeNo);
            String bizContent = JSONUtil.toJsonPrettyStr(bizContentMap);
            request.setBizContent(bizContent);
            logger.info("支付宝查单请求参数:[{}]", request);
            AlipayTradeQueryResponse response = alipayClient.execute(request);
            logger.info("支付宝查单返回结果:[{}]", response);
            if (response.isSuccess()) {
                logger.info("支付宝订单查询调用成功!");
                String tradeStatus = response.getTradeStatus();
                logger.info("订单:[{}]返回状态码为:[{}]", outTradeNo, tradeStatus);
                if (AliPayTradeStatusEnum.WAIT_BUYER_PAY.name().equals(tradeStatus)) {
                    logger.info("订单:[{}]等待支付", outTradeNo);
                } else if (AliPayTradeStatusEnum.TRADE_CLOSED.name().equals(tradeStatus)) {
                    logger.info("订单:[{}]未付款交易超时关闭，或支付完成后全额退款", outTradeNo);
                } else if (AliPayTradeStatusEnum.TRADE_SUCCESS.name().equals(tradeStatus)) {
                    logger.info("订单:[{}]交易支付成功", outTradeNo);
                    orderStatus = OrderStatusEnum.SUCCESS.getCode();
                } else if (AliPayTradeStatusEnum.TRADE_FINISHED.name().equals(tradeStatus)) {
                    logger.info("订单:[{}]交易结束，不可退款", outTradeNo);
                } else {
                    logger.info("不属于正确状态码，订单结果未知!");
                }
            } else {
                logger.info("支付宝支付调用失败!");
            }
        } catch (AlipayApiException e) {
            logger.error("支付宝订单查询异常信息", e);
        }
        return orderStatus;
    }

    /**
     * 支付宝异步通知验签
     *
     * @param notifyBO  通知参数
     * @param publickey 公钥
     * @return 验签结果
     */
    public static PayNotifyDTO notifyCheck(PayNotifyBO notifyBO, String publickey) {
        PayNotifyDTO notifyDTO = new PayNotifyDTO();
        notifyDTO.setVerify(false);
        Map<String, Object> paramMapObj = JSONObject.parseObject(notifyBO.getBody());
        logger.debug("返回属性:{}", paramMapObj);
        Map tempMap = paramMapObj;
        Map<String, String> paramMap = tempMap;
        logger.debug("校验属性:{}", paramMap);
        boolean resultStatus = false;
        try {
            resultStatus = AlipaySignature.rsaCheckV1(paramMap, publickey, "utf-8", "RSA2");
        } catch (AlipayApiException e) {
            logger.error(paramMap.toString(), e);
        }
        notifyDTO.setOrderNo(paramMap.get("out_trade_no"));
        notifyDTO.setVerify(false);
        notifyDTO.setOrderStatus(OrderStatusEnum.WAIT.getCode());
        if (resultStatus) {
            notifyDTO.setVerify(true);
            logger.info("支付宝支付回调验签成功！");
            if (AliPayTradeStatusEnum.TRADE_SUCCESS.getCode().equals(paramMap.get("trade_status")) || AliPayTradeStatusEnum.TRADE_FINISHED.getCode().equals(paramMap.get("trade_status"))) {
                // 交易成功
                notifyDTO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
                notifyDTO.setResponseStr("success");
            }
            if (AliPayTradeStatusEnum.TRADE_CLOSED.getCode().equals(paramMap.get("trade_status"))) {
                // 交易失败
                notifyDTO.setOrderStatus(OrderStatusEnum.FAIL.getCode());
            }
        } else {
            logger.error("支付宝支付回调验签不通过，原字符串为：{}，公钥为：{}", paramMap.toString(), publickey);
        }
        return notifyDTO;
    }

}
