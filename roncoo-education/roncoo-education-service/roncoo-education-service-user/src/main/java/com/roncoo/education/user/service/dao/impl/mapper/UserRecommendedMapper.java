package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserRecommendedMapper {
    int countByExample(UserRecommendedExample example);

    int deleteByExample(UserRecommendedExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserRecommended record);

    int insertSelective(UserRecommended record);

    List<UserRecommended> selectByExample(UserRecommendedExample example);

    UserRecommended selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserRecommended record, @Param("example") UserRecommendedExample example);

    int updateByExample(@Param("record") UserRecommended record, @Param("example") UserRecommendedExample example);

    int updateByPrimaryKeySelective(UserRecommended record);

    int updateByPrimaryKey(UserRecommended record);
}
