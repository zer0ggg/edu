package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserMsgMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserMsgDaoImpl implements UserMsgDao {
	@Autowired
	private UserMsgMapper userMsgMapper;

	@Override
	public int save(UserMsg record) {
		record.setId(IdWorker.getId());
		return this.userMsgMapper.insertSelective(record);
	}

	@Override
	public int deleteById(Long id) {
		return this.userMsgMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateById(UserMsg record) {
		return this.userMsgMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateBySelective(UserMsg record,UserMsgExample example) {
		return this.userMsgMapper.updateByExampleSelective(record,example);
	}

	@Override
	public UserMsg getById(Long id) {
		return this.userMsgMapper.selectByPrimaryKey(id);
	}

	@Override
	public Page<UserMsg> listForPage(int pageCurrent, int pageSize, UserMsgExample example) {
		int count = this.userMsgMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<UserMsg>(count, totalPage, pageCurrent, pageSize, this.userMsgMapper.selectByExample(example));
	}

	@Override
	public int deleteByMsgId(Long id) {
		UserMsgExample example = new UserMsgExample();
		Criteria c = example.createCriteria();
		c.andMsgIdEqualTo(id);
		return this.userMsgMapper.deleteByExample(example);
	}

	@Override
	public int readAllByUserNo(Long userNo) {
		return this.userMsgMapper.readAllByUserNo(userNo);
	}

	@Override
	public int countByUserNoAndIsReadAndIsSend(Long userNo, Integer isRead, Integer isSend) {
		UserMsgExample example = new UserMsgExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		c.andIsReadEqualTo(isRead);
		c.andIsSendEqualTo(isSend);
		return this.userMsgMapper.countByExample(example);
	}

	@Override
	public List<UserMsg> listByMsgId(Long msgId) {
		UserMsgExample example = new UserMsgExample();
		Criteria c = example.createCriteria();
		c.andMsgIdEqualTo(msgId);
		return this.userMsgMapper.selectByExample(example);
	}
}
