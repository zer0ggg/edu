package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户教育信息
 */
@Data
@Accessors(chain = true)
public class AuthUserAccountBankBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号", required = true)
    private String bankCardNo;
    /**
     * 银行名称
     */
    @ApiModelProperty(value = "银行名称", required = true)
    private String bankName;
    /**
     * 开户支行
     */
    @ApiModelProperty(value = "开户支行", required = true)
    private String bankBranchName;
    /**
     * 开户人名称
     */
    @ApiModelProperty(value = "开户人名称", required = true)
    private String bankUserName;
    /**
     * 银行身份证号
     */
    @ApiModelProperty(value = "银行身份证号", required = true)
    private String bankIdCardNo;
    /**
     * 银行卡手机号
     */
    @ApiModelProperty(value = "银行卡手机号", required = true)
    private String bankMobile;
    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码", required = true)
    private String smsCode;

}
