package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignLiveChannel;
import com.roncoo.education.user.feign.qo.LiveChannelQO;
import com.roncoo.education.user.feign.vo.LiveChannelVO;
import com.roncoo.education.user.service.feign.biz.FeignLiveChannelBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * <p>
 * 直播频道表
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@RestController
public class FeignLiveChannelController extends BaseController implements IFeignLiveChannel{

    @Autowired
    private FeignLiveChannelBiz biz;


	@Override
	public Page<LiveChannelVO> listForPage(@RequestBody LiveChannelQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody LiveChannelQO qo) {
	return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody LiveChannelQO qo) {
	return biz.updateById(qo);
	}

	@Override
	public LiveChannelVO getById(@PathVariable(value = "id") Long id) {
	return biz.getById(id);
	}

	@Override
	public int updateChannelIsUseAndPasswd(@RequestBody LiveChannelQO qo) {
		return biz.updateChannelIsUseAndPasswd(qo);
	}

	@Override
	public List<LiveChannelVO> listBySceneAndIsUse(@RequestBody LiveChannelQO qo) {
		return biz.listBySceneAndIsUse(qo);
	}

	@Override
	public LiveChannelVO getByChannelId(@PathVariable(value = "channelId") String channelId) {
		return biz.getByChannelId(channelId);
	}
}
