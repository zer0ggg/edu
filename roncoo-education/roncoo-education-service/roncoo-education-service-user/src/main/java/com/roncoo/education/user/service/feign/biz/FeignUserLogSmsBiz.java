package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.aliyuncs.exceptions.ClientException;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.IsSuccessEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliyunVO;
import com.roncoo.education.user.feign.qo.UserLogSmsQO;
import com.roncoo.education.user.feign.vo.UserLogSmsVO;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserLogSmsDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSms;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 用户发送短信日志
 *
 * @author YZJ
 */
@Component
public class FeignUserLogSmsBiz {

    private static final String REGEX_MOBILE = "^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199)\\d{8}$";// 手机号码校验

    @Autowired
    private UserLogSmsDao dao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Autowired
    private IFeignSysConfig feignSysConfig;

    public Page<UserLogSmsVO> listForPage(UserLogSmsQO qo) {
        UserLogSmsExample example = new UserLogSmsExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(qo.getMobile())) {
            c.andMobileEqualTo(qo.getMobile());
        }
        if (StringUtils.hasText(qo.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(qo.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" id desc ");
        Page<UserLogSms> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, UserLogSmsVO.class);
    }

    public int save(UserLogSmsQO qo) {
        UserLogSms record = BeanUtil.copyProperties(qo, UserLogSms.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public UserLogSmsVO getById(Long id) {
        UserLogSms record = dao.getById(id);
        return BeanUtil.copyProperties(record, UserLogSmsVO.class);
    }

    public int updateById(UserLogSmsQO qo) {
        UserLogSms record = BeanUtil.copyProperties(qo, UserLogSms.class);
        return dao.updateById(record);
    }

    public int send(UserLogSmsQO qo) {
        if (StringUtils.isEmpty(qo.getMobile())) {
            throw new BaseException("手机号不能为空");
        }
        // 手机号码校验
        if (!Pattern.compile(REGEX_MOBILE).matcher(qo.getMobile()).matches()) {
            throw new BaseException("手机号码格式不正确");
        }
        User user = userDao.getByMobile(qo.getMobile());
        if (ObjectUtil.isNull(user)) {
            throw new BaseException("用户不存在");
        }

        if (myRedisTemplate.hasKey(Constants.SMS_CODE + qo.getMobile())) {
            throw new BaseException("操作过于频繁，请稍后重试（不少于5分钟）");
        }
        ConfigAliyunVO sys = feignSysConfig.getAliyun();
        if (ObjectUtil.isNull(sys)) {
            throw new BaseException("未配置系统配置表");
        }
        if (StringUtils.isEmpty(sys.getAliyunAccessKeyId()) || StringUtils.isEmpty(sys.getAliyunAccessKeySecret())) {
            throw new BaseException("aliyunAccessKeyId或aliyunAccessKeySecret未配置");
        }
        if (StringUtils.isEmpty(sys.getAliyunSmsCode()) || StringUtils.isEmpty(sys.getAliyunSmsSignName())) {
            throw new BaseException("smsCode或signName未配置");
        }
        // 创建日志实例
        UserLogSms userLogSms = new UserLogSms();
        userLogSms.setMobile(qo.getMobile());
        userLogSms.setTemplate(sys.getAliyunSmsCode());
        // 随机生成验证码
        userLogSms.setSmsCode(RandomUtil.randomNumbers(6));
        try {
            // 发送验证码
            boolean result = AliyunUtil.sendMsg(qo.getMobile(), userLogSms.getSmsCode(), BeanUtil.copyProperties(sys, Aliyun.class));
            if (result) {
                // 成功发送，验证码存入缓存：5分钟有效
                myRedisTemplate.set(Constants.SMS_CODE + qo.getMobile(), userLogSms.getSmsCode(), 5, TimeUnit.MINUTES);
                userLogSms.setIsSuccess(IsSuccessEnum.SUCCESS.getCode());
                return dao.save(userLogSms);
            }
            // 发送失败
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            dao.save(userLogSms);
            throw new BaseException("发送失败");
        } catch (ClientException e) {
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            dao.save(userLogSms);
            throw new BaseException("发送失败，原因={" + e.getErrMsg() + "}");
        }
    }

    public UserLogSmsVO statistical() {

        UserLogSmsVO vo = new UserLogSmsVO();

        // 查找总发送数
        List<UserLogSms> total = dao.listByAll();
        if (ObjectUtil.isNotNull(total)) {
            vo.setTotal(total.size());
        } else {
            vo.setTotal(0);
        }
        // 查找成功数
        List<UserLogSms> succeedCount = dao.listByIsSuccess(IsSuccessEnum.SUCCESS.getCode());
        if (ObjectUtil.isNotNull(succeedCount)) {
            vo.setSucceedCount(succeedCount.size());
        } else {
            vo.setSucceedCount(0);
        }
        // 查找失败数
        List<UserLogSms> failCount = dao.listByIsSuccess(IsSuccessEnum.FAIL.getCode());
        if (ObjectUtil.isNotNull(failCount)) {
            vo.setFailCount(failCount.size());
        } else {
            vo.setFailCount(0);
        }
        return vo;
    }

}
