package com.roncoo.education.user.common;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.JSUtil;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/** @author wujing */
@RestControllerAdvice(
    basePackages = {
      "com.roncoo.education.user.service.api",
      "com.roncoo.education.user.service.pc"
    })
public class HandlerException extends BaseController {

  @ExceptionHandler({FeignException.class})
  @ResponseStatus(HttpStatus.OK)
  public Result<String> processBizException(FeignException e) {
    String msg = e.getMessage();
    if (StringUtils.isEmpty(msg)) {
      return Result.error("服务繁忙，请重试");
    }
    logger.error(msg);
    Map<String, Object> m =
        JSUtil.parseArray(msg.substring(msg.lastIndexOf("[{")), Map.class).get(0);
    if (null != m.get("message")) {
      return Result.error(m.get("message").toString());
    }
    return Result.error("服务繁忙，请重试");
  }

  /**
   * 校验参数错误
   *
   * @param ex 参数校验错误
   * @return 错误返回
   */
  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(HttpStatus.OK)
  public Result<String> validException(MethodArgumentNotValidException ex) {
    StringBuilder sb = new StringBuilder();
    for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
      sb.append(fieldError.getDefaultMessage()).append(",");
      if (StringUtils.hasText(sb)) {
        break;
      }
    }
    sb.substring(0, sb.length() - 1);
    logger.error("校验参数异常:{}", ex.getMessage());
    return Result.error(sb.toString());
  }

  @ExceptionHandler({BaseException.class})
  @ResponseStatus(HttpStatus.OK)
  public Result<String> processException(BaseException e) {
    logger.error("BaseException", e);
    return Result.error(e.getMessage());
  }

  @ExceptionHandler({Exception.class})
  @ResponseStatus(HttpStatus.OK)
  public Result<String> processException(Exception e) {
    logger.error("Exception", e);
    return Result.error("系统错误");
  }
}
