package com.roncoo.education.user.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.UserLogModifiedEditREQ;
import com.roncoo.education.user.common.req.UserLogModifiedListREQ;
import com.roncoo.education.user.common.req.UserLogModifiedSaveREQ;
import com.roncoo.education.user.common.resp.UserLogModifiedListRESP;
import com.roncoo.education.user.common.resp.UserLogModifiedViewRESP;
import com.roncoo.education.user.service.dao.UserLogModifiedDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogModified;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogModifiedExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogModifiedExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户修改日志
 *
 * @author wujing
 */
@Component
public class PcUserLogModifiedBiz extends BaseBiz {

    @Autowired
    private UserLogModifiedDao dao;

    /**
    * 用户修改日志列表
    *
    * @param userLogModifiedListREQ 用户修改日志分页查询参数
    * @return 用户修改日志分页查询结果
    */
    public Result<Page<UserLogModifiedListRESP>> list(UserLogModifiedListREQ userLogModifiedListREQ) {
        UserLogModifiedExample example = new UserLogModifiedExample();
        Criteria c = example.createCriteria();
        Page<UserLogModified> page = dao.listForPage(userLogModifiedListREQ.getPageCurrent(), userLogModifiedListREQ.getPageSize(), example);
        Page<UserLogModifiedListRESP> respPage = PageUtil.transform(page, UserLogModifiedListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 用户修改日志添加
    *
    * @param userLogModifiedSaveREQ 用户修改日志
    * @return 添加结果
    */
    public Result<String> save(UserLogModifiedSaveREQ userLogModifiedSaveREQ) {
        UserLogModified record = BeanUtil.copyProperties(userLogModifiedSaveREQ, UserLogModified.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 用户修改日志查看
    *
    * @param id 主键ID
    * @return 用户修改日志
    */
    public Result<UserLogModifiedViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserLogModifiedViewRESP.class));
    }


    /**
    * 用户修改日志修改
    *
    * @param userLogModifiedEditREQ 用户修改日志修改对象
    * @return 修改结果
    */
    public Result<String> edit(UserLogModifiedEditREQ userLogModifiedEditREQ) {
        UserLogModified record = BeanUtil.copyProperties(userLogModifiedEditREQ, UserLogModified.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 用户修改日志删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
