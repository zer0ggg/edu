package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.system.feign.vo.MsgPushVO;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.feign.vo.UserMsgVO;
import com.roncoo.education.user.service.api.biz.ApiUserExtBiz;
import com.roncoo.education.user.service.dao.MsgDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Msg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@Component
public class FeignUserMsgBiz extends BaseBiz {

    @Autowired
    private UserMsgDao dao;

    @Autowired
    private MsgDao msgDao;

    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private ApiUserExtBiz userExtBiz;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<UserMsgVO> listForPage(UserMsgQO qo) {
        UserMsgExample example = new UserMsgExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotEmpty(qo.getMsgTitle())) {
            c.andMsgTitleLike(PageUtil.rightLike(qo.getMsgTitle()));
        }
        if (StringUtils.isNotEmpty(qo.getMobile())) {
            c.andMobileLike(PageUtil.rightLike(qo.getMobile()));
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<UserMsg> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, UserMsgVO.class);
    }

    public int save(UserMsgQO qo) {
        UserMsg record = BeanUtil.copyProperties(qo, UserMsg.class);
        return dao.save(record);
    }

    @Transactional(rollbackFor = Exception.class)
    public int deleteById(Long id) {
        dao.deleteById(id);
        int result = msgDao.deleteById(id);
        if (result < 1) {
            throw new BaseException("更新表失败");
        }
        return result;
    }

    public UserMsgVO getById(Long id) {
        UserMsg record = dao.getById(id);
        return BeanUtil.copyProperties(record, UserMsgVO.class);
    }

    public int updateById(UserMsgQO qo) {
        UserMsg record = BeanUtil.copyProperties(qo, UserMsg.class);
        return dao.updateById(record);
    }

    /**
     * 定时器任务：推送站内信到用户
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int push() {
        List<Msg> list = msgDao.listByStatusIdAndIsSendAndIsTimeSendAndSendTime(StatusIdEnum.YES.getCode(), IsSendEnum.NO.getCode(), IsTimeSendEnum.YES.getCode(), new Date());
        List<MsgPushVO> msgList = ArrayListUtil.copy(list, MsgPushVO.class);
        if (CollectionUtil.isNotEmpty(list)) {
            for (Msg msg : list) {
                // 进行推送前，将当前站内信推送状态置为已通知
                msg.setIsSend(HasNoticeEnum.YES.getCode());
                msgDao.updateById(msg);
                CALLBACK_EXECUTOR.execute(new Runnable() {
                    @Override
                    public void run() {
                        pushToUserByMsgPush(msg);
                    }
                });
            }
            return 1;
        }
        return 0;
    }

    @Transactional(rollbackFor = Exception.class)
    public int pushByManual(Long id) {
        // 获得模板
        Msg msg = msgDao.getById(id);
        if (msg == null) {
            throw new BaseException("查找msg失败");
        }
        final MsgPushVO msgPush = BeanUtil.copyProperties(msg, MsgPushVO.class);
        // 刷新站内信
        msg.setIsSend(HasNoticeEnum.YES.getCode());
        msgDao.updateById(msg);
        CALLBACK_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                pushToUserByMsgPush(msg);
            }
        });
        return 1;
    }

    private void pushToUserByMsgPush(Msg msg) {
        // 获取缓存的条数
        int num = getCacheNum();
        for (int i = 1; i < num + 1; i++) {
            List<UserExtMsgVO> list = list(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, UserExtMsgVO.class);
            if (CollectionUtil.isNotEmpty(list)) {
                // 批量生成
                for (UserExtMsgVO vo : list) {
                    saveUserMsg(msg, vo);
                }
            }
        }
    }

    private int getCacheNum() {
        boolean flag = hasKey(RedisPreEnum.SYS_MSG_SEND_NUM.getCode());
        if (!flag) {// 找不到，去缓存用户信息
            userExtBiz.cacheUserForMsg();
        }
        int num = get(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), int.class);
        return num;
    }

    private void saveUserMsg(Msg msg, UserExtMsgVO vo) {
        UserMsg record = new UserMsg();
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setMsgId(msg.getId());
        record.setMsgTitle(msg.getMsgTitle());
        record.setUserNo(vo.getUserNo());
        record.setMobile(vo.getMobile());
        dao.save(record);
    }

    private <T> List<T> list(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseArray(myRedisTemplate.get(key).toString(), clazz);
        }
        return null;
    }

    private boolean hasKey(Object key) {
        if (myRedisTemplate.hasKey(key)) {
            return true;
        }
        return false;
    }

    private <T> T get(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseObject(myRedisTemplate.get(key).toString(), clazz);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public void batchSaveUserMsgAndMsg(List<UserMsgAndMsgSaveQO> qo) {
        for (UserMsgAndMsgSaveQO userMsgAndMsgSaveQO : qo) {
            Msg msg = BeanUtil.copyProperties(userMsgAndMsgSaveQO.getMsg(), Msg.class);
            msgDao.save(msg);
            UserMsg userMsg = BeanUtil.copyProperties(userMsgAndMsgSaveQO.getUserMsg(), UserMsg.class);
            dao.save(userMsg);
        }
    }
}
