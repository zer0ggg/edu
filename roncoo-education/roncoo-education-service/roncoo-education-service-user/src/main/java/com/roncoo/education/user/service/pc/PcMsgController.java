package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.MsgEditREQ;
import com.roncoo.education.user.common.req.MsgListREQ;
import com.roncoo.education.user.common.req.MsgSaveREQ;
import com.roncoo.education.user.common.resp.MsgListRESP;
import com.roncoo.education.user.common.resp.MsgViewRESP;
import com.roncoo.education.user.service.pc.biz.PcMsgBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 站内信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/msg")
@Api(value = "user-站内信息", tags = {"user-站内信息"})
public class PcMsgController {

    @Autowired
    private PcMsgBiz biz;

    @ApiOperation(value = "站内信息列表", notes = "站内信息列表")
    @PostMapping(value = "/list")
    public Result<Page<MsgListRESP>> list(@RequestBody MsgListREQ msgListREQ) {
        return biz.list(msgListREQ);
    }

    @ApiOperation(value = "站内信息添加", notes = "站内信息添加")
    @PostMapping(value = "/save")
    @SysLog(value = "站内信息添加")
    public Result<String> save(@RequestBody MsgSaveREQ msgSaveREQ) {
        return biz.save(msgSaveREQ);
    }

    @ApiOperation(value = "站内信息查看", notes = "站内信息查看")
    @GetMapping(value = "/view")
    public Result<MsgViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "站内信息修改", notes = "站内信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "站内信息修改")
    public Result<String> edit(@RequestBody MsgEditREQ msgEditREQ) {
        return biz.edit(msgEditREQ);
    }

    @ApiOperation(value = "站内信息删除", notes = "站内信息删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "站内信息删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "推送站内信息", notes = "推送站内信息")
    @PostMapping(value = "/push")
    @SysLog(value = "推送站内信息")
    public Result<String> pushByManual(@RequestParam Long id) {
        return biz.pushByManual(id);
    }
}
