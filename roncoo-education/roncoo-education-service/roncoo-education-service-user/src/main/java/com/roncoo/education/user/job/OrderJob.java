package com.roncoo.education.user.job;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 定时任务-订单处理
 *
 * @author wuyun
 */
@Slf4j
@Component
public class OrderJob extends IJobHandler {

  @Autowired private OrderInfoDao dao;
  @Autowired private IFeignOrderLog feignOrderLog;

  protected static final ExecutorService CALLBACK_EXECUTOR = Executors.newFixedThreadPool(50);

  @Override
  @XxlJob(value = "orderJob")
  public ReturnT<String> execute(String param) {
    log.info("开始执行[订单处理],{}", param);
    XxlJobLogger.log("开始执行[订单处理]");
    try {
      handleScheduledTasks();
    } catch (Exception e) {
      log.error("定时任务-订单处理-执行出错", e);
      return new ReturnT<>(IJobHandler.FAIL.getCode(), "执行[订单处理]--处理异常");
    }
    XxlJobLogger.log("结束执行[订单处理]");
    return SUCCESS;
  }

  /**
   * 1小时后如果订单不支付，就关闭订单和标记订单支付日志，每次处理10条数据
   *
   * @author wuyun
   */
  private void handleScheduledTasks() {
    // 1.订单信息的处理
    OrderInfoExample example = new OrderInfoExample();
    OrderInfoExample.Criteria c = example.createCriteria();
    c.andOrderStatusEqualTo(OrderStatusEnum.WAIT.getCode());
    c.andGmtCreateLessThan(new Date(System.currentTimeMillis() - 3600000L));
    example.setOrderByClause(" id desc ");
    Page<OrderInfo> page = dao.listForPage(1, 10, example);
    if (CollectionUtil.isNotEmpty(page.getList())) {
      for (OrderInfo orderInfo : page.getList()) {
        // 更新订单信息
        OrderInfo argOrderInfo = new OrderInfo();
        argOrderInfo.setId(orderInfo.getId());
        argOrderInfo.setOrderStatus(OrderStatusEnum.CLOSE.getCode());
        argOrderInfo.setRemark("系统自动关闭该订单");
        dao.updateById(argOrderInfo);
        // 异步更新支付日志
        CALLBACK_EXECUTOR.execute(new orderLog(argOrderInfo));
      }
    }
  }

  /** 异步保存订单日志 */
  class orderLog implements Runnable {
    private final OrderInfo orderInfo;

    public orderLog(OrderInfo orderInfo) {
      this.orderInfo = orderInfo;
    }

    @SneakyThrows
    @Override
    public void run() {
      feignOrderLog.updateById(BeanUtil.copyProperties(orderInfo, OrderLogQO.class));
    }
  }
}
