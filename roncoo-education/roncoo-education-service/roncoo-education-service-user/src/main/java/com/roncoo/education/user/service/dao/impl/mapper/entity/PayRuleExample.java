package com.roncoo.education.user.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayRuleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public PayRuleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIsNull() {
            addCriterion("pay_channel_code is null");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIsNotNull() {
            addCriterion("pay_channel_code is not null");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeEqualTo(String value) {
            addCriterion("pay_channel_code =", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotEqualTo(String value) {
            addCriterion("pay_channel_code <>", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeGreaterThan(String value) {
            addCriterion("pay_channel_code >", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("pay_channel_code >=", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLessThan(String value) {
            addCriterion("pay_channel_code <", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLessThanOrEqualTo(String value) {
            addCriterion("pay_channel_code <=", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLike(String value) {
            addCriterion("pay_channel_code like", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotLike(String value) {
            addCriterion("pay_channel_code not like", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIn(List<String> values) {
            addCriterion("pay_channel_code in", values, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotIn(List<String> values) {
            addCriterion("pay_channel_code not in", values, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeBetween(String value1, String value2) {
            addCriterion("pay_channel_code between", value1, value2, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotBetween(String value1, String value2) {
            addCriterion("pay_channel_code not between", value1, value2, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIsNull() {
            addCriterion("pay_channel_name is null");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIsNotNull() {
            addCriterion("pay_channel_name is not null");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameEqualTo(String value) {
            addCriterion("pay_channel_name =", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotEqualTo(String value) {
            addCriterion("pay_channel_name <>", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameGreaterThan(String value) {
            addCriterion("pay_channel_name >", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameGreaterThanOrEqualTo(String value) {
            addCriterion("pay_channel_name >=", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLessThan(String value) {
            addCriterion("pay_channel_name <", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLessThanOrEqualTo(String value) {
            addCriterion("pay_channel_name <=", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLike(String value) {
            addCriterion("pay_channel_name like", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotLike(String value) {
            addCriterion("pay_channel_name not like", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIn(List<String> values) {
            addCriterion("pay_channel_name in", values, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotIn(List<String> values) {
            addCriterion("pay_channel_name not in", values, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameBetween(String value1, String value2) {
            addCriterion("pay_channel_name between", value1, value2, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotBetween(String value1, String value2) {
            addCriterion("pay_channel_name not between", value1, value2, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityIsNull() {
            addCriterion("channel_priority is null");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityIsNotNull() {
            addCriterion("channel_priority is not null");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityEqualTo(Integer value) {
            addCriterion("channel_priority =", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityNotEqualTo(Integer value) {
            addCriterion("channel_priority <>", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityGreaterThan(Integer value) {
            addCriterion("channel_priority >", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityGreaterThanOrEqualTo(Integer value) {
            addCriterion("channel_priority >=", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityLessThan(Integer value) {
            addCriterion("channel_priority <", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityLessThanOrEqualTo(Integer value) {
            addCriterion("channel_priority <=", value, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityIn(List<Integer> values) {
            addCriterion("channel_priority in", values, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityNotIn(List<Integer> values) {
            addCriterion("channel_priority not in", values, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityBetween(Integer value1, Integer value2) {
            addCriterion("channel_priority between", value1, value2, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andChannelPriorityNotBetween(Integer value1, Integer value2) {
            addCriterion("channel_priority not between", value1, value2, "channelPriority");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("pay_type is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("pay_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Integer value) {
            addCriterion("pay_type =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Integer value) {
            addCriterion("pay_type <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Integer value) {
            addCriterion("pay_type >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pay_type >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Integer value) {
            addCriterion("pay_type <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pay_type <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Integer> values) {
            addCriterion("pay_type in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Integer> values) {
            addCriterion("pay_type not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Integer value1, Integer value2) {
            addCriterion("pay_type between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pay_type not between", value1, value2, "payType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}