/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.common.pay.OrderFacade;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * 订单信息表
 *
 * @author wujing123
 */
@Component
public class CallbackOrderBiz extends BaseBiz {

    @Autowired
    private OrderFacade orderFacade;

    @Autowired
    private OrderInfoDao orderInfoDao;
    @Autowired
    private PayChannelDao payChannelDao;

    @Autowired
    private Map<String, PayFacade> payFacades;

    /**
     * 统一异步回调
     *
     * @param body           接收内容
     * @param payChannelCode 渠道编号
     * @return 返回内容, null为继续接收通知
     */
    public String callback(String body, String payChannelCode) {
        PayNotifyBO payNotifyBO = new PayNotifyBO();
        payNotifyBO.setPayChannelCode(payChannelCode);
        payNotifyBO.setBody(body);
        logger.info("统一异步回调:{}", payNotifyBO);
        if (StrUtil.isEmpty(payChannelCode)) {
            logger.debug("渠道编码为空{}", payChannelCode);
            return null;
        }
        PayChannel payChannel = payChannelDao.getByPayChannelCode(payChannelCode);
        if (ObjectUtil.isNull(payChannel)) {
            logger.info("渠道:[{}]不存在", payChannel.getPayChannelName());
            return null;
        }
        PayFacade payFacade = payFacades.get(payChannel.getPayObjectCode());
        PayNotifyDTO payNotifyDTO = payFacade.verify(payNotifyBO);

        // 验签成功
        if (payNotifyDTO.isVerify()) {
            // 注意：这里返回的交易号=订单的流水号
            Long serialNumber = Long.valueOf(payNotifyDTO.getOrderNo());
            logger.debug("订单{} 通道方返回数据验签成功 开始订单处理", serialNumber);
            if (StringUtils.isEmpty(serialNumber)) {
                logger.debug("传入的订单流水号不能为空!");
                return null;
            }
            if (StringUtils.isEmpty(payNotifyDTO.getOrderStatus())) {
                logger.debug("传入的交易状态不能为空!");
                return null;
            }
            // 根据流水号查找订单信息
            OrderInfo orderInfo = orderInfoDao.getBySerialNumber(serialNumber);
            if (StringUtils.isEmpty(orderInfo)) {
                logger.debug("根据传入的交易订单号{},没找到对应的订单信息!", serialNumber);
                return null;
            }
            // 如果订单状态不是待支付状态证明订单已经处理过,不用再处理
            if (!OrderStatusEnum.WAIT.getCode().equals(orderInfo.getOrderStatus())) {
                logger.debug("订单{}已经处理过,不用再处理", serialNumber);
                return null;
            }
            logger.debug("订单信息{}开始处理{}", serialNumber, payNotifyDTO.getOrderStatus());
            // 订单信息处理orderInfo
            if (orderFacade.orderInfoComplete(serialNumber, payNotifyDTO.getOrderStatus())) {
                return payNotifyDTO.getResponseStr();
            }
        } else {
            logger.debug("通道方返回数据验签失败");
        }
        return null;
    }

}
