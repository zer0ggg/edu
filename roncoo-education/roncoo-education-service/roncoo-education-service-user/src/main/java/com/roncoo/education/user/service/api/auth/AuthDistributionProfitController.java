package com.roncoo.education.user.service.api.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import com.roncoo.education.user.service.api.auth.biz.AuthDistributionProfitBiz;

/**
 * 代理分润记录 UserApi接口
 *
 * @author wujing
 * @date 2021-01-05
 */
@Api(tags = "API-AUTH-代理分润记录")
@RestController
@RequestMapping("/user/auth/distributionProfit")
public class AuthDistributionProfitController {

    @Autowired
    private AuthDistributionProfitBiz biz;

}
