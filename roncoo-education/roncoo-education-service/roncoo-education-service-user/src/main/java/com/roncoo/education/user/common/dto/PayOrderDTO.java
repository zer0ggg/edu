package com.roncoo.education.user.common.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 订单查询
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayOrderDTO", description = "订单查询")
public class PayOrderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单状态[OrderStatusEnum]")
    private int orderStatus;

}
