package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.user.feign.qo.UserExtEchartsQO;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserEchartsVO;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExtExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExtExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Component
public class FeignUserExtBiz extends BaseBiz {
    @Autowired
    private UserExtDao dao;
    @Autowired
    private UserDao userDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<UserExtVO> listForPage(UserExtQO qo) {
        UserExtExample example = new UserExtExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(qo.getMobile())) {
            c.andMobileLike(PageUtil.rightLike(qo.getMobile()));
        }
        if (!StringUtils.isEmpty(qo.getUserNo())) {
            c.andUserNoEqualTo(qo.getUserNo());
        }
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }
        if (qo.getIsVip() != null) {
            c.andIsVipEqualTo(qo.getIsVip());
        }
        if (StringUtils.hasText(qo.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(qo.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" status_id desc, id desc ");
        Page<UserExt> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<UserExtVO> listForPage = PageUtil.transform(page, UserExtVO.class);
        for (UserExtVO vo : listForPage.getList()) {
            User user = userDao.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNull(user)) {
                throw new BaseException("找不到用户信息");
            }
            if (!StringUtils.isEmpty(user.getUniconId())) {
                vo.setIsBinding(1);
            } else {
                vo.setIsBinding(0);
            }

        }
        return listForPage;
    }

    public int save(UserExtQO qo) {
        UserExt record = BeanUtil.copyProperties(qo, UserExt.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public UserExtVO getById(Long id) {
        UserExt record = dao.getById(id);
        UserExtVO vo = BeanUtil.copyProperties(record, UserExtVO.class);
        if (record.getExpireTime() != null) {
            vo.setExpireTimeString(DateUtil.format(record.getExpireTime(), "yyyy-MM-dd HH:mm:ss"));
            Calendar cal = Calendar.getInstance();
            cal.setTime(record.getExpireTime());
            long time1 = cal.getTimeInMillis();
            cal.setTime(new Date());
            long time2 = cal.getTimeInMillis();
            long surplus = (time1 - time2) / (1000 * 3600 * 24);

            if (Integer.parseInt(String.valueOf(surplus)) < 0) {
                vo.setSurplus("已过期");
            } else {
                vo.setSurplus(String.valueOf(surplus));
            }
        }
        return vo;
    }

    public int updateById(UserExtQO qo) {
        UserExt record = BeanUtil.copyProperties(qo, UserExt.class);
        if (!StringUtils.isEmpty(qo.getExpireTimeString())) {
            record.setExpireTime(DateUtil.parseDate(qo.getExpireTimeString(), "yyyy-MM-dd HH:mm:ss"));
        }
        return dao.updateById(record);
    }

    public UserExtVO getByUserNo(Long userNo) {
        UserExt record = dao.getByUserNo(userNo);
        return BeanUtil.copyProperties(record, UserExtVO.class);
    }

    /**
     * 获取用户注册量
     *
     * @param userExtEchartsQO
     * @return
     * @author wuyun
     */
    public List<UserEchartsVO> sumByCounts(UserExtEchartsQO userExtEchartsQO) {
        List<UserEchartsVO> list = new ArrayList<>();
        List<Integer> countOrders = new ArrayList<>();
        UserEchartsVO vo = new UserEchartsVO();
        for (String date : userExtEchartsQO.getDateList()) {
            Integer sum = dao.sumByCountOrders(date);
            countOrders.add(sum);
        }
        vo.setCount(countOrders);
        list.add(vo);
        return list;
    }

    /**
     * 根据状态，角色获取可用的用户信息的集合
     *
     * @author wuyun
     */
    public void cachUserForMsg() {
        int pageSize = 1000;
        Page<UserExtMsgVO> page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), 1, pageSize);
        // 缓存key条数
        set(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), page.getTotalPage(), 120);
        // 缓存用户
        for (int i = 1; i < page.getTotalPage() + 1; i++) {
            page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), i, pageSize);
            // 缓存，2个小时
            set(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, page.getList(), 120);
        }
    }

    public List<UserExtVO> listAllForUser() {
        List<UserExt> list = dao.listAllForUser(StatusIdEnum.YES.getCode());
        return ArrayListUtil.copy(list, UserExtVO.class);
    }

    public UserExtVO getByMobile(UserExtQO qo) {
        UserExt userExt = dao.getByMobile(qo.getMobile());
        return BeanUtil.copyProperties(userExt, UserExtVO.class);
    }

    public UserExtVO getByReferralCode(String referralCode) {
        UserExt userExt = dao.getByReferralCode(referralCode);
        return BeanUtil.copyProperties(userExt, UserExtVO.class);
    }

    public int userIsEnterpriseUser(UserExtQO qo) {
        UserExt userExt = dao.getByUserNo(qo.getUserNo());
        return dao.updateByUserNo(userExt);
    }

    /**
     * 批量获取用户信息
     *
     * @param qo
     * @return
     */
    public List<UserExtVO> listByUserNos(UserExtQO qo) {
        return ArrayListUtil.copy(dao.listByUserNos(qo.getUserNos()), UserExtVO.class);
    }

    /**
     * 单位分钟
     */
    private <T> T set(String key, T t, long time) {
        if (t != null) {
            myRedisTemplate.set(key, JSUtil.toJSONString(t), time, TimeUnit.MINUTES);
        }
        return t;
    }


}
