package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 会员设置
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class VipSetBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 设置类型(1.年费，2.季度，3.月度)
     */
    private Integer setType;
    /**
     * 会员原价
     */
    private BigDecimal orgPrice;
    /**
     * 会员优惠价
     */
    private BigDecimal fabPrice;
    /**
     * 续费价格
     */
    private BigDecimal renewalPrice;
}
