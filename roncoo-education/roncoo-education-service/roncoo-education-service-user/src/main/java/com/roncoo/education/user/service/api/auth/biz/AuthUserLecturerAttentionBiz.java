package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionDeleteBO;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionPageBO;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionSaveBO;
import com.roncoo.education.user.common.dto.auth.AuthUserLecturerAttentionPageDTO;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserLecturerAttentionDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@Component
public class AuthUserLecturerAttentionBiz extends BaseBiz {

    @Autowired
    private UserExtDao userExtDao;

    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private UserLecturerAttentionDao userLecturerAttentionDao;

    public Result<Page<AuthUserLecturerAttentionPageDTO>> list(AuthUserLecturerAttentionPageBO bo) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("用户异常");
        }
        UserLecturerAttentionExample example = new UserLecturerAttentionExample();
        Criteria criteria = example.createCriteria();
        criteria.andUserNoEqualTo(userExt.getUserNo());
        Page<UserLecturerAttention> page = userLecturerAttentionDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserLecturerAttentionPageDTO> listForPage = PageUtil.transform(page, AuthUserLecturerAttentionPageDTO.class);
        for (AuthUserLecturerAttentionPageDTO dto : listForPage.getList()) {
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(dto.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturer)) {
                dto.setLecturerUserName(lecturer.getLecturerName());
                dto.setHeadImgUrl(lecturer.getHeadImgUrl());
            }
        }
        return Result.success(listForPage);
    }

    public Result<Integer> save(AuthUserLecturerAttentionSaveBO bo) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("用户异常");
        }
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(bo.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("讲师异常");
        }
        UserLecturerAttention userLecturerAttention = userLecturerAttentionDao.getByUserAndLecturerUserNo(userExt.getUserNo(), lecturer.getLecturerUserNo());
        if (ObjectUtil.isNotNull(userLecturerAttention)) {
            return Result.error(ResultEnum.USER_ATTENTION);
        }
        UserLecturerAttention record = new UserLecturerAttention();
        record.setUserNo(userExt.getUserNo());
        record.setLecturerUserNo(bo.getLecturerUserNo());
        int result = userLecturerAttentionDao.save(record);
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error(ResultEnum.USER_SAVE_FAIL);
    }

    /**
     * 取消删除
     *
     * @param bo
     * @return
     */
    public Result<Integer> delet(AuthUserLecturerAttentionDeleteBO bo) {
        if (null == bo.getLecturerUserNo()) {
            return Result.error("LecturerUserNo不能为空");
        }
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("用户异常");
        }
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(bo.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("讲师异常");
        }

        UserLecturerAttention userLecturerAttention = userLecturerAttentionDao.getByUserAndLecturerUserNo(userExt.getUserNo(), bo.getLecturerUserNo());
        if (ObjectUtil.isNull(userLecturerAttention)) {
            return Result.error("已取消关注");
        }
        int result = userLecturerAttentionDao.deleteById(userLecturerAttention.getId());
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error(ResultEnum.USER_UPDATE_FAIL);
    }

}
