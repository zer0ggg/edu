package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.system.feign.vo.MsgPushVO;
import com.roncoo.education.user.common.req.MsgEditREQ;
import com.roncoo.education.user.common.req.MsgListREQ;
import com.roncoo.education.user.common.req.MsgSaveREQ;
import com.roncoo.education.user.common.resp.MsgListRESP;
import com.roncoo.education.user.common.resp.MsgViewRESP;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.service.dao.MsgDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 站内信息
 *
 * @author wujing
 */
@Component
public class PcMsgBiz extends BaseBiz {

    @Autowired
    private MsgDao dao;
    @Autowired
    private UserMsgDao userMsgDao;
    @Autowired
    private PcUserExtBiz userExtBiz;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 站内信息列表
     *
     * @param msgListREQ 站内信息分页查询参数
     * @return 站内信息分页查询结果
     */
    public Result<Page<MsgListRESP>> list(MsgListREQ msgListREQ) {
        MsgExample example = new MsgExample();
        Criteria c = example.createCriteria();
        if (msgListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(msgListREQ.getStatusId());
        }
        if (StringUtils.isNotEmpty(msgListREQ.getMsgTitle())) {
            c.andMsgTitleLike(PageUtil.like(msgListREQ.getMsgTitle()));
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<Msg> page = dao.listForPage(msgListREQ.getPageCurrent(), msgListREQ.getPageSize(), example);
        Page<MsgListRESP> respPage = PageUtil.transform(page, MsgListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 站内信息添加
     *
     * @param req 站内信息
     * @return 添加结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(MsgSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //保存msg
        Msg msg = BeanUtil.copyProperties(req, Msg.class);
        msg.setId(IdWorker.getId());
        dao.save(msg);

        if (IsToPartEnum.NO.getCode().equals(req.getIsToPart())) {
            return Result.success("添加成功");
        }

        //发给部分用户，新建userMsg
        if (StringUtils.isEmpty(req.getUserNos())) {
            return Result.error("指定用户时，用户不能为空");
        }
        String[] userNoArr = req.getUserNos().split(",");
        if (userNoArr.length > 5) {
            return Result.error("指定用户数量不宜超过5个");
        }
        for (String userNoStr : userNoArr) {
            Long userNo = Long.valueOf(userNoStr);
            UserExt userExt = userExtDao.getByUserNo(userNo);
            if (userExt == null) {
                continue;
            }
            UserMsg userMsg = new UserMsg();
            userMsg.setMsgId(msg.getId());
            userMsg.setMsgTitle(msg.getMsgTitle());
            userMsg.setUserNo(userNo);
            userMsg.setMobile(userExt.getMobile());
            userMsg.setIsTop(msg.getIsTop());
            if (IsTimeSendEnum.NO.getCode().equals(msg.getIsTimeSend())) {
                userMsg.setIsSend(IsSendEnum.NO.getCode());
            } else {
                userMsg.setIsSend(IsSendEnum.YES.getCode());
            }
            userMsgDao.save(userMsg);
        }
        return Result.success("添加成功");
    }


    /**
     * 站内信息查看
     *
     * @param id 主键ID
     * @return 站内信息
     */
    public Result<MsgViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), MsgViewRESP.class));
    }


    /**
     * 站内信息修改
     *
     * @param req 站内信息修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> edit(MsgEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Msg msg = dao.getById(req.getId());
        if (msg == null) {
            return Result.error("未查询到站内信");
        }
        if (IsSendEnum.YES.getCode().equals(msg.getIsSend())) {
            return Result.error("消息已发送，不允许修改");
        }
        //修改部分 用户站内信
        if (IsToPartEnum.YES.getCode().equals(msg.getIsToPart())) {
            UserMsg userMsg = new UserMsg();
            if (StringUtils.isNotEmpty(req.getMsgTitle())) {
                userMsg.setMsgTitle(req.getMsgTitle());
            }
            if (req.getIsTop() != null) {
                userMsg.setIsTop(req.getIsTop());
            }
            UserMsgExample example = new UserMsgExample();
            UserMsgExample.Criteria c = example.createCriteria();
            c.andMsgIdEqualTo(req.getId());
            userMsgDao.updateBySelective(userMsg, example);
        }
        Msg record = BeanUtil.copyProperties(req, Msg.class);

        //修改站内信
        dao.updateById(record);
        return Result.success("编辑成功");
    }

    /**
     * 站内信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        dao.deleteById(id);
        userMsgDao.deleteByMsgId(id);
        return Result.success("删除成功");
    }

    /**
     * 手动推送站内信
     * 全量类型：新建用户站内信
     * 部分类型：将站内信isSend置为true
     *
     * @param id 站内消息ID
     * @return 推送结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> pushByManual(Long id) {
        // 获得模板
        Msg msg = dao.getById(id);
        if (msg == null) {
            return Result.error("获取消息失败");
        }
        // 进行推送前，将当前站内信推送状态置为已通知
        updateMsg(id);

        final MsgPushVO msgPush = BeanUtil.copyProperties(msg, MsgPushVO.class);
        CALLBACK_EXECUTOR.execute(() -> pushToUserByMsgPush(msgPush));
        return Result.success("发送处理中");
    }

    private void updateMsg(Long id) {
        Msg msgNew = new Msg();
        msgNew.setId(id);
        msgNew.setIsSend(HasNoticeEnum.YES.getCode());
        msgNew.setSendTime(new Date());
        dao.updateById(msgNew);
    }

    private int getCacheNum() {
        boolean flag = hasKey(RedisPreEnum.SYS_MSG_SEND_NUM.getCode());
        if (!flag) {
            // 找不到，去缓存用户信息
            userExtBiz.cacheUserForMsg();
        }
        return get(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), int.class);
    }

    private void pushToUserByMsgPush(MsgPushVO msgPush) {

        if (IsToPartEnum.YES.getCode().equals(msgPush.getIsToPart())) {
            // 部分类型，刷新用户站内信
            UserMsg userMsg = new UserMsg();
            userMsg.setIsSend(IsSendEnum.YES.getCode());
            UserMsgExample example = new UserMsgExample();
            UserMsgExample.Criteria c = example.createCriteria();
            c.andMsgIdEqualTo(msgPush.getId());
            userMsgDao.updateBySelective(userMsg, example);
        } else {
            // 全量类型，新建用户站内信
            // 获取缓存的条数
            int num = getCacheNum();
            for (int i = 1; i < num + 1; i++) {
                List<UserExtMsgVO> list = list(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, UserExtMsgVO.class);
                if (CollectionUtil.isNotEmpty(list)) {
                    // 批量生成
                    for (UserExtMsgVO vo : list) {
                        saveUserMsg(msgPush, vo, IsSendEnum.YES.getCode());
                    }
                }
            }
        }
    }

    /**
     * 生成用户站内信
     *
     * @param msg
     * @param vo
     * @param isSend 是否已经发送
     */
    private void saveUserMsg(MsgPushVO msg, UserExtMsgVO vo, Integer isSend) {
        UserMsg record = new UserMsg();
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setMsgId(msg.getId());
        record.setMsgTitle(msg.getMsgTitle());
        record.setUserNo(vo.getUserNo());
        record.setMobile(vo.getMobile());
        record.setIsTop(msg.getIsTop());
        record.setIsSend(isSend);
        userMsgDao.save(record);
    }

    private <T> List<T> list(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseArray(myRedisTemplate.get(key), clazz);
        }
        return null;
    }

    private boolean hasKey(Object key) {
        return myRedisTemplate.hasKey(key);
    }

    private <T> T get(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseObject(myRedisTemplate.get(key), clazz);
        }
        return null;
    }
}
