package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserLogModifiedDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserLogModifiedMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogModified;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogModifiedExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserLogModifiedDaoImpl implements UserLogModifiedDao {
    @Autowired
    private UserLogModifiedMapper userLogModifiedMapper;

    @Override
    public int save(UserLogModified record) {
        record.setId(IdWorker.getId());
        return this.userLogModifiedMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.userLogModifiedMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserLogModified record) {
        return this.userLogModifiedMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserLogModified getById(Long id) {
        return this.userLogModifiedMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserLogModified> listForPage(int pageCurrent, int pageSize, UserLogModifiedExample example) {
        int count = this.userLogModifiedMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserLogModified>(count, totalPage, pageCurrent, pageSize, this.userLogModifiedMapper.selectByExample(example));
    }
}
