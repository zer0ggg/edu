package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchEditBO;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchSaveBO;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchViewBO;
import com.roncoo.education.user.common.dto.UserResearchViewDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserResearchBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户调研信息 UserApi接口
 *
 * @author wujing
 * @date 2020-10-21
 */
@Api(tags = "API-AUTH-用户调研信息")
@RestController
@RequestMapping("/user/auth/user/research")
public class AuthUserResearchController {

    @Autowired
    private AuthUserResearchBiz biz;

    /**
     * 用户调研信息-保存接口
     *
     * @param authUserResearchSaveBO
     * @author kyh
     */
    @ApiOperation(value = "用户调研信息-保存接口", notes = "用户调研信息-保存接口")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result<String> save(@RequestBody AuthUserResearchSaveBO authUserResearchSaveBO) {
        return biz.save(authUserResearchSaveBO);
    }

    /**
     * 用户调研信息-修改接口
     *
     * @param authUserResearchEditBO
     * @author kyh
     */
    @ApiOperation(value = "用户调研信息-修改接口", notes = "用户调研信息-修改接口")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public Result<String> edit(@RequestBody AuthUserResearchEditBO authUserResearchEditBO) {
        return biz.edit(authUserResearchEditBO);
    }


    /**
     * 用户调研信息-保存接口
     *
     * @param authUserResearchViewBO
     * @author kyh
     */
    @ApiOperation(value = "用户调研信息-保存接口", notes = "用户调研信息-保存接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<UserResearchViewDTO> view(@RequestBody AuthUserResearchViewBO authUserResearchViewBO) {
        return biz.view(authUserResearchViewBO);
    }

}
