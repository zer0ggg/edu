package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 手工录单参数
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "OrderInfoManualREQ", description = "手工录单参数")
public class OrderInfoManualREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品类型")
    private Integer productType;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "讲师是否可见")
    private Integer isShowLecturer;

    @ApiModelProperty(value = "用户是否可见")
    private Integer isShowUser;

    @ApiModelProperty(value = "备注内容")
    private String remark;


}
