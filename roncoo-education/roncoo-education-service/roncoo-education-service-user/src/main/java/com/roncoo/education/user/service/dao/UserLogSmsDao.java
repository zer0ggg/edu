package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSms;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample;

import java.util.List;

public interface UserLogSmsDao {
	int save(UserLogSms record);

	int deleteById(Long id);

	int updateById(UserLogSms record);

	UserLogSms getById(Long id);

	Page<UserLogSms> listForPage(int pageCurrent, int pageSize, UserLogSmsExample example);

	/**
	 * 查找所有短信信息
	 *
	 * @author kyh
	 */
	List<UserLogSms> listByAll();

	/**
	 * 根据是否发送成功状态查找短信信息
	 *
	 * @param isSuccess
	 * @author kyh
	 */
	List<UserLogSms> listByIsSuccess(Integer isSuccess);
}
