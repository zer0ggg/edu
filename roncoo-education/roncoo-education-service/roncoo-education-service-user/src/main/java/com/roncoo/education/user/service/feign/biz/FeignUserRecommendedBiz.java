package com.roncoo.education.user.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.UserRecommendedQO;
import com.roncoo.education.user.feign.vo.UserRecommendedVO;
import com.roncoo.education.user.service.dao.UserRecommendedDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户推荐信息
 *
 * @author wujing
 */
@Component
public class FeignUserRecommendedBiz extends BaseBiz {

    @Autowired
    private UserRecommendedDao dao;

	public Page<UserRecommendedVO> listForPage(UserRecommendedQO qo) {
	    UserRecommendedExample example = new UserRecommendedExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserRecommended> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserRecommendedVO.class);
	}

	public int save(UserRecommendedQO qo) {
		UserRecommended record = BeanUtil.copyProperties(qo, UserRecommended.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserRecommendedVO getById(Long id) {
		UserRecommended record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserRecommendedVO.class);
	}

	public int updateById(UserRecommendedQO qo) {
		UserRecommended record = BeanUtil.copyProperties(qo, UserRecommended.class);
		return dao.updateById(record);
	}

}
