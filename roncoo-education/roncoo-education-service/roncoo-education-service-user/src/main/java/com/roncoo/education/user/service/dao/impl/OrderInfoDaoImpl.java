package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.impl.mapper.OrderInfoMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

@Repository
public class OrderInfoDaoImpl extends AbstractBaseJdbc implements OrderInfoDao {
    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Override
    public int save(OrderInfo record) {
        if (record.getId() == null) {
            record.setId(IdWorker.getId());
        }
        return this.orderInfoMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.orderInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(OrderInfo record) {
        return this.orderInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public OrderInfo getById(Long id) {
        return this.orderInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<OrderInfo> listForPage(int pageCurrent, int pageSize, OrderInfoExample example) {
        int count = this.orderInfoMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<OrderInfo>(count, totalPage, pageCurrent, pageSize, this.orderInfoMapper.selectByExample(example));
    }

    @Override
    public OrderInfo getByUserNoAndProductId(Long userNo, Long productId) {
        OrderInfoExample example = new OrderInfoExample();
        example.createCriteria().andUserNoEqualTo(userNo).andProductIdEqualTo(productId);
        example.setOrderByClause(" id desc ");
        List<OrderInfo> list = this.orderInfoMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * 根据订单编号查找订单信息
     */
    @Override
    public OrderInfo getByOrderNo(long orderNo) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        c.andOrderNoEqualTo(orderNo);
        List<OrderInfo> list = this.orderInfoMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public BigDecimal sumByLecturerUserNoAndCourseCategoryAndData(Long lecturerUserNo, Integer courseCategory, String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(lecturer_profit) as sum from order_info where ");
        builder.append(" lecturer_user_no = ").append(lecturerUserNo);
        if (courseCategory != null) {
            builder.append(" and course_category = ").append(courseCategory);
        }
        builder.append(" and order_status = 2 ");
        builder.append(" and pay_time >= '").append(date).append(" 00:00:00' ");
        builder.append("and pay_time <= '").append(date).append(" 23:59:59'");
        String sql = builder.toString();
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        BigDecimal paidPrice = BigDecimal.valueOf(0);
        if (!StringUtils.isEmpty(map.get("sum"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("sum")));
            paidPrice = bd.setScale(2, RoundingMode.DOWN);
        }
        return paidPrice;
    }

    /**
     * 统计时间段内机构的总订单数
     */
    @Override
    public Integer sumByCountOrders(String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) as count from order_info where");
        builder.append(" order_status = 2 ");
        builder.append("and pay_time >= '").append(date).append(" 00:00:00' ");
        builder.append("and pay_time <= '").append(date).append(" 23:59:59'");
        String sql = builder.toString();
        Integer count = 0;
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        if (!StringUtils.isEmpty(map.get("count"))) {
            count = Integer.valueOf(String.valueOf(map.get("count")));
        }

        return count;
    }

    /**
     * 统计时间段内机构的总收入
     */
    @Override
    public BigDecimal sumByPayTime(String date) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(price_paid) paidPrice from order_info where");
        builder.append(" order_status = 2 ");
        builder.append("and pay_time >= '").append(date).append(" 00:00:00' ");
        builder.append("and pay_time <= '").append(date).append(" 23:59:59'");
        String sql = builder.toString();

        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        BigDecimal paidPrice = BigDecimal.valueOf(0);
        if (!StringUtils.isEmpty(map.get("paidPrice"))) {
            BigDecimal bd = new BigDecimal(String.valueOf(map.get("paidPrice")));
            paidPrice = bd.setScale(2, RoundingMode.DOWN);
        }
        return paidPrice;
    }

    @Override
    public OrderInfo getBySerialNumber(long serialNumber) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        c.andSerialNumberEqualTo(serialNumber);
        List<OrderInfo> list = this.orderInfoMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<OrderInfo> listByUserNo(Long userNo) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        return this.orderInfoMapper.selectByExample(example);
    }

    @Override
    public int updateByMobile(Long userNo, String mobile) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUserNo(userNo);
        orderInfo.setMobile(mobile);
        return this.orderInfoMapper.updateByExampleSelective(orderInfo, example);
    }

}
