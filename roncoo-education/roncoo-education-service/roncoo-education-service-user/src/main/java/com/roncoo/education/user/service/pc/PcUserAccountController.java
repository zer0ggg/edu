package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserAccountEditREQ;
import com.roncoo.education.user.common.req.UserAccountListREQ;
import com.roncoo.education.user.common.req.UserAccountSaveREQ;
import com.roncoo.education.user.common.resp.UserAccountListRESP;
import com.roncoo.education.user.common.resp.UserAccountViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserAccountBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户账户信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/account")
@Api(value = "user-用户账户信息", tags = {"user-用户账户信息"})
public class PcUserAccountController {

    @Autowired
    private PcUserAccountBiz biz;

    @ApiOperation(value = "用户账户信息列表", notes = "用户账户信息列表")
    @PostMapping(value = "/list")
    public Result<Page<UserAccountListRESP>> list(@RequestBody UserAccountListREQ userAccountListREQ) {
        return biz.list(userAccountListREQ);
    }

    @ApiOperation(value = "用户账户信息添加", notes = "用户账户信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserAccountSaveREQ userAccountSaveREQ) {
        return biz.save(userAccountSaveREQ);
    }

    @ApiOperation(value = "用户账户信息查看", notes = "用户账户信息查看")
    @GetMapping(value = "/view")
    public Result<UserAccountViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户账户信息修改", notes = "用户账户信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserAccountEditREQ userAccountEditREQ) {
        return biz.edit(userAccountEditREQ);
    }

    @ApiOperation(value = "用户账户信息删除", notes = "用户账户信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
