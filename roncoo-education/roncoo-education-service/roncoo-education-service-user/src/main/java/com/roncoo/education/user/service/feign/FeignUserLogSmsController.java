package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserLogSms;
import com.roncoo.education.user.feign.qo.UserLogSmsQO;
import com.roncoo.education.user.feign.vo.UserLogSmsVO;
import com.roncoo.education.user.service.feign.biz.FeignUserLogSmsBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户发送短信日志
 *
 * @author YZJ
 */
@RestController
public class FeignUserLogSmsController extends BaseController implements IFeignUserLogSms {

	@Autowired
	private FeignUserLogSmsBiz biz;

	@Override
	public Page<UserLogSmsVO> listForPage(@RequestBody UserLogSmsQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserLogSmsQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserLogSmsQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public UserLogSmsVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public int send(@RequestBody UserLogSmsQO qo) {
		return biz.send(qo);
	}

	@Override
	public UserLogSmsVO statistical() {
		return biz.statistical();
	}

}
