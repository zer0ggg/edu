package com.roncoo.education.user.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 异步通知
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayNotifyBO", description = "异步通知")
public class PayNotifyBO implements Serializable {

	private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "渠道编号")
    private String payChannelCode;

    @ApiModelProperty(value = "返回参数")
    private String body;

}
