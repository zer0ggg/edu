package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ProfitStatusEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.user.common.ReportExcelUtil;
import com.roncoo.education.user.common.req.UserAccountExtractLogEditREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogExprotREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogListREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogSaveREQ;
import com.roncoo.education.user.common.resp.UserAccountExtractLogListRESP;
import com.roncoo.education.user.common.resp.UserAccountExtractLogRESP;
import com.roncoo.education.user.common.resp.UserAccountExtractLogViewRESP;
import com.roncoo.education.user.service.dao.UserAccountExtractLogDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 用户账户提现日志
 *
 * @author wujing
 */
@Component
public class PcUserAccountExtractLogBiz extends BaseBiz {

    @Autowired
    private UserAccountExtractLogDao dao;

    /**
     * 用户账户提现日志列表
     *
     * @param userAccountExtractLogListREQ 用户账户提现日志分页查询参数
     * @return 用户账户提现日志分页查询结果
     */
    public Result<Page<UserAccountExtractLogListRESP>> list(UserAccountExtractLogListREQ userAccountExtractLogListREQ) {
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        Criteria c = example.createCriteria();
        // 手机号
        if (ObjectUtil.isNotEmpty(userAccountExtractLogListREQ.getPhone())) {
            c.andPhoneLike(SqlUtil.like(userAccountExtractLogListREQ.getPhone()));
        }
        // 申请状态
        if (ObjectUtil.isNotEmpty(userAccountExtractLogListREQ.getExtractStatus())) {
            c.andExtractStatusEqualTo(userAccountExtractLogListREQ.getExtractStatus());
        }
        // 申请时间
        if (StringUtils.hasText(userAccountExtractLogListREQ.getBeginTime())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(userAccountExtractLogListREQ.getBeginTime(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(userAccountExtractLogListREQ.getEndTime())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(userAccountExtractLogListREQ.getEndTime(), "yyyy-MM-dd"), 1));
        }
        Page<UserAccountExtractLog> page = dao.listForPage(userAccountExtractLogListREQ.getPageCurrent(), userAccountExtractLogListREQ.getPageSize(), example);
        Page<UserAccountExtractLogListRESP> respPage = PageUtil.transform(page, UserAccountExtractLogListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 用户账户提现日志添加
     *
     * @param userAccountExtractLogSaveREQ 用户账户提现日志
     * @return 添加结果
     */
    public Result<String> save(UserAccountExtractLogSaveREQ userAccountExtractLogSaveREQ) {
        UserAccountExtractLog record = BeanUtil.copyProperties(userAccountExtractLogSaveREQ, UserAccountExtractLog.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户账户提现日志查看
     *
     * @param id 主键ID
     * @return 用户账户提现日志
     */
    public Result<UserAccountExtractLogViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserAccountExtractLogViewRESP.class));
    }


    /**
     * 用户账户提现日志修改
     *
     * @param userAccountExtractLogEditREQ 用户账户提现日志修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserAccountExtractLogEditREQ userAccountExtractLogEditREQ) {
        UserAccountExtractLog record = BeanUtil.copyProperties(userAccountExtractLogEditREQ, UserAccountExtractLog.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户账户提现日志删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 用户账户提现日志删除
     *
     * @param ids ID主键
     * @return 删除结果
     */
    public Result<String> batchAudit(String ids) {
        if (StrUtil.isBlank(ids)) {
            return Result.error("请勾选数据");
        }
        String[] id = ids.split(",");
        for (String i : id) {
            UserAccountExtractLog userAccountExtractLog = new UserAccountExtractLog();
            userAccountExtractLog.setId(Long.valueOf(i));
            userAccountExtractLog.setExtractStatus(ProfitStatusEnum.SUCCESS.getCode());
            dao.updateById(userAccountExtractLog);
        }
        return Result.success("操作完成");
    }

    public void export(HttpServletResponse response, UserAccountExtractLogExprotREQ req) {
        try {
            // 设置强制下载不打开
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            // 设置文件名
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode("分润报表", "utf-8") + ".xlsx");
        } catch (UnsupportedEncodingException e) {
            logger.error("导出文件失败", e);
        }
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(req.getPhone())) {
            criteria.andPhoneEqualTo(req.getPhone());
        }
        if (!StringUtils.isEmpty(req.getExtractStatus())) {
            criteria.andExtractStatusEqualTo(req.getExtractStatus());
        }
        // 申请时间
        if (StringUtils.hasText(req.getBeginTime())) {
            criteria.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(req.getBeginTime(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(req.getEndTime())) {
            criteria.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(req.getEndTime(), "yyyy-MM-dd"), 1));
        }
        Page<UserAccountExtractLog> result = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        ReportExcelUtil.exportExcelForLecturerProfit(response, PageUtil.transform(result, UserAccountExtractLogRESP.class));
    }
}
