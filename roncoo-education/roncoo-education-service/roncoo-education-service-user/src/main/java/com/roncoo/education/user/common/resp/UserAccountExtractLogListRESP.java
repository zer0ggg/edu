package com.roncoo.education.user.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户账户提现日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserAccountExtractLogListRESP", description="用户账户提现日志列表")
public class UserAccountExtractLogListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "银行卡号")
    private String bankCardNo;

    @ApiModelProperty(value = "银行名称")
    private String bankName;

    @ApiModelProperty(value = "银行开户名")
    private String bankUserName;

    @ApiModelProperty(value = "提现金额")
    private BigDecimal extractMoney;

    @ApiModelProperty(value = "用户收入")
    private BigDecimal userIncome;

    @ApiModelProperty(value = "提现状态（1申请中，2支付中，3确认中，4成功，5失败）")
    private Integer extractStatus;


    @ApiModelProperty(value = "申请时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

}
