package com.roncoo.education.user.common.pay.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.OrderLogVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.ExamInfoQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefSaveQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.interfaces.IFeignVipSet;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.user.common.pay.OrderFacade;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class OrderImpl extends BaseBiz implements OrderFacade {

    @Autowired
    private OrderInfoDao orderInfoDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private ChosenDao chosenDao;
    @Autowired
    private DistributionDao distributionDao;
    @Autowired
    private DistributionProfitDao distributionProfitDao;

    @Autowired
    private IFeignVipSet feignVipSet;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignUserOrderExamRef feignUserOrderExamRef;
    @Autowired
    private IFeignOrderLog feignOrderLog;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public Boolean orderInfoComplete(Long serialNumber, Integer orderStatus) {
        OrderInfo orderInfo = orderInfoDao.getBySerialNumber(serialNumber);
        if (ObjectUtil.isNull(orderInfo)) {
            logger.debug("根据传入的交易订单流水号{},没找到对应的订单信息", serialNumber);
            return false;
        }
        // 订单状态为成功时处理
        if (OrderStatusEnum.SUCCESS.getCode().equals(orderStatus)) {
            // 处理课程购买成功业务
            if (ProductTypeEnum.ORDINARY.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.LIVE.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.RESOURCES.getCode().equals(orderInfo.getProductType())) {
                return courseOrderCompleteHandle(orderInfo);
            }
            // 处理会员购买成功业务
            if (ProductTypeEnum.VIP.getCode().equals(orderInfo.getProductType())) {
                return vipOrderCompleteHandle(orderInfo);
            }
            // 处理试卷购买成功业务
            if (ProductTypeEnum.EXAM.getCode().equals(orderInfo.getProductType())) {
                return examOrderCompleteHandle(orderInfo);
            }
        } else if (OrderStatusEnum.FAIL.getCode().equals(orderStatus)) {
            // 更新订单信息
            Date payTime = new Date();
            orderInfo.setPayTime(payTime);
            orderInfo.setOrderStatus(OrderStatusEnum.FAIL.getCode());
            orderInfoDao.updateById(orderInfo);
            OrderLogVO orderLogVO = new OrderLogVO();
            if (orderInfo.getActTypeId() != null) {
                orderLogVO.setActTypeId(orderInfo.getActTypeId());
                orderLogVO.setActType(orderInfo.getActType());
            }
            orderLogVO.setOrderNo(orderInfo.getOrderNo());
            orderLogVO.setOrderStatus(OrderStatusEnum.FAIL.getCode());
            // 异步更新支付日志
            CALLBACK_EXECUTOR.execute(new orderLog(orderLogVO));
            return true;
        }
        return false;
    }

    /**
     * 会员订单完成处理流程
     *
     * @param orderInfo 订单信息
     * @return 处理结果
     */
    private Boolean vipOrderCompleteHandle(OrderInfo orderInfo) {
        UserExt userExt = userExtDao.getByUserNo(orderInfo.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            logger.debug("找不到用户信息{}", orderInfo.getOrderNo());
            return false;
        }
        VipSetVO vipSetVO = feignVipSet.getById(orderInfo.getProductId());
        if (ObjectUtil.isNull(vipSetVO)) {
            logger.debug("找不到会员设置{}", orderInfo.getOrderNo());
            return false;
        }
        if (userExt.getExpireTime() != null && VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            // 如果是会员，且在有效期内为续费
            userExt.setExpireTime(DateUtil.addDate(userExt.getExpireTime(), VipTypeEnum.getByExpireTime(vipSetVO.getSetType()).getExpireTime()));
            userExt.setVipType(vipSetVO.getSetType());
        } else {
            userExt.setExpireTime(DateUtil.addDate(new Date(), VipTypeEnum.getByExpireTime(vipSetVO.getSetType()).getExpireTime()));
            userExt.setIsVip(IsVipEnum.YES.getCode());
            userExt.setVipType(vipSetVO.getSetType());
        }
        userExtDao.updateById(userExt);

        // 用户代理分成结算
        Map<String, BigDecimal> map = profit(orderInfo, orderInfo.getPricePaid());
        orderInfo.setAgentIncome(map.get("distributionParent"));

        // 平台分成
        orderInfo.setPlatformIncome(map.get("pricePaid"));

        // 更新订单信息
        Date payTime = new Date();
        orderInfo.setPayTime(payTime);
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfoDao.updateById(orderInfo);
        // 更新订单支付信息
        OrderLogVO orderLogVO = new OrderLogVO();
        orderLogVO.setOrderNo(orderInfo.getOrderNo());
        orderLogVO.setPlatformIncome(orderInfo.getPricePaid());
        orderLogVO.setLecturerIncome(orderInfo.getLecturerIncome());
        orderLogVO.setPayTime(payTime);
        orderLogVO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());

        CALLBACK_EXECUTOR.execute(new orderLog(orderLogVO));
        return true;
    }

    /**
     * 课程处理
     *
     * @param orderInfo 订单信息
     * @return 是否处理完成
     * @author wuyun
     */
    private Boolean courseOrderCompleteHandle(OrderInfo orderInfo) {
        // 根据课程No查找课程信息
        CourseVO course = feignCourse.getById(orderInfo.getProductId());
        if (StringUtils.isEmpty(course)) {
            logger.debug("根据订单的课程编号,没找到对应的课程信息!");
            return false;
        }

        // 根据讲师编号查找讲师账户信息
        UserAccount lecturerAccount = userAccountDao.getByUserNoAndCheck(course.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerAccount)) {
            logger.debug("讲师账户信息异常!");
            return false;
        }

        Lecturer lecturer = lecturerDao.getByLecturerUserNo(course.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer)) {
            logger.debug("找不到讲师信息");
            return false;
        }
        UserOrderCourseRefQO userOrderCourseRef = BeanUtil.copyProperties(orderInfo, UserOrderCourseRefQO.class);
        userOrderCourseRef.setCourseId(orderInfo.getProductId());
        userOrderCourseRef.setCourseCategory(orderInfo.getProductType());
        userOrderCourseRef.setCourseType(CourseTypeEnum.COURSE.getCode());
        userOrderCourseRef.setRefId(orderInfo.getProductId());
        userOrderCourseRef.setIsPay(IsPayEnum.YES.getCode());
        userOrderCourseRef.setIsStudy(IsStudyEnum.NO.getCode());
        userOrderCourseRef.setExpireTime(DateUtil.addYear(new Date(), 1));

        // 如果用户购买过，更新
        UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();

        userOrderCourseRefQO.setUserNo(orderInfo.getUserNo());
        userOrderCourseRefQO.setRefId(orderInfo.getProductId());
        UserOrderCourseRefVO userOrderCourse = feignUserOrderCourseRef.getByUserNoAndRefId(userOrderCourseRefQO);
        if (ObjectUtil.isNull(userOrderCourse)) {
            feignUserOrderCourseRef.save(userOrderCourseRef);
        } else {
            userOrderCourseRef.setId(userOrderCourse.getId());
            feignUserOrderCourseRef.updateById(userOrderCourseRef);
        }

        // 更新课程信息的购买人数
        course.setCountBuy(course.getCountBuy() + 1);
        CourseQO courseQO = BeanUtil.copyProperties(course, CourseQO.class);
        feignCourse.updateById(courseQO);
        // 计算结算金额
        orderInfo = settlement(orderInfo, lecturer.getLecturerProportion());

        // 更新讲师账户信息
        UserAccount lecturerAccountRecord = new UserAccount();
        lecturerAccountRecord.setUserNo(lecturerAccount.getUserNo());
        lecturerAccountRecord.setTotalIncome(orderInfo.getLecturerIncome().add(lecturerAccount.getEnableBalances()));
        lecturerAccountRecord.setEnableBalances(orderInfo.getLecturerIncome().add(lecturerAccount.getEnableBalances()));
        lecturerAccountRecord.setSign(SignUtil.getByLecturer(lecturerAccountRecord.getTotalIncome(), lecturerAccount.getHistoryMoney(), lecturerAccountRecord.getEnableBalances(), lecturerAccount.getFreezeBalances()));
        userAccountDao.updateByUserNo(lecturerAccountRecord);


        // 更新订单信息
        Date payTime = new Date();
        orderInfo.setPayTime(payTime);
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfoDao.updateById(orderInfo);
        // 更新订单支付信息

        OrderLogVO orderLogVO = new OrderLogVO();
        orderLogVO.setOrderNo(orderInfo.getOrderNo());
        orderLogVO.setPlatformIncome(orderInfo.getPlatformIncome());
        orderLogVO.setLecturerIncome(orderInfo.getLecturerIncome());
        orderLogVO.setPayTime(payTime);
        orderLogVO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());

        CALLBACK_EXECUTOR.execute(new orderLog(orderLogVO));
        return true;
    }

    private OrderInfo settlement(OrderInfo orderInfo, BigDecimal lecturerProportion) {
        Map<String, BigDecimal> courseMap = chosen(orderInfo);
        orderInfo.setChosenIncome(courseMap.get("chosenProfit"));
        Map<String, BigDecimal> map = profit(orderInfo, courseMap.get("pricePaid"));
        // 讲师收入 = 订单金额 * 讲师分成比例   保留2位小数，四舍五入到零
        BigDecimal lecturerProfit = NumberUtil.mul(map.get("pricePaid"), lecturerProportion).setScale(2, RoundingMode.DOWN);
        // 平台收入 = 订单金额 - 讲师收入
        BigDecimal platformProfit = NumberUtil.sub(map.get("pricePaid"), lecturerProfit).setScale(2, RoundingMode.DOWN);
        orderInfo.setLecturerIncome(lecturerProfit);
        orderInfo.setPlatformIncome(platformProfit);

        orderInfo.setAgentIncome(map.get("distributionParent"));
        return orderInfo;
    }

    private Map<String, BigDecimal> chosen(OrderInfo orderInfo) {
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("chosenProfit", BigDecimal.ZERO);
        map.put("pricePaid", orderInfo.getPricePaid());

        // 分销订单
        if (StrUtil.isNotEmpty(orderInfo.getReferralCode())) {
            Chosen chosen = chosenDao.getByCourseId(orderInfo.getProductId());
            if (ObjectUtil.isNotNull(chosen) && StatusIdEnum.YES.getCode().equals(chosen.getStatusId())) {
                // 分销收入 = 订单金额 * (分销比例 / 100)
                BigDecimal chosenProfit = NumberUtil.div(NumberUtil.mul(orderInfo.getPricePaid(), chosen.getChosenPercent()), 100).setScale(2, RoundingMode.DOWN);
                // 更新分销账户信息
                UserExt chosenUser = userExtDao.getByReferralCode(orderInfo.getReferralCode());
                // 根据分销编号查找分销账户信息
                UserAccount chosenAccount = userAccountDao.getByUserNoAndCheck(chosenUser.getUserNo());
                if (ObjectUtil.isNull(chosenAccount)) {
                    logger.debug("分销账户信息异常!");
                    return map;
                }
                map.put("chosenProfit", chosenProfit);
                map.put("pricePaid", NumberUtil.sub(orderInfo.getPricePaid(), chosenProfit));
                // 更新分销人账户
                UserAccount chosenAccountRecord = new UserAccount();
                chosenAccountRecord.setUserNo(chosenAccount.getUserNo());
                chosenAccountRecord.setTotalIncome(chosenProfit.add(chosenAccount.getEnableBalances()));
                chosenAccountRecord.setEnableBalances(chosenProfit.add(chosenAccount.getEnableBalances()));
                chosenAccountRecord.setSign(SignUtil.getByLecturer(chosenAccountRecord.getTotalIncome(), chosenAccount.getHistoryMoney(), chosenAccountRecord.getEnableBalances(), chosenAccount.getFreezeBalances()));
                userAccountDao.updateByUserNo(chosenAccountRecord);
                // 更新分销数量
                chosen.setChosenCount(chosen.getChosenCount() + 1);
                chosenDao.updateById(chosen);
                orderInfo.setChosenIncome(chosenProfit);
            }
        }
        return map;
    }

    private Map<String, BigDecimal> profit(OrderInfo orderInfo, BigDecimal pricePaid) {
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("pricePaid", pricePaid);
        map.put("distributionParent", BigDecimal.ZERO);
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey("isAgentBenefit");
        if (StringUtils.isEmpty(sysConfigVO) || sysConfigVO.getConfigValue().equals("0")) {
            return map;
        }
        Distribution distribution = distributionDao.getByUserNo(orderInfo.getUserNo());
        if (ObjectUtil.isNull(distribution)) {
            return map;
        } else if (distribution.getParentId().equals(0L)) {
            return map;
        }
        // 订单金额 - 分销结算
        BigDecimal distributionParent = distributionParent(distribution.getParentId(), pricePaid, orderInfo);
        BigDecimal platformProfit = NumberUtil.sub(pricePaid, distributionParent).setScale(2, RoundingMode.DOWN);
        map.put("pricePaid", platformProfit);
        map.put("distributionParent", distributionParent);
        return map;
    }

    /**
     * 分销结算只允许往上分3级
     *
     * @param parentId
     * @param orderInfo
     * @return
     */
    private BigDecimal distributionParent(Long parentId, BigDecimal pricePaid, OrderInfo orderInfo) {
        Distribution updateRecord = new Distribution();
        DistributionProfit record = new DistributionProfit();
        record.setOrderNo(orderInfo.getOrderNo());
        record.setOrderPrice(orderInfo.getPricePaid());
        record.setProductId(orderInfo.getProductId());
        record.setProductName(orderInfo.getProductName());
        record.setProductType(orderInfo.getProductType());

        // 三级结算
        Distribution distributionParent = distributionDao.getById(parentId);
        BigDecimal profitMoney = BigDecimal.ZERO;
        if (ObjectUtil.isNull(distributionParent)) {
            return profitMoney;
        }

        UserAccount chosenAccount = userAccountDao.getByUserNoAndCheck(distributionParent.getUserNo());
        if (ObjectUtil.isNull(chosenAccount)) {
            logger.debug("分销账户信息异常!");
            return profitMoney;
        }
        record.setDistributionId(distributionParent.getId());
        record.setUserNo(distributionParent.getUserNo());
        record.setParentId(distributionParent.getParentId());
        record.setFloor(distributionParent.getFloor());
        record.setProfitType(ProfitTypeEnum.PROMOTE.getCode());
        record.setProfit(distributionParent.getSpreadProfit());
        BigDecimal profit = NumberUtil.mul(pricePaid, distributionParent.getSpreadProfit()).setScale(2, RoundingMode.DOWN);
        record.setProfitMoney(profit);
        distributionProfitDao.save(record);



        // 更新分销人账户
        UserAccount chosenAccountRecord = new UserAccount();
        chosenAccountRecord.setUserNo(chosenAccount.getUserNo());
        chosenAccountRecord.setTotalIncome(profit.add(chosenAccount.getEnableBalances()));
        chosenAccountRecord.setEnableBalances(profit.add(chosenAccount.getEnableBalances()));
        chosenAccountRecord.setSign(SignUtil.getByLecturer(chosenAccountRecord.getTotalIncome(), chosenAccount.getHistoryMoney(), chosenAccountRecord.getEnableBalances(), chosenAccount.getFreezeBalances()));
        userAccountDao.updateByUserNo(chosenAccountRecord);

        updateRecord.setId(distributionParent.getId());
        updateRecord.setEarnings(distributionParent.getEarnings().add(profit));
        distributionDao.updateById(updateRecord);

        profitMoney = profitMoney.add(profit);
        if (distributionParent.getParentId().equals(0L)) {
            return profitMoney;
        }

        // 二级结算
        Distribution distributionParent2 = distributionDao.getById(distributionParent.getParentId());
        if (ObjectUtil.isNull(distributionParent2)) {
            return profitMoney;
        }

        UserAccount chosenAccount2 = userAccountDao.getByUserNoAndCheck(distributionParent2.getUserNo());
        if (ObjectUtil.isNull(chosenAccount2)) {
            logger.debug("分销账户信息异常!");
            return profitMoney;
        }
        record.setDistributionId(distributionParent2.getId());
        record.setUserNo(distributionParent2.getUserNo());
        record.setParentId(distributionParent2.getParentId());
        record.setFloor(distributionParent2.getFloor());
        record.setProfitType(ProfitTypeEnum.INVITATION.getCode());
        record.setProfit(distributionParent2.getInviteProfit());
        record.setProfitMoney(NumberUtil.mul(pricePaid, distributionParent2.getInviteProfit()).setScale(2, RoundingMode.DOWN));
        distributionProfitDao.save(record);

        // 更新分销人账户
        UserAccount chosenAccount2Record = new UserAccount();
        chosenAccount2Record.setUserNo(chosenAccount2.getUserNo());
        chosenAccount2Record.setTotalIncome(record.getProfitMoney().add(chosenAccount2.getEnableBalances()));
        chosenAccount2Record.setEnableBalances(record.getProfitMoney().add(chosenAccount2.getEnableBalances()));
        chosenAccount2Record.setSign(SignUtil.getByLecturer(chosenAccount2Record.getTotalIncome(), chosenAccount2.getHistoryMoney(), chosenAccount2Record.getEnableBalances(), chosenAccount2.getFreezeBalances()));
        userAccountDao.updateByUserNo(chosenAccount2Record);
        updateRecord.setId(distributionParent2.getId());
        updateRecord.setEarnings(distributionParent2.getEarnings().add(record.getProfitMoney()));
        distributionDao.updateById(updateRecord);
        return profitMoney.add(record.getProfitMoney());
    }

    /**
     * 试卷订单完成处理流程
     *
     * @param orderInfo 订单信息
     * @return 处理结果
     */
    private Boolean examOrderCompleteHandle(OrderInfo orderInfo) {
        // 根据课程No查找试卷信息
        ExamInfoVO examInfoVO = feignExamInfo.getById(orderInfo.getProductId());
        if (StringUtils.isEmpty(examInfoVO)) {
            logger.debug("根据订单的课程编号,没找到对应的试卷信息!");
            return false;
        }

        // 根据讲师编号和机构编号查找讲师账户信息
        UserAccount lecturerAccount = userAccountDao.getByUserNoAndCheck(examInfoVO.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerAccount)) {
            logger.debug("讲师账户不可用!");
            return false;
        }

        Lecturer lecturer = lecturerDao.getByLecturerUserNo(examInfoVO.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer)) {
            logger.debug("找不到讲师信息");
            return false;
        }
        UserOrderExamRefSaveQO userOrderExamRefSaveQO = new UserOrderExamRefSaveQO();
        userOrderExamRefSaveQO.setLecturerUserNo(orderInfo.getLecturerUserNo());
        userOrderExamRefSaveQO.setUserNo(orderInfo.getUserNo());
        userOrderExamRefSaveQO.setOrderNo(orderInfo.getOrderNo());
        userOrderExamRefSaveQO.setExamId(orderInfo.getProductId());

        // 如果用户购买过，不作处理，需要人工进行退款处理
        UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
        userOrderExamRefQO.setUserNo(orderInfo.getUserNo());
        userOrderExamRefQO.setExamId(orderInfo.getProductId());
        UserOrderExamRefVO userOrderExamRefVO = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
        if (ObjectUtil.isNull(userOrderExamRefVO)) {
            feignUserOrderExamRef.save(userOrderExamRefSaveQO);

            // 更新课程信息的购买人数
            examInfoVO.setCountBuy(examInfoVO.getCountBuy() + 1);
            ExamInfoQO examInfoQO = BeanUtil.copyProperties(examInfoVO, ExamInfoQO.class);
            feignExamInfo.updateById(examInfoQO);

            // 计算结算金额
            Map<String, BigDecimal> map = profit(orderInfo, orderInfo.getPricePaid());
            // 讲师收入 = 订单金额 * 讲师分成比例   保留2位小数，四舍五入到零
            BigDecimal lecturerProfit = NumberUtil.mul(map.get("pricePaid"), lecturer.getLecturerProportion()).setScale(2, RoundingMode.DOWN);
            // 平台收入 = 订单金额 - 讲师收入
            BigDecimal platformProfit = NumberUtil.sub(map.get("pricePaid"), lecturerProfit).setScale(2, RoundingMode.DOWN);
            orderInfo.setLecturerIncome(lecturerProfit);
            orderInfo.setPlatformIncome(platformProfit);
            orderInfo.setAgentIncome(map.get("distributionParent"));
            orderInfo.setLecturerIncome(lecturerProfit);

            // 更新讲师账户信息
            UserAccount lecturerAccountRecord = new UserAccount();
            lecturerAccountRecord.setUserNo(lecturerAccount.getUserNo());
            lecturerAccountRecord.setTotalIncome(orderInfo.getLecturerIncome().add(lecturerAccount.getEnableBalances()));
            lecturerAccountRecord.setEnableBalances(orderInfo.getLecturerIncome().add(lecturerAccount.getEnableBalances()));
            lecturerAccountRecord.setSign(SignUtil.getByLecturer(lecturerAccountRecord.getTotalIncome(), lecturerAccount.getHistoryMoney(), lecturerAccountRecord.getEnableBalances(), lecturerAccount.getFreezeBalances()));
            userAccountDao.updateByUserNo(lecturerAccountRecord);
        } else {
            // 重复购买，不添加数据，直接通知客服进行人工退款
            if (!orderInfo.getOrderNo().equals(userOrderExamRefVO.getOrderNo())) {
                logger.warn("用户：[{}]---订单号：[{}]--试卷：[{}]---已经重复购买，需要人工处理", orderInfo.getUserNo(), orderInfo.getOrderNo(), examInfoVO.getExamName());
            }
        }

        // 更新订单信息
        Date payTime = new Date();
        orderInfo.setPayTime(payTime);
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfoDao.updateById(orderInfo);
        // 更新订单支付信息
        OrderLogVO orderLogVO = new OrderLogVO();
        orderLogVO.setOrderNo(orderInfo.getOrderNo());
        orderLogVO.setPlatformIncome(orderInfo.getPlatformIncome());
        orderLogVO.setLecturerIncome(orderInfo.getLecturerIncome());
        orderLogVO.setPayTime(payTime);
        orderLogVO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        // 异步更新支付日志
        CALLBACK_EXECUTOR.execute(new orderLog(orderLogVO));
        return true;
    }

    /**
     * 异步保存订单日志
     */
    class orderLog implements Runnable {
        private OrderLogVO orderLogVO;

        public orderLog(OrderLogVO orderLogVO) {
            this.orderLogVO = orderLogVO;
        }

        @Override
        public void run() {
            try {
                feignOrderLog.updateByOrderNo(BeanUtil.copyProperties(orderLogVO, OrderLogQO.class));
            } catch (Exception e) {
                logger.error("异步更新订单日志", e);
            }
        }
    }
}
