package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.user.common.req.UserEditREQ;
import com.roncoo.education.user.common.req.UserListREQ;
import com.roncoo.education.user.common.req.UserSaveREQ;
import com.roncoo.education.user.common.resp.UserListRESP;
import com.roncoo.education.user.common.resp.UserViewRESP;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.User;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户基本信息
 *
 * @author wujing
 */
@Component
public class PcUserBiz extends BaseBiz {

    @Autowired
    private UserDao dao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 用户基本信息列表
     *
     * @param userListREQ 用户基本信息分页查询参数
     * @return 用户基本信息分页查询结果
     */
    public Result<Page<UserListRESP>> list(UserListREQ userListREQ) {
        UserExample example = new UserExample();
        Criteria c = example.createCriteria();
        Page<User> page = dao.listForPage(userListREQ.getPageCurrent(), userListREQ.getPageSize(), example);
        Page<UserListRESP> respPage = PageUtil.transform(page, UserListRESP.class);
        return Result.success(respPage);
    }

    /**
     * 用户基本信息添加
     *
     * @param userSaveREQ 用户基本信息
     * @return 添加结果
     */
    public Result<String> save(UserSaveREQ userSaveREQ) {
        User record = BeanUtil.copyProperties(userSaveREQ, User.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 用户基本信息查看
     *
     * @param id 主键ID
     * @return 用户基本信息
     */
    public Result<UserViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserViewRESP.class));
    }

    /**
     * 用户基本信息修改
     *
     * @param userEditREQ 用户基本信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserEditREQ userEditREQ) {
        User record = BeanUtil.copyProperties(userEditREQ, User.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户基本信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 解绑小程序
     *
     * @param userNo
     * @return
     */
    public Result<String> unbind(Long userNo) {
        User record = dao.getByUserNo(userNo);
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("找不到用户信息");
        }
        record.setUniconId("");
        // 解除绑定，清空该用户token
        if (myRedisTemplate.hasKey(Constants.Platform.XCX.concat(record.getUserNo().toString()))) {
            myRedisTemplate.delete(Constants.Platform.XCX.concat(record.getUserNo().toString()));
        }
        if (dao.updateById(record) > 0) {
            return Result.success("解绑成功");
        }
        return Result.error("解绑失败");
    }
}
