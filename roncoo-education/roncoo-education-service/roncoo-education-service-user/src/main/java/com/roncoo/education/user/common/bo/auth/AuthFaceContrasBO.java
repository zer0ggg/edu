package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 人脸对比信息
 */
@Data
@Accessors(chain = true)
public class AuthFaceContrasBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 录制视频的base64编码
     */
    @ApiModelProperty(value = "录制视频的base64编码", required = true)
    private String videoBase64;
    /**
     * 第一次认证获取的图片的base64编码
     */
    @ApiModelProperty(value = "第一次认证获取的图片的base64编码")
    private String imageBase64;
    /**
     * 课时id
     */
    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;
}
