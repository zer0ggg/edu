package com.roncoo.education.user.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 直播频道表
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value="LiveChannelDTO", description="直播频道表")
public class LiveChannelDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "频道id")
    private String channelId;

    @ApiModelProperty(value = "频道密码")
    private String channelPasswd;

    @ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏)")
    private String scene;

    @ApiModelProperty(value = "直播状态(1:未开播;2:直播中;3:待生成回放;4:待转存;5:直播结束)")
    private Integer liveStatus;

    @ApiModelProperty(value = "是否被使用(0:未使用,1:已使用)")
    private Integer isUse;
}
