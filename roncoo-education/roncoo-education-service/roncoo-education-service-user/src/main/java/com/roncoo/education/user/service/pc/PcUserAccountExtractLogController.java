package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.UserAccountExtractLogEditREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogExprotREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogListREQ;
import com.roncoo.education.user.common.req.UserAccountExtractLogSaveREQ;
import com.roncoo.education.user.common.resp.UserAccountExtractLogListRESP;
import com.roncoo.education.user.common.resp.UserAccountExtractLogViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserAccountExtractLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户账户提现日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/account/extract/log")
@Api(value = "user-用户账户提现日志", tags = {"user-用户账户提现日志"})
public class PcUserAccountExtractLogController {

    @Autowired
    private PcUserAccountExtractLogBiz biz;

    @ApiOperation(value = "用户账户提现日志列表", notes = "用户账户提现日志列表")
    @PostMapping(value = "/list")
    public Result<Page<UserAccountExtractLogListRESP>> list(@RequestBody UserAccountExtractLogListREQ userAccountExtractLogListREQ) {
        return biz.list(userAccountExtractLogListREQ);
    }

    @ApiOperation(value = "用户账户提现日志添加", notes = "用户账户提现日志添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserAccountExtractLogSaveREQ userAccountExtractLogSaveREQ) {
        return biz.save(userAccountExtractLogSaveREQ);
    }

    @ApiOperation(value = "用户账户提现日志查看", notes = "用户账户提现日志查看")
    @GetMapping(value = "/view")
    public Result<UserAccountExtractLogViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户账户提现日志修改", notes = "用户账户提现日志修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserAccountExtractLogEditREQ userAccountExtractLogEditREQ) {
        return biz.edit(userAccountExtractLogEditREQ);
    }

    @ApiOperation(value = "用户账户提现日志删除", notes = "用户账户提现日志删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "用户账户提现日志批量打款", notes = "用户账户提现日志删除")
    @PostMapping(value = "/batch/audit")
    @SysLog(value = "用户账户提现日志批量打款")
    public Result<String> batchAudit(@RequestParam String ids) {
        return biz.batchAudit(ids);
    }

    @ApiOperation(value = "报表导出", notes = "报表导出")
    @RequestMapping(value = "/export", method = RequestMethod.GET)
    @SysLog(value = "用户账户提现报表导出")
    public void export(HttpServletResponse response, UserAccountExtractLogExprotREQ req) throws IOException {
        biz.export(response, req);
    }
}
