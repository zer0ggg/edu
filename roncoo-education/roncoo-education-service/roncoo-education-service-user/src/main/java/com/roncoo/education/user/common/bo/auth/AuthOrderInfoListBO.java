package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 订单信息分页列表
 *
 * @author YZJ
 */
@Data
@Accessors(chain = true)
public class AuthOrderInfoListBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单状态(1待支付，2成功支付，3支付失败，4已关闭)")
    private Integer orderStatus;

    @ApiModelProperty(value = "讲师编号（讲师收益用到）", required = true)
    private Long lecturerUserNo;

    @ApiModelProperty(value = "产品类型(1:点播，2:直播，3:会员，4:文库，5:试卷)")
    private Integer productType;

    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize = 20;
}
