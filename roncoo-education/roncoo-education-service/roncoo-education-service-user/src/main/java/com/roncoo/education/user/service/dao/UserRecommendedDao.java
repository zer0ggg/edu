package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;

/**
 * 用户推荐信息 服务类
 *
 * @author wujing
 * @date 2020-07-15
 */
public interface UserRecommendedDao {

    /**
    * 保存用户推荐信息
    *
    * @param record 用户推荐信息
    * @return 影响记录数
    */
    int save(UserRecommended record);

    /**
    * 根据ID删除用户推荐信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户推荐信息
    * @return 影响记录数
    */
    int updateById(UserRecommended record);

    /**
    * 根据ID获取用户推荐信息
    *
    * @param id 主键ID
    * @return 用户推荐信息
    */
    UserRecommended getById(Long id);

    /**
    * 用户推荐信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<UserRecommended> listForPage(int pageCurrent, int pageSize, UserRecommendedExample example);

}
