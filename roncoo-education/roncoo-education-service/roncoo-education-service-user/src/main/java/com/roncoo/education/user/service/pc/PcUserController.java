package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserEditREQ;
import com.roncoo.education.user.common.req.UserListREQ;
import com.roncoo.education.user.common.req.UserSaveREQ;
import com.roncoo.education.user.common.resp.UserListRESP;
import com.roncoo.education.user.common.resp.UserViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户基本信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user")
@Api(value = "user-用户基本信息", tags = {"user-用户基本信息"})
public class PcUserController {

    @Autowired
    private PcUserBiz biz;

    @ApiOperation(value = "用户基本信息列表", notes = "用户基本信息列表")
    @PostMapping(value = "/list")
    public Result<Page<UserListRESP>> list(@RequestBody UserListREQ userListREQ) {
        return biz.list(userListREQ);
    }

    @ApiOperation(value = "用户基本信息添加", notes = "用户基本信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserSaveREQ userSaveREQ) {
        return biz.save(userSaveREQ);
    }

    @ApiOperation(value = "用户基本信息查看", notes = "用户基本信息查看")
    @GetMapping(value = "/view")
    public Result<UserViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户基本信息修改", notes = "用户基本信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserEditREQ userEditREQ) {
        return biz.edit(userEditREQ);
    }

    @ApiOperation(value = "用户基本信息删除", notes = "用户基本信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
