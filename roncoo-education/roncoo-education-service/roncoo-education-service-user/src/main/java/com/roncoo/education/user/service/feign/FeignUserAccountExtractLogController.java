package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserAccountExtractLog;
import com.roncoo.education.user.feign.qo.UserAccountExtractLogQO;
import com.roncoo.education.user.feign.vo.UserAccountExtractLogVO;
import com.roncoo.education.user.service.feign.biz.FeignUserAccountExtractLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@RestController
public class FeignUserAccountExtractLogController extends BaseController implements IFeignUserAccountExtractLog {

	@Autowired
	private FeignUserAccountExtractLogBiz biz;

	@Override
	public Page<UserAccountExtractLogVO> listForPage(@RequestBody UserAccountExtractLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserAccountExtractLogQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@RequestBody Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserAccountExtractLogQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public UserAccountExtractLogVO getById(@RequestBody Long id) {
		return biz.getById(id);
	}

}
