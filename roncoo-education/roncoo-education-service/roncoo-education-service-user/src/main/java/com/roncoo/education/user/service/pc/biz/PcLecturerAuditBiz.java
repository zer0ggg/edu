package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.AuditStatusEnum;
import com.roncoo.education.common.core.enums.UserTypeEnum;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.interfaces.IFeignSysUser;
import com.roncoo.education.system.feign.qo.SysUserQO;
import com.roncoo.education.system.feign.vo.ConfigSysVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.common.req.LecturerAuditEditREQ;
import com.roncoo.education.user.common.req.LecturerAuditListREQ;
import com.roncoo.education.user.common.req.LecturerAuditREQ;
import com.roncoo.education.user.common.req.LecturerAuditSaveREQ;
import com.roncoo.education.user.common.resp.LecturerAuditListRESP;
import com.roncoo.education.user.common.resp.LecturerAuditViewRESP;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAuditExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@Component
public class PcLecturerAuditBiz extends BaseBiz {

    @Autowired
    private LecturerAuditDao dao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignSysUser feignSysUser;

    /**
     * 讲师信息-审核列表
     *
     * @param lecturerAuditListREQ 讲师信息-审核分页查询参数
     * @return 讲师信息-审核分页查询结果
     */
    public Result<Page<LecturerAuditListRESP>> list(LecturerAuditListREQ lecturerAuditListREQ) {
        LecturerAuditExample example = new LecturerAuditExample();
        Criteria c = example.createCriteria();
        // 讲师名称
        if (ObjectUtil.isNotEmpty(lecturerAuditListREQ.getLecturerName())) {
            c.andLecturerNameLike(SqlUtil.like(lecturerAuditListREQ.getLecturerName()));
        }
        // 讲师手机
        if (ObjectUtil.isNotEmpty(lecturerAuditListREQ.getLecturerMobile())) {
            c.andLecturerMobileLike(SqlUtil.like(lecturerAuditListREQ.getLecturerMobile()));
        }
        // 审核状态
        if (ObjectUtil.isNotEmpty(lecturerAuditListREQ.getAuditStatus())) {
            c.andAuditStatusEqualTo(lecturerAuditListREQ.getAuditStatus());
        }
        Page<LecturerAudit> page = dao.listForPage(lecturerAuditListREQ.getPageCurrent(), lecturerAuditListREQ.getPageSize(), example);
        Page<LecturerAuditListRESP> respPage = PageUtil.transform(page, LecturerAuditListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 讲师信息-审核添加
     *
     * @param lecturerAuditSaveREQ 讲师信息-审核
     * @return 添加结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(LecturerAuditSaveREQ lecturerAuditSaveREQ) {
        // 手机号去空处理
        String mobile = lecturerAuditSaveREQ.getLecturerMobile().trim();
        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(mobile).matches()) {
            return Result.error("手机号码格式不正确");
        }
        ConfigSysVO sys = feignSysConfig.getSys();
        if (ObjectUtil.isNull(sys)) {
            return Result.error("找不到系统配置信息");
        }

        // 根据传入手机号获取用户信息(讲师的用户信息)
        UserExt userExt = userExtDao.getByMobile(mobile);
        // 1、用户不存在，注册用户
        if (ObjectUtil.isNull(userExt)) {
            if (StringUtils.isEmpty(lecturerAuditSaveREQ.getMobilePsw())) {
                return Result.error("请输入密码");
            }
            if (!lecturerAuditSaveREQ.getConfirmPasswd().equals(lecturerAuditSaveREQ.getMobilePsw())) {
                return Result.error("两次密码不一致，请重试！");
            }
            // 注册用户
            userExt = register(lecturerAuditSaveREQ, sys, mobile);

            lecturerInfo(lecturerAuditSaveREQ, userExt, sys);

            // 返回成功
            return Result.success("添加成功");
        }
        userExt.setUserType(UserTypeEnum.LECTURER.getCode());
        userExtDao.updateById(userExt);

        // 2、讲师不存在，添加讲师
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(userExt.getUserNo());
        if (ObjectUtil.isNotNull(lecturer)) {
            throw new BaseException("该账号已申请入驻成为讲师，请更换账号重试");
        }
        lecturerInfo(lecturerAuditSaveREQ, userExt, sys);
        // 返回成功
        SysUserQO qo = new SysUserQO();
        qo.setNickname(lecturerAuditSaveREQ.getLecturerName());
        qo.setLoginName(lecturerAuditSaveREQ.getLecturerMobile());
        if (org.springframework.util.StringUtils.isEmpty(lecturerAuditSaveREQ.getMobilePsw())) {
            // 如果为空设置初始密码
            qo.setLoginPassword("123456");
        } else {
            qo.setLoginPassword(lecturerAuditSaveREQ.getMobilePsw());
        }
        feignSysUser.save(qo);
        return Result.success("添加成功");
    }


    /**
     * 添加讲师信息
     *
     * @author wuyun
     */
    private void lecturerInfo(LecturerAuditSaveREQ auditBO, UserExt userExt, ConfigSysVO sys) {
        // 插入讲师信息
        LecturerAudit infoAudit = BeanUtil.copyProperties(auditBO, LecturerAudit.class);
        if (!StringUtils.isEmpty(userExt.getHeadImgUrl())) {
            infoAudit.setHeadImgUrl(userExt.getHeadImgUrl());
        }
        infoAudit.setAuditStatus(AuditStatusEnum.SUCCESS.getCode());
        infoAudit.setLecturerUserNo(userExt.getUserNo());
        infoAudit.setLecturerProportion(sys.getAgentDefaultProportion());// 设置讲师默认分成百分之70
        infoAudit.setId(IdWorker.getId());
        int infoAuditNum = dao.save(infoAudit);
        if (infoAuditNum < 1) {
            throw new BaseException("讲师信息表新增失败");
        }
        Lecturer lecturer = BeanUtil.copyProperties(infoAudit, Lecturer.class);
        lecturerDao.save(lecturer);

        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(userExt.getUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            userAccount = new UserAccount();
            userAccount.setUserNo(userExt.getUserNo());
            userAccount.setTotalIncome(BigDecimal.valueOf(0));
            userAccount.setHistoryMoney(BigDecimal.valueOf(0));
            userAccount.setEnableBalances(BigDecimal.valueOf(0));
            userAccount.setFreezeBalances(BigDecimal.valueOf(0));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            infoAuditNum = userAccountDao.save(userAccount);
            if (infoAuditNum < 1) {
                throw new BaseException("讲师账号信息表新增失败");
            }
        }
    }

    /**
     * 添加用户信息
     *
     * @author wuyun
     */
    private UserExt register(LecturerAuditSaveREQ lecturerAudit, ConfigSysVO sys, String mobile) {
        // 用户基本信息
        User user = new User();
        user.setUserNo(NOUtil.getUserNo());
        user.setMobile(mobile);
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + lecturerAudit.getMobilePsw()));
        userDao.save(user);

        // 用户教育信息
        UserExt userExt = new UserExt();
        userExt.setUserNo(user.getUserNo());
        userExt.setMobile(user.getMobile());
        userExt.setNickname(lecturerAudit.getLecturerName());
        userExt.setUserType(UserTypeEnum.LECTURER.getCode());
        userExt.setReferralCode(referralCode(ReferralCodeUtil.getStringRandom()));// 推荐码校验
        // 用户默认头像
        SysConfigVO SysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        if (org.springframework.util.StringUtils.isEmpty(SysVO.getConfigValue())) {
            throw new BaseException("未设置平台域名");
        }
        userExt.setHeadImgUrl(SysVO.getConfigValue() + "/friend.jpg");
        userExtDao.save(userExt);

        UserAccount userAccount = new UserAccount();
        userAccount.setUserNo(user.getUserNo());
        userAccount.setTotalIncome(BigDecimal.valueOf(0));
        userAccount.setHistoryMoney(BigDecimal.valueOf(0));
        userAccount.setEnableBalances(BigDecimal.valueOf(0));
        userAccount.setFreezeBalances(BigDecimal.valueOf(0));
        userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
        userAccountDao.save(userAccount);
        return userExt;
    }

    /**
     * 推荐码校验唯一性
     */
    private String referralCode(String referralCode) {
        // 查询生成的推荐码在数据库是否存在，不存在就返回，存在重新生成再次判断
        UserExt userExt = userExtDao.getByReferralCode(referralCode);
        if (ObjectUtil.isNull(userExt)) {
            return referralCode;
        }
        return referralCode(ReferralCodeUtil.getStringRandom());
    }


    /**
     * 讲师信息-审核查看
     *
     * @param id 主键ID
     * @return 讲师信息-审核
     */
    public Result<LecturerAuditViewRESP> view(Long id) {
        LecturerAuditViewRESP lecturerAuditViewRESP = BeanUtil.copyProperties(dao.getById(id), LecturerAuditViewRESP.class);
        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(lecturerAuditViewRESP.getLecturerUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            userAccount = new UserAccount();
            userAccount.setUserNo(lecturerAuditViewRESP.getLecturerUserNo());
            userAccount.setTotalIncome(BigDecimal.ZERO);
            userAccount.setHistoryMoney(BigDecimal.ZERO);
            userAccount.setEnableBalances(BigDecimal.ZERO);
            userAccount.setFreezeBalances(BigDecimal.ZERO);
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.save(userAccount);
        }
        lecturerAuditViewRESP.setUserAccountVO(BeanUtil.copyProperties(userAccount, UserAccountVO.class));
        return Result.success(lecturerAuditViewRESP);
    }


    /**
     * 讲师信息-审核修改
     *
     * @param lecturerAuditEditREQ 讲师信息-审核修改对象
     * @return 修改结果
     */
    public Result<String> edit(LecturerAuditEditREQ lecturerAuditEditREQ) {
        LecturerAudit record = BeanUtil.copyProperties(lecturerAuditEditREQ, LecturerAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 讲师信息-审核删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 讲师审核
     *
     * @param lecturerAuditREQ 审核入参
     * @return 审核结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> audit(LecturerAuditREQ lecturerAuditREQ) {
        LecturerAudit lecturerAudit = dao.getById(lecturerAuditREQ.getId());

        // 查找用户信息是否存在
        UserExt userExt = userExtDao.getByUserNo(lecturerAudit.getLecturerUserNo());
        if (ObjectUtil.isNull(userExt)) {
            throw new BaseException("获取不到用户信息");
        }
        if (AuditStatusEnum.SUCCESS.getCode().equals(lecturerAuditREQ.getAuditStatus())) {
            // 审核通过，更新为讲师
            userExt.setUserType(UserTypeEnum.LECTURER.getCode());
            userExtDao.updateById(userExt);

            Lecturer record = BeanUtil.copyProperties(lecturerAudit, Lecturer.class);
            record.setGmtCreate(null);
            record.setGmtModified(null);
            // 查找讲师信息表，是否存在该讲师
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(lecturerAudit.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturer)) {
                // 插入
                lecturerDao.save(record);
            } else {
                // 更新
                lecturerDao.updateById(record);
            }

            SysUserQO qo = new SysUserQO();
            qo.setNickname(record.getLecturerName());
            qo.setLoginName(record.getLecturerMobile());
            // 如果为空设置初始密码
            qo.setLoginPassword("123456");

            feignSysUser.save(qo);
        }
        // 审核不通过
        if (AuditStatusEnum.FAIL.getCode().equals(lecturerAuditREQ.getAuditStatus())) {
            //申请讲师操作 ，变为用户，可再次申请；修改操作，不改变用户类型。
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(lecturerAudit.getLecturerUserNo());
            if (lecturer == null) {
                userExt.setUserType(UserTypeEnum.USER.getCode());
                userExtDao.updateById(userExt);
            }
            //  直播状态不一致，同步实体表的直播功能到审核表（恢复直播状态）
            if (lecturer != null && !lecturer.getIsLive().equals(lecturerAudit.getIsLive())) {
                lecturerAuditREQ.setIsLive(lecturer.getIsLive());
            }
        }
        LecturerAudit record = BeanUtil.copyProperties(lecturerAuditREQ, LecturerAudit.class);
        if (dao.updateById(record) < 0) {
            throw new BaseException("讲师审核更新失败");
        }
        //审核已完成
        return Result.success("审核完成");
    }

    /**
     * 手机号码校验用户信息表是否存在
     *
     * @param lecturerMobile 手机号
     * @return 判断
     * @author wuyun
     */
    public Result<Boolean> checkUserAndLecturer(String lecturerMobile) {
        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(lecturerMobile).matches()) {
            return Result.error("手机号码格式不正确");
        }
        // 根据传入手机号获取用户信息(讲师的用户信息)
        UserExt userExt = userExtDao.getByMobile(lecturerMobile);
        if (ObjectUtil.isNull(userExt)) {
            // 用户不存在
            return Result.success(true);
        } else {
            // 根据用户编号获取讲师信息
            LecturerAudit record = dao.getByLecturerUserNo(userExt.getUserNo());
            if (ObjectUtil.isNull(record)) {
                return Result.success(false);
            }
            return Result.error("讲师手机号码已存在");
        }
    }

}
