package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.MsgTemplateEditREQ;
import com.roncoo.education.user.common.req.MsgTemplateListREQ;
import com.roncoo.education.user.common.req.MsgTemplateSaveREQ;
import com.roncoo.education.user.common.resp.MsgTemplateListRESP;
import com.roncoo.education.user.common.resp.MsgTemplateViewRESP;
import com.roncoo.education.user.service.pc.biz.PcMsgTemplateBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 消息模板 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/msg/template")
@Api(value = "user-消息模板", tags = {"user-消息模板"})
public class PcMsgTemplateController {

    @Autowired
    private PcMsgTemplateBiz biz;

    @ApiOperation(value = "消息模板列表", notes = "消息模板列表")
    @PostMapping(value = "/list")
    public Result<Page<MsgTemplateListRESP>> list(@RequestBody MsgTemplateListREQ msgTemplateListREQ) {
        return biz.list(msgTemplateListREQ);
    }

    @ApiOperation(value = "消息模板添加", notes = "消息模板添加")
    @PostMapping(value = "/save")
    @SysLog(value = "消息模板添加")
    public Result<String> save(@RequestBody MsgTemplateSaveREQ msgTemplateSaveREQ) {
        return biz.save(msgTemplateSaveREQ);
    }

    @ApiOperation(value = "消息模板查看", notes = "消息模板查看")
    @GetMapping(value = "/view")
    public Result<MsgTemplateViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "消息模板修改", notes = "消息模板修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "消息模板修改")
    public Result<String> edit(@RequestBody MsgTemplateEditREQ msgTemplateEditREQ) {
        return biz.edit(msgTemplateEditREQ);
    }

    @ApiOperation(value = "消息模板删除", notes = "消息模板删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "消息模板删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
