package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 频道状态修改
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ChannelUpdateStatusIdREQ", description = "频道状态修改")
public class ChannelUpdateStatusIdREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)", required = true)
    private Integer statusId;
}
