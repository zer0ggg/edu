/**
 * Copyright 2015-2016 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserXcxBindingBO implements Serializable {

	private String uniconId;

	private String username;

	private String password;

	private Long timestamp;

	private String sign;

	private static final long serialVersionUID = 1L;

}
