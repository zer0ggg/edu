package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.*;
import com.roncoo.education.user.common.resp.OrderInfoCallbackUserRESP;
import com.roncoo.education.user.common.resp.OrderInfoListRESP;
import com.roncoo.education.user.common.resp.OrderInfoViewRESP;
import com.roncoo.education.user.service.pc.biz.PcOrderInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 订单信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/order/info")
@Api(value = "user-订单信息", tags = {"user-订单信息"})
public class PcOrderInfoController {

    private static final Logger logger = LoggerFactory.getLogger(PcOrderInfoController.class);

    @Autowired
    private PcOrderInfoBiz biz;

    @ApiOperation(value = "订单信息列表", notes = "订单信息列表")
    @PostMapping(value = "/list")
    public Result<Page<OrderInfoListRESP>> list(@RequestBody OrderInfoListREQ orderInfoListREQ) {
        return biz.list(orderInfoListREQ);
    }

    @ApiOperation(value = "导出订单信息", notes = "导出订单信息")
    @PostMapping(value = "/export")
    public void export(OrderInfoListREQ orderInfoListREQ) {
        biz.export(orderInfoListREQ);
    }


    @ApiOperation(value = "订单信息添加", notes = "订单信息添加")
    @PostMapping(value = "/save")
    @SysLog(value = "订单信息添加")
    public Result<String> save(@RequestBody OrderInfoSaveREQ orderInfoSaveREQ) {
        return biz.save(orderInfoSaveREQ);
    }

    @ApiOperation(value = "订单信息查看", notes = "订单信息查看")
    @GetMapping(value = "/view")
    public Result<OrderInfoViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "订单信息修改", notes = "订单信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "订单信息修改")
    public Result<String> edit(@RequestBody OrderInfoEditREQ orderInfoEditREQ) {
        return biz.edit(orderInfoEditREQ);
    }

    @ApiOperation(value = "订单信息删除", notes = "订单信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "用户查找带回", notes = "用户查找带回")
    @PostMapping(value = "/user/page")
    public Result<Page<OrderInfoCallbackUserRESP>> userPage(@RequestBody OrderInfoCallbackUserREQ orderInfoCallbackUserREQ) {
        return biz.userPage(orderInfoCallbackUserREQ);
    }

    @ApiOperation(value = "手工录单", notes = "手工录单")
    @PostMapping(value = "/manual")
    @SysLog(value = "手工录单")
    public Result<String> manualOrder(@RequestBody OrderInfoManualREQ orderInfoManualREQ) {
        return biz.manualOrder(orderInfoManualREQ);
    }

    @ApiOperation(value = "查单", notes = "查单")
    @GetMapping(value = "/query")
    public Result<String> orderQuery(@RequestParam Long orderNo) {
        return biz.orderQuery(orderNo);
    }

    @ApiOperation(value = "订单改价", notes = "订单改价")
    @PutMapping(value = "/update/price")
    @SysLog(value = "订单改价")
    public Result<String> updatePrice(@RequestBody @Valid OrderInfoUpdatePriceREQ orderInfoUpdatePriceREQ) {
        return biz.updatePrice(orderInfoUpdatePriceREQ);
    }

    @ApiOperation(value = "订单解绑", notes = "订单解绑")
    @PostMapping(value = "/unbind")
    @SysLog(value = "订单解绑")
    public Result<String> orderUnbind(@RequestParam Long id) {
        return biz.orderUnbind(id);
    }
}
