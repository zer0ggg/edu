package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplate;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplateExample;

import java.util.List;

public interface MsgTemplateDao {
    int save(MsgTemplate record);

    int deleteById(Long id);

    int updateById(MsgTemplate record);

    MsgTemplate getById(Long id);

    Page<MsgTemplate> listForPage(int pageCurrent, int pageSize, MsgTemplateExample example);

    /**
     * 根据消息模板类型获取模板信息
     *
     * @param msgTemplateType
     * @return
     */
    List<MsgTemplate> listByMsgTemplateType(Integer msgTemplateType);

    /**
     * 根据模板ID获取模板信息
     *
     * @param templateCode
     * @return
     */
    MsgTemplate getByTemplateCode(String templateCode);

    /**
     * 根据消息模板类型、是否使用模板获取模板信息
     *
     * @param msgTemplateType
     * @param isUseTemplate
     * @return
     */
    MsgTemplate getByMsgTemplateTypeAndIsUseTemplate(Integer msgTemplateType, Integer isUseTemplate);
}
