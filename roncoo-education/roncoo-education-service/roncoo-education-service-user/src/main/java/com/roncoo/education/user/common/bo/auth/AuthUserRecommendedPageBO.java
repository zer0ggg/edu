package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户推荐信息
 * </p>
 *
 * @author wujing
 * @date 2020-04-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserRecommendedBO", description = "用户推荐信息")
public class AuthUserRecommendedPageBO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private int pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数", required = true)
    private int pageSize = 20;
}
