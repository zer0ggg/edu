package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.UserResearchEditREQ;
import com.roncoo.education.user.common.req.UserResearchListREQ;
import com.roncoo.education.user.common.req.UserResearchSaveREQ;
import com.roncoo.education.user.common.resp.UserResearchListRESP;
import com.roncoo.education.user.common.resp.UserResearchViewRESP;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserResearchDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearch;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearchExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearchExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 用户调研信息
 *
 * @author wujing
 */
@Component
public class PcUserResearchBiz extends BaseBiz {

    @Autowired
    private UserResearchDao dao;

    @Autowired
    private UserExtDao userExtDao;


    /**
     * 用户调研信息列表
     *
     * @param req 用户调研信息分页查询参数
     * @return 用户调研信息分页查询结果
     */
    public Result<Page<UserResearchListRESP>> list(UserResearchListREQ req) {
        UserResearchExample example = new UserResearchExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getMobile())) {
            UserExt userExt = userExtDao.getByMobile(req.getMobile());
            if (ObjectUtil.isNotNull(userExt)) {
                c.andNameLike(req.getName());
            }
        }
        if (StringUtils.hasText(req.getName())) {
            c.andNameLike(req.getName());
        }
        Page<UserResearch> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<UserResearchListRESP> respPage = PageUtil.transform(page, UserResearchListRESP.class);
        for (UserResearchListRESP resp : respPage.getList()) {
            UserExt userExt = userExtDao.getByUserNo(resp.getUserNo());
            if (ObjectUtil.isNotNull(userExt)) {
                resp.setMobile(userExt.getMobile());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 用户调研信息添加
     *
     * @param userResearchSaveREQ 用户调研信息
     * @return 添加结果
     */
    public Result<String> save(UserResearchSaveREQ userResearchSaveREQ) {
        UserResearch record = BeanUtil.copyProperties(userResearchSaveREQ, UserResearch.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户调研信息查看
     *
     * @param id 主键ID
     * @return 用户调研信息
     */
    public Result<UserResearchViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserResearchViewRESP.class));
    }


    /**
     * 用户调研信息修改
     *
     * @param userResearchEditREQ 用户调研信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserResearchEditREQ userResearchEditREQ) {
        UserResearch record = BeanUtil.copyProperties(userResearchEditREQ, UserResearch.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户调研信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
