package com.roncoo.education.user.service.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignDistributionProfit;
import com.roncoo.education.user.feign.qo.DistributionProfitQO;
import com.roncoo.education.user.feign.vo.DistributionProfitVO;
import com.roncoo.education.user.service.feign.biz.FeignDistributionProfitBiz;

/**
 * 代理分润记录
 *
 * @author wujing
 * @date 2021-01-05
 */
@RestController
public class FeignDistributionProfitController extends BaseController implements IFeignDistributionProfit{

    @Autowired
    private FeignDistributionProfitBiz biz;

	@Override
	public Page<DistributionProfitVO> listForPage(@RequestBody DistributionProfitQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody DistributionProfitQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody DistributionProfitQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public DistributionProfitVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
