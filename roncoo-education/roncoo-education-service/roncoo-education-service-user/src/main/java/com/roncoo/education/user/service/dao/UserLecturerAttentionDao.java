package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;

public interface UserLecturerAttentionDao {
	int save(UserLecturerAttention record);

	int deleteById(Long id);

	int updateById(UserLecturerAttention record);

	int updateByExampleSelective(UserLecturerAttention record, UserLecturerAttentionExample example);

	UserLecturerAttention getById(Long id);

	Page<UserLecturerAttention> listForPage(int pageCurrent, int pageSize, UserLecturerAttentionExample example);

	/**
	 * 根据用户编号获取关注讲师信息
	 * 
	 * @param userNo
	 * @param lecturerUserNo
	 * @author kyh
	 */
	UserLecturerAttention getByUserAndLecturerUserNo(Long userNo, Long lecturerUserNo);
}