package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 讲师信息-审核
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "LecturerAuditSaveREQ", description = "讲师信息-审核添加")
public class LecturerAuditSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "讲师手机")
    private String lecturerMobile;

    @ApiModelProperty(value = "讲师邮箱")
    private String lecturerEmail;

    @ApiModelProperty(value = "添加代理时使用")
    private String referralCode;

    @ApiModelProperty(value = "登陆密码")
    private String mobilePsw;

    @ApiModelProperty(value = "确认密码")
    private String confirmPasswd;
}
