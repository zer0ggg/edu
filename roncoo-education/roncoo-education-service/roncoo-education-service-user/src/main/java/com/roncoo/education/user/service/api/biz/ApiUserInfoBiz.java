package com.roncoo.education.user.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.aliyuncs.exceptions.ClientException;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.community.feign.interfaces.IFeignBlogger;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.data.feign.interfaces.IFeignUserLog;
import com.roncoo.education.data.feign.qo.UserLogQO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliyunVO;
import com.roncoo.education.system.feign.vo.ConfigWeixinVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.common.bo.*;
import com.roncoo.education.user.common.bo.auth.UserUpdateBO;
import com.roncoo.education.user.common.dto.UserLoginDTO;
import com.roncoo.education.user.common.dto.UserXcxBindingDTO;
import com.roncoo.education.user.common.dto.UserXcxLoginDTO;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 用户基本信息
 *
 * @author wujing
 */
@Component
public class ApiUserInfoBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignUserLog feignUserLog;
    @Autowired
    private IFeignBlogger feignBlogger;

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserLogSmsDao userLogSmsDao;
    @Autowired
    private UserLogLoginDao userLogLoginDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private UserRecommendedDao userRecommendedDao;
    @Autowired
    private DistributionDao distributionDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 5分钟
     */
    private static final Long TIME = 5 * 60 * 1000L;

    @Transactional(rollbackFor = Exception.class)
    public Result<UserLoginDTO> register(UserRegisterBO userRegisterBO) {
        if (StringUtils.isEmpty(userRegisterBO.getMobile())) {
            return Result.error("手机号不能为空");
        }
        if (StringUtils.isEmpty(userRegisterBO.getPassword())) {
            return Result.error("密码不能为空");
        }

        // 密码校验
        if (!userRegisterBO.getPassword().equals(userRegisterBO.getRepassword())) {
            return Result.error("2次密码不一致");
        }

        // 验证码校验
        String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + userRegisterBO.getMobile());
        if (StringUtils.isEmpty(redisSmsCode)) {
            return Result.error("请输入验证码");
        }
        if (!redisSmsCode.equals(userRegisterBO.getCode())) {
            return Result.error("验证码不正确，请重新输入");
        }

        // 手机号重复校验
        User user = userDao.getByMobile(userRegisterBO.getMobile());
        if (ObjectUtil.isNotNull(user)) {
            return Result.error("该手机号已经注册，请更换手机号");
        }

        // 用户注册
        UserExt userExt = register(userRegisterBO.getMobile(), userRegisterBO.getPassword());

        if (StringUtils.hasText(userRegisterBO.getReferralCode())) {
            UserExt userExtReferralCode = userExtDao.getByReferralCode(userRegisterBO.getReferralCode());
            if (ObjectUtil.isNull(userExtReferralCode)) {
                return Result.error("推荐人不存在");
            }
            // 添加推荐人关联
            UserRecommended userRecommended = new UserRecommended();
            userRecommended.setUserNo(userExt.getUserNo());
            userRecommended.setReferralCode(userRegisterBO.getReferralCode());
            userRecommendedDao.save(userRecommended);

            Distribution distributionParent = distributionDao.getByUserNo(userExtReferralCode.getUserNo());
            if (ObjectUtil.isNotNull(distributionParent)) {
                Distribution distribution = new Distribution();
                distribution.setFloor(distributionParent.getFloor() + 1);
                distribution.setParentId(distributionParent.getParentId());
                distribution.setUserNo(userExt.getUserNo());
                distribution.setIsAllowInvitation(IsAllowInvitationEnum.NOT_ALLOW.getCode());
                distributionDao.save(distribution);
            }
        }
        UserLoginDTO dto = new UserLoginDTO();
        dto.setUserNo(userExt.getUserNo());
        dto.setMobile(userExt.getMobile());
        dto.setToken(JWTUtil.create(userExt.getUserNo(), JWTUtil.DATE, Constants.Platform.PC));
        return Result.success(dto);
    }

    public Result<UserLoginDTO> loginPassword(UserLoginPasswordBO userLoginPasswordBO) {
        if (StringUtils.isEmpty(userLoginPasswordBO.getMobile())) {
            return Result.error("手机号不能为空");
        }
        if (StringUtils.isEmpty(userLoginPasswordBO.getPassword())) {
            return Result.error("密码不能为空");
        }

        // 用户校验
        User user = userDao.getByMobile(userLoginPasswordBO.getMobile());
        if (null == user) {
            return Result.error("账号或者密码不正确");
        }

        // 获取当天该用户该状态的登录信息
        Integer errorCount = userLogLoginDao.listByUserNoAndLonginStatus(user.getUserNo(), LoginStatusEnum.FAIL.getCode());
        // 如果登录次数大于5次则返回错误
        if (errorCount >= 5) {
            return Result.error(ResultEnum.USER_LOGIN_ERROR.getCode(), "当天登录错误次数过多");
        }

        UserExt userExt = userExtDao.getByUserNo(user.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExt.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }

        // 密码校验
        if (!DigestUtil.sha1Hex(user.getMobileSalt() + userLoginPasswordBO.getPassword()).equals(user.getMobilePsw())) {
            CALLBACK_EXECUTOR.execute(new LoginLog(user.getUserNo(), LoginStatusEnum.FAIL, userLoginPasswordBO.getClientType(), userExt.getGmtCreate(), BeanUtil.copyProperties(userLoginPasswordBO, UserLogQO.class)));
            // 错误次数+1
            return Result.error("账号或者密码不正确");
        }
        // 记录用户成功登陆次数
        user.setLoginCount(user.getLoginCount() + 1);
        userDao.updateById(user);

        // 登录日志
        CALLBACK_EXECUTOR.execute(new LoginLog(user.getUserNo(), LoginStatusEnum.SUCCESS, userLoginPasswordBO.getClientType(), userExt.getGmtCreate(), BeanUtil.copyProperties(userLoginPasswordBO, UserLogQO.class)));

        UserLoginDTO dto = new UserLoginDTO();
        dto.setUserNo(user.getUserNo());
        dto.setMobile(user.getMobile());
        String client = Constants.Platform.PC;
        if (AlientTypeEnum.ANDROID.getCode().equals(userLoginPasswordBO.getClientType())) {
            client = Constants.Platform.ANDROID;
        } else if (AlientTypeEnum.IOS.getCode().equals(userLoginPasswordBO.getClientType())) {
            client = Constants.Platform.IOS;
        }
        dto.setToken(JWTUtil.create(user.getUserNo(), JWTUtil.DATE, client));
        // 登录成功，存入缓存(时间为一天)，单点登录使用
        myRedisTemplate.set(client.concat(dto.getUserNo().toString()), dto.getToken(), 1, TimeUnit.DAYS);
        return Result.success(dto);
    }

    public Result<UserLoginDTO> loginCode(UserLoginCodeBO userLoginCodeBO) {
        if (StringUtils.isEmpty(userLoginCodeBO.getMobile())) {
            return Result.error("手机号码不能为空");
        }

        User user = userDao.getByMobile(userLoginCodeBO.getMobile());
        if (null == user) {
            return Result.error("该用户不存在");
        }

        // 获取当天该用户该状态的登录信息
        Integer errorCount = userLogLoginDao.listByUserNoAndLonginStatus(user.getUserNo(), LoginStatusEnum.FAIL.getCode());
        // 如果登录次数大于5次则返回错误
        if (errorCount >= 5) {
            return Result.error(ResultEnum.USER_LOGIN_ERROR.getCode(), "当天登录错误次数过多");
        }

        UserExt userExt = userExtDao.getByUserNo(user.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExt.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }

        // 验证码校验
        String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + userLoginCodeBO.getMobile());
        if (StringUtils.isEmpty(redisSmsCode)) {
            return Result.error("验证码已经过期，请重新获取");
        }

        if (!redisSmsCode.equals(userLoginCodeBO.getCode())) {
            CALLBACK_EXECUTOR.execute(new LoginLog(user.getUserNo(), LoginStatusEnum.FAIL, userLoginCodeBO.getClientType(), userExt.getGmtCreate(), BeanUtil.copyProperties(userLoginCodeBO, UserLogQO.class)));
            // 缓存控制错误次数
            return Result.error("验证码不正确,重新输入");
        }

        // 清空缓存
        myRedisTemplate.delete(Constants.SMS_CODE + userLoginCodeBO.getMobile());

        // 登录日志
        CALLBACK_EXECUTOR.execute(new LoginLog(user.getUserNo(), LoginStatusEnum.SUCCESS, userLoginCodeBO.getClientType(), userExt.getGmtCreate(), BeanUtil.copyProperties(userLoginCodeBO, UserLogQO.class)));
        // 记录用户成功登陆次数
        user.setLoginCount(user.getLoginCount() + 1);
        userDao.updateById(user);

        UserLoginDTO dto = new UserLoginDTO();
        dto.setUserNo(user.getUserNo());
        dto.setMobile(user.getMobile());
        String client = Constants.Platform.PC;
        if (AlientTypeEnum.ANDROID.getCode().equals(userLoginCodeBO.getClientType())) {
            client = Constants.Platform.ANDROID;
        } else if (AlientTypeEnum.IOS.getCode().equals(userLoginCodeBO.getClientType())) {
            client = Constants.Platform.IOS;
        }

        dto.setToken(JWTUtil.create(user.getUserNo(), JWTUtil.DATE, client));
        // 登录成功，存入缓存(时间为一天)，单点登录使用
        myRedisTemplate.set(client.concat(dto.getUserNo().toString()), dto.getToken(), 1, TimeUnit.DAYS);
        return Result.success(dto);
    }

    public Result<String> sendCode(UserSendCodeBO userSendCodeBO) {
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(userSendCodeBO.getMobile()).matches()) {
            return Result.error("手机号码格式不正确");
        }

        ConfigAliyunVO sys = feignSysConfig.getAliyun();
        if (ObjectUtil.isNull(sys)) {
            return Result.error("找不到系统配置信息");
        }
        // 创建日志实例
        UserLogSms userLogSms = new UserLogSms();
        userLogSms.setMobile(userSendCodeBO.getMobile());
        userLogSms.setTemplate(sys.getAliyunSmsCode());
        // 随机生成验证码
        userLogSms.setSmsCode(RandomUtil.randomNumbers(6));
        try {
            // 发送验证码
            boolean result = AliyunUtil.sendMsg(userSendCodeBO.getMobile(), userLogSms.getSmsCode(), BeanUtil.copyProperties(sys, Aliyun.class));
            // 发送成功，验证码存入缓存：5分钟有效
            if (result) {
                myRedisTemplate.set(Constants.SMS_CODE + userSendCodeBO.getMobile(), userLogSms.getSmsCode(), 5, TimeUnit.MINUTES);
                userLogSms.setIsSuccess(IsSuccessEnum.SUCCESS.getCode());
                userLogSmsDao.save(userLogSms);
                return Result.success("发送成功");
            }
            // 发送失败
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            userLogSmsDao.save(userLogSms);
            throw new BaseException("发送失败");
        } catch (ClientException e) {
            userLogSms.setIsSuccess(IsSuccessEnum.FAIL.getCode());
            userLogSmsDao.save(userLogSms);
            logger.error("发送失败，原因={}", e.getErrMsg());
            return Result.error("发送失败");
        }
    }

    private UserExt register(String mobile, String password) {
        // 用户基本信息
        User user = new User();
        user.setUserNo(NOUtil.getUserNo());
        user.setMobile(mobile);
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + password));
        userDao.save(user);

        UserAccount userAccount = new UserAccount();
        userAccount.setUserNo(user.getUserNo());
        userAccount.setTotalIncome(BigDecimal.valueOf(0));
        userAccount.setHistoryMoney(BigDecimal.valueOf(0));
        userAccount.setEnableBalances(BigDecimal.valueOf(0));
        userAccount.setFreezeBalances(BigDecimal.valueOf(0));
        userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
        userAccountDao.save(userAccount);

        // 用户其他信息
        UserExt userExt = new UserExt();
        userExt.setUserNo(user.getUserNo());
        userExt.setUserType(UserTypeEnum.USER.getCode());
        userExt.setMobile(user.getMobile());
        // 用户默认头像
        SysConfigVO SysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        if (StringUtils.isEmpty(SysVO.getConfigValue())) {
            throw new BaseException("未设置平台域名");
        }
        userExt.setHeadImgUrl(SysVO.getConfigValue() + "/friend.jpg");
        userExtDao.save(userExt);

        // 添加博主信息
        BloggerQO bloggerQO = new BloggerQO();
        bloggerQO.setUserNo(user.getUserNo());
        bloggerQO.setMobile(user.getMobile());
        feignBlogger.save(bloggerQO);

        return userExt;
    }

    public Result<Integer> updatePassword(UserUpdateBO userUpdateBO) {
        if (StringUtils.isEmpty(userUpdateBO.getMobile())) {
            return Result.error("手机号为空,请重试");
        }
        if (StringUtils.isEmpty(userUpdateBO.getCode())) {
            return Result.error("验证码不能为空");
        }

        String redisCode = myRedisTemplate.get(Constants.SMS_CODE + userUpdateBO.getMobile());
        if (StringUtils.isEmpty(redisCode)) {
            return Result.error("请输入验证码");
        }
        if (!userUpdateBO.getCode().equals(redisCode)) {
            return Result.error("验证码匹配不正确");
        }
        // 手机号去空处理
        String mobile = userUpdateBO.getMobile().replaceAll(" +", "");

        if (StringUtils.isEmpty(userUpdateBO.getConfirmPassword())) {
            return Result.error("新登录密码为空,请重试");
        }
        if (!userUpdateBO.getNewPassword().equals(userUpdateBO.getConfirmPassword())) {
            return Result.error("密码输入不一致，请重试");
        }

        User user = userDao.getByMobile(mobile);
        if (ObjectUtil.isNull(user)) {
            return Result.error("没找到用户信息,请重试");
        }
        if (DigestUtil.sha1Hex(user.getMobileSalt() + userUpdateBO.getNewPassword()).equals(user.getMobilePsw())) {
            return Result.error("输入的密码与原密码一致,请重试");
        }

        // 更新密码
        User bean = new User();
        bean.setId(user.getId());
        bean.setMobileSalt(StrUtil.get32UUID());
        bean.setMobilePsw(DigestUtil.sha1Hex(bean.getMobileSalt() + userUpdateBO.getNewPassword()));
        int result = userDao.updateById(bean);
        return result == 1 ? Result.success(result) : Result.error(ResultEnum.USER_UPDATE_FAIL.getDesc());
    }

    public Result<UserXcxBindingDTO> xcxBinding(UserXcxBindingBO bo) {
        // 校验时间
        if (System.currentTimeMillis() - bo.getTimestamp() > TIME) {
            return Result.error("校验失败，timestamp不正确或者过期");
        }
        // 校验签名
        if (!checkSign(ObjectMapUtil.Obj2Map(bo))) {
            logger.error("签名不正确,userXcxBindingBO= {} ", bo);
            return Result.error("签名不正确");
        }

        User user = userDao.getByUniconId(bo.getUniconId());
        if (ObjectUtil.isNotNull(user)) {
            logger.error("已经绑定，请勿重复绑定{}", user);
            return Result.error("已经绑定，请勿重复绑定");
        }

        // 校验手机号
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(bo.getUsername()).matches()) {
            logger.error("请填写正确的手机号{}", bo.getUsername());
            return Result.error("请填写正确的手机号！");
        }
        // 校验用户基本信息
        UserExt userExt = userExtDao.getByMobile(bo.getUsername());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("该手机号未注册");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }

        user = userDao.getByMobile(bo.getUsername());

        if (ObjectUtil.isNull(user)) {
            return Result.error("账号或者密码不正确");
        }
        if (!StringUtils.isEmpty(user.getUniconId())) {
            logger.error("已经绑定，请勿重复绑定{}", user);
            return Result.error("该已经绑定，请勿重复绑定");
        }

        user = checkUser(user);

        // 密码校验
        if (!DigestUtil.sha1Hex(user.getMobileSalt() + bo.getPassword()).equals(user.getMobilePsw())) {
            return Result.error("账号或者密码不正确");
        }

        UserXcxBindingDTO userXcxBindingDTO = new UserXcxBindingDTO();
        userXcxBindingDTO.setUserNo(user.getUserNo());
        userXcxBindingDTO.setMobile(user.getMobile());
        userXcxBindingDTO.setHeadImgUrl(userExt.getHeadImgUrl());
        userXcxBindingDTO.setUserType(userExt.getUserType());
        userXcxBindingDTO.setVipType(userExt.getIsVip());
        userXcxBindingDTO.setUserType(userExt.getVipType());
        userXcxBindingDTO.setExpireTime(userExt.getExpireTime());
        extNickname(userXcxBindingDTO, userExt);

        try {
            if (myRedisTemplate.hasKey(Constants.Platform.XCX.concat(userExt.getUserNo().toString()))) {
                myRedisTemplate.delete(Constants.Platform.XCX.concat(userExt.getUserNo().toString()));
            }
            userXcxBindingDTO.setToken(JWTUtil.create(user.getUserNo(), JWTUtil.DATE, Constants.Platform.XCX));
            // 登录成功，存入缓存时间30天
            myRedisTemplate.set(Constants.Platform.XCX.concat(user.getUserNo().toString()), userXcxBindingDTO.getToken(), JWTUtil.DATE, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("系统异常");
            return Result.error("系统异常");
        }

        // 绑定关系
        user.setUniconId(bo.getUniconId());
        userDao.updateById(user);

        return Result.success(userXcxBindingDTO);
    }

    private void extNickname(UserXcxBindingDTO userXcxBindingDTO, UserExt userExt) {
        if (StringUtils.hasText(userExt.getNickname())) {
            userXcxBindingDTO.setNickname(userExt.getNickname());
        } else {
            userXcxBindingDTO.setNickname(userExt.getMobile());
        }
    }

    private User checkUser(User user) {
        if (StatusIdEnum.NO.getCode().equals(user.getStatusId()) || Constants.FREEZE.equals(user.getStatusId())) {
            throw new BaseException("该账号已被禁用，请联系管理员");
        } else {
            logger.warn("用户状态正常，可以登录");
            return user;
        }
    }

    private boolean checkSign(Map<String, Object> map) {
        TreeMap<String, Object> treeMap = new TreeMap<>(map);
        String sign = treeMap.get("sign").toString();
        treeMap.remove("sign");
        treeMap.remove("serialVersionUID");
        StringBuilder content = new StringBuilder();
        for (Map.Entry<String, Object> m : treeMap.entrySet()) {
            content.append(m.getValue().toString());
        }
        String argSign = MD5Util.MD5(content.toString());
        return argSign.equals(sign);
    }

    public Result<UserXcxLoginDTO> xcxLogin(UserXcxLoginBO bo) {
        // 校验时间
        long nowTimeMillis = System.currentTimeMillis();
        if (nowTimeMillis - bo.getTimestamp() > TIME) {
            return Result.error("校验失败，timestamp不正确或者过期");
        }

        UserXcxLoginDTO userXcxLoginDTO = new UserXcxLoginDTO();

        ConfigWeixinVO sysVO = feignSysConfig.getWeixin();
        // 根据code获取unicodeId
        String uniconId = WXUtil.getUniconId(bo.getCode(), sysVO.getMinappAppId(), sysVO.getMinappAppSecret());
        if (StringUtils.isEmpty(uniconId)) {
            return Result.error("登录失败");
        }

        User user = userDao.getByUniconId(uniconId);
        userXcxLoginDTO.setUniconId(uniconId);
        if (ObjectUtil.isNull(user)) {
            logger.error("找不到用户信息，没绑定小程序，需要进行绑定流程, 获取到的uniconId为{}", uniconId);
            return Result.success(100, userXcxLoginDTO);
        }
        if (!StatusIdEnum.YES.getCode().equals(user.getStatusId())) {
            return Result.error("该账号已禁用，请联系管理员");
        }

        UserExt userExt = userExtDao.getByUserNo(user.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("找不到用户信息");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("该账号已禁用，请联系管理员");
        }

        userXcxLoginDTO.setUserNo(user.getUserNo());
        userXcxLoginDTO.setUserType(userExt.getUserType());
        userXcxLoginDTO.setMobile(user.getMobile());
        userXcxLoginDTO.setHeadImgUrl(userExt.getHeadImgUrl());
        userXcxLoginDTO.setIsVip(userExt.getIsVip());
        userXcxLoginDTO.setVipType(userExt.getVipType());
        userXcxLoginDTO.setUserType(userExt.getUserType());
        userXcxLoginDTO.setExpireTime(userExt.getExpireTime());
        extNickname(userXcxLoginDTO, userExt);

        UserLogQO userLogQO = BeanUtil.copyProperties(bo, UserLogQO.class);
        userLogQO.setMobile(userExt.getMobile());
        CALLBACK_EXECUTOR.execute(new LoginLog(user.getUserNo(), LoginStatusEnum.SUCCESS, bo.getClientType(), userExt.getGmtCreate(), userLogQO));

        try {
            userXcxLoginDTO.setToken(JWTUtil.create(user.getUserNo(), JWTUtil.DATE, Constants.Platform.XCX));
            // 登录成功，存入缓存时间30天
            myRedisTemplate.set(Constants.Platform.XCX.concat(user.getUserNo().toString()), userXcxLoginDTO.getToken(), JWTUtil.DATE, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            logger.error("系统异常");
            return Result.error("系统异常");
        }

        return Result.success(userXcxLoginDTO);
    }

    private void extNickname(UserXcxLoginDTO userXcxLoginDTO, UserExt userExt) {
        if (StringUtils.hasText(userExt.getNickname())) {
            userXcxLoginDTO.setNickname(userExt.getNickname());
        } else {
            userXcxLoginDTO.setNickname(userExt.getMobile());
        }
    }

    public Result<String> verifySmsCode(UserSendCodeVerifyBO bo) {
        if (StringUtils.isEmpty(bo.getMobile())) {
            return Result.error("手机号码不能为空");
        }
        String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + bo.getMobile());
        if (StringUtils.isEmpty(redisSmsCode)) {
            return Result.error("验证码已失效!");
        }
        if (redisSmsCode.equals(bo.getCode())) {
            return Result.success("验证通过!");
        }
        return Result.error("验证码验证不通过!");
    }

    /**
     * 异步保存登录日志
     */
    class LoginLog implements Runnable {
        private final Long userNo;
        private final LoginStatusEnum status;
        private final Integer loginType;
        private final Date gmtCreate;
        private final UserLogQO userLogQO;

        public LoginLog(Long userNo, LoginStatusEnum status, Integer loginType, Date gmtCreate, UserLogQO userLogQO) {
            this.userNo = userNo;
            this.status = status;
            this.loginType = loginType;
            this.gmtCreate = gmtCreate;
            this.userLogQO = userLogQO;
        }

        @Override
        public void run() {
            UserLogLogin record = new UserLogLogin();
            record.setUserNo(userNo);
            record.setLoginStatus(status.getCode());
            record.setLoginIp(userLogQO.getLoginIp());
            userLogLoginDao.save(record);

            if (StringUtils.hasText(userLogQO.getCity())) {
                userLogQO.setCity(userLogQO.getCity().replace("市", ""));
            }
            if (StringUtils.hasText(userLogQO.getProvince())) {
                if ("省".equals(userLogQO.getProvince().substring(userLogQO.getProvince().length() - 1))) {
                    userLogQO.setProvince(userLogQO.getProvince().replace("省", ""));
                } else {
                    userLogQO.setProvince(userLogQO.getProvince().replace("市", ""));
                }
            }

            userLogQO.setLoginType(loginType);
            userLogQO.setRegisterTime(gmtCreate);
            userLogQO.setUserNo(userNo);
            feignUserLog.save(userLogQO);
        }
    }

}
