/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class UserXcxBindingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "用户编号")
	private Long userNo;
	/**
	 * 手机号码
	 */
	@ApiModelProperty(value = "手机号")
	private String mobile;
	/**
	 * token，设置有效期为1天
	 */
	@ApiModelProperty(value = "token，有效期为1天")
	private String token;

	/**
	 * 用户头像
	 */
	@ApiModelProperty(value = "用户头像")
	private String headImgUrl;
	/**
	 * 用户昵称
	 */
	@ApiModelProperty(value = "用户昵称")
	private String nickname;
	/**
	 * 用户类型(1用户，2讲师)
	 */
	@ApiModelProperty(value = "用户类型(1用户，2讲师)")
	private Integer userType;

	/**
	 * 是否是会员（0：不是，1：是）
	 */
	@ApiModelProperty(value = "是否是会员（0：不是，1：是）")
	private Integer isVip;
	/**
	 * 会员类型(1.年费，2.季度，3.月度)
	 */
	@ApiModelProperty(value = "会员类型(1.年费，2.季度，3.月度)")
	private Integer vipType;
	/**
	 * 过期时间
	 */
	@ApiModelProperty(value = "过期时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date expireTime;
}
