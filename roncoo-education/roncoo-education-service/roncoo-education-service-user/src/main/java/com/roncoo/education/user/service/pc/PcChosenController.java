package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.ChosenEditREQ;
import com.roncoo.education.user.common.req.ChosenListREQ;
import com.roncoo.education.user.common.req.ChosenSaveREQ;
import com.roncoo.education.user.common.req.ChosenUpdateStatusREQ;
import com.roncoo.education.user.common.resp.ChosenListRESP;
import com.roncoo.education.user.common.resp.ChosenViewRESP;
import com.roncoo.education.user.service.pc.biz.PcChosenBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 分销信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-分销信息")
@RestController
@RequestMapping("/user/pc/chosen")
public class PcChosenController {

    @Autowired
    private PcChosenBiz biz;

    @ApiOperation(value = "分销信息列表", notes = "分销信息列表")
    @PostMapping(value = "/list")
    public Result<Page<ChosenListRESP>> list(@RequestBody ChosenListREQ chosenListREQ) {
        return biz.list(chosenListREQ);
    }

    @ApiOperation(value = "分销信息添加", notes = "分销信息添加")
    @PostMapping(value = "/save")
    @SysLog(value = "分销信息添加")
    public Result<String> save(@RequestBody ChosenSaveREQ chosenSaveREQ) {
        return biz.save(chosenSaveREQ);
    }

    @ApiOperation(value = "分销信息查看", notes = "分销信息查看")
    @GetMapping(value = "/view")
    public Result<ChosenViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "分销信息修改", notes = "分销信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "分销信息修改")
    public Result<String> edit(@RequestBody ChosenEditREQ chosenEditREQ) {
        return biz.edit(chosenEditREQ);
    }

    @ApiOperation(value = "分销信息删除", notes = "分销信息删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "分销信息删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "分销信息状态修改", notes = "分销信息状态修改")
    @PutMapping(value = "/update/status")
    @SysLog(value = "分销信息状态修改")
    public Result<String> updateStatus(@RequestBody @Valid ChosenUpdateStatusREQ chosenUpdateStatusREQ) {
        return biz.updateStatus(chosenUpdateStatusREQ);
    }
}
