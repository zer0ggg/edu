package com.roncoo.education.user.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 支付
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayBO", description = "支付")
public class PayBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "渠道编号")
    private String payChannelCode;

    @ApiModelProperty(value = "订单编号")
    private String orderNo;

    @ApiModelProperty(value = "支付类型")
    private Integer payType;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "异步回调地址")
    private String notifyUrl;

    @ApiModelProperty(value = "同步回调地址")
    private String returnUrl;

    @ApiModelProperty(value = "微信公众号个人openId")
    private String openId;

}
