/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author LHR
 */
@Data
@Accessors(chain = true)
public class AuthUserOrderPayDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 订单号
	 */
	@ApiModelProperty(value = "订单号")
	private String orderNo;
	/**
	 * 支付消息
	 */
	@ApiModelProperty(value = "支付消息")
	private String payMessage;
	/**
	 * 实付金额
	 */
	@ApiModelProperty(value = "实付金额")
	private BigDecimal price;
	/**
	 * 支付方式：1微信支付，2支付宝支付
	 */
	@ApiModelProperty(value = "支付方式：1微信支付，2支付宝支付")
	private Integer payType;
	/**
	 * 签名方式(注意此处需与统一下单的签名类型一致)
	 */
	private String signType = "MD5";

}
