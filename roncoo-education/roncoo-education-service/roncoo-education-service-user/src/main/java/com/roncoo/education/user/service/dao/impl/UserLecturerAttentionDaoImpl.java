package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserLecturerAttentionDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserLecturerAttentionMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserLecturerAttentionDaoImpl implements UserLecturerAttentionDao {
	@Autowired
	private UserLecturerAttentionMapper userLecturerAttentionMapper;

	@Override
    public int save(UserLecturerAttention record) {
		record.setId(IdWorker.getId());
		return this.userLecturerAttentionMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.userLecturerAttentionMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(UserLecturerAttention record) {
		return this.userLecturerAttentionMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(UserLecturerAttention record, UserLecturerAttentionExample example) {
		return this.userLecturerAttentionMapper.updateByExampleSelective(record, example);
	}

	@Override
    public UserLecturerAttention getById(Long id) {
		return this.userLecturerAttentionMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<UserLecturerAttention> listForPage(int pageCurrent, int pageSize, UserLecturerAttentionExample example) {
		int count = this.userLecturerAttentionMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<UserLecturerAttention>(count, totalPage, pageCurrent, pageSize, this.userLecturerAttentionMapper.selectByExample(example));
	}

	@Override
	public UserLecturerAttention getByUserAndLecturerUserNo(Long userNo, Long lecturerUserNo) {
		UserLecturerAttentionExample example = new UserLecturerAttentionExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		criteria.andLecturerUserNoEqualTo(lecturerUserNo);
		List<UserLecturerAttention> list = this.userLecturerAttentionMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}
}
