package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户推荐信息
 * </p>
 *
 * @author wujing
 * @date 2020-04-10
 */
@Data
@Accessors(chain = true)
public class AuthSharingBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
