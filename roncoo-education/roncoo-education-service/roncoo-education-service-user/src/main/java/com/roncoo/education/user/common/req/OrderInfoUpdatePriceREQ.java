package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单信息改价
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "OrderInfoUpdatePriceREQ", description = "订单信息改价")
public class OrderInfoUpdatePriceREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @DecimalMin(value = "0", inclusive = false, message = "金额需要大于0")
    @NotNull(message = "实付金额不能为空")
    @ApiModelProperty(value = "实付金额", required = true)
    private BigDecimal pricePaid;
}
