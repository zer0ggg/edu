package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.UserRecommendedEditREQ;
import com.roncoo.education.user.common.req.UserRecommendedListREQ;
import com.roncoo.education.user.common.req.UserRecommendedSaveREQ;
import com.roncoo.education.user.common.resp.UserRecommendedListRESP;
import com.roncoo.education.user.common.resp.UserRecommendedViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserRecommendedBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户推荐信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户推荐信息")
@RestController
@RequestMapping("/user/pc/user/recommended")
public class PcUserRecommendedController {

    @Autowired
    private PcUserRecommendedBiz biz;

    @ApiOperation(value = "用户推荐信息列表", notes = "用户推荐信息列表")
    @PostMapping(value = "/list")
    public Result<Page<UserRecommendedListRESP>> list(@RequestBody UserRecommendedListREQ userRecommendedListREQ) {
        return biz.list(userRecommendedListREQ);
    }

    @ApiOperation(value = "用户推荐信息添加", notes = "用户推荐信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserRecommendedSaveREQ userRecommendedSaveREQ) {
        return biz.save(userRecommendedSaveREQ);
    }

    @ApiOperation(value = "用户推荐信息查看", notes = "用户推荐信息查看")
    @GetMapping(value = "/view")
    public Result<UserRecommendedViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户推荐信息修改", notes = "用户推荐信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserRecommendedEditREQ userRecommendedEditREQ) {
        return biz.edit(userRecommendedEditREQ);
    }

    @ApiOperation(value = "用户推荐信息删除", notes = "用户推荐信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "导出推荐信息表", notes = "导出推荐信息表")
    @PostMapping(value = "/export")
    public void export(@RequestBody UserRecommendedListREQ userRecommendedListREQ){
        biz.export(userRecommendedListREQ);
    }
}
