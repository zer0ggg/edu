package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.PayRuleEditREQ;
import com.roncoo.education.user.common.req.PayRuleListREQ;
import com.roncoo.education.user.common.req.PayRuleSaveREQ;
import com.roncoo.education.user.common.req.PayRuleUpdateStatusREQ;
import com.roncoo.education.user.common.resp.PayRuleListRESP;
import com.roncoo.education.user.common.resp.PayRuleViewRESP;
import com.roncoo.education.user.service.pc.biz.PcPayRuleBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 支付路由 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/pay/rule")
@Api(value = "user-支付路由", tags = {"user-支付路由"})
public class PcPayRuleController {

    @Autowired
    private PcPayRuleBiz biz;

    @ApiOperation(value = "支付路由列表", notes = "支付路由列表")
    @PostMapping(value = "/list")
    public Result<Page<PayRuleListRESP>> list(@RequestBody PayRuleListREQ payRuleListREQ) {
        return biz.list(payRuleListREQ);
    }

    @ApiOperation(value = "支付路由添加", notes = "支付路由添加")
    @PostMapping(value = "/save")
    @SysLog(value = "支付路由添加")
    public Result<String> save(@RequestBody PayRuleSaveREQ payRuleSaveREQ) {
        return biz.save(payRuleSaveREQ);
    }

    @ApiOperation(value = "支付路由查看", notes = "支付路由查看")
    @GetMapping(value = "/view")
    public Result<PayRuleViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "支付路由修改", notes = "支付路由修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "支付路由修改")
    public Result<String> edit(@RequestBody PayRuleEditREQ payRuleEditREQ) {
        return biz.edit(payRuleEditREQ);
    }

    @ApiOperation(value = "支付路由删除", notes = "支付路由删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "支付路由删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "支付路由状态修改", notes = "支付路由状态修改")
    @PutMapping(value = "/update/status")
    @SysLog(value = "支付路由状态修改")
    public Result<String> updateStatus(@RequestBody @Valid PayRuleUpdateStatusREQ payRuleUpdateStatusREQ) {
        return biz.updateStatus(payRuleUpdateStatusREQ);
    }
}
