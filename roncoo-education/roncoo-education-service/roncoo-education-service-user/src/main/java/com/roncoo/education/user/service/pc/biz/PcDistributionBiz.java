package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsAllowInvitationEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.user.common.req.DistributionEditREQ;
import com.roncoo.education.user.common.req.DistributionListREQ;
import com.roncoo.education.user.common.req.DistributionPageREQ;
import com.roncoo.education.user.common.req.DistributionSaveREQ;
import com.roncoo.education.user.common.resp.DistributionListRESP;
import com.roncoo.education.user.common.resp.DistributionPageRESP;
import com.roncoo.education.user.common.resp.DistributionViewRESP;
import com.roncoo.education.user.service.dao.DistributionDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.UserExtDaoImpl;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Distribution;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 分销信息
 *
 * @author wujing
 */
@Component
public class PcDistributionBiz extends BaseBiz {

    @Autowired
    private DistributionDao dao;
    @Autowired
    private UserExtDao userExtDao;

    /**
     * 分销信息列表
     *
     * @param distributionPageREQ 分销信息分页查询参数
     * @return 分销信息分页查询结果
     */
    public Result<Page<DistributionPageRESP>> list(DistributionPageREQ distributionPageREQ) {
        DistributionExample example = new DistributionExample();
        Criteria c = example.createCriteria();
        c.andFloorEqualTo(1);
        Page<Distribution> page = dao.listForPage(distributionPageREQ.getPageCurrent(), distributionPageREQ.getPageSize(), example);
        Page<DistributionPageRESP> respPage = PageUtil.transform(page, DistributionPageRESP.class);
        if (respPage.getList().isEmpty()) {
            return Result.success(respPage);
        }

        List<Distribution> distributionList = dao.listByFloor(1);
        List<DistributionPageRESP> DistributionPageRESPList = ArrayListUtil.copy(distributionList, DistributionPageRESP.class);
        if (CollectionUtil.isNotEmpty(DistributionPageRESPList)) {
            Set<Long> userNoLsit = DistributionPageRESPList.stream().map(DistributionPageRESP::getUserNo).collect(Collectors.toSet());
            List<UserExt> usersList = userExtDao.listByUserNos(new ArrayList<>(userNoLsit));
            Map<Long, UserExt> usersListMap = usersList.stream().collect(Collectors.toMap(UserExt::getUserNo, item -> item));
            DistributionPageRESPList.stream().forEach(item -> {
                UserExt userExt = usersListMap.get(item.getUserNo());
                if (ObjectUtil.isNotNull(userExt)) {
                    item.setMobile(userExt.getMobile());
                }
            });
        }
        Set<Long> userNos = respPage.getList().stream().map(DistributionPageRESP::getUserNo).collect(Collectors.toSet());
        List<UserExt> userList = userExtDao.listByUserNos(new ArrayList<>(userNos));
        Map<Long, UserExt> userListMap = userList.stream().collect(Collectors.toMap(UserExt::getUserNo, item -> item));
        respPage.getList().stream().forEach(item -> {
            UserExt userExt = userListMap.get(item.getUserNo());
            if (ObjectUtil.isNotNull(userExt)) {
                item.setMobile(userExt.getMobile());
            }
            if (CollectionUtil.isNotEmpty(DistributionPageRESPList)) {
                item.setChildrenList(getChildrens(item, DistributionPageRESPList));
            }
        });
        return Result.success(respPage);
    }

    /**
     * 递归查询子节点
     *
     * @param root 根节点
     * @param all  所有节点
     * @return 根节点信息
     */
    private List<DistributionPageRESP> getChildrens(DistributionPageRESP root, List<DistributionPageRESP> all) {
        List<DistributionPageRESP> children = all.stream().filter(m -> {
            return Objects.equals(m.getParentId(), root.getId());
        }).map(
                (m) -> {
                    m.setChildrenList(getChildrens(m, all));
                    return m;
                }
        ).collect(Collectors.toList());
        return children;
    }


    /**
     * 分销信息添加
     *
     * @param distributionSaveREQ 分销信息
     * @return 添加结果
     */
    public Result<String> save(DistributionSaveREQ distributionSaveREQ) {
        distributionSaveREQ.setInviteProfit(NumberUtil.div(distributionSaveREQ.getInviteProfit(), 100).setScale(2, RoundingMode.DOWN));
        distributionSaveREQ.setSpreadProfit(NumberUtil.div(distributionSaveREQ.getSpreadProfit(), 100).setScale(2, RoundingMode.DOWN));
        BigDecimal bigDecimal = distributionSaveREQ.getSpreadProfit().add(distributionSaveREQ.getInviteProfit());
        if (bigDecimal.compareTo(new BigDecimal(90L)) == 1) {
            return Result.error("同一等级下，推广奖励比例与邀请奖励比例之和不能大于90%");
        }
        if (distributionSaveREQ.getInviteProfit().compareTo(distributionSaveREQ.getSpreadProfit()) == 1) {
            return Result.error("同一等级下，推广奖励比例需大于邀请奖励比例");
        }
        Distribution distribution = dao.getByUserNo(distributionSaveREQ.getUserNo());
        if (ObjectUtil.isNotNull(distribution)) {
            return Result.error("用户已添加成为分销");
        }
        Distribution record = BeanUtil.copyProperties(distributionSaveREQ, Distribution.class);
        record.setFloor(1);
        if (!distributionSaveREQ.getParentId().equals(0L)) {
            Distribution distributionParentId = dao.getById(distributionSaveREQ.getParentId());
            if (ObjectUtil.isNull(distributionParentId)) {
                return Result.error("找不到上级分销信息");
            }
            if (IsAllowInvitationEnum.NOT_ALLOW.getCode().equals(distributionParentId.getIsAllowInvitation())) {
                return Result.error("上级分销信息没有权限邀请分销");
            }
            if (distributionSaveREQ.getInviteProfit().compareTo(distributionParentId.getInviteProfit()) == 1) {
                return Result.error("高等级的邀请比例需大于低等级的邀请比例");
            }
            if (distributionSaveREQ.getSpreadProfit().compareTo(distributionParentId.getSpreadProfit()) == 1) {
                return Result.error("高等级的推广比例需大于低等级的推广比例");
            }
            if (distributionSaveREQ.getInviteProfit().add(distributionParentId.getSpreadProfit()).compareTo(new BigDecimal(0.9)) == 1) {
                return Result.error("推广奖励与上级邀请奖励之和不能大于90%");
            }
            record.setFloor(distributionParentId.getFloor() + 1);
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 分销信息查看
     *
     * @param id 主键ID
     * @return 分销信息
     */
    public Result<DistributionViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), DistributionViewRESP.class));
    }


    /**
     * 分销信息修改
     *
     * @param distributionEditREQ 分销信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(DistributionEditREQ distributionEditREQ) {
        distributionEditREQ.setInviteProfit(NumberUtil.div(distributionEditREQ.getInviteProfit(), 100).setScale(2, RoundingMode.DOWN));
        distributionEditREQ.setSpreadProfit(NumberUtil.div(distributionEditREQ.getSpreadProfit(), 100).setScale(2, RoundingMode.DOWN));
        Distribution distribution = dao.getById(distributionEditREQ.getId());
        if (ObjectUtil.isNull(distribution)) {
            return Result.error("找不到分销信息");
        }
        if (!distribution.getParentId().equals(0L)) {
            Distribution distributionParentId = dao.getById(distribution.getParentId());
            if (ObjectUtil.isNull(distributionParentId)) {
                return Result.error("找不到上级分销信息");
            }
            if (IsAllowInvitationEnum.NOT_ALLOW.getCode().equals(distributionParentId.getIsAllowInvitation())) {
                return Result.error("上级分销信息没有权限邀请分销");
            }
            if (distributionEditREQ.getInviteProfit().compareTo(distributionParentId.getInviteProfit()) == 1) {
                return Result.error("高等级的邀请比例需大于低等级的邀请比例");
            }
            if (distributionEditREQ.getSpreadProfit().compareTo(distributionParentId.getSpreadProfit()) == 1) {
                return Result.error("高等级的推广比例需大于低等级的推广比例");
            }
            if (distributionEditREQ.getSpreadProfit().add(distributionParentId.getInviteProfit()).compareTo(new BigDecimal(0.9)) == 1) {
                return Result.error("推广奖励与上级邀请奖励之和不能大于90%");
            }
        }
        // 下级
        List<Distribution> list = dao.listByParentId(distributionEditREQ.getId());
        if (CollectionUtil.isNotEmpty(list)) {
            BigDecimal minEntryInviteProfit = list.stream().map(Distribution::getInviteProfit).min(BigDecimal::compareTo).get();
            BigDecimal minEntrySpreadProfit = list.stream().map(Distribution::getSpreadProfit).min(BigDecimal::compareTo).get();
            if (minEntryInviteProfit.compareTo(distributionEditREQ.getInviteProfit()) == 1) {
                return Result.error("高等级的邀请比例需大于低等级的邀请比例");
            }
            if (minEntrySpreadProfit.compareTo(distributionEditREQ.getSpreadProfit()) == 1) {
                return Result.error("高等级的推广比例需大于低等级的推广比例");
            }
            if (distributionEditREQ.getInviteProfit().add(minEntrySpreadProfit).compareTo(new BigDecimal(0.9)) == 1) {
                return Result.error("邀请奖励与下级推广奖励之和不能大于90%");
            }
        }

        Distribution record = BeanUtil.copyProperties(distributionEditREQ, Distribution.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 分销信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        Distribution distribution = dao.getById(id);
        if (ObjectUtil.isNull(distribution)) {
            return Result.error("找不到分销信息");
        }
        if (!distribution.getParentId().equals(0L)) {
            List<Distribution> listByParent = dao.listByParentId(distribution.getId());
            if (CollectionUtil.isNotEmpty(listByParent)) {
                return Result.error("已存下级分销，请先删除下级分销信息");
            }
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
