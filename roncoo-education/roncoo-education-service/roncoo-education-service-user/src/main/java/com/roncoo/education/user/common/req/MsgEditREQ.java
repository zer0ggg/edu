package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 站内信息
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "MsgEditREQ", description = "站内信息修改")
public class MsgEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "短信标题")
    private String msgTitle;

    @ApiModelProperty(value = "短信内容")
    private String msgText;

    @ApiModelProperty(value = "是否置顶(1是;0否)")
    private Integer isTop;
}
