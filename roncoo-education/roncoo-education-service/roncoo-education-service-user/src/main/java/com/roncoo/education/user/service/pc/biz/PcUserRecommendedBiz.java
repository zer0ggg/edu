package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.UserTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.user.common.req.UserRecommendedEditREQ;
import com.roncoo.education.user.common.req.UserRecommendedListREQ;
import com.roncoo.education.user.common.req.UserRecommendedSaveREQ;
import com.roncoo.education.user.common.resp.UserRecommendedListRESP;
import com.roncoo.education.user.common.resp.UserRecommendedViewRESP;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserRecommendedDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample.Criteria;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * 用户推荐信息
 *
 * @author wujing
 */
@Component
public class PcUserRecommendedBiz extends BaseBiz {

    /**
     * 内存中缓存记录行数
     */
    private static final Integer REPORT_CACHE_NUM = 100;
    private static final Integer NUM_PER_PAGE = 1000;
    private static final Integer REPORT_MAX_TOTAL = 100000;
    private static final Integer SHEET_MAX_NUM = 60000;

    @Autowired
    private UserRecommendedDao dao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private HttpServletResponse response;

    /**
     * 用户推荐信息列表
     *
     * @param req 用户推荐信息分页查询参数
     * @return 用户推荐信息分页查询结果
     */
    public Result<Page<UserRecommendedListRESP>> list(UserRecommendedListREQ req) {
        UserRecommendedExample example = new UserRecommendedExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getReferralCode())) {
            c.andReferralCodeEqualTo(req.getReferralCode());
        }
        if (StringUtils.hasText(req.getMobile())) {
            UserExt user = userExtDao.getByMobile(req.getMobile());
            if (ObjectUtil.isNotNull(user)) {
                c.andUserNoEqualTo(user.getUserNo());
            }
        }
        example.setOrderByClause(" id desc ");
        Page<UserRecommended> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<UserRecommendedListRESP> respPage = PageUtil.transform(page, UserRecommendedListRESP.class);
        for (UserRecommendedListRESP resp : respPage.getList()) {
            UserExt userExt = userExtDao.getByUserNo(resp.getUserNo());
            if (ObjectUtil.isNotNull(userExt)) {
                resp.setUserId(userExt.getId());
                resp.setMobile(userExt.getMobile());
                resp.setUserType(userExt.getUserType());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 用户推荐信息添加
     *
     * @param userRecommendedSaveREQ 用户推荐信息
     * @return 添加结果
     */
    public Result<String> save(UserRecommendedSaveREQ userRecommendedSaveREQ) {
        UserRecommended record = BeanUtil.copyProperties(userRecommendedSaveREQ, UserRecommended.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户推荐信息查看
     *
     * @param id 主键ID
     * @return 用户推荐信息
     */
    public Result<UserRecommendedViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserRecommendedViewRESP.class));
    }


    /**
     * 用户推荐信息修改
     *
     * @param userRecommendedEditREQ 用户推荐信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserRecommendedEditREQ userRecommendedEditREQ) {
        UserRecommended record = BeanUtil.copyProperties(userRecommendedEditREQ, UserRecommended.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户推荐信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 分页，导出共用查询条件
     *
     * @param req 推荐人信息分页查询参数
     * @return 查询条件
     */
    public UserRecommendedExample getPageExample(UserRecommendedListREQ req) {

        UserRecommendedExample example = new UserRecommendedExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getReferralCode())) {
            c.andReferralCodeEqualTo(req.getReferralCode());
        }
        if (StringUtils.hasText(req.getMobile())) {
            //手机号
            UserExt user = userExtDao.getByMobile(req.getMobile());
            if (ObjectUtil.isNotNull(user)) {
                c.andUserNoEqualTo(user.getUserNo());
            }
        }
        example.setOrderByClause(" id desc ");
        return example;
    }

    /**
     * 导出用户推荐人信息表
     */
    public void export(UserRecommendedListREQ userRecommendedListREQ) {

        logger.info("导出报表");
        logger.info("导出参数:{}", userRecommendedListREQ);
        try {

            String fileName = "推荐人信息" + DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN);
            response.setCharacterEncoding("utf-8");
            logger.info("导出推荐人信息！");
            //推荐人信息
            String[] userr = {"用户编号","手机号","昵称","推荐数量"};
            //被推荐人信息
            String[] names = {"用户编号", "手机号", "用户角色", "注册时间"};
            //导出行数
            Integer[] widths = {10, 10, 10, 10};
            // 当前sheet行数
            int rowIndex = 8;

            // 创建一个excel文件
            final SXSSFWorkbook workbook = new SXSSFWorkbook(REPORT_CACHE_NUM);
            //为该工作簿创建一张图纸sheet
            SXSSFSheet sheet = workbook.createSheet("1表");

            //合并列单元对象
            //起始行,结束行,起始列,结束列
            CellRangeAddress callRangeAddress1 = new CellRangeAddress(0,1,0,3);
            //加载合并单元格对象
            sheet.addMergedRegion(callRangeAddress1);

            //创建推荐人 / 被推荐人头列样式
            //创建样式
            CellStyle cellStyle = workbook.createCellStyle();
            //创建 FONT
            Font font = workbook.createFont();
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            //写入样式居中 CENTER:水平对齐方式以多个单元格为中心。
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
            font.setBold(true);
            //文字加粗
            cellStyle.setFont(font);

            //头列 推荐信息
            SXSSFRow row2 = sheet.createRow(0);
            SXSSFCell cell1 = row2.createCell(0);
            cell1.setCellValue("推荐信息");
            //文本赋值样式
            cell1.setCellStyle(cellStyle);


             //创建sheet第一列
            //起始行0
            SXSSFRow row3 = sheet.createRow(2);
            //列表题标
            int userrLength = userr.length;
            for(int j = 0; j < userrLength;j++){
                //第一行
                row3.createCell(j).setCellValue(userr[j]);
                sheet.setColumnWidth(j, widths[j] * 512);
            }

            UserExt userExt = userExtDao.getByReferralCode(userRecommendedListREQ.getReferralCode());
            //统计推荐人信息  创建第三行
            SXSSFRow row4 = sheet.createRow(3);
            //推荐编号
            row4.createCell(0).setCellValue(userExt.getUserNo().toString());
            //推荐手机号
            row4.createCell(1).setCellValue(userExt.getMobile());
            //推荐昵称
            row4.createCell(2).setCellValue(userExt.getNickname());
            //推荐数量获取页面被推荐人信息数
            row4.createCell(3).setCellValue(0);


            //合并列单元对象
            //起始行,结束行,起始列,结束列
            CellRangeAddress callRangeAddress2 = new CellRangeAddress(5,6,0,3);
            //加载合并单元对象
            sheet.addMergedRegion(callRangeAddress2);
            //创建合并单元
            SXSSFRow row5 = sheet.createRow(5);
            SXSSFCell cell2 = row5.createCell(0);
            cell2.setCellValue("被推荐人信息");
            cell2.setCellStyle(cellStyle);

            //  被推荐人信息  第六列
            SXSSFRow rowRecommendedInfo = sheet.createRow(7);
            //被推荐人信息创建头部  循环输出信息列
            int nameLength = names.length;
            for (int i = 0; i < nameLength; i++) {
                rowRecommendedInfo.createCell(i).setCellValue(names[i]);
                sheet.setColumnWidth(i, widths[i] * 512);
            }



            SXSSFRow row;
            // 第一次获取数据
            boolean isGo = true;
            userRecommendedListREQ.setPageSize(NUM_PER_PAGE);
            Page<UserRecommended> page = null;
            do {
                try {
                    UserRecommendedExample example = getPageExample(userRecommendedListREQ);
                    page = dao.listForPage(userRecommendedListREQ.getPageCurrent(), userRecommendedListREQ.getPageSize(), example);
                } catch (Exception e) {
                    logger.info("获取推荐信息失败!", e);

                }
                //是否导出成功的异常判断
                if (userRecommendedListREQ.getPageCurrent() <= 1) {
                    boolean isError = false;
                    if (StringUtils.isEmpty(page) || page.getList() == null || page.getList().isEmpty()) {
                        logger.error("没有需要导出的推荐信息!");
                        isError = true;
                        sheet.createRow(rowIndex++).createCell(0).setCellValue("没有需要导出的推荐信息!");
                    }
                    // 判断中记录数是否大于最大限额
                    assert page != null;
                    if (REPORT_MAX_TOTAL.compareTo(page.getTotalCount()) < 0) {
                        logger.error("选中的推荐信息据大于" + REPORT_MAX_TOTAL + "条");
                        isError = true;
                        sheet.createRow(++rowIndex).createCell(0).setCellValue("选中的推荐信息据大于" + REPORT_MAX_TOTAL + "条");
                    }
                    if (isError) {
                        writeExcel(response, workbook, fileName);
                    }
                }
                //推荐数量获取页面被推荐人信息数
                row4.createCell(3).setCellValue(page.getList().size());


                //获取页面显示被推荐人人
                int totalCount = page.getList().size();
                //用String 类型接收到 枚举字段
                String StringUserTypeEnum;
                List<UserRecommended> resultList = page.getList();
                // 被推荐人列表导出
                for (int i = 0; i < totalCount; i++) {
                    //被推荐人 循环创建 防止覆盖
                    row = sheet.createRow(rowIndex++);
                    UserRecommended userRecommended = resultList.get(i);
                    UserExt user = userExtDao.getByUserNo(userRecommended.getUserNo());
                    if (UserTypeEnum.USER.getCode().equals(user.getUserType())) {
                        StringUserTypeEnum = UserTypeEnum.USER.getDesc();
                    } else {
                        StringUserTypeEnum = UserTypeEnum.LECTURER.getDesc();
                    }
                    //用户编号
                    row.createCell(0).setCellValue(userRecommended.getUserNo().toString());
                    //手机号
                    row.createCell(1).setCellValue(user.getMobile());
                    //用户角色
                    row.createCell(2).setCellValue(StringUserTypeEnum);
                    //创建时间
                    row.createCell(3).setCellValue(DateUtil.format(userRecommended.getGmtCreate(),DatePattern.NORM_DATETIME_PATTERN));

                }

                // 每当行数达到设置的值就刷新数据到硬盘,以清理内存
                if (rowIndex % REPORT_CACHE_NUM == 0) {
                    try {
                        sheet.flushRows();
                    } catch (IOException e) {
                        logger.error("推荐信息导出,刷新数据到硬盘失败！");
                    }
                }

                // 当记录达到每页的sheet记录数大小，创建新的sheet
                if (rowIndex % SHEET_MAX_NUM == 0) {
                    logger.info("推荐信息导出，创建新sheet:{}", workbook.getNumberOfSheets() + 1);
                    sheet = workbook.createSheet((workbook.getNumberOfSheets() + 1) + "表");
                    rowIndex = 0;
                    row = sheet.createRow(rowIndex);
                    // 创建sheet头部
                    nameLength = names.length;
                    for (int j = 0; j < nameLength; j++) {
                        row.createCell(j).setCellValue(names[j]);
                        sheet.setColumnWidth(j, widths[j] * 512);
                    }
                }
                if (page.getPageCurrent() >= page.getTotalPage()) {
                    isGo = false;
                }
                userRecommendedListREQ.setPageCurrent(page.getPageCurrent() + 1);
            } while (isGo);
            writeExcel(response, workbook, fileName);
        } catch (Exception e) {
            logger.error("错误信息", e);
        }
    }

    /**
     * 写入文件
     *
     * @param response
     * @param workbook
     * @param fileName
     */
    private void writeExcel(HttpServletResponse response, SXSSFWorkbook workbook, String fileName) {
        // 设置强制下载不打开
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");

        // 设置文件名
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes(), StandardCharsets.ISO_8859_1) + ".xlsx");

        ServletOutputStream outputStream = null;
        // 写入数据
        try {
            outputStream = response.getOutputStream();
            workbook.write(outputStream);
            outputStream.flush();
        } catch (IOException e) {
            logger.error("日志导出失败！", e);
        } finally {
            try {
                if (workbook != null) {
                    workbook.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                logger.error("日志导出失败", e);
            }
        }
    }
}
