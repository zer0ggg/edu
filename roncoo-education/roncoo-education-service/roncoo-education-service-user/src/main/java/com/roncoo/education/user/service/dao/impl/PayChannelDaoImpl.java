package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.PayChannelMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 银行渠道信息表 服务实现类
 *
 * @author wujing
 * @date 2020-03-25
 */
@Repository
public class PayChannelDaoImpl implements PayChannelDao {

    @Autowired
    private PayChannelMapper mapper;

    @Override
    public int save(PayChannel record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(PayChannel record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public PayChannel getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<PayChannel> listForPage(int pageCurrent, int pageSize, PayChannelExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent((int) count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage((int) count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<PayChannel>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public PayChannel getByPayChannelCode(String payChannelCode) {
        PayChannelExample example = new PayChannelExample();
        Criteria c = example.createCriteria();
        c.andPayChannelCodeEqualTo(payChannelCode);
        List<PayChannel> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<PayChannel> listUsable() {
        PayChannelExample example = new PayChannelExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        return this.mapper.selectByExample(example);
    }


}
