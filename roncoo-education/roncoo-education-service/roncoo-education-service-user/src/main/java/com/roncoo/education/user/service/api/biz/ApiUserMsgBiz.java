package com.roncoo.education.user.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.IsDefaultEnum;
import com.roncoo.education.user.service.dao.MsgDao;
import com.roncoo.education.user.service.dao.UserMsgDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Msg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.HashMap;
import java.util.Map;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@Component
public class ApiUserMsgBiz extends BaseBiz {

	/**
	 * 消息简介模板
	 */
	private static final String TEMPLATE1 = "msg.ftl";

	@Autowired
	private UserMsgDao dao;
	@Autowired
	private MsgDao msgDao;
	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;

	public String view(Long id) {
		UserMsg record = dao.getById(id);
		if (ObjectUtil.isNull(record)) {
			throw new BaseException("找不到站内信用户记录");
		}
		// 未阅读，则刷新阅读状态
		if (IsDefaultEnum.NO.getCode().equals(record.getIsRead())) {
			record.setIsRead(IsDefaultEnum.YES.getCode());
			dao.updateById(record);
		}
		// 返回消息实体
		Msg msg = msgDao.getById(record.getMsgId());
		if (ObjectUtil.isNull(msg)) {
			throw new BaseException("找不到站内信");
		}
		Map<String, Object> model = new HashMap<>();

		model.put("msg", msg);
		return getTextByTemplate(TEMPLATE1, model);
	}

	private String getTextByTemplate(String template, Object model) {
		try {
			return FreeMarkerTemplateUtils.processTemplateIntoString(freeMarkerConfigurer.getConfiguration().getTemplate(template), model);
		} catch (Exception e) {
			logger.error("小程序创建页面模板失败，原因：", e);
			return "";
		}
	}

}
