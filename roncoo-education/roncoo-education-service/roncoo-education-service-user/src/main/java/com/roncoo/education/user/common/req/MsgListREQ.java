package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 站内信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="MsgListREQ", description="站内信息列表")
public class MsgListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "短信类型(1系统消息,2其他)")
    private Integer msgType;

    @ApiModelProperty(value = "短信标题")
    private String msgTitle;

    @ApiModelProperty(value = "短信内容")
    private String msgText;

    @ApiModelProperty(value = "是否定时发送（1是，0否）")
    private Integer isTimeSend;

    @ApiModelProperty(value = "发送时间")
    private Date sendTime;

    @ApiModelProperty(value = "是否已发送(1是;0否)")
    private Integer isSend;

    @ApiModelProperty(value = "是否置顶(1是;0否)")
    private Integer isTop;

    @ApiModelProperty(value = "后台备注")
    private String backRemark;

    @ApiModelProperty(value = "是否发给部分用户(1是;0否)")
    private Integer isToPart;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
