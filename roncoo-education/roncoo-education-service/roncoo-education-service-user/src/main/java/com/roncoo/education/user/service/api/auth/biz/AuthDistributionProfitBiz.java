package com.roncoo.education.user.service.api.auth.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.user.service.dao.DistributionProfitDao;

/**
 * 代理分润记录
 *
 * @author wujing
 */
@Component
public class AuthDistributionProfitBiz extends BaseBiz {

    @Autowired
    private DistributionProfitDao dao;

}
