package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.UserExtEditREQ;
import com.roncoo.education.user.common.req.UserExtListREQ;
import com.roncoo.education.user.common.req.UserExtUpdateStatusREQ;
import com.roncoo.education.user.common.resp.UserExtListRESP;
import com.roncoo.education.user.common.resp.UserExtViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserBiz;
import com.roncoo.education.user.service.pc.biz.PcUserExtBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * 用户教育信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/ext")
@Api(value = "user-用户教育信息", tags = {"user-用户教育信息"})
public class PcUserExtController {

    @Autowired
    private PcUserExtBiz biz;

    @Autowired
    private PcUserBiz userBiz;

    @ApiOperation(value = "用户教育信息列表", notes = "用户教育信息列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExtListRESP>> list(@RequestBody UserExtListREQ userExtListREQ) {
        return biz.list(userExtListREQ);
    }

    @ApiOperation(value = "用户教育信息查看", notes = "用户教育信息查看")
    @GetMapping(value = "/view")
    public Result<UserExtViewRESP> view(@RequestParam Long userNo) {
        return biz.view(userNo);
    }

    @ApiOperation(value = "用户教育信息修改", notes = "用户教育信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "用户教育信息修改")
    public Result<String> edit(@RequestBody UserExtEditREQ userExtEditREQ) {
        return biz.edit(userExtEditREQ);
    }

    @ApiOperation(value = "重置登录错误次数", notes = "重置登录错误次数")
    @PostMapping(value = "/reset/error")
    @SysLog(value = "重置登录错误次数")
    public Result<String> resetError(@RequestParam Long userNo) {
        return biz.resetError(userNo);
    }

    @ApiOperation(value = "解绑小程序", notes = "解绑小程序")
    @PostMapping(value = "/unbind")
    @SysLog(value = "解绑小程序")
    public Result<String> unbind(@RequestParam Long userNo) {
        return userBiz.unbind(userNo);
    }

    @ApiOperation(value = "用户教育信息状态修改", notes = "用户教育信息状态修改")
    @PutMapping(value = "/update/status")
    @SysLog(value = "用户教育信息状态修改")
    public Result<String> updateStatus(@RequestBody @Valid UserExtUpdateStatusREQ userExtUpdateStatusREQ) {
        return biz.updateStatus(userExtUpdateStatusREQ);
    }

    @ApiOperation(value = "用户教育信息导出报表", notes = "用户教育信息导出报表")
    @PostMapping(value = "/export")
    @SysLog(value = "用户教育信息导出报表")
    public void export(UserExtListREQ userExtListREQ) {
        biz.export(userExtListREQ);
    }

    @ApiOperation(value = "导入用户信息", notes = "导入用户信息")
    @SysLog(value = "导入用户信息")
    @PostMapping(value = "/upload/excel")
    public Result<String> uploadExcel(@RequestParam("file") MultipartFile file) {
        return biz.uploadExcel(file);
    }
}
