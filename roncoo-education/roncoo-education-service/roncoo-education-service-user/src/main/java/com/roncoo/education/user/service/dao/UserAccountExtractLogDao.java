package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;

import java.util.List;

public interface UserAccountExtractLogDao {
    int save(UserAccountExtractLog record);

    int deleteById(Long id);

    int updateById(UserAccountExtractLog record);

    int updateByExampleSelective(UserAccountExtractLog record, UserAccountExtractLogExample example);

    UserAccountExtractLog getById(Long id);

    Page<UserAccountExtractLog> listForPage(int pageCurrent, int pageSize, UserAccountExtractLogExample example);

    /**
     * 根据用户编号列出用户提现信息
     *
     * @param userNo
     * @return
     */
    List<UserAccountExtractLog> listByUserNo(Long userNo);

    /**
     * 根据用户编号更新手机号
     *
     * @param userNo
     * @param phone
     *
     * @return
     */
    int updateByMobile(Long userNo, String phone);
}