package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserRecommendedDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserRecommendedMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 用户推荐信息 服务实现类
 *
 * @author wujing
 * @date 2020-07-15
 */
@Repository
public class UserRecommendedDaoImpl implements UserRecommendedDao {

    @Autowired
    private UserRecommendedMapper mapper;

    @Override
    public int save(UserRecommended record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserRecommended record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserRecommended getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserRecommended> listForPage(int pageCurrent, int pageSize, UserRecommendedExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


}
