package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.NOUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.OrderEchartsQO;
import com.roncoo.education.user.feign.qo.OrderInfoQO;
import com.roncoo.education.user.feign.vo.*;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单信息表
 *
 * @author wujing
 */
@Component
public class FeignOrderInfoBiz extends BaseBiz {

    @Autowired
    private OrderInfoDao dao;

    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Page<OrderInfoVO> listForPage(OrderInfoQO qo) {
        OrderInfoExample example = new OrderInfoExample();
        Criteria c = example.createCriteria();
        if (qo.getOrderNo() != null) {
            c.andOrderNoEqualTo(qo.getOrderNo());
        }
        if (qo.getTradeType() != null) {
            c.andTradeTypeEqualTo(qo.getTradeType());
        }
        if (qo.getPayType() != null) {
            c.andPayTypeEqualTo(qo.getPayType());
        }
        if (qo.getChannelType() != null) {
            c.andChannelTypeEqualTo(qo.getChannelType());
        }
        if (qo.getOrderStatus() != null) {
            c.andOrderStatusEqualTo(qo.getOrderStatus());
        } else {
            c.andOrderStatusNotEqualTo(OrderStatusEnum.CLOSE.getCode());
        }
        // 用户手机号
        if (StringUtils.hasText(qo.getMobile())) {
            c.andMobileLike(PageUtil.like(qo.getMobile()));
        }
        // 讲师编号
        if (qo.getLecturerUserNo() != null) {
            c.andLecturerUserNoEqualTo(qo.getLecturerUserNo());
        }
        // 讲师名称
        if (StringUtils.hasText(qo.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.like(qo.getLecturerName()));
        }
        // 课程ID
        if (qo.getProductId() != null) {
            c.andProductIdEqualTo(qo.getProductId());
        }
        // 课程名称
        if (StringUtils.hasText(qo.getProductName())) {
            c.andProductNameLike(PageUtil.like(qo.getProductName()));
        }
        // 后台备注
        if (StringUtils.hasText(qo.getRemark())) {
            c.andRemarkLike(PageUtil.like(qo.getRemark()));
        }
        // 课程分类
        if (qo.getProductType() != null) {
            c.andProductTypeEqualTo(qo.getProductType());
        }
        if (StringUtils.hasText(qo.getBeginPayTime())) {
            c.andPayTimeGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginPayTime(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(qo.getEndPayTime())) {
            c.andPayTimeLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndPayTime(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" order_status asc , id desc ");
        Page<OrderInfo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, OrderInfoVO.class);
    }

    public int save(OrderInfoQO qo) {
        OrderInfo record = BeanUtil.copyProperties(qo, OrderInfo.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public OrderInfoVO getById(Long id) {
        OrderInfo record = dao.getById(id);
        return BeanUtil.copyProperties(record, OrderInfoVO.class);
    }

    public int updateById(OrderInfoQO qo) {
        OrderInfo record = BeanUtil.copyProperties(qo, OrderInfo.class);
        return dao.updateById(record);
    }

    /**
     * 订单信息汇总（导出报表）
     */
    public List<OrderReportVO> listForReport(OrderInfoQO orderInfoQO) {
        StringBuilder sql = new StringBuilder();
        sql.append("select lecturer_name, lecturer_user_no, course_name, course_id, count(*) as courseCount, sum(price_paid) as pricePaidSum from order_info where 1=1 ");
        sql.append(joinSql(orderInfoQO));
        sql.append(" group by lecturer_user_no, course_id ");
        return jdbcTemplate.query(sql.toString(), (resultSet, rowNum) -> {
            OrderReportVO vo = new OrderReportVO();
            vo.setLecturerName(resultSet.getString("lecturer_name"));
            vo.setLecturerUserNo(resultSet.getLong("lecturer_user_no"));
            vo.setCourseName(resultSet.getString("course_name"));
            vo.setCourseCount(resultSet.getInt("courseCount"));
            vo.setCourseId(resultSet.getLong("course_id"));
            vo.setCountPaidprice(new BigDecimal(resultSet.getString("pricePaidSum")));
            return vo;
        });
    }

    /**
     * 统计时间段的总订单数
     *
     * @param orderEchartsQO
     * @return
     * @author wuyun
     */
    public List<OrderEchartsVO> sumByCountOrders(OrderEchartsQO orderEchartsQO) {
        List<OrderEchartsVO> list = new ArrayList<>();
        List<Integer> countOrders = new ArrayList<>();
        OrderEchartsVO vo = new OrderEchartsVO();
        for (String date : orderEchartsQO.getDateList()) {
            Integer sum = dao.sumByCountOrders(date);
            countOrders.add(sum);
        }
        vo.setCountOrders(countOrders);
        list.add(vo);
        return list;
    }

    /**
     * 统计时间段的总收入
     *
     * @param orderEchartsQO
     * @return
     * @author wuyun
     */
    public List<OrderEchartsVO> sumByPayTime(OrderEchartsQO orderEchartsQO) {
        List<OrderEchartsVO> list = new ArrayList<>();
        List<BigDecimal> countPaidPrice = new ArrayList<>();
        OrderEchartsVO vo = new OrderEchartsVO();
        for (String date : orderEchartsQO.getDateList()) {
            BigDecimal sum = dao.sumByPayTime(date);
            countPaidPrice.add(sum);
        }
        vo.setCountPaidPrice(countPaidPrice);
        list.add(vo);
        return list;
    }

    /**
     * 统计订单收入情况
     */
    public CountIncomeVO countIncome(OrderInfoQO qo) {
        StringBuilder sql = new StringBuilder();
        sql.append("select sum(price_paid) as totalProfit, sum(lecturer_profit) as lecturerProfit, sum(platform_profit) as platformProfit from order_info where 1=1 ");
        sql.append(joinSql(qo));
        return jdbcTemplate.queryForObject(sql.toString(), (resultSet, arg1) -> {
            CountIncomeVO o = new CountIncomeVO();
            if (StringUtils.isEmpty(resultSet.getBigDecimal("totalProfit"))) {
                o.setTotalProfit(BigDecimal.ZERO);
            } else {
                o.setTotalProfit(resultSet.getBigDecimal("totalProfit"));
            }

            if (StringUtils.isEmpty(resultSet.getBigDecimal("lecturerProfit"))) {
                o.setLecturerProfit(BigDecimal.ZERO);
            } else {
                o.setLecturerProfit(resultSet.getBigDecimal("lecturerProfit"));
            }

            if (StringUtils.isEmpty(resultSet.getBigDecimal("platformProfit"))) {
                o.setPlatformProfit(BigDecimal.ZERO);
            } else {
                o.setPlatformProfit(resultSet.getBigDecimal("platformProfit"));
            }
            return o;
        });
    }


    /**
     * 拼接sql查询条件
     *
     * @param bean
     * @return
     */
    private String joinSql(OrderInfoQO bean) {
        StringBuilder sql = new StringBuilder();
        if (StringUtils.hasText(bean.getRemark())) {
            sql.append(" and remark like '").append(SqlUtil.like(bean.getRemark())).append("'");
        }
        if (!StringUtils.isEmpty(bean.getId())) {
            sql.append(" and id = ").append(bean.getId());
        }
        if (!StringUtils.isEmpty(bean.getOrderNo())) {
            sql.append(" and order_no = ").append(bean.getOrderNo());
        }
        if (StringUtils.hasText(bean.getProductName()) && !StringUtils.isEmpty(bean.getProductId())) {
            sql.append(" and product_name like '").append(SqlUtil.like(bean.getProductName())).append("'");
        }
        if (!StringUtils.isEmpty(bean.getProductId())) {
            sql.append(" and product_id = '").append(bean.getProductId()).append("'");
        }
        if (StringUtils.hasText(bean.getMobile())) {
            sql.append(" and mobile = ").append(bean.getMobile());
        }
        if (StringUtils.hasText(bean.getBeginPayTime())) {
            sql.append(" and pay_time >= '").append(DateUtil.format(DateUtil.parseDate(bean.getBeginPayTime(), "yyyy-MM-dd"), "yyyy-MM-dd HH:mm:ss")).append("'");
        }
        if (StringUtils.hasText(bean.getEndPayTime())) {
            sql.append(" and pay_time < '").append(DateUtil.format(DateUtil.addDate(DateUtil.parseDate(bean.getEndPayTime(), "yyyy-MM-dd"), 1), "yyyy-MM-dd HH:mm:ss")).append("'");
        }
        if (!StringUtils.isEmpty(bean.getLecturerUserNo())) {
            sql.append(" and lecturer_user_no = ").append(bean.getLecturerUserNo());
        }
        if (!StringUtils.isEmpty(bean.getTradeType())) {
            sql.append(" and trade_type = ").append(bean.getTradeType());
        }
        if (!StringUtils.isEmpty(bean.getChannelType())) {
            sql.append(" and channel_type = ").append(bean.getChannelType());
        }
        if (!StringUtils.isEmpty(bean.getLecturerName())) {
            sql.append(" and lecturer_name like '").append(SqlUtil.like(bean.getLecturerName())).append("'");
        }
        // 价格
        if (!StringUtils.isEmpty(bean.getBeginPaidPrice())) {
            sql.append(" and price_paid >= ").append(bean.getBeginPaidPrice());
        }
        // 价格
        if (!StringUtils.isEmpty(bean.getEndPaidPrice())) {
            sql.append(" and price_paid < ").append(bean.getEndPaidPrice());
        }
        if (!StringUtils.isEmpty(bean.getOrderStatus())) {
            sql.append(" and order_status = ").append(bean.getOrderStatus());
        } else {
            // 统计已经成功的订单
            sql.append(" and order_status = ").append(OrderStatusEnum.SUCCESS.getCode());
        }
        return sql.toString();
    }


    public OrderInfoVO getByOrderNo(OrderInfoQO qo) {
        OrderInfo record = dao.getByOrderNo(qo.getOrderNo());
        return BeanUtil.copyProperties(record, OrderInfoVO.class);
    }

    /**
     * 手工下单
     *
     * @param qo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int manualOrder(OrderInfoQO qo) {
        if (qo.getProductId() == null) {
            throw new BaseException("请选择课程");
        }
        if (qo.getUserNo() == null) {
            throw new BaseException("请选择用户");
        }

        // 判断所要购买的课程是否已经购买
        UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();
        userOrderCourseRefQO.setUserNo(qo.getUserNo());
        userOrderCourseRefQO.setCourseId(qo.getProductId());
        UserOrderCourseRefVO userOrderCourseRef = feignUserOrderCourseRef.getByUserNoAndRefId(userOrderCourseRefQO);
        if (!checkUserOrderCourseRef(userOrderCourseRef)) {
            throw new BaseException("已经购买该课程");
        }
        // 课程信息
        CourseVO course = feignCourse.getById(qo.getProductId());
        if (ObjectUtil.isNull(course)) {
            throw new BaseException("找不到课程信息");
        }

        // 获取讲师信息
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(course.getLecturerUserNo());
        if (StringUtils.isEmpty(lecturerVO)) {
            throw new BaseException("根据课程的讲师编号,没找到对应的讲师信息!");
        }
        // 根据用户编号和机构编号查找用户信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(qo.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("根据传入的userNo没找到对应的用户信息!");
        }

        // 插入订单信息
        OrderInfo orderInfo = saveOrderInfoForManual(qo, course, lecturerVO, userExtVO);

        if (ObjectUtil.isNull(userOrderCourseRef)) {
            // 如果原来的课程用户订单关系信息为空,则插入新的用户课程订单信息关系
            return saveUserOrderRefForManual(orderInfo.getOrderNo(), qo, course);
        } else {
            // 不为空则更新
            UserOrderCourseRefQO ref = new UserOrderCourseRefQO();
            ref.setId(userOrderCourseRef.getId());
            ref.setIsPay(IsPayEnum.YES.getCode());
            ref.setExpireTime(DateUtil.addYear(new Date(), 1));
            return feignUserOrderCourseRef.updateById(ref);
        }
    }



    /**
     * 保存用户订单课程关联关系
     *
     * @param orderNo
     * @param qo
     * @return
     */
    private Integer saveUserOrderRefForManual(Long orderNo, OrderInfoQO qo, CourseVO course) {
        UserOrderCourseRefQO userOrderRef = new UserOrderCourseRefQO();
        userOrderRef.setLecturerUserNo(course.getLecturerUserNo());
        userOrderRef.setUserNo(qo.getUserNo());
        userOrderRef.setCourseId(course.getId());
        userOrderRef.setRefId(course.getId());
        userOrderRef.setCourseType(CourseTypeEnum.COURSE.getCode());
        userOrderRef.setCourseCategory(course.getCourseCategory());
        userOrderRef.setIsPay(IsPayEnum.YES.getCode());
        userOrderRef.setIsStudy(IsStudyEnum.YES.getCode());
        userOrderRef.setOrderNo(orderNo);
        userOrderRef.setExpireTime(DateUtil.addYear(new Date(), 1));
        return feignUserOrderCourseRef.save(userOrderRef);
    }

    /**
     * 保存订单信息
     *
     * @param qo
     */
    private OrderInfo saveOrderInfoForManual(OrderInfoQO qo, CourseVO course, LecturerVO lecturerVO, UserExtVO userExtVO) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setLecturerUserNo(course.getLecturerUserNo());
        orderInfo.setLecturerName(lecturerVO.getLecturerName());
        orderInfo.setUserNo(qo.getUserNo());

        orderInfo.setMobile(userExtVO.getMobile());
        orderInfo.setRegisterTime(userExtVO.getGmtCreate());
        orderInfo.setOrderNo(NOUtil.getOrderNo());
        orderInfo.setSerialNumber(NOUtil.getSerialNumber());
        orderInfo.setProductId(course.getId());
        orderInfo.setProductName(course.getCourseName());
        orderInfo.setProductType(course.getCourseCategory());
        orderInfo.setPricePayable(course.getCourseOriginal());
        orderInfo.setPriceDiscount(BigDecimal.ZERO);
        orderInfo.setPricePaid(course.getCourseDiscount());
        orderInfo.setPlatformIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        orderInfo.setTradeType(TradeTypeEnum.OFFLINE.getCode());
        orderInfo.setPayType(PayTypeEnum.MANUAL.getCode());
        orderInfo.setChannelType(ChannelTypeEnum.MANUAL.getCode());
        orderInfo.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
        orderInfo.setIsShowLecturer(IsShowUserEnum.NO.getCode());
        orderInfo.setIsShowUser(IsShowEnum.NO.getCode());
        orderInfo.setRemark(qo.getRemark());
        orderInfo.setPayTime(new Date());
        dao.save(orderInfo);
        return orderInfo;
    }

    /**
     * 判断课程是否已经支付
     */
    private boolean checkUserOrderCourseRef(UserOrderCourseRefVO userOrderCourseRef) {
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            return true;
        } else if (userOrderCourseRef.getExpireTime().before(new Date())) {
            // 判断课程是否在有效期内
            return true;
        }
        return false;
    }


    /**
     * 根据订单号和课程ID获取订单信息
     *
     * @param qo
     * @return
     */
    public OrderInfoVO getByUserNoAndProductId(OrderInfoQO qo) {
        OrderInfo record = dao.getByUserNoAndProductId(qo.getUserNo(), qo.getProductId());
        return BeanUtil.copyProperties(record, OrderInfoVO.class);
    }

}
