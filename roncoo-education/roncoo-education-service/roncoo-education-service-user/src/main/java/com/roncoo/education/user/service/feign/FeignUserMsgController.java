package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserMsg;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.UserMsgVO;
import com.roncoo.education.user.service.feign.biz.FeignUserMsgBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@RestController
public class FeignUserMsgController extends BaseController implements IFeignUserMsg {

	@Autowired
	private FeignUserMsgBiz biz;

	@Override
	public Page<UserMsgVO> listForPage(@RequestBody UserMsgQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserMsgQO qo) {
		return biz.save(qo);
	}

	@Override
	public void batchSaveUserMsgAndMsg(@RequestBody List<UserMsgAndMsgSaveQO> qo) {
		biz.batchSaveUserMsgAndMsg(qo);
	}

	@Override
	public int deleteById(@RequestBody Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserMsgQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public UserMsgVO getById(@RequestBody Long id) {
		return biz.getById(id);
	}

    @Override
    public int push() {
        return biz.push();
    }

    @Override
    public int pushByManual(@RequestBody Long id) {
        return biz.pushByManual(id);
    }

}
