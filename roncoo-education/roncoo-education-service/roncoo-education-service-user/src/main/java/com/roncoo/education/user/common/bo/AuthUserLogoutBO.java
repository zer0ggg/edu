package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户注销
 * </p>
 *
 * @author wujing
 * @date 2020-04-10
 */
@Data
@Accessors(chain = true)
public class AuthUserLogoutBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
