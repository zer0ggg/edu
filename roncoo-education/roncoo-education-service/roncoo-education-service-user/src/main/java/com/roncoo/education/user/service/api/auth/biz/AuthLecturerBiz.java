package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsLiveEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.LecturerViewBO;
import com.roncoo.education.user.common.dto.auth.AuthLecturerViewDTO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAudit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Slf4j
@Component
public class AuthLecturerBiz {

    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private LecturerAuditDao lecturerAuditDao;

    /**
     * 讲师信息查看接口
     *
     * @author wuyun
     */
    public Result<AuthLecturerViewDTO> view(LecturerViewBO lecturerViewBO) {
        if (!lecturerViewBO.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("数据异常");
        }
        if (lecturerViewBO.getLecturerUserNo() == null) {
            return Result.error("讲师编号不能为空");
        }
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(lecturerViewBO.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer)) {
            return Result.error("找不到该讲师");
        }
        AuthLecturerViewDTO dto = BeanUtil.copyProperties(lecturer, AuthLecturerViewDTO.class);
        //判断直播状态
        if (IsLiveEnum.NO.getCode().equals(lecturer.getIsLive())) {
            LecturerAudit lecturerAudit = lecturerAuditDao.getByLecturerUserNo(lecturerViewBO.getLecturerUserNo());
            if (IsLiveEnum.YES.getCode().equals(lecturerAudit.getIsLive())) {
                dto.setLiveAudit(true);
            }
        }
        return Result.success(dto);
    }

}
