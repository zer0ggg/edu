package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户教育信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthUserAccountViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long userNo;
	/**
	 * 总收入
	 */
	@ApiModelProperty(value = "总收入")
	private BigDecimal totalIncome;
	/**
	 * 可提余额
	 */
	@ApiModelProperty(value = "可提余额")
	private BigDecimal enableBalances;
	/**
	 * 银行卡号
	 */
	@ApiModelProperty(value = "银行卡号")
	private String bankCardNo;
	/**
	 * 银行名称
	 */
	@ApiModelProperty(value = "银行名称")
	private String bankName;
	/**
	 * 开户支行
	 */
	@ApiModelProperty(value = "开户支行")
	private String bankBranchName;
	/**
	 * 开户人名称
	 */
	@ApiModelProperty(value = "开户人名称")
	private String bankUserName;
	/**
	 * 银行身份证号
	 */
	@ApiModelProperty(value = "银行身份证号")
	private String bankIdCardNo;
	/**
	 * 银行卡手机号
	 */
	@ApiModelProperty(value = "银行卡手机号")
	private String bankMobile;
}
