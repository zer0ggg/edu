package com.roncoo.education.user.common.pay.util;

import cn.hutool.core.date.DateField;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpUtil;
import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.common.core.enums.WxTradeStateEnum;
import com.roncoo.education.common.core.enums.WxTradeTypeEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class WxPayUtil {

    private static final Logger logger = LoggerFactory.getLogger(WxPayUtil.class);

    private WxPayUtil() {

    }

    /**
     * 微信支付
     *
     * @param payChannel 支付配置
     * @param payBO      请求参数
     * @param tradeType  交易类型
     * @return 微信支付返回
     */
    public static Map<String, String> wxPay(PayChannel payChannel, PayBO payBO, String tradeType) {
        SortedMap<String, String> paramMap = new TreeMap<>();
        paramMap.put("appid", payChannel.getField1());
        paramMap.put("mch_id", payChannel.getPayKey());
        paramMap.put("nonce_str", getNonceStr());
        paramMap.put("sign_type", "MD5");
        paramMap.put("body", payBO.getGoodsName());
        paramMap.put("out_trade_no", payBO.getOrderNo());
        paramMap.put("fee_type", "CNY");
        String totalFee = new DecimalFormat("#0").format(payBO.getOrderAmount().multiply(BigDecimal.valueOf(100L)));
        paramMap.put("total_fee", totalFee);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        paramMap.put("time_start", sdf.format(date));
        paramMap.put("time_expire", sdf.format(DateUtil.offset(date, DateField.MINUTE, 30)));
        paramMap.put("notify_url", String.format(payBO.getNotifyUrl(), payBO.getPayChannelCode()));
        paramMap.put("trade_type", tradeType);
        // 微信小程序支付
        if (WxTradeTypeEnum.JSAPI.getCode().equals(tradeType)) {
            paramMap.put("openid", payBO.getOpenId());
        }
        // 微信app支付
        if (WxTradeTypeEnum.APP.getCode().equals(tradeType) || WxTradeTypeEnum.MWEB.getCode().equals(tradeType)) {
            HttpServletRequest req = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            paramMap.put("spbill_create_ip", req.getHeader("HTTP_X_FORWARDED_FOR"));
        }

        paramMap.put("sign", getSign(paramMap, payChannel.getPaySecret()));

        String reqParam = mapToXml(paramMap);
        logger.info("微信支付请求参数:[{}]", reqParam);
        String resultStr = HttpUtil.post(payChannel.getRequestUrl(), reqParam);
        logger.info("微信支付返回结果:[{}]", resultStr);
        Map<String, String> resultMap = xmlToMap(resultStr);
        String sign = String.valueOf(resultMap.get("sign"));
        SortedMap<String, String> respMap = new TreeMap<>(resultMap);
        String resultSign = getSign(respMap, payChannel.getPaySecret());
        if (resultSign.equals(sign)) {
            logger.info("微信支付返回结果验签成功!");
            // 小程序支付
            String timeStamp = String.valueOf(System.currentTimeMillis()).substring(0, 10);
            if (WxTradeTypeEnum.JSAPI.getCode().equals(tradeType)) {
                SortedMap<String, String> map = new TreeMap<>();
                map.put("appId", resultMap.get("appid"));
                map.put("timeStamp", timeStamp);
                map.put("nonceStr", resultMap.get("nonce_str"));
                map.put("package", "prepay_id=" + resultMap.get("prepay_id"));
                map.put("signType", "MD5");

                resultMap.put("paySign", WxPayUtil.getSign(map, payChannel.getPaySecret()));
                resultMap.put("timeStamp", map.get(timeStamp));
                resultMap.put("nonceStr", resultMap.get("nonce_str"));
                resultMap.put("packageStr", map.get("package"));
                resultMap.put("signType", "MD5");
            }
            if (WxTradeTypeEnum.APP.getCode().equals(tradeType)) {
                SortedMap<String, String> parameterMap = new TreeMap<>();

                parameterMap.put("appid", payChannel.getField1());
                parameterMap.put("partnerid", resultMap.get("mch_id"));
                parameterMap.put("prepayid", resultMap.get("prepay_id"));
                parameterMap.put("package", "Sign=WXPay");
                parameterMap.put("noncestr", getNonceStr());
                parameterMap.put("timestamp", timeStamp);

                resultMap.put("sign", getSign(parameterMap, payChannel.getPaySecret()));
                resultMap.put("appid", parameterMap.get("appid"));
                resultMap.put("partnerid", parameterMap.get("partnerid"));
                resultMap.put("prepayid", parameterMap.get("prepayid"));
                resultMap.put("packageStr", parameterMap.get("package"));
                resultMap.put("nonceStr", parameterMap.get("noncestr"));
                resultMap.put("timeStamp", parameterMap.get("timestamp"));
            }
            return resultMap;
        } else {
            logger.info("微信支付返回结果验签失败!返回签名:[{}],报文信息签名结果:[{}]", sign, resultSign);
            return null;
        }
    }

    public static int orderQuery(String outTradeNo, PayChannel payChannel) {
        SortedMap<String, String> paramMap = new TreeMap<>();
        paramMap.put("appid", payChannel.getField1());
        paramMap.put("mch_id", payChannel.getPayKey());
        paramMap.put("out_trade_no", outTradeNo);
        paramMap.put("nonce_str", getNonceStr());
        paramMap.put("sign_type", "MD5");
        paramMap.put("sign", getSign(paramMap, payChannel.getPaySecret()));

        String reqParam = mapToXml(paramMap);
        logger.info("微信订单查询请求参数:[{}]", reqParam);
        String resultStr = HttpUtil.post(payChannel.getQueryUrl(), reqParam);
        logger.info("微信订单查询请求返回结果:{}", resultStr);

        Map<String, String> resultMap = xmlToMap(resultStr);
        String sign = String.valueOf(resultMap.get("sign"));
        SortedMap<String, String> respMap = new TreeMap<>(resultMap);
        String resultSign = getSign(respMap, payChannel.getPaySecret());
        int orderStatus = OrderStatusEnum.WAIT.getCode();
        if (resultSign.equals(sign)) {
            logger.info("微信订单查询返回结果验签成功!");
            String resultCode = String.valueOf(resultMap.get("result_code"));
            if ("SUCCESS".equals(resultCode)) {
                String tradeState = String.valueOf(resultMap.get("trade_state"));
                logger.info("订单:[{}]返回状态码为:[{}]", outTradeNo, tradeState);
                if (WxTradeStateEnum.SUCCESS.name().equals(tradeState)) {
                    logger.info("订单:[{}]支付成功", outTradeNo);
                    orderStatus = OrderStatusEnum.SUCCESS.getCode();
                } else if (WxTradeStateEnum.REFUND.name().equals(tradeState)) {
                    logger.info("订单:[{}]转入退款", outTradeNo);
                } else if (WxTradeStateEnum.NOTPAY.name().equals(tradeState)) {
                    logger.info("订单:[{}]未支付", outTradeNo);
                } else if (WxTradeStateEnum.CLOSED.name().equals(tradeState)) {
                    logger.info("订单:[{}]已关闭", outTradeNo);
                } else if (WxTradeStateEnum.USERPAYING.name().equals(tradeState)) {
                    logger.info("订单:[{}]用户支付中", outTradeNo);
                } else if (WxTradeStateEnum.PAYERROR.name().equals(tradeState)) {
                    logger.info("订单:[{}]支付失败", outTradeNo);
                    orderStatus = OrderStatusEnum.FAIL.getCode();
                } else {
                    logger.info("返回状态码非法，订单状态未知！");
                }
                return orderStatus;
            }
            return orderStatus;
        } else {
            logger.warn("微信订单查询返回结果验签失败!返回签名:[{}],报文信息签名结果:[{}]", sign, resultSign);
            return orderStatus;
        }
    }

    /**
     * 异步通知结果验签
     *
     * @param notifyBO   通知参数
     * @param payChannel 支付配置
     * @return 校验
     */
    public static PayNotifyDTO checkNotify(PayNotifyBO notifyBO, PayChannel payChannel) {
        Map<String, String> responseMap = xmlToMap(notifyBO.getBody());
        logger.debug("校验属性:{}", responseMap);
        String sign = String.valueOf(responseMap.get("sign"));
        SortedMap<String, String> respMap = new TreeMap<>(responseMap);
        String resultSign = getSign(respMap, payChannel.getPaySecret());
        PayNotifyDTO notifyDTO = new PayNotifyDTO();
        notifyDTO.setOrderNo(responseMap.get("out_trade_no"));
        notifyDTO.setVerify(false);
        notifyDTO.setOrderStatus(OrderStatusEnum.WAIT.getCode());
        if (resultSign.equals(sign)) {
            notifyDTO.setVerify(true);
            logger.info("微信支付回调验签成功！");
            if ("SUCCESS".equals(responseMap.get("result_code"))) {
                notifyDTO.setOrderStatus(OrderStatusEnum.SUCCESS.getCode());
                notifyDTO.setResponseStr("success");
            }
        } else {
            logger.warn("微信通知结果结果验签失败!返回签名:[{}],报文信息签名结果:[{}]", sign, resultSign);
        }
        return notifyDTO;
    }

    private static String getSign(SortedMap<String, String> paramMap, String signKey) {
        if (paramMap == null || paramMap.isEmpty()) {
            logger.warn("微信支付签名参数为空!");
            return "";
        }
        if (!StringUtils.hasText(signKey)) {
            logger.warn("微信支付签名秘钥为空!");
            return "";
        }
        StringBuilder signContentBuilder = new StringBuilder();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            if (!"sign".equals(entry.getKey()) && !ObjectUtils.isEmpty(entry.getValue())) {
                signContentBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        signContentBuilder.append("key=").append(signKey);
        logger.debug("微信支付签名={}", signContentBuilder.toString());
        return DigestUtil.md5Hex(signContentBuilder.toString(), "UTF-8").toUpperCase();
    }

    public static String mapToXml(SortedMap<String, String> paramMap) {
        StringBuilder xmlBuilder = new StringBuilder("<xml>");
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            xmlBuilder.append("<").append(entry.getKey()).append(">").append(entry.getValue()).append("</").append(entry.getKey()).append(">");
        }
        xmlBuilder.append("</xml>");
        return xmlBuilder.toString();
    }

    public static Map<String, String> xmlToMap(String xmlStr) {
        Map<String, String> resultMap = new HashMap<>();

        Document document = XmlUtil.parseXml(xmlStr);
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            String key = nodeList.item(i).getNodeName();
            String val = nodeList.item(i).getTextContent();
            if (!"#text".equals(key)) {
                resultMap.put(key, val);
            }
        }
        return resultMap;
    }

    /**
     * 产生一个31位的随机数
     *
     * @return 随机数
     */
    private static String getNonceStr() {
        return DateUtil.format(new Date(), Constants.DATE.YYYYMMDDHHMMSS) + RandomUtil.randomNumbers(17);
    }

}
