package com.roncoo.education.user.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.bo.LecturerViewBO;
import com.roncoo.education.user.common.dto.LecturerViewDTO;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Slf4j
@Component
public class ApiLecturerBiz {

    @Autowired
    private LecturerDao lecturerDao;

    /**
     * 讲师信息查看接口
     *
     * @author wuyun
     */
    public Result<LecturerViewDTO> view(LecturerViewBO lecturerViewBO) {
        if (lecturerViewBO.getLecturerUserNo() == null) {
            return Result.error("讲师编号不能为空");
        }
        Lecturer lecturer = lecturerDao.getByLecturerUserNoWithBLOBs(lecturerViewBO.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer)) {
            return Result.error("找不到该讲师");
        }
        LecturerViewDTO dto = BeanUtil.copyProperties(lecturer, LecturerViewDTO.class);
        return Result.success(dto);
    }
}
