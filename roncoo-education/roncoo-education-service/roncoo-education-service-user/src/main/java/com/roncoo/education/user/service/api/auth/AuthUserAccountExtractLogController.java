package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountExtractLogPageBO;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountExtractLogSaveBO;
import com.roncoo.education.user.common.dto.auth.AuthUserAccountExtractLogPageDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserAccountExtractLogBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/user/auth/user/account/extract/log")
public class AuthUserAccountExtractLogController {

	@Autowired
	private AuthUserAccountExtractLogBiz biz;

	/**
	 * 用户提现记录分页列出接口
	 *
	 * @param authUserAccountExtractLogPageBO
	 * @author wuyun
	 */
	@ApiOperation(value = "用户提现记录分页列出", notes = "用户提现记录分页列出接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthUserAccountExtractLogPageDTO>> list(@RequestBody AuthUserAccountExtractLogPageBO authUserAccountExtractLogPageBO) {
		return biz.list(authUserAccountExtractLogPageBO);
	}

	/**
	 * 用户申请提现接口
	 *
	 * @param authUserAccountExtractLogSaveBO
	 * @author wuyun
	 */
	@ApiOperation(value = "用户申请提现", notes = "用户申请提现接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthUserAccountExtractLogSaveBO authUserAccountExtractLogSaveBO) {
		return biz.save(authUserAccountExtractLogSaveBO);
	}

}
