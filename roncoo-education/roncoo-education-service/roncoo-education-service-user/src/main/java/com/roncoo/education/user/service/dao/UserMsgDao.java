package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsg;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserMsgExample;

import java.util.List;

public interface UserMsgDao {
	int save(UserMsg record);

	int deleteById(Long id);

	int updateById(UserMsg record);

	/**
	 * 自定义修改
	 * @param record
	 * @param example
	 * @return
	 */
	int updateBySelective(UserMsg record,UserMsgExample example);

	UserMsg getById(Long id);

	Page<UserMsg> listForPage(int pageCurrent, int pageSize, UserMsgExample example);

	int deleteByMsgId(Long id);

	/**
	 * 根据用户编号 设置已阅读
	 * @param userNo
	 * @return
	 */
	int readAllByUserNo(Long userNo);

	/**
	 * 获得学员未读消息总条数
	 *
	 * @param userNo
	 * @param isRead
	 * @return
	 */
	int countByUserNoAndIsReadAndIsSend(Long userNo, Integer isRead,Integer isSend);

	/**
	 * 根据消息id获得集合
	 * @param msgId
	 * @return
	 */
    List<UserMsg> listByMsgId(Long msgId);
}
