package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignPayChannel;
import com.roncoo.education.user.feign.qo.PayChannelQO;
import com.roncoo.education.user.feign.vo.PayChannelVO;
import com.roncoo.education.user.service.feign.biz.FeignPayChannelBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 银行渠道信息表
 *
 * @author wujing
 * @date 2020-03-25
 */
@RestController
public class FeignPayChannelController extends BaseController implements IFeignPayChannel{

    @Autowired
    private FeignPayChannelBiz biz;

	@Override
	public Page<PayChannelVO> listForPage(@RequestBody PayChannelQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody PayChannelQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody PayChannelQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public PayChannelVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
