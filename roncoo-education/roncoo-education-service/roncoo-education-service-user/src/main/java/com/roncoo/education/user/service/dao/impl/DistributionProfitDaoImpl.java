package com.roncoo.education.user.service.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.DistributionProfitDao;
import com.roncoo.education.user.service.dao.impl.mapper.DistributionProfitMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample;

import java.util.List;

/**
 * 代理分润记录 服务实现类
 *
 * @author wujing
 * @date 2021-01-05
 */
@Repository
public class DistributionProfitDaoImpl implements DistributionProfitDao {

    @Autowired
    private DistributionProfitMapper mapper;

    @Override
    public int save(DistributionProfit record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(DistributionProfit record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public DistributionProfit getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<DistributionProfit> listForPage(int pageCurrent, int pageSize, DistributionProfitExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }
}
