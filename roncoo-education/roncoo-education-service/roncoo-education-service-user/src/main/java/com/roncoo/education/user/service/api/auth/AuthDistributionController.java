package com.roncoo.education.user.service.api.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import com.roncoo.education.user.service.api.auth.biz.AuthDistributionBiz;

/**
 * 分销信息 UserApi接口
 *
 * @author wujing
 * @date 2020-12-29
 */
@Api(tags = "API-AUTH-分销信息")
@RestController
@RequestMapping("/user/auth/distribution")
public class AuthDistributionController {

    @Autowired
    private AuthDistributionBiz biz;

}
