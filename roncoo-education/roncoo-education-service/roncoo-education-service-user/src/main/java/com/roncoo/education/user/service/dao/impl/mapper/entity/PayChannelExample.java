package com.roncoo.education.user.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayChannelExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public PayChannelExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIsNull() {
            addCriterion("pay_channel_code is null");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIsNotNull() {
            addCriterion("pay_channel_code is not null");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeEqualTo(String value) {
            addCriterion("pay_channel_code =", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotEqualTo(String value) {
            addCriterion("pay_channel_code <>", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeGreaterThan(String value) {
            addCriterion("pay_channel_code >", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("pay_channel_code >=", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLessThan(String value) {
            addCriterion("pay_channel_code <", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLessThanOrEqualTo(String value) {
            addCriterion("pay_channel_code <=", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeLike(String value) {
            addCriterion("pay_channel_code like", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotLike(String value) {
            addCriterion("pay_channel_code not like", value, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeIn(List<String> values) {
            addCriterion("pay_channel_code in", values, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotIn(List<String> values) {
            addCriterion("pay_channel_code not in", values, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeBetween(String value1, String value2) {
            addCriterion("pay_channel_code between", value1, value2, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelCodeNotBetween(String value1, String value2) {
            addCriterion("pay_channel_code not between", value1, value2, "payChannelCode");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIsNull() {
            addCriterion("pay_channel_name is null");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIsNotNull() {
            addCriterion("pay_channel_name is not null");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameEqualTo(String value) {
            addCriterion("pay_channel_name =", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotEqualTo(String value) {
            addCriterion("pay_channel_name <>", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameGreaterThan(String value) {
            addCriterion("pay_channel_name >", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameGreaterThanOrEqualTo(String value) {
            addCriterion("pay_channel_name >=", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLessThan(String value) {
            addCriterion("pay_channel_name <", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLessThanOrEqualTo(String value) {
            addCriterion("pay_channel_name <=", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameLike(String value) {
            addCriterion("pay_channel_name like", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotLike(String value) {
            addCriterion("pay_channel_name not like", value, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameIn(List<String> values) {
            addCriterion("pay_channel_name in", values, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotIn(List<String> values) {
            addCriterion("pay_channel_name not in", values, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameBetween(String value1, String value2) {
            addCriterion("pay_channel_name between", value1, value2, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayChannelNameNotBetween(String value1, String value2) {
            addCriterion("pay_channel_name not between", value1, value2, "payChannelName");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeIsNull() {
            addCriterion("pay_object_code is null");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeIsNotNull() {
            addCriterion("pay_object_code is not null");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeEqualTo(String value) {
            addCriterion("pay_object_code =", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeNotEqualTo(String value) {
            addCriterion("pay_object_code <>", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeGreaterThan(String value) {
            addCriterion("pay_object_code >", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeGreaterThanOrEqualTo(String value) {
            addCriterion("pay_object_code >=", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeLessThan(String value) {
            addCriterion("pay_object_code <", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeLessThanOrEqualTo(String value) {
            addCriterion("pay_object_code <=", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeLike(String value) {
            addCriterion("pay_object_code like", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeNotLike(String value) {
            addCriterion("pay_object_code not like", value, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeIn(List<String> values) {
            addCriterion("pay_object_code in", values, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeNotIn(List<String> values) {
            addCriterion("pay_object_code not in", values, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeBetween(String value1, String value2) {
            addCriterion("pay_object_code between", value1, value2, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectCodeNotBetween(String value1, String value2) {
            addCriterion("pay_object_code not between", value1, value2, "payObjectCode");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameIsNull() {
            addCriterion("pay_object_name is null");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameIsNotNull() {
            addCriterion("pay_object_name is not null");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameEqualTo(String value) {
            addCriterion("pay_object_name =", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameNotEqualTo(String value) {
            addCriterion("pay_object_name <>", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameGreaterThan(String value) {
            addCriterion("pay_object_name >", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameGreaterThanOrEqualTo(String value) {
            addCriterion("pay_object_name >=", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameLessThan(String value) {
            addCriterion("pay_object_name <", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameLessThanOrEqualTo(String value) {
            addCriterion("pay_object_name <=", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameLike(String value) {
            addCriterion("pay_object_name like", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameNotLike(String value) {
            addCriterion("pay_object_name not like", value, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameIn(List<String> values) {
            addCriterion("pay_object_name in", values, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameNotIn(List<String> values) {
            addCriterion("pay_object_name not in", values, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameBetween(String value1, String value2) {
            addCriterion("pay_object_name between", value1, value2, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayObjectNameNotBetween(String value1, String value2) {
            addCriterion("pay_object_name not between", value1, value2, "payObjectName");
            return (Criteria) this;
        }

        public Criteria andPayKeyIsNull() {
            addCriterion("pay_key is null");
            return (Criteria) this;
        }

        public Criteria andPayKeyIsNotNull() {
            addCriterion("pay_key is not null");
            return (Criteria) this;
        }

        public Criteria andPayKeyEqualTo(String value) {
            addCriterion("pay_key =", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyNotEqualTo(String value) {
            addCriterion("pay_key <>", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyGreaterThan(String value) {
            addCriterion("pay_key >", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyGreaterThanOrEqualTo(String value) {
            addCriterion("pay_key >=", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyLessThan(String value) {
            addCriterion("pay_key <", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyLessThanOrEqualTo(String value) {
            addCriterion("pay_key <=", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyLike(String value) {
            addCriterion("pay_key like", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyNotLike(String value) {
            addCriterion("pay_key not like", value, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyIn(List<String> values) {
            addCriterion("pay_key in", values, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyNotIn(List<String> values) {
            addCriterion("pay_key not in", values, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyBetween(String value1, String value2) {
            addCriterion("pay_key between", value1, value2, "payKey");
            return (Criteria) this;
        }

        public Criteria andPayKeyNotBetween(String value1, String value2) {
            addCriterion("pay_key not between", value1, value2, "payKey");
            return (Criteria) this;
        }

        public Criteria andPaySecretIsNull() {
            addCriterion("pay_secret is null");
            return (Criteria) this;
        }

        public Criteria andPaySecretIsNotNull() {
            addCriterion("pay_secret is not null");
            return (Criteria) this;
        }

        public Criteria andPaySecretEqualTo(String value) {
            addCriterion("pay_secret =", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretNotEqualTo(String value) {
            addCriterion("pay_secret <>", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretGreaterThan(String value) {
            addCriterion("pay_secret >", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretGreaterThanOrEqualTo(String value) {
            addCriterion("pay_secret >=", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretLessThan(String value) {
            addCriterion("pay_secret <", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretLessThanOrEqualTo(String value) {
            addCriterion("pay_secret <=", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretLike(String value) {
            addCriterion("pay_secret like", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretNotLike(String value) {
            addCriterion("pay_secret not like", value, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretIn(List<String> values) {
            addCriterion("pay_secret in", values, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretNotIn(List<String> values) {
            addCriterion("pay_secret not in", values, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretBetween(String value1, String value2) {
            addCriterion("pay_secret between", value1, value2, "paySecret");
            return (Criteria) this;
        }

        public Criteria andPaySecretNotBetween(String value1, String value2) {
            addCriterion("pay_secret not between", value1, value2, "paySecret");
            return (Criteria) this;
        }

        public Criteria andRequestUrlIsNull() {
            addCriterion("request_url is null");
            return (Criteria) this;
        }

        public Criteria andRequestUrlIsNotNull() {
            addCriterion("request_url is not null");
            return (Criteria) this;
        }

        public Criteria andRequestUrlEqualTo(String value) {
            addCriterion("request_url =", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlNotEqualTo(String value) {
            addCriterion("request_url <>", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlGreaterThan(String value) {
            addCriterion("request_url >", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlGreaterThanOrEqualTo(String value) {
            addCriterion("request_url >=", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlLessThan(String value) {
            addCriterion("request_url <", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlLessThanOrEqualTo(String value) {
            addCriterion("request_url <=", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlLike(String value) {
            addCriterion("request_url like", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlNotLike(String value) {
            addCriterion("request_url not like", value, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlIn(List<String> values) {
            addCriterion("request_url in", values, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlNotIn(List<String> values) {
            addCriterion("request_url not in", values, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlBetween(String value1, String value2) {
            addCriterion("request_url between", value1, value2, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andRequestUrlNotBetween(String value1, String value2) {
            addCriterion("request_url not between", value1, value2, "requestUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlIsNull() {
            addCriterion("query_url is null");
            return (Criteria) this;
        }

        public Criteria andQueryUrlIsNotNull() {
            addCriterion("query_url is not null");
            return (Criteria) this;
        }

        public Criteria andQueryUrlEqualTo(String value) {
            addCriterion("query_url =", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlNotEqualTo(String value) {
            addCriterion("query_url <>", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlGreaterThan(String value) {
            addCriterion("query_url >", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlGreaterThanOrEqualTo(String value) {
            addCriterion("query_url >=", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlLessThan(String value) {
            addCriterion("query_url <", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlLessThanOrEqualTo(String value) {
            addCriterion("query_url <=", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlLike(String value) {
            addCriterion("query_url like", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlNotLike(String value) {
            addCriterion("query_url not like", value, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlIn(List<String> values) {
            addCriterion("query_url in", values, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlNotIn(List<String> values) {
            addCriterion("query_url not in", values, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlBetween(String value1, String value2) {
            addCriterion("query_url between", value1, value2, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andQueryUrlNotBetween(String value1, String value2) {
            addCriterion("query_url not between", value1, value2, "queryUrl");
            return (Criteria) this;
        }

        public Criteria andField1IsNull() {
            addCriterion("field1 is null");
            return (Criteria) this;
        }

        public Criteria andField1IsNotNull() {
            addCriterion("field1 is not null");
            return (Criteria) this;
        }

        public Criteria andField1EqualTo(String value) {
            addCriterion("field1 =", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotEqualTo(String value) {
            addCriterion("field1 <>", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThan(String value) {
            addCriterion("field1 >", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1GreaterThanOrEqualTo(String value) {
            addCriterion("field1 >=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThan(String value) {
            addCriterion("field1 <", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1LessThanOrEqualTo(String value) {
            addCriterion("field1 <=", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Like(String value) {
            addCriterion("field1 like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotLike(String value) {
            addCriterion("field1 not like", value, "field1");
            return (Criteria) this;
        }

        public Criteria andField1In(List<String> values) {
            addCriterion("field1 in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotIn(List<String> values) {
            addCriterion("field1 not in", values, "field1");
            return (Criteria) this;
        }

        public Criteria andField1Between(String value1, String value2) {
            addCriterion("field1 between", value1, value2, "field1");
            return (Criteria) this;
        }

        public Criteria andField1NotBetween(String value1, String value2) {
            addCriterion("field1 not between", value1, value2, "field1");
            return (Criteria) this;
        }

        public Criteria andField2IsNull() {
            addCriterion("field2 is null");
            return (Criteria) this;
        }

        public Criteria andField2IsNotNull() {
            addCriterion("field2 is not null");
            return (Criteria) this;
        }

        public Criteria andField2EqualTo(String value) {
            addCriterion("field2 =", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotEqualTo(String value) {
            addCriterion("field2 <>", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThan(String value) {
            addCriterion("field2 >", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2GreaterThanOrEqualTo(String value) {
            addCriterion("field2 >=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThan(String value) {
            addCriterion("field2 <", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2LessThanOrEqualTo(String value) {
            addCriterion("field2 <=", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Like(String value) {
            addCriterion("field2 like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotLike(String value) {
            addCriterion("field2 not like", value, "field2");
            return (Criteria) this;
        }

        public Criteria andField2In(List<String> values) {
            addCriterion("field2 in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotIn(List<String> values) {
            addCriterion("field2 not in", values, "field2");
            return (Criteria) this;
        }

        public Criteria andField2Between(String value1, String value2) {
            addCriterion("field2 between", value1, value2, "field2");
            return (Criteria) this;
        }

        public Criteria andField2NotBetween(String value1, String value2) {
            addCriterion("field2 not between", value1, value2, "field2");
            return (Criteria) this;
        }

        public Criteria andField3IsNull() {
            addCriterion("field3 is null");
            return (Criteria) this;
        }

        public Criteria andField3IsNotNull() {
            addCriterion("field3 is not null");
            return (Criteria) this;
        }

        public Criteria andField3EqualTo(String value) {
            addCriterion("field3 =", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotEqualTo(String value) {
            addCriterion("field3 <>", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThan(String value) {
            addCriterion("field3 >", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3GreaterThanOrEqualTo(String value) {
            addCriterion("field3 >=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThan(String value) {
            addCriterion("field3 <", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3LessThanOrEqualTo(String value) {
            addCriterion("field3 <=", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Like(String value) {
            addCriterion("field3 like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotLike(String value) {
            addCriterion("field3 not like", value, "field3");
            return (Criteria) this;
        }

        public Criteria andField3In(List<String> values) {
            addCriterion("field3 in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotIn(List<String> values) {
            addCriterion("field3 not in", values, "field3");
            return (Criteria) this;
        }

        public Criteria andField3Between(String value1, String value2) {
            addCriterion("field3 between", value1, value2, "field3");
            return (Criteria) this;
        }

        public Criteria andField3NotBetween(String value1, String value2) {
            addCriterion("field3 not between", value1, value2, "field3");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}