package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 讲师信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "LecturerEditREQ", description = "讲师信息修改")
public class LecturerEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "讲师手机")
    private String lecturerMobile;

    @ApiModelProperty(value = "讲师邮箱")
    private String lecturerEmail;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "简介")
    private String introduce;

    @ApiModelProperty(value = "是否拥有直播功能（0：否，1: 是）")
    private Integer isLive;

    @ApiModelProperty(value = "直播状态(1:未开始;2:直播中;3:直播结束)")
    private Integer liveStatus;

    @ApiModelProperty(value = "头像")
    private String headImgUrl;
}
