package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Chosen;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample;

/**
 * 分销信息 服务类
 *
 * @author wujing
 * @date 2020-06-17
 */
public interface ChosenDao {

    /**
    * 保存分销信息
    *
    * @param record 分销信息
    * @return 影响记录数
    */
    int save(Chosen record);

    /**
    * 根据ID删除分销信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 分销信息
    * @return 影响记录数
    */
    int updateById(Chosen record);

    /**
    * 根据ID获取分销信息
    *
    * @param id 主键ID
    * @return 分销信息
    */
    Chosen getById(Long id);

    /**
    * 分销信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<Chosen> listForPage(int pageCurrent, int pageSize, ChosenExample example);


    /**
     * 根据课程ID获取分销信息
     *
     * @param courseId 课程ID
     * @return 分销信息
     */
    Chosen getByCourseId(Long courseId);

}
