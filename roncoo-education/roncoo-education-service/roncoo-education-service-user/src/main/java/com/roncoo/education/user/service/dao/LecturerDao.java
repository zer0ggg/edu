package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerExample;

import java.util.List;

public interface LecturerDao {
    int save(Lecturer record);

    int deleteById(Long id);

    int updateById(Lecturer record);

    Lecturer getById(Long id);

    Page<Lecturer> listForPage(int pageCurrent, int pageSize, LecturerExample example);

    /**
     * 根据讲师编号查找讲师信息
     *
     * @param lecturerUserNo
     * @return
     * @author WY
     */
    Lecturer getByLecturerUserNo(Long lecturerUserNo);

    /**
     * 根据讲师编号查找讲师信息,含大字段
     *
     * @param lecturerUserNo
     * @return
     * @author WY
     */
    Lecturer getByLecturerUserNoWithBLOBs(Long lecturerUserNo);

    /**
     * 根据讲师名称查找讲师信息
     *
     * @param LecturerName
     * @return
     * @author WY
     */
    List<Lecturer> getByLecturerName(String LecturerName);

    List<Lecturer> listByStatusId(Integer statusId);

    /**
     * 根据讲师编号、状态查找讲师信息
     *
     * @param lecturerUserNo
     * @param statusId
     * @return
     * @author WY
     */
    Lecturer getByLecturerUserNoAndStatusId(Long lecturerUserNo, Integer statusId);

    /**
     * 根据讲师手机号获取讲师信息
     *
     * @param lecturerMobile
     * @return
     */
    Lecturer getByLecturerMobile(String lecturerMobile);
    /**
     * 根据讲师编号集合获取讲师信息
     * @param lecturerUserNos
     * @return
     */
    List<Lecturer> listByLecturerUserNos(List<Long> lecturerUserNos);
}
