package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.auth.AuthUserRecommendedPageBO;
import com.roncoo.education.user.common.dto.auth.AuthUserRecommendedPageDTO;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserRecommendedDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommended;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserRecommendedExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 用户推荐信息
 *
 * @author wujing
 */
@Component
public class AuthUserRecommendedBiz extends BaseBiz {

    @Autowired
    private UserRecommendedDao dao;
    @Autowired
    private UserExtDao userExtDao;

    public Result<Page<AuthUserRecommendedPageDTO>> listForPage(AuthUserRecommendedPageBO bo) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("用户禁用");
        }
        if (StringUtils.isEmpty(userExt.getReferralCode())) {
            return Result.error("该用户未成为推荐人");
        }
        UserRecommendedExample example = new UserRecommendedExample();
        UserRecommendedExample.Criteria criteria = example.createCriteria();
        criteria.andReferralCodeEqualTo(userExt.getReferralCode());
        Page<UserRecommended> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserRecommendedPageDTO> listForPage = PageUtil.transform(page, AuthUserRecommendedPageDTO.class);
        for (AuthUserRecommendedPageDTO dto : listForPage.getList()) {
            UserExt user = userExtDao.getByUserNo(dto.getUserNo());
            if (ObjectUtil.isNull(user)) {
                return Result.error("用户不存在");
            }
            if (!StatusIdEnum.YES.getCode().equals(user.getStatusId())) {
                return Result.error("用户禁用");
            }
            if (StringUtils.isEmpty(user.getNickname())) {
                dto.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
            } else {
                dto.setNickname(user.getNickname());
            }
        }
        return Result.success(listForPage);
    }

}
