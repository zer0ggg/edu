/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.service.api;

import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.user.service.api.biz.CallbackOrderBiz;
import org.apache.http.util.ByteArrayBuffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 统一异步回调
 *
 * @author Quanf
 */
@RestController
@RequestMapping(value = "/callback/order")
public class CallbackOrderController extends BaseController {

    @Autowired
    private CallbackOrderBiz biz;

    @RequestMapping(value = "/notify/{payChannelCode}", method = {RequestMethod.POST, RequestMethod.GET})
    public String notify(@PathVariable(name = "payChannelCode") String payChannelCode) {
        return biz.callback(getNotifyParams(), payChannelCode);
    }

    /**
     * 获取通知参数
     *
     * @return 通知参数
     */
    private String getNotifyParams() {
        HttpServletRequest httpServletRequest = getRequest();
        Map<String, String[]> paramMap = httpServletRequest.getParameterMap();

        if (paramMap == null || paramMap.isEmpty()) {
            // 获取参数为空，从io流中获取
            try {
                InputStream is = httpServletRequest.getInputStream();
                byte[] bytes = new byte[4096];
                int len;

                ByteArrayBuffer resultBuf = new ByteArrayBuffer(4096);
                while ((len = is.read(bytes)) >= 0) {
                    resultBuf.append(bytes, 0, len);
                }
                return new String(resultBuf.toByteArray(), StandardCharsets.UTF_8);
            } catch (Exception e) {
                logger.error("异步通知获取参数失败！", e);
                return null;
            }
        }

        // 获取参数不为空，做转换
        Set<Entry<String, String[]>> entrySet = paramMap.entrySet();
        JSONObject bodyJson = new JSONObject();
        for (Entry<String, String[]> entry : entrySet) {
            if (entry.getValue() == null || entry.getValue().length <= 0) {
                bodyJson.put(entry.getKey(), "");
            } else {
                StringBuilder valueBody = new StringBuilder();
                String[] values = entry.getValue();
                for (String value : values) {
                    valueBody.append(value).append(",");
                }
                bodyJson.put(entry.getKey(), valueBody.deleteCharAt(valueBody.length() - 1).toString());
            }
        }
        return bodyJson.toJSONString();
    }

}
