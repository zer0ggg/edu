package com.roncoo.education.user.service.api.auth.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.user.service.dao.DistributionDao;

/**
 * 分销信息
 *
 * @author wujing
 */
@Component
public class AuthDistributionBiz extends BaseBiz {

    @Autowired
    private DistributionDao dao;

}
