package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ExtractStatusEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountExtractLogPageBO;
import com.roncoo.education.user.common.bo.auth.AuthUserAccountExtractLogSaveBO;
import com.roncoo.education.user.common.dto.auth.AuthUserAccountExtractLogPageDTO;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserAccountExtractLogDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@Component
public class AuthUserAccountExtractLogBiz extends BaseBiz {

    @Autowired
    private UserAccountExtractLogDao userAccountExtractLogDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private UserExtDao userExtDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 讲师提现记录分页列出接口
     *
     * @param bo
     */
    public Result<Page<AuthUserAccountExtractLogPageDTO>> list(AuthUserAccountExtractLogPageBO bo) {
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        example.setOrderByClause(" id desc ");
        Page<UserAccountExtractLog> page = userAccountExtractLogDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserAccountExtractLogPageDTO> dtoPage = PageUtil.transform(page, AuthUserAccountExtractLogPageDTO.class);
        for (AuthUserAccountExtractLogPageDTO dto : dtoPage.getList()) {
            if (!StringUtils.isEmpty(dto.getBankCardNo())) {
                dto.setBankCardNo(dto.getBankCardNo().substring(0, 6) + "****" + dto.getBankCardNo().substring(12));
            }
        }
        return Result.success(dtoPage);
    }

    /**
     * 申请提现接口
     *
     * @param bo
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthUserAccountExtractLogSaveBO bo) {
        if (!StringUtils.hasText(bo.getSmsCode())) {
            return Result.error("输入的验证码不能为空!");
        }
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("该用户不存在");
        }
        // 查询用户账户信息
        UserAccount userAccount = userAccountDao.getByUserNoAndStatusId(userExt.getUserNo(), StatusIdEnum.YES.getCode());
        if (ObjectUtil.isNull(userAccount)) {
            return Result.error("该账户不存在");
        }
        if (userAccount.getEnableBalances().compareTo(bo.getExtractMoney()) < 0) {
            return Result.error("账户余额不足");
        }
        if (StringUtils.isEmpty(userAccount.getBankCardNo())) {
            return Result.error("银行卡未绑定");
        }
        if (StringUtils.isEmpty(userAccount.getBankMobile())) {
            return Result.error("银行手机号未绑定");
        } else {
            if (!userAccount.getBankMobile().equals(bo.getBankMobile())) {
                return Result.error("银行手机号不一致");
            }
        }
        String redisSmsCode = myRedisTemplate.get(Constants.SMS_CODE + userAccount.getBankMobile());
        if (StringUtils.isEmpty(redisSmsCode)) {
            return Result.error("验证码已失效!");
        }
        String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
        if (sign.equals(userAccount.getSign())) {
            // 先存日志，再操作
            UserAccountExtractLog userAccountExtractLog = BeanUtil.copyProperties(userAccount, UserAccountExtractLog.class);
            userAccountExtractLog.setGmtCreate(null);
            userAccountExtractLog.setPhone(userAccount.getBankMobile());
            userAccountExtractLog.setExtractStatus(ExtractStatusEnum.APPLICATION.getCode());
            userAccountExtractLog.setExtractMoney(bo.getExtractMoney());
            userAccountExtractLog.setUserIncome(bo.getExtractMoney());
            userAccountExtractLogDao.save(userAccountExtractLog);

            // 减少账户可提现金额,增加账号冻结金额,生成新的签名防篡改
            // 账户可提现金额减去申请的金额
            userAccount.setEnableBalances(userAccount.getEnableBalances().subtract(bo.getExtractMoney()));
            // 账号冻结金额加上申请的金额
            userAccount.setFreezeBalances(userAccount.getFreezeBalances().add(bo.getExtractMoney()));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            return Result.success(userAccountDao.updateById(userAccount));
        } else {
            logger.error("签名为：{}，{}", sign, userAccount.getSign());
            return Result.error("账户异常，请联系客服");
        }

    }

}
