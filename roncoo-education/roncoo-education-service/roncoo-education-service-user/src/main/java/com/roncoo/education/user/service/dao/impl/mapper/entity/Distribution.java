package com.roncoo.education.user.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Distribution implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private Long parentId;

    private Integer floor;

    private Integer isAllowInvitation;

    private BigDecimal spreadProfit;

    private BigDecimal inviteProfit;

    private BigDecimal earnings;

    private String remark;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getIsAllowInvitation() {
        return isAllowInvitation;
    }

    public void setIsAllowInvitation(Integer isAllowInvitation) {
        this.isAllowInvitation = isAllowInvitation;
    }

    public BigDecimal getSpreadProfit() {
        return spreadProfit;
    }

    public void setSpreadProfit(BigDecimal spreadProfit) {
        this.spreadProfit = spreadProfit;
    }

    public BigDecimal getInviteProfit() {
        return inviteProfit;
    }

    public void setInviteProfit(BigDecimal inviteProfit) {
        this.inviteProfit = inviteProfit;
    }

    public BigDecimal getEarnings() {
        return earnings;
    }

    public void setEarnings(BigDecimal earnings) {
        this.earnings = earnings;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", parentId=").append(parentId);
        sb.append(", floor=").append(floor);
        sb.append(", isAllowInvitation=").append(isAllowInvitation);
        sb.append(", spreadProfit=").append(spreadProfit);
        sb.append(", inviteProfit=").append(inviteProfit);
        sb.append(", earnings=").append(earnings);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}