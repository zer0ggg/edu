package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserLecturerAttentionBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 讲师编号
	 */
	private Long lecturerUserNo;
}
