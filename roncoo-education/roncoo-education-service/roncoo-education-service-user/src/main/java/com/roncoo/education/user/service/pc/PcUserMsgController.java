package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.UserMsgEditREQ;
import com.roncoo.education.user.common.req.UserMsgListREQ;
import com.roncoo.education.user.common.req.UserMsgSaveREQ;
import com.roncoo.education.user.common.resp.UserMsgListRESP;
import com.roncoo.education.user.common.resp.UserMsgViewRESP;
import com.roncoo.education.user.service.pc.biz.PcUserMsgBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 站内信用户记录 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/msg")
@Api(value = "user-站内信用户记录", tags = {"user-站内信用户记录"})
public class PcUserMsgController {

    @Autowired
    private PcUserMsgBiz biz;

    @ApiOperation(value = "站内信用户记录列表", notes = "站内信用户记录列表")
    @PostMapping(value = "/list")
    public Result<Page<UserMsgListRESP>> list(@RequestBody UserMsgListREQ userMsgListREQ) {
        return biz.list(userMsgListREQ);
    }

    @ApiOperation(value = "站内信用户记录添加", notes = "站内信用户记录添加")
    @PostMapping(value = "/save")
    @SysLog(value = "站内信用户记录添加")
    public Result<String> save(@RequestBody UserMsgSaveREQ userMsgSaveREQ) {
        return biz.save(userMsgSaveREQ);
    }

    @ApiOperation(value = "站内信用户记录查看", notes = "站内信用户记录查看")
    @GetMapping(value = "/view")
    public Result<UserMsgViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "站内信用户记录修改", notes = "站内信用户记录修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "站内信用户记录修改")
    public Result<String> edit(@RequestBody UserMsgEditREQ userMsgEditREQ) {
        return biz.edit(userMsgEditREQ);
    }

    @ApiOperation(value = "站内信用户记录删除", notes = "站内信用户记录删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "站内信用户记录删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
