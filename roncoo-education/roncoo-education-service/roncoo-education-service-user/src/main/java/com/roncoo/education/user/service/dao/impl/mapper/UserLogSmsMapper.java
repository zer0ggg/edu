package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSms;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserLogSmsMapper {
    int countByExample(UserLogSmsExample example);

    int deleteByExample(UserLogSmsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserLogSms record);

    int insertSelective(UserLogSms record);

    List<UserLogSms> selectByExample(UserLogSmsExample example);

    UserLogSms selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserLogSms record, @Param("example") UserLogSmsExample example);

    int updateByExample(@Param("record") UserLogSms record, @Param("example") UserLogSmsExample example);

    int updateByPrimaryKeySelective(UserLogSms record);

    int updateByPrimaryKey(UserLogSms record);
}
