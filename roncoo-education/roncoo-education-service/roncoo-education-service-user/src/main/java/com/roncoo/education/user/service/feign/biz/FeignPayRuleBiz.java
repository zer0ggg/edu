package com.roncoo.education.user.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.PayRuleQO;
import com.roncoo.education.user.feign.vo.PayRuleVO;
import com.roncoo.education.user.service.dao.PayRuleDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRuleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 支付路由表
 *
 * @author wujing
 */
@Component
public class FeignPayRuleBiz extends BaseBiz {

    @Autowired
    private PayRuleDao dao;

	public Page<PayRuleVO> listForPage(PayRuleQO qo) {
	    PayRuleExample example = new PayRuleExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<PayRule> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, PayRuleVO.class);
	}

	public int save(PayRuleQO qo) {
		PayRule record = BeanUtil.copyProperties(qo, PayRule.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public PayRuleVO getById(Long id) {
		PayRule record = dao.getById(id);
		return BeanUtil.copyProperties(record, PayRuleVO.class);
	}

	public int updateById(PayRuleQO qo) {
		PayRule record = BeanUtil.copyProperties(qo, PayRule.class);
		return dao.updateById(record);
	}

}
