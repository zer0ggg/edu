package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.MsgTemplateDao;
import com.roncoo.education.user.service.dao.impl.mapper.MsgTemplateMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplate;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgTemplateExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MsgTemplateDaoImpl implements MsgTemplateDao {
    @Autowired
    private MsgTemplateMapper msgTemplateMapper;

    @Override
    public int save(MsgTemplate record) {
        record.setId(IdWorker.getId());
        return this.msgTemplateMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.msgTemplateMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(MsgTemplate record) {
        return this.msgTemplateMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public MsgTemplate getById(Long id) {
        return this.msgTemplateMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<MsgTemplate> listForPage(int pageCurrent, int pageSize, MsgTemplateExample example) {
        int count = this.msgTemplateMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<MsgTemplate>(count, totalPage, pageCurrent, pageSize, this.msgTemplateMapper.selectByExample(example));
    }


    @Override
    public List<MsgTemplate> listByMsgTemplateType(Integer msgTemplateType) {
        MsgTemplateExample example = new MsgTemplateExample();
        MsgTemplateExample.Criteria c = example.createCriteria();
        c.andMsgTemplateTypeEqualTo(msgTemplateType);
        return this.msgTemplateMapper.selectByExample(example);
    }

    @Override
    public MsgTemplate getByTemplateCode(String templateCode) {
        MsgTemplateExample example = new MsgTemplateExample();
        MsgTemplateExample.Criteria c = example.createCriteria();
        c.andTemplateCodeEqualTo(templateCode);
        List<MsgTemplate> list = this.msgTemplateMapper.selectByExample(example);
        if (list.isEmpty() || list.size() < 1) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public MsgTemplate getByMsgTemplateTypeAndIsUseTemplate(Integer msgTemplateType, Integer isUseTemplate) {
        MsgTemplateExample example = new MsgTemplateExample();
        MsgTemplateExample.Criteria c = example.createCriteria();
        c.andMsgTemplateTypeEqualTo(msgTemplateType);
        c.andIsUseTemplateEqualTo(isUseTemplate);
        List<MsgTemplate> list = this.msgTemplateMapper.selectByExampleWithBLOBs(example);
        if (list.isEmpty() || list.size() < 1) {
            return null;
        }
        return list.get(0);
    }
}
