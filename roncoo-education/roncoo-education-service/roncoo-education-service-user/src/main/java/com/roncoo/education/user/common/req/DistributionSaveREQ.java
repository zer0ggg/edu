package com.roncoo.education.user.common.req;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 分销信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="DistributionSaveREQ", description="分销信息添加")
public class DistributionSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户编号")
    @NotNull(message = "用户编号不能为空")
    private Long userNo;

    @ApiModelProperty(value = "父ID")
    private Long parentId = 0L;

    @ApiModelProperty(value = "是否允许推广下级")
    @NotNull(message = "请选择是否允许推广下级")
    private Integer isAllowInvitation;

    @ApiModelProperty(value = "推广比例")
    @NotNull(message = "请输入推广比例")
    private BigDecimal spreadProfit;

    @ApiModelProperty(value = "邀请比例")
    @NotNull(message = "请输入邀请比例")
    private BigDecimal inviteProfit;

    @ApiModelProperty(value = "备注")
    private String remark;
}
