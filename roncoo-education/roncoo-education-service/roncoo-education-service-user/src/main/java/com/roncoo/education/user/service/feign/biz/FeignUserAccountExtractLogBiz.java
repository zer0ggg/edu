package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ExtractStatusEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.SignUtil;
import com.roncoo.education.user.feign.qo.UserAccountExtractLogQO;
import com.roncoo.education.user.feign.vo.UserAccountExtractLogVO;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserAccountExtractLogDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 用户账户提现日志表
 *
 * @author wujing
 */
@Component
public class FeignUserAccountExtractLogBiz extends BaseBiz {

    @Autowired
    private UserAccountExtractLogDao dao;
    @Autowired
    private UserAccountDao userAccountDao;

    public Page<UserAccountExtractLogVO> listForPage(UserAccountExtractLogQO qo) {
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotEmpty(qo.getPhone())) {
            c.andPhoneLike(PageUtil.like(qo.getPhone()));
        }
        if (qo.getExtractStatus() != null) {
            c.andExtractStatusEqualTo(qo.getExtractStatus());
        }
        if (StringUtils.isNotEmpty(qo.getBeginDate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginDate(), "yyyy-MM-dd"));
        }
        if (StringUtils.isNotEmpty(qo.getEndDate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndDate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" extract_status desc, id desc ");
        Page<UserAccountExtractLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, UserAccountExtractLogVO.class);
    }

    public int save(UserAccountExtractLogQO qo) {
        UserAccountExtractLog record = BeanUtil.copyProperties(qo, UserAccountExtractLog.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public UserAccountExtractLogVO getById(Long id) {
        UserAccountExtractLog record = dao.getById(id);
        return BeanUtil.copyProperties(record, UserAccountExtractLogVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public int updateById(UserAccountExtractLogQO qo) {
        UserAccountExtractLog userAccountExtractLog = dao.getById(qo.getId());
        if (ObjectUtil.isNull(userAccountExtractLog)) {
            throw new BaseException("讲师提现日志信息不存在");
        }
        // 审核成功时进来
        if (ExtractStatusEnum.SUCCESS.getCode().equals(qo.getExtractStatus())) {
            // 根据传入讲师用户编号获取讲师账户信息
            UserAccount userAccount = userAccountDao.getByUserNo(userAccountExtractLog.getUserNo());
            if (ObjectUtil.isNull(userAccount)) {
                throw new BaseException("账户信息不存在");
            }

            String sign = SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances());
            if (!sign.equals(userAccount.getSign())) {
                logger.error("签名为：{}，{}", sign, userAccount.getSign());
            }

            userAccount.setHistoryMoney(userAccount.getHistoryMoney().add(userAccount.getFreezeBalances()));// 已提金额 = 原来的已提金额 + 冻结金额
            userAccount.setFreezeBalances(BigDecimal.ZERO);
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            int lecturerExtNum = userAccountDao.updateById(userAccount);
            if (lecturerExtNum < 1) {
                throw new BaseException("讲师账户信息更新失败");
            }
        }
        userAccountExtractLog.setExtractStatus(qo.getExtractStatus());
        return dao.updateById(userAccountExtractLog);
    }

}
