package com.roncoo.education.user.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;

import com.roncoo.education.user.service.api.biz.ApiDistributionProfitBiz;
/**
 * 代理分润记录 Api接口
 *
 * @author wujing
 * @date 2021-01-05
 */
@Api(tags = "API-代理分润记录")
@RestController
@RequestMapping("/user/api/distributionProfit")
public class ApiDistributionProfitController {

    @Autowired
    private ApiDistributionProfitBiz biz;

}
