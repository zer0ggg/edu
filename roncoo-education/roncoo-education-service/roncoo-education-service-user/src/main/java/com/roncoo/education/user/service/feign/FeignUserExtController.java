package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtEchartsQO;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserEchartsVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.service.feign.biz.FeignUserExtBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@RestController
public class FeignUserExtController extends BaseController implements IFeignUserExt {

    @Autowired
    private FeignUserExtBiz biz;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Override
    public Page<UserExtVO> listForPage(@RequestBody UserExtQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody UserExtQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody UserExtQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public UserExtVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

    @Override
    public UserExtVO getByUserNo(@PathVariable(value = "userNo") Long userNo) {
        return biz.getByUserNo(userNo);
    }

    @Override
    public UserExtVO getByUserNoForCache(@PathVariable(value = "userNo") Long userNo) {
        UserExtVO result = biz.getByUserNo(userNo);
        if (null != result) {
            myRedisTemplate.setByJson(RedisPreEnum.USER_EXT.getCode() + userNo, result, 1, TimeUnit.HOURS);
        }
        return result;
    }

    @Override
    public List<UserEchartsVO> sumByCounts(@RequestBody UserExtEchartsQO userExtEchartsQO) {
        return biz.sumByCounts(userExtEchartsQO);
    }

    @Override
    public void cachUserForMsg() {
        biz.cachUserForMsg();
    }

    @Override
    public List<UserExtVO> listAllForUser() {
        return biz.listAllForUser();
    }

    @Override
    public UserExtVO getByMobile(@RequestBody UserExtQO userExtQO) {
        return biz.getByMobile(userExtQO);
    }

    @Override
    public UserExtVO getByReferralCode(@PathVariable(value = "referralCode") String referralCode) {
        return biz.getByReferralCode(referralCode);
    }

    @Override
    public int userIsEnterpriseUser(@RequestBody UserExtQO qo) {
        return biz.userIsEnterpriseUser(qo);
    }

    @Override
    public List<UserExtVO> listByUserNos(@RequestBody UserExtQO userExtQO) {
        return biz.listByUserNos(userExtQO);
    }

}
