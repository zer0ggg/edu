package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.MsgReadBO;
import com.roncoo.education.user.common.bo.MsgViewAllBO;
import com.roncoo.education.user.common.bo.MsgViewBO;
import com.roncoo.education.user.common.bo.UserMsgBO;
import com.roncoo.education.user.common.dto.MsgDTO;
import com.roncoo.education.user.common.dto.MsgReadDTO;
import com.roncoo.education.user.common.dto.UserMsgDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserMsgBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站内信用户记录表
 *
 * @author wuyun
 */
@RestController
@RequestMapping(value = "/user/auth/msg/user")
public class AuthUserMsgController extends BaseController {

	@Autowired
	private AuthUserMsgBiz biz;

	/**
	 * 站内信分页列表接口
	 *
	 * @param userMsgBO
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "学员站内信分页列表接口", notes = "分页列出学员站内信信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<UserMsgDTO>> list(@RequestBody UserMsgBO userMsgBO) {
		return biz.list(userMsgBO);
	}

	/**
	 * 用户查看站内信内容
	 *
	 * @param MsgViewBO
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "用户查看站内信", notes = "用户查看站内信")
	@RequestMapping(value = "/read", method = RequestMethod.POST)
	public Result<MsgDTO> readMsg(@RequestBody MsgViewBO MsgViewBO) {
		return biz.readMsg(MsgViewBO);
	}

	/**
	 * 用户查将所有未读消息置为已读
	 * @param bo
	 * @return
	 */
	@ApiOperation(value = "将所有未读消息置为已读", notes = "将所有未读消息置为已读")
	@RequestMapping(value = "/read/all", method = RequestMethod.POST)
	public Result<String> readAllMsg(@RequestBody MsgViewAllBO bo) {
		return biz.readAllMsg(bo);
	}

	/**
	 * 用户站内信未读条数
	 *
	 * @param msgReadBO
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "用户站内信未读条数", notes = "用户站内信未读条数")
	@RequestMapping(value = "/num", method = RequestMethod.POST)
	public Result<MsgReadDTO> getNumOfUnReadMsg(@RequestBody MsgReadBO msgReadBO) {
		return biz.getNumOfUnReadMsg(msgReadBO);
	}

}
