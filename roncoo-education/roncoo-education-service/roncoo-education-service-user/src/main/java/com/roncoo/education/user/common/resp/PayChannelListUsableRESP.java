package com.roncoo.education.user.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 银行渠道信息表
 * </p>
 *
 * @author wujing
 * @date 2020-03-25
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayChannelListUsableRESP", description="银行渠道信息表")
public class PayChannelListUsableRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "支付渠道名称")
    private String payChannelName;

    @ApiModelProperty(value = "支付渠道编码")
    private String payChannelCode;

}
