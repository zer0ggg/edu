package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.AxisType;
import com.github.abel533.echarts.code.Magic;
import com.github.abel533.echarts.code.Tool;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.feature.MagicType;
import com.github.abel533.echarts.series.Line;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.OrderLogVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.marketing.feign.interfaces.IFeignAct;
import com.roncoo.education.marketing.feign.qo.ActOrderPayBO;
import com.roncoo.education.marketing.feign.vo.ActOrderVO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.interfaces.IFeignVipSet;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.user.common.bo.OrderInfoCloseBO;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.auth.*;
import com.roncoo.education.user.common.dto.UnifiedOrderDTO;
import com.roncoo.education.user.common.dto.auth.*;
import com.roncoo.education.user.common.pay.UnifiedOrder;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.OrderInfoDao;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample.Criteria;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 订单信息表
 *
 * @author wujing
 */
@Component
public class AuthOrderInfoBiz extends BaseBiz {

    @Autowired
    private OrderInfoDao orderInfoDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LecturerDao lecturerDao;

    @Autowired
    private IFeignVipSet feignVipSet;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignOrderLog feignOrderLog;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignUserOrderExamRef feignUserOrderExamRef;
    @Autowired
    private IFeignAct feignAct;

    @Autowired
    private UnifiedOrder payFacades;

    /**
     * 订单列表接口
     *
     * @param authOrderInfoListBO
     * @return
     */
    public Result<Page<AuthOrderInfoListDTO>> listForPage(AuthOrderInfoListBO authOrderInfoListBO) {
        OrderInfoExample Example = new OrderInfoExample();
        // 根据传入用户编号获取用户信息校验用户是否存在或禁用
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !userExt.getStatusId().equals(StatusIdEnum.YES.getCode())) {
            return Result.error("用户异常,请联系统管理员");
        }
        Criteria c = Example.createCriteria();
        c.andUserNoEqualTo(userExt.getUserNo());
        c.andIsShowUserEqualTo(IsShowEnum.YES.getCode());
        if (authOrderInfoListBO.getProductType() != null) {
            c.andProductTypeEqualTo(authOrderInfoListBO.getProductType());
        }
        // 0用于判断前端,查出除了关闭状态的所有订单
        if (authOrderInfoListBO.getOrderStatus() != null && !authOrderInfoListBO.getOrderStatus().equals(0)) {
            c.andOrderStatusEqualTo(authOrderInfoListBO.getOrderStatus());
        } else {
            c.andOrderStatusNotEqualTo(OrderStatusEnum.CLOSE.getCode());
        }
        Example.setOrderByClause(" id desc ");
        Page<OrderInfo> page = orderInfoDao.listForPage(authOrderInfoListBO.getPageCurrent(), authOrderInfoListBO.getPageSize(), Example);
        Page<AuthOrderInfoListDTO> dtoPage = PageUtil.transform(page, AuthOrderInfoListDTO.class);
        for (AuthOrderInfoListDTO dto : dtoPage.getList()) {
            if (ProductTypeEnum.ORDINARY.getCode().equals(dto.getProductType()) || ProductTypeEnum.LIVE.getCode().equals(dto.getProductType()) || ProductTypeEnum.RESOURCES.getCode().equals(dto.getProductType())) {
                CourseVO course = feignCourse.getById(dto.getProductId());
                dto.setCourseLogo(course.getCourseLogo());
                dto.setProductId(course.getId());
            }
        }
        return Result.success(dtoPage);
    }

    /**
     * 订单下单接口
     *
     * @param authOrderPayBO 下单参数
     * @return 支付链接
     */
    @GlobalTransactional(rollbackFor = Exception.class)
    public Result<AuthOrderPayDTO> pay(AuthOrderPayBO authOrderPayBO) {
        // 参数校验
        verifyParam(authOrderPayBO);

        // 根据用户编号查找用户教育信息
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("该账号不可用，请联系客服");
        }

        // 根据用户编号查找用户信息
        User user = userDao.getByUserNo(userExt.getUserNo());
        if (ObjectUtil.isNull(user) || !StatusIdEnum.YES.getCode().equals(user.getStatusId())) {
            return Result.error("该账号已禁用，请联系客服");
        }
        // 活动下单
        if (authOrderPayBO.getActId() != null) {
            return buyAct(authOrderPayBO, userExt);
        }

        // 购买点播、直播、文库
        if (ProductTypeEnum.ORDINARY.getCode().equals(authOrderPayBO.getProductType()) || ProductTypeEnum.LIVE.getCode().equals(authOrderPayBO.getProductType()) || ProductTypeEnum.RESOURCES.getCode().equals(authOrderPayBO.getProductType())) {
            return buyCourse(authOrderPayBO, userExt);
        }

        // 购买会员
        if (ProductTypeEnum.VIP.getCode().equals(authOrderPayBO.getProductType())) {
            return buyVip(authOrderPayBO, userExt);
        }

        // 购买试卷
        if (ProductTypeEnum.EXAM.getCode().equals(authOrderPayBO.getProductType())) {
            return buyExam(authOrderPayBO, userExt);
        }

        return null;
    }


    /**
     * 购买课程
     *
     * @param authOrderPayBO 支付信息
     * @param userExt        用户信息
     * @return 处理结果
     */
    private Result<AuthOrderPayDTO> buyAct(AuthOrderPayBO authOrderPayBO, UserExt userExt) {
        // 课程信息
        CourseVO course = feignCourse.getById(authOrderPayBO.getProductId());
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            return Result.error("获取产品失败，请联系客服");
        }
        if (!course.getCourseCategory().equals(authOrderPayBO.getProductType())) {
            return Result.error("获取产品失败，请联系客服");
        }

        // 获取讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(course.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("该讲师已失效，请联系客服");
        }

        // 判断所要购买的课程是否已经购买
        if (checkOrderInfo(userExt.getUserNo(), authOrderPayBO.getProductId())) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), "已经购买，请勿重复购买");
        }

        String isFreePrompt = "课程免费，无需重复购买";
        if (CourseCategoryEnum.RESOURCES.getCode().equals(course.getCourseCategory())) {
            isFreePrompt = "文库免费，无需重复购买";
        }
        // 价格为0
        if (course.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) == 0) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), isFreePrompt);
        }
        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExt.getIsVip(), userExt.getExpireTime(), course.getCourseDiscount())) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), isFreePrompt);
        }

        // 校验活动基本信息
        ActOrderPayBO actOrderPayBO = BeanUtil.copyProperties(authOrderPayBO, ActOrderPayBO.class);
        actOrderPayBO.setUserNo(ThreadContext.userNo());
        ActOrderVO actVO = feignAct.order(actOrderPayBO);

        // 产品信息
        AuthOrderPayProductBO payProductBO = new AuthOrderPayProductBO();
        if (ObjectUtil.isNull(actVO)) {
            if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
                // 如果该用户为会员，且在有效期内
                // 应付金额
                payProductBO.setPricePayable(course.getCourseOriginal());
                // 优惠金额=课程原价减去优惠价
                payProductBO.setPriceDiscount(course.getCourseOriginal().subtract(course.getCourseDiscount()));
                // 则实付金额为优惠价
                // 实付金额
                payProductBO.setPricePaid(course.getCourseDiscount());
            } else {
                // 如果不是会员实付金额为课程的原价
                // 应付金额
                payProductBO.setPricePayable(course.getCourseOriginal());
                // 优惠金额
                payProductBO.setPriceDiscount(BigDecimal.valueOf(0));
                // 实付金额
                payProductBO.setPricePaid(course.getCourseOriginal());
            }
        } else {
            // 活动价格
            payProductBO.setActTypeId(actVO.getActTypeId());
            payProductBO.setActType(actVO.getActType());
            payProductBO.setPricePayable(course.getCourseOriginal());// 应付金额
            payProductBO.setPriceDiscount(course.getCourseOriginal().subtract(actVO.getActPricePaid()));// 优惠金额= 课程原价-活动折后金额（实付金额）
            payProductBO.setPricePaid(actVO.getActPricePaid());// 实付金额
        }

        payProductBO.setProductId(course.getId());
        payProductBO.setProductName(course.getCourseName());
        payProductBO.setMobile(userExt.getMobile());
        payProductBO.setRegisterTime(userExt.getGmtCreate());
        payProductBO.setLecturerUserNo(lecturer.getLecturerUserNo());
        payProductBO.setLecturerName(lecturer.getLecturerName());
        // 创建订单信息
        return createOrderInfo(authOrderPayBO, payProductBO);

    }

    /**
     * 购买试卷
     *
     * @param authOrderPayBO 支付信息
     * @param userExt        用户信息
     * @return 处理结果
     */
    private Result<AuthOrderPayDTO> buyExam(AuthOrderPayBO authOrderPayBO, UserExt userExt) {
        // 试卷信息
        ExamInfoVO examInfo = feignExamInfo.getById(authOrderPayBO.getProductId());
        if (!StatusIdEnum.YES.getCode().equals(examInfo.getStatusId()) || !IsPutawayEnum.YES.getCode().equals(examInfo.getIsPutaway())) {
            logger.warn("用户编号：[{}]---试卷：[{}]---状态：[{}]---上下架：[{}], 试卷不符合购买", userExt.getUserNo(), examInfo.getId(), examInfo.getStatusId(), examInfo.getIsPutaway());
            return Result.error("试卷不可用，请联系客服");
        }
        if (IsFreeEnum.FREE.getCode().equals(examInfo.getIsFree())) {
            logger.warn("试卷：[{}]为免费试卷，不需要购买", examInfo.getId());
            return Result.error("试卷为免费试卷，不需要购买");
        }

        // 获取讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(examInfo.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("讲师已失效，请联系客服");
        }

        // 校验是否购买
        UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
        userOrderExamRefQO.setUserNo(userExt.getUserNo());
        userOrderExamRefQO.setExamId(examInfo.getId());
        UserOrderExamRefVO userOrderExamRef = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
        if (ObjectUtil.isNotNull(userOrderExamRef)) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), "试卷已经购买，无需重复购买");
        }

        // 获取支付价钱
        BigDecimal pricePaid = examInfo.getOrgPrice();
        if (BigDecimal.ZERO.compareTo(examInfo.getFabPrice()) <= 0) {
            pricePaid = examInfo.getFabPrice();
        }

        // 产品信息
        AuthOrderPayProductBO payProductBO = new AuthOrderPayProductBO();
        // 应付价格
        payProductBO.setPricePayable(examInfo.getOrgPrice());
        // 优惠金额=应付价格-实付价格
        payProductBO.setPriceDiscount(examInfo.getOrgPrice().subtract(pricePaid));
        // 实付价格
        payProductBO.setPricePaid(pricePaid);
        payProductBO.setProductId(examInfo.getId());
        payProductBO.setProductName(examInfo.getExamName());
        payProductBO.setMobile(userExt.getMobile());
        payProductBO.setRegisterTime(userExt.getGmtCreate());
        payProductBO.setLecturerUserNo(lecturer.getLecturerUserNo());
        payProductBO.setLecturerName(lecturer.getLecturerName());
        // 创建订单信息
        return createOrderInfo(authOrderPayBO, payProductBO);
    }

    /**
     * 购买课程
     *
     * @param authOrderPayBO 支付信息
     * @param userExt        用户信息
     * @return 处理结果
     */
    private Result<AuthOrderPayDTO> buyCourse(AuthOrderPayBO authOrderPayBO, UserExt userExt) {
        // 课程信息
        CourseVO course = feignCourse.getById(authOrderPayBO.getProductId());
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            return Result.error("获取产品失败，请联系客服");
        }
        if (!course.getCourseCategory().equals(authOrderPayBO.getProductType())) {
            return Result.error("获取产品失败，请联系客服");
        }

        // 获取讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(course.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("该讲师已失效，请联系客服");
        }

        // 判断所要购买的课程是否已经购买
        if (checkOrderInfo(userExt.getUserNo(), authOrderPayBO.getProductId())) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), "已经购买，请勿重复购买");
        }

        String isFreePrompt = "课程免费，无需重复购买";
        if (CourseCategoryEnum.RESOURCES.getCode().equals(course.getCourseCategory())) {
            isFreePrompt = "文库免费，无需重复购买";
        }
        // 价格为0
        if (course.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) == 0) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), isFreePrompt);
        }
        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExt.getIsVip(), userExt.getExpireTime(), course.getCourseDiscount())) {
            return Result.error(ResultEnum.REPEAT_PAY.getCode(), isFreePrompt);
        }

        // 产品信息
        AuthOrderPayProductBO payProductBO = new AuthOrderPayProductBO();
        if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            // 如果该用户为会员，且在有效期内
            payProductBO.setPricePayable(course.getCourseOriginal());// 应付金额
            // 优惠金额=课程原价减去优惠价
            payProductBO.setPriceDiscount(course.getCourseOriginal().subtract(course.getCourseDiscount()));
            // 则实付金额为优惠价
            payProductBO.setPricePaid(course.getCourseDiscount());// 实付金额
        } else {
            // 如果不是会员实付金额为课程的原价
            payProductBO.setPricePayable(course.getCourseOriginal());// 应付金额
            payProductBO.setPricePaid(course.getCourseOriginal());// 实付金额
            payProductBO.setPriceDiscount(BigDecimal.valueOf(0));// 优惠金额
        }
        payProductBO.setProductId(course.getId());
        payProductBO.setProductName(course.getCourseName());
        payProductBO.setMobile(userExt.getMobile());
        payProductBO.setRegisterTime(userExt.getGmtCreate());
        payProductBO.setLecturerUserNo(lecturer.getLecturerUserNo());
        payProductBO.setLecturerName(lecturer.getLecturerName());
        // 创建订单信息
        return createOrderInfo(authOrderPayBO, payProductBO);
    }

    /**
     * 购买会员
     *
     * @param authOrderPayBO 支付信息
     * @param userExt        用户信息
     * @return 处理结果
     */
    private Result<AuthOrderPayDTO> buyVip(AuthOrderPayBO authOrderPayBO, UserExt userExt) {
        // 查找该是否设置该会员类型
        VipSetVO vipSet = feignVipSet.getById(authOrderPayBO.getProductId());
        if (ObjectUtil.isNull(vipSet) || !StatusIdEnum.YES.getCode().equals(vipSet.getStatusId())) {
            return Result.error("会员设置不可用!");
        }

        // 产品信息
        AuthOrderPayProductBO payProductBO = new AuthOrderPayProductBO();
        payProductBO.setPricePayable(vipSet.getOrgPrice());
        payProductBO.setPricePaid(vipSet.getFabPrice());
        // 已经购买会员且在有效期内,为续费
        if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            payProductBO.setPricePaid(vipSet.getRenewalPrice());
        }
        String goodsName = Constants.VIP_NAME;
        // 支付显示，"领课网络-VIP会员"
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.ALIYUN_SMS_SIGN_NAME);
        if (ObjectUtil.isNotEmpty(sysConfigVO) && ObjectUtil.isNotEmpty(sysConfigVO.getConfigValue())) {
            goodsName = sysConfigVO.getConfigValue() + "-" + Constants.VIP_NAME;
        }
        // 优惠金额
        payProductBO.setPriceDiscount(payProductBO.getPricePayable().subtract(payProductBO.getPricePaid()));
        payProductBO.setProductId(vipSet.getId());
        payProductBO.setProductName(goodsName);
        payProductBO.setMobile(userExt.getMobile());
        payProductBO.setRegisterTime(userExt.getGmtCreate());
        payProductBO.setLecturerUserNo(0L);
        payProductBO.setLecturerName("购买会员");

        // 创建订单信息
        return createOrderInfo(authOrderPayBO, payProductBO);
    }

    /**
     * 订单继续支付接口
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthOrderPayDTO> continuePay(AuthOrderInfoContinuePayBO authOrderInfoContinuePayBO) {
        if (StringUtils.isEmpty(authOrderInfoContinuePayBO.getOrderNo())) {
            return Result.error("orderNo不能为空");
        }
        if (StringUtils.isEmpty(authOrderInfoContinuePayBO.getPayType())) {
            return Result.error("payType不能为空");
        }

        // 订单校验
        OrderInfo orderInfo = orderInfoDao.getByOrderNo(authOrderInfoContinuePayBO.getOrderNo());
        if (ObjectUtil.isNull(orderInfo)) {
            return Result.error("orderNo不正确，没有找到订单信息");
        }
        if (!orderInfo.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("orderNo不正确，没有找到订单信息");
        }

        // 根据用户编号查找用户信息
        UserExt userExt = userExtDao.getByUserNo(orderInfo.getUserNo());
        if (StringUtils.isEmpty(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("该用户不可用，请联系客服!");
        }

        // 购买点播、直播、文库
        if (ProductTypeEnum.ORDINARY.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.LIVE.getCode().equals(orderInfo.getProductType()) || ProductTypeEnum.RESOURCES.getCode().equals(orderInfo.getProductType())) {
            // 重复判断所要购买的课程是否已经购买
            if (checkOrderInfo(orderInfo.getUserNo(), orderInfo.getProductId())) {
                return Result.error(ResultEnum.REPEAT_PAY.getCode(), "已经购买该产品，请勿重复购买");
            }
            // 查找课程信息
            CourseVO course = feignCourse.getById(orderInfo.getProductId());
            if (StringUtils.isEmpty(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
                return Result.error("该课程不可用，请联系客服!");
            }
        }

        // 购买会员
        if (ProductTypeEnum.VIP.getCode().equals(orderInfo.getProductType())) {
            // 查找该是否设置该会员类型
            VipSetVO vipSet = feignVipSet.getById(orderInfo.getProductId());
            if (ObjectUtil.isNull(vipSet) || !StatusIdEnum.YES.getCode().equals(vipSet.getStatusId())) {
                return Result.error("会员设置不可用!");
            }
        }

        // 购买试卷
        if (ProductTypeEnum.EXAM.getCode().equals(orderInfo.getProductType())) {
            // 试卷信息
            ExamInfoVO examInfo = feignExamInfo.getById(orderInfo.getProductId());
            if (!StatusIdEnum.YES.getCode().equals(examInfo.getStatusId()) || !IsPutawayEnum.YES.getCode().equals(examInfo.getIsPutaway())) {
                logger.warn("用户编号：[{}]---试卷：[{}]---状态：[{}]---上下架：[{}], 试卷不符合购买", orderInfo.getUserNo(), examInfo.getId(), examInfo.getStatusId(), examInfo.getIsPutaway());
                return Result.error("试卷不可用，请联系客服");
            }
            // 获取讲师信息
            Lecturer lecturer = lecturerDao.getByLecturerUserNo(examInfo.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                return Result.error("讲师已失效，请联系客服");
            }
            // 校验是否购买
            UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
            userOrderExamRefQO.setUserNo(orderInfo.getUserNo());
            userOrderExamRefQO.setExamId(examInfo.getId());
            UserOrderExamRefVO userOrderExamRef = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
            if (ObjectUtil.isNotNull(userOrderExamRef)) {
                return Result.error(ResultEnum.REPEAT_PAY.getCode(), "试卷已经购买，无需重复购买");
            }
        }

        // 更新订单信息
        orderInfo.setPayType(authOrderInfoContinuePayBO.getPayType());
        orderInfo.setOrderStatus(OrderStatusEnum.WAIT.getCode());
        orderInfo.setSerialNumber(NOUtil.getSerialNumber());
        orderInfoDao.updateById(orderInfo);

        // 获取支付链接
        AuthOrderPayDTO dto = pay(orderInfo);
        return Result.success(dto);
    }

    /**
     * 获取支付链接
     *
     * @param orderInfo 订单信息
     * @return 支付链接
     */
    private AuthOrderPayDTO pay(OrderInfo orderInfo) {
        // 查找系统配置信息
        SysConfigVO gatewayDomain = feignSysConfig.getByConfigKey(SysConfigConstants.GATEWAY_DOMAIN);
        if (ObjectUtil.isNull(gatewayDomain.getConfigValue())) {
            throw new BaseException("未配置网关域名，gatewayDomain");
        }
        SysConfigVO h5Domain = feignSysConfig.getByConfigKey(SysConfigConstants.H5_DOMAIN);
        if (StringUtils.isEmpty(h5Domain.getConfigValue())) {
            throw new BaseException("未配置移动端域名，h5Domain");
        }
        // 调支付
        PayBO payBO = new PayBO();
        // 微信小程序支付
        if (PayTypeEnum.XCX.getCode().equals(orderInfo.getPayType())) {
            User user = userDao.getByUserNo(orderInfo.getUserNo());
            if (ObjectUtil.isNull(user) || StatusIdEnum.NO.getCode().equals(user.getStatusId())) {
                throw new BaseException("userNo不正确");
            }
            if (StringUtils.isEmpty(user.getUniconId())) {
                throw new BaseException("没绑定小程序");
            }
            // 用户微信小程序uniconId
            payBO.setOpenId(user.getUniconId());
        }
        payBO.setOrderNo(String.valueOf(orderInfo.getSerialNumber()));
        payBO.setGoodsName(orderInfo.getProductName());
        payBO.setOrderAmount(orderInfo.getPricePaid());
        payBO.setPayType(orderInfo.getPayType());
        payBO.setNotifyUrl(gatewayDomain.getConfigValue() + "/callback/order/notify/%s");
        payBO.setReturnUrl(h5Domain.getConfigValue() + "/pages/account/order/order");
        UnifiedOrderDTO unifiedOrderDTO = payFacades.unifiedorder(payBO);
        String results = unifiedOrderDTO.getPayMessage();
        if (StringUtils.isEmpty(results)) {
            throw new BaseException("下单失败,请联系客服!");
        }
        orderInfo.setPayChannelCode(unifiedOrderDTO.getPayChannelCode());
        orderInfo.setPayChannelName(unifiedOrderDTO.getPayChannelName());
        if (orderInfo.getId() != null) {
            // 继续支付
            orderInfoDao.updateById(orderInfo);
        } else {
            // 创建订单
            orderInfoDao.save(orderInfo);
        }

        // 返回实体
        AuthOrderPayDTO dto = new AuthOrderPayDTO();
        dto.setPayMessage(results);
        dto.setOrderNo(String.valueOf(orderInfo.getOrderNo()));
        dto.setCourseName(orderInfo.getProductName());
        dto.setPayType(orderInfo.getPayType());
        dto.setPrice(orderInfo.getPricePaid());
        return dto;
    }

    /**
     * 关闭订单信息接口
     *
     * @return
     */
    @GlobalTransactional(rollbackFor = Exception.class)
    public Result<String> close(OrderInfoCloseBO orderInfoCloseBO) {
        if (StringUtils.isEmpty(orderInfoCloseBO.getOrderNo())) {
            return Result.error("orderNo不能为空");
        }
        OrderInfo orderInfo = orderInfoDao.getByOrderNo(orderInfoCloseBO.getOrderNo());
        if (ObjectUtil.isNull(orderInfo)) {
            return Result.error("orderNo不正确,找不到订单信息");
        }
        if (!orderInfo.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("orderNo不正确,找不到订单信息");
        }
        if (!OrderStatusEnum.WAIT.getCode().equals(orderInfo.getOrderStatus())) {
            return Result.error("该订单已经处理完成，不需要再处理");
        }
        // 根据用户编号查找用户信息
        UserExt userExt = userExtDao.getByUserNo(orderInfo.getUserNo());
        if (StringUtils.isEmpty(userExt) || !StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            return Result.error("根据传入的userNo没找到对应的用户信息!");
        }
        orderInfo.setOrderStatus(OrderStatusEnum.CLOSE.getCode());
        int orderNum = orderInfoDao.updateById(orderInfo);
        if (orderNum < 1) {
            throw new BaseException("订单信息更新失败");
        }
        OrderLogVO orderLog = feignOrderLog.getByOrderNo(orderInfoCloseBO.getOrderNo());
        if (ObjectUtil.isNull(orderLog)) {
            throw new BaseException("找不到订单记录信息");
        }
        orderLog.setOrderStatus(OrderStatusEnum.CLOSE.getCode());
        if (feignOrderLog.updateById(BeanUtil.copyProperties(orderLog, OrderLogQO.class)) < 1) {
            throw new BaseException("订单流水号更新失败");
        }
        return Result.success("订单关闭成功");
    }

    /**
     * 订单详情
     *
     * @return
     */
    public Result<AuthOrderInfoDTO> view(AuthOrderInfoViewBO authOrderInfoViewBO) {
        if (StringUtils.isEmpty(authOrderInfoViewBO.getOrderNo())) {
            return Result.error("orderNo不能为空");
        }
        // 根据订单编号查找订单信息
        OrderInfo order = orderInfoDao.getByOrderNo(authOrderInfoViewBO.getOrderNo());
        if (ObjectUtil.isNull(order)) {
            return Result.error("orderNo不正确");
        }
        if (!order.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("orderNo不正确");
        }
        return Result.success(BeanUtil.copyProperties(order, AuthOrderInfoDTO.class));
    }

    /**
     * 查找订单信息列表信息
     *
     * @param authOrderInfoListBO
     * @return
     */
    public Result<Page<AuthOrderInfoListForLecturerDTO>> list(AuthOrderInfoListBO authOrderInfoListBO) {
        if (StringUtils.isEmpty(authOrderInfoListBO.getLecturerUserNo())) {
            return Result.error("lecturerUserNo不正确");
        }
        OrderInfoExample example = new OrderInfoExample();
        OrderInfoExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        c.andIsShowUserEqualTo(IsShowUserEnum.YES.getCode());
        if (authOrderInfoListBO.getProductType() != null) {
            c.andProductTypeEqualTo(authOrderInfoListBO.getProductType());
        }
        // 收入大于0.5块才显示
        c.andPricePaidGreaterThanOrEqualTo(BigDecimal.valueOf(0.5));
        // 不查找已经关闭了的订单
        c.andOrderStatusEqualTo(OrderStatusEnum.SUCCESS.getCode());
        // 不查找手工下单的订单
        c.andPayTypeNotEqualTo(PayTypeEnum.MANUAL.getCode());
        example.setOrderByClause(" id desc ");
        Page<OrderInfo> page = orderInfoDao.listForPage(authOrderInfoListBO.getPageCurrent(), authOrderInfoListBO.getPageSize(), example);
        Page<AuthOrderInfoListForLecturerDTO> dtoPage = PageUtil.transform(page, AuthOrderInfoListForLecturerDTO.class);
        for (AuthOrderInfoListForLecturerDTO dto : dtoPage.getList()) {
            dto.setPhone(dto.getMobile().substring(0, 3) + "****" + dto.getMobile().substring(7));
        }
        return Result.success(dtoPage);
    }

    /**
     * 讲师收益折线图
     *
     * @param authOrderInfoForChartsBO
     * @return
     */
    public Result<Option> charts(AuthOrderInfoForChartsBO authOrderInfoForChartsBO) {
        Option option = new Option();
        option.legend().data("订单收益", "日期时间");
        option.tooltip().trigger(Trigger.axis).axisPointer();
        option.calculable(true);
        // 设置x轴数据
        CategoryAxis categoryAxis = new CategoryAxis();
        List<String> xData = new ArrayList<>();
        payTime(authOrderInfoForChartsBO, xData);
        for (String x : xData) {
            categoryAxis.data(x);
        }
        option.xAxis(categoryAxis);

        // 设置y轴数据
        ValueAxis valueAxis = new ValueAxis();
        valueAxis.type(AxisType.value);
        valueAxis.splitArea().show(true);
        valueAxis.axisLabel().formatter("{value}元");
        option.yAxis(valueAxis);
        // 第一条线为当天收益
        Line line1 = new Line();
        List<AuthOrderInfoLecturerIncomeDTO> dtoList = sumByLecturerUserNoAndCourseCategoryAndData(ThreadContext.userNo(), authOrderInfoForChartsBO.getProductType(), xData);
        for (AuthOrderInfoLecturerIncomeDTO dto : dtoList) {
            for (BigDecimal bi : dto.getLecturerProfit()) {
                line1.data(bi);
            }
        }
        line1.name("讲师	");
        option.series(line1);
        option.toolbox().show(true).feature(new MagicType(Magic.line, Magic.bar), Tool.restore, Tool.saveAsImage);
        return Result.success(option);
    }

    private List<AuthOrderInfoLecturerIncomeDTO> sumByLecturerUserNoAndCourseCategoryAndData(Long lecturerUserNo, Integer courseCategory, List<String> xData) {
        List<AuthOrderInfoLecturerIncomeDTO> list = new ArrayList<>();
        AuthOrderInfoLecturerIncomeDTO dto = new AuthOrderInfoLecturerIncomeDTO();
        List<BigDecimal> countPaidPrice = new ArrayList<>();
        for (String date : xData) {
            BigDecimal sum = orderInfoDao.sumByLecturerUserNoAndCourseCategoryAndData(lecturerUserNo, courseCategory, date);
            countPaidPrice.add(sum);
        }
        dto.setLecturerProfit(countPaidPrice);
        list.add(dto);
        return list;
    }

    private List<String> payTime(AuthOrderInfoForChartsBO authOrderInfoForChartsBO, List<String> xData) {
        // 如果时间为空，则传入现在当前时间七天前的订单
        if (authOrderInfoForChartsBO.getBeginCreate() == null && authOrderInfoForChartsBO.getEndCreate() == null) {
            authOrderInfoForChartsBO.setBeginCreate(DateUtil.format(DateUtil.addDate(new Date(), -7)));
            authOrderInfoForChartsBO.setEndCreate(DateUtil.format(new Date()));
        }
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(DateUtil.parseDate(authOrderInfoForChartsBO.getBeginCreate(), "yyyy-MM-dd"));
        tempStart.add(Calendar.DAY_OF_YEAR, 0);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(DateUtil.parseDate(authOrderInfoForChartsBO.getEndCreate(), "yyyy-MM-dd"));
        tempEnd.add(Calendar.DAY_OF_YEAR, 1);
        while (tempStart.before(tempEnd)) {
            xData.add(DateUtil.formatDate(tempStart.getTime()));
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return xData;
    }

    /**
     * 校验下单时传入的参数
     */
    private void verifyParam(AuthOrderPayBO authOrderPayBO) {
        if (StringUtils.isEmpty(authOrderPayBO.getProductId())) {
            throw new BaseException("ProductId不能为空");
        }
        if (StringUtils.isEmpty(authOrderPayBO.getPayType())) {
            throw new BaseException("payType不能为空");
        }
        if (StringUtils.isEmpty(authOrderPayBO.getChannelType())) {
            throw new BaseException("channelType不能为空");
        }
        if (StringUtils.isEmpty(authOrderPayBO.getProductType())) {
            throw new BaseException("ProductType不能为空");
        }
    }

    /**
     * 判断课程是否已经支付
     */
    private boolean checkOrderInfo(long userNo, long courseId) {
        UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();
        userOrderCourseRefQO.setUserNo(userNo);
        userOrderCourseRefQO.setRefId(courseId);
        UserOrderCourseRefVO userOrderCourseRef = feignUserOrderCourseRef.getByUserNoAndRefId(userOrderCourseRefQO);
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            return false;
        }
        // 判断课程是否在有效期内---已经过期
        return !userOrderCourseRef.getExpireTime().before(new Date());
    }

    /**
     * 创建订单信息表
     */
    private Result<AuthOrderPayDTO> createOrderInfo(AuthOrderPayBO authOrderPayBO, AuthOrderPayProductBO payProductBO) {
        OrderInfo orderInfo = BeanUtil.copyProperties(payProductBO, OrderInfo.class);
        orderInfo.setOrderNo(NOUtil.getOrderNo()); // 订单号，不要使用IdWorker生成
        orderInfo.setPlatformIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setAgentIncome(BigDecimal.ZERO);
        orderInfo.setLecturerIncome(BigDecimal.ZERO);
        orderInfo.setIsShowUser(IsShowUserEnum.YES.getCode());
        orderInfo.setTradeType(TradeTypeEnum.ONLINE.getCode());
        orderInfo.setProductType(authOrderPayBO.getProductType());
        orderInfo.setPayType(authOrderPayBO.getPayType());
        orderInfo.setChannelType(authOrderPayBO.getChannelType());
        orderInfo.setRemarkCus(authOrderPayBO.getRemarkCus());
        orderInfo.setReferralCode(authOrderPayBO.getReferralCode());
        orderInfo.setOrderStatus(OrderStatusEnum.WAIT.getCode());
        orderInfo.setSerialNumber(NOUtil.getSerialNumber());
        orderInfo.setUserNo(ThreadContext.userNo());
        if (payProductBO.getActTypeId() != null) {
            orderInfo.setActTypeId(payProductBO.getActTypeId());
            orderInfo.setActType(payProductBO.getActType());
        }
        orderInfo.setId(IdWorker.getId());
        orderInfoDao.save(orderInfo); // 保存订单
        // 异步保存支付日志
        CALLBACK_EXECUTOR.execute(new orderLog(orderInfo));

        //feignOrderLog.save(BeanUtil.copyProperties(orderInfo, OrderLogQO.class));

        // 获取支付链接并操作订单
        AuthOrderPayDTO dto = pay(orderInfo);
        return Result.success(dto);
    }

    /**
     * 异步保存订单日志
     */
    class orderLog implements Runnable {
        private final OrderInfo orderInfo;

        public orderLog(OrderInfo orderInfo) {
            this.orderInfo = orderInfo;
        }

        @Override
        public void run() {
            try {
                feignOrderLog.save(BeanUtil.copyProperties(orderInfo, OrderLogQO.class));
            } catch (Exception e) {
                logger.error("异步保存订单日志", e);
            }
        }
    }

}
