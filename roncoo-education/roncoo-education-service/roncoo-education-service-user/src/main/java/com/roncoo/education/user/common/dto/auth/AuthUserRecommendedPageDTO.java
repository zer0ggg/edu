package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户推荐信息
 * </p>
 *
 * @author wujing
 * @date 2020-04-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserRecommendedPageDTO", description="用户推荐信息")
public class AuthUserRecommendedPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;
    /**
     * 昵称（如果为空返回****手机号）
     */
    @ApiModelProperty(value = "昵称（如果为空返回****手机号）")
    private String nickname;

    @ApiModelProperty(value = "推荐码")
    private String referralCode;
}
