package com.roncoo.education.user.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 支付渠道信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayChannelRESP", description="支付渠道信息")
public class PayChannelRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "支付渠道编码")
    private String payChannelCode;

    @ApiModelProperty(value = "支付渠道名称")
    private String payChannelName;

    @ApiModelProperty(value = "支付实现编码")
    private String payObjectCode;

    @ApiModelProperty(value = "支付实现名称")
    private String payObjectName;

    @ApiModelProperty(value = "支付编号")
    private String payKey;

    @ApiModelProperty(value = "支付密钥")
    private String paySecret;

    @ApiModelProperty(value = "请求地址")
    private String requestUrl;

    @ApiModelProperty(value = "查询地址")
    private String queryUrl;

    @ApiModelProperty(value = "备用字段1")
    private String field1;

    @ApiModelProperty(value = "备用字段2")
    private String field2;

    @ApiModelProperty(value = "备用字段3")
    private String field3;
}
