package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.LiveChannelQO;
import com.roncoo.education.user.feign.vo.LiveChannelVO;
import com.roncoo.education.user.service.dao.LiveChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannelExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannelExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 直播频道表
 *
 * @author LHR
 */
@Component
public class FeignLiveChannelBiz extends BaseBiz {

    @Autowired
    private LiveChannelDao dao;


	public Page<LiveChannelVO> listForPage(LiveChannelQO qo) {
	LiveChannelExample example = new LiveChannelExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<LiveChannel> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, LiveChannelVO.class);
	}

	public int save(LiveChannelQO qo) {
		LiveChannel record = BeanUtil.copyProperties(qo, LiveChannel.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public LiveChannelVO getById(Long id) {
		LiveChannel record = dao.getById(id);
		return BeanUtil.copyProperties(record, LiveChannelVO.class);
	}

	public int updateById(LiveChannelQO qo) {
		LiveChannel record = BeanUtil.copyProperties(qo, LiveChannel.class);
		return dao.updateById(record);
	}

    public int updateChannelIsUseAndPasswd(LiveChannelQO qo) {
		LiveChannel liveChannel = dao.getByChannel(qo.getChannelId());
		if (ObjectUtil.isNull(liveChannel)) {
			return 0;
		}
		liveChannel.setChannelPasswd(qo.getChannelPasswd());
		liveChannel.setIsUse(qo.getIsUse());
		return dao.updateById(liveChannel);
    }

	public List<LiveChannelVO> listBySceneAndIsUse(LiveChannelQO qo) {
		List<LiveChannel> list = dao.listBySceneAndIsUse(qo.getScene(), qo.getIsUse());
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return PageUtil.copyList(list, LiveChannelVO.class);
	}

	public LiveChannelVO getByChannelId(String channelId) {
		return BeanUtil.copyProperties(dao.getByChannel(channelId), LiveChannelVO.class);
	}
}
