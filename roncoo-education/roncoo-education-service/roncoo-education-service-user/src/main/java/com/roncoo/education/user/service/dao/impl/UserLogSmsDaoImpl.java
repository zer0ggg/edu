package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserLogSmsDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserLogSmsMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSms;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogSmsExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserLogSmsDaoImpl implements UserLogSmsDao {
	@Autowired
	private UserLogSmsMapper userLogSmsMapper;

	@Override
    public int save(UserLogSms record) {
		record.setId(IdWorker.getId());
		return this.userLogSmsMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.userLogSmsMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(UserLogSms record) {
		return this.userLogSmsMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public UserLogSms getById(Long id) {
		return this.userLogSmsMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<UserLogSms> listForPage(int pageCurrent, int pageSize, UserLogSmsExample example) {
		int count = this.userLogSmsMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<UserLogSms>(count, totalPage, pageCurrent, pageSize, this.userLogSmsMapper.selectByExample(example));
	}

	@Override
	public List<UserLogSms> listByAll() {
		UserLogSmsExample example = new UserLogSmsExample();
		return this.userLogSmsMapper.selectByExample(example);
	}

	@Override
	public List<UserLogSms> listByIsSuccess(Integer isSuccess) {
		UserLogSmsExample example = new UserLogSmsExample();
		Criteria criteria = example.createCriteria();
		criteria.andIsSuccessEqualTo(isSuccess);
		return this.userLogSmsMapper.selectByExample(example);
	}
}
