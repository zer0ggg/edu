package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserAccountExtractLogMapper {
    int countByExample(UserAccountExtractLogExample example);

    int deleteByExample(UserAccountExtractLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserAccountExtractLog record);

    int insertSelective(UserAccountExtractLog record);

    List<UserAccountExtractLog> selectByExample(UserAccountExtractLogExample example);

    UserAccountExtractLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserAccountExtractLog record, @Param("example") UserAccountExtractLogExample example);

    int updateByExample(@Param("record") UserAccountExtractLog record, @Param("example") UserAccountExtractLogExample example);

    int updateByPrimaryKeySelective(UserAccountExtractLog record);

    int updateByPrimaryKey(UserAccountExtractLog record);
}
