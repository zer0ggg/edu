package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchEditBO;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchSaveBO;
import com.roncoo.education.user.common.bo.auth.AuthUserResearchViewBO;
import com.roncoo.education.user.common.dto.UserResearchViewDTO;
import com.roncoo.education.user.service.dao.UserResearchDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * 用户调研信息
 *
 * @author wujing
 */
@Component
public class AuthUserResearchBiz extends BaseBiz {

    @Autowired
    private UserResearchDao dao;

    public Result<String> save(AuthUserResearchSaveBO bo) {
        if (StringUtils.isEmpty(bo.getContent())) {
            return Result.error("内容不能为空");
        }
        UserResearch record = BeanUtil.copyProperties(bo, UserResearch.class);
        record.setUserNo(ThreadContext.userNo());
        record.setName(DateUtil.format(new Date(), Constants.DATE.YYYYMMDDHHMMSSSSS) + RandomUtil.randomNumbers(3));
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    public Result<UserResearchViewDTO> view(AuthUserResearchViewBO bo) {
        UserResearch userResearch = dao.getById(bo.getId());
        if (ObjectUtil.isNull(userResearch)) {
            return Result.error("找不到用户调研信息");
        }
        if (!userResearch.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("找不到用户调研信息");
        }
        return Result.success(BeanUtil.copyProperties(userResearch, UserResearchViewDTO.class));
    }

    public Result<String> edit(AuthUserResearchEditBO bo) {
        UserResearch userResearch = dao.getById(bo.getId());
        if (ObjectUtil.isNull(userResearch)) {
            return Result.error("找不到用户调研信息");
        }
        if (!userResearch.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("找不到用户调研信息");
        }
        if (dao.updateById(BeanUtil.copyProperties(bo, UserResearch.class)) > 0) {
            return Result.success("更新成功");
        }
        return Result.error("更新失败");
    }
}
