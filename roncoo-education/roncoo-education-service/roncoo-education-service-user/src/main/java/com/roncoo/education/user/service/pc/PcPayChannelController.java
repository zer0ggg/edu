package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.PayChannelEditREQ;
import com.roncoo.education.user.common.req.PayChannelListREQ;
import com.roncoo.education.user.common.req.PayChannelSaveREQ;
import com.roncoo.education.user.common.req.PayChannelUpdateStatusREQ;
import com.roncoo.education.user.common.resp.PayChannelListRESP;
import com.roncoo.education.user.common.resp.PayChannelListUsableRESP;
import com.roncoo.education.user.common.resp.PayChannelViewRESP;
import com.roncoo.education.user.service.pc.biz.PcPayChannelBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * 支付渠道信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/pay/channel")
@Api(value = "user-支付渠道信息", tags = {"user-支付渠道信息"})
public class PcPayChannelController {

    @Autowired
    private PcPayChannelBiz biz;

    @ApiOperation(value = "支付渠道信息列表", notes = "支付渠道信息列表")
    @PostMapping(value = "/list")
    public Result<Page<PayChannelListRESP>> list(@RequestBody PayChannelListREQ payChannelListREQ) {
        return biz.list(payChannelListREQ);
    }

    @ApiOperation(value = "支付渠道信息添加", notes = "支付渠道信息添加")
    @PostMapping(value = "/save")
    @SysLog(value = "支付渠道信息添加")
    public Result<String> save(@RequestBody PayChannelSaveREQ payChannelSaveREQ) {
        return biz.save(payChannelSaveREQ);
    }

    @ApiOperation(value = "支付渠道信息查看", notes = "支付渠道信息查看")
    @GetMapping(value = "/view")
    public Result<PayChannelViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "支付渠道信息修改", notes = "支付渠道信息修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "支付渠道信息修改")
    public Result<String> edit(@RequestBody PayChannelEditREQ payChannelEditREQ) {
        return biz.edit(payChannelEditREQ);
    }

    @ApiOperation(value = "支付渠道信息删除", notes = "支付渠道信息删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "支付渠道信息删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "获取可用渠道", notes = "获取可用渠道")
    @GetMapping(value = "/list/usable")
    public Result<ArrayList<PayChannelListUsableRESP>> listUsable() {
        return biz.listUsable();
    }

    @ApiOperation(value = "支付渠道状态修改", notes = "支付渠道状态修改")
    @PutMapping(value = "/update/status")
    @SysLog(value = "支付渠道状态修改")
    public Result<String> updateStatus(@RequestBody @Valid PayChannelUpdateStatusREQ payChannelUpdateStatusREQ) {
        return biz.updateStatus(payChannelUpdateStatusREQ);
    }
}
