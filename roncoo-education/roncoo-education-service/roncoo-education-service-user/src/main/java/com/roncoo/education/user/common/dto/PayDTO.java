package com.roncoo.education.user.common.dto;

import com.roncoo.education.common.core.enums.PayNetWorkStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 支付
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PayDTO", description = "支付")
public class PayDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "支付信息 扫码支付返回二维码支付链接 wap支付返回wap支付链接 公众号支付返回公众号所需信息")
    private String payMessage;

    @ApiModelProperty(value = "通信状态")
    private PayNetWorkStatusEnum payNetWorkStatusEnum;
}
