package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.faceid.FaceContrastResult;
import com.roncoo.education.common.core.faceid.FaceContrastUtil;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.data.feign.interfaces.IFeignOrderLog;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.qo.OrderLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import com.roncoo.education.system.feign.interfaces.IFeignSharingTemplate;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.qo.SharingTemplateQO;
import com.roncoo.education.system.feign.vo.*;
import com.roncoo.education.user.common.WxMaAppUtil;
import com.roncoo.education.user.common.bo.AuthSharingBO;
import com.roncoo.education.user.common.bo.AuthUserLogoutBO;
import com.roncoo.education.user.common.bo.auth.*;
import com.roncoo.education.user.common.dto.auth.AuthUserExtDTO;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户教育信息
 *
 * @author wujing
 */
@Component
public class AuthUserExtBiz extends BaseBiz {

    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserAccountDao userAccountDao;
    @Autowired
    private UserAccountExtractLogDao userAccountExtractLogDao;
    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private LecturerAuditDao lecturerAuditDao;
    @Autowired
    private OrderInfoDao orderInfoDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignOrderLog feignOrderLog;
    @Autowired
    private IFeignCourseLog feignCourseLog;

    @Autowired
    private IFeignSharingTemplate feignSharingTemplate;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 用户信息查看接口
     */
    public Result<AuthUserExtDTO> view(AuthUserExtViewBO authUserExtViewBO) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("找不到该用户信息");
        }
        User user = userDao.getByUserNo(userExt.getUserNo());
        // 如果用户邀请码为空直接生产一个邀请码
        if (StringUtils.isEmpty(userExt.getReferralCode())) {
            // 推荐码校验;
            userExt.setReferralCode(check(ReferralCodeUtil.getStringRandom()));
            userExtDao.updateById(userExt);
        }
        AuthUserExtDTO dto = BeanUtil.copyProperties(userExt, AuthUserExtDTO.class);
        if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
            dto.setIsVip(IsVipEnum.YES.getCode());
        } else {
            dto.setIsVip(IsVipEnum.NO.getCode());
        }
        dto.setUniconId(user.getUniconId());
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.POLYV_LIVE_UPLOAD_SET);
        if (ObjectUtil.isNotEmpty(sysConfigVO)) {
            dto.setIsAllowLiveUploadVideo(sysConfigVO.getConfigValue());
        }
        // 若讲师禁用，则没讲师权限
        Lecturer lecturer = lecturerDao.getByLecturerUserNoAndStatusId(user.getUserNo(), StatusIdEnum.YES.getCode());
        if (ObjectUtils.isEmpty(lecturer)) {
            // 设置为普通用户
            dto.setUserType(UserTypeEnum.USER.getCode());
        }
        return Result.success(dto);
    }

    /**
     * 用户信息更新接口
     *
     * @param authUserExtBO
     * @author wuyun
     */
    public Result<AuthUserExtDTO> update(AuthUserExtBO authUserExtBO) {
        if (authUserExtBO.getId() == null) {
            return Result.error("Id不能为空");
        }
        // 根据传入userNo查找到对应的用户信息
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("没找到对应的用户信息");
        }
        // 如果修改图片删除阿里云oss上的原图片
        if (!StringUtils.isEmpty(authUserExtBO.getHeadImgUrl())) {
            if (!StringUtils.isEmpty(userExt.getHeadImgUrl()) && !authUserExtBO.getHeadImgUrl().equals(userExt.getHeadImgUrl())) {
                AliyunUtil.delete(userExt.getHeadImgUrl(), BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
            }
        }
        userExt = BeanUtil.copyProperties(authUserExtBO, UserExt.class);
        userExt.setUserNo(ThreadContext.userNo());
        int resultNum = userExtDao.updateByUserNo(userExt);
        if (resultNum > 0) {
            // 修改成功返回用户信息
            UserExt education = userExtDao.getByUserNo(ThreadContext.userNo());
            AuthUserExtDTO dto = BeanUtil.copyProperties(education, AuthUserExtDTO.class);
            return Result.success(dto);
        }
        return Result.error(ResultEnum.USER_UPDATE_FAIL.getDesc());

    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> referralCode(AuthUserExtReferralCodeBO bo) {
        ConfigWeixinVO sysVO = feignSysConfig.getWeixin();
        if (ObjectUtil.isNull(sysVO)) {
            return Result.error("找不到系统配置信息");
        }

        if (StringUtils.isEmpty(sysVO.getMinappAppId()) || StringUtils.isEmpty(sysVO.getMinappAppSecret())) {
            return Result.error("appid或者appSecret没配置");
        }
        // 根据邀请码查找用户信息
        UserExt userExt = userExtDao.getByReferralCode(bo.getReferralCode());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("推荐码错误,找不到用户信息");
        }
        // 判断如果用户的二维码不为空返回图片URL,如果为空生成微信邀请码
        if (!StringUtils.isEmpty(userExt.getCodeUrl())) {
            return Result.success(userExt.getCodeUrl());
        }

        try {
            File file = WxMaAppUtil.getService(sysVO.getMinappAppId(), sysVO.getMinappAppSecret(), WxMaAppUtil.AESKEY, myRedisTemplate.getStringRedisTemplate()).getQrcodeService().createWxaCodeUnlimit(bo.getReferralCode(), bo.getPage());
            // 上传到阿里云
            userExt.setCodeUrl(AliyunUtil.uploadPic(PlatformEnum.USER, file, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class)));
            userExtDao.updateById(userExt);
            return Result.success(userExt.getCodeUrl());
        } catch (WxErrorException e) {
            logger.warn("获取二维码失败", e);
        }
        return Result.error("获取二维码失败");
    }

    /**
     * 推荐码校验全局唯一性
     */
    private String check(String referralCode) {
        // 查询生成的推荐码在数据库是否存在，不存在就返回，存在重新生成再次判断
        UserExt userExt = userExtDao.getByReferralCode(referralCode);
        if (ObjectUtil.isNull(userExt)) {
            return referralCode;
        }
        return check(ReferralCodeUtil.getStringRandom());
    }

    public Result<String> sharingRecommended(AuthSharingBO bo) {
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("找不到用户信息");
        }

        ConfigWeixinVO configWeixinVO = feignSysConfig.getWeixin();
        if (StringUtils.isEmpty(configWeixinVO.getMinappAppId()) || StringUtils.isEmpty(configWeixinVO.getMinappAppSecret())) {
            return Result.error("appid或者appSecret没配置");
        }
        SharingTemplateQO sharingTemplateQO = new SharingTemplateQO();
        sharingTemplateQO.setIsUseTemplate(IsUseTemplateEnum.YES.getCode());
        sharingTemplateQO.setTemplateType(TemplateTypeEnum.USER.getCode());
        SharingTemplateVO sharingTemplateVO = feignSharingTemplate.getIsUseTemplateAndTemplateType(sharingTemplateQO);
        if (ObjectUtil.isNull(sharingTemplateVO)) {
            return Result.error("请配置分享图片模板");
        }
        try {
            // 底层模板
            File sharingImgFile = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), sharingTemplateVO.getTemplateUrl());

            File file;
            // 如果不存在微信二维码，生成
            if (StringUtils.isEmpty(userExt.getRecommendedCodeUrl())) {
                file = WxMaAppUtil.getService(configWeixinVO.getMinappAppId(), configWeixinVO.getMinappAppSecret(), WxMaAppUtil.AESKEY, myRedisTemplate.getStringRedisTemplate()).getQrcodeService().createWxaCodeUnlimit(userExt.getReferralCode(), "pages/index/index", 430, true, null, true);
                userExt.setRecommendedCodeUrl(AliyunUtil.uploadPic(PlatformEnum.USER, file, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class)));
                userExtDao.updateById(userExt);
            } else {
                file = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), userExt.getRecommendedCodeUrl());
            }
            // 压缩小程序，指定长宽
            ImgUtil.assignImage(file, 130, 130);
            // 设置小程序水印
            ImgUtil.pressImage(file, sharingImgFile, sharingTemplateVO.getPicx(), sharingTemplateVO.getPicy());
            if (file.isFile() && file.exists()) {
                if (!file.delete()) {
                    logger.error("删除本地文件失败");
                    return Result.error("生成图片失败，请重试");
                }
            }
            InputStream in = null;
            byte[] data;
            // 读取图片字节数组
            try {
                in = new FileInputStream(sharingImgFile);
                data = new byte[in.available()];
                int count = 0;
                while ((count = in.read(data)) > 0) {
                    // 对字节数组Base64编码
                    return Result.success("data:image/" + sharingImgFile.getName().toLowerCase() + ";base64, " + Base64Utils.encodeToString(data));
                }
            } catch (IOException e) {
                logger.error("系统错误", e);
            } finally {
                in.close();
            }
            return Result.error("生成图片失败，请重试");
        } catch (Exception e) {
            logger.error("生成图片失败", e);
            return Result.error("生成图片失败，请重试");
        }
    }

    /**
     * 用户注销
     *
     * @param bo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> logout(AuthUserLogoutBO bo) {
        // new Date()为获取当前系统时间，也可使用当前时间戳
        Long date = System.currentTimeMillis() / 1000;
        String mobile = "d" + date.toString();
        Integer statusId = Constants.FREEZE;
        // 用户教育信息
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(userExt)) {
            userExt.setStatusId(statusId);
            userExt.setMobile(mobile);
            userExtDao.updateById(userExt);
        }
        // 用户基本信息
        User user = userDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(user)) {
            user.setStatusId(statusId);
            user.setMobile(mobile);
            userDao.updateById(user);
        }
        // 用户账户
        UserAccount userAccount = userAccountDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(userAccount)) {
            userAccount.setStatusId(statusId);
            userAccountDao.updateById(userAccount);
        }
        // 提现记录
        List<UserAccountExtractLog> userAccountExtractLogList = userAccountExtractLogDao.listByUserNo(ThreadContext.userNo());
        if (CollectionUtil.isNotEmpty(userAccountExtractLogList)) {
            userAccountExtractLogDao.updateByMobile(ThreadContext.userNo(), mobile);
        }
        // 讲师信息
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(lecturer)) {
            lecturer.setStatusId(statusId);
            lecturer.setLecturerMobile(mobile);
            lecturerDao.updateById(lecturer);
        }
        // 讲师审核信息
        LecturerAudit lecturerAudit = lecturerAuditDao.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(lecturerAudit)) {
            lecturerAudit.setStatusId(statusId);
            lecturerAudit.setLecturerMobile(mobile);
            lecturerAuditDao.updateById(lecturerAudit);
        }
        List<OrderInfo> orderInfo = orderInfoDao.listByUserNo(ThreadContext.userNo());
        if (CollectionUtil.isNotEmpty(orderInfo)) {
            orderInfoDao.updateByMobile(ThreadContext.userNo(), mobile);
        }
        OrderLogQO orderLogQO = new OrderLogQO();
        orderLogQO.setMobile(mobile);
        orderLogQO.setUserNo(ThreadContext.userNo());
        feignOrderLog.updateByMobile(orderLogQO);

        return Result.success("注销成功");
    }

    public Result<String> faceContras(AuthFaceContrasBO bo) throws Exception {
        if (StringUtils.isEmpty(bo.getVideoBase64())) {
            return Result.error("人脸识别不能为空!");
        }
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户信息不能为空!");
        }
        //如果个人信息没上传身份证信息则先上传
        if (StringUtils.isEmpty(userExt.getIdCardNo()) && StringUtils.isEmpty(userExt.getIdCardAfterImg()) && StringUtils.isEmpty(userExt.getIdCardFrontImg())) {
            return Result.error(ResultEnum.REAL_NAME.getCode(), "请先上传实名信息!");
        }

        ConfigTencentVO configTencentVO = feignSysConfig.getTencent();
        if (StringUtils.isEmpty(configTencentVO.getTencentSecretId()) || StringUtils.isEmpty(configTencentVO.getTencentSecretKey())) {
            return Result.error("人脸对比参数配置错误!");
        }

        //如果用户第一次认证，则上传图片到阿里云
        if (StringUtils.isEmpty(userExt.getFaceContrasImg()) && !StringUtils.isEmpty(bo.getImageBase64())) {
            saveFaceImage(bo.getImageBase64(), userExt, configTencentVO);
        }
        FaceContrastResult faceContrastResult = FaceContrastUtil.contrans(configTencentVO.getTencentSecretId(), configTencentVO.getTencentSecretKey(), encodeBase64File(userExt.getFaceContrasImg()), bo.getVideoBase64());

        //更新人脸识别结果
        CourseLogQO courseLogQO = new CourseLogQO();
        courseLogQO.setUserNo(ThreadContext.userNo());
        courseLogQO.setPeriodId(bo.getPeriodId());
        CourseLogVO courseLogVO = feignCourseLog.getByUserNoAndPeriodIdLatest(courseLogQO);
        if (ObjectUtil.isNotEmpty(courseLogVO)) {
            if (FaceContrastResultEnum.SUCCESS.getCode().equals(faceContrastResult.getResult())) {
                courseLogVO.setContrastResult("是本人");
            } else if (FaceContrastResultEnum.ACTIONNODETECTFACE.getCode().equals(faceContrastResult.getResult()) || FaceContrastResultEnum.LIFEPHOTODETECTNOFACES.getCode().equals(faceContrastResult.getResult())) {
                courseLogVO.setContrastResult("检测不到人脸");
            } else {
                courseLogVO.setContrastResult("人脸对比不通过");
            }
            if (courseLogVO.getResidueContrastTotal() > 0) {
                courseLogVO.setResidueContrastTotal(courseLogVO.getResidueContrastTotal() - 1);
            }
            feignCourseLog.updateById(BeanUtil.copyProperties(courseLogVO, CourseLogQO.class));
        }

        //如果不为success则对比失败
        if (!"Success".equals(faceContrastResult.getResult())) {
            String result = FaceContrastResultEnum.getContrastResult(faceContrastResult.getResult()).getDesc();
            return Result.error(result);
        }
        //如果相似度小于70%则提示对比失败
        if (faceContrastResult.getSim() < 70 && FaceContrastResultEnum.COMPARELOWSIMILARITY.getCode().equals(faceContrastResult.getResult())) {
            return Result.error("人脸对比失败,相似度未达到通过标准!");
        }
        myRedisTemplate.set(userExt.getUserNo().toString() + bo.getPeriodId().toString(), "true", 1, TimeUnit.MINUTES);
        return Result.success(faceContrastResult.getResult());
    }

    public Result<String> faceContrasCode(AuthFaceContrasCodeBO bo) throws WxErrorException {
        if (StringUtils.isEmpty(bo.getPeriodId())) {
            return Result.error("课时不能为空!");
        }
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户信息不能为空!");
        }

        ConfigWeixinVO configWeixinVO = feignSysConfig.getWeixin();
        if (ObjectUtil.isNull(configWeixinVO)) {
            return Result.error("找不到系统配置信息");
        }
        myRedisTemplate.set(ThreadContext.userNo().toString() + bo.getPeriodId().toString(), "false", 1, TimeUnit.MINUTES);
        File file = WxMaAppUtil.getService(configWeixinVO.getMinappAppId(), configWeixinVO.getMinappAppSecret(), WxMaAppUtil.AESKEY, myRedisTemplate.getStringRedisTemplate()).getQrcodeService().createWxaCodeUnlimit(bo.getPeriodId().toString(), bo.getPage(), 430, true, null, true);
        // 上传到阿里云
        ConfigAliyunVO configAliyunVO = feignSysConfig.getAliyun();
        if (ObjectUtil.isNull(configAliyunVO)) {
            return Result.error("找不到系统配置信息");
        }
        String url = AliyunUtil.uploadPic(PlatformEnum.USER, file, BeanUtil.copyProperties(configAliyunVO, Aliyun.class));
        return Result.success(url);
    }

    public Result<String> faceContrasPass(AuthFaceContrasPassBO bo) {
        if (StringUtils.isEmpty(bo.getPeriodId())) {
            return Result.error("课时不能为空!");
        }
        UserExt userExt = userExtDao.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("用户信息不能为空!");
        }
        String value = myRedisTemplate.get(ThreadContext.userNo().toString() + bo.getPeriodId().toString());
        if ("true".equals(value)) {
            return Result.success("校验通过!");
        }
        return Result.success("");
    }

    /**
     * 把base64文件下载到本地
     *
     * @param base64Code
     * @throws Exception
     */
    @Transactional
    public void saveFaceImage(String base64Code, UserExt userExt, ConfigTencentVO configTencentVO) throws Exception {
        // 下载到本地
        File targetFile = new File(configTencentVO.getFaceContrasPath() + userExt.getUserNo() + ".jpg");
        targetFile.setLastModified(System.currentTimeMillis());
        // 判断文件目录是否存在，不存在就创建文件目录
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }

        byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
        FileOutputStream out = new FileOutputStream(targetFile.getPath());
        out.write(buffer);
        out.close();

        ConfigAliyunVO configAliyunVO = feignSysConfig.getAliyun();
        if (ObjectUtil.isEmpty(configAliyunVO)) {
            throw new BaseException("获取阿里云信息失败!");
        }

        // 上传到阿里云
        Aliyun aliyun = BeanUtil.copyProperties(configAliyunVO, Aliyun.class);
        aliyun.setAliyunOssBucket(configTencentVO.getFaceContrasBucketName());
        aliyun.setAliyunOssUrl(configTencentVO.getFaceContrasBucketUrl());
        String fileUrl = AliyunUtil.uploadPic(PlatformEnum.FACE, targetFile, aliyun);
        if (StringUtils.isEmpty(fileUrl)) {
            //上传失败
            throw new BaseException("上传失败!");
        }
        //fileUrl = fileUrl.replace("https://static.zhuoyi100.cn/", configTencentVO.getFaceContrasBucketUrl());
        userExt.setFaceContrasImg(fileUrl);
        userExtDao.updateById(userExt);

        //成功删除本地文件
        if (targetFile.isFile() && targetFile.exists()) {
            if (!targetFile.delete()) {
                logger.error("删除本地文件失败");
            }
        }
    }

    public static String encodeBase64File(String imgURL) {
        ByteArrayOutputStream outPut = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        try {
            // 创建URL
            URL url = new URL(imgURL);
            // 创建链接
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(10 * 1000);

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //连接失败/链接失效/图片不存在
                return "fail";
            }
            InputStream inStream = conn.getInputStream();
            int len;
            while ((len = inStream.read(data)) != -1) {
                outPut.write(data, 0, len);
            }
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(outPut.toByteArray());
    }

    public Result<String> faceContrasFail(AuthFaceContrasFailBO bo) {
        CourseLogQO courseLogQO = new CourseLogQO();
        courseLogQO.setUserNo(ThreadContext.userNo());
        courseLogQO.setPeriodId(bo.getPeriodId());
        CourseLogVO courseLogVO = feignCourseLog.getByUserNoAndPeriodIdLatest(courseLogQO);
        if (ObjectUtil.isNotEmpty(courseLogVO)) {
            courseLogVO.setContrastResult("检测不到人脸");
            if (courseLogVO.getResidueContrastTotal() > 0) {
                courseLogVO.setResidueContrastTotal(courseLogVO.getResidueContrastTotal() - 1);
            }
            feignCourseLog.updateById(BeanUtil.copyProperties(courseLogVO, CourseLogQO.class));
        }
        return Result.success("标记成功");
    }
}
