package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.LecturerAuditEditREQ;
import com.roncoo.education.user.common.req.LecturerAuditListREQ;
import com.roncoo.education.user.common.req.LecturerAuditREQ;
import com.roncoo.education.user.common.req.LecturerAuditSaveREQ;
import com.roncoo.education.user.common.resp.LecturerAuditListRESP;
import com.roncoo.education.user.common.resp.LecturerAuditViewRESP;
import com.roncoo.education.user.service.pc.biz.PcLecturerAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 讲师信息-审核 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/lecturer/audit")
@Api(value = "user-讲师信息-审核", tags = {"user-讲师信息-审核"})
public class PcLecturerAuditController {

    @Autowired
    private PcLecturerAuditBiz biz;

    @ApiOperation(value = "讲师信息-审核列表", notes = "讲师信息-审核列表")
    @PostMapping(value = "/list")
    public Result<Page<LecturerAuditListRESP>> list(@RequestBody LecturerAuditListREQ lecturerAuditListREQ) {
        return biz.list(lecturerAuditListREQ);
    }

    @ApiOperation(value = "讲师信息-审核添加", notes = "讲师信息-审核添加")
    @PostMapping(value = "/save")
    @SysLog(value = "讲师信息-审核添加")
    public Result<String> save(@RequestBody LecturerAuditSaveREQ lecturerAuditSaveREQ) {
        return biz.save(lecturerAuditSaveREQ);
    }

    @ApiOperation(value = "讲师信息-审核查看", notes = "讲师信息-审核查看")
    @GetMapping(value = "/view")
    public Result<LecturerAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "讲师信息-审核修改", notes = "讲师信息-审核修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "讲师信息-审核修改")
    public Result<String> edit(@RequestBody LecturerAuditEditREQ lecturerAuditEditREQ) {
        return biz.edit(lecturerAuditEditREQ);
    }

    @ApiOperation(value = "讲师信息-审核删除", notes = "讲师信息-审核删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "讲师信息-审核删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "讲师信息-讲师审核", notes = "讲师信息-讲师审核")
    @PostMapping(value = "/audit")
    @SysLog(value = "讲师信息-讲师审核")
    public Result<String> audit(@RequestBody LecturerAuditREQ lecturerAuditREQ) {
        return biz.audit(lecturerAuditREQ);
    }

    @ApiOperation(value = "讲师信息-校验手机", notes = "讲师信息-校验手机")
    @PostMapping(value = "/checkUserAndLecturer")
    public Result<Boolean> checkUserAndLecturer(@RequestParam String lecturerMobile) {
        return biz.checkUserAndLecturer(lecturerMobile);
    }

}
