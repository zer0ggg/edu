package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户提现日志表_分页列出
 */
@Data
@Accessors(chain = true)
public class AuthUserAccountExtractLogPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;
    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userNo;
    /**
     * 银行卡号
     */
    @ApiModelProperty(value = "银行卡号")
    private String bankCardNo;
    /**
     * 银行名称
     */
    @ApiModelProperty(value = "银行名称")
    private String bankName;
    /**
     * 银行支行名称
     */
    @ApiModelProperty(value = "银行支行名称")
    private String bankBranchName;
    /**
     * 银行开户名
     */
    @ApiModelProperty(value = "银行开户名")
    private String bankUserName;
    /**
     * 银行身份证号
     */
    @ApiModelProperty(value = "银行身份证号")
    private String bankIdCardNo;
    /**
     * 提现金额
     */
    @ApiModelProperty(value = "提现金额")
    private BigDecimal extractMoney;
    /**
     * 用户收入
     */
    @ApiModelProperty(value = "用户收入")
    private BigDecimal userIncome;
    /**
     * 提现状态（1申请中，2支付中，3确认中，4成功，5失败）
     */
    @ApiModelProperty(value = "提现状态（1申请中，2支付中，3确认中，4成功，5失败）")
    private Integer extractStatus;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
