package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserLecturerAttention;
import com.roncoo.education.user.feign.qo.UserLecturerAttentionQO;
import com.roncoo.education.user.feign.vo.UserLecturerAttentionVO;
import com.roncoo.education.user.service.feign.biz.FeignUserLecturerAttentionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@RestController
public class FeignUserLecturerAttentionController extends BaseController implements IFeignUserLecturerAttention {

	@Autowired
	private FeignUserLecturerAttentionBiz biz;

	@Override
	public Page<UserLecturerAttentionVO> listForPage(@RequestBody UserLecturerAttentionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserLecturerAttentionQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserLecturerAttentionQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public UserLecturerAttentionVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public UserLecturerAttentionVO getByUserAndLecturerUserNo(@RequestBody UserLecturerAttentionQO qo) {
		return biz.getByUserAndLecturerUserNo(qo);
	}

}
