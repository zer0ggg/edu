package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearch;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearchExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserResearchMapper {
    int countByExample(UserResearchExample example);

    int deleteByExample(UserResearchExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserResearch record);

    int insertSelective(UserResearch record);

    List<UserResearch> selectByExampleWithBLOBs(UserResearchExample example);

    List<UserResearch> selectByExample(UserResearchExample example);

    UserResearch selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserResearch record, @Param("example") UserResearchExample example);

    int updateByExampleWithBLOBs(@Param("record") UserResearch record, @Param("example") UserResearchExample example);

    int updateByExample(@Param("record") UserResearch record, @Param("example") UserResearchExample example);

    int updateByPrimaryKeySelective(UserResearch record);

    int updateByPrimaryKeyWithBLOBs(UserResearch record);

    int updateByPrimaryKey(UserResearch record);
}