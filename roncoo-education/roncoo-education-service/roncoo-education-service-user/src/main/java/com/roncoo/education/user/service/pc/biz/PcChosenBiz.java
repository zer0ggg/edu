package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.user.common.req.ChosenEditREQ;
import com.roncoo.education.user.common.req.ChosenListREQ;
import com.roncoo.education.user.common.req.ChosenSaveREQ;
import com.roncoo.education.user.common.req.ChosenUpdateStatusREQ;
import com.roncoo.education.user.common.resp.ChosenListRESP;
import com.roncoo.education.user.common.resp.ChosenViewRESP;
import com.roncoo.education.user.service.dao.ChosenDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Chosen;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分销信息
 *
 * @author wujing
 */
@Component
public class PcChosenBiz extends BaseBiz {

    @Autowired
    private ChosenDao dao;

    @Autowired
    private IFeignCourse feignCourse;

    /**
     * 分销信息列表
     *
     * @param chosenListREQ 分销信息分页查询参数
     * @return 分销信息分页查询结果
     */
    public Result<Page<ChosenListRESP>> list(ChosenListREQ chosenListREQ) {
        ChosenExample example = new ChosenExample();
        Criteria c = example.createCriteria();
        // 课程ID
        if (ObjectUtil.isNotEmpty(chosenListREQ.getCourseId())) {
            c.andCourseIdEqualTo(chosenListREQ.getCourseId());
        }
        example.setOrderByClause("id desc");
        Page<Chosen> page = dao.listForPage(chosenListREQ.getPageCurrent(), chosenListREQ.getPageSize(), example);
        Page<ChosenListRESP> respPage = PageUtil.transform(page, ChosenListRESP.class);
        for (ChosenListRESP chosenListRESP : respPage.getList()) {
            CourseVO courseVO = feignCourse.getById(chosenListRESP.getCourseId());
            if (ObjectUtil.isNotNull(courseVO)) {
                chosenListRESP.setCourseName(courseVO.getCourseName());
                chosenListRESP.setCourseLogo(courseVO.getCourseLogo());
                chosenListRESP.setCourseCategory(courseVO.getCourseCategory());
                chosenListRESP.setCourseDiscount(courseVO.getCourseDiscount());
                chosenListRESP.setCourseOriginal(courseVO.getCourseOriginal());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 分销信息添加
     *
     * @param chosenSaveREQ 分销信息
     * @return 添加结果
     */
    public Result<String> save(ChosenSaveREQ chosenSaveREQ) {
        if (ObjectUtil.isNotNull(dao.getByCourseId(chosenSaveREQ.getCourseId()))) {
            return Result.error("该课程已存在");
        }
        Chosen record = BeanUtil.copyProperties(chosenSaveREQ, Chosen.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 分销信息查看
     *
     * @param id 主键ID
     * @return 分销信息
     */
    public Result<ChosenViewRESP> view(Long id) {
        ChosenViewRESP chosenViewRESP = BeanUtil.copyProperties(dao.getById(id), ChosenViewRESP.class);
        chosenViewRESP.setCourseInfo(feignCourse.getById(chosenViewRESP.getCourseId()));
        return Result.success(chosenViewRESP);
    }


    /**
     * 分销信息修改
     *
     * @param chosenEditREQ 分销信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(ChosenEditREQ chosenEditREQ) {
        Chosen record = BeanUtil.copyProperties(chosenEditREQ, Chosen.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 分销信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 修改状态
     *
     * @param chosenUpdateStatusREQ 分销信息修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(ChosenUpdateStatusREQ chosenUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(chosenUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(chosenUpdateStatusREQ.getId()))) {
            return Result.error("渠道不存在");
        }
        Chosen record = BeanUtil.copyProperties(chosenUpdateStatusREQ, Chosen.class);
        if (dao.updateById(record) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }
}
