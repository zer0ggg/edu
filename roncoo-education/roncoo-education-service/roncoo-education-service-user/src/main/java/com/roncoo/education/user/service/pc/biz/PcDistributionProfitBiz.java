package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.common.req.DistributionProfitEditREQ;
import com.roncoo.education.user.common.req.DistributionProfitListREQ;
import com.roncoo.education.user.common.req.DistributionProfitSaveREQ;
import com.roncoo.education.user.common.resp.DistributionProfitListRESP;
import com.roncoo.education.user.common.resp.DistributionProfitViewRESP;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.service.dao.DistributionDao;
import com.roncoo.education.user.service.dao.DistributionProfitDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Distribution;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfit;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.DistributionProfitExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 代理分润记录
 *
 * @author wujing
 */
@Component
public class PcDistributionProfitBiz extends BaseBiz {

    @Autowired
    private DistributionDao distributionDao;
    @Autowired
    private DistributionProfitDao dao;
    @Autowired
    private UserExtDao userExtDao;

    /**
     * 代理分润记录列表
     *
     * @param distributionProfitListREQ 代理分润记录分页查询参数
     * @return 代理分润记录分页查询结果
     */
    public Result<Page<DistributionProfitListRESP>> list(DistributionProfitListREQ distributionProfitListREQ) {
        DistributionProfitExample example = new DistributionProfitExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(distributionProfitListREQ.getMobile())) {
            List<UserExt> userExtList = userExtDao.listByLikeMobile(distributionProfitListREQ.getMobile());
            if (CollectionUtil.isEmpty(userExtList)) {
                return Result.success(new Page<>());
            } else {
                Set<Long> userNos = userExtList.stream().map(UserExt::getUserNo).collect(Collectors.toSet());
                c.andUserNoIn(new ArrayList<>(userNos));
            }
        }
        if (StringUtils.hasText(distributionProfitListREQ.getParentMobile())) {
            List<UserExt> userExtList = userExtDao.listByLikeMobile(distributionProfitListREQ.getParentMobile());
            if (CollectionUtil.isEmpty(userExtList)) {
                return Result.success(new Page<>());
            } else {
                Set<Long> userNos = userExtList.stream().map(UserExt::getUserNo).collect(Collectors.toSet());
                List<Distribution> distributionProfitList = distributionDao.listByUserNos(new ArrayList<>(userNos));
                if (CollectionUtil.isEmpty(distributionProfitList)) {
                    return Result.success(new Page<>());
                } else {
                    Set<Long> parentIds = distributionProfitList.stream().map(Distribution::getId).collect(Collectors.toSet());
                    c.andParentIdIn(new ArrayList<>(parentIds));
                }
            }
        }
        if (StringUtils.hasText(distributionProfitListREQ.getProductName())) {
            c.andProductNameLike(PageUtil.like(distributionProfitListREQ.getProductName()));
        }
        if (distributionProfitListREQ.getProfitType() != null) {
            c.andProfitTypeEqualTo(distributionProfitListREQ.getProfitType());
        }
        if (distributionProfitListREQ.getProductType() != null) {
            c.andProductTypeEqualTo(distributionProfitListREQ.getProductType());
        }
        Page<DistributionProfit> page = dao.listForPage(distributionProfitListREQ.getPageCurrent(), distributionProfitListREQ.getPageSize(), example);
        Page<DistributionProfitListRESP> respPage = PageUtil.transform(page, DistributionProfitListRESP.class);
        if (respPage == null || respPage.getList().isEmpty()) {
            return Result.success(respPage);
        }
        Set<Long> userNos = respPage.getList().stream().map(DistributionProfitListRESP::getUserNo).collect(Collectors.toSet());
        // 获取用户手机号
        List<UserExt> userExtList = userExtDao.listByUserNos(new ArrayList<>(userNos));
        if (CollectionUtil.isNotEmpty(userExtList)) {
            Map<Long, String> userExtMap = userExtList.stream().collect(Collectors.toMap(UserExt::getUserNo, item -> item.getMobile()));
            respPage.getList().stream().forEach(item -> {
                String mobile = userExtMap.get(item.getUserNo());
                item.setMobile(StringUtils.hasText(mobile) ? mobile : "");
            });
        }
        Set<Long> parentIds = respPage.getList().stream().map(DistributionProfitListRESP::getParentId).collect(Collectors.toSet());
        // 获取推荐人手机号
        List<Distribution> distributionProfitList = distributionDao.listByIds(new ArrayList<>(parentIds));
        if (CollectionUtil.isNotEmpty(distributionProfitList)) {
            Set<Long> parentUserNos = distributionProfitList.stream().map(Distribution::getUserNo).collect(Collectors.toSet());
            List<UserExt> parentUserExtList = userExtDao.listByUserNos(new ArrayList<>(parentUserNos));
            Map<Long, String> userExtMap = parentUserExtList.stream().collect(Collectors.toMap(UserExt::getUserNo, item -> item.getMobile()));
            Map<Long, String> map = new HashMap<>();
            distributionProfitList.forEach(item -> {
                String mobile = userExtMap.get(item.getUserNo());
                if (StringUtils.hasText(mobile)) {
                    map.put(item.getId(), mobile);
                }
            });
            respPage.getList().stream().forEach(item -> {
                String mobile = map.get(item.getParentId());
                if (StringUtils.hasText(mobile)) {
                    item.setParentMobile(StringUtils.hasText(mobile) ? mobile : "");
                }
            });
        }
        return Result.success(respPage);
    }


    /**
     * 代理分润记录添加
     *
     * @param distributionProfitSaveREQ 代理分润记录
     * @return 添加结果
     */
    public Result<String> save(DistributionProfitSaveREQ distributionProfitSaveREQ) {
        DistributionProfit record = BeanUtil.copyProperties(distributionProfitSaveREQ, DistributionProfit.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 代理分润记录查看
     *
     * @param id 主键ID
     * @return 代理分润记录
     */
    public Result<DistributionProfitViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), DistributionProfitViewRESP.class));
    }


    /**
     * 代理分润记录修改
     *
     * @param distributionProfitEditREQ 代理分润记录修改对象
     * @return 修改结果
     */
    public Result<String> edit(DistributionProfitEditREQ distributionProfitEditREQ) {
        DistributionProfit record = BeanUtil.copyProperties(distributionProfitEditREQ, DistributionProfit.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 代理分润记录删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
