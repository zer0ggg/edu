package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfo;
import com.roncoo.education.user.service.dao.impl.mapper.entity.OrderInfoExample;

import java.math.BigDecimal;
import java.util.List;

public interface OrderInfoDao {
    int save(OrderInfo record);

    int deleteById(Long id);

    int updateById(OrderInfo record);

    OrderInfo getById(Long id);

    Page<OrderInfo> listForPage(int pageCurrent, int pageSize, OrderInfoExample example);

    OrderInfo getByUserNoAndProductId(Long userNo, Long productId);

    /**
     * 根据订单编号查找订单信息
     *
     * @param orderNo
     * @return
     */
    OrderInfo getByOrderNo(long orderNo);

    /**
     * 统计时间段内该讲师该类型的订单收益
     *
     * @param lecturerUserNo
     * @param date
     * @return
     * @author YZJ
     */
    BigDecimal sumByLecturerUserNoAndCourseCategoryAndData(Long lecturerUserNo, Integer courseCategory, String date);

    /**
     * 统计时间段内的总订单数
     *
     * @param date
     * @return
     * @author wuyun
     */
    Integer sumByCountOrders(String date);

    /**
     * 统计时间段的总收入
     *
     * @param date
     * @return
     * @author wuyun
     */
    BigDecimal sumByPayTime(String date);

    /**
     * 根据流水号查找订单信息
     *
     * @param serialNumber
     * @return
     */
    OrderInfo getBySerialNumber(long serialNumber);

    /**
     * 根据用户编号获取用户订单信息
     *
     * @param userNo
     * @return
     */
    List<OrderInfo> listByUserNo(Long userNo);
    /**
     * 根据用户编号更新手机号
     *
     * @param userNo
     * @param mobile
     *
     * @return
     */
    int updateByMobile(Long userNo, String mobile);
}
