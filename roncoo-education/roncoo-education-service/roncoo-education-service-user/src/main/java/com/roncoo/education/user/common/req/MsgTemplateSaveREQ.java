package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 消息模板
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="MsgTemplateSaveREQ", description="消息模板添加")
public class MsgTemplateSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模板类型（1：课程审核；2：活动推送）")
    private Integer msgTemplateType;

    @ApiModelProperty(value = "是否使用模板（1:是；0:否）")
    private Integer isUseTemplate;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "备注")
    private String remark;
}
