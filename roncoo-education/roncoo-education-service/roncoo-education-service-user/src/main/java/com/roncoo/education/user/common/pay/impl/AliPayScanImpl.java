package com.roncoo.education.user.common.pay.impl;

import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.roncoo.education.common.core.base.Base;
import com.roncoo.education.common.core.enums.PayNetWorkStatusEnum;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.bo.PayNotifyBO;
import com.roncoo.education.user.common.bo.PayOrderBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.dto.PayNotifyDTO;
import com.roncoo.education.user.common.dto.PayOrderDTO;
import com.roncoo.education.user.common.pay.PayFacade;
import com.roncoo.education.user.common.pay.util.AliPayUtil;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 官方支付宝扫码
 *
 * @author Quanf
 * 2020/3/31 17:59
 */
@Service("ALI_PAY_SCAN")
public class AliPayScanImpl extends Base implements PayFacade {

    @Autowired
    private PayChannelDao payChannelDao;

    @Override
    public PayDTO pay(PayBO payBO) {
        logger.info("官方支付宝扫码:{}", payBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(payBO.getPayChannelCode());
        PayDTO payDTO = new PayDTO();
        payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.FAILED);
        AlipayTradePrecreateResponse aliPay = AliPayUtil.scanPay(payChannel, payBO);
        logger.debug("官方支付宝扫码:{}", aliPay.getQrCode());
        if (aliPay.isSuccess()) {
            payDTO.setPayMessage(aliPay.getQrCode());
            payDTO.setPayNetWorkStatusEnum(PayNetWorkStatusEnum.SUCCESS);
        }
        return payDTO;
    }

    @Override
    public PayNotifyDTO verify(PayNotifyBO notifyBO) {
        logger.debug("官方支付宝扫码通知:[{}]", notifyBO);
        PayChannel payChannel = payChannelDao.getByPayChannelCode(notifyBO.getPayChannelCode());
        return AliPayUtil.notifyCheck(notifyBO, payChannel.getField1());
    }

    @Override
    public PayOrderDTO order(PayOrderBO orderBO) {
        logger.debug("官方支付宝扫码查询:[{}]", orderBO);
        PayOrderDTO payOrderDTO = new PayOrderDTO();
        PayChannel payChannel = payChannelDao.getByPayChannelCode(orderBO.getPayChannelCode());
        int orderStatus = AliPayUtil.orderQuery(orderBO.getOrderNo(), payChannel);
        payOrderDTO.setOrderStatus(orderStatus);
        return payOrderDTO;
    }
}
