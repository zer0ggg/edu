package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.user.common.req.UserLogSmsEditREQ;
import com.roncoo.education.user.common.req.UserLogSmsListREQ;
import com.roncoo.education.user.common.req.UserLogSmsSaveREQ;
import com.roncoo.education.user.common.resp.UserLogSmsListRESP;
import com.roncoo.education.user.common.resp.UserLogSmsViewRESP;
import com.roncoo.education.user.feign.vo.UserLogSmsVO;
import com.roncoo.education.user.service.pc.biz.PcUserLogSmsBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户发送短信日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/user/pc/user/log/sms")
@Api(value = "user-用户发送短信日志", tags = {"user-用户发送短信日志"})
public class PcUserLogSmsController {

    @Autowired
    private PcUserLogSmsBiz biz;

    @ApiOperation(value = "用户发送短信日志列表", notes = "用户发送短信日志列表")
    @PostMapping(value = "/list")
    public Result<Page<UserLogSmsListRESP>> list(@RequestBody UserLogSmsListREQ userLogSmsListREQ) {
        return biz.list(userLogSmsListREQ);
    }

    @ApiOperation(value = "用户发送短信统计", notes = "用户发送短信统计")
    @PostMapping(value = "/count")
    public Result<UserLogSmsVO> countIncome() {
        return biz.countIncome();
    }

    @ApiOperation(value = "发送短信", notes = "发送短信")
    @GetMapping(value = "/send")
    @SysLog(value = "用户发送短信")
    public Result<String> send(@RequestParam String mobile) {
        return biz.send(mobile);
    }

    @ApiOperation(value = "用户发送短信日志添加", notes = "用户发送短信日志添加")
    @PostMapping(value = "/save")
    @SysLog(value = "用户发送短信日志添加")
    public Result<String> save(@RequestBody UserLogSmsSaveREQ userLogSmsSaveREQ) {
        return biz.save(userLogSmsSaveREQ);
    }

    @ApiOperation(value = "用户发送短信日志查看", notes = "用户发送短信日志查看")
    @GetMapping(value = "/view")
    public Result<UserLogSmsViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户发送短信日志修改", notes = "用户发送短信日志修改")
    @PutMapping(value = "/edit")
    @SysLog(value = "用户发送短信日志修改")
    public Result<String> edit(@RequestBody UserLogSmsEditREQ userLogSmsEditREQ) {
        return biz.edit(userLogSmsEditREQ);
    }

    @ApiOperation(value = "用户发送短信日志删除", notes = "用户发送短信日志删除")
    @DeleteMapping(value = "/delete")
    @SysLog(value = "用户发送短信日志删除")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
