package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignUserAccount;
import com.roncoo.education.user.feign.qo.UserAccountQO;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.feign.biz.FeignUserAccountBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户账户信息表
 *
 * @author wujing
 */
@RestController
public class FeignUserAccountController extends BaseController implements IFeignUserAccount {

	@Autowired
	private FeignUserAccountBiz biz;

	@Override
	public Page<UserAccountVO> listForPage(@RequestBody UserAccountQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserAccountQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserAccountQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public UserAccountVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public UserAccountVO getByUserNo(@RequestBody UserAccountQO qo) {
		return biz.getByUserNo(qo);
	}

	@Override
	public void updateTotalIncomeByUserNo(@RequestBody UserAccountQO userAccountQO) {
		biz.updateTotalIncomeByUserNo(userAccountQO);
	}

}
