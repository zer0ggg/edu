package com.roncoo.education.user.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthLecturerViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 讲师编号
	 */
	@ApiModelProperty(value = "讲师编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lecturerUserNo;
	/**
	 * 讲师名称
	 */
	@ApiModelProperty(value = "讲师名称")
	private String lecturerName;
	/**
	 * 电话
	 */
	@ApiModelProperty(value = "电话")
	private String lecturerMobile;
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	/**
	 * 邮箱
	 */
	@ApiModelProperty(value = "邮箱")
	private String lecturerEmail;
	/**
	 * 职位
	 */
	@ApiModelProperty(value = "职位")
	private String position;
	/**
	 * 简介
	 */
	@ApiModelProperty(value = "简介")
	private String introduce;
	/**
	 * 是否拥有直播功能（0：否，1: 是）
	 */
	@ApiModelProperty(value = "是否拥有直播功能（0：否，1: 是）")
	private Integer isLive;
	/**
	 * 频道id
	 */
	@ApiModelProperty(value = "频道id")
    private String channelId;
	/**
	 * 频道密码
	 */
	@ApiModelProperty(value = "频道密码")
    private String channelPasswd;
	/**
	 * 直播推流地址
	 */
	@ApiModelProperty(value = "直播推流地址")
	private String pushFlowUrl;
    /**
     * 直播状态(1:未开始;2:直播中;3:直播结束)
     */
	@ApiModelProperty(value = "直播状态(1:未开始;2:直播中;3:直播结束)")
    private Integer liveStatus;
	/**
	 * 三分屏频道id
	 */
	@ApiModelProperty(value = "三分屏频道id")
	private String pptChannelId;
	/**
	 * 三分屏频道密码
	 */
	@ApiModelProperty(value = "三分屏频道密码")
	private String pptChannelPasswd;

	// 补充信息 直播申请中
	@ApiModelProperty(value = "直播申请中标识(1申请中，0不处于申请中)")
	private boolean liveAudit =false;
}
