package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearch;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserResearchExample;

/**
 * 用户调研信息 服务类
 *
 * @author wujing
 * @date 2020-10-21
 */
public interface UserResearchDao {

    /**
    * 保存用户调研信息
    *
    * @param record 用户调研信息
    * @return 影响记录数
    */
    int save(UserResearch record);

    /**
    * 根据ID删除用户调研信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户调研信息
    * @return 影响记录数
    */
    int updateById(UserResearch record);

    /**
    * 根据ID获取用户调研信息
    *
    * @param id 主键ID
    * @return 用户调研信息
    */
    UserResearch getById(Long id);

    /**
    * 用户调研信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<UserResearch> listForPage(int pageCurrent, int pageSize, UserResearchExample example);

    /**
     * 根据用户编号获取用户调研信息
     * @param userNo
     * @return
     */
    UserResearch getByUserNo(Long userNo);
}
