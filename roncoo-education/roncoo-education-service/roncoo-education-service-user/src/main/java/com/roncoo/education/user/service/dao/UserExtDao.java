package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExtExample;

import java.util.List;

public interface UserExtDao {
    int save(UserExt record);

    int deleteById(Long id);

    int updateById(UserExt record);

    UserExt getById(Long id);

    Page<UserExt> listForPage(int pageCurrent, int pageSize, UserExtExample example);

    UserExt getByUserNo(Long userNo);

    int updateByUserNo(UserExt record);

    /**
     * 根据手机号码获取用户信息
     *
     * @param mobile
     * @return
     * @author wuyun
     */
    UserExt getByMobile(String mobile);

    /**
     * 获取用户注册量
     *
     * @param date
     * @return
     * @author wuyun
     */
    Integer sumByCountOrders(String date);

    /**
     * 列出所以可用用户信息
     *
     * @param statusId
     * @return
     */
    List<UserExt> listAllForUser(Integer statusId);

    /**
     * 根据推荐码获取用户信息
     *
     * @param referralCode
     * @return
     */
    UserExt getByReferralCode(String referralCode);

    /**
     * 批量获取用户信息
     *
     * @param userNos
     * @return
     */
    List<UserExt> listByUserNos(List<Long> userNos);

    /**
     * 手机号模糊查询
     *
     * @param mobile
     * @return
     */
    List<UserExt> listByLikeMobile(String mobile);
}
