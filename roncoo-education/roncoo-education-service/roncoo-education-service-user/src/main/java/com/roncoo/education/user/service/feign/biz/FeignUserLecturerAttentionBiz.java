package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.user.feign.qo.UserLecturerAttentionQO;
import com.roncoo.education.user.feign.vo.UserLecturerAttentionVO;
import com.roncoo.education.user.service.dao.LecturerDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.UserLecturerAttentionDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Lecturer;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserExt;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttention;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLecturerAttentionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@Component
public class FeignUserLecturerAttentionBiz {

	@Autowired
	private UserExtDao userExtDao;
	@Autowired
	private LecturerDao lecturerDao;

	@Autowired
	private UserLecturerAttentionDao dao;

	public Page<UserLecturerAttentionVO> listForPage(UserLecturerAttentionQO qo) {
		UserLecturerAttentionExample example = new UserLecturerAttentionExample();
		example.setOrderByClause(" id desc ");
		Page<UserLecturerAttention> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<UserLecturerAttentionVO> listForPage = PageUtil.transform(page, UserLecturerAttentionVO.class);
		for (UserLecturerAttentionVO vo : listForPage.getList()) {
			UserExt userExt = userExtDao.getByUserNo(vo.getUserNo());
			if (ObjectUtil.isNotNull(userExt) && StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
				vo.setUserMobile(userExt.getMobile());
			}
			Lecturer lecturer = lecturerDao.getByLecturerUserNo(vo.getLecturerUserNo());
			if (ObjectUtil.isNotNull(lecturer) && StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
				vo.setLecturerMobile(lecturer.getLecturerMobile());
				vo.setLecturerName(lecturer.getLecturerName());
			}
		}
		return listForPage;
	}

	public int save(UserLecturerAttentionQO qo) {
		UserLecturerAttention record = BeanUtil.copyProperties(qo, UserLecturerAttention.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserLecturerAttentionVO getById(Long id) {
		UserLecturerAttention record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserLecturerAttentionVO.class);
	}

	public int updateById(UserLecturerAttentionQO qo) {
		UserLecturerAttention record = BeanUtil.copyProperties(qo, UserLecturerAttention.class);
		return dao.updateById(record);
	}

	/**
	 * 根据用户编号、讲师编号获取用户关注讲师信息
	 *
	 * @param qo
	 * @return
	 */
	public UserLecturerAttentionVO getByUserAndLecturerUserNo(UserLecturerAttentionQO qo) {
		UserLecturerAttention record = dao.getByUserAndLecturerUserNo(qo.getUserNo(), qo.getLecturerUserNo());
		return BeanUtil.copyProperties(record, UserLecturerAttentionVO.class);
	}

}
