package com.roncoo.education.user.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 支付渠道信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayChannelSaveREQ", description="支付渠道信息添加")
public class PayChannelSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "支付渠道编码")
    private String payChannelCode;

    @ApiModelProperty(value = "支付渠道名称")
    private String payChannelName;

    @ApiModelProperty(value = "支付实现编码")
    private String payObjectCode;

    @ApiModelProperty(value = "支付实现名称")
    private String payObjectName;

    @ApiModelProperty(value = "支付编号")
    private String payKey;

    @ApiModelProperty(value = "支付密钥")
    private String paySecret;

    @ApiModelProperty(value = "请求地址")
    private String requestUrl;

    @ApiModelProperty(value = "查询地址")
    private String queryUrl;

    @ApiModelProperty(value = "备用字段1")
    private String field1;

    @ApiModelProperty(value = "备用字段2")
    private String field2;

    @ApiModelProperty(value = "备用字段3")
    private String field3;
}
