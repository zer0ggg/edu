package com.roncoo.education.user.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.PingyinUtil;
import com.roncoo.education.user.common.req.PayChannelEditREQ;
import com.roncoo.education.user.common.req.PayChannelListREQ;
import com.roncoo.education.user.common.req.PayChannelSaveREQ;
import com.roncoo.education.user.common.req.PayChannelUpdateStatusREQ;
import com.roncoo.education.user.common.resp.PayChannelListRESP;
import com.roncoo.education.user.common.resp.PayChannelListUsableRESP;
import com.roncoo.education.user.common.resp.PayChannelViewRESP;
import com.roncoo.education.user.service.dao.PayChannelDao;
import com.roncoo.education.user.service.dao.PayRuleDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayChannelExample.Criteria;
import com.roncoo.education.user.service.dao.impl.mapper.entity.PayRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

/**
 * 支付渠道信息
 *
 * @author wujing
 */
@Component
public class PcPayChannelBiz extends BaseBiz {

    @Autowired
    private PayChannelDao dao;

    @Autowired
    private PayRuleDao payRuleDao;

    /**
     * 支付渠道信息列表
     *
     * @param payChannelListREQ 支付渠道信息分页查询参数
     * @return 支付渠道信息分页查询结果
     */
    public Result<Page<PayChannelListRESP>> list(PayChannelListREQ payChannelListREQ) {
        PayChannelExample example = new PayChannelExample();
        Criteria c = example.createCriteria();
        if (StrUtil.isNotEmpty(payChannelListREQ.getPayChannelName())) {
            c.andPayChannelNameLike(PageUtil.like(payChannelListREQ.getPayChannelName()));
        }
        example.setOrderByClause("id desc");
        Page<PayChannel> page = dao.listForPage(payChannelListREQ.getPageCurrent(), payChannelListREQ.getPageSize(), example);
        Page<PayChannelListRESP> respPage = PageUtil.transform(page, PayChannelListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 支付渠道信息表添加
     *
     * @param req
     * @return
     */
    public Result<String> save(PayChannelSaveREQ req) {
        PayChannel record = BeanUtil.copyProperties(req, PayChannel.class);
        String payChannelCode = PingyinUtil.toPingyin(req.getPayChannelName());
        record.setPayChannelCode(payChannelCode);
        PayChannel payChannel = dao.getByPayChannelCode(payChannelCode);
        if (ObjectUtil.isNotEmpty(payChannel)) {
            return Result.error("渠道名称不可用");
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 支付渠道信息查看
     *
     * @param id 主键ID
     * @return 支付渠道信息
     */
    public Result<PayChannelViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), PayChannelViewRESP.class));
    }


    /**
     * 支付渠道信息修改
     *
     * @param payChannelEditREQ 支付渠道信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(PayChannelEditREQ payChannelEditREQ) {
        PayChannel record = BeanUtil.copyProperties(payChannelEditREQ, PayChannel.class);
        record.setGmtModified(new Date());
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 支付渠道信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        PayChannel payChannel = dao.getById(id);
        PayRule payRule = payRuleDao.getByPayChannelCode(payChannel.getPayChannelCode());
        if (ObjectUtil.isNotEmpty(payRule)) {
            return Result.error("请先删除交易配置中[" + payRule.getPayChannelName() + "]");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 获取可用渠道
     *
     * @return
     */
    public Result<ArrayList<PayChannelListUsableRESP>> listUsable() {
        ArrayList<PayChannelListUsableRESP> list = (ArrayList<PayChannelListUsableRESP>) PageUtil.copyList(dao.listUsable(), PayChannelListUsableRESP.class);
        return Result.success(list);
    }

    /**
     * 修改状态
     *
     * @param payChannelUpdateStatusREQ 支付渠道修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(PayChannelUpdateStatusREQ payChannelUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(payChannelUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(payChannelUpdateStatusREQ.getId()))) {
            return Result.error("渠道不存在");
        }
        PayChannel record = BeanUtil.copyProperties(payChannelUpdateStatusREQ, PayChannel.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
