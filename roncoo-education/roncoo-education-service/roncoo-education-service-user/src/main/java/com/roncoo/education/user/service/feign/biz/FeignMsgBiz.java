package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.system.feign.vo.MsgPushVO;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.vo.MsgVO;
import com.roncoo.education.user.feign.vo.UserExtMsgVO;
import com.roncoo.education.user.service.dao.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.MsgExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@Component
public class FeignMsgBiz extends BaseBiz {

    @Autowired
    private MsgDao dao;
    @Autowired
    private UserMsgDao userMsgDao;
    @Autowired
    private LecturerDao lecturerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MsgTemplateDao msgTemplateDao;


    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<MsgVO> listForPage(MsgQO qo) {
        MsgExample example = new MsgExample();
        Criteria c = example.createCriteria();
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        if (StringUtils.isNotEmpty(qo.getMsgTitle())) {
            c.andMsgTitleLike(PageUtil.rightLike(qo.getMsgTitle()));
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<Msg> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, MsgVO.class);
    }

    public int save(MsgQO qo) {
        Msg record = BeanUtil.copyProperties(qo, Msg.class);
        return dao.save(record);
    }

    @Transactional(rollbackFor = Exception.class)
    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public MsgVO getById(Long id) {
        Msg record = dao.getById(id);
        return BeanUtil.copyProperties(record, MsgVO.class);
    }

    public int updateById(MsgQO qo) {
        Msg record = BeanUtil.copyProperties(qo, Msg.class);
        return dao.updateById(record);
    }

    public List<MsgVO> listByStatusIdAndIsSendAndIsTimeSendAndSendTime() {
        List<Msg> msgList = dao.listByStatusIdAndIsSendAndIsTimeSendAndSendTime(StatusIdEnum.YES.getCode(), IsSendEnum.NO.getCode(), IsTimeSendEnum.YES.getCode(), new Date());
        return BeanUtil.copyProperties(msgList, MsgVO.class);
    }

    /**
     * 定时器任务：推送站内信到用户
     *
     * @return
     */
    @Transactional
    public int push() {
        List<Msg> list = dao.listByStatusIdAndIsSendAndIsTimeSendAndSendTime(StatusIdEnum.YES.getCode(), IsSendEnum.NO.getCode(), IsTimeSendEnum.YES.getCode(), new Date());
        List<MsgPushVO> msgList = ArrayListUtil.copy(list, MsgPushVO.class);
        if (CollectionUtil.isEmpty(msgList)) {
            //跳出
            return 0;
        }
        for (MsgPushVO vo : msgList) {
            // 进行推送前，将当前站内信推送状态置为已通知
            updateMsg(vo.getId());

            if (IsToPartEnum.YES.getCode().equals(vo.getIsToPart())) {
                // 部分类型，刷新用户站内信
                UserMsg userMsg = new UserMsg();
                userMsg.setIsSend(IsSendEnum.YES.getCode());
                UserMsgExample example = new UserMsgExample();
                UserMsgExample.Criteria c = example.createCriteria();
                c.andMsgIdEqualTo(vo.getId());
                userMsgDao.updateBySelective(userMsg, example);
            } else {
                //全量类型：新建用户站内信
                CALLBACK_EXECUTOR.execute(() -> pushToUserByMsgPush(vo));

            }
        }
        return 1;
    }

    /**
     * 分两种情况推送：全量推送与特定推送
     * 全量推送：需要新建userMsg
     * 特定推送：已存在userMsg，只需要将isSend置为true
     *
     * @param msgPush
     */
    private void pushToUserByMsgPush(MsgPushVO msgPush) {
        if (IsToPartEnum.YES.getCode().equals(msgPush.getIsToPart())) {
            //特定推送
            List<UserMsg> userMsgList = userMsgDao.listByMsgId(msgPush.getId());
            for (UserMsg userMsg : userMsgList) {
                userMsg.setIsSend(IsSendEnum.YES.getCode());
                userMsgDao.updateById(userMsg);
            }
        } else {
            //全量推送
            int num = getCacheNum();
            for (int i = 1; i < num + 1; i++) {
                List<UserExtMsgVO> list = list(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, UserExtMsgVO.class);
                if (CollectionUtil.isNotEmpty(list)) {
                    // 批量生成
                    for (UserExtMsgVO vo : list) {
                        saveUserMsg(msgPush, vo);
                    }
                }
            }
        }

    }

    private void saveUserMsg(MsgPushVO msg, UserExtMsgVO vo) {
        UserMsg record = new UserMsg();
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setMsgId(msg.getId());
        record.setMsgTitle(msg.getMsgTitle());
        record.setUserNo(vo.getUserNo());
        record.setMobile(vo.getMobile());
        record.setIsTop(msg.getIsTop());
        record.setIsSend(HasNoticeEnum.YES.getCode());
        userMsgDao.save(record);
    }

    private <T> List<T> list(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseArray(myRedisTemplate.get(key), clazz);
        }
        return null;
    }

    private boolean hasKey(Object key) {
        return myRedisTemplate.hasKey(key);
    }

    private <T> T get(String key, Class<T> clazz) {
        if (myRedisTemplate.hasKey(key)) {
            return JSUtil.parseObject(myRedisTemplate.get(key), clazz);
        }
        return null;
    }


    /**
     * 根据状态，角色获取可用的用户信息的集合
     *
     * @author wuyun
     */
    public void cacheUserForMsg() {
        int pageSize = 1000;
        Page<UserExtMsgVO> page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), 1, pageSize);
        // 缓存key条数
        set(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), page.getTotalPage(), 120);
        // 缓存用户
        for (int i = 1; i < page.getTotalPage() + 1; i++) {
            page = userDao.pageByStatusIdForMsg(StatusIdEnum.YES.getCode(), i, pageSize);
            // 缓存，2个小时
            set(RedisPreEnum.SYS_MSG_SEND.getCode() + "_" + i, page.getList(), 120);
        }
    }


    private void updateMsg(Long id) {
        Msg msgNew = new Msg();
        msgNew.setId(id);
        msgNew.setIsSend(HasNoticeEnum.YES.getCode());
        msgNew.setSendTime(new Date());
        dao.updateById(msgNew);
    }

    public int msgSendLecturer(MsgQO qo) {
        Lecturer lecturer = lecturerDao.getByLecturerUserNo(qo.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturer)) {
            return 0;
        }
        Msg msg = new Msg();
        // 获取使用的课程信息模板
        MsgTemplate msgTemplate = msgTemplateDao.getByMsgTemplateTypeAndIsUseTemplate(MsgTemplateTypeEnum.COURSE.getCode(), IsUseTemplateEnum.YES.getCode());
        if (ObjectUtil.isNotNull(msgTemplate) && StatusIdEnum.YES.getCode().equals(msgTemplate.getStatusId())) {
            msg.setMsgTitle(msgTemplate.getTitle());
            String msgText = msgTemplate.getRemark();
            if (msgText.contains("${name}")) {
                // 讲师名称
                if (org.springframework.util.StringUtils.isEmpty(lecturer.getLecturerName())) {
                    msgText = msgText.replace("${name}", lecturer.getLecturerMobile());
                } else {
                    msgText = msgText.replace("${name}", lecturer.getLecturerName());
                }
            }
            if (msgText.contains("${courseName}")) {
                // 课程名称
                msgText = msgText.replace("${courseName}", qo.getCourseName());
            }
            if (msgText.contains("${auditStatus}")) {
                // 审核状态
                if (AuditStatusEnum.SUCCESS.getCode().equals(qo.getAuditStatus())) {
                    msgText = msgText.replace("${auditStatus}", AuditStatusEnum.SUCCESS.getDesc());
                } else if (AuditStatusEnum.FAIL.getCode().equals(qo.getAuditStatus())) {
                    msgText = msgText.replace("${auditStatus}", AuditStatusEnum.FAIL.getDesc());
                }
            }
            if (msgText.contains("${courseId}")) {
                // 课程课程ID
                msgText = msgText.replace("${courseId}", qo.getCourseId().toString());
            }
            if (msgText.contains("${auditOpinion}")) {
                // 课程审核意见
                msgText = msgText.replace("${auditOpinion}", qo.getAuditOpinion());
            }
            msg.setMsgText(msgText);
        }
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsToPart(IsToPartEnum.YES.getCode());
        msg.setId(IdWorker.getId());
        dao.save(msg);

        UserMsg userMsg = new UserMsg();
        userMsg.setMsgId(msg.getId());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setUserNo(qo.getLecturerUserNo());
        userMsg.setMobile(lecturer.getLecturerMobile());
        userMsg.setIsTop(msg.getIsTop());
        if (IsTimeSendEnum.NO.getCode().equals(msg.getIsTimeSend())) {
            userMsg.setIsSend(IsSendEnum.YES.getCode());
        } else {
            userMsg.setIsSend(IsSendEnum.YES.getCode());
        }
        return userMsgDao.save(userMsg);
    }

    private int getCacheNum() {
        boolean flag = hasKey(RedisPreEnum.SYS_MSG_SEND_NUM.getCode());
        if (!flag) {
            // 找不到，去缓存用户信息
            cacheUserForMsg();
        }
        int num = get(RedisPreEnum.SYS_MSG_SEND_NUM.getCode(), int.class);
        return num;
    }

    /**
     * 单位分钟
     */
    private <T> T set(String key, T t, long time) {
        if (t != null) {
            myRedisTemplate.set(key, JSUtil.toJSONString(t), time, TimeUnit.MINUTES);
        }
        return t;
    }
}
