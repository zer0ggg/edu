package com.roncoo.education.user.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.UserAccountExtractLogDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserAccountExtractLogMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLog;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExtractLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserAccountExtractLogDaoImpl implements UserAccountExtractLogDao {
    @Autowired
    private UserAccountExtractLogMapper userAccountExtractLogMapper;

    @Override
    public int save(UserAccountExtractLog record) {
        record.setId(IdWorker.getId());
        return this.userAccountExtractLogMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.userAccountExtractLogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserAccountExtractLog record) {
        return this.userAccountExtractLogMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(UserAccountExtractLog record, UserAccountExtractLogExample example) {
        return this.userAccountExtractLogMapper.updateByExampleSelective(record, example);
    }

    @Override
    public UserAccountExtractLog getById(Long id) {
        return this.userAccountExtractLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserAccountExtractLog> listForPage(int pageCurrent, int pageSize, UserAccountExtractLogExample example) {
        int count = this.userAccountExtractLogMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserAccountExtractLog>(count, totalPage, pageCurrent, pageSize, this.userAccountExtractLogMapper.selectByExample(example));
    }

    @Override
    public List<UserAccountExtractLog> listByUserNo(Long userNo) {
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        UserAccountExtractLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        return this.userAccountExtractLogMapper.selectByExample(example);
    }

    @Override
    public int updateByMobile(Long userNo, String phone) {
        UserAccountExtractLogExample example = new UserAccountExtractLogExample();
        UserAccountExtractLogExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        UserAccountExtractLog userAccountExtractLog = new UserAccountExtractLog();
        userAccountExtractLog.setUserNo(userNo);
        userAccountExtractLog.setPhone(phone);
        return this.userAccountExtractLogMapper.updateByExampleSelective(userAccountExtractLog, example);
    }
}
