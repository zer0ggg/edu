package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户关注讲师保存
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserLecturerAttentionSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 讲师用户编号
     */
    @ApiModelProperty(value = " 讲师用户编号", required = true)
    private Long lecturerUserNo;

}
