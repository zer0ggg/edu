package com.roncoo.education.user.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.dfa.SensitiveUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AuditStatusEnum;
import com.roncoo.education.common.core.enums.IsLiveEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.system.feign.interfaces.IFeignSensitiveWordLibrary;
import com.roncoo.education.user.common.bo.auth.AuthLecturerAuditApplyLiveBO;
import com.roncoo.education.user.common.bo.auth.AuthLecturerAuditBO;
import com.roncoo.education.user.common.bo.auth.AuthLecturerAuditViewBO;
import com.roncoo.education.user.common.dto.auth.AuthLecturerAuditViewDTO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@Component
public class AuthLecturerAuditBiz extends BaseBiz {

    @Autowired
    private LecturerAuditDao lecturerAuditDao;

    @Autowired
    private IFeignSensitiveWordLibrary feignSensitiveWordLibrary;

    /**
     * 讲师信息修改接口
     *
     * @param authLecturerAuditBO
     * @author wuyun
     */
    public Result<Integer> update(AuthLecturerAuditBO authLecturerAuditBO) {
        if (!authLecturerAuditBO.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("数据异常");
        }
        if (!StringUtils.isEmpty(authLecturerAuditBO.getIntroduce())) {
            //校验敏感词
            SensitiveUtil.init(feignSensitiveWordLibrary.listAll());
            String introduce = SensitiveUtil.getFindedFirstSensitive(authLecturerAuditBO.getIntroduce());
            if (!com.alibaba.druid.util.StringUtils.isEmpty(introduce)) {
                return Result.error("存在敏感词{" + introduce + "}");
            }
        }
        LecturerAudit record = BeanUtil.copyProperties(authLecturerAuditBO, LecturerAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        record.setAuditOpinion("");
        int resultNum = lecturerAuditDao.updateById(record);
        if (resultNum > 0) {
            return Result.success(resultNum);
        }
        return Result.error(ResultEnum.USER_UPDATE_FAIL.getDesc());
    }

    /**
     * 讲师信息查看接口
     *
     * @param authLecturerAuditViewBO
     * @author wuyun
     */
    public Result<AuthLecturerAuditViewDTO> view(AuthLecturerAuditViewBO authLecturerAuditViewBO) {
        LecturerAudit lecturerAudit = lecturerAuditDao.getByLecturerUserNo(ThreadContext.userNo());
        if (lecturerAudit == null) {
            return Result.error("找不到该讲师");
        }
        return Result.success(BeanUtil.copyProperties(lecturerAudit, AuthLecturerAuditViewDTO.class));
    }

    public Result<Integer> applyLive(AuthLecturerAuditApplyLiveBO authLecturerAuditApplyLiveBO) {
        // 判断是否是讲师
        LecturerAudit lecturerAudit = lecturerAuditDao.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerAudit)) {
            return Result.error("找不到讲师信息");
        }
        lecturerAudit.setIsLive(IsLiveEnum.YES.getCode());
        lecturerAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        return Result.success(lecturerAuditDao.updateById(lecturerAudit));
    }

}
