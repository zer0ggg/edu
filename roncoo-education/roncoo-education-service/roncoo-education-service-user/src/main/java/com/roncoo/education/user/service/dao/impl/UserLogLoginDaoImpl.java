package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.user.service.dao.UserLogLoginDao;
import com.roncoo.education.user.service.dao.impl.mapper.UserLogLoginMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLogin;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLoginExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserLogLoginExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class UserLogLoginDaoImpl extends AbstractBaseJdbc implements UserLogLoginDao {
    @Autowired
    private UserLogLoginMapper userLogLoginMapper;

    @Override
	public int save(UserLogLogin record) {
        record.setId(IdWorker.getId());
        return this.userLogLoginMapper.insertSelective(record);
    }

    @Override
	public int deleteById(Long id) {
        return this.userLogLoginMapper.deleteByPrimaryKey(id);
    }

    @Override
	public int updateById(UserLogLogin record) {
        return this.userLogLoginMapper.updateByPrimaryKeySelective(record);
    }

    @Override
	public UserLogLogin getById(Long id) {
        return this.userLogLoginMapper.selectByPrimaryKey(id);
    }

    @Override
	public Page<UserLogLogin> listForPage(int pageCurrent, int pageSize, UserLogLoginExample example) {
        int count = this.userLogLoginMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserLogLogin>(count, totalPage, pageCurrent, pageSize, this.userLogLoginMapper.selectByExample(example));
    }

    @Override
    public Integer listByUserNoAndLonginStatus(Long userNo, Integer loginStatus) {
        String sql = "select count(*) from user_log_login where user_no=? and login_status=? and gmt_create > ?";
        return jdbcTemplate.queryForObject(sql, Integer.class, userNo, loginStatus, DateUtil.format(new Date(), Constants.DATE.YYYY_MM_DD));
    }

    @Override
    public List<UserLogLogin> listByUserAndGmtCreateAndLoginStatus(Long userNo, Date beginOfDay, Integer loginStatus) {
        UserLogLoginExample example = new UserLogLoginExample();
        Criteria criteria = example.createCriteria();
        criteria.andUserNoEqualTo(userNo);
        criteria.andLoginStatusEqualTo(loginStatus);
        criteria.andGmtCreateGreaterThanOrEqualTo(beginOfDay);
        return this.userLogLoginMapper.selectByExample(example);
    }
}
