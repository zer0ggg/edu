package com.roncoo.education.user.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.feign.interfaces.IFeignMsg;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.vo.MsgVO;
import com.roncoo.education.user.service.feign.biz.FeignMsgBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 站内信息表
 *
 * @author wuyun
 */
@RestController
public class FeignMsgController extends BaseController implements IFeignMsg {

	@Autowired
	private FeignMsgBiz biz;

	@Override
	public Page<MsgVO> listForPage(@RequestBody MsgQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody MsgQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@RequestBody Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody MsgQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public MsgVO getById(@RequestBody Long id) {
		return biz.getById(id);
	}

    @Override
    public List<MsgVO> listByStatusIdAndIsSendAndIsTimeSendAndSendTime() {
        return biz.listByStatusIdAndIsSendAndIsTimeSendAndSendTime();
    }

    @Override
    public int msgSendLecturer(@RequestBody MsgQO qo) {
        return biz.msgSendLecturer(qo);
    }


}
