package com.roncoo.education.user.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.user.service.dao.ChosenDao;
import com.roncoo.education.user.service.dao.impl.mapper.ChosenMapper;
import com.roncoo.education.user.service.dao.impl.mapper.entity.Chosen;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 分销信息 服务实现类
 *
 * @author wujing
 * @date 2020-06-17
 */
@Repository
public class ChosenDaoImpl implements ChosenDao {

    @Autowired
    private ChosenMapper mapper;

    @Override
    public int save(Chosen record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Chosen record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Chosen getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Chosen> listForPage(int pageCurrent, int pageSize, ChosenExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public Chosen getByCourseId(Long courseId) {
        ChosenExample example = new ChosenExample();
        Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(courseId);
        List<Chosen> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }


}
