package com.roncoo.education.user.service.dao.impl.mapper;

import com.roncoo.education.user.service.dao.impl.mapper.entity.Chosen;
import com.roncoo.education.user.service.dao.impl.mapper.entity.ChosenExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChosenMapper {
    int countByExample(ChosenExample example);

    int deleteByExample(ChosenExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Chosen record);

    int insertSelective(Chosen record);

    List<Chosen> selectByExample(ChosenExample example);

    Chosen selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Chosen record, @Param("example") ChosenExample example);

    int updateByExample(@Param("record") Chosen record, @Param("example") ChosenExample example);

    int updateByPrimaryKeySelective(Chosen record);

    int updateByPrimaryKey(Chosen record);
}
