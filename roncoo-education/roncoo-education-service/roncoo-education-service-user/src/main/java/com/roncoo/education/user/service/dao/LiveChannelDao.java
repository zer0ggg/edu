package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannel;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LiveChannelExample;

import java.util.List;

/**
 * <p>
 * 直播频道表 服务类
 * </p>
 *
 * @author LHR
 * @date 2020-05-20
 */
public interface LiveChannelDao {

    int save(LiveChannel record);

    int deleteById(Long id);

    int updateById(LiveChannel record);

    LiveChannel getById(Long id);

    Page<LiveChannel> listForPage(int pageCurrent, int pageSize, LiveChannelExample example);

    /**
     * 获取频道号信息
     * @param channelId
     * @return
     */
    LiveChannel getByChannel(String channelId);

    /**
     * 根据直播场景、是否使用查询
     * @param scene
     * @param isUse
     * @return
     */
    List<LiveChannel> listBySceneAndIsUse(String scene, Integer isUse);
}
