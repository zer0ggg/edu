package com.roncoo.education.user.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 支付路由表
 * </p>
 *
 * @author wujing
 * @date 2020-04-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PayRuleDTO", description="支付路由表")
public class PayRuleDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "支付渠道编码")
    private String payChannelCode;

    @ApiModelProperty(value = "支付渠道名称")
    private String payChannelName;

    @ApiModelProperty(value = "渠道优先等级")
    private Integer channelPriority;

    @ApiModelProperty(value = "参考PayTypeEnum")
    private Integer payType;
}
