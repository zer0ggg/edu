/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.user.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 获取微信accessToken实体
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class AuthUserExtReferralCodeBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 推荐码
	 */
	@ApiModelProperty(value = "推荐码")
	private String referralCode;

	/**
	 * 跳转页面
	 */
	@ApiModelProperty(value = "跳转页面")
	private String page;

}
