package com.roncoo.education.user.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.AuditStatusEnum;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigSysVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.feign.qo.LecturerAuditQO;
import com.roncoo.education.user.feign.vo.LecturerAuditVO;
import com.roncoo.education.user.feign.vo.UserAccountVO;
import com.roncoo.education.user.service.dao.LecturerAuditDao;
import com.roncoo.education.user.service.dao.UserAccountDao;
import com.roncoo.education.user.service.dao.UserDao;
import com.roncoo.education.user.service.dao.UserExtDao;
import com.roncoo.education.user.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.service.dao.impl.mapper.entity.LecturerAuditExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */
@Component
public class FeignLecturerAuditBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private LecturerAuditDao dao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserExtDao userExtDao;
    @Autowired
    private UserAccountDao userAccountDao;

    public Page<LecturerAuditVO> listForPage(LecturerAuditQO qo) {
        LecturerAuditExample example = new LecturerAuditExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotEmpty(qo.getLecturerMobile())) {
            c.andLecturerMobileEqualTo(qo.getLecturerMobile());
        }
        if (StringUtils.isNotEmpty(qo.getLecturerName())) {
            c.andLecturerNameLike(PageUtil.rightLike(qo.getLecturerName()));
        }
        if (qo.getAuditStatus() != null) {
            c.andAuditStatusEqualTo(qo.getAuditStatus());
        } else {
            c.andAuditStatusNotEqualTo(AuditStatusEnum.SUCCESS.getCode());
        }
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }
        example.setOrderByClause(" audit_status asc, status_id desc, sort asc, id desc ");
        Page<LecturerAudit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, LecturerAuditVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public int save(LecturerAuditQO qo) {
        // 手机号去空处理
        String mobile = qo.getLecturerMobile().trim();
        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(mobile).matches()) {
            throw new BaseException("手机号码格式不正确");
        }
        ConfigSysVO sys = feignSysConfig.getSys();
        if (ObjectUtil.isNull(sys)) {
            throw new BaseException("找不到系统配置信息");
        }

        // 根据传入手机号获取用户信息(讲师的用户信息)
        UserExt userExt = userExtDao.getByMobile(mobile);
        // 1、用户不存在，注册用户
        if (ObjectUtil.isNull(userExt)) {
            if (StringUtils.isEmpty(qo.getMobilePsw())) {
                throw new BaseException("密码不能为空");
            }
            if (!qo.getConfirmPasswd().equals(qo.getMobilePsw())) {
                throw new BaseException("两次密码不一致，请重试！");
            }
            // 注册用户
            userExt = register(qo, sys, mobile);

            lecturerInfo(qo, userExt, sys);

            // 返回成功
            return 1;
        }
        lecturerInfo(qo, userExt, sys);
        return 1;
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public LecturerAuditVO getById(Long id) {
        LecturerAudit record = dao.getById(id);
        LecturerAuditVO vo = BeanUtil.copyProperties(record, LecturerAuditVO.class);
        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(vo.getLecturerUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            userAccount = new UserAccount();
            userAccount.setUserNo(vo.getLecturerUserNo());
            userAccount.setTotalIncome(BigDecimal.valueOf(0));
            userAccount.setHistoryMoney(BigDecimal.valueOf(0));
            userAccount.setEnableBalances(BigDecimal.valueOf(0));
            userAccount.setFreezeBalances(BigDecimal.valueOf(0));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.save(userAccount);
        }
        vo.setUserAccountVO(BeanUtil.copyProperties(userAccount, UserAccountVO.class));
        return vo;
    }

    public int updateById(LecturerAuditQO qo) {
        LecturerAudit record = BeanUtil.copyProperties(qo, LecturerAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        return dao.updateById(record);
    }

    /**
     * 手机号码校验用户信息表是否存在
     *
     * @param qo
     * @return
     * @author wuyun
     */
    public LecturerAuditVO checkUserAndLecturer(LecturerAuditQO qo) {
        // 手机号码校验
        if (!Pattern.compile(Constants.REGEX_MOBILE).matcher(qo.getLecturerMobile()).matches()) {
            throw new BaseException("手机号码格式不正确");
        }
        // 根据传入手机号获取用户信息(讲师的用户信息)
        UserExt userExt = userExtDao.getByMobile(qo.getLecturerMobile());
        LecturerAuditVO vo = new LecturerAuditVO();

        if (ObjectUtil.isNull(userExt)) {
            // 用户不存在
            vo.setCheckUserAndLecturer(1);
        } else {
            // 根据用户编号获取讲师信息
            LecturerAudit record = dao.getByLecturerUserNo(userExt.getUserNo());
            if (ObjectUtil.isNull(record)) {
                vo.setCheckUserAndLecturer(2);
            } else {
                vo.setCheckUserAndLecturer(3);
            }
        }
        return vo;
    }

    /**
     * 根据讲师编号获取讲师信息
     *
     * @param lecturerUserNo
     * @return
     */
    public LecturerAuditVO getByLecturerUserNo(Long lecturerUserNo) {
        LecturerAudit lecturerAudit = dao.getByLecturerUserNo(lecturerUserNo);
        return BeanUtil.copyProperties(lecturerAudit, LecturerAuditVO.class);
    }

    /**
     * 添加用户信息
     *
     * @author wuyun
     */
    private UserExt register(LecturerAuditQO lecturerAudit, ConfigSysVO sys, String mobile) {
        // 用户基本信息
        User user = new User();
        user.setUserNo(NOUtil.getUserNo());
        user.setMobile(mobile);
        user.setMobileSalt(StrUtil.get32UUID());
        user.setMobilePsw(DigestUtil.sha1Hex(user.getMobileSalt() + lecturerAudit.getMobilePsw()));
        userDao.save(user);

        // 用户教育信息
        UserExt userExt = new UserExt();
        userExt.setUserNo(user.getUserNo());
        userExt.setMobile(user.getMobile());
        userExt.setNickname(lecturerAudit.getLecturerName());
        // 推荐码校验
        userExt.setReferralCode(referralCode(ReferralCodeUtil.getStringRandom()));
        // 用户默认头像
        SysConfigVO sysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        if (org.springframework.util.StringUtils.isEmpty(sysVO.getConfigValue())) {
            throw new BaseException("未设置平台域名");
        }
        userExt.setHeadImgUrl(sysVO.getConfigValue() + "/friend.jpg");
        userExtDao.save(userExt);

        UserAccount userAccount = new UserAccount();
        userAccount.setUserNo(user.getUserNo());
        userAccount.setTotalIncome(BigDecimal.valueOf(0));
        userAccount.setHistoryMoney(BigDecimal.valueOf(0));
        userAccount.setEnableBalances(BigDecimal.valueOf(0));
        userAccount.setFreezeBalances(BigDecimal.valueOf(0));
        userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
        userAccountDao.save(userAccount);
        return userExt;
    }

    /**
     * 推荐码校验唯一性
     */
    private String referralCode(String referralCode) {
        // 查询生成的推荐码在数据库是否存在，不存在就返回，存在重新生成再次判断
        UserExt userExt = userExtDao.getByReferralCode(referralCode);
        if (ObjectUtil.isNull(userExt)) {
            return referralCode;
        }
        return referralCode(ReferralCodeUtil.getStringRandom());
    }

    /**
     * 添加讲师信息
     *
     * @author wuyun
     */
    private int lecturerInfo(LecturerAuditQO auditBO, UserExt userExt, ConfigSysVO sys) {
        // 插入讲师信息
        LecturerAudit infoAudit = BeanUtil.copyProperties(auditBO, LecturerAudit.class);
        if (!StringUtils.isEmpty(userExt.getHeadImgUrl())) {
            infoAudit.setHeadImgUrl(userExt.getHeadImgUrl());
        }
        infoAudit.setLecturerUserNo(userExt.getUserNo());
        // 设置讲师默认分成百分之70
        infoAudit.setLecturerProportion(sys.getAgentDefaultProportion());
        int infoAuditNum = dao.save(infoAudit);
        if (infoAuditNum < 1) {
            throw new BaseException("讲师信息表新增失败");
        }
        // 查找账户信息
        UserAccount userAccount = userAccountDao.getByUserNo(userExt.getUserNo());
        if (ObjectUtil.isNull(userAccount)) {
            userAccount = new UserAccount();
            userAccount.setUserNo(userExt.getUserNo());
            userAccount.setTotalIncome(BigDecimal.valueOf(0));
            userAccount.setHistoryMoney(BigDecimal.valueOf(0));
            userAccount.setEnableBalances(BigDecimal.valueOf(0));
            userAccount.setFreezeBalances(BigDecimal.valueOf(0));
            userAccount.setSign(SignUtil.getByLecturer(userAccount.getTotalIncome(), userAccount.getHistoryMoney(), userAccount.getEnableBalances(), userAccount.getFreezeBalances()));
            userAccountDao.save(userAccount);
        }
        return infoAuditNum;
    }
}
