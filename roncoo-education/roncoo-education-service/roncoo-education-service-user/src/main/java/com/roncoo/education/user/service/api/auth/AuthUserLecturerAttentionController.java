package com.roncoo.education.user.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionDeleteBO;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionPageBO;
import com.roncoo.education.user.common.bo.auth.AuthUserLecturerAttentionSaveBO;
import com.roncoo.education.user.common.dto.auth.AuthUserLecturerAttentionPageDTO;
import com.roncoo.education.user.service.api.auth.biz.AuthUserLecturerAttentionBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户关注讲师表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/user/auth/lecturer/attention")
public class AuthUserLecturerAttentionController {

	@Autowired
	private AuthUserLecturerAttentionBiz biz;

	/**
	 * 用戶关注讲师记录分页列出接口
	 *
	 * @param authUserLecturerAttentionPageBO
	 * @author kyh
	 */
	@ApiOperation(value = "用戶关注讲师记录分页列出接口", notes = "用戶关注讲师记录分页列出接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthUserLecturerAttentionPageDTO>> list(@RequestBody AuthUserLecturerAttentionPageBO authUserLecturerAttentionPageBO) {
		return biz.list(authUserLecturerAttentionPageBO);
	}

	/**
	 * 用戶关注讲师保存接口接口
	 *
	 * @param authUserLecturerAttentionSaveBO
	 * @author kyh
	 */
	@ApiOperation(value = "用戶关注讲师保存接口", notes = "用戶关注讲师保存接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthUserLecturerAttentionSaveBO authUserLecturerAttentionSaveBO) {
		return biz.save(authUserLecturerAttentionSaveBO);
	}

	/**
	 * 用戶关注讲师删除接口
	 *
	 * @param authUserLecturerAttentionDeleteBO
	 * @author kyh
	 */
	@ApiOperation(value = "用戶关注讲师删除接口", notes = "用戶关注讲师删除接口")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthUserLecturerAttentionDeleteBO authUserLecturerAttentionDeleteBO) {
		return biz.delet(authUserLecturerAttentionDeleteBO);
	}

}
