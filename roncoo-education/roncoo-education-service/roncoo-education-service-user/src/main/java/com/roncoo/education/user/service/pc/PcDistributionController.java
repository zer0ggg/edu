package com.roncoo.education.user.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.user.common.req.DistributionEditREQ;
import com.roncoo.education.user.common.req.DistributionListREQ;
import com.roncoo.education.user.common.req.DistributionPageREQ;
import com.roncoo.education.user.common.req.DistributionSaveREQ;
import com.roncoo.education.user.common.resp.DistributionListRESP;
import com.roncoo.education.user.common.resp.DistributionPageRESP;
import com.roncoo.education.user.common.resp.DistributionViewRESP;
import com.roncoo.education.user.service.pc.biz.PcDistributionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 分销信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-分销信息")
@RestController
@RequestMapping("/user/pc/distribution")
public class PcDistributionController {

    @Autowired
    private PcDistributionBiz biz;

    @ApiOperation(value = "分销信息列表", notes = "分销信息列表")
    @PostMapping(value = "/list")
    public Result<Page<DistributionPageRESP>> list(@RequestBody DistributionPageREQ distributionPageREQ) {
        return biz.list(distributionPageREQ);
    }

    @ApiOperation(value = "分销信息添加", notes = "分销信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody  @Valid DistributionSaveREQ distributionSaveREQ) {
        return biz.save(distributionSaveREQ);
    }

    @ApiOperation(value = "分销信息查看", notes = "分销信息查看")
    @GetMapping(value = "/view")
    public Result<DistributionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "分销信息修改", notes = "分销信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody DistributionEditREQ distributionEditREQ) {
        return biz.edit(distributionEditREQ);
    }

    @ApiOperation(value = "分销信息删除", notes = "分销信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
