package com.roncoo.education.user.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccount;
import com.roncoo.education.user.service.dao.impl.mapper.entity.UserAccountExample;

public interface UserAccountDao {
    int save(UserAccount record);

    int deleteById(Long id);

    int updateById(UserAccount record);

    int updateByExampleSelective(UserAccount record, UserAccountExample example);

    UserAccount getById(Long id);

    Page<UserAccount> listForPage(int pageCurrent, int pageSize, UserAccountExample example);

    /**
     * 根据用户编号获取用户账号信息
     *
     * @param userNo
     * @return
     */
    UserAccount getByUserNo(Long userNo);

    /**
     * 根据用户编号获取用户账号信息
     * 并检测签名和账户状态
     *
     * @param userNo 用户编号
     * @return 可用账户
     */
    UserAccount getByUserNoAndCheck(Long userNo);

    /**
     * 根据用户信息更新账户信息
     *
     * @param record
     * @return
     */
    int updateByUserNo(UserAccount userAccount);

    /**
     * 根据用户编号、状态获取用户账号信息
     *
     * @param userNo
     * @param statusId
     * @return
     */
    UserAccount getByUserNoAndStatusId(Long userNo, Integer statusId);
}
