package com.roncoo.education.user.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 站内信息表  全部已经阅读
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class MsgViewAllBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
