package com.roncoo.education.user.service.test;

import com.roncoo.education.common.core.enums.OrderStatusEnum;
import com.roncoo.education.common.core.enums.PayTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.VipTypeEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.feign.interfaces.IFeignVipSet;
import com.roncoo.education.system.feign.qo.VipSetQO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.user.common.bo.PayBO;
import com.roncoo.education.user.common.dto.PayDTO;
import com.roncoo.education.user.common.pay.OrderFacade;
import com.roncoo.education.user.common.pay.PayFacade;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author Quanf
 * 2020/4/14 9:56
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PayTest {

    @Autowired
    private IFeignVipSet feignVipSet;

    @Autowired
    private OrderFacade orderFacade;

    @Resource(name = "ALI_PAY_APP")
    PayFacade payFacade;

    @Test

    public void order() {
        orderFacade.orderInfoComplete(202006191600397422L, OrderStatusEnum.SUCCESS.getCode());
    }

    @Test
    public void vipSet() {
        VipSetQO qo = new VipSetQO();
        qo.setSetType(VipTypeEnum.MONTH.getCode());
        qo.setStatusId(StatusIdEnum.YES.getCode());
        VipSetVO vipSet = feignVipSet.getByVipTypeAndStatusId(qo);
        System.out.println(vipSet);
    }

    @Test
    public void payTest() {
        PayBO payBO = new PayBO();
        payBO.setPayChannelCode("guanfangweixinAPP");
        payBO.setOrderNo(String.valueOf(IdWorker.getId()));
        payBO.setPayType(PayTypeEnum.APP_ALIPAY.getCode());
        payBO.setOrderAmount(new BigDecimal("3.01"));
        payBO.setGoodsName("测试");
        payBO.setNotifyUrl("http://dev.edu.roncoos.com/gateway/callback/order/notify/");
        PayDTO payDTO = payFacade.pay(payBO);
        System.out.println(payDTO);

    }

}
