package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsListDTO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsViewDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthQuestionsBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/questions/")
public class AuthQuestionsController {

    @Autowired
    private AuthQuestionsBiz biz;

    /**
     * 添加问答信息接口
     * @param authQuestionsSaveBO
     * @return
     */
	@ApiOperation(value = "添加问答信息接口", notes = "添加问答信息接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthQuestionsSaveBO authQuestionsSaveBO) {
		return biz.save(authQuestionsSaveBO);
	}

	/**
	 * 删除问答信息接口
	 * @param authQuestionsDeleteBO
	 * @return
	 */
	@ApiOperation(value = "删除问答信息接口", notes = "删除问答信息接口")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthQuestionsDeleteBO authQuestionsDeleteBO) {
		return biz.delete(authQuestionsDeleteBO);
	}

	/**
	 * 修改问答信息接口
	 * @param authQuestionsUpdateBO
	 * @return
	 */
	@ApiOperation(value = "修改问答信息接口", notes = "修改问答信息接口")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<Integer> update(@RequestBody AuthQuestionsUpdateBO authQuestionsUpdateBO) {
		return biz.update(authQuestionsUpdateBO);
	}

	/**
	 * 问答已登录详情接口
	 * @param authQuestionsViewBO
	 * @return
	 */
	@ApiOperation(value = "问答已登录详情接口", notes = "问答已登录详情接口")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthQuestionsViewDTO> view(@RequestBody AuthQuestionsViewBO authQuestionsViewBO) {
		return biz.view(authQuestionsViewBO);
	}

	/**
	 * 问答列表接口
	 * @param authQuestionsListBO
	 * @return
	 */
	@ApiOperation(value = "问答列表接口", notes = "问答列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthQuestionsListDTO>> list(@RequestBody AuthQuestionsListBO authQuestionsListBO) {
		return biz.list(authQuestionsListBO);
	}
}
