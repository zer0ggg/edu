package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ArticleTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.ArticleRecommendListRESP;
import com.roncoo.education.community.common.resp.ArticleRecommendViewRESP;
import com.roncoo.education.community.service.dao.ArticleRecommendDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Component
public class PcArticleRecommendBiz extends BaseBiz {

    @Autowired
    private ArticleRecommendDao dao;
    @Autowired
    private BlogDao blogDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 文章推荐列表
     *
     * @param articleRecommendListREQ 文章推荐分页查询参数
     * @return 文章推荐分页查询结果
     */
    public Result<Page<ArticleRecommendListRESP>> list(ArticleRecommendListREQ articleRecommendListREQ) {
        ArticleRecommendExample example = new ArticleRecommendExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotEmpty(articleRecommendListREQ.getTitle())) {
            List<Blog> blogList = blogDao.listByTitle(articleRecommendListREQ.getTitle());
            if(!CollectionUtil.isEmpty(blogList)){
                Set<Long> blogIdList = blogList.stream().map(Blog::getId).collect(Collectors.toSet());
                c.andArtcleIdIn(new ArrayList<>(blogIdList));
            }else {// 查无博客信息，使用不存在博客id，继续查询，保证返回数据为空
                c.andArtcleIdEqualTo(0L);
            }
        }
        if (articleRecommendListREQ.getArticleType() != null) {
            c.andArticleTypeEqualTo(articleRecommendListREQ.getArticleType());
        }
        if (articleRecommendListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(articleRecommendListREQ.getStatusId());
        }else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        example.setOrderByClause("sort asc, id desc ");
        Page<ArticleRecommend> page = dao.listForPage(articleRecommendListREQ.getPageCurrent(), articleRecommendListREQ.getPageSize(), example);
        Page<ArticleRecommendListRESP> respPage = PageUtil.transform(page, ArticleRecommendListRESP.class);
        for (ArticleRecommendListRESP vo : respPage.getList()) {
            // 获取博客信息
            Blog blog = blogDao.getById(vo.getArtcleId());
            if (ObjectUtil.isNotNull(blog)) {
                vo.setTitle(blog.getTitle());
                if (ArticleTypeEnum.BLOG.getCode().equals(blog.getArticleType())) {
                    vo.setUserNo(blog.getUserNo());
                    // 获取作者信息
                    UserExtVO userExtVO = feignUserExt.getByUserNo(blog.getUserNo());
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        vo.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        vo.setBloggerNickname(userExtVO.getNickname());
                    }
                }
            }
        }
        return Result.success(respPage);
    }


    /**
     * 文章推荐添加
     *
     * @param req 文章推荐
     * @return 添加结果
     */
    public Result<String> save(ArticleRecommendSaveREQ req) {
        if (req.getArtcleId() == null) {
            return Result.error("文章id不能为空");
        }
        if (req.getArticleType() == null) {
            return Result.error("文章类型(1:博客;2:资讯)不能为空");
        }
        Blog blog = blogDao.getById(req.getArtcleId());
        //博客不存在,则跳过
        if(ObjectUtils.isEmpty(blog)){
            return Result.error("文章id不正确");
        }
        ArticleRecommend record = dao.getByArtcleId(req.getArtcleId());
        if (ObjectUtil.isNotNull(record)) {
            return Result.error("已添加文章推荐");
        }
        ArticleRecommend bean = BeanUtil.copyProperties(req, ArticleRecommend.class);
        if (dao.save(bean) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 文章推荐查看
     *
     * @param id 主键ID
     * @return 文章推荐
     */
    public Result<ArticleRecommendViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ArticleRecommendViewRESP.class));
    }


    /**
     * 文章推荐修改
     *
     * @param articleRecommendEditREQ 文章推荐修改对象
     * @return 修改结果
     */
    public Result<String> edit(ArticleRecommendEditREQ articleRecommendEditREQ) {
        ArticleRecommend record = BeanUtil.copyProperties(articleRecommendEditREQ, ArticleRecommend.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 文章推荐修改
     *
     * @param articleRecommendUpdateStatusREQ 文章推荐状态修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(ArticleRecommendUpdateStatusREQ articleRecommendUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(articleRecommendUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        ArticleRecommend record = BeanUtil.copyProperties(articleRecommendUpdateStatusREQ, ArticleRecommend.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 文章推荐删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> saveBatch(ArticleRecommendSaveBathREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        String[] articleIds = req.getArticleIds().split(",");
        try {
            for(String  articleStr: articleIds){
                Long articleId = Long.valueOf(articleStr);
                Blog blog = blogDao.getById(articleId);
                //博客不存在,则跳过
                if(ObjectUtils.isEmpty(blog)){
                    continue;
                }
                ArticleRecommend record = dao.getByArtcleId(articleId);
                // 已推荐，跳过
                if(!ObjectUtils.isEmpty(record)){
                    continue;
                }
                ArticleRecommend bean = new ArticleRecommend();
                bean.setArtcleId(articleId);
                bean.setArticleType(blog.getArticleType());
                dao.save(bean);
            }
        }catch (Exception e){
            logger.error(e.toString());
        }

        return Result.success("添加成功");

    }
}
