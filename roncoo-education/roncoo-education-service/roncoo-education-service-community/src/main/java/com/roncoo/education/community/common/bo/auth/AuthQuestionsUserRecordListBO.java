package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsUserRecordListBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户操作类型(1收藏，2点赞)
     */
    @ApiModelProperty(value = "用户操作类型(1收藏，2点赞)")
    private Integer opType;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private int pageSize = 20;
}
