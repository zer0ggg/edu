package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 问答信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="QuestionsListREQ", description="问答信息列表")
public class QuestionsListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "问题标题")
    private String title;

    @ApiModelProperty(value = "问题标签")
    private String tagsName;

    @ApiModelProperty(value = "是否置顶(1置顶，0否)")
    private Integer isTop;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "是否存在最佳回答(1有，0无)")
    private Integer isGood;

    @ApiModelProperty(value = "开始时间")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束时间")
    private String endGmtCreate;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
