package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.ArticleZoneRefListRESP;
import com.roncoo.education.community.common.resp.ArticleZoneRefViewRESP;
import com.roncoo.education.community.service.dao.ArticleZoneCategoryDao;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章专区关联
 *
 * @author wujing
 */
@Component
public class PcArticleZoneRefBiz extends BaseBiz {

    @Autowired
    private ArticleZoneRefDao dao;
    @Autowired
    private ArticleZoneCategoryDao zoneCategoryDao;
    @Autowired
    private BlogDao blogDao;
    @Autowired
    private IFeignUserExt feignUserExt;

    /**
    * 文章专区关联列表
    *
    * @param req 文章专区关联分页查询参数
    * @return 文章专区关联分页查询结果
    */
    public Result<Page<ArticleZoneRefListRESP>> list(ArticleZoneRefListREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }

        ArticleZoneRefExample example = new ArticleZoneRefExample();
        Criteria c = example.createCriteria();
        c.andArticleZoneIdEqualTo(req.getArticleZoneId());
        c.andPlatShowEqualTo(req.getPlatShow());
        //跨表查询标题
        List<Blog> blogList = null;
        List<Long> articleIdList = null;

        if(StringUtils.isNotEmpty(req.getTitle())){
            blogList = blogDao.listByTitle(req.getTitle());
        }
        if(!CollectionUtils.isEmpty(blogList)){
            articleIdList = new ArrayList<>();
            for(Blog blog : blogList){
                articleIdList.add(blog.getId());
            }
        }
        if(!CollectionUtils.isEmpty(articleIdList)){
            c.andArticleIdIn(articleIdList);
        }
//        if (StringUtils.isNotBlank(qo.getEndGmtCreate())) {
//            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
//        }
        example.setOrderByClause("sort asc, status_id desc,   id desc ");
        Page<ArticleZoneRef> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ArticleZoneRefListRESP> respPage = PageUtil.transform(page, ArticleZoneRefListRESP.class);
        for (ArticleZoneRefListRESP resp : respPage.getList()) {
            // 获取文章信息
            getArticle(resp);
        }
        return Result.success(respPage);
    }

    private void getArticle(ArticleZoneRefListRESP resp) {
        Blog blog = blogDao.getById(resp.getArticleId());
        if (ObjectUtil.isNotNull(blog)) {
            // 获取作者信息
            resp.setUserNo(blog.getUserNo());
            resp.setTitle(blog.getTitle());
            UserExtVO userExtVO = feignUserExt.getByUserNo(blog.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    resp.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    resp.setBloggerNickname(userExtVO.getNickname());
                }
            }
        }
    }


    /**
    * 文章专区关联添加
    *
    * @param req 文章专区关联
    * @return 添加结果
    */
    public Result<String> save(ArticleZoneRefSaveREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        ArticleZoneCategory zoneCategory = zoneCategoryDao.getById(req.getArticleZoneId());
        if(ObjectUtils.isEmpty(zoneCategory)){
            return Result.error("专区ID不正确");
        }
        Blog blog = blogDao.getById(req.getArticleId());
        if(ObjectUtils.isEmpty(blog)){
            return Result.error("资讯id不正确");
        }
        ArticleZoneRef articleZoneRef = dao.getByArticleZoneIdAndArticleIdAndPlatShow(req.getArticleZoneId(), req.getArticleId(), zoneCategory.getPlatShow());
        if (!ObjectUtils.isEmpty(articleZoneRef)) {
            return Result.success("添加成功");
        }
        ArticleZoneRef record = new ArticleZoneRef();
        record.setArticleZoneId(req.getArticleZoneId());
        record.setArticleId(req.getArticleId());
        record.setPlatShow(zoneCategory.getPlatShow());
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    public Result<String> saveBatch(ArticleZoneRefSaveBatchREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        ArticleZoneCategory zoneCategory = zoneCategoryDao.getById(req.getArticleZoneId());
        if(ObjectUtils.isEmpty(zoneCategory)){
            return Result.error("专区ID不正确");
        }
        String[] articleIds = req.getArticleIds().split(",");
        try {
            for(String  articleStr: articleIds){
                Long articleId = Long.valueOf(articleStr);
                Blog blog = blogDao.getById(articleId);
                //不存在,则跳过
                if(ObjectUtils.isEmpty(blog)){
                    continue;
                }
                ArticleZoneRef ref = dao.getByArticleZoneIdAndArticleIdAndPlatShow(req.getArticleZoneId(),articleId,zoneCategory.getPlatShow());
                //已添加
                if(!ObjectUtils.isEmpty(ref)){
                    continue;
                }
                ArticleZoneRef record = new ArticleZoneRef();
                record.setArticleZoneId(req.getArticleZoneId());
                record.setArticleId(articleId);
                record.setPlatShow(zoneCategory.getPlatShow());
                dao.save(record);
            }
        }catch (Exception e){
            logger.error(e.toString());
        }

        return Result.success("添加成功");
    }


    /**
    * 文章专区关联查看
    *
    * @param id 主键ID
    * @return 文章专区关联
    */
    public Result<ArticleZoneRefViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ArticleZoneRefViewRESP.class));
    }


    /**
    * 文章专区关联修改
    *
    * @param req 文章专区关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(ArticleZoneRefEditREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        ArticleZoneRef record = BeanUtil.copyProperties(req, ArticleZoneRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 文章专区关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {

        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> updateStatusId(ArticleZoneRefUpdateStatusIdREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        ArticleZoneRef record = BeanUtil.copyProperties(req, ArticleZoneRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
