package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsCommentListBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsCommentSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsCommentListDTO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsCommentSaveDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthQuestionsCommentBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答评论表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/questions/comment")
public class AuthQuestionsCommentController {

    @Autowired
    private AuthQuestionsCommentBiz biz;

    /**
     * 添加评论信息接口
     * @param authQuestionsSaveBO
     * @return
     */
	@ApiOperation(value = "添加评论信息接口", notes = "添加评论信息接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<AuthQuestionsCommentSaveDTO> save(@RequestBody AuthQuestionsCommentSaveBO authQuestionsCommentSaveBO) {
		return biz.save(authQuestionsCommentSaveBO);
	}

	/**
     * 我的评论列表接口
     * @param authQuestionsSaveBO
     * @return
     */
	@ApiOperation(value = "我的评论列表接口", notes = "我的评论列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthQuestionsCommentListDTO>> list(@RequestBody AuthQuestionsCommentListBO authQuestionsCommentListBO) {
		return biz.list(authQuestionsCommentListBO);
	}
}
