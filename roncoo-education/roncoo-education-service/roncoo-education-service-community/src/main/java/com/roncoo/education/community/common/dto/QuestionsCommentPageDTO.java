package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsCommentPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;
    /**
     * 父ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "父ID")
    private Long parentId;
    /**
     * 问题ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "问题ID")
    private Long questionsId;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;
    /**
     * 评论者用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "评论者用户编号")
    private Long userNo;
    /**
	 * 评论者用户头像
	 */
	@ApiModelProperty(value = "评论者用户头像")
	private String userImg;
	/**
	 * 评论者用户昵称
	 */
	@ApiModelProperty(value = "评论者用户昵称")
	private String nickname;
    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容")
    private String content;
    /**
     * 被评论者用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "被评论者用户编号")
    private Long commentUserNo;
    /**
	 * 被评论者用户头像
	 */
	@ApiModelProperty(value = "被评论者用户头像")
	private String commentUserImg;
	/**
	 * 被评论者用户昵称
	 */
	@ApiModelProperty(value = "被评论者用户昵称")
	private String commentUickname;

    private List<QuestionsCommentPageDTO> list;
}
