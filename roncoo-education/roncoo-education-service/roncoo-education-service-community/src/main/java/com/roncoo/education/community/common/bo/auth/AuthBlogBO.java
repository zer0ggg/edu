package com.roncoo.education.community.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthBlogBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否存在封面(1,存在；0,不存在)
     */
    private Integer isImg;
    /**
     * 博客封面
     */
    private String blogImg;
    /**
     * 博客标题
     */
    private String title;
    /**
     * 博客摘要
     */
    private String summary;
    /**
     * 博客标签
     */
    private String tagsName;
    /**
     * 博客内容
     */
    private String content;
    /**
     * 质量度
     */
    private Integer qualityScore;
    /**
     * 博客关键词
     */
    private String keywords;
    /**
     * 编辑模式（1可视编辑，2markdown）
     */
    private Integer editMode;
    /**
     * 是否公开博客(1公开，0不公开)
     */
    private Integer isOpen;
    /**
     * 是否已经发布(1发布，0没发布)
     */
    private Integer isIssue;
    /**
     * 博客类型(1原创，2转载)
     */
    private Integer typeId;
    /**
     * 是否置顶(1置顶，0否)
     */
    private Integer isTop;
    /**
     * 收藏人数
     */
    private Integer collectionAcount;
    /**
     * 点赞人数
     */
    private Integer admireAcount;
    /**
     * 评论人数
     */
    private Integer commentAcount;
    /**
     * 阅读人数
     */
    private Integer readAcount;

}
