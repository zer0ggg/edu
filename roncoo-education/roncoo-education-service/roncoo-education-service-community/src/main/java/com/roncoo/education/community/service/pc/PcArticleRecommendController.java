package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.ArticleRecommendListRESP;
import com.roncoo.education.community.common.resp.ArticleRecommendViewRESP;
import com.roncoo.education.community.service.pc.biz.PcArticleRecommendBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文章推荐 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/article/recommend")
@Api(value = "community-文章推荐", tags = {"community-文章推荐"})
public class PcArticleRecommendController {

    @Autowired
    private PcArticleRecommendBiz biz;

    @ApiOperation(value = "文章推荐列表", notes = "文章推荐列表")
    @PostMapping(value = "/list")
    public Result<Page<ArticleRecommendListRESP>> list(@RequestBody ArticleRecommendListREQ articleRecommendListREQ) {
        return biz.list(articleRecommendListREQ);
    }


    @ApiOperation(value = "文章推荐添加", notes = "文章推荐添加")
    @SysLog(value = "文章推荐添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ArticleRecommendSaveREQ articleRecommendSaveREQ) {
        return biz.save(articleRecommendSaveREQ);
    }


    @ApiOperation(value = "批量关联", notes = "批量关联")
    @SysLog(value = "文章推荐批量关联")
    @PostMapping(value = "/save/batch")
    public Result<String> saveForBatch(@RequestBody ArticleRecommendSaveBathREQ req) {
        return biz.saveBatch(req);
    }

    @ApiOperation(value = "文章推荐查看", notes = "文章推荐查看")
    @GetMapping(value = "/view")
    public Result<ArticleRecommendViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "文章推荐修改", notes = "文章推荐修改")
    @SysLog(value = "文章推荐修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ArticleRecommendEditREQ articleRecommendEditREQ) {
        return biz.edit(articleRecommendEditREQ);
    }


    @ApiOperation(value = "文章推荐状态修改", notes = "文章推荐状态修改")
    @SysLog(value = "文章推荐状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ArticleRecommendUpdateStatusREQ articleRecommendUpdateStatusREQ) {
        return biz.updateStatus(articleRecommendUpdateStatusREQ);
    }


    @ApiOperation(value = "文章推荐删除", notes = "文章推荐删除")
    @SysLog(value = "文章推荐删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
