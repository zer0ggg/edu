package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.PlatEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.community.common.req.ArticleZoneCategoryEditREQ;
import com.roncoo.education.community.common.req.ArticleZoneCategoryListREQ;
import com.roncoo.education.community.common.req.ArticleZoneCategorySaveREQ;
import com.roncoo.education.community.common.resp.ArticleZoneCategoryListRESP;
import com.roncoo.education.community.common.resp.ArticleZoneCategoryViewRESP;
import com.roncoo.education.community.service.dao.ArticleZoneCategoryDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@Component
public class PcArticleZoneCategoryBiz extends BaseBiz {

    @Autowired
    private ArticleZoneCategoryDao dao;

    /**
    * 文章专区首页分类列表
    *
    * @param req 文章专区首页分类分页查询参数
    * @return 文章专区首页分类分页查询结果
    */
    public Result<Page<ArticleZoneCategoryListRESP>> list(ArticleZoneCategoryListREQ req) {
        ArticleZoneCategoryExample example = new ArticleZoneCategoryExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotBlank(req.getArticleZoneName())) {
            c.andArticleZoneNameLike(PageUtil.like(req.getArticleZoneName()));
        }
        if (req.getPlatShow() != null) {
            c.andPlatShowEqualTo(req.getPlatShow());
        }

        example.setOrderByClause("sort asc, id desc ");
        Page<ArticleZoneCategory> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ArticleZoneCategoryListRESP> respPage = PageUtil.transform(page, ArticleZoneCategoryListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 文章专区首页分类添加
    *
    * @param req 文章专区首页分类
    * @return 添加结果
    */
    public Result<String> save(ArticleZoneCategorySaveREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        ArticleZoneCategory articleZoneCategory = dao.getByArticleZoneName(req.getArticleZoneName());
        if (ObjectUtil.isNotNull(articleZoneCategory)) {
            return Result.error("已存在文章专区");
        }
        ArticleZoneCategory record = BeanUtil.copyProperties(req, ArticleZoneCategory.class);
        record.setStatusId(StatusIdEnum.YES.getCode());
        if(record.getPlatShow() == null){
            record.setPlatShow(PlatEnum.PC.getCode());
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 文章专区首页分类查看
    *
    * @param id 主键ID
    * @return 文章专区首页分类
    */
    public Result<ArticleZoneCategoryViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ArticleZoneCategoryViewRESP.class));
    }


    /**
    * 文章专区首页分类修改
    *
    * @param articleZoneCategoryEditREQ 文章专区首页分类修改对象
    * @return 修改结果
    */
    public Result<String> edit(ArticleZoneCategoryEditREQ articleZoneCategoryEditREQ) {
        ArticleZoneCategory record = BeanUtil.copyProperties(articleZoneCategoryEditREQ, ArticleZoneCategory.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 文章专区首页分类删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
