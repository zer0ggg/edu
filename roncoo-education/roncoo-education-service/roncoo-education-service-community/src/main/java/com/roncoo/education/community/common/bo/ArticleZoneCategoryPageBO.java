package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章专区首页分页查询
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneCategoryPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 显示平台(1:PC端显示;2:微信端展示)
	 */
	@ApiModelProperty(value = "显示平台(1:PC端显示;2:微信端展示)", required = true)
	private Integer platShow;
}
