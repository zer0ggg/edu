package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsUserRecordListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;
    /**
     * 问题ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "问题ID")
    private Long questionsId;
    /**
     * 用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "用户编号")
    private Long userNo;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String headImgUrl;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickname;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;

}
