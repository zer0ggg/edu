package com.roncoo.education.community.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.common.req.QuestionsUserRecordEditREQ;
import com.roncoo.education.community.common.req.QuestionsUserRecordListREQ;
import com.roncoo.education.community.common.req.QuestionsUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.QuestionsUserRecordListRESP;
import com.roncoo.education.community.common.resp.QuestionsUserRecordViewRESP;
import com.roncoo.education.community.service.dao.QuestionsUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 问题与用户关联
 *
 * @author wujing
 */
@Component
public class PcQuestionsUserRecordBiz extends BaseBiz {

    @Autowired
    private QuestionsUserRecordDao dao;

    /**
    * 问题与用户关联列表
    *
    * @param questionsUserRecordListREQ 问题与用户关联分页查询参数
    * @return 问题与用户关联分页查询结果
    */
    public Result<Page<QuestionsUserRecordListRESP>> list(QuestionsUserRecordListREQ questionsUserRecordListREQ) {
        QuestionsUserRecordExample example = new QuestionsUserRecordExample();
        //Criteria c = example.createCriteria();
        Page<QuestionsUserRecord> page = dao.listForPage(questionsUserRecordListREQ.getPageCurrent(), questionsUserRecordListREQ.getPageSize(), example);
        Page<QuestionsUserRecordListRESP> respPage = PageUtil.transform(page, QuestionsUserRecordListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 问题与用户关联添加
    *
    * @param questionsUserRecordSaveREQ 问题与用户关联
    * @return 添加结果
    */
    public Result<String> save(QuestionsUserRecordSaveREQ questionsUserRecordSaveREQ) {
        QuestionsUserRecord record = BeanUtil.copyProperties(questionsUserRecordSaveREQ, QuestionsUserRecord.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 问题与用户关联查看
    *
    * @param id 主键ID
    * @return 问题与用户关联
    */
    public Result<QuestionsUserRecordViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), QuestionsUserRecordViewRESP.class));
    }


    /**
    * 问题与用户关联修改
    *
    * @param questionsUserRecordEditREQ 问题与用户关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(QuestionsUserRecordEditREQ questionsUserRecordEditREQ) {
        QuestionsUserRecord record = BeanUtil.copyProperties(questionsUserRecordEditREQ, QuestionsUserRecord.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 问题与用户关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
