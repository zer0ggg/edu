package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class BlogViewBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客ID
	 */
	@ApiModelProperty(value = "博客ID")
	private Long id;
}
