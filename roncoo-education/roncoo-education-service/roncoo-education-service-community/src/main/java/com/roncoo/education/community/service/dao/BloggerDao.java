package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample;

public interface BloggerDao {
    int save(Blogger record);

    int deleteById(Long id);

    int updateById(Blogger record);

    int updateByExampleSelective(Blogger record, BloggerExample example);

    Blogger getById(Long id);

    Page<Blogger> listForPage(int pageCurrent, int pageSize, BloggerExample example);

    /**
     * 根据用户编号查找博主信息
     * @param userNo
     * @return
     */
	Blogger getByBloggerUserNo(Long userNo);
}