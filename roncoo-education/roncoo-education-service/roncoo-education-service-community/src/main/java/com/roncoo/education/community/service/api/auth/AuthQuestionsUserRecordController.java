package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordListBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsUserRecordListDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthQuestionsUserRecordBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/questions/user/record")
public class AuthQuestionsUserRecordController {

    @Autowired
    private AuthQuestionsUserRecordBiz biz;

    /**
     * 点赞、收藏问答
     * @param blogPageBO
     * @return
     */
	@ApiOperation(value = "点赞、收藏问答", notes = "点赞、收藏问答")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthQuestionsUserRecordSaveBO authQuestionsUserRecordSaveBO) {
		return biz.save(authQuestionsUserRecordSaveBO);
	}

	/**
     * 取消点赞、收藏
     * @param blogPageBO
     * @return
     */
	@ApiOperation(value = "取消点赞、收藏", notes = "取消点赞、收藏")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthQuestionsUserRecordDeleteBO authQuestionsUserRecordDeleteBO) {
		return biz.delete(authQuestionsUserRecordDeleteBO);
	}

	/**
     * 点赞、收藏列表
     * @param blogPageBO
     * @return
     */
	@ApiOperation(value = "点赞、收藏列表", notes = "点赞、收藏列表")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthQuestionsUserRecordListDTO>> list(@RequestBody AuthQuestionsUserRecordListBO authQuestionsUserRecordListBO) {
		return biz.list(authQuestionsUserRecordListBO);
	}
}
