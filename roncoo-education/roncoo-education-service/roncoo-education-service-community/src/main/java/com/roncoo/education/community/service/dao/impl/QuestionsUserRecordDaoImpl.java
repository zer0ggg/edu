package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.QuestionsUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.QuestionsUserRecordMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionsUserRecordDaoImpl implements QuestionsUserRecordDao {
    @Autowired
    private QuestionsUserRecordMapper questionsUserRecordMapper;

    @Override
	public int save(QuestionsUserRecord record) {
        record.setId(IdWorker.getId());
        return this.questionsUserRecordMapper.insertSelective(record);
    }

    @Override
	public int deleteById(Long id) {
        return this.questionsUserRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
	public int updateById(QuestionsUserRecord record) {
        return this.questionsUserRecordMapper.updateByPrimaryKeySelective(record);
    }

    @Override
	public int updateByExampleSelective(QuestionsUserRecord record, QuestionsUserRecordExample example) {
        return this.questionsUserRecordMapper.updateByExampleSelective(record, example);
    }

    @Override
	public QuestionsUserRecord getById(Long id) {
        return this.questionsUserRecordMapper.selectByPrimaryKey(id);
    }

    @Override
	public Page<QuestionsUserRecord> listForPage(int pageCurrent, int pageSize, QuestionsUserRecordExample example) {
        int count = this.questionsUserRecordMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<QuestionsUserRecord>(count, totalPage, pageCurrent, pageSize, this.questionsUserRecordMapper.selectByExample(example));
    }

	@Override
	public QuestionsUserRecord getByQuestionIdAndUserNoAndOpType(Long questionsId, Long userNo, Integer opType) {
		QuestionsUserRecordExample example = new QuestionsUserRecordExample();
		Criteria c = example.createCriteria();
		c.andQuestionsIdEqualTo(questionsId);
		c.andUserNoEqualTo(userNo);
		c.andOpTypeEqualTo(opType);
		List<QuestionsUserRecord> list = this.questionsUserRecordMapper.selectByExample(example);
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}
}
