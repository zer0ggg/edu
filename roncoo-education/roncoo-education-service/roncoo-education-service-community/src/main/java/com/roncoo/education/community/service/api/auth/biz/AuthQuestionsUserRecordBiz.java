package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.OpTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordListBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsUserRecordSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsUserRecordListDTO;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.QuestionsUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Component
public class AuthQuestionsUserRecordBiz {

    @Autowired
    private QuestionsUserRecordDao dao;
    @Autowired
    private QuestionsDao questionsDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    public Result<Integer> save(AuthQuestionsUserRecordSaveBO bo) {
        if (bo.getQuestionsId() == null) {
            return Result.error("questionsId不能为空");
        }
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        Questions questions = questionsDao.getById(bo.getQuestionsId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答信息");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("找不到该用户信息");
        }
        QuestionsUserRecord questionsUserRecord = dao.getByQuestionIdAndUserNoAndOpType(bo.getQuestionsId(), userExtVO.getUserNo(), bo.getOpType());
        if (ObjectUtil.isNotNull(questionsUserRecord)) {
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                return Result.error("已收藏该问答");
            }
            if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                return Result.error("已点赞该问答");
            }
        }
        QuestionsUserRecord record = BeanUtil.copyProperties(bo, QuestionsUserRecord.class);
        record.setUserNo(ThreadContext.userNo());
        int result = dao.save(record);
        if (result > 0) {
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                //收藏人数+1
                questions.setCollectionAcount(questions.getCollectionAcount() + 1);
            }
            if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                //点赞人数+1
                questions.setAdmireAcount(questions.getAdmireAcount() + 1);
            }
            questionsDao.updateById(questions);
            return Result.success(result);
        }
        return Result.error("操作失败");
    }

    public Result<Integer> delete(AuthQuestionsUserRecordDeleteBO bo) {
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        Questions questions = questionsDao.getById(bo.getQuestionsId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答信息");
        }
        QuestionsUserRecord questionsUserRecord = dao.getByQuestionIdAndUserNoAndOpType(bo.getQuestionsId(), questions.getUserNo(), bo.getOpType());
        if (ObjectUtil.isNull(questionsUserRecord)) {
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                return Result.error("没收藏该问答");
            }
            if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                return Result.error("没点赞该问答");
            }
        }
        int result = dao.deleteById(questionsUserRecord.getId());
        if (result > 0) {
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                //收藏人数-1
                questions.setCollectionAcount(questions.getCollectionAcount() - 1);
            }
            if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                //点赞人数-1
                questions.setAdmireAcount(questions.getAdmireAcount() - 1);
            }
            questionsDao.updateById(questions);
            return Result.success(result);
        }
        return Result.error("操作失败");
    }

    public Result<Page<AuthQuestionsUserRecordListDTO>> list(AuthQuestionsUserRecordListBO bo) {
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        UserExtVO vo = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(vo)) {
            return Result.error("获取不到用户信息");
        }
        QuestionsUserRecordExample example = new QuestionsUserRecordExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(vo.getUserNo());
        c.andOpTypeEqualTo(bo.getOpType());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause(" id desc");
        Page<QuestionsUserRecord> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthQuestionsUserRecordListDTO> pageDto = PageUtil.transform(page, AuthQuestionsUserRecordListDTO.class);
        for (AuthQuestionsUserRecordListDTO dto : pageDto.getList()) {
            Questions questions = questionsDao.getById(dto.getQuestionsId());
            if (ObjectUtil.isNotNull(questions)) {
                dto.setTitle(questions.getTitle());
            }
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
                dto.setNickname(userExtVO.getNickname());
            }
        }
        return Result.success(pageDto);
    }

}
