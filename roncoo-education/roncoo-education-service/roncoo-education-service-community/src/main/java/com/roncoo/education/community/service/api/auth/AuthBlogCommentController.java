package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserPageBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthBlogCommentUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogCommentUserSaveDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthBlogCommentBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客评论表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/blog/comment")
public class AuthBlogCommentController {

	@Autowired
	private AuthBlogCommentBiz biz;

	/**
	 * 评论列表展示接口(博主查看我的评论或者回复的评论)
	 *
	 * @param weblogCommentUserPageBO
	 * @return
	 */
	@ApiOperation(value = "评论列表展示接口", notes = "博主查看我的评论或者回复的评论(bloggerUserNo和userNo传一个即可，传userNo是查看我的评论，传bloggerUserNo是回复的评论)")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthBlogCommentUserPageDTO>> list(@RequestBody AuthBlogCommentUserPageBO authBlogCommentUserPageBO) {
		return biz.list(authBlogCommentUserPageBO);
	}

	/**
	 * 添加博客评论接口
	 *
	 * @param weblogCommentUserSaveBO
	 * @author wuyun
	 */
	@ApiOperation(value = "添加博客评论接口", notes = "博主对博客进行评论")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<AuthBlogCommentUserSaveDTO> save(@RequestBody AuthBlogCommentUserSaveBO authBlogCommentUserSaveBO) {
		return biz.save(authBlogCommentUserSaveBO);
	}

	/**
	 * 删除博客评论接口(状态删除)
	 *
	 * @param authBlogCommentUserDeleteBO
	 * @return
	 */
	@ApiOperation(value = "删除博客评论接口", notes = "博主对自己评论进行删除(状态删除)")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthBlogCommentUserDeleteBO authBlogCommentUserDeleteBO) {
		return biz.delete(authBlogCommentUserDeleteBO);
	}
}
