package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.BlogUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.BlogUserRecordMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BlogUserRecordDaoImpl implements BlogUserRecordDao {
    @Autowired
    private BlogUserRecordMapper blogUserRecordMapper;

    @Override
    public int save(BlogUserRecord record) {
        record.setId(IdWorker.getId());
        return this.blogUserRecordMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.blogUserRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(BlogUserRecord record) {
        return this.blogUserRecordMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(BlogUserRecord record, BlogUserRecordExample example) {
        return this.blogUserRecordMapper.updateByExampleSelective(record, example);
    }

    @Override
    public BlogUserRecord getById(Long id) {
        return this.blogUserRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<BlogUserRecord> listForPage(int pageCurrent, int pageSize, BlogUserRecordExample example) {
        int count = this.blogUserRecordMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<BlogUserRecord>(count, totalPage, pageCurrent, pageSize, this.blogUserRecordMapper.selectByExample(example));
    }

	@Override
	public BlogUserRecord getByUserNoAndBlogIdAndOpType(Long userNo, Long blogId, Integer opType) {
		BlogUserRecordExample example = new BlogUserRecordExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		c.andBlogIdEqualTo(blogId);
		c.andOpTypeEqualTo(opType);
		List<BlogUserRecord> list = this.blogUserRecordMapper.selectByExample(example);
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<BlogUserRecord> listByUserNoAndStatusId(Long userNo, Integer statusId) {
		BlogUserRecordExample example = new BlogUserRecordExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		c.andStatusIdEqualTo(statusId);
		return this.blogUserRecordMapper.selectByExample(example);
	}
}
