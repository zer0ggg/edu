package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 问答信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="QuestionsREQ", description="问答信息")
public class QuestionsREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "问题标题")
    private String title;

    @ApiModelProperty(value = "问题标签")
    private String tagsName;

    @ApiModelProperty(value = "问题关键词")
    private String keywords;

    @ApiModelProperty(value = "问题内容")
    private String content;

    @ApiModelProperty(value = "摘要")
    private String summary;

    @ApiModelProperty(value = "质量度")
    private Integer qualityScore;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionAcount;

    @ApiModelProperty(value = "点赞人数")
    private Integer admireAcount;

    @ApiModelProperty(value = "评论人数")
    private Integer commentAcount;

    @ApiModelProperty(value = "阅读人数")
    private Integer readAcount;

    @ApiModelProperty(value = "是否置顶(1置顶，0否)")
    private Integer isTop;

    @ApiModelProperty(value = "是否悬赏积分(1悬赏，0不悬赏)")
    private Integer isReward;

    @ApiModelProperty(value = "悬赏积分")
    private Integer rewardIntegral;

    @ApiModelProperty(value = "是否关联课程(1关联，0不关联)")
    private Integer isCourse;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "是否存在最佳回答(1有，0无)")
    private Integer isGood;

    @ApiModelProperty(value = "最佳回答ID")
    private Long goodCommentId;

    @ApiModelProperty(value = "最佳回答用户编号")
    private Long goodUserNo;

    @ApiModelProperty(value = "最佳回答内容")
    private String goodContent;
}
