package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.IsGoodEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.feign.qo.QuestionsQO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Component
public class FeignQuestionsBiz extends BaseBiz {

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private QuestionsDao dao;
    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private QuestionsCommentDao questionsCommentDao;

    public Page<QuestionsVO> listForPage(QuestionsQO qo) {
        QuestionsExample example = new QuestionsExample();
        Criteria c = example.createCriteria();
        if (qo.getUserNo() != null) {
            c.andUserNoEqualTo(qo.getUserNo());
        }
        if (StringUtils.hasText(qo.getTagsName())) {
            c.andTagsNameLike(PageUtil.like(qo.getTagsName()));
        }
        if (!StringUtils.isEmpty(qo.getTitle())) {
            c.andTitleLike(PageUtil.rightLike(qo.getTitle()));
        }
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }
        if (qo.getIsTop() != null) {
            c.andIsTopEqualTo(qo.getIsTop());
        }
        if (StringUtils.hasText(qo.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(qo.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" status_id desc, is_top desc, sort asc, id desc ");
        Page<Questions> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<QuestionsVO> listForPage = PageUtil.transform(page, QuestionsVO.class);
        for (QuestionsVO vo : listForPage.getList()) {
			/*if (vo.getCourseId() != null) {
				CourseVO courseVO = feignCourse.getById(vo.getCourseId());
				if (ObjectUtil.isNotNull(courseVO)) {
					vo.setCourseName(courseVO.getCourseName());
				}
			}*/

            // 获取用户信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    vo.setNickname(userExtVO.getNickname());
                }
            }
        }
        return listForPage;
    }

    public int save(QuestionsQO qo) {
        Questions record = BeanUtil.copyProperties(qo, Questions.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public QuestionsVO getById(Long id) {
        Questions record = dao.getById(id);
        QuestionsVO questionsVO = BeanUtil.copyProperties(record, QuestionsVO.class);
        // 存在最佳回答
        if (IsGoodEnum.YES.getCode().equals(questionsVO.getIsGood())) {
            // 查询评论信息
            QuestionsComment questionsComment = questionsCommentDao.getById(questionsVO.getGoodCommentId());
            if (ObjectUtil.isNull(questionsComment)) {
                throw new BaseException("评论表里查询不到最佳评论");
            }
        }
        // 最新评论
        QuestionsComment questionsComment = questionsCommentDao.getByNewest();
        if (ObjectUtil.isNotNull(questionsComment)) {
            questionsVO.setNewestCommentId(questionsComment.getId());
            UserExtVO userExtVO = feignUserExt.getByUserNo(questionsComment.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    questionsVO.setNewestNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    questionsVO.setNewestNickname(userExtVO.getNickname());
                }
            }
            questionsVO.setNewestUserNo(questionsComment.getUserNo());
            questionsVO.setNewestContent(questionsComment.getContent());
            questionsVO.setNewestContentCreate(questionsComment.getGmtCreate());
        }

        UserExtVO userExtVO = feignUserExt.getByUserNo(questionsVO.getUserNo());
        if (ObjectUtil.isNotNull(userExtVO)) {
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                questionsVO.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
            } else {
                questionsVO.setNickname(userExtVO.getNickname());
            }
            questionsVO.setHeadImgUrl(userExtVO.getHeadImgUrl());
        }
        return questionsVO;
    }

    public int updateById(QuestionsQO qo) {
        Questions questions = dao.getById(qo.getId());
        if (ObjectUtil.isNull(questions)) {
            throw new BaseException("找不到问题信息");
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(questions.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            throw new BaseException("该账号已被禁用，请联系管理员");
        }
        Questions record = BeanUtil.copyProperties(qo, Questions.class);
        int results = dao.updateById(record);
        if (results > 0) {
            if (StatusIdEnum.YES.getCode().equals(record.getStatusId())) {
                try {
                    // 插入els或者更新els
                    EsQuestions esQuestions = BeanUtil.copyProperties(dao.getById(questions.getId()), EsQuestions.class);
                    esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esQuestions.setNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsQuestions.QUESTIONS));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                }
            }
            return results;
        }
        throw new BaseException("更新失败");
    }

    public int updateGoodComment(QuestionsQO qo) {
        if (qo.getGoodCommentId() == null) {
            throw new BaseException("评论ID不能为空");
        }
        // 查询问答信息表
        Questions questions = dao.getById(qo.getId());
        if (ObjectUtils.isEmpty(questions)) {
            throw new BaseException("问答信息表为空");
        }
        // 查询评论信息表
        QuestionsComment questionsComment = questionsCommentDao.getById(qo.getGoodCommentId());
        if (ObjectUtils.isEmpty(questionsComment)) {
            throw new BaseException("评论表里查询不到该评论");
        }

        if (IsGoodEnum.YES.getCode().equals(qo.getIsGood())) {
            questions.setGoodUserNo(questionsComment.getUserNo());
            questions.setGoodContent(questionsComment.getContent());
            questions.setGoodCommentId(questionsComment.getId());

            // 问答存在最佳评论时,初始化评论表的排序
            if (IsGoodEnum.YES.getCode().equals(questions.getIsGood())) {
                QuestionsComment comment = questionsCommentDao.getById(questions.getGoodCommentId());
                if (ObjectUtils.isEmpty(comment)) {
                    throw new BaseException("评论表里查询不到最佳评论");
                }
                comment.setSort(1);
                questionsCommentDao.updateById(comment);
            }

            // 更新评论表最佳评论排序置顶
            questionsComment.setSort(1000);
            questionsCommentDao.updateById(questionsComment);
        } else if (IsGoodEnum.NO.getCode().equals(qo.getIsGood())) {

            // 更新评论表最佳评论排序为1
            questionsComment.setSort(1);
            questionsCommentDao.updateById(questionsComment);
        }
        // 设置问答信表最佳评论
        return dao.updateById(questions);
    }

    /**
     * 一键导入ES
     *
     * @param qo
     * @return
     */
    public boolean addEs(QuestionsQO qo) {
        try {
            if (!StringUtils.isEmpty(qo.getIds())) {
                String[] questionsId = qo.getIds().split(",");
                List<IndexQuery> queries = new ArrayList<>();
                for (String i : questionsId) {
                    Questions questions = dao.getById(Long.valueOf(i));
                    // 根据userNo查找用户教育信息
                    UserExtVO userExtVO = feignUserExt.getByUserNo(questions.getUserNo());
                    if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
                        throw new BaseException("用户异常");
                    }
                    EsQuestions esQuestions = BeanUtil.copyProperties(questions, EsQuestions.class);
                    esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esQuestions.setNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                    queries.add(query);
                }
                elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsQuestions.QUESTIONS));
                return true;
            }
        } catch (Exception e) {
            logger.error("elasticsearch导入数据失败", e);
            return false;
        }
        return false;
    }

    public int updateStatusId(QuestionsQO qo) {
        Questions questions = dao.getById(qo.getId());
        if (ObjectUtil.isNull(questions)) {
            throw new BaseException("找不到问答信息");
        }
        Questions record = BeanUtil.copyProperties(qo, Questions.class);
        Blogger blogger = bloggerDao.getByBloggerUserNo(questions.getUserNo());
        if (ObjectUtil.isNull(blogger)) {
            throw new BaseException("找不到博主信息");
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(blogger.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            throw new BaseException("该账号已被禁用，请联系管理员");
        }
        if (!StatusIdEnum.YES.getCode().equals(record.getStatusId())) {
            blogger.setQuestionsAccount(blogger.getQuestionsAccount() - 1);
            // 删除els数据
            elasticsearchRestTemplate.delete(String.valueOf(qo.getId()), EsQuestions.class);
        } else {
            try {
                // 插入els或者更新els
                EsQuestions esQuestions = BeanUtil.copyProperties(dao.getById(questions.getId()), EsQuestions.class);
                esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                    esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    esQuestions.setNickname(userExtVO.getNickname());
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsQuestions.QUESTIONS));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
            }
            blogger.setQuestionsAccount(blogger.getQuestionsAccount() + 1);
        }
        bloggerDao.updateById(blogger);
        return dao.updateById(record);
    }
}
