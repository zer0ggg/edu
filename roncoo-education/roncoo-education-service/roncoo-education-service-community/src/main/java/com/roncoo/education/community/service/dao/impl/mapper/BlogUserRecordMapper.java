package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BlogUserRecordMapper {
    int countByExample(BlogUserRecordExample example);

    int deleteByExample(BlogUserRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BlogUserRecord record);

    int insertSelective(BlogUserRecord record);

    List<BlogUserRecord> selectByExample(BlogUserRecordExample example);

    BlogUserRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BlogUserRecord record, @Param("example") BlogUserRecordExample example);

    int updateByExample(@Param("record") BlogUserRecord record, @Param("example") BlogUserRecordExample example);

    int updateByPrimaryKeySelective(BlogUserRecord record);

    int updateByPrimaryKey(BlogUserRecord record);
}
