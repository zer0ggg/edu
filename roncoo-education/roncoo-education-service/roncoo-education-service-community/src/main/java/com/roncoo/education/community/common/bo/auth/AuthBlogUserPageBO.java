package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogUserPageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 博客标题
     */
    @ApiModelProperty(value = "博客标题")
    private String title;
    /**
     * 博客类型标签(1公开，2私密，3草稿，4回收站)
     */
    @ApiModelProperty(value = "博客类型标签(1公开，2私密，3草稿，4回收站)")
    private Integer tagTypes;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize = 20;

}
