package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;

import java.util.List;

public interface BloggerUserRecordDao {
    int save(BloggerUserRecord record);

    int deleteById(Long id);

    int updateById(BloggerUserRecord record);

    int updateByExampleSelective(BloggerUserRecord record, BloggerUserRecordExample example);

    BloggerUserRecord getById(Long id);

    Page<BloggerUserRecord> listForPage(int pageCurrent, int pageSize, BloggerUserRecordExample example);

    /**
     * 根据用户编号、状态查询关注列表
     * @param userNo
     * @param code
     * @return
     */
	List<BloggerUserRecord> listByUserNoAndStatusId(Long userNo, Integer statusId);

	/**
	 * 根据用户编号和博主用户编号查询关联信息
	 * @param userNo
	 * @param userNo2
	 * @return
	 */
	BloggerUserRecord getByUserNoAndBloggerUserNo(Long userNo, Long bloggerUserNo);
}
