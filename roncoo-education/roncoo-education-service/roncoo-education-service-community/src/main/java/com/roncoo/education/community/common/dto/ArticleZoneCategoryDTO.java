package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneCategoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 专区ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "专区ID")
	private Long id;
	/**
	 * 文章专区名称
	 */
	@ApiModelProperty(value = "文章专区名称")
	private String articleZoneName;
	/**
	 * 专区描述
	 */
	@ApiModelProperty(value = "专区描述")
	private String articleZoneDesc;
}
