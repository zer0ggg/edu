package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.ArticleZoneCategoryEditREQ;
import com.roncoo.education.community.common.req.ArticleZoneCategoryListREQ;
import com.roncoo.education.community.common.req.ArticleZoneCategorySaveREQ;
import com.roncoo.education.community.common.resp.ArticleZoneCategoryListRESP;
import com.roncoo.education.community.common.resp.ArticleZoneCategoryViewRESP;
import com.roncoo.education.community.service.pc.biz.PcArticleZoneCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文章专区首页分类 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/article/zone/category")
@Api(value = "community-文章专区首页分类", tags = {"community-文章专区首页分类"})
public class PcArticleZoneCategoryController {

    @Autowired
    private PcArticleZoneCategoryBiz biz;

    @ApiOperation(value = "文章专区首页分类列表", notes = "文章专区首页分类列表")
    @PostMapping(value = "/list")
    public Result<Page<ArticleZoneCategoryListRESP>> list(@RequestBody ArticleZoneCategoryListREQ articleZoneCategoryListREQ) {
        return biz.list(articleZoneCategoryListREQ);
    }


    @ApiOperation(value = "文章专区首页分类添加", notes = "文章专区首页分类添加")
    @SysLog(value = "文章专区首页分类添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ArticleZoneCategorySaveREQ articleZoneCategorySaveREQ) {
        return biz.save(articleZoneCategorySaveREQ);
    }

    @ApiOperation(value = "文章专区首页分类查看", notes = "文章专区首页分类查看")
    @GetMapping(value = "/view")
    public Result<ArticleZoneCategoryViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "文章专区首页分类修改", notes = "文章专区首页分类修改")
    @SysLog(value = "文章专区首页分类修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ArticleZoneCategoryEditREQ articleZoneCategoryEditREQ) {
        return biz.edit(articleZoneCategoryEditREQ);
    }


    @ApiOperation(value = "文章专区首页分类删除", notes = "文章专区首页分类删除")
    @SysLog(value = "文章专区首页分类删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
