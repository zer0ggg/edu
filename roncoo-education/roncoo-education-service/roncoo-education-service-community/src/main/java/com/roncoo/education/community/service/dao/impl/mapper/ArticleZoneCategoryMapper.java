package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleZoneCategoryMapper {
    int countByExample(ArticleZoneCategoryExample example);

    int deleteByExample(ArticleZoneCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ArticleZoneCategory record);

    int insertSelective(ArticleZoneCategory record);

    List<ArticleZoneCategory> selectByExample(ArticleZoneCategoryExample example);

    ArticleZoneCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ArticleZoneCategory record, @Param("example") ArticleZoneCategoryExample example);

    int updateByExample(@Param("record") ArticleZoneCategory record, @Param("example") ArticleZoneCategoryExample example);

    int updateByPrimaryKeySelective(ArticleZoneCategory record);

    int updateByPrimaryKey(ArticleZoneCategory record);
}
