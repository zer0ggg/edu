package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.qo.BlogQO;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.community.service.feign.biz.FeignBlogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客信息表
 *
 * @author wujing
 */
@RestController
public class FeignBlogController extends BaseController implements IFeignBlog {

	@Autowired
	private FeignBlogBiz biz;

	@Override
	public Page<BlogVO> listForPage(@RequestBody BlogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody BlogQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody BlogQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public BlogVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public BlogVO getByIdAndStatusId(@PathVariable(value = "id") Long id,@PathVariable(value = "statusId") Integer statusId) {
		return biz.getByIdAndStatusId(id,statusId);
	}

	@Override
	public boolean addEs(@RequestBody BlogQO qo) {
		return biz.addEs(qo);
	}

}
