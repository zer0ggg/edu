package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserAttentionPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserViewDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthBlogBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/blog")
public class AuthBlogController {

	@Autowired
	private AuthBlogBiz biz;

	/**
	 * 添加博客信息接口
	 *
	 * @param authBlogUserSaveBO
	 * @return
	 */
	@ApiOperation(value = "添加博客信息接口", notes = "博主写博客")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthBlogUserSaveBO authBlogUserSaveBO) {
		return biz.save(authBlogUserSaveBO);
	}

	/**
	 * 删除博客信息接口
	 *
	 * @param authBlogUserDeleteBO
	 * @return
	 */
	@ApiOperation(value = "删除博客信息接口", notes = "博主彻底删除博客")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthBlogUserDeleteBO authBlogUserDeleteBO) {
		return biz.delete(authBlogUserDeleteBO);
	}

	/**
	 * 修改博客信息接口
	 *
	 * @param authBlogUserUpdateBO
	 * @return
	 */
	@ApiOperation(value = "修改博客信息接口", notes = "博主修改博客信息")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<Integer> update(@RequestBody AuthBlogUserUpdateBO authBlogUserUpdateBO) {
		return biz.update(authBlogUserUpdateBO);
	}

	/**
	 * 博客信息列表展示接口(博主查看自己的博客列表)
	 *
	 * @param authBlogUserPageBO
	 * @return
	 */
	@ApiOperation(value = "博客信息列表展示接口", notes = "博主查看自己的博客列表")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthBlogUserPageDTO>> list(@RequestBody AuthBlogUserPageBO authBlogUserPageBO) {
		return biz.list(authBlogUserPageBO);
	}

	/**
	 * 下架博客信息接口
	 *
	 * @param authBlogUserStandBO
	 * @return
	 */
	@ApiOperation(value = "下架博客信息接口", notes = "博主对博客下架操作")
	@RequestMapping(value = "/stand", method = RequestMethod.POST)
	public Result<Integer> stand(@RequestBody AuthBlogUserStandBO authBlogUserStandBO) {
		return biz.stand(authBlogUserStandBO);
	}

	/**
	 * 博客信息详情查看接口
	 *
	 * @param authBlogUserViewBO
	 * @return
	 */
	@ApiOperation(value = "博客信息详情查看接口", notes = "博客信息详情查看")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthBlogUserViewDTO> view(@RequestBody AuthBlogUserViewBO authBlogUserViewBO) {
		return biz.view(authBlogUserViewBO);
	}

	/**
	 * 首页关注的博客信息列表
	 *
	 * @return
	 */
	@ApiOperation(value = "首页关注的博客信息列表", notes = "首页关注的博客信息列表")
	@RequestMapping(value = "/attention/list", method = RequestMethod.POST)
	public Result<Page<AuthBlogUserAttentionPageDTO>> attentionList(@RequestBody AuthBlogUserAttentionPageBO authBlogUserAttentionPageBO) {
		return biz.attentionList(authBlogUserAttentionPageBO);
	}
}
