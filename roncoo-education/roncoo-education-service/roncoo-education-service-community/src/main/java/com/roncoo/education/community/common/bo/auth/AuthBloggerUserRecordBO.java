package com.roncoo.education.community.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUserRecordBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 博主用户头像
     */
    private Long bloggerId;
    /**
     * 操作类型(1收藏，2点赞)
     */
    private Integer opType;
    /**
     * 用户IP
     */
    private String userIp;
    /**
     * 用户终端
     */
    private String userTerminal;

}
