package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 博主与用户关注关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="BloggerUserRecordREQ", description="博主与用户关注关联")
public class BloggerUserRecordREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "博主用户编号")
    private Long bloggerUserNo;

    @ApiModelProperty(value = "操作类型(1收藏，2点赞)")
    private Integer opType;

    @ApiModelProperty(value = "关注者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户IP")
    private String userIp;

    @ApiModelProperty(value = "用户终端")
    private String userTerminal;
}
