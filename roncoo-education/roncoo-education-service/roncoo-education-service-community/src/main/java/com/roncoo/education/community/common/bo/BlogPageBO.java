package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class BlogPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客标签
	 */
	@ApiModelProperty(value = "博客标签", required = false)
	private String tagsName;
	/**
	 * 博客标题
	 */
	@ApiModelProperty(value = "博客标题", required = false)
	private String title;
	/**
	 * 博主用户编号
	 */
	@ApiModelProperty(value = "博主用户编号", required = false)
	private Long bloggerUserNo;
	/**
	 * 文章类型(1:博客;2:资讯)
	 */
	@ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
	private Integer articleType = 1;
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
	/**
	 * 博客栏目(1最新文章，2热门文章，3推荐文章)
	 */
	@ApiModelProperty(value = "博客栏目(1最新文章，2热门文章，3推荐文章)", required = false)
	private Integer sections;
}
