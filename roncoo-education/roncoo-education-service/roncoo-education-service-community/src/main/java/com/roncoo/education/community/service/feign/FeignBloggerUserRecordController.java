package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBloggerUserRecord;
import com.roncoo.education.community.feign.qo.BloggerUserRecordQO;
import com.roncoo.education.community.feign.vo.BloggerUserRecordVO;
import com.roncoo.education.community.service.feign.biz.FeignBloggerUserRecordBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@RestController
public class FeignBloggerUserRecordController extends BaseController implements IFeignBloggerUserRecord {

	@Autowired
	private FeignBloggerUserRecordBiz biz;

	@Override
	public Page<BloggerUserRecordVO> listForPage(@RequestBody BloggerUserRecordQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody BloggerUserRecordQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody BloggerUserRecordQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public BloggerUserRecordVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public BloggerUserRecordVO getByUserNoAndBloggerUserNo(@RequestBody BloggerUserRecordQO qo) {
		return biz.getByUserNoAndBloggerUserNo(qo);
	}

}
