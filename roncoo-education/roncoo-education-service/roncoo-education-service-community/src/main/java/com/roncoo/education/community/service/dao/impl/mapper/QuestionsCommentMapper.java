package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuestionsCommentMapper {
    int countByExample(QuestionsCommentExample example);

    int deleteByExample(QuestionsCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(QuestionsComment record);

    int insertSelective(QuestionsComment record);

    List<QuestionsComment> selectByExampleWithBLOBs(QuestionsCommentExample example);

    List<QuestionsComment> selectByExample(QuestionsCommentExample example);

    QuestionsComment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") QuestionsComment record, @Param("example") QuestionsCommentExample example);

    int updateByExampleWithBLOBs(@Param("record") QuestionsComment record, @Param("example") QuestionsCommentExample example);

    int updateByExample(@Param("record") QuestionsComment record, @Param("example") QuestionsCommentExample example);

    int updateByPrimaryKeySelective(QuestionsComment record);

    int updateByPrimaryKeyWithBLOBs(QuestionsComment record);

    int updateByPrimaryKey(QuestionsComment record);
}
