package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleZoneRefMapper {
    int countByExample(ArticleZoneRefExample example);

    int deleteByExample(ArticleZoneRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ArticleZoneRef record);

    int insertSelective(ArticleZoneRef record);

    List<ArticleZoneRef> selectByExample(ArticleZoneRefExample example);

    ArticleZoneRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ArticleZoneRef record, @Param("example") ArticleZoneRefExample example);

    int updateByExample(@Param("record") ArticleZoneRef record, @Param("example") ArticleZoneRefExample example);

    int updateByPrimaryKeySelective(ArticleZoneRef record);

    int updateByPrimaryKey(ArticleZoneRef record);
}
