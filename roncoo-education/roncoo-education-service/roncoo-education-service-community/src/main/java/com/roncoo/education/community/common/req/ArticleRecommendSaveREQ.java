package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 文章推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ArticleRecommendSaveREQ", description = "文章推荐添加")
public class ArticleRecommendSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章id", required = true)
    private Long artcleId;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
    private Integer articleType;
}
