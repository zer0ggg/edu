package com.roncoo.education.community.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 博主信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BloggerViewRESP", description = "博主信息查看")
public class BloggerViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "博主简介")
    private String introduction;

    @ApiModelProperty(value = "粉丝人数")
    private Integer fansAccount;

    @ApiModelProperty(value = "关注人数")
    private Integer attentionAcount;

    @ApiModelProperty(value = "博客数量")
    private Integer weblogAccount;

    @ApiModelProperty(value = "资讯数量")
    private Integer informationAccount;

    @ApiModelProperty(value = "我的问答数量")
    private Integer questionsAccount;

    @ApiModelProperty(value = "我的回答数量")
    private Integer answerAccount;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "用户头像")
    private String headImgUrl;
}
