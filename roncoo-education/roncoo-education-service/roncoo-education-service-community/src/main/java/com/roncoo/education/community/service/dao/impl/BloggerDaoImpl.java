package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.BloggerMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BloggerDaoImpl implements BloggerDao {
    @Autowired
    private BloggerMapper bloggerMapper;

    @Override
    public int save(Blogger record) {
        record.setId(IdWorker.getId());
        return this.bloggerMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.bloggerMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Blogger record) {
        return this.bloggerMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(Blogger record, BloggerExample example) {
        return this.bloggerMapper.updateByExampleSelective(record, example);
    }

    @Override
    public Blogger getById(Long id) {
        return this.bloggerMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Blogger> listForPage(int pageCurrent, int pageSize, BloggerExample example) {
        int count = this.bloggerMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<Blogger>(count, totalPage, pageCurrent, pageSize, this.bloggerMapper.selectByExample(example));
    }

	@Override
	public Blogger getByBloggerUserNo(Long userNo) {
		BloggerExample example = new BloggerExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		List<Blogger> list = this.bloggerMapper.selectByExample(example);
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}
}
