package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsUserRecordDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 问题ID
     */
    @ApiModelProperty(value = "问题ID")
    private Long questionsId;
    /**
     * 用户操作类型(1收藏，2点赞)
     */
    @ApiModelProperty(value = "用户操作类型(1收藏，2点赞)")
    private Integer opType;

}
