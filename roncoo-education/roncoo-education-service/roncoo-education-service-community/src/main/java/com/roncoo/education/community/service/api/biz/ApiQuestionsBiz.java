package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsGoodEnum;
import com.roncoo.education.common.core.enums.IsHfield;
import com.roncoo.education.common.core.enums.SectionsEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.elasticsearch.EsPageUtil;
import com.roncoo.education.community.common.bo.QuestionsPageBO;
import com.roncoo.education.community.common.bo.QuestionsSearchPageBO;
import com.roncoo.education.community.common.bo.QuestionsViewBO;
import com.roncoo.education.community.common.dto.QuestionsPageDTO;
import com.roncoo.education.community.common.dto.QuestionsSearchPageDTO;
import com.roncoo.education.community.common.dto.QuestionsViewDTO;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Component
public class ApiQuestionsBiz {

    @Autowired
    private QuestionsDao dao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    public Result<Page<QuestionsPageDTO>> list(QuestionsPageBO bo) {
        QuestionsExample example = new QuestionsExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        if (StringUtils.hasText(bo.getTagsName())) {
            c.andTagsNameLike(bo.getTagsName());
        }
        if (StringUtils.hasText(bo.getTitle())) {
            c.andTitleLike(PageUtil.like(bo.getTitle()));
        }
        // 推荐、热门按照一季度时间内进行排序
        if (SectionsEnum.RECOMMEND.getCode().equals(bo.getSections()) || SectionsEnum.HOT.getCode().equals(bo.getSections())) {
            // 当前时间
            Date date = DateUtil.parseDate(DateUtil.formatToString("yyyy-MM-dd", System.currentTimeMillis()));
            // 一季度内的时间
            c.andGmtModifiedGreaterThanOrEqualTo(DateUtil.subDate(date, 90));
            c.andGmtModifiedLessThanOrEqualTo(date);
        }
        if (bo.getSections() == null || bo.getSections() == 0) {
            // 默认
            example.setOrderByClause(" is_top desc, sort asc, id desc ");
        } else if (bo.getSections().equals(SectionsEnum.NEW.getCode())) {
            // 最新
            example.setOrderByClause(" is_top desc, id desc ");
        } else if (bo.getSections().equals(SectionsEnum.HOT.getCode())) {
            // 热门
            example.setOrderByClause(" quality_score desc, read_acount desc, id desc ");
        }
        Page<Questions> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<QuestionsPageDTO> dtoPage = PageUtil.transform(page, QuestionsPageDTO.class);
        for (QuestionsPageDTO dto : dtoPage.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    dto.setNickname(userExtVO.getNickname());
                }
                dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
            }
        }
        return Result.success(dtoPage);
    }

    public Result<QuestionsViewDTO> view(QuestionsViewBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Questions questions = dao.getById(bo.getId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答");
        }

        // 阅读数量+1
        questions.setReadAcount(questions.getReadAcount() + 1);
        // 质量度+1
        questions.setQualityScore(questions.getQualityScore() + 1);
        QuestionsViewDTO dto = BeanUtil.copyProperties(questions, QuestionsViewDTO.class);

        UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
        } else {
            dto.setNickname(userExtVO.getNickname());
        }
        dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
        if (IsGoodEnum.YES.getCode().equals(questions.getIsGood())) {
            UserExtVO vo = feignUserExt.getByUserNo(questions.getGoodUserNo());
            if (StringUtils.isEmpty(vo.getNickname())) {
                dto.setGoodUserNickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
            } else {
                dto.setGoodUserNickname(vo.getNickname());
            }
            dto.setGoodUserHeadImgUrl(vo.getHeadImgUrl());
        }

        Questions quest = new Questions();
        quest.setId(questions.getId());
        quest.setReadAcount(questions.getReadAcount());
        quest.setQualityScore(questions.getQualityScore());
        dao.updateById(quest);

        return Result.success(dto);
    }

    public Result<Page<QuestionsSearchPageDTO>> searchList(QuestionsSearchPageBO bo) {

        NativeSearchQueryBuilder nsb = new NativeSearchQueryBuilder();
        if (bo.getIsHfield() != null && bo.getIsHfield().equals(IsHfield.YES.getCode())) {
            String heightField = "title";
            HighlightBuilder.Field hfield = new HighlightBuilder.Field(heightField).preTags("<mark>").postTags("</mark>");
            // 高亮字段
            nsb.withHighlightFields(hfield);
        }
        // 评分排序（_source）
        nsb.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
        // 更新时间排序（gmtModified）
        nsb.withSort(new FieldSortBuilder("gmtModified").order(SortOrder.DESC));
        // 观看人数排序（readAcount）
        nsb.withSort(new FieldSortBuilder("readAcount").order(SortOrder.DESC));
        nsb.withPageable(PageRequest.of(bo.getPageCurrent() - 1, bo.getPageSize()));
        // 复合查询，外套boolQuery
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        // 精确查询termQuery不分词，must参数等价于AND
        if (StringUtils.hasText(bo.getTagsName())) {
            // 多字段匹配
            qb.must(QueryBuilders.multiMatchQuery(bo.getTagsName(), "tagsName", "summary").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));

        }
        // 模糊查询multiMatchQuery，最佳字段best_fields
        if (StringUtils.hasText(bo.getTitle())) {
            // 多字段匹配
            qb.must(QueryBuilders.multiMatchQuery(bo.getTitle(), "title", "summary").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));

        }
        nsb.withQuery(qb);
        SearchHits<EsQuestions> page = elasticsearchRestTemplate.search(nsb.build(), EsQuestions.class, IndexCoordinates.of(EsQuestions.QUESTIONS));
        return Result.success(EsPageUtil.transform(page, bo.getPageCurrent(), bo.getPageSize(), QuestionsSearchPageDTO.class));
    }
}
