package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 文章专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneRefSaveBatchREQ", description="文章专区关联  批量添加")
public class ArticleZoneRefSaveBatchREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "专区ID不能为空")
    @ApiModelProperty(value = "专区ID",required = true)
    private Long articleZoneId;

    @NotEmpty(message = "文章IDs不能为空")
    @ApiModelProperty(value = "文章IDs",required = true)
    private String articleIds;
}
