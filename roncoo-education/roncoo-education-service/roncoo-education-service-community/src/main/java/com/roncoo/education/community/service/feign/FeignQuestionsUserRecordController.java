package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignQuestionsUserRecord;
import com.roncoo.education.community.feign.qo.QuestionsUserRecordQO;
import com.roncoo.education.community.feign.vo.QuestionsUserRecordVO;
import com.roncoo.education.community.service.feign.biz.FeignQuestionsUserRecordBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@RestController
public class FeignQuestionsUserRecordController extends BaseController implements IFeignQuestionsUserRecord {

	@Autowired
	private FeignQuestionsUserRecordBiz biz;

	@Override
	public Page<QuestionsUserRecordVO> listForPage(@RequestBody QuestionsUserRecordQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody QuestionsUserRecordQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody QuestionsUserRecordQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public QuestionsUserRecordVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

}
