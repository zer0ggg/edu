package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.feign.qo.BlogUserRecordQO;
import com.roncoo.education.community.feign.vo.BlogUserRecordVO;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BlogUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 博客与用户关联表
 *
 * @author wujing
 */
@Component
public class FeignBlogUserRecordBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BlogUserRecordDao dao;

	@Autowired
	private BlogDao blogDao;

	public Page<BlogUserRecordVO> listForPage(BlogUserRecordQO qo) {
		BlogUserRecordExample example = new BlogUserRecordExample();
		Criteria c = example.createCriteria();
		if (qo.getUserNo() != null) {
			c.andUserNoEqualTo(qo.getUserNo());
		}
		if (qo.getOpType() != null) {
			c.andOpTypeEqualTo(qo.getOpType());
		}
		if (StringUtils.hasText(qo.getBeginGmtCreate())) {
			c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
		}
		if (StringUtils.hasText(qo.getEndGmtCreate())) {
			c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<BlogUserRecord> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<BlogUserRecordVO> listForPage = PageUtil.transform(page, BlogUserRecordVO.class);
		for (BlogUserRecordVO vo : listForPage.getList()) {
			Blog blog = blogDao.getById(vo.getBlogId());
			if (ObjectUtil.isNull(blog)) {
				throw new BaseException("找不到博客信息");
			}
			vo.setTitle(blog.getTitle());

			UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
			if (ObjectUtil.isNotNull(userExtVO)) {
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					vo.setNickname(userExtVO.getNickname());
				}
			}

		}
		return listForPage;
	}

	public int save(BlogUserRecordQO qo) {
		BlogUserRecord record = BeanUtil.copyProperties(qo, BlogUserRecord.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		BlogUserRecord blogUserRecord = dao.getById(id);
		if (ObjectUtil.isNull(blogUserRecord)) {
			throw new BaseException("id不正确");
		}
		int resultNum = dao.deleteById(id);
		// 更新博客收藏人数和质量度
		if (resultNum > 0) {
			Blog blog = blogDao.getById(blogUserRecord.getBlogId());
			// 收藏人数-1
			blog.setCollectionAcount(blog.getCollectionAcount() - 1);
			// 质量度-1
			blog.setQualityScore(blog.getQualityScore() - 1);
			blogDao.updateById(blog);
		}
		return resultNum;
	}

	public BlogUserRecordVO getById(Long id) {
		BlogUserRecord record = dao.getById(id);
		return BeanUtil.copyProperties(record, BlogUserRecordVO.class);
	}

	public int updateById(BlogUserRecordQO qo) {
		BlogUserRecord record = BeanUtil.copyProperties(qo, BlogUserRecord.class);
		return dao.updateById(record);
	}

}
