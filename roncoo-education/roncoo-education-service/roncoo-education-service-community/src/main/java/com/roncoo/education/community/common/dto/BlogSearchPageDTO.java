package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class BlogSearchPageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博客ID")
	private Long id;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date gmtModified;
	/**
	 * 封面
	 */
	@ApiModelProperty(value = "封面")
	private String blogImg;
	/**
	 * 博客用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博客用户编号")
	private Long userNo;
	/**
	 * 博客用户昵称
	 */
	@ApiModelProperty(value = "博客用户昵称")
	private String bloggerNickname;
	/**
	 * 博客标题
	 */
	@ApiModelProperty(value = "博客标题")
	private String title;
	/**
	 * 博客摘要
	 */
	@ApiModelProperty(value = "博客摘要")
	private String summary;
	/**
	 * 博客类型(1原创，2转载)
	 */
	@ApiModelProperty(value = "博客类型(1原创，2转载)")
	private Integer typeId;
	/**
	 * 是否置顶(1置顶，0否)
	 */
	@ApiModelProperty(value = "是否置顶(1置顶，0否)")
	private Integer isTop;
	/**
	 * 收藏人数
	 */
	@ApiModelProperty(value = "收藏人数")
	private Integer collectionAcount;
	/**
	 * 点赞人数
	 */
	@ApiModelProperty(value = "点赞人数")
	private Integer admireAcount;
	/**
	 * 评论人数
	 */
	@ApiModelProperty(value = "评论人数")
	private Integer commentAcount;
	/**
	 * 阅读人数
	 */
	@ApiModelProperty(value = "阅读人数")
	private Integer readAcount;
	/**
	 * 是否公开博客(1公开，0不公开)
	 */
	@ApiModelProperty(value = "是否公开博客(1公开，0不公开)")
	private Integer isOpen;
	/**
	 * 是否已经发布(1发布，0没发布)
	 */
	@ApiModelProperty(value = "是否已经发布(1发布，0没发布)")
	private Integer isIssue;

}
