package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBloggerFansListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "用户编号")
	private Long userNo;
	/**
	 * 用户头像
	 */
	@ApiModelProperty(value = "用户头像")
	private String userImg;
	/**
	 * 用户昵称
	 */
	@ApiModelProperty(value = "用户昵称")
	private String nickname;
}
