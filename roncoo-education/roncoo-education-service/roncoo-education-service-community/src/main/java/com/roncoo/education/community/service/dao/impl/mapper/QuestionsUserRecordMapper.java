package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuestionsUserRecordMapper {
    int countByExample(QuestionsUserRecordExample example);

    int deleteByExample(QuestionsUserRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(QuestionsUserRecord record);

    int insertSelective(QuestionsUserRecord record);

    List<QuestionsUserRecord> selectByExample(QuestionsUserRecordExample example);

    QuestionsUserRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") QuestionsUserRecord record, @Param("example") QuestionsUserRecordExample example);

    int updateByExample(@Param("record") QuestionsUserRecord record, @Param("example") QuestionsUserRecordExample example);

    int updateByPrimaryKeySelective(QuestionsUserRecord record);

    int updateByPrimaryKey(QuestionsUserRecord record);
}
