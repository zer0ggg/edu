package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "id")
    private Long id;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;
    /**
     * 用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "用户编号")
    private Long userNo;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;
    /**
     * 问题标签
     */
    @ApiModelProperty(value = "问题标签")
    private String tagsName;
    /**
     * 问题关键词
     */
    @ApiModelProperty(value = "问题关键词")
    private String keywords;
    /**
     * 问题内容
     */
    @ApiModelProperty(value = "问题内容")
    private String content;
    /**
     * 收藏人数
     */
    @ApiModelProperty(value = "收藏人数")
    private Integer collectionAcount;
    /**
     * 点赞人数
     */
    @ApiModelProperty(value = "点赞人数")
    private Integer admireAcount;
    /**
     * 评论人数
     */
    @ApiModelProperty(value = "评论人数")
    private Integer commentAcount;
    /**
     * 阅读人数
     */
    @ApiModelProperty(value = "阅读人数")
    private Integer readAcount;
}
