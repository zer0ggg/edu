package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.BloggerUserRecordMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BloggerUserRecordDaoImpl implements BloggerUserRecordDao {
	@Autowired
	private BloggerUserRecordMapper bloggerUserRecordMapper;

	@Override
    public int save(BloggerUserRecord record) {
		record.setId(IdWorker.getId());
		return this.bloggerUserRecordMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.bloggerUserRecordMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(BloggerUserRecord record) {
		return this.bloggerUserRecordMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(BloggerUserRecord record, BloggerUserRecordExample example) {
		return this.bloggerUserRecordMapper.updateByExampleSelective(record, example);
	}

	@Override
    public BloggerUserRecord getById(Long id) {
		return this.bloggerUserRecordMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<BloggerUserRecord> listForPage(int pageCurrent, int pageSize, BloggerUserRecordExample example) {
		int count = this.bloggerUserRecordMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<BloggerUserRecord>(count, totalPage, pageCurrent, pageSize, this.bloggerUserRecordMapper.selectByExample(example));
	}

	@Override
	public List<BloggerUserRecord> listByUserNoAndStatusId(Long userNo, Integer statusId) {
		BloggerUserRecordExample example = new BloggerUserRecordExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		c.andStatusIdEqualTo(statusId);
		return this.bloggerUserRecordMapper.selectByExample(example);
	}

	@Override
	public BloggerUserRecord getByUserNoAndBloggerUserNo(Long userNo, Long bloggerUserNo) {
		BloggerUserRecordExample example = new BloggerUserRecordExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(userNo);
		c.andBloggerUserNoEqualTo(bloggerUserNo);
		List<BloggerUserRecord> list = this.bloggerUserRecordMapper.selectByExample(example);
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

}
