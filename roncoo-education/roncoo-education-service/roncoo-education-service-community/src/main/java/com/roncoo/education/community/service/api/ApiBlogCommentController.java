package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.BlogCommentPageBO;
import com.roncoo.education.community.common.dto.BlogCommentPageDTO;
import com.roncoo.education.community.service.api.biz.ApiBlogCommentBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客评论表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/blog/comment")
public class ApiBlogCommentController {

	@Autowired
	private ApiBlogCommentBiz biz;

	/**
	 * 博客评论信息展示接口
	 *
	 * @param bloggerViewBO
	 */
	@ApiOperation(value = "博客评论信息展示接口", notes = "博客评论信息展示接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<BlogCommentPageDTO>> list(@RequestBody BlogCommentPageBO blogCommentPageBO) {
		return biz.list(blogCommentPageBO);
	}

}
