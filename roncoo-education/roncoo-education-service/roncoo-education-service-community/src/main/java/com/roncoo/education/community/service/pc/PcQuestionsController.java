package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.QuestionsEditREQ;
import com.roncoo.education.community.common.req.QuestionsListREQ;
import com.roncoo.education.community.common.req.QuestionsSaveREQ;
import com.roncoo.education.community.common.req.QuestionsStatusUpdateREQ;
import com.roncoo.education.community.common.resp.QuestionsListRESP;
import com.roncoo.education.community.common.resp.QuestionsViewRESP;
import com.roncoo.education.community.service.pc.biz.PcQuestionsBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 问答信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/questions")
@Api(value = "community-问答信息", tags = {"community-问答信息"})
public class PcQuestionsController {

    @Autowired
    private PcQuestionsBiz biz;

    @ApiOperation(value = "问答信息列表", notes = "问答信息列表")
    @PostMapping(value = "/list")
    public Result<Page<QuestionsListRESP>> list(@RequestBody QuestionsListREQ questionsListREQ) {
        return biz.list(questionsListREQ);
    }


    @ApiOperation(value = "问答信息添加", notes = "问答信息添加")
    @SysLog(value = "问答信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody QuestionsSaveREQ questionsSaveREQ) {
        return biz.save(questionsSaveREQ);
    }

    @ApiOperation(value = "问答信息查看", notes = "问答信息查看")
    @GetMapping(value = "/view")
    public Result<QuestionsViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "问答信息修改", notes = "问答信息修改")
    @SysLog(value = "问答信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody QuestionsEditREQ questionsEditREQ) {
        return biz.edit(questionsEditREQ);
    }


    @ApiOperation(value = "问答信息删除", notes = "问答信息删除")
    @SysLog(value = "问答信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "状态更新", notes = "状态更新")
    @SysLog(value = "问答信息状态更新")
    @PutMapping(value = "/updateStatusId")
    public Result<String> updateStatusId(@RequestBody QuestionsStatusUpdateREQ req) {
        return biz.updateStatusId(req);
    }


    @ApiOperation(value = "问答信息添加Es", notes = "问答信息添加Es")
    @SysLog(value = "问答信息添加Es")
    @PostMapping(value = "/add/es")
    public Result<String> addEs() {
        return biz.addEs();
    }

}
