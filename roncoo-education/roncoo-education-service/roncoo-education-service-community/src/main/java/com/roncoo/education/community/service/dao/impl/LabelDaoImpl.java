package com.roncoo.education.community.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.LabelDao;
import com.roncoo.education.community.service.dao.impl.mapper.LabelMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Label;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LabelDaoImpl implements LabelDao {
	@Autowired
	private LabelMapper labelMapper;

	@Override
    public int save(Label record) {
		record.setId(IdWorker.getId());
		return this.labelMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.labelMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(Label record) {
		return this.labelMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(Label record, LabelExample example) {
		return this.labelMapper.updateByExampleSelective(record, example);
	}

	@Override
    public Label getById(Long id) {
		return this.labelMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<Label> listForPage(int pageCurrent, int pageSize, LabelExample example) {
		int count = this.labelMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<Label>(count, totalPage, pageCurrent, pageSize, this.labelMapper.selectByExample(example));
	}

	@Override
	public List<Label> listByAllAndLabelType(Integer labelType) {
		LabelExample example = new LabelExample();
		Criteria criteria = example.createCriteria();
		criteria.andLabelTypeEqualTo(labelType);
		return this.labelMapper.selectByExample(example);
	}

	@Override
	public Label getByLabelTypeAndLabelName(Integer labelType, String labelName) {
		LabelExample example = new LabelExample();
		Criteria criteria = example.createCriteria();
		criteria.andLabelTypeEqualTo(labelType);
		criteria.andLabelNameEqualTo(labelName);
		List<Label> list = this.labelMapper.selectByExample(example);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<Label> listByAllAndLabelTypeAndStatusId(Integer labelType, Integer statusId) {
		LabelExample example = new LabelExample();
		Criteria criteria = example.createCriteria();
		criteria.andLabelTypeEqualTo(labelType);
		criteria.andStatusIdEqualTo(statusId);
		example.setOrderByClause("sort asc, id desc ");
		return this.labelMapper.selectByExample(example);
	}
}
