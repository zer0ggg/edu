package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsIssueEnum;
import com.roncoo.education.common.core.enums.IsOpenEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.common.bo.BloggerViewBO;
import com.roncoo.education.community.common.dto.BloggerViewDTO;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 博主信息表
 *
 * @author wujing
 */
@Component
public class ApiBloggerBiz extends BaseBiz {

	@Autowired
	private BloggerDao dao;
	@Autowired
	private BlogDao blogDao;

	@Autowired
	private IFeignUserExt feignUserExt;

	public Result<BloggerViewDTO> view(BloggerViewBO bo) {
		if (bo.getBloggerUserNo() == null) {
			return Result.error("bloggerUserNo不能为空!");
		}
		Blogger blogger = dao.getByBloggerUserNo(bo.getBloggerUserNo());
		if (ObjectUtil.isNull(blogger)) {
			return Result.error("博主信息不存在!");
		}
		UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getBloggerUserNo());
		if (ObjectUtil.isNull(userExtVO)) {
			return Result.error("博主用户信息不存在!");
		}

		BloggerViewDTO dto = BeanUtil.copyProperties(blogger, BloggerViewDTO.class);
		List<Blog> blogList = blogDao.listByUserNoAndIsOpenAndIsIssue(bo.getBloggerUserNo(), IsOpenEnum.YES.getCode(), IsIssueEnum.YES.getCode());
		dto.setWeblogAccount(blogList.size());
		dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
		if (StringUtils.isEmpty(userExtVO.getNickname())) {
			dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
		} else {
			dto.setBloggerNickname(userExtVO.getNickname());
		}
		return Result.success(dto);
	}

}
