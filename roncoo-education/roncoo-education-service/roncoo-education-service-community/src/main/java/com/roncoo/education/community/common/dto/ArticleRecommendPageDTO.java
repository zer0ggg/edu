package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleRecommendPageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 文章id
	 */
	@ApiModelProperty(value = "文章id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long artcleId;
	/**
	 * 文章标题
	 */
	@ApiModelProperty(value = "文章id")
	private String title;
	/**
	 * 文章类型(1:博客;2:资讯)
	 */
	@ApiModelProperty(value = "文章id")
	private Integer articleType;

	/**
	 * 阅读人数
	 */
	@ApiModelProperty(value = "阅读人数")
	private Integer readAcount;
}
