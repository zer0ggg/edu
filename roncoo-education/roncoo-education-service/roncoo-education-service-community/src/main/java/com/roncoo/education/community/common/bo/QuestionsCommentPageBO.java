package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsCommentPageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 问题ID
     */
    @ApiModelProperty(value = "问题ID")
    private Long questionsId;
    /**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
}
