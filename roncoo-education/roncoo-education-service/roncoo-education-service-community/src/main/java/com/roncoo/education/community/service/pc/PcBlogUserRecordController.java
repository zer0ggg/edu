package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.BlogUserRecordEditREQ;
import com.roncoo.education.community.common.req.BlogUserRecordListREQ;
import com.roncoo.education.community.common.req.BlogUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.BlogUserRecordListRESP;
import com.roncoo.education.community.common.resp.BlogUserRecordViewRESP;
import com.roncoo.education.community.service.pc.biz.PcBlogUserRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 博客与用户关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/blog/user/record")
@Api(value = "community-博客与用户关联", tags = {"community-博客与用户关联"})
public class PcBlogUserRecordController {

    @Autowired
    private PcBlogUserRecordBiz biz;

    @ApiOperation(value = "博客与用户关联列表", notes = "博客与用户关联列表")
    @PostMapping(value = "/list")
    public Result<Page<BlogUserRecordListRESP>> list(@RequestBody BlogUserRecordListREQ blogUserRecordListREQ) {
        return biz.list(blogUserRecordListREQ);
    }


    @ApiOperation(value = "博客与用户关联添加", notes = "博客与用户关联添加")
    @SysLog(value = "博客与用户关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody BlogUserRecordSaveREQ blogUserRecordSaveREQ) {
        return biz.save(blogUserRecordSaveREQ);
    }

    @ApiOperation(value = "博客与用户关联查看", notes = "博客与用户关联查看")
    @GetMapping(value = "/view")
    public Result<BlogUserRecordViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "博客与用户关联修改", notes = "博客与用户关联修改")
    @SysLog(value = "博客与用户关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody BlogUserRecordEditREQ blogUserRecordEditREQ) {
        return biz.edit(blogUserRecordEditREQ);
    }


    @ApiOperation(value = "博客与用户关联删除", notes = "博客与用户关联删除")
    @SysLog(value = "博客与用户关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
