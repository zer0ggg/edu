package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.QuestionsUserRecordEditREQ;
import com.roncoo.education.community.common.req.QuestionsUserRecordListREQ;
import com.roncoo.education.community.common.req.QuestionsUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.QuestionsUserRecordListRESP;
import com.roncoo.education.community.common.resp.QuestionsUserRecordViewRESP;
import com.roncoo.education.community.service.pc.biz.PcQuestionsUserRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 问题与用户关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/questions/user/record")
@Api(value = "community-问题与用户关联", tags = {"community-问题与用户关联"})
public class PcQuestionsUserRecordController {

    @Autowired
    private PcQuestionsUserRecordBiz biz;

    @ApiOperation(value = "问题与用户关联列表", notes = "问题与用户关联列表")
    @PostMapping(value = "/list")
    public Result<Page<QuestionsUserRecordListRESP>> list(@RequestBody QuestionsUserRecordListREQ questionsUserRecordListREQ) {
        return biz.list(questionsUserRecordListREQ);
    }


    @ApiOperation(value = "问题与用户关联添加", notes = "问题与用户关联添加")
    @SysLog(value = "问题与用户关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody QuestionsUserRecordSaveREQ questionsUserRecordSaveREQ) {
        return biz.save(questionsUserRecordSaveREQ);
    }

    @ApiOperation(value = "问题与用户关联查看", notes = "问题与用户关联查看")
    @GetMapping(value = "/view")
    public Result<QuestionsUserRecordViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "问题与用户关联修改", notes = "问题与用户关联修改")
    @SysLog(value = "问题与用户关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody QuestionsUserRecordEditREQ questionsUserRecordEditREQ) {
        return biz.edit(questionsUserRecordEditREQ);
    }


    @ApiOperation(value = "问题与用户关联删除", notes = "问题与用户关联删除")
    @SysLog(value = "问题与用户关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
