package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 博客评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BlogCommentStatusDelREQ", description = "博客评论状态删除")
public class BlogCommentStatusDelREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

}
