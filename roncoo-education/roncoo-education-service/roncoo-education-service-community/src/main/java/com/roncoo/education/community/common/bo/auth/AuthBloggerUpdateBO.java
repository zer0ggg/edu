package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 更新博主信息
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUpdateBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 博主简介
     */
    @ApiModelProperty(value = "博主简介")
    private String introduction;
}
