package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.OpTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserCollectionPageBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserRefDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserRefSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserCollectionPageDTO;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BlogUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 博客与用户关联表
 *
 * @author wujing
 */
@Component
public class AuthBlogUserRecordBiz extends BaseBiz {

    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private BlogUserRecordDao blogUserRecordDao;
    @Autowired
    private BlogDao blogDao;

    public Result<Page<AuthBlogUserCollectionPageDTO>> collectionList(AuthBlogUserCollectionPageBO bo) {
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        BlogUserRecordExample example = new BlogUserRecordExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        c.andOpTypeEqualTo(bo.getOpType());
        if (bo.getArticleType() != null) {
            c.andArticleTypeEqualTo(bo.getArticleType());
        }
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        Page<BlogUserRecord> page = blogUserRecordDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBlogUserCollectionPageDTO> dtoPage = PageUtil.transform(page, AuthBlogUserCollectionPageDTO.class);
        for (AuthBlogUserCollectionPageDTO dto : dtoPage.getList()) {
            Blog blog = blogDao.getById(dto.getBlogId());
            dto.setTitle(blog.getTitle());
        }
        return Result.success(dtoPage);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthBlogUserRefSaveBO bo) {
        if (bo.getWeblogId() == null) {
            return Result.error("weblogId不能为空");
        }
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        if (StringUtils.isEmpty(bo.getUserIp())) {
            return Result.error("userIp不能为空");
        }
        if (StringUtils.isEmpty(bo.getUserTerminal())) {
            return Result.error("userTerminal不能为空");
        }
        // 校验博客是否存在
        Blog blog = blogDao.getById(bo.getWeblogId());
        if (ObjectUtils.isEmpty(blog)) {
            return Result.error("博客ID不正确");
        }

        // 校验用户信息是否存在
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("找不到用户信息");
        }

        // 查询用户是否收藏、点赞该博客
        BlogUserRecord blogUserRecord = blogUserRecordDao.getByUserNoAndBlogIdAndOpType(ThreadContext.userNo(), blog.getId(), bo.getOpType());
        // 查询不为空时
        if (!ObjectUtils.isEmpty(blogUserRecord)) {
            if (OpTypeEnum.COLLECTION.getCode().equals(blogUserRecord.getOpType())) {
                return Result.error("已经收藏过该博客");
            } else if (OpTypeEnum.PRAISE.getCode().equals(blogUserRecord.getOpType())) {
                return Result.error("已经点赞过该博客");
            }
        }

        // 添加用户博客关联表信息
        BlogUserRecord record = BeanUtil.copyProperties(blog, BlogUserRecord.class);
        record.setBlogId(blog.getId());
        record.setOpType(bo.getOpType());
        record.setUserNo(ThreadContext.userNo());
        record.setUserIp(bo.getUserIp());
        record.setUserTerminal(bo.getUserTerminal());
        record.setArticleType(blog.getArticleType());
        int resultNum = blogUserRecordDao.save(record);

        // 更新博客信息
        if (resultNum > 0) {
            // 添加收藏时候
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                // 收藏人数+1
                blog.setCollectionAcount(blog.getCollectionAcount() + 1);
                // 质量度+1
                blog.setQualityScore(blog.getQualityScore() + 1);
                blogDao.updateById(blog);
            }
            // 添加点赞时候
            else if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                // 点赞人数+1
                blog.setAdmireAcount(blog.getAdmireAcount() + 1);
                // 质量度+10
                blog.setQualityScore(blog.getQualityScore() + 10);
                blogDao.updateById(blog);
            }
            return Result.success(resultNum);
        }

        return Result.error("操作失败");
    }

    public Result<Integer> delete(AuthBlogUserRefDeleteBO bo) {
        if (bo.getOpType() == null) {
            return Result.error("opType不能为空");
        }
        if (bo.getWeblogId() == null) {
            return Result.error("weblogId不能为空");
        }
        // 校验博客是否存在
        Blog blog = blogDao.getById(bo.getWeblogId());
        if (ObjectUtils.isEmpty(blog)) {
            return Result.error("weblogId不正确");
        }

        // 查询用户是否收藏、点赞该博客
        BlogUserRecord blogUserRecord = blogUserRecordDao.getByUserNoAndBlogIdAndOpType(ThreadContext.userNo(), blog.getId(), bo.getOpType());
        // 查询为空时
        if (ObjectUtils.isEmpty(blogUserRecord)) {
            // 收藏时候
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                return Result.error("用户没有收藏过该博客");
            }
            // 点赞时候
            else if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                return Result.error("用户没有点赞过该博客");
            }
        } else {
            // 收藏时候
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                if (!ThreadContext.userNo().equals(blogUserRecord.getUserNo())) {
                    return Result.error("不能取消其他博主的收藏！");
                }
            }
            // 点赞时候
            else if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                if (!ThreadContext.userNo().equals(blogUserRecord.getUserNo())) {
                    return Result.error("不能取消其他博主的点赞！");
                }
            }
        }

        int resultNum = blogUserRecordDao.deleteById(blogUserRecord.getId());

        // 更新博客信息
        if (resultNum > 0) {
            // 取消收藏时候
            if (OpTypeEnum.COLLECTION.getCode().equals(bo.getOpType())) {
                if (blog.getCollectionAcount() <= 0) {
                    logger.error("更新收藏人数失败，收藏人数为{}", blog.getCollectionAcount());
                    return Result.error("更新收藏人数失败，收藏人数为" + blog.getCollectionAcount());
                }
                blog.setCollectionAcount(blog.getCollectionAcount() - 1);// 收藏人数-1
                blog.setQualityScore(blog.getQualityScore() - 1);// 质量度-1
                blogDao.updateById(blog);
            }
            // 取消点赞时候
            else if (OpTypeEnum.PRAISE.getCode().equals(bo.getOpType())) {
                if (blog.getAdmireAcount() <= 0) {
                    logger.error("更新点赞人数失败，点赞人数为{}", blog.getAdmireAcount());
                    return Result.error("更新点赞人数失败，点赞人数为" + blog.getAdmireAcount());
                }
                blog.setAdmireAcount(blog.getAdmireAcount() - 1);// 点赞人数-1
                blog.setQualityScore(blog.getQualityScore() - 10);// 质量度-10
                blogDao.updateById(blog);
            }
            return Result.success(resultNum);
        }

        return Result.error("操作失败");
    }
}
