package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneRefListBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 文章专区ID
	 */
	@ApiModelProperty(value = "文章专区ID", required = true)
	private Long articleZoneId;
}
