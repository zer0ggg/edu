package com.roncoo.education.community.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsCommentBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 问题ID
     */
    private Long questionsId;
    /**
     * 问题标题
     */
    private String title;
    /**
     * 评论者用户编号
     */
    private Long userNo;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 用户IP
     */
    private String userIp;
    /**
     * 用户终端
     */
    private String userTerminal;
}
