package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogUserCollectionPageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 操作类型(1收藏，2点赞)
     */
    @ApiModelProperty(value = "操作类型(1收藏，2点赞)")
    private Integer opType;

    /**
     * 文章类型(1:博客;2:资讯)
     */
    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)")
    private Integer articleType;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize = 20;
}
