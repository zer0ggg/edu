package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.ArticleZoneCategoryQO;
import com.roncoo.education.community.feign.vo.ArticleZoneCategoryVO;
import com.roncoo.education.community.service.dao.ArticleZoneCategoryDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@Component
public class FeignArticleZoneCategoryBiz {

	@Autowired
	private ArticleZoneCategoryDao dao;

	public Page<ArticleZoneCategoryVO> listForPage(ArticleZoneCategoryQO qo) {
		ArticleZoneCategoryExample example = new ArticleZoneCategoryExample();
		Criteria c = example.createCriteria();
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		}
		if (StringUtils.isNotBlank(qo.getArticleZoneName())) {
			c.andArticleZoneNameLike(PageUtil.like(qo.getArticleZoneName()));
		}
		if (qo.getPlatShow() != null) {
			c.andPlatShowEqualTo(qo.getPlatShow());
		}

		example.setOrderByClause("status_id desc,sort asc, id desc ");
		Page<ArticleZoneCategory> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ArticleZoneCategoryVO.class);
	}

	public int save(ArticleZoneCategoryQO qo) {
		if (StringUtils.isEmpty(qo.getArticleZoneName())) {
			throw new BaseException("文章专区名称不能为空");
		}
		ArticleZoneCategory articleZoneCategory = dao.getByArticleZoneName(qo.getArticleZoneName());
		if (ObjectUtil.isNotNull(articleZoneCategory)) {
			throw new BaseException("已存在文章专区");
		}
		ArticleZoneCategory record = BeanUtil.copyProperties(qo, ArticleZoneCategory.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ArticleZoneCategoryVO getById(Long id) {
		ArticleZoneCategory record = dao.getById(id);
		return BeanUtil.copyProperties(record, ArticleZoneCategoryVO.class);
	}

	public int updateById(ArticleZoneCategoryQO qo) {
		ArticleZoneCategory record = BeanUtil.copyProperties(qo, ArticleZoneCategory.class);
		return dao.updateById(record);
	}

}
