package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客评论表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogCommentUserSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private Long parentId;
    /**
     * 博客ID
     */
    @ApiModelProperty(value = "博客ID")
    private Long weblogId;
    /**
     * 被评论者用户编号
     */
    @ApiModelProperty(value = "被评论者用户编号")
    private Long bloggerUserNo;
    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容")
    private String content;
    /**
     * 用户IP
     */
    @ApiModelProperty(value = "用户IP")
    private String userIp;
    /**
     * 用户终端
     */
    @ApiModelProperty(value = "用户终端")
    private String userTerminal;
}
