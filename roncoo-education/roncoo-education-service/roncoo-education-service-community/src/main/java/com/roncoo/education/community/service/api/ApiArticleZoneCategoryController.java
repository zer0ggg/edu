package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.ArticleZoneCategoryPageBO;
import com.roncoo.education.community.common.dto.ArticleZoneCategoryListDTO;
import com.roncoo.education.community.service.api.biz.ApiArticleZoneCategoryBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/article/zone/category")
public class ApiArticleZoneCategoryController {

	@Autowired
	private ApiArticleZoneCategoryBiz biz;

	/**
	 * 资讯专区列表接口
	 *
	 * @param articleZoneCategoryPageBO
	 */
	@ApiOperation(value = "资讯专区列表接口", notes = "资讯专区列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<ArticleZoneCategoryListDTO> list(@RequestBody ArticleZoneCategoryPageBO articleZoneCategoryPageBO) {
		return biz.list(articleZoneCategoryPageBO);
	}

}
