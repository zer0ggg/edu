package com.roncoo.education.community.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文章推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ArticleRecommendListRESP", description = "文章推荐列表")
public class ArticleRecommendListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "文章id")
    private Long artcleId;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)")
    private Integer articleType;

    @ApiModelProperty(value = "文章标题")
    private String title;

    @ApiModelProperty(value = "作者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "作者用户昵称")
    private String bloggerNickname;
}
