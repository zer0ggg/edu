package com.roncoo.education.community.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.community.common.bo.LabelListBO;
import com.roncoo.education.community.common.dto.LabelDTO;
import com.roncoo.education.community.common.dto.LabelListDTO;
import com.roncoo.education.community.service.dao.LabelDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 标签
 *
 * @author wujing
 */
@Component
public class ApiLabelBiz extends BaseBiz {

	@Autowired
	private LabelDao dao;

	public Result<LabelListDTO> list(LabelListBO bo) {
		LabelListDTO dto = new LabelListDTO();
		List<Label> list = dao.listByAllAndLabelTypeAndStatusId(bo.getLabelType(), StatusIdEnum.YES.getCode());
		dto.setList(ArrayListUtil.copy(list, LabelDTO.class));
		return Result.success(dto);
	}
}
