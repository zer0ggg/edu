package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class BlogCommentPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客ID
	 */
	@ApiModelProperty(value = "博客ID", required = true)
	private Long blogId;
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
}
