package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 文章专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneRefListREQ", description="文章专区关联列表")
public class ArticleZoneRefListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "专区ID不能为空")
    @ApiModelProperty(value = "文章专区ID",required = true)
    private Long articleZoneId;

    @NotNull(message = "显示平台不能为空")
    @ApiModelProperty(value = "显示平台(1:PC端显示;2:微信端展示)",required = true)
    private Integer platShow;

    @ApiModelProperty(value = "标题")
    private String title;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
