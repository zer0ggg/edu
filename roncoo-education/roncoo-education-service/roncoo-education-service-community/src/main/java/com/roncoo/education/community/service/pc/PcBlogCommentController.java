package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.BlogCommentEditREQ;
import com.roncoo.education.community.common.req.BlogCommentListREQ;
import com.roncoo.education.community.common.req.BlogCommentSaveREQ;
import com.roncoo.education.community.common.req.BlogCommentStatusDelREQ;
import com.roncoo.education.community.common.resp.BlogCommentListRESP;
import com.roncoo.education.community.common.resp.BlogCommentViewRESP;
import com.roncoo.education.community.service.pc.biz.PcBlogCommentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 博客评论 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/blog/comment")
@Api(value = "community-博客评论", tags = {"community-博客评论"})
public class PcBlogCommentController {

    @Autowired
    private PcBlogCommentBiz biz;

    @ApiOperation(value = "博客评论列表", notes = "博客评论列表")
    @PostMapping(value = "/list")
    public Result<Page<BlogCommentListRESP>> list(@RequestBody BlogCommentListREQ blogCommentListREQ) {
        return biz.list(blogCommentListREQ);
    }


    @ApiOperation(value = "博客评论添加", notes = "博客评论添加")
    @SysLog(value = "博客评论添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody BlogCommentSaveREQ blogCommentSaveREQ) {
        return biz.save(blogCommentSaveREQ);
    }

    @ApiOperation(value = "博客评论查看", notes = "博客评论查看")
    @GetMapping(value = "/view")
    public Result<BlogCommentViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "博客评论修改", notes = "博客评论修改")
    @SysLog(value = "博客评论修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody BlogCommentEditREQ blogCommentEditREQ) {
        return biz.edit(blogCommentEditREQ);
    }


    @ApiOperation(value = "博客评论状态删除", notes = "博客评论状态删除")
    @SysLog(value = "博客评论状态删除")
    @PostMapping(value = "/del")
    public Result<String> statusDel(@RequestBody BlogCommentStatusDelREQ blogCommentStatusDelREQ) {
        return biz.statusDel(blogCommentStatusDelREQ);
    }


    @ApiOperation(value = "博客评论删除", notes = "博客评论删除")
    @SysLog(value = "博客评论删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
