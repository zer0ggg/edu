package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogUserSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 博客标题
     */
    @ApiModelProperty(value = "博客标题", required = true)
    private String title;
    /**
     * 文章类型(1:博客;2:资讯)
     */
    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
    private Integer articleType = 1;
    /**
     * 博客标签
     */
    @ApiModelProperty(value = "博客标签", required = true)
    private String tagsName;
    /**
     * 博客内容
     */
    @ApiModelProperty(value = "博客内容", required = true)
    private String content;

    @ApiModelProperty(value = "MD内容", required = true)
    private String mdContet;

    @ApiModelProperty(value = "是否使用MD(1:使用；2:不使用)", required = true)
    private Integer isMd;
    /**
     * 博客关键词
     */
    @ApiModelProperty(value = "博客关键词", required = false)
    private String keywords;
    /**
     * 是否公开博客(1公开，0不公开)
     */
    @ApiModelProperty(value = "是否公开博客(1公开，0不公开)", required = true)
    private Integer isOpen;
    /**
     * 是否已经发布(1发布，0没发布)
     */
    @ApiModelProperty(value = "是否已经发布(1发布，0没发布)", required = true)
    private Integer isIssue;
    /**
     * 博客类型(1原创，2转载)
     */
    @ApiModelProperty(value = "博客类型(1原创，2转载)", required = true)
    private Integer typeId;
}
