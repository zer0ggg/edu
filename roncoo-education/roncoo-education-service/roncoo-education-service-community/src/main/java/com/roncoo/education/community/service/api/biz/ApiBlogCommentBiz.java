package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.common.bo.BlogCommentPageBO;
import com.roncoo.education.community.common.dto.BlogCommentPageDTO;
import com.roncoo.education.community.service.dao.BlogCommentDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 博客评论表
 *
 * @author wujing
 */
@Component
public class ApiBlogCommentBiz {
	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BlogCommentDao dao;

	public Result<Page<BlogCommentPageDTO>> list(BlogCommentPageBO bo) {
		if (bo.getBlogId() == null) {
			return Result.error("博客id不能为空");
		}

		BlogCommentExample example = new BlogCommentExample();
		Criteria c = example.createCriteria();
		c.andParentIdEqualTo(0L);
		c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
		c.andBlogIdEqualTo(bo.getBlogId());
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<BlogComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		Page<BlogCommentPageDTO> dto = PageUtil.transform(page, BlogCommentPageDTO.class);
		for (BlogCommentPageDTO blogComment : dto.getList()) {

			BlogComment comment = dao.getById(blogComment.getId());
			if (ObjectUtil.isNotNull(comment)) {
				if (StringUtils.hasText(comment.getContent())) {
					blogComment.setContent(comment.getContent());
				}
			}
			// 被评论者用户
			UserExtVO bloggerUser = feignUserExt.getByUserNo(blogComment.getBloggerUserNo());
			if (ObjectUtil.isNotNull(bloggerUser)) {
				if(StringUtils.isEmpty(blogComment.getBloggerNickname())){
					if (StringUtils.isEmpty(bloggerUser.getNickname())) {
						blogComment.setBloggerNickname(bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7));
					} else {
						blogComment.setBloggerNickname(bloggerUser.getNickname());
					}
				}
				if (StringUtils.hasText(bloggerUser.getHeadImgUrl())) {
					blogComment.setBloggerUserImg(bloggerUser.getHeadImgUrl());
				}
			}
			// 评论者用户
			UserExtVO user = feignUserExt.getByUserNo(blogComment.getUserNo());
			if (ObjectUtil.isNotNull(user)) {
				if(StringUtils.isEmpty(blogComment.getNickname())){
					if (StringUtils.isEmpty(user.getNickname())) {
						blogComment.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
					} else {
						blogComment.setNickname(user.getNickname());
					}
				}
				if (StringUtils.hasText(user.getHeadImgUrl())) {
					blogComment.setUserImg(user.getHeadImgUrl());
				}
			}
			blogComment.setBlogCommentList(recursionList(blogComment.getId()));
		}
		return Result.success(dto);
	}

	/**
	 * 展示第二级
	 *
	 */
	private List<BlogCommentPageDTO> recursionList(Long parentId) {
		List<BlogCommentPageDTO> list = new ArrayList<>();
		List<BlogComment> weblogCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
		if (CollectionUtils.isNotEmpty(weblogCommentList)) {
			for (BlogComment weblogComment : weblogCommentList) {
				BlogCommentPageDTO vo = BeanUtil.copyProperties(weblogComment, BlogCommentPageDTO.class);
				BlogComment comment = dao.getById(vo.getId());
				if (ObjectUtil.isNotNull(comment)) {
					vo.setContent(comment.getContent());
				}
				// 被评论者用户
				UserExtVO bloggerUser = feignUserExt.getByUserNo(vo.getBloggerUserNo());
				if (ObjectUtil.isNotNull(bloggerUser)) {
					if(StringUtils.isEmpty(weblogComment.getBloggerNickname())){
						if (StringUtils.isEmpty(bloggerUser.getNickname())) {
							vo.setBloggerNickname(bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7));
						} else {
							vo.setBloggerNickname(bloggerUser.getNickname());
						}
					}
					if (StringUtils.hasText(bloggerUser.getHeadImgUrl())) {
						vo.setBloggerUserImg(bloggerUser.getHeadImgUrl());
					}
				}
				// 评论者用户
				UserExtVO user = feignUserExt.getByUserNo(vo.getUserNo());
				if (ObjectUtil.isNotNull(user)) {
					if(StringUtils.isEmpty(weblogComment.getNickname())){
						if (StringUtils.isEmpty(user.getNickname())) {
							vo.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
						} else {
							vo.setNickname(user.getNickname());
						}
					}
					if (StringUtils.hasText(user.getHeadImgUrl())) {
						vo.setUserImg(user.getHeadImgUrl());
					}
				}
				list.add(vo);
			}
		}
		return list;
	}

}
