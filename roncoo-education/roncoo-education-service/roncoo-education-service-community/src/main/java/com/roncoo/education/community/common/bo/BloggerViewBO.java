package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class BloggerViewBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博主用户编号
	 */
	@ApiModelProperty(value = "博主用户编号")
	private Long bloggerUserNo;
}
