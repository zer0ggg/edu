package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserAttentionUserSaveBO;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserRecordFansListBO;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserRecordListBO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerFansListDTO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserRecordListDTO;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@Component
public class AuthBloggerUserRecordBiz extends BaseBiz {

    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private BloggerUserRecordDao dao;

    @Autowired
    private IFeignUserExt feignUserExt;

    public Result<Integer> save(AuthBloggerUserAttentionUserSaveBO bo) {
        if (bo.getBloggerUserNo() == null) {
            return Result.error("bloggerUserNo不能为空");
        }
        if (StringUtils.isEmpty(bo.getUserTerminal())) {
            return Result.error("userTerminal不能为空");
        }

        // 博主信息
        Blogger blogger = bloggerDao.getByBloggerUserNo(bo.getBloggerUserNo());
        if (ObjectUtils.isEmpty(blogger)) {
            return Result.error("需要关注的博主信息不存在");
        }
        // 博主用户信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getBloggerUserNo());
        if (ObjectUtils.isEmpty(userExtVO)) {
            return Result.error("博主用户教育信息不存在");
        }

        // 关注者
        Blogger followers = bloggerDao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtils.isEmpty(followers)) {
            return Result.error("博主信息不存在");
        }
        // 关注者用户信息
        UserExtVO vo = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtils.isEmpty(vo)) {
            return Result.error("关注者用户教育信息不存在");
        }

        // 查询用户有没有关注博主
        BloggerUserRecord bloggerUserRecord = dao.getByUserNoAndBloggerUserNo(ThreadContext.userNo(), bo.getBloggerUserNo());
        // 没有关注，添加关注
        if (ObjectUtils.isEmpty(bloggerUserRecord)) {
            BloggerUserRecord record = new BloggerUserRecord();
            record.setBloggerUserNo(bo.getBloggerUserNo());
            record.setUserNo(ThreadContext.userNo());
            record.setUserIp(bo.getUserIp());
            record.setUserTerminal(bo.getUserTerminal());

            int resultNum = dao.save(record);
            if (resultNum > 0) {
                // 更新需要关注的博主信息粉丝人数
                blogger.setFansAccount(blogger.getFansAccount() + 1);
                bloggerDao.updateById(blogger);
                // 更新关注者的博主信息关注人数
                followers.setAttentionAcount(followers.getAttentionAcount() + 1);
                bloggerDao.updateById(followers);
            }
            return Result.success(resultNum);
        }

        // 已关注，取消关注
        // 删除博主用户关联表信息
        int resultNum = dao.deleteById(bloggerUserRecord.getId());
        if (resultNum > 0) {
            // 更新需要关注的博主信息粉丝人数
            if (blogger.getFansAccount() <= 0) {
                logger.error("更新粉丝人数失败，粉丝人数为{}", blogger.getFansAccount());
                return Result.error("更新粉丝人数失败，粉丝人数为" + blogger.getFansAccount());
            }
            blogger.setFansAccount(blogger.getFansAccount() - 1);
            bloggerDao.updateById(blogger);

            // 更新关注者的博主信息关注人数
            if (followers.getAttentionAcount() <= 0) {
                logger.error("更新关注人数失败，关注人数为{}", followers.getAttentionAcount());
                return Result.error("更新关注人数失败，关注人数为" + followers.getAttentionAcount());
            }
            followers.setAttentionAcount(followers.getAttentionAcount() - 1);
            bloggerDao.updateById(followers);
        }
        return Result.success(resultNum);
    }

    public Result<Page<AuthBloggerUserRecordListDTO>> list(AuthBloggerUserRecordListBO bo) {
        BloggerUserRecordExample example = new BloggerUserRecordExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        example.setOrderByClause(" id desc");
        Page<BloggerUserRecord> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBloggerUserRecordListDTO> dtoPage = PageUtil.transform(page, AuthBloggerUserRecordListDTO.class);
        for (AuthBloggerUserRecordListDTO dto : dtoPage.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getBloggerUserNo());
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
            } else {
                dto.setBloggerNickname(userExtVO.getNickname());
            }
            dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
        }
        return Result.success(dtoPage);
    }

    public Result<Page<AuthBloggerFansListDTO>> fansList(AuthBloggerUserRecordFansListBO bo) {
        BloggerUserRecordExample example = new BloggerUserRecordExample();
        Criteria c = example.createCriteria();
        c.andBloggerUserNoEqualTo(ThreadContext.userNo());
        example.setOrderByClause(" id desc");
        Page<BloggerUserRecord> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBloggerFansListDTO> dtoPage = PageUtil.transform(page, AuthBloggerFansListDTO.class);
        for (AuthBloggerFansListDTO dto : dtoPage.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    dto.setNickname(userExtVO.getNickname());
                }
                dto.setUserImg(userExtVO.getHeadImgUrl());
            }
        }
        return Result.success(dtoPage);
    }

}
