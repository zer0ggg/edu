package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标签
 *
 */
@Data
@Accessors(chain = true)
public class EsDeleteBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 标签类型（1:博客标签; 2:问答标签; 3:资讯标签）
	 */
	@ApiModelProperty(value = "标签类型（1:博客标签; 2:问答标签; 3:资讯标签）")
	private String indexName;

}
