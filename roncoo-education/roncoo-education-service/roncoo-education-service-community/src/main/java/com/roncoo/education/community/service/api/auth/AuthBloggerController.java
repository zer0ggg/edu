package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserViewDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthBloggerBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博主信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/blogger")
public class AuthBloggerController {

	@Autowired
	private AuthBloggerBiz biz;

	/**
	 * 推荐博主列表展示接口
	 *
	 * @param bloggerUserPageBO
	 * @return
	 */
	@ApiOperation(value = "推荐博主列表展示接口", notes = "根据博客的发表数量由高到低列出博主信息")
	@RequestMapping(value = "/recommend/list", method = RequestMethod.POST)
	public Result<Page<AuthBloggerUserPageDTO>> list(@RequestBody AuthBloggerUserPageBO authBloggerUserPageBO) {
		return biz.recommendList(authBloggerUserPageBO);
	}

	/**
	 * 博主信息查看接口(用户查看博主使用)
	 *
	 * @param bloggerUserViewBO
	 * @return
	 */
	@ApiOperation(value = "获取博主信息接口(用户查看博主使用)", notes = "获取博主信息(用户查看博主使用)")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthBloggerUserViewDTO> view(@RequestBody AuthBloggerUserViewBO authBloggerUserViewBO) {
		return biz.view(authBloggerUserViewBO);
	}

	/**
	 * 个人中心博主信息查看接口
	 *
	 * @param bloggerUserMyselfViewBO
	 * @return
	 */
	@ApiOperation(value = "个人中心博主信息查看接口", notes = "博主自己查看自己的博主信息")
	@RequestMapping(value = "/myself", method = RequestMethod.POST)
	public Result<AuthBloggerUserViewDTO> myself(@RequestBody AuthBloggerUserMyselfViewBO authBloggerUserMyselfViewBO) {
		return biz.myself(authBloggerUserMyselfViewBO);
	}

	/**
	 * 新增博主信息接口
	 *
	 * @param bloggerUserSaveBO
	 * @return
	 */
	@ApiOperation(value = "新增博主信息接口", notes = "新增博主信息")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthBloggerUserSaveBO authBloggerUserSaveBO) {
		return biz.save(authBloggerUserSaveBO);
	}

	/**
	 * 更新博主信息接口
	 *
	 * @param authBloggerUpdateBO
	 * @return
	 */
	@ApiOperation(value = "更新博主信息接口", notes = "更新博主信息接口")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<Integer> update(@RequestBody AuthBloggerUpdateBO authBloggerUpdateBO) {
		return biz.update(authBloggerUpdateBO);
	}
}
