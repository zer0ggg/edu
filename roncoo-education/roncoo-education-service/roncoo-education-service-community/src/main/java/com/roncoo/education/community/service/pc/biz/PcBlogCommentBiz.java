package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.common.req.BlogCommentEditREQ;
import com.roncoo.education.community.common.req.BlogCommentListREQ;
import com.roncoo.education.community.common.req.BlogCommentSaveREQ;
import com.roncoo.education.community.common.req.BlogCommentStatusDelREQ;
import com.roncoo.education.community.common.resp.BlogCommentListRESP;
import com.roncoo.education.community.common.resp.BlogCommentViewRESP;
import com.roncoo.education.community.service.dao.BlogCommentDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 博客评论
 *
 * @author wujing
 */
@Component
public class PcBlogCommentBiz extends BaseBiz {

    @Autowired
    private BlogCommentDao dao;
    @Autowired
    private BlogDao blogDao;
    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 博客评论列表
     *
     * @param req 博客评论分页查询参数
     * @return 博客评论分页查询结果
     */
    public Result<Page<BlogCommentListRESP>> list(BlogCommentListREQ req) {
        BlogCommentExample example = new BlogCommentExample();
        Criteria c = example.createCriteria();
        if (req.getBlogId() != null) {
            c.andBlogIdEqualTo(req.getBlogId());
        }
        if (req.getParentId() != null) {
            c.andParentIdEqualTo(req.getParentId());
        }
        if(req.getStatusId() != null){
            c.andStatusIdEqualTo(req.getStatusId());
        }else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        example.setOrderByClause("sort asc, id desc ");
        Page<BlogComment> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<BlogCommentListRESP> respPage = PageUtil.transform(page, BlogCommentListRESP.class);
        for (BlogCommentListRESP blogCommentListRESP : respPage.getList()) {
            // 评论用户
            if(StringUtils.isEmpty(blogCommentListRESP.getNickname())){
                UserExtVO userExtVO = feignUserExt.getByUserNo(blogCommentListRESP.getUserNo());
                if (ObjectUtil.isNotNull(userExtVO)) {
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        blogCommentListRESP.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        blogCommentListRESP.setNickname(userExtVO.getNickname());
                    }
                }
            }

            // 被评论用户
            if(StringUtils.isEmpty(blogCommentListRESP.getBloggerNickname())){
                UserExtVO bloggerUser = feignUserExt.getByUserNo(blogCommentListRESP.getBloggerUserNo());
                if (ObjectUtil.isNotNull(bloggerUser)) {
                    if (StringUtils.isEmpty(bloggerUser.getNickname())) {
                        blogCommentListRESP.setBloggerNickname(bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7));
                    } else {
                        blogCommentListRESP.setBloggerNickname(bloggerUser.getNickname());
                    }
                }
            }

            // 获取大字段博客内容content
            String content = getBigFieldById(blogCommentListRESP.getId());
            blogCommentListRESP.setContent(content);
            blogCommentListRESP.setBlogCommentList(recursionList(blogCommentListRESP.getId()));
        }
        return Result.success(respPage);
    }


    /**
     * 博客评论添加(管理人员刷评论)
     *
     * @param req 博客评论
     * @return 添加结果
     */
    public Result<String> save(BlogCommentSaveREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }

        // 根据博客ID查询博客信息
        Blog blog = blogDao.getById(req.getBlogId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("博客信息不存在");
        }
        //被评论者信息
        Long bloggerUserNo;
        String bloggerNickname;
        if(req.getParentId() != null && req.getParentId() != 0){
            // 评论某一条评论
            BlogComment pBlogComment = dao.getById(req.getParentId());
            if(ObjectUtils.isEmpty(pBlogComment)){
                return Result.error("父id错误");
            }
            bloggerUserNo = pBlogComment.getUserNo();
            bloggerNickname = pBlogComment.getNickname();
        }else {
            // 直接评论博主
            bloggerUserNo = blog.getUserNo();
            UserExtVO bloggerUser = feignUserExt.getByUserNo(bloggerUserNo);
            bloggerNickname = bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7);

        }
        // 添加评论
        BlogComment blogComment = new BlogComment();
        blogComment.setParentId(req.getParentId());
        blogComment.setBlogId(req.getBlogId());
        blogComment.setTitle(blog.getTitle());
        blogComment.setBloggerUserNo(bloggerUserNo);
        blogComment.setUserNo(req.getUserNo());
        blogComment.setNickname(req.getNickname());
        blogComment.setBloggerNickname(bloggerNickname);
        blogComment.setContent(req.getContent());
        // 默认值
//        blogComment.setUserIp(req.getUserIp());
        blogComment.setUserTerminal(req.getUserTerminal());
        blogComment.setArticleType(blog.getArticleType());
        int resultNum = dao.save(blogComment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (req.getParentId() == 0) {
                // 评论人数+1
                blog.setCommentAcount(blog.getCommentAcount() + 1);
                // 质量度+5
                blog.setQualityScore(blog.getQualityScore() + 5);
                blogDao.updateById(blog);
            }
        }
        if (resultNum > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 博客评论查看
     *
     * @param id 主键ID
     * @return 博客评论
     */
    public Result<BlogCommentViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), BlogCommentViewRESP.class));
    }


    /**
     * 博客评论修改
     *
     * @param req 博客评论修改对象
     * @return 修改结果
     */
    public Result<String> edit(BlogCommentEditREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        BlogComment record = BeanUtil.copyProperties(req, BlogComment.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 状态删除
     *
     * @param blogCommentStatusDelREQ 状态删除参数
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> statusDel(BlogCommentStatusDelREQ blogCommentStatusDelREQ) {
        // 校验博客评论是否存在
        BlogComment weblogComment = dao.getById(blogCommentStatusDelREQ.getId());
        if (ObjectUtils.isEmpty(weblogComment)) {
            throw new BaseException("找不到博客评论的信息");
        }
        // 查询博客信息
        Blog blog = blogDao.getById(weblogComment.getBlogId());
        if (ObjectUtils.isEmpty(blog)) {
            throw new BaseException("博客信息不存在");
        }

        weblogComment.setStatusId(Constants.FREEZE);
        int resultNum = dao.updateById(weblogComment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (blogCommentStatusDelREQ.getParentId() == 0) {
                if (blog.getCommentAcount() <= 0) {
                    throw new BaseException("更新评论人数失败，评论人数为" + blog.getCommentAcount());
                }
                // 评论人数-1
                blog.setCommentAcount(blog.getCommentAcount() - 1);
                // 质量度-5
                blog.setQualityScore(blog.getQualityScore() - 5);
                blogDao.updateById(blog);

                // 删除该一级评论下的所有二级评论
                List<BlogComment> secondWeblogComment = dao.listByParentIdAndStatusId(blogCommentStatusDelREQ.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isEmpty(secondWeblogComment)) {
                    return Result.success("删除成功");
                }
                for (BlogComment secondComment : secondWeblogComment) {
                    secondComment.setStatusId(StatusIdEnum.NO.getCode());
                    dao.updateById(secondComment);
                }
            }
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 博客评论删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 递归展示分类
     *
     * @author wuyun
     */
    private List<BlogCommentListRESP> recursionList(Long parentId) {
        List<BlogCommentListRESP> list = new ArrayList<>();
        List<BlogComment> blogCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(blogCommentList)) {
            for (BlogComment blogComment : blogCommentList) {
                BlogCommentListRESP resp = BeanUtil.copyProperties(blogComment, BlogCommentListRESP.class);
                // 获取大字段博客内容content
                String content = getBigFieldById(resp.getId());
                resp.setContent(content);
                resp.setBlogCommentList(recursionList(blogComment.getId()));
                list.add(resp);
            }
        }
        return list;
    }

    /**
     * 获取大字段博客内容content
     *
     * @param id 博客ID
     * @return 博客内容
     */
    private String getBigFieldById(Long id) {
        // 获取大字段博客内容content
        BlogComment blogComment = dao.getById(id);
        // 截取评论摘要
        String content = blogComment.getContent();
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        return content;
    }
}
