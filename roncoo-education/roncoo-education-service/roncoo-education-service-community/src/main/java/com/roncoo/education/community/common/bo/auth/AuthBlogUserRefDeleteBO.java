package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客与用户关联表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogUserRefDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户操作类型(1收藏，2点赞)
     */
    @ApiModelProperty(value = "用户操作类型(1收藏，2点赞)")
    private Integer opType;
    /**
     * 博客ID
     */
    @ApiModelProperty(value = "博客ID")
    private Long weblogId;
}
