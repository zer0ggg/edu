package com.roncoo.education.community.common.dto.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 标签
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthLabelDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 标签编号
     */
    private Long labelNo;
    /**
     * 标签名称
     */
    private String labelName;
    /**
     * 标签类型（1:博客标签）
     */
    private Integer labelType;

}
