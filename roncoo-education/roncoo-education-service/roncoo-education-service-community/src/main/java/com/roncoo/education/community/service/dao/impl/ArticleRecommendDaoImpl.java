package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.ArticleRecommendDao;
import com.roncoo.education.community.service.dao.impl.mapper.ArticleRecommendMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ArticleRecommendDaoImpl implements ArticleRecommendDao {
	@Autowired
	private ArticleRecommendMapper articleRecommendMapper;

	@Override
    public int save(ArticleRecommend record) {
		record.setId(IdWorker.getId());
		return this.articleRecommendMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.articleRecommendMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(ArticleRecommend record) {
		return this.articleRecommendMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(ArticleRecommend record, ArticleRecommendExample example) {
		return this.articleRecommendMapper.updateByExampleSelective(record, example);
	}

	@Override
    public ArticleRecommend getById(Long id) {
		return this.articleRecommendMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<ArticleRecommend> listForPage(int pageCurrent, int pageSize, ArticleRecommendExample example) {
		int count = this.articleRecommendMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<ArticleRecommend>(count, totalPage, pageCurrent, pageSize, this.articleRecommendMapper.selectByExample(example));
	}

	@Override
	public ArticleRecommend getByArtcleId(Long artcleId) {
		ArticleRecommendExample example = new ArticleRecommendExample();
		Criteria criteria = example.createCriteria();
		criteria.andArtcleIdEqualTo(artcleId);
		List<ArticleRecommend> list = this.articleRecommendMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}
}
