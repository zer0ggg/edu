package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 博客信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="BlogREQ", description="博客信息")
public class BlogREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "是否存在封面(1,存在；0,不存在)")
    private Integer isImg;

    @ApiModelProperty(value = "博客封面")
    private String blogImg;

    @ApiModelProperty(value = "博客标题")
    private String title;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)")
    private Integer articleType;

    @ApiModelProperty(value = "博客摘要")
    private String summary;

    @ApiModelProperty(value = "博客标签")
    private String tagsName;

    @ApiModelProperty(value = "博客内容")
    private String content;

    @ApiModelProperty(value = "质量度")
    private Integer qualityScore;

    @ApiModelProperty(value = "博客关键词")
    private String keywords;

    @ApiModelProperty(value = "编辑模式（1可视编辑，2markdown）")
    private Integer editMode;

    @ApiModelProperty(value = "是否公开博客(1公开，0不公开)")
    private Integer isOpen;

    @ApiModelProperty(value = "是否已经发布(1发布，0没发布)")
    private Integer isIssue;

    @ApiModelProperty(value = "博客类型(1原创，2转载)")
    private Integer typeId;

    @ApiModelProperty(value = "是否置顶(1置顶，0否)")
    private Integer isTop;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionAcount;

    @ApiModelProperty(value = "点赞人数")
    private Integer admireAcount;

    @ApiModelProperty(value = "评论人数")
    private Integer commentAcount;

    @ApiModelProperty(value = "阅读人数")
    private Integer readAcount;
}
