package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;

import java.util.List;

public interface QuestionsDao {
	int save(Questions record);

	int deleteById(Long id);

	int updateById(Questions record);

	int updateByExampleSelective(Questions record, QuestionsExample example);

	Questions getById(Long id);

	Page<Questions> listForPage(int pageCurrent, int pageSize, QuestionsExample example);

	/**
	 * 根据用户编号获取问答信息
	 *
	 * @param userNo
	 * @return
	 */
	List<Questions> listByUserNo(Long userNo);

	/**
	 * 根据状态获取集合
	 * @param code
	 * @return
	 */
    List<Questions> listByStatusId(Integer code);
}
