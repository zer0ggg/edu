package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 标签
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "LabelUpdateStatusREQ", description = "标签修改状态")
public class LabelUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)", required = true)
    private Integer statusId;

}
