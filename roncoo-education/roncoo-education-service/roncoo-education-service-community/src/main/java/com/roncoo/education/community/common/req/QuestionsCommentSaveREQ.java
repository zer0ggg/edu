package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 问答评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="QuestionsCommentSaveREQ", description="问答评论添加")
public class QuestionsCommentSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotNull(message = "父ID不能为空")
    @ApiModelProperty(value = "父ID",required = true)
    private Long parentId;

    @NotNull(message = "问题ID不能为空")
    @ApiModelProperty(value = "问题ID",required = true)
    private Long questionsId;

    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @NotEmpty(message = "评论内容不能为空")
    @ApiModelProperty(value = "评论内容",required = true)
    private String content;

    @ApiModelProperty(value = "用户IP")
    private String userIp;

    @NotEmpty(message = "评论者终端不能为空")
    @ApiModelProperty(value = "用户终端",required = true)
    private String userTerminal;

    @NotEmpty(message = "评论者昵称不能为空")
    @ApiModelProperty(value = "评论者昵称",required = true)
    private String nickname;
}
