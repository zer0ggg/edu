package com.roncoo.education.community.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 用户编号
     */
    private Long userNo;
    /**
     * 问题标题
     */
    private String title;
    /**
     * 问题标签
     */
    private String tagsName;
    /**
     * 问题关键词
     */
    private String keywords;
    /**
     * 问题内容
     */
    private String content;
    /**
     * 问题摘要
     */
    private String summary;
    /**
     * 质量度
     */
    private Integer qualityScore;
    /**
     * 收藏人数
     */
    private Integer collectionAcount;
    /**
     * 点赞人数
     */
    private Integer admireAcount;
    /**
     * 评论人数
     */
    private Integer commentAcount;
    /**
     * 阅读人数
     */
    private Integer readAcount;
    /**
     * 是否置顶(1置顶，0否)
     */
    private Integer isTop;
    /**
     * 是否悬赏积分(1悬赏，0不悬赏)
     */
    private Integer isReward;
    /**
     * 悬赏积分
     */
    private Integer rewardIntegral;
    /**
     * 是否关联课程(1关联，0不关联)
     */
    private Integer isCourse;
    /**
     * 课程ID
     */
    private Long courseId;
    /**
     * 是否存在最佳回答(1有，0无)
     */
    private Integer isGood;
    /**
     * 最佳回答ID
     */
    private Long goodCommentId;
    /**
     * 最佳回答用户编号
     */
    private Long goodUserNo;
    /**
     * 最佳回答内容
     */
    private String goodContent;
}
