package com.roncoo.education.community.common.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答信息表
 *
 * @author wuyun
 */
@Document(indexName = EsQuestions.QUESTIONS)
public class EsQuestions implements Serializable {

	public static final String QUESTIONS = "edu_questions";

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 用户头像
	 */
	private String headImgUrl;
	/**
	 * 用户昵称
	 */
	private String nickname;
	/**
	 * 问答标题
	 */
	private String title;
	/**
	 * 问答标签
	 */
	private String tagsName;
	/**
	 * 问答关键词
	 */
	private String keywords;
	/**
	 * 问答内容
	 */
	private String content;
	/**
	 * 是否置顶(1置顶，0否)
	 */
	private Integer isTop;
	/**
	 * 是否悬赏积分(1悬赏，0不悬赏)
	 */
	private Integer isReward;
	/**
	 * 是否关联课程(1关联，0不关联)
	 */
	private Integer isCourse;
	/**
	 * 课程id
	 */
	private Long courseId;
	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 质量度
	 */
	private Integer qualityScore;
	/**
	 * 收藏人数
	 */
	private Integer collectionAcount;
	/**
	 * 点赞人数
	 */
	private Integer admireAcount;
	/**
	 * 评论人数
	 */
	private Integer commentAcount;
	/**
	 * 阅读人数
	 */
	private Integer readAcount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getBloggerUserNo() {
		return userNo;
	}

	public void setUserNo(Long userNo) {
		this.userNo = userNo;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTagsName() {
		return tagsName;
	}

	public void setTagsName(String tagsName) {
		this.tagsName = tagsName;
	}

	public Integer getQualityScore() {
		return qualityScore;
	}

	public void setQualityScore(Integer qualityScore) {
		this.qualityScore = qualityScore;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Integer getIsTop() {
		return isTop;
	}

	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}

	public Integer getCollectionAcount() {
		return collectionAcount;
	}

	public void setCollectionAcount(Integer collectionAcount) {
		this.collectionAcount = collectionAcount;
	}

	public Integer getAdmireAcount() {
		return admireAcount;
	}

	public void setAdmireAcount(Integer admireAcount) {
		this.admireAcount = admireAcount;
	}

	public Integer getCommentAcount() {
		return commentAcount;
	}

	public void setCommentAcount(Integer commentAcount) {
		this.commentAcount = commentAcount;
	}

	public Integer getReadAcount() {
		return readAcount;
	}

	public void setReadAcount(Integer readAcount) {
		this.readAcount = readAcount;
	}

	public Integer getIsReward() {
		return isReward;
	}

	public void setIsReward(Integer isReward) {
		this.isReward = isReward;
	}

	public Integer getIsCourse() {
		return isCourse;
	}

	public void setIsCourse(Integer isCourse) {
		this.isCourse = isCourse;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "EsQuestions [id=" + id + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + ", sort=" + sort + ", userNo=" + userNo + ", headImgUrl=" + headImgUrl + ", nickname=" + nickname + ", title=" + title + ", tagsName=" + tagsName + ", keywords=" + keywords + ", content=" + content + ", isTop=" + isTop + ", isReward=" + isReward + ", isCourse=" + isCourse + ", courseId=" + courseId + ", courseName=" + courseName + ", qualityScore=" + qualityScore + ", collectionAcount=" + collectionAcount + ", admireAcount=" + admireAcount + ", commentAcount=" + commentAcount + ", readAcount=" + readAcount + "]";
	}

}
