package com.roncoo.education.community.service.feign.biz;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.feign.qo.BloggerUserRecordQO;
import com.roncoo.education.community.feign.vo.BloggerUserRecordVO;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@Component
public class FeignBloggerUserRecordBiz {

	@Autowired
	private BloggerUserRecordDao dao;

	public Page<BloggerUserRecordVO> listForPage(BloggerUserRecordQO qo) {
		BloggerUserRecordExample example = new BloggerUserRecordExample();
		Criteria c = example.createCriteria();
		if (qo.getUserNo() != null) {
			c.andUserNoEqualTo(qo.getUserNo());
		}
		if (qo.getOpType() != null) {
			c.andOpTypeEqualTo(qo.getOpType());
		}
		if (StringUtils.hasText(qo.getBeginGmtCreate())) {
			c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
		}
		if (StringUtils.hasText(qo.getEndGmtCreate())) {
			c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
		}
		example.setOrderByClause(" id desc ");
		Page<BloggerUserRecord> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, BloggerUserRecordVO.class);
	}

	public int save(BloggerUserRecordQO qo) {
		BloggerUserRecord record = BeanUtil.copyProperties(qo, BloggerUserRecord.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public BloggerUserRecordVO getById(Long id) {
		BloggerUserRecord record = dao.getById(id);
		return BeanUtil.copyProperties(record, BloggerUserRecordVO.class);
	}

	public int updateById(BloggerUserRecordQO qo) {
		BloggerUserRecord record = BeanUtil.copyProperties(qo, BloggerUserRecord.class);
		return dao.updateById(record);
	}

	public BloggerUserRecordVO getByUserNoAndBloggerUserNo(BloggerUserRecordQO qo) {
		BloggerUserRecord record = dao.getByUserNoAndBloggerUserNo(qo.getUserNo(), qo.getBloggerUserNo());
		return BeanUtil.copyProperties(record, BloggerUserRecordVO.class);
	}

}
