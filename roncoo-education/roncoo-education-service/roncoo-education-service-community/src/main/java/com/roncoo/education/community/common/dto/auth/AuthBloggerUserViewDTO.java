package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUserViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "主键")
	private Long id;
	/**
	 * 博主用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博主用户编号")
	private Long bloggerUserNo;
	/**
	 * 博主用户头像
	 */
	@ApiModelProperty(value = "博主用户头像")
	private String bloggerUserImg;
	/**
	 * 博主用户昵称
	 */
	@ApiModelProperty(value = "博主用户昵称")
	private String bloggerNickname;
	/**
	 * 博主简介
	 */
	@ApiModelProperty(value = "博主简介")
	private String introduction;
	/**
	 * 粉丝人数
	 */
	@ApiModelProperty(value = "粉丝人数")
	private Integer fansAccount;
	/**
	 * 关注人数
	 */
	@ApiModelProperty(value = "关注人数")
	private Integer attentionAcount;
	/**
	 * 博客数量
	 */
	@ApiModelProperty(value = "博客数量")
	private Integer weblogAccount;
	/**
	 * 问答数量
	 */
	@ApiModelProperty(value = "问答数量")
	private Integer questionsAccount;
	/**
	 * 我的回答数量
	 */
	@ApiModelProperty(value = "我的回答数量")
	private Integer answerAccount;
	/**
	 * 是否关注(1、关注，0、未关注)
	 */
	@ApiModelProperty(value = "是否关注(1、关注，0、未关注)")
	private Integer isAttention;
	/**
	 * 是否点赞(1、点赞，0、未点赞)
	 */
	@ApiModelProperty(value = "是否点赞(1、点赞，0、未点赞)")
	private Integer isPraise;
}
