package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客评论表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogCommentUserPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 被评论者用户编号
	 */
	@ApiModelProperty(value = "被评论者用户编号")
	private Long bloggerUserNo;
	/**
	 * 评论者用户编号
	 */
	@ApiModelProperty(value = "评论者用户编号")
	private Long commentUserNo;
	/**
	 * 文章类型(1:博客;2:资讯)
	 */
	@ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
	private Integer articleType;
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
}
