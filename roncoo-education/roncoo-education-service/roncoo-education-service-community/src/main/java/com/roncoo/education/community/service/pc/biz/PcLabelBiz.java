package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.common.req.LabelEditREQ;
import com.roncoo.education.community.common.req.LabelListREQ;
import com.roncoo.education.community.common.req.LabelSaveREQ;
import com.roncoo.education.community.common.req.LabelUpdateStatusREQ;
import com.roncoo.education.community.common.resp.LabelListRESP;
import com.roncoo.education.community.common.resp.LabelViewRESP;
import com.roncoo.education.community.service.dao.LabelDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Label;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 标签
 *
 * @author wujing
 */
@Component
public class PcLabelBiz extends BaseBiz {

    @Autowired
    private LabelDao dao;

    /**
     * 标签列表
     *
     * @param labelListREQ 标签分页查询参数
     * @return 标签分页查询结果
     */
    public Result<Page<LabelListRESP>> list(LabelListREQ labelListREQ) {
        LabelExample example = new LabelExample();
        Criteria c = example.createCriteria();
        if (StringUtils.isNotBlank(labelListREQ.getLabelName())) {
            c.andLabelNameLike(PageUtil.like(labelListREQ.getLabelName()));
        }
        if (labelListREQ.getLabelType() != null) {
            c.andLabelTypeEqualTo(labelListREQ.getLabelType());
        }
        if (labelListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(labelListREQ.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        example.setOrderByClause("sort asc, id desc ");
        Page<Label> page = dao.listForPage(labelListREQ.getPageCurrent(), labelListREQ.getPageSize(), example);
        Page<LabelListRESP> respPage = PageUtil.transform(page, LabelListRESP.class);
        return Result.success(respPage);
    }

    public Result<List<String>> listName(LabelListREQ req) {
        List<String> nameList = new ArrayList<>();
        LabelExample example = new LabelExample();
        Criteria c = example.createCriteria();
        if (req.getLabelType() != null) {
            c.andLabelTypeEqualTo(req.getLabelType());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        Page<Label> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        for(Label label:page.getList()){
            nameList.add(label.getLabelName());
        }
        return Result.success(nameList);
    }


    /**
     * 标签添加
     *
     * @param labelSaveREQ 标签
     * @return 添加结果
     */
    public Result<String> save(LabelSaveREQ labelSaveREQ) {
        if (labelSaveREQ.getLabelType() == null) {
            return Result.error("标签类型不能为空");
        }
        Label courseLabel = dao.getByLabelTypeAndLabelName(labelSaveREQ.getLabelType(), labelSaveREQ.getLabelName());
        if (ObjectUtil.isNotNull(courseLabel)) {
            return Result.error("已存在该标签");
        }
        Label record = BeanUtil.copyProperties(labelSaveREQ, Label.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 标签查看
     *
     * @param id 主键ID
     * @return 标签
     */
    public Result<LabelViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), LabelViewRESP.class));
    }


    /**
     * 标签修改
     *
     * @param labelEditREQ 标签修改对象
     * @return 修改结果
     */
    public Result<String> edit(LabelEditREQ labelEditREQ) {
        Label record = BeanUtil.copyProperties(labelEditREQ, Label.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 标签修改
     *
     * @param labelUpdateStatusREQ 标签状态修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(LabelUpdateStatusREQ labelUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(labelUpdateStatusREQ.getStatusId()))) {
            return Result.error("状态不正确，不能修改");
        }
        Label record = BeanUtil.copyProperties(labelUpdateStatusREQ, Label.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 标签删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 根据标签类型列出标签
     *
     * @param labelType 标签类型
     * @return 标签集合
     */
    public Result<List<LabelListRESP>> listByAllAndLabelType(Integer labelType) {
        List<LabelListRESP> resultList = PageUtil.copyList(dao.listByAllAndLabelType(labelType), LabelListRESP.class);
        return Result.success(resultList);
    }
}
