package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.BlogCommentQO;
import com.roncoo.education.community.feign.vo.BlogCommentVO;
import com.roncoo.education.community.service.dao.BlogCommentDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 博客评论表
 *
 * @author wujing
 */
@Component
public class FeignBlogCommentBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BlogDao blogDao;
	@Autowired
	private BlogCommentDao dao;

	public Page<BlogCommentVO> listForPage(BlogCommentQO qo) {
		BlogCommentExample example = new BlogCommentExample();
		Criteria c = example.createCriteria();
		if (qo.getBlogId() != null) {
			c.andBlogIdEqualTo(qo.getBlogId());
		}
		if (qo.getParentId() != null) {
			c.andParentIdEqualTo(qo.getParentId());
		}
		example.setOrderByClause(" id desc ");
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<BlogComment> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<BlogCommentVO> list = PageUtil.transform(page, BlogCommentVO.class);
		for (BlogCommentVO blogCommentVO : list.getList()) {
			// 评论用户
			UserExtVO userExtVO = feignUserExt.getByUserNo(blogCommentVO.getUserNo());
			if (ObjectUtil.isNotNull(userExtVO)) {
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					blogCommentVO.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					blogCommentVO.setNickname(userExtVO.getNickname());
				}
			}

			// 被评论用户
			UserExtVO bloggerUser = feignUserExt.getByUserNo(blogCommentVO.getBloggerUserNo());
			if (ObjectUtil.isNotNull(bloggerUser)) {
				if (StringUtils.isEmpty(bloggerUser.getNickname())) {
					blogCommentVO.setBloggerNickname(bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7));
				} else {
					blogCommentVO.setBloggerNickname(bloggerUser.getNickname());
				}
			}

			// 获取大字段博客内容content
			String content = getBigField(blogCommentVO);
			blogCommentVO.setContent(content);
			blogCommentVO.setBlogCommentList(recursionList(blogCommentVO.getId()));
		}
		return list;
	}

	/**
	 * 递归展示分类
	 *
	 * @author wuyun
	 */
	private List<BlogCommentVO> recursionList(Long parentId) {
		List<BlogCommentVO> list = new ArrayList<>();
		List<BlogComment> blogCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
		if (CollectionUtils.isNotEmpty(blogCommentList)) {
			for (BlogComment blogComment : blogCommentList) {
				BlogCommentVO vo = BeanUtil.copyProperties(blogComment, BlogCommentVO.class);
				// 获取大字段博客内容content
				String content = getBigField(vo);
				vo.setContent(content);
				vo.setBlogCommentList(recursionList(blogComment.getId()));
				list.add(vo);
			}
		}
		return list;
	}

	public int save(BlogCommentQO qo) {
		BlogComment record = BeanUtil.copyProperties(qo, BlogComment.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public BlogCommentVO getById(Long id) {
		BlogComment record = dao.getById(id);
		return BeanUtil.copyProperties(record, BlogCommentVO.class);
	}

	@Transactional(rollbackFor = Exception.class)
	public int updateById(BlogCommentQO qo) {
		// 校验博客评论是否存在
		BlogComment weblogComment = dao.getById(qo.getId());
		if (ObjectUtils.isEmpty(weblogComment)) {
			throw new BaseException("找不到博客评论的信息");
		}
		// 查询博客信息
		Blog blog = blogDao.getById(weblogComment.getBlogId());
		if (ObjectUtils.isEmpty(blog)) {
			throw new BaseException("博客信息不存在");
		}

		weblogComment.setStatusId(StatusIdEnum.NO.getCode());
		int resultNum = dao.updateById(weblogComment);

		if (resultNum > 0) {
			// 只统计一级的评论，更新博客信息
			if (qo.getParentId() == 0) {
				if (blog.getCommentAcount() <= 0) {
					throw new BaseException("更新评论人数失败，评论人数为" + blog.getCommentAcount());
				}
				// 评论人数-1
				blog.setCommentAcount(blog.getCommentAcount() - 1);
				// 质量度-5
				blog.setQualityScore(blog.getQualityScore() - 5);
				blogDao.updateById(blog);

				// 删除该一级评论下的所有二级评论
				List<BlogComment> secondWeblogComment = dao.listByParentIdAndStatusId(qo.getId(), StatusIdEnum.YES.getCode());
				if (CollectionUtils.isEmpty(secondWeblogComment)) {
					return resultNum;
				}
				for (BlogComment secondComment : secondWeblogComment) {
					secondComment.setStatusId(StatusIdEnum.NO.getCode());
					dao.updateById(secondComment);
				}
			}
		}
		return resultNum;
	}

	/**
	 * 获取大字段博客内容content
	 * @param blogCommentVO
	 * @return
	 */
	private String getBigField(BlogCommentVO blogCommentVO) {
		// 获取大字段博客内容content
		BlogComment blogComment = dao.getById(blogCommentVO.getId());
		// 截取评论摘要
		String content = blogComment.getContent();
		// 过滤文章内容中的html
		content = content.replaceAll("</?[^<]+>", "");
		// 去除字符串中的空格 回车 换行符 制表符 等
		content = content.replaceAll("\\s*|\t|\r|\n", "");
		// 去除空格
		content = content.replaceAll("&nbsp;", "");
		return content;
	}

}
