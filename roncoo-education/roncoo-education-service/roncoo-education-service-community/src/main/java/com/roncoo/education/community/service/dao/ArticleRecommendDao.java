package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;

public interface ArticleRecommendDao {
	int save(ArticleRecommend record);

	int deleteById(Long id);

	int updateById(ArticleRecommend record);

	int updateByExampleSelective(ArticleRecommend record, ArticleRecommendExample example);

	ArticleRecommend getById(Long id);

	Page<ArticleRecommend> listForPage(int pageCurrent, int pageSize, ArticleRecommendExample example);

	/**
	 * 根据文章id获取文章推荐信息
	 * 
	 * @param artcleId
	 * @return
	 */
	ArticleRecommend getByArtcleId(Long artcleId);
}