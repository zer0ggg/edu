package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUserRecordListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博主用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博主用户编号")
	private Long bloggerUserNo;
	/**
	 * 博主用户头像
	 */
	@ApiModelProperty(value = "博主用户头像")
	private String bloggerUserImg;
	/**
	 * 博主用户昵称
	 */
	@ApiModelProperty(value = "博主用户昵称")
	private String bloggerNickname;
}
