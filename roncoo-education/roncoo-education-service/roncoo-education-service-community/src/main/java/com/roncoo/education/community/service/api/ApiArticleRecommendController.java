package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.ArticleRecommendPageBO;
import com.roncoo.education.community.common.dto.ArticleRecommendPageDTO;
import com.roncoo.education.community.service.api.biz.ApiArticleRecommendBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章推荐
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/article/recommend")
public class ApiArticleRecommendController {

	@Autowired
	private ApiArticleRecommendBiz biz;

	/**
	 * (博客、资讯)推荐列表接口
	 *
	 * @param articleRecommendBO
	 */
	@ApiOperation(value = "(博客、资讯)推荐列表接口", notes = "(博客、资讯)推荐列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<ArticleRecommendPageDTO>> list(@RequestBody ArticleRecommendPageBO articleRecommendBO) {
		return biz.list(articleRecommendBO);
	}

}
