package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.CommentTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsCommentDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsCommentListBO;
import com.roncoo.education.community.common.bo.auth.AuthQuestionsCommentSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsCommentListDTO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsCommentSaveDTO;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Component
public class AuthQuestionsCommentBiz {

    @Autowired
    private QuestionsCommentDao dao;
    @Autowired
    private QuestionsDao questionsDao;
    @Autowired
    private BloggerDao bloggerDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    public Result<AuthQuestionsCommentSaveDTO> save(AuthQuestionsCommentSaveBO bo) {
        if (bo.getQuestionsId() == null) {
            return Result.error("问题id不能为空");
        }
        if (StringUtils.isEmpty(bo.getContent())) {
            return Result.error("内容不能为空");
        }
        if (StringUtils.isEmpty(bo.getTitle())) {
            return Result.error("问题标题不能为空");
        }
        Questions questions = questionsDao.getById(bo.getQuestionsId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答信息");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("找不到用户信息");
        }
        Blogger blogger = bloggerDao.getByBloggerUserNo(userExtVO.getUserNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("找不到用户信息");
        }
        QuestionsComment questionsComment = BeanUtil.copyProperties(bo, QuestionsComment.class);
        questionsComment.setGmtModified(new Date());
        if (bo.getParentId() != null) {
            QuestionsComment comment = dao.getById(bo.getParentId());
            if (ObjectUtil.isNull(comment)) {
                return Result.error("找不到上级评论信息");
            }
            //回复评论的时候，被评论者为上级评论用户编号
            questionsComment.setCommentUserNo(comment.getUserNo());
        } else {
            //如果是直接评论问答，被评论者为问答用户编号
            questionsComment.setCommentUserNo(questions.getUserNo());
        }
        questionsComment.setUserNo(ThreadContext.userNo());
        int result = dao.save(questionsComment);
        if (result > 0) {
            //博主评论数量+1
            blogger.setAnswerAccount(blogger.getAnswerAccount() + 1);
            bloggerDao.updateById(blogger);
            //评论人数+1
            questions.setCommentAcount(questions.getCommentAcount() + 1);
            questionsDao.updateById(questions);
            //评论者用户信息
            AuthQuestionsCommentSaveDTO dto = BeanUtil.copyProperties(questionsComment, AuthQuestionsCommentSaveDTO.class);
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
            } else {
                dto.setNickname(userExtVO.getNickname());
            }
            dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
            //被评论者用户信息
            UserExtVO vo = feignUserExt.getByUserNo(questionsComment.getCommentUserNo());
            if (StringUtils.isEmpty(vo.getNickname())) {
                dto.setCommentUickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
            } else {
                dto.setCommentUickname(vo.getNickname());
            }
            dto.setCommentUserImg(vo.getHeadImgUrl());
            return Result.success(dto);
        }
        return Result.error("评论失败");
    }

    public Result<Integer> delete(AuthQuestionsCommentDeleteBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        QuestionsComment questionsComment = dao.getById(bo.getId());
        if (ObjectUtil.isNull(questionsComment)) {
            return Result.error("获取不到评论信息");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("获取不到用户信息");
        }
        if (!questionsComment.getUserNo().equals(userExtVO.getUserNo())) {
            return Result.error("不能删除别人的评论");
        }
        int result = dao.deleteById(bo.getId());
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error("删除失败");
    }

    public Result<Page<AuthQuestionsCommentListDTO>> list(AuthQuestionsCommentListBO bo) {
        QuestionsCommentExample example = new QuestionsCommentExample();
        Criteria c = example.createCriteria();
        if (CommentTypeEnum.MECOMMENT.getCode().equals(bo.getCommentType())) {
            c.andUserNoEqualTo(ThreadContext.userNo());
        }
        if (CommentTypeEnum.COMMENTME.getCode().equals(bo.getCommentType())) {
            c.andCommentUserNoEqualTo(ThreadContext.userNo());
        }
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause(" id desc");
        Page<QuestionsComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthQuestionsCommentListDTO> dtoPage = PageUtil.transform(page, AuthQuestionsCommentListDTO.class);
        for (AuthQuestionsCommentListDTO dto : dtoPage.getList()) {
            Questions questions = questionsDao.getById(dto.getQuestionsId());
            if (ObjectUtil.isNotNull(questions)) {
                dto.setTitle(questions.getTitle());
            }
            //评论者用户信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
            if (StringUtils.isEmpty(dto.getNickname())) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    dto.setNickname(userExtVO.getNickname());
                }
            }
            dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
            //被评论者用户信息
            UserExtVO vo = feignUserExt.getByUserNo(dto.getCommentUserNo());
            if (StringUtils.isEmpty(dto.getCommentUickname())) {
                if (StringUtils.isEmpty(vo.getNickname())) {
                    dto.setCommentUickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
                } else {
                    dto.setCommentUickname(vo.getNickname());
                }
            }
            dto.setCommentUserImg(vo.getHeadImgUrl());
            // 获取大字段博客内容content
            String content = getBigField(dto);
            dto.setContent(content);
        }
        return Result.success(dtoPage);
    }


    private String getBigField(AuthQuestionsCommentListDTO dto) {
        // 获取大字段博客内容content
        QuestionsComment questionsComment = dao.getById(dto.getId());
        // 截取评论摘要
        String content = questionsComment.getContent();
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        return content;
    }
}
