package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class QuestionsPageBO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 问题标题
	 */
	@ApiModelProperty(value = "问题标题", required = false)
	private String title;
	/**
	 * 问题标签
	 */
	@ApiModelProperty(value = "问题标签", required = false)
	private String tagsName;

	@ApiModelProperty(value = "问答(1最新，2热门，3推荐)", required = false)
	private Integer sections = Integer.valueOf(1);
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
}
