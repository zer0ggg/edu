package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlogComment;
import com.roncoo.education.community.feign.qo.BlogCommentQO;
import com.roncoo.education.community.feign.vo.BlogCommentVO;
import com.roncoo.education.community.service.feign.biz.FeignBlogCommentBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客评论表
 *
 * @author wujing
 */
@RestController
public class FeignBlogCommentController extends BaseController implements IFeignBlogComment{

	@Autowired
	private FeignBlogCommentBiz biz;

	@Override
	public Page<BlogCommentVO> listForPage(@RequestBody BlogCommentQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody BlogCommentQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@PathVariable(value = "id") Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody BlogCommentQO qo){
		return biz.updateById(qo);
	}

    @Override
	public BlogCommentVO getById(@PathVariable(value = "id") Long id){
		return biz.getById(id);
	}

}
