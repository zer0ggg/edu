package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignQuestionsComment;
import com.roncoo.education.community.feign.qo.QuestionsCommentQO;
import com.roncoo.education.community.feign.vo.QuestionsCommentVO;
import com.roncoo.education.community.feign.vo.QuestionsStatisticalVO;
import com.roncoo.education.community.service.feign.biz.FeignQuestionsCommentBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答评论表
 *
 * @author wujing
 */
@RestController
public class FeignQuestionsCommentController extends BaseController implements IFeignQuestionsComment {

	@Autowired
	private FeignQuestionsCommentBiz biz;

	@Override
	public Page<QuestionsCommentVO> listForPage(@RequestBody QuestionsCommentQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody QuestionsCommentQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody QuestionsCommentQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public QuestionsCommentVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public QuestionsStatisticalVO statisticalByUserNo(@PathVariable(value = "userNo") Long userNo) {
		return biz.statisticalByUserNo(userNo);
	}

}
