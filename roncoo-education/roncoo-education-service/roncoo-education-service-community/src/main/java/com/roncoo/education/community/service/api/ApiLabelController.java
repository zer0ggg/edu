package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.LabelListBO;
import com.roncoo.education.community.common.dto.LabelListDTO;
import com.roncoo.education.community.service.api.biz.ApiLabelBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 标签
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/label")
public class ApiLabelController {

	@Autowired
	private ApiLabelBiz biz;

	/**
	 * 标签列表接口
	 *
	 * @param bloggerViewBO
	 * @author wuyun
	 */
	@ApiOperation(value = "标签列表接口", notes = "标签列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<LabelListDTO> list(@RequestBody LabelListBO labelListBO) {
		return biz.list(labelListBO);
	}

}
