package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserPageBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogCommentUserSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthBlogCommentUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogCommentUserSaveDTO;
import com.roncoo.education.community.service.dao.BlogCommentDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 博客评论表
 *
 * @author wujing
 */
@Component
public class AuthBlogCommentBiz extends BaseBiz {
    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private BlogCommentDao dao;
    @Autowired
    private BlogDao blogDao;

    public Result<Page<AuthBlogCommentUserPageDTO>> list(AuthBlogCommentUserPageBO bo) {
        if (bo.getArticleType() == null) {
            return Result.error("ArticleType不能为空");
        }
        BlogCommentExample example = new BlogCommentExample();
        Criteria c = example.createCriteria();
        if (bo.getBloggerUserNo() != null) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getBloggerUserNo());
            if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("用户异常");
            }
            c.andBloggerUserNoEqualTo(bo.getBloggerUserNo());
        }
        if (bo.getCommentUserNo() != null) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getCommentUserNo());
            if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("用户异常");
            }
            c.andUserNoEqualTo(bo.getCommentUserNo());
        }
        c.andArticleTypeEqualTo(bo.getArticleType());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause(" status_id desc, sort asc, gmt_create desc, id desc ");
        Page<BlogComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBlogCommentUserPageDTO> pageDto = PageUtil.transform(page, AuthBlogCommentUserPageDTO.class);
        for (AuthBlogCommentUserPageDTO dto : pageDto.getList()) {
            BlogComment comment = dao.getById(dto.getId());
            if (ObjectUtil.isNotNull(comment)) {
                if (StringUtils.hasText(comment.getContent())) {
                    dto.setContent(comment.getContent());
                }
            }
            // 查找被评论者信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getBloggerUserNo());
            if (StringUtils.isEmpty(dto.getBloggerNickname())) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    dto.setBloggerNickname(userExtVO.getNickname());
                }
            }
            dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
            // 查找评论者信息
            UserExtVO vo = feignUserExt.getByUserNo(dto.getUserNo());
            if (StringUtils.isEmpty(vo.getNickname())) {
                dto.setNickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
            } else {
                dto.setNickname(vo.getNickname());
            }
            dto.setUserImg(vo.getHeadImgUrl());
        }
        return Result.success(pageDto);
    }

    public Result<AuthBlogCommentUserSaveDTO> save(AuthBlogCommentUserSaveBO bo) {
        if (bo.getBloggerUserNo() == null) {
            return Result.error("bloggerUserNo不能为空");
        }
        if (bo.getWeblogId() == null) {
            return Result.error("weblogId不能为空");
        }
        if (bo.getParentId() == null) {
            return Result.error("pId不能为空");
        }
        if (StringUtils.isEmpty(bo.getContent())) {
            return Result.error("content不能为空");
        }
        if (StringUtils.isEmpty(bo.getUserIp())) {
            return Result.error("userIp不能为空");
        }
        if (StringUtils.isEmpty(bo.getUserTerminal())) {
            return Result.error("userTerminal不能为空");
        }
        // 被评论者用户信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getBloggerUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("被评论者用户教育信息不存在");
        }
        // 评论者用户信息
        UserExtVO vo = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(vo)) {
            return Result.error("评论者用户教育信息不存在");
        }

        // 根据博客ID查询博客信息
        Blog blog = blogDao.getById(bo.getWeblogId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("博客信息不存在");
        }

        // 添加评论
        BlogComment blogComment = new BlogComment();
        blogComment.setParentId(bo.getParentId());
        blogComment.setBlogId(bo.getWeblogId());
        blogComment.setTitle(blog.getTitle());
        blogComment.setBloggerUserNo(bo.getBloggerUserNo());
        blogComment.setUserNo(ThreadContext.userNo());
        blogComment.setContent(bo.getContent());
        blogComment.setUserIp(bo.getUserIp());
        blogComment.setUserTerminal(bo.getUserTerminal());
        blogComment.setArticleType(blog.getArticleType());
        int resultNum = dao.save(blogComment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (bo.getParentId() == 0) {
                // 评论人数+1
                blog.setCommentAcount(blog.getCommentAcount() + 1);
                // 质量度+5
                blog.setQualityScore(blog.getQualityScore() + 5);
                blogDao.updateById(blog);
            }
        }
        AuthBlogCommentUserSaveDTO dto = BeanUtil.copyProperties(blogComment, AuthBlogCommentUserSaveDTO.class);
        // 查找被评论者信息
        dto.setBloggerNickname(userExtVO.getNickname());
        dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
        // 查找评论者信息
        dto.setNickname(vo.getNickname());
        dto.setUserImg(vo.getHeadImgUrl());
        return Result.success(dto);
    }

    public Result<Integer> delete(AuthBlogCommentUserDeleteBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        if (bo.getParentId() == null) {
            return Result.error("parentId不能为空");
        }
        if (bo.getWeblogId() == null) {
            return Result.error("weblogId不能为空");
        }
        // 查询评论信息
        BlogComment blogComment = dao.getById(bo.getId());
        if (ObjectUtil.isNull(blogComment)) {
            return Result.error("该评论不存在");
        }
        // 查询博客信息
        Blog blog = blogDao.getById(bo.getWeblogId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("博客信息不存在");
        }
        blogComment.setStatusId(StatusIdEnum.NO.getCode());
        int resultNum = dao.updateById(blogComment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (bo.getParentId() == 0) {
                if (blog.getCommentAcount() <= 0) {
                    logger.error("更新评论人数失败，评论人数为{}", blog.getCommentAcount());
                    return Result.error("更新评论人数失败，评论人数为" + blog.getCommentAcount());
                }
                // 评论人数-1
                blog.setCommentAcount(blog.getCommentAcount() - 1);
                // 质量度-5
                blog.setQualityScore(blog.getQualityScore() - 5);
                blogDao.updateById(blog);

                // 删除该一级评论下的所有二级评论
                List<BlogComment> blogCommentList = dao.listByParentIdAndStatusId(bo.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isEmpty(blogCommentList)) {
                    return Result.success(resultNum);
                }
                for (BlogComment comment : blogCommentList) {
                    comment.setStatusId(StatusIdEnum.NO.getCode());
                    dao.updateById(comment);
                }
            }
        }

        return Result.success(resultNum);
    }

}
