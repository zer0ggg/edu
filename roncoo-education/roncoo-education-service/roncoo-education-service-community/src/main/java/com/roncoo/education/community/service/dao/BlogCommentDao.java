package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;

import java.util.List;

public interface BlogCommentDao {
    int save(BlogComment record);

    int deleteById(Long id);

    int updateById(BlogComment record);

    int updateByExampleSelective(BlogComment record, BlogCommentExample example);

    BlogComment getById(Long id);

    Page<BlogComment> listForPage(int pageCurrent, int pageSize, BlogCommentExample example);

    /**
     * 根据父类id、状态查找评论信息
     * @param parentId
     * @param statusId
     * @return
     */
	List<BlogComment> listByParentIdAndStatusId(Long parentId, Integer statusId);
}
