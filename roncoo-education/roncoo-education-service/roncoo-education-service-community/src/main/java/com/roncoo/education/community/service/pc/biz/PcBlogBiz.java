package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.common.es.EsBlog;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.BlogListRESP;
import com.roncoo.education.community.common.resp.BlogViewRESP;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliyunVO;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 博客信息
 *
 * @author wujing
 */
@Component
public class PcBlogBiz extends BaseBiz {

    @Autowired
    private BlogDao dao;
    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private ArticleZoneRefDao articleZoneRefDao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 博客信息列表
     *
     * @param blogListREQ 博客信息分页查询参数
     * @return 博客信息分页查询结果
     */
    public Result<Page<BlogListRESP>> list(BlogListREQ blogListREQ) {
        BlogExample example = new BlogExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(blogListREQ.getUserNo())) {
            c.andUserNoEqualTo(blogListREQ.getUserNo());
        }
        if (ObjectUtil.isNotNull(blogListREQ.getStatusId())) {
            c.andStatusIdEqualTo(blogListREQ.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        if (ObjectUtil.isNotNull(blogListREQ.getArticleType())) {
            c.andArticleTypeEqualTo(blogListREQ.getArticleType());
        }
        if (StringUtils.hasText(blogListREQ.getTitle())) {
            c.andTitleLike(PageUtil.like(blogListREQ.getTitle()));
        }
        if (StringUtils.hasText(blogListREQ.getTagsName())) {
            c.andTagsNameLike(PageUtil.like(blogListREQ.getTagsName()));
        }
        if (ObjectUtil.isNotNull(blogListREQ.getIsOpen())) {
            c.andIsOpenEqualTo(blogListREQ.getIsOpen());
        }
        if (ObjectUtil.isNotNull(blogListREQ.getTypeId())) {
            c.andTypeIdEqualTo(blogListREQ.getTypeId());
        }
        if (ObjectUtil.isNotNull(blogListREQ.getIsIssue())) {
            c.andIsIssueEqualTo(blogListREQ.getIsIssue());
        }
        if (ObjectUtil.isNotNull(blogListREQ.getIsTop())) {
            c.andIsTopEqualTo(blogListREQ.getIsTop());
        }
        if (ObjectUtil.isNotNull(blogListREQ.getBeginGmtModified())) {
            c.andGmtModifiedGreaterThanOrEqualTo(DateUtil.beginOfDay(blogListREQ.getBeginGmtModified()));
        }
        if (ObjectUtil.isNotNull(blogListREQ.getEndGmtModified())) {
            c.andGmtModifiedLessThanOrEqualTo(DateUtil.offset(DateUtil.beginOfDay(blogListREQ.getEndGmtModified()), DateField.DAY_OF_YEAR, 1));
        }

        example.setOrderByClause("is_top desc, sort asc, id desc ");
        Page<Blog> page = dao.listForPage(blogListREQ.getPageCurrent(), blogListREQ.getPageSize(), example);
        Page<BlogListRESP> respPage = PageUtil.transform(page, BlogListRESP.class);
        if (null == respPage.getList() || respPage.getList().isEmpty()) {
            return Result.success(respPage);
        }

        if (ArticleTypeEnum.BLOG.getCode().equals(blogListREQ.getArticleType())) {
            Set<Long> userNos = respPage.getList().stream().map(BlogListRESP::getUserNo).collect(Collectors.toSet());
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setUserNos(new ArrayList<>(userNos));
            List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
            if (CollectionUtil.isNotEmpty(userExtVOList)) {
                Map<Long, UserExtVO> userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item));
                respPage.getList().stream().forEach(item -> {
                    UserExtVO userExtVO = userExtVOMap.get(item.getUserNo());
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        item.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        item.setBloggerNickname(userExtVO.getNickname());
                    }
                });
            }
        }
        for (BlogListRESP vo : respPage.getList()) {
            // 专区添加文章时用的
            if (ObjectUtil.isNotNull(blogListREQ.getPlatShow()) && ObjectUtil.isNotNull(blogListREQ.getArticleZoneId())) {
                ArticleZoneRef articleZoneRef = articleZoneRefDao.getByArticleZoneIdAndArticleIdAndPlatShow(blogListREQ.getArticleZoneId(), vo.getId(), blogListREQ.getPlatShow());
                if (ObjectUtil.isNotNull(articleZoneRef)) {
                    vo.setIsExist(0);
                } else {
                    vo.setIsExist(1);
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 博客信息添加
     *
     * @param blogSaveREQ 博客信息
     * @return 添加结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(BlogSaveREQ blogSaveREQ) {
        if (StringUtils.isEmpty(blogSaveREQ.getBlogImg())) {
            return Result.error("请上传文章封面");
        }
        if (StringUtils.isEmpty(blogSaveREQ.getTitle())) {
            return Result.error("请输入标题");
        }
        if (StringUtils.isEmpty(blogSaveREQ.getTagsName())) {
            return Result.error("请选择文章标签");
        }
        if (ObjectUtil.isNull(blogSaveREQ.getArticleType())) {
            return Result.error("文章类型不能为空");
        }

        Blog record = BeanUtil.copyProperties(blogSaveREQ, Blog.class);
        record.setId(IdWorker.getId());
        // 获取大字段博客内容content进行截取
        String summary = getBigField(record.getContent());
        record.setSummary(summary + "...");
        // 如果添加是博客、博客数+1


        String headImgUrl = "";
        String nickname = "";
        String mobile = "";
        if (ArticleTypeEnum.BLOG.getCode().equals(blogSaveREQ.getArticleType())) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(blogSaveREQ.getUserNo());
            if (ObjectUtil.isNull(userExtVO)) {
                return Result.error("找不到用户信息");
            }
            if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("该账号已被禁用");
            }

            headImgUrl = userExtVO.getHeadImgUrl();
            nickname = userExtVO.getNickname();
            mobile = userExtVO.getMobile();
            Blogger blogger = bloggerDao.getByBloggerUserNo(blogSaveREQ.getUserNo());
            if (ObjectUtil.isNull(blogger)) {
                return Result.error("找不到博主");
            }
            blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
            bloggerDao.updateById(blogger);
        }

        int blogResults = dao.save(record);
        if (blogResults > 0) {
            Blog blog = dao.getById(record.getId());
            // 只有状态正常、已发布、公开的博客才添加进ES
            if (blog.getStatusId().equals(StatusIdEnum.YES.getCode()) && blog.getIsIssue().equals(IsIssueEnum.YES.getCode()) && blog.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                try {
                    // 插入els或者更新els
                    EsBlog esBlog = BeanUtil.copyProperties(blog, EsBlog.class);
                    if (ArticleTypeEnum.BLOG.getCode().equals(blogSaveREQ.getArticleType())) {
                        esBlog.setBloggerUserImg(headImgUrl);
                        if (org.springframework.util.StringUtils.isEmpty(nickname)) {
                            esBlog.setBloggerNickname(mobile.substring(0, 3) + "****" + mobile.substring(7));
                        } else {
                            esBlog.setBloggerNickname(nickname);
                        }
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                    throw new BaseException("添加失败");
                }
            }
            return Result.success("添加成功");
        }
        throw new BaseException("添加失败");
    }

    /**
     * 博客信息查看
     *
     * @param id 主键ID
     * @return 博客信息
     */
    public Result<BlogViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), BlogViewRESP.class));
    }

    /**
     * 博客信息修改
     *
     * @param blogEditREQ 博客信息修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> edit(BlogEditREQ blogEditREQ) {
        Blog record = dao.getById(blogEditREQ.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("文章不存在");
        }
        UserExtVO userExtVO = new UserExtVO();
        Blogger blogger = new Blogger();
        if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
            userExtVO = feignUserExt.getByUserNo(record.getUserNo());
            if (ObjectUtil.isNull(userExtVO)) {
                return Result.error("找不到用户信息");
            }
            if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("该账号已被禁用");
            }
            blogger = bloggerDao.getByBloggerUserNo(record.getUserNo());
            if (ObjectUtil.isNull(blogger)) {
                return Result.error("找不到博主");
            }
        }

        record = BeanUtil.copyProperties(blogEditREQ, Blog.class);
        if (!StatusIdEnum.YES.getCode().equals(record.getStatusId()) || !IsIssueEnum.YES.getCode().equals(record.getIsIssue()) || !IsOpenEnum.YES.getCode().equals(record.getIsOpen())) {
            // 删除els数据
            elasticsearchRestTemplate.delete(blogEditREQ.getId().toString(), EsBlog.class);
            if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
                blogger.setWeblogAccount(blogger.getWeblogAccount() - 1);
            }
        } else {
            try {
                // 插入els或者更新els
                EsBlog esBlog = BeanUtil.copyProperties(record, EsBlog.class);
                if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
                    esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esBlog.setBloggerNickname(userExtVO.getNickname());
                    }
                    blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
                throw new BaseException("编辑失败");
            }
        }
        dao.updateById(record);
        return Result.success("编辑成功");
    }

    /**
     * 博客信息修改
     *
     * @param blogUpdateStatusREQ 博客信息修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> updateStatus(BlogUpdateStatusREQ blogUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(blogUpdateStatusREQ.getStatusId()))) {
            return Result.error("状态不正确");
        }
        Blog record = dao.getById(blogUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("文章不存在");
        }
        UserExtVO userExtVO = new UserExtVO();
        Blogger blogger = new Blogger();
        if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
            userExtVO = feignUserExt.getByUserNo(record.getUserNo());
            if (ObjectUtil.isNull(userExtVO)) {
                return Result.error("找不到用户信息");
            }
            if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("该账号已被禁用");
            }
            blogger = bloggerDao.getByBloggerUserNo(record.getUserNo());
            if (ObjectUtil.isNull(blogger)) {
                return Result.error("找不到博主");
            }
        }

        record = BeanUtil.copyProperties(blogUpdateStatusREQ, Blog.class);
        if (!StatusIdEnum.YES.getCode().equals(record.getStatusId()) || !IsIssueEnum.YES.getCode().equals(record.getIsIssue()) || !IsOpenEnum.YES.getCode().equals(record.getIsOpen())) {
            // 删除els数据
            elasticsearchRestTemplate.delete(blogUpdateStatusREQ.getId().toString(), EsBlog.class);
            if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
                blogger.setWeblogAccount(blogger.getWeblogAccount() - 1);
            }
        } else {
            try {
                // 插入els或者更新els
                EsBlog esBlog = BeanUtil.copyProperties(record, EsBlog.class);
                if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
                    esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esBlog.setBloggerNickname(userExtVO.getNickname());
                    }
                    blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
                throw new BaseException("编辑失败");
            }
        }
        dao.updateById(record);
        return Result.success("编辑成功");
    }

    /**
     * 博客信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        Blog bean = new Blog();
        bean.setId(id);
        bean.setStatusId(Constants.FREEZE);
        if (dao.updateById(bean) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 导入ES
     */
    public Result<String> addEs(BlogAddEsREQ blogAddEsREQ) {
        try {
            List<IndexQuery> queries = new ArrayList<>();
            // 查询全部，删除索引重建
            List<Blog> blogList = dao.listByStatusId(StatusIdEnum.YES.getCode());
            for (Blog weblog : blogList) {
                EsBlog esBlog = BeanUtil.copyProperties(weblog, EsBlog.class);
                IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                queries.add(query);
            }
            elasticsearchRestTemplate.indexOps(EsBlog.class).delete();
            elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsBlog.BLOG));
            return Result.success("添加ES成功");
        } catch (Exception e) {
            logger.error("elasticsearch导入数据失败", e);
            return Result.error("添加ES失败");
        }
    }

    /**
     * 上传文件
     *
     * @param multipartFile 文件
     * @return 文件路径
     */
    public Result<String> upload(MultipartFile multipartFile) {
        ConfigAliyunVO sysVO = feignSysConfig.getAliyun();
        if (StringUtils.isEmpty(sysVO.getAliyunAccessKeyId())) {
            throw new BaseException("未配置accessKeyId");
        }
        if (StringUtils.isEmpty(sysVO.getAliyunAccessKeySecret())) {
            throw new BaseException("未配置AliyunAccessKeySecret");
        }
        if (StringUtils.isEmpty(sysVO.getAliyunOssBucket())) {
            throw new BaseException("未配置AliyunOssBucket");
        }
        String url = AliyunUtil.uploadPic(PlatformEnum.COMMUNITY, multipartFile, BeanUtil.copyProperties(sysVO, Aliyun.class));
        if (StrUtil.isNotBlank(url)) {
            return Result.success(url);
        }
        return Result.error("上传文件失败");
    }

    private String getBigField(String content) {
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        // 截取前100字符串
        content = com.roncoo.education.common.core.tools.StrUtil.subStrStart(content, 80);
        return content;
    }

}
