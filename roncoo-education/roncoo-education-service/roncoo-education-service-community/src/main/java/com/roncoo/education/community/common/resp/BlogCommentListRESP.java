package com.roncoo.education.community.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 博客评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BlogCommentListRESP", description = "博客评论列表")
public class BlogCommentListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "博客ID")
    private Long blogId;

    @ApiModelProperty(value = "博客标题")
    private String title;

    @ApiModelProperty(value = "被评论用户编号")
    private Long bloggerUserNo;

    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论者IP")
    private String userIp;

    @ApiModelProperty(value = "评论者终端")
    private String userTerminal;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)")
    private Integer articleType;

    @ApiModelProperty(value = "评论者用户昵称")
    private String nickname;

    @ApiModelProperty(value = "被评论用户昵称")
    private String bloggerNickname;

    @ApiModelProperty(value = "博客评论集合")
    private List<BlogCommentListRESP> blogCommentList;
}
