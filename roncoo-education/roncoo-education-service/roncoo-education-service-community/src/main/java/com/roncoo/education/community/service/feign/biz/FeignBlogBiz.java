package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ArticleTypeEnum;
import com.roncoo.education.common.core.enums.IsIssueEnum;
import com.roncoo.education.common.core.enums.IsOpenEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.common.es.EsBlog;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.feign.qo.BlogQO;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 博客信息表
 *
 * @author wujing
 */
@Component
public class FeignBlogBiz extends BaseBiz {

    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private BlogDao dao;
    @Autowired
    private ArticleZoneRefDao articleZoneRefDao;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    public Page<BlogVO> listForPage(BlogQO qo) {
        BlogExample example = new BlogExample();
        Criteria c = example.createCriteria();
        if (qo.getUserNo() != null) {
            c.andUserNoEqualTo(qo.getUserNo());
        }
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }
        if (qo.getArticleType() != null) {
            c.andArticleTypeEqualTo(qo.getArticleType());
        }
        if (StringUtils.hasText(qo.getTitle())) {
            c.andTitleLike(PageUtil.like(qo.getTitle()));
        }
        if (StringUtils.hasText(qo.getTagsName())) {
            c.andTagsNameLike(PageUtil.like(qo.getTagsName()));
        }
        if (qo.getIsOpen() != null) {
            c.andIsOpenEqualTo(qo.getIsOpen());
        }
        if (qo.getTypeId() != null) {
            c.andTypeIdEqualTo(qo.getTypeId());
        }
        if (qo.getIsIssue() != null) {
            c.andIsIssueEqualTo(qo.getIsIssue());
        }
        if (qo.getIsTop() != null) {
            c.andIsTopEqualTo(qo.getIsTop());
        }
        if (StringUtils.hasText(qo.getBeginGmtCreate())) {
            c.andGmtModifiedGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(qo.getEndGmtCreate())) {
            c.andGmtModifiedLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }

        example.setOrderByClause(" status_id desc, is_top desc, sort asc, id desc ");
        Page<Blog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<BlogVO> listForPage = PageUtil.transform(page, BlogVO.class);
        for (BlogVO vo : listForPage.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    vo.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    vo.setBloggerNickname(userExtVO.getNickname());
                }
            }
            // 专区添加文章时用的
            if (qo.getPlatShow() != null && qo.getArticleZoneId() != null) {
                ArticleZoneRef articleZoneRef = articleZoneRefDao.getByArticleZoneIdAndArticleIdAndPlatShow(qo.getArticleZoneId(), vo.getId(), qo.getPlatShow());
                if (ObjectUtil.isNotNull(articleZoneRef)) {
                    vo.setIsExist(0);
                } else {
                    vo.setIsExist(1);
                }
            }
        }
        return listForPage;
    }

    @Transactional(rollbackFor = Exception.class)
    public int save(BlogQO qo) {
        if (StringUtils.isEmpty(qo.getBlogImg())) {
            throw new BaseException("请上传文章封面");
        }
        if (StringUtils.isEmpty(qo.getTitle())) {
            throw new BaseException("请输入标题");
        }
        if (StringUtils.isEmpty(qo.getTagsName())) {
            throw new BaseException("请选择文章标签");
        }
        if (qo.getArticleType() == null) {
            throw new BaseException("文章类型不能为空");
        }
        if (ArticleTypeEnum.INFORMATION.getCode().equals(qo.getArticleType())) {
            // 资讯默认用户管理员添加
            qo.setUserNo(Constants.ADMIN_USER_NO);
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(qo.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("找不到用信息");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            throw new BaseException("该账号已被禁用");
        }
        Blog record = BeanUtil.copyProperties(qo, Blog.class);
        record.setId(IdWorker.getId());
        int blogResults = dao.save(record);
        // 如果添加是博客、博客数+1
        if (ArticleTypeEnum.BLOG.getCode().equals(qo.getArticleType())) {
            Blogger blogger = bloggerDao.getByBloggerUserNo(qo.getUserNo());
            if (ObjectUtil.isNull(blogger)) {
                throw new BaseException("找不到博主");
            }
            blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
            bloggerDao.updateById(blogger);
        }
        if (blogResults > 0) {
            Blog blog = dao.getById(record.getId());
            // 只有状态正常、已发布、公开的博客才添加进ES
            if (blog.getStatusId().equals(StatusIdEnum.YES.getCode()) && blog.getIsIssue().equals(IsIssueEnum.YES.getCode()) && blog.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                try {
                    // 插入els或者更新els
                    EsBlog esBlog = BeanUtil.copyProperties(blog, EsBlog.class);
                    esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esBlog.setBloggerNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                }
            }
            return blogResults;
        }
        throw new BaseException("添加失败");
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public BlogVO getById(Long id) {
        Blog record = dao.getById(id);
        return BeanUtil.copyProperties(record, BlogVO.class);
    }

    public BlogVO getByIdAndStatusId(Long id, Integer statusId) {
        Blog record = dao.getByIdAndStatusId(id, statusId);
        if (ObjectUtil.isNull(record)) {
            return null;
        }
        BlogVO vo = BeanUtil.copyProperties(record, BlogVO.class);
        UserExtVO userExtVO = feignUserExt.getByUserNo(record.getUserNo());
        if (!ObjectUtil.isNull(userExtVO)) {
            vo.setBloggerNickname(userExtVO.getNickname());
        }
        return vo;
    }

    public int updateById(BlogQO qo) {
        Blog record = dao.getById(qo.getId());
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("文章不存在");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(record.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            throw new BaseException("找不到用信息");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            throw new BaseException("该账号已被禁用");
        }
        Blogger blogger = bloggerDao.getByBloggerUserNo(record.getUserNo());
        if (ObjectUtil.isNull(blogger)) {
            throw new BaseException("找不到博主");
        }
        record = BeanUtil.copyProperties(qo, Blog.class);
        if (!StatusIdEnum.YES.getCode().equals(record.getStatusId()) || !IsIssueEnum.YES.getCode().equals(record.getIsIssue()) || !IsOpenEnum.YES.getCode().equals(record.getIsOpen())) {
            // 删除els数据
            elasticsearchRestTemplate.delete(String.valueOf(qo.getId()), EsQuestions.class);
            if (ArticleTypeEnum.BLOG.getCode().equals(record.getArticleType())) {
                blogger.setWeblogAccount(blogger.getWeblogAccount() - 1);
            }
        } else {
            try {
                // 插入els或者更新els
                EsBlog esBlog = BeanUtil.copyProperties(record, EsBlog.class);
                esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                    esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    esBlog.setBloggerNickname(userExtVO.getNickname());
                }

                IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
            }
            blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
        }
        return dao.updateById(record);
    }

    /**
     * 导入ES
     *
     * @param qo
     * @author wuyun
     */
    public boolean addEs(BlogQO qo) {
        try {
            if (!StringUtils.isEmpty(qo.getIds())) {
                String[] weblogId = qo.getIds().split(",");
                List<IndexQuery> queries = new ArrayList<>();
                for (String i : weblogId) {
                    Blog weblog = dao.getById(Long.valueOf(i));
                    EsBlog esWeblogQO = BeanUtil.copyProperties(weblog, EsBlog.class);
                    IndexQuery query = new IndexQueryBuilder().withObject(esWeblogQO).build();
                    queries.add(query);
                }
                elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsBlog.BLOG));
                return true;
            }
        } catch (Exception e) {
            logger.error("elasticsearch导入数据失败", e);
            return false;
        }
        return false;
    }
}
