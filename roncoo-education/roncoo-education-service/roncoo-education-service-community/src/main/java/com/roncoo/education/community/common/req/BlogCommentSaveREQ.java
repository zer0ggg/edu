package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 博客评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="BlogCommentSaveREQ", description="博客评论添加")
public class BlogCommentSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotNull(message = "父ID不能为空")
    @ApiModelProperty(value = "父ID",required = true)
    private Long parentId;

    @NotNull(message = "博客ID不能为空")
    @ApiModelProperty(value = "博客ID",required = true)
    private Long blogId;

    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @NotEmpty(message = "评论者昵称不能为空")
    @ApiModelProperty(value = "评论者昵称",required = true)
    private String nickname;

    @NotEmpty(message = "评论内容不能为空")
    @ApiModelProperty(value = "评论内容",required = true)
    private String content;

    @NotEmpty(message = "评论者终端不能为空")
    @ApiModelProperty(value = "评论者终端",required = true)
    private String userTerminal;


}
