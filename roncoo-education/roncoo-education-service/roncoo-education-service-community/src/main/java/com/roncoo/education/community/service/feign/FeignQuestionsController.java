package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignQuestions;
import com.roncoo.education.community.feign.qo.QuestionsQO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import com.roncoo.education.community.service.feign.biz.FeignQuestionsBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答信息表
 *
 * @author wujing
 */
@RestController
public class FeignQuestionsController extends BaseController implements IFeignQuestions {

	@Autowired
	private FeignQuestionsBiz biz;

	@Override
	public Page<QuestionsVO> listForPage(@RequestBody QuestionsQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody QuestionsQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody QuestionsQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public QuestionsVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public int updateGoodComment(@RequestBody QuestionsQO qo) {
		return biz.updateGoodComment(qo);
	}

    @Override
    public boolean addEs(@RequestBody QuestionsQO qo) {
        return biz.addEs(qo);
    }

    @Override
    public int updateStatusId(@RequestBody QuestionsQO qo) {
        return biz.updateStatusId(qo);
    }

}
