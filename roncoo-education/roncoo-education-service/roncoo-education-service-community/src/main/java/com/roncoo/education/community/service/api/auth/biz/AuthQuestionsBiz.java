package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.IsGoodEnum;
import com.roncoo.education.common.core.enums.OpTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsListDTO;
import com.roncoo.education.community.common.dto.auth.AuthQuestionsViewDTO;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.QuestionsUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Component
public class AuthQuestionsBiz extends BaseBiz {

    @Autowired
    private QuestionsDao dao;
    @Autowired
    private QuestionsUserRecordDao questionsUserRecordDao;
    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private QuestionsCommentDao questionsCommentDao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthQuestionsSaveBO bo) {
        if (StringUtils.isEmpty(bo.getContent())) {
            return Result.error("内容不能为空");
        }
        if (StringUtils.isEmpty(bo.getTagsName())) {
            return Result.error("标签不能为空");
        }
        if (StringUtils.isEmpty(bo.getTitle())) {
            return Result.error("标题不能为空");
        }
        Blogger blogger = bloggerDao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("获取不到用户信息");
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(blogger.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }
        Questions questions = BeanUtil.copyProperties(bo, Questions.class);
        questions.setUserNo(ThreadContext.userNo());
        // 获取大字段问答内容content进行截取
        String summary = getBigField(questions.getContent());
        questions.setSummary(summary + "...");
        int result = dao.save(questions);
        if (result > 0) {
            // 更新问答数量
            blogger.setQuestionsAccount(blogger.getQuestionsAccount() + 1);
            bloggerDao.updateById(blogger);
            try {
                // 插入els或者更新els
                EsQuestions esQuestions = BeanUtil.copyProperties(dao.getById(questions.getId()), EsQuestions.class);
                esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                    esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    esQuestions.setNickname(userExtVO.getNickname());
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsQuestions.QUESTIONS));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
                throw new BaseException("添加失败");
            }
            return Result.success(result);
        }
        return Result.error("添加失败");
    }

    private String getBigField(String content) {
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        // 截取前100字符串
        content = StrUtil.subStrStart(content, 100);
        return content;
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> delete(AuthQuestionsDeleteBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Questions questions = dao.getById(bo.getId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答信息");
        }
        if (!questions.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不能删除别人的问答");
        }
        int result = dao.deleteById(bo.getId());
        if (result > 0) {
            // 删除该问答对应的评论
            questionsCommentDao.deleteByQuestionId(bo.getId());
            // 更新博主的问答信息,问答数量-1
            Blogger blogger = bloggerDao.getByBloggerUserNo(ThreadContext.userNo());
            if (ObjectUtil.isNull(blogger)) {
                return Result.error("找不到用户信息");
            }
            blogger.setQuestionsAccount(blogger.getQuestionsAccount() - 1);
            bloggerDao.updateById(blogger);
            // 删除els数据
            elasticsearchRestTemplate.delete(String.valueOf(bo.getId()), EsQuestions.class);
            return Result.success(result);
        }
        return Result.error("删除失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> update(AuthQuestionsUpdateBO bo) {
        if (!StringUtils.hasText(bo.getContent())) {
            return Result.error("内容不能为空");
        }
        if (!StringUtils.hasText(bo.getTagsName())) {
            return Result.error("标签不能为空");
        }
        if (!StringUtils.hasText(bo.getTitle())) {
            return Result.error("标题不能为空");
        }
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Questions questions = dao.getById(bo.getId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答信息");
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(questions.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }
        if (!questions.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不能修改别人的问答");
        }
        Questions record = BeanUtil.copyProperties(bo, Questions.class);
        // 获取大字段问答内容content进行截取
        String summary = getBigField(record.getContent());
        record.setSummary(summary + "...");
        int result = dao.updateById(record);
        if (result > 0) {
            try {
                // 插入els或者更新els
                EsQuestions esQuestions = BeanUtil.copyProperties(dao.getById(questions.getId()), EsQuestions.class);
                esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                    esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    esQuestions.setNickname(userExtVO.getNickname());
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsQuestions.QUESTIONS));
            } catch (Exception e) {
                logger.error("elasticsearch更新数据失败", e);
                throw new BaseException("修改失败");
            }

            return Result.success(result);
        }
        return Result.error("修改失败");
    }

    public Result<AuthQuestionsViewDTO> view(AuthQuestionsViewBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Questions questions = dao.getById(bo.getId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到该问答");
        }

        // 阅读数量+1
        questions.setReadAcount(questions.getReadAcount() + 1);
        // 质量度+1
        questions.setQualityScore(questions.getQualityScore() + 1);
        AuthQuestionsViewDTO dto = BeanUtil.copyProperties(questions, AuthQuestionsViewDTO.class);
        UserExtVO userExtVO = feignUserExt.getByUserNo(questions.getUserNo());
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
        } else {
            dto.setNickname(userExtVO.getNickname());
        }
        dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
        if (IsGoodEnum.YES.getCode().equals(questions.getIsGood())) {
            UserExtVO vo = feignUserExt.getByUserNo(questions.getGoodUserNo());
            if (StringUtils.isEmpty(vo.getNickname())) {
                dto.setGoodUserNickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
            } else {
                dto.setGoodUserNickname(vo.getNickname());
            }
            dto.setGoodUserHeadImgUrl(vo.getHeadImgUrl());
        }
        QuestionsUserRecord questionsUserRecord = questionsUserRecordDao.getByQuestionIdAndUserNoAndOpType(bo.getId(), questions.getUserNo(), OpTypeEnum.COLLECTION.getCode());
        if (ObjectUtil.isNull(questionsUserRecord)) {
            // 未收藏
            dto.setIsCollection(0);
        } else {
            // 已收藏
            dto.setIsCollection(1);
        }
        QuestionsUserRecord record = questionsUserRecordDao.getByQuestionIdAndUserNoAndOpType(bo.getId(), questions.getUserNo(), OpTypeEnum.PRAISE.getCode());
        if (ObjectUtil.isNull(record)) {
            // 未点赞
            dto.setIsPraise(0);
        } else {
            // 已点赞
            dto.setIsPraise(1);
        }

        Questions quest = new Questions();
        quest.setId(questions.getId());
        quest.setReadAcount(questions.getReadAcount());
        quest.setQualityScore(questions.getQualityScore());
        dao.updateById(quest);
        return Result.success(dto);
    }

    public Result<Page<AuthQuestionsListDTO>> list(AuthQuestionsListBO bo) {
        UserExtVO userExt = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExt)) {
            return Result.error("找不到该用户信息");
        }
        QuestionsExample example = new QuestionsExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause(" id desc");
        Page<Questions> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(page, AuthQuestionsListDTO.class));
    }

}
