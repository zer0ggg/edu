package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample;

import java.util.List;

public interface BlogDao {
    int save(Blog record);

    int deleteById(Long id);

    int updateById(Blog record);

    int updateByExampleSelective(Blog record, BlogExample example);

    Blog getById(Long id);

    Page<Blog> listForPage(int pageCurrent, int pageSize, BlogExample example);

    /**
     * 查询关注的博主的博客信息列表
     * @param userNos
     * @param pageCurrent
     * @param pageSize
     * @return
     */
	Page<Blog> listByWeblogUserAttention(List<Long> userNos, int pageCurrent, int pageSize);

    List<Blog> listByUserNoAndIsOpenAndIsIssue(Long bloggerUserNo, Integer isOpen, Integer isIssue);

    /**
     * 根据标题列出
     * @param title
     * @return
     */
    List<Blog> listByTitle(String title);

    /**
     * 根据文章类型获取博客信息集合
     * @return
     */
    List<Blog> listByArticleType(Integer articleType);

    /**
     * 获取博客信息集合
     * @return
     */
    List<Blog> getBlogList();

    /**
     * 根据状态获取博客信息集合
     * @return
     */
    List<Blog> listByStatusId(Integer statusId);

    /**
     * 根据ID和状态获取信息
     * @param id
     * @param statusId
     * @return
     */
    Blog getByIdAndStatusId(Long id,Integer statusId);
}
