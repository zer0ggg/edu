package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlogExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public BlogExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andIsImgIsNull() {
            addCriterion("is_img is null");
            return (Criteria) this;
        }

        public Criteria andIsImgIsNotNull() {
            addCriterion("is_img is not null");
            return (Criteria) this;
        }

        public Criteria andIsImgEqualTo(Integer value) {
            addCriterion("is_img =", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgNotEqualTo(Integer value) {
            addCriterion("is_img <>", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgGreaterThan(Integer value) {
            addCriterion("is_img >", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_img >=", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgLessThan(Integer value) {
            addCriterion("is_img <", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgLessThanOrEqualTo(Integer value) {
            addCriterion("is_img <=", value, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgIn(List<Integer> values) {
            addCriterion("is_img in", values, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgNotIn(List<Integer> values) {
            addCriterion("is_img not in", values, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgBetween(Integer value1, Integer value2) {
            addCriterion("is_img between", value1, value2, "isImg");
            return (Criteria) this;
        }

        public Criteria andIsImgNotBetween(Integer value1, Integer value2) {
            addCriterion("is_img not between", value1, value2, "isImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgIsNull() {
            addCriterion("blog_img is null");
            return (Criteria) this;
        }

        public Criteria andBlogImgIsNotNull() {
            addCriterion("blog_img is not null");
            return (Criteria) this;
        }

        public Criteria andBlogImgEqualTo(String value) {
            addCriterion("blog_img =", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgNotEqualTo(String value) {
            addCriterion("blog_img <>", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgGreaterThan(String value) {
            addCriterion("blog_img >", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgGreaterThanOrEqualTo(String value) {
            addCriterion("blog_img >=", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgLessThan(String value) {
            addCriterion("blog_img <", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgLessThanOrEqualTo(String value) {
            addCriterion("blog_img <=", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgLike(String value) {
            addCriterion("blog_img like", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgNotLike(String value) {
            addCriterion("blog_img not like", value, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgIn(List<String> values) {
            addCriterion("blog_img in", values, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgNotIn(List<String> values) {
            addCriterion("blog_img not in", values, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgBetween(String value1, String value2) {
            addCriterion("blog_img between", value1, value2, "blogImg");
            return (Criteria) this;
        }

        public Criteria andBlogImgNotBetween(String value1, String value2) {
            addCriterion("blog_img not between", value1, value2, "blogImg");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andArticleTypeIsNull() {
            addCriterion("article_type is null");
            return (Criteria) this;
        }

        public Criteria andArticleTypeIsNotNull() {
            addCriterion("article_type is not null");
            return (Criteria) this;
        }

        public Criteria andArticleTypeEqualTo(Integer value) {
            addCriterion("article_type =", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeNotEqualTo(Integer value) {
            addCriterion("article_type <>", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeGreaterThan(Integer value) {
            addCriterion("article_type >", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("article_type >=", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeLessThan(Integer value) {
            addCriterion("article_type <", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeLessThanOrEqualTo(Integer value) {
            addCriterion("article_type <=", value, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeIn(List<Integer> values) {
            addCriterion("article_type in", values, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeNotIn(List<Integer> values) {
            addCriterion("article_type not in", values, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeBetween(Integer value1, Integer value2) {
            addCriterion("article_type between", value1, value2, "articleType");
            return (Criteria) this;
        }

        public Criteria andArticleTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("article_type not between", value1, value2, "articleType");
            return (Criteria) this;
        }

        public Criteria andSummaryIsNull() {
            addCriterion("summary is null");
            return (Criteria) this;
        }

        public Criteria andSummaryIsNotNull() {
            addCriterion("summary is not null");
            return (Criteria) this;
        }

        public Criteria andSummaryEqualTo(String value) {
            addCriterion("summary =", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotEqualTo(String value) {
            addCriterion("summary <>", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryGreaterThan(String value) {
            addCriterion("summary >", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryGreaterThanOrEqualTo(String value) {
            addCriterion("summary >=", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLessThan(String value) {
            addCriterion("summary <", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLessThanOrEqualTo(String value) {
            addCriterion("summary <=", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLike(String value) {
            addCriterion("summary like", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotLike(String value) {
            addCriterion("summary not like", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryIn(List<String> values) {
            addCriterion("summary in", values, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotIn(List<String> values) {
            addCriterion("summary not in", values, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryBetween(String value1, String value2) {
            addCriterion("summary between", value1, value2, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotBetween(String value1, String value2) {
            addCriterion("summary not between", value1, value2, "summary");
            return (Criteria) this;
        }

        public Criteria andTagsNameIsNull() {
            addCriterion("tags_name is null");
            return (Criteria) this;
        }

        public Criteria andTagsNameIsNotNull() {
            addCriterion("tags_name is not null");
            return (Criteria) this;
        }

        public Criteria andTagsNameEqualTo(String value) {
            addCriterion("tags_name =", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotEqualTo(String value) {
            addCriterion("tags_name <>", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameGreaterThan(String value) {
            addCriterion("tags_name >", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameGreaterThanOrEqualTo(String value) {
            addCriterion("tags_name >=", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLessThan(String value) {
            addCriterion("tags_name <", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLessThanOrEqualTo(String value) {
            addCriterion("tags_name <=", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLike(String value) {
            addCriterion("tags_name like", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotLike(String value) {
            addCriterion("tags_name not like", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameIn(List<String> values) {
            addCriterion("tags_name in", values, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotIn(List<String> values) {
            addCriterion("tags_name not in", values, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameBetween(String value1, String value2) {
            addCriterion("tags_name between", value1, value2, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotBetween(String value1, String value2) {
            addCriterion("tags_name not between", value1, value2, "tagsName");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andMdContetIsNull() {
            addCriterion("md_contet is null");
            return (Criteria) this;
        }

        public Criteria andMdContetIsNotNull() {
            addCriterion("md_contet is not null");
            return (Criteria) this;
        }

        public Criteria andMdContetEqualTo(String value) {
            addCriterion("md_contet =", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetNotEqualTo(String value) {
            addCriterion("md_contet <>", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetGreaterThan(String value) {
            addCriterion("md_contet >", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetGreaterThanOrEqualTo(String value) {
            addCriterion("md_contet >=", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetLessThan(String value) {
            addCriterion("md_contet <", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetLessThanOrEqualTo(String value) {
            addCriterion("md_contet <=", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetLike(String value) {
            addCriterion("md_contet like", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetNotLike(String value) {
            addCriterion("md_contet not like", value, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetIn(List<String> values) {
            addCriterion("md_contet in", values, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetNotIn(List<String> values) {
            addCriterion("md_contet not in", values, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetBetween(String value1, String value2) {
            addCriterion("md_contet between", value1, value2, "mdContet");
            return (Criteria) this;
        }

        public Criteria andMdContetNotBetween(String value1, String value2) {
            addCriterion("md_contet not between", value1, value2, "mdContet");
            return (Criteria) this;
        }

        public Criteria andIsMdIsNull() {
            addCriterion("is_md is null");
            return (Criteria) this;
        }

        public Criteria andIsMdIsNotNull() {
            addCriterion("is_md is not null");
            return (Criteria) this;
        }

        public Criteria andIsMdEqualTo(Integer value) {
            addCriterion("is_md =", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdNotEqualTo(Integer value) {
            addCriterion("is_md <>", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdGreaterThan(Integer value) {
            addCriterion("is_md >", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_md >=", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdLessThan(Integer value) {
            addCriterion("is_md <", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdLessThanOrEqualTo(Integer value) {
            addCriterion("is_md <=", value, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdIn(List<Integer> values) {
            addCriterion("is_md in", values, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdNotIn(List<Integer> values) {
            addCriterion("is_md not in", values, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdBetween(Integer value1, Integer value2) {
            addCriterion("is_md between", value1, value2, "isMd");
            return (Criteria) this;
        }

        public Criteria andIsMdNotBetween(Integer value1, Integer value2) {
            addCriterion("is_md not between", value1, value2, "isMd");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIsNull() {
            addCriterion("quality_score is null");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIsNotNull() {
            addCriterion("quality_score is not null");
            return (Criteria) this;
        }

        public Criteria andQualityScoreEqualTo(Integer value) {
            addCriterion("quality_score =", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotEqualTo(Integer value) {
            addCriterion("quality_score <>", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreGreaterThan(Integer value) {
            addCriterion("quality_score >", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("quality_score >=", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreLessThan(Integer value) {
            addCriterion("quality_score <", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreLessThanOrEqualTo(Integer value) {
            addCriterion("quality_score <=", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIn(List<Integer> values) {
            addCriterion("quality_score in", values, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotIn(List<Integer> values) {
            addCriterion("quality_score not in", values, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreBetween(Integer value1, Integer value2) {
            addCriterion("quality_score between", value1, value2, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("quality_score not between", value1, value2, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNull() {
            addCriterion("keywords is null");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNotNull() {
            addCriterion("keywords is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordsEqualTo(String value) {
            addCriterion("keywords =", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotEqualTo(String value) {
            addCriterion("keywords <>", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThan(String value) {
            addCriterion("keywords >", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThanOrEqualTo(String value) {
            addCriterion("keywords >=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThan(String value) {
            addCriterion("keywords <", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThanOrEqualTo(String value) {
            addCriterion("keywords <=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLike(String value) {
            addCriterion("keywords like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotLike(String value) {
            addCriterion("keywords not like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsIn(List<String> values) {
            addCriterion("keywords in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotIn(List<String> values) {
            addCriterion("keywords not in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsBetween(String value1, String value2) {
            addCriterion("keywords between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotBetween(String value1, String value2) {
            addCriterion("keywords not between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andEditModeIsNull() {
            addCriterion("edit_mode is null");
            return (Criteria) this;
        }

        public Criteria andEditModeIsNotNull() {
            addCriterion("edit_mode is not null");
            return (Criteria) this;
        }

        public Criteria andEditModeEqualTo(Integer value) {
            addCriterion("edit_mode =", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeNotEqualTo(Integer value) {
            addCriterion("edit_mode <>", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeGreaterThan(Integer value) {
            addCriterion("edit_mode >", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeGreaterThanOrEqualTo(Integer value) {
            addCriterion("edit_mode >=", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeLessThan(Integer value) {
            addCriterion("edit_mode <", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeLessThanOrEqualTo(Integer value) {
            addCriterion("edit_mode <=", value, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeIn(List<Integer> values) {
            addCriterion("edit_mode in", values, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeNotIn(List<Integer> values) {
            addCriterion("edit_mode not in", values, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeBetween(Integer value1, Integer value2) {
            addCriterion("edit_mode between", value1, value2, "editMode");
            return (Criteria) this;
        }

        public Criteria andEditModeNotBetween(Integer value1, Integer value2) {
            addCriterion("edit_mode not between", value1, value2, "editMode");
            return (Criteria) this;
        }

        public Criteria andIsOpenIsNull() {
            addCriterion("is_open is null");
            return (Criteria) this;
        }

        public Criteria andIsOpenIsNotNull() {
            addCriterion("is_open is not null");
            return (Criteria) this;
        }

        public Criteria andIsOpenEqualTo(Integer value) {
            addCriterion("is_open =", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenNotEqualTo(Integer value) {
            addCriterion("is_open <>", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenGreaterThan(Integer value) {
            addCriterion("is_open >", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_open >=", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenLessThan(Integer value) {
            addCriterion("is_open <", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenLessThanOrEqualTo(Integer value) {
            addCriterion("is_open <=", value, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenIn(List<Integer> values) {
            addCriterion("is_open in", values, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenNotIn(List<Integer> values) {
            addCriterion("is_open not in", values, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenBetween(Integer value1, Integer value2) {
            addCriterion("is_open between", value1, value2, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsOpenNotBetween(Integer value1, Integer value2) {
            addCriterion("is_open not between", value1, value2, "isOpen");
            return (Criteria) this;
        }

        public Criteria andIsIssueIsNull() {
            addCriterion("is_issue is null");
            return (Criteria) this;
        }

        public Criteria andIsIssueIsNotNull() {
            addCriterion("is_issue is not null");
            return (Criteria) this;
        }

        public Criteria andIsIssueEqualTo(Integer value) {
            addCriterion("is_issue =", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueNotEqualTo(Integer value) {
            addCriterion("is_issue <>", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueGreaterThan(Integer value) {
            addCriterion("is_issue >", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_issue >=", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueLessThan(Integer value) {
            addCriterion("is_issue <", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueLessThanOrEqualTo(Integer value) {
            addCriterion("is_issue <=", value, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueIn(List<Integer> values) {
            addCriterion("is_issue in", values, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueNotIn(List<Integer> values) {
            addCriterion("is_issue not in", values, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueBetween(Integer value1, Integer value2) {
            addCriterion("is_issue between", value1, value2, "isIssue");
            return (Criteria) this;
        }

        public Criteria andIsIssueNotBetween(Integer value1, Integer value2) {
            addCriterion("is_issue not between", value1, value2, "isIssue");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNull() {
            addCriterion("type_id is null");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNotNull() {
            addCriterion("type_id is not null");
            return (Criteria) this;
        }

        public Criteria andTypeIdEqualTo(Integer value) {
            addCriterion("type_id =", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotEqualTo(Integer value) {
            addCriterion("type_id <>", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThan(Integer value) {
            addCriterion("type_id >", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("type_id >=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThan(Integer value) {
            addCriterion("type_id <", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThanOrEqualTo(Integer value) {
            addCriterion("type_id <=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdIn(List<Integer> values) {
            addCriterion("type_id in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotIn(List<Integer> values) {
            addCriterion("type_id not in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdBetween(Integer value1, Integer value2) {
            addCriterion("type_id between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("type_id not between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNull() {
            addCriterion("is_top is null");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNotNull() {
            addCriterion("is_top is not null");
            return (Criteria) this;
        }

        public Criteria andIsTopEqualTo(Integer value) {
            addCriterion("is_top =", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotEqualTo(Integer value) {
            addCriterion("is_top <>", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThan(Integer value) {
            addCriterion("is_top >", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_top >=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThan(Integer value) {
            addCriterion("is_top <", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThanOrEqualTo(Integer value) {
            addCriterion("is_top <=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopIn(List<Integer> values) {
            addCriterion("is_top in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotIn(List<Integer> values) {
            addCriterion("is_top not in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopBetween(Integer value1, Integer value2) {
            addCriterion("is_top between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotBetween(Integer value1, Integer value2) {
            addCriterion("is_top not between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIsNull() {
            addCriterion("collection_acount is null");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIsNotNull() {
            addCriterion("collection_acount is not null");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountEqualTo(Integer value) {
            addCriterion("collection_acount =", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotEqualTo(Integer value) {
            addCriterion("collection_acount <>", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountGreaterThan(Integer value) {
            addCriterion("collection_acount >", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("collection_acount >=", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountLessThan(Integer value) {
            addCriterion("collection_acount <", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountLessThanOrEqualTo(Integer value) {
            addCriterion("collection_acount <=", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIn(List<Integer> values) {
            addCriterion("collection_acount in", values, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotIn(List<Integer> values) {
            addCriterion("collection_acount not in", values, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountBetween(Integer value1, Integer value2) {
            addCriterion("collection_acount between", value1, value2, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("collection_acount not between", value1, value2, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIsNull() {
            addCriterion("admire_acount is null");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIsNotNull() {
            addCriterion("admire_acount is not null");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountEqualTo(Integer value) {
            addCriterion("admire_acount =", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotEqualTo(Integer value) {
            addCriterion("admire_acount <>", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountGreaterThan(Integer value) {
            addCriterion("admire_acount >", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("admire_acount >=", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountLessThan(Integer value) {
            addCriterion("admire_acount <", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountLessThanOrEqualTo(Integer value) {
            addCriterion("admire_acount <=", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIn(List<Integer> values) {
            addCriterion("admire_acount in", values, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotIn(List<Integer> values) {
            addCriterion("admire_acount not in", values, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountBetween(Integer value1, Integer value2) {
            addCriterion("admire_acount between", value1, value2, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("admire_acount not between", value1, value2, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIsNull() {
            addCriterion("comment_acount is null");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIsNotNull() {
            addCriterion("comment_acount is not null");
            return (Criteria) this;
        }

        public Criteria andCommentAcountEqualTo(Integer value) {
            addCriterion("comment_acount =", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotEqualTo(Integer value) {
            addCriterion("comment_acount <>", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountGreaterThan(Integer value) {
            addCriterion("comment_acount >", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("comment_acount >=", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountLessThan(Integer value) {
            addCriterion("comment_acount <", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountLessThanOrEqualTo(Integer value) {
            addCriterion("comment_acount <=", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIn(List<Integer> values) {
            addCriterion("comment_acount in", values, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotIn(List<Integer> values) {
            addCriterion("comment_acount not in", values, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountBetween(Integer value1, Integer value2) {
            addCriterion("comment_acount between", value1, value2, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("comment_acount not between", value1, value2, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountIsNull() {
            addCriterion("read_acount is null");
            return (Criteria) this;
        }

        public Criteria andReadAcountIsNotNull() {
            addCriterion("read_acount is not null");
            return (Criteria) this;
        }

        public Criteria andReadAcountEqualTo(Integer value) {
            addCriterion("read_acount =", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotEqualTo(Integer value) {
            addCriterion("read_acount <>", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountGreaterThan(Integer value) {
            addCriterion("read_acount >", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("read_acount >=", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountLessThan(Integer value) {
            addCriterion("read_acount <", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountLessThanOrEqualTo(Integer value) {
            addCriterion("read_acount <=", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountIn(List<Integer> values) {
            addCriterion("read_acount in", values, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotIn(List<Integer> values) {
            addCriterion("read_acount not in", values, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountBetween(Integer value1, Integer value2) {
            addCriterion("read_acount between", value1, value2, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("read_acount not between", value1, value2, "readAcount");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}