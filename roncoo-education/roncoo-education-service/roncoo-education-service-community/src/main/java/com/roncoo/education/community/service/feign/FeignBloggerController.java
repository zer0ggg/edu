package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlogger;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.community.feign.vo.BloggerVO;
import com.roncoo.education.community.service.feign.biz.FeignBloggerBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博主信息表
 *
 * @author wujing
 */
@RestController
public class FeignBloggerController extends BaseController implements IFeignBlogger {

	@Autowired
	private FeignBloggerBiz biz;

	@Override
	public Page<BloggerVO> listForPage(@RequestBody BloggerQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody BloggerQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody BloggerQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public BloggerVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public BloggerVO getByUserNo(@RequestBody BloggerQO qo) {
		return biz.getByUserNo(qo);
	}

}
