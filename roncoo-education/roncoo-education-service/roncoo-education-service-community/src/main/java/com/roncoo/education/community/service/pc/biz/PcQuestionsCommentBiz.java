package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.common.req.BlogCommentStatusDelREQ;
import com.roncoo.education.community.common.req.QuestionsCommentEditREQ;
import com.roncoo.education.community.common.req.QuestionsCommentListREQ;
import com.roncoo.education.community.common.req.QuestionsCommentSaveREQ;
import com.roncoo.education.community.common.resp.QuestionsCommentListRESP;
import com.roncoo.education.community.common.resp.QuestionsCommentViewRESP;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 问答评论
 *
 * @author wujing
 */
@Component
public class PcQuestionsCommentBiz extends BaseBiz {

    @Autowired
    private QuestionsCommentDao dao;
    @Autowired
    private QuestionsDao questionsDao;
    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 问答评论列表
     *
     * @param req 问答评论分页查询参数
     * @return 问答评论分页查询结果
     */
    public Result<Page<QuestionsCommentListRESP>> list(QuestionsCommentListREQ req) {
        QuestionsCommentExample example = new QuestionsCommentExample();
        Criteria c = example.createCriteria();
        if (req.getParentId() != null) {
            c.andParentIdEqualTo(req.getParentId());
        } else {
            c.andParentIdEqualTo(0L);
        }
        if (req.getQuestionsId() != null) {
            c.andQuestionsIdEqualTo(req.getQuestionsId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        example.setOrderByClause("sort asc, id desc ");
        Page<QuestionsComment> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<QuestionsCommentListRESP> respPage = PageUtil.transform(page, QuestionsCommentListRESP.class);
        for (QuestionsCommentListRESP resp : respPage.getList()) {
            // 获取评论用户信息
            if (StringUtils.isEmpty(resp.getNickname())) {
                UserExtVO userExtVO = feignUserExt.getByUserNo(resp.getUserNo());
                if (ObjectUtil.isNotNull(userExtVO)) {
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        resp.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        resp.setNickname(userExtVO.getNickname());
                    }
                }
            }
            // 获取被评论用户信息
            if (StringUtils.isEmpty(resp.getCommentNickname())) {
                UserExtVO commentUser = feignUserExt.getByUserNo(resp.getCommentUserNo());
                if (ObjectUtil.isNotNull(commentUser)) {
                    if (StringUtils.isEmpty(commentUser.getNickname())) {
                        resp.setCommentNickname(commentUser.getMobile().substring(0, 3) + "****" + commentUser.getMobile().substring(7));
                    } else {
                        resp.setCommentNickname(commentUser.getNickname());
                    }
                }
            }
            // 获取大字段博客内容content
            String content = getBigFieldById(resp.getId());
            resp.setContent(content);
            resp.setQuestionsCommentList(recursionList(resp.getId()));
        }

        return Result.success(respPage);
    }

    /**
     * 获取大字段博客内容content
     *
     * @param id
     * @return
     */
    private String getBigFieldById(Long id) {
        // 获取大字段博客内容content
        QuestionsComment questionsComment = dao.getById(id);
        // 截取评论摘要
        String content = questionsComment.getContent();
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        return content;
    }

    /**
     * 递归展示分类
     */
    private List<QuestionsCommentListRESP> recursionList(Long parentId) {
        List<QuestionsComment> questionsCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        List<QuestionsCommentListRESP> list = ArrayListUtil.copy(questionsCommentList, QuestionsCommentListRESP.class);
        if (CollectionUtils.isNotEmpty(questionsCommentList)) {
            for (QuestionsCommentListRESP resp : list) {
                // 获取被评论用户信息
                if (StringUtils.isEmpty(resp.getNickname())) {
                    UserExtVO commentUser = feignUserExt.getByUserNo(resp.getCommentUserNo());
                    if (ObjectUtil.isNotNull(commentUser)) {
                        if (StringUtils.isEmpty(commentUser.getNickname())) {
                            resp.setCommentNickname(commentUser.getMobile().substring(0, 3) + "****" + commentUser.getMobile().substring(7));
                        } else {
                            resp.setCommentNickname(commentUser.getNickname());
                        }
                    }
                }
                // 获取评论用户信息
                if (StringUtils.isEmpty(resp.getCommentNickname())) {
                    UserExtVO userExtVO = feignUserExt.getByUserNo(resp.getUserNo());
                    if (ObjectUtil.isNotNull(userExtVO)) {
                        if (StringUtils.isEmpty(userExtVO.getNickname())) {
                            resp.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                        } else {
                            resp.setNickname(userExtVO.getNickname());
                        }
                    }
                }

                // 获取大字段博客内容content
                String content = getBigFieldById(resp.getId());
                resp.setContent(content);
                resp.setQuestionsCommentList(recursionList(resp.getId()));
            }
        }
        return list;
    }


    /**
     * 问答评论添加
     *
     * @param req 问答评论
     * @return 添加结果
     */
    public Result<String> save(QuestionsCommentSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        // 根据博客ID查询博客信息
        Questions questions = questionsDao.getById(req.getQuestionsId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("问答信息不存在");
        }
        //被评论者信息
        Long commentUserNo;
        String commentNickname;
        if (req.getParentId() != null && req.getParentId() != 0) {
            // 评论某一条评论
            QuestionsComment pQuestionsComment = dao.getById(req.getParentId());
            if (ObjectUtils.isEmpty(pQuestionsComment)) {
                return Result.error("父id错误");
            }
            commentUserNo = pQuestionsComment.getUserNo();
            commentNickname = pQuestionsComment.getNickname();
        } else {
            // 直接评论博主
            commentUserNo = questions.getUserNo();
            UserExtVO bloggerUser = feignUserExt.getByUserNo(commentUserNo);
            commentNickname = bloggerUser.getMobile().substring(0, 3) + "****" + bloggerUser.getMobile().substring(7);

        }
        // 添加评论
        QuestionsComment comment = new QuestionsComment();
        comment.setParentId(req.getParentId());
        comment.setQuestionsId(req.getQuestionsId());
        comment.setTitle(questions.getTitle());
        comment.setCommentUserNo(commentUserNo);
        comment.setUserNo(req.getUserNo());
        comment.setNickname(req.getNickname());
        comment.setCommentNickname(commentNickname);
        comment.setContent(req.getContent());
        // 默认值
//        comment.setUserIp(req.getUserIp());
        comment.setUserTerminal(req.getUserTerminal());
        int resultNum = dao.save(comment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (req.getParentId() == 0) {
                // 评论人数+1
                questions.setCommentAcount(questions.getCommentAcount() + 1);
                // 质量度+5
                questions.setQualityScore(questions.getQualityScore() + 5);
                questionsDao.updateById(questions);
            }
        }
        if (resultNum > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 问答评论查看
     *
     * @param id 主键ID
     * @return 问答评论
     */
    public Result<QuestionsCommentViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), QuestionsCommentViewRESP.class));
    }


    /**
     * 问答评论修改
     *
     * @param req 问答评论修改对象
     * @return 修改结果
     */
    public Result<String> edit(QuestionsCommentEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        QuestionsComment record = BeanUtil.copyProperties(req, QuestionsComment.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }


    /**
     * 状态删除
     *
     * @param req 状态删除参数
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> statusDel(BlogCommentStatusDelREQ req) {
        // 校验博客评论是否存在
        QuestionsComment comment = dao.getById(req.getId());
        if (ObjectUtils.isEmpty(comment)) {
            throw new BaseException("找不到评论的信息");
        }
        // 查询博客信息
        Questions questions = questionsDao.getById(comment.getQuestionsId());
        if (ObjectUtils.isEmpty(questions)) {
            throw new BaseException("问答信息不存在");
        }

        comment.setStatusId(Constants.FREEZE);
        int resultNum = dao.updateById(comment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (req.getParentId() == 0) {
                if (questions.getCommentAcount() <= 0) {
                    throw new BaseException("更新评论人数失败，评论人数为" + questions.getCommentAcount());
                }
                // 评论人数-1
                questions.setCommentAcount(questions.getCommentAcount() - 1);
                // 质量度-5
                questions.setQualityScore(questions.getQualityScore() - 5);
                questionsDao.updateById(questions);

                // 删除该一级评论下的所有二级评论
                List<QuestionsComment> secondCommentList = dao.listByParentIdAndStatusId(req.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isEmpty(secondCommentList)) {
                    return Result.success("删除成功");
                }
                for (QuestionsComment secondComment : secondCommentList) {
                    secondComment.setStatusId(StatusIdEnum.NO.getCode());
                    dao.updateById(secondComment);
                }
            }
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 问答评论删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
