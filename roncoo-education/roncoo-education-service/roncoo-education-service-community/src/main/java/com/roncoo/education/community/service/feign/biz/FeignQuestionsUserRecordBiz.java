package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.QuestionsUserRecordQO;
import com.roncoo.education.community.feign.vo.QuestionsUserRecordVO;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.QuestionsUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 问题与用户关联表
 *
 * @author wujing
 */
@Component
public class FeignQuestionsUserRecordBiz {
	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private QuestionsDao questionsDao;
	@Autowired
	private QuestionsUserRecordDao dao;

	public Page<QuestionsUserRecordVO> listForPage(QuestionsUserRecordQO qo) {
		QuestionsUserRecordExample example = new QuestionsUserRecordExample();
		example.setOrderByClause(" id desc ");
		Page<QuestionsUserRecord> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<QuestionsUserRecordVO> listForPage = PageUtil.transform(page, QuestionsUserRecordVO.class);
		for (QuestionsUserRecordVO vo : listForPage.getList()) {
			// 获取问题信息
			Questions questions = questionsDao.getById(vo.getQuestionsId());
			if (ObjectUtil.isNotNull(questions)) {
				vo.setTitle(questions.getTitle());
			}
			UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
			if (ObjectUtil.isNotNull(userExtVO)) {
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					vo.setNickname(userExtVO.getNickname());
				}
			}
		}
		return listForPage;
	}

	public int save(QuestionsUserRecordQO qo) {
		QuestionsUserRecord record = BeanUtil.copyProperties(qo, QuestionsUserRecord.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public QuestionsUserRecordVO getById(Long id) {
		QuestionsUserRecord record = dao.getById(id);
		return BeanUtil.copyProperties(record, QuestionsUserRecordVO.class);
	}

	public int updateById(QuestionsUserRecordQO qo) {
		QuestionsUserRecord record = BeanUtil.copyProperties(qo, QuestionsUserRecord.class);
		return dao.updateById(record);
	}

}
