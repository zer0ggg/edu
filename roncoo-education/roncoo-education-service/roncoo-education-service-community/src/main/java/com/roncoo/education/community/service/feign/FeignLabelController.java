package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignLabel;
import com.roncoo.education.community.feign.qo.LabelQO;
import com.roncoo.education.community.feign.vo.LabelVO;
import com.roncoo.education.community.service.feign.biz.FeignLabelBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 标签
 *
 * @author wujing
 */
@RestController
public class FeignLabelController extends BaseController implements IFeignLabel {

	@Autowired
	private FeignLabelBiz biz;

	@Override
	public Page<LabelVO> listForPage(@RequestBody LabelQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody LabelQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody LabelQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public LabelVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public List<LabelVO> listByAllAndLabelType(@RequestBody LabelQO qo) {
		return biz.listByAllAndLabelType(qo);
	}

}
