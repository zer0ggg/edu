package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.dfa.SensitiveUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserAttentionPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserViewDTO;
import com.roncoo.education.community.common.es.EsBlog;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BlogUserRecordDao;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.*;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample.Criteria;
import com.roncoo.education.system.feign.interfaces.IFeignSensitiveWordLibrary;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 博客信息表
 *
 * @author wujing
 */
@Component
@Slf4j
public class AuthBlogBiz extends BaseBiz {

    @Autowired
    private BlogDao dao;
    @Autowired
    private BloggerDao bloggerDao;
    @Autowired
    private BlogUserRecordDao blogUserRecordDao;
    @Autowired
    private BloggerUserRecordDao bloggerUserRecordDao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignSensitiveWordLibrary feignSensitiveWordLibrary;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthBlogUserSaveBO bo) {
        if (StringUtils.isEmpty(bo.getTitle())) {
            return Result.error("请输入博客标题");
        }
        if (StringUtils.isEmpty(bo.getTagsName())) {
            return Result.error("tagsName不能为空");
        }
        if (StringUtils.isEmpty(bo.getContent())) {
            return Result.error("请输入博客内容");
        }
        if (bo.getIsOpen() == null) {
            return Result.error("请选择是否公开博客");
        }
        if (bo.getIsIssue() == null) {
            return Result.error("请选择是否已经发布");
        }
        if (bo.getTypeId() == null) {
            return Result.error("请选择博客类型");
        }

        List<String> list = myRedisTemplate.list(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode(), String.class);
        if (CollUtil.isEmpty(list)) {
            list = feignSensitiveWordLibrary.listAll();
            // 存一天
            myRedisTemplate.setByJson(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode(), list, 1, TimeUnit.DAYS);
        }

        //校验敏感词
        SensitiveUtil.init(list);
        String title = SensitiveUtil.getFindedFirstSensitive(bo.getTitle());
        if (!StringUtils.isEmpty(title)) {
            return Result.error("存在敏感词{" + title + "}");
        }
        String content = SensitiveUtil.getFindedFirstSensitive(bo.getContent());
        if (!StringUtils.isEmpty(content)) {
            return Result.error("存在敏感词{" + content + "}");
        }

        // 根据用户编号查询博主信息
        Blogger blogger = bloggerDao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("博主信息不存在");
        }

        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(blogger.getUserNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户异常");
        }

        Blog record = BeanUtil.copyProperties(bo, Blog.class);
        record.setUserNo(blogger.getUserNo());
        // 获取大字段博客内容content进行截取
        String summary = getBigField(record.getContent());
        record.setSummary(summary + "...");
        record.setId(IdWorker.getId());
        // 添加博客信息
        dao.save(record);

        // 更新博主信息博客数量
        if (ArticleTypeEnum.BLOG.getCode().equals(bo.getArticleType())) {
            // 更新博主信息博客数量
            blogger.setWeblogAccount(blogger.getWeblogAccount() + 1);
        }
        if (ArticleTypeEnum.INFORMATION.getCode().equals(bo.getArticleType())) {
            // 更新博主信息资讯数量
            blogger.setInformationAccount(blogger.getInformationAccount() + 1);
        }
        int resultNum = bloggerDao.updateById(blogger);
        if (resultNum > 0) {
            Blog blog = dao.getById(record.getId());
            // 只有状态正常、已发布、公开的博客才添加进ES
            if (blog.getStatusId().equals(StatusIdEnum.YES.getCode()) && blog.getIsIssue().equals(IsIssueEnum.YES.getCode()) && blog.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                try {
                    // 插入els或者更新els
                    EsBlog esBlog = BeanUtil.copyProperties(blog, EsBlog.class);
                    esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esBlog.setBloggerNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                    throw new BaseException("添加失败");
                }
            }
            return Result.success(resultNum);
        }
        return Result.error("添加失败");
    }

    public Result<Integer> delete(AuthBlogUserDeleteBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Blog blog = dao.getById(bo.getId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("找不到博客信息");
        }
        // 根据用户编号查询博主信息
        Blogger blogger = bloggerDao.getByBloggerUserNo(blog.getUserNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("博主信息不存在");
        }

        dao.deleteById(bo.getId());
        // 更新博主信息博客数量
        if (ArticleTypeEnum.BLOG.getCode().equals(blog.getArticleType())) {
            // 更新博主信息博客数量
            blogger.setWeblogAccount(blogger.getWeblogAccount() - 1);
        }
        if (ArticleTypeEnum.INFORMATION.getCode().equals(blog.getArticleType())) {
            // 更新博主信息资讯数量
            blogger.setInformationAccount(blogger.getInformationAccount() - 1);
        }
        int results = bloggerDao.updateById(blogger);
        if (results < 0) {
            return Result.error(ResultEnum.COMMUNITY_DELETE_FAIL);
        }
        return Result.success(results);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> update(AuthBlogUserUpdateBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Blog blog = dao.getById(bo.getId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("id不正确");
        }
        if (!blog.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("传入的useNo与该博客的博主useNo不一致");
        }
        //校验敏感词
        SensitiveUtil.init(feignSensitiveWordLibrary.listAll());
        String title = SensitiveUtil.getFindedFirstSensitive(bo.getTitle());
        if (!StringUtils.isEmpty(title)) {
            return Result.error("存在敏感词{" + title + "}");
        }
        String content = SensitiveUtil.getFindedFirstSensitive(bo.getContent());
        if (!StringUtils.isEmpty(content)) {
            return Result.error("存在敏感词{" + content + "}");
        }

        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(blog.getUserNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户异常");
        }

        Blog record = BeanUtil.copyProperties(bo, Blog.class);
        record.setUserNo(blog.getUserNo());
        if (!StringUtils.isEmpty(record.getContent())) {
            record.setStatusId(StatusIdEnum.YES.getCode());
            // 获取大字段博客内容content进行截取
            String summary = getBigField(record.getContent());
            record.setSummary(summary + "...");
        }
        if (StringUtils.isEmpty(bo.getMdContet())) {
            record.setMdContet("");
        }
        int resultNum = dao.updateById(record);

        if (resultNum > 0) {
            // 只有状态正常、已发布、公开的博客才添加进ES
            if (blog.getStatusId().equals(StatusIdEnum.YES.getCode()) && blog.getIsIssue().equals(IsIssueEnum.YES.getCode()) && blog.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                try {
                    // 插入els或者更新els
                    EsBlog esBlog = BeanUtil.copyProperties(blog, EsBlog.class);
                    esBlog.setBloggerUserImg(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esBlog.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        esBlog.setBloggerNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esBlog).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsBlog.BLOG));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                    throw new BaseException("更新失败");
                }
            }
            return Result.success(resultNum);
        }
        return Result.error("更新失败");
    }

    public Result<Page<AuthBlogUserPageDTO>> list(AuthBlogUserPageBO bo) {
        BlogExample example = new BlogExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        if (bo.getTagTypes() != null) {
            // 1公开
            if (TagTypesEnum.PUBLICITY.getCode().equals(bo.getTagTypes())) {
                c.andIsIssueEqualTo(IsIssueEnum.YES.getCode());
                c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
                c.andIsOpenEqualTo(IsOpenEnum.YES.getCode());
            }
            // 2私密
            else if (TagTypesEnum.PRIVACY.getCode().equals(bo.getTagTypes())) {
                c.andIsIssueEqualTo(IsIssueEnum.YES.getCode());
                c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
                c.andIsOpenEqualTo(IsOpenEnum.NO.getCode());
            }
            // 3草稿
            else if (TagTypesEnum.DRAFT.getCode().equals(bo.getTagTypes())) {
                c.andIsIssueEqualTo(IsIssueEnum.NO.getCode());
                c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
            }
            // 4回收站
            else if (TagTypesEnum.TRASH.getCode().equals(bo.getTagTypes())) {
                c.andIsIssueEqualTo(IsIssueEnum.NO.getCode());
                c.andStatusIdEqualTo(StatusIdEnum.NO.getCode());
            }
        } else {
            c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        }
        if (!StringUtils.isEmpty(bo.getTitle())) {
            c.andTitleLike(PageUtil.rightLike(bo.getTitle()));
        }
        c.andArticleTypeEqualTo(ArticleTypeEnum.BLOG.getCode());
        example.setOrderByClause(" status_id desc, is_top desc, sort asc, id desc ");
        Page<Blog> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBlogUserPageDTO> dtoList = PageUtil.transform(page, AuthBlogUserPageDTO.class);
        for (AuthBlogUserPageDTO dto : dtoList.getList()) {
            // 发布状态并且状态为正常
            if (dto.getIsIssue().equals(IsIssueEnum.YES.getCode()) && dto.getStatusId().equals(StatusIdEnum.YES.getCode())) {
                // 博客公开或者私密；博客类型标签(1公开，2私密，3草稿，4回收站)
                if (dto.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                    // 1公开
                    dto.setTagTypes(TagTypesEnum.PUBLICITY.getCode());
                } else {
                    // 2私密
                    dto.setTagTypes(TagTypesEnum.PRIVACY.getCode());
                }
                // 未发布状态
            } else if (dto.getIsIssue().equals(IsIssueEnum.NO.getCode())) {
                if (dto.getStatusId().equals(StatusIdEnum.YES.getCode())) {
                    // 3草稿
                    dto.setTagTypes(TagTypesEnum.DRAFT.getCode());
                } else {
                    // 4回收站
                    dto.setTagTypes(TagTypesEnum.TRASH.getCode());
                }
            }
        }
        return Result.success(dtoList);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> stand(AuthBlogUserStandBO bo) {
        Blog blog = dao.getById(bo.getId());
        if (ObjectUtil.isNull(blog)) {
            return Result.error("id不正确");
        }
        if (!blog.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("传入的useNo与该博客的博主useNo不一致");
        }
        Blog record = BeanUtil.copyProperties(bo, Blog.class);
        record.setUserNo(blog.getUserNo());
        record.setStatusId(StatusIdEnum.NO.getCode());
        record.setIsIssue(IsIssueEnum.NO.getCode());
        int resultNum = dao.updateById(record);
        if (resultNum > 0) {
            // 只有状态正常、已发布、公开的博客才添加进ES
            if (blog.getStatusId().equals(StatusIdEnum.YES.getCode()) && blog.getIsIssue().equals(IsIssueEnum.YES.getCode()) && blog.getIsOpen().equals(IsOpenEnum.YES.getCode())) {
                // 删除els数据
                elasticsearchRestTemplate.delete(bo.getId().toString(), EsBlog.class);
            }
            return Result.success(resultNum);
        }
        return Result.error(ResultEnum.COMMUNITY_UPDATE_FAIL);
    }

    public Result<AuthBlogUserViewDTO> view(AuthBlogUserViewBO bo) {
        Blog blog = dao.getById(bo.getId());
        if (ObjectUtils.isEmpty(blog)) {
            return Result.error("找不到博客信息");
        }

        // 更新博客信息
        // 阅读人数+1
        blog.setReadAcount(blog.getReadAcount() + 1);
        // 质量度+1
        blog.setQualityScore(blog.getQualityScore() + 1);
        AuthBlogUserViewDTO dto = BeanUtil.copyProperties(blog, AuthBlogUserViewDTO.class);

        if (ArticleTypeEnum.BLOG.getCode().equals(blog.getArticleType())) {
            dto.setBloggerUserNo(blog.getUserNo());
            // 查询博主是否存在
            Blogger blogger = bloggerDao.getByBloggerUserNo(blog.getUserNo());
            if (ObjectUtil.isNull(blogger)) {
                return Result.error("找不到博主信息");
            }
            UserExtVO userExtVO = feignUserExt.getByUserNo(blogger.getUserNo());
            if (ObjectUtils.isEmpty(userExtVO)) {
                dto.setBloggerStatusId(StatusIdEnum.NO.getCode());
            } else {
                dto.setBloggerNickname(userExtVO.getNickname());
                dto.setBloggerHeadImgUrl(userExtVO.getHeadImgUrl());
            }
        }

        // 1、查询用户是否关注博客
        BlogUserRecord blogUserRecord = blogUserRecordDao.getByUserNoAndBlogIdAndOpType(blog.getUserNo(), bo.getId(), OpTypeEnum.COLLECTION.getCode());
        if (ObjectUtil.isNull(blogUserRecord)) {
            // 未关注
            dto.setIsCollection(0);
        } else {
            // 已关注
            dto.setIsCollection(1);
        }
        // 2、查询用户是否点赞博客
        BlogUserRecord record = blogUserRecordDao.getByUserNoAndBlogIdAndOpType(blog.getUserNo(), bo.getId(), OpTypeEnum.PRAISE.getCode());
        if (ObjectUtil.isNull(record)) {
            // 未点赞
            dto.setIsAdmire(0);
        } else {
            // 已点赞
            dto.setIsAdmire(1);
        }

        // 更新博客信息
        Blog blogUpdate = new Blog();
        blogUpdate.setId(blog.getId());
        // 阅读人数+1
        blogUpdate.setReadAcount(blog.getReadAcount());
        // 质量度+1
        blogUpdate.setQualityScore(blog.getQualityScore());
        dao.updateById(blogUpdate);
        return Result.success(dto);
    }

    public Result<Page<AuthBlogUserAttentionPageDTO>> attentionList(AuthBlogUserAttentionPageBO bo) {
        // 查询关注的博主有哪些
        List<BloggerUserRecord> attentionList = bloggerUserRecordDao.listByUserNoAndStatusId(ThreadContext.userNo(), StatusIdEnum.YES.getCode());
        if (CollectionUtils.isEmpty(attentionList)) {
            // 使其返回空的page
            Page<AuthBlogUserAttentionPageDTO> dto = new Page<>();
            return Result.success(dto);
        }
        // 关注的博主的用户编号集合
        List<Long> userNos = new ArrayList<>();
        for (BloggerUserRecord bloggerUserAttention : attentionList) {
            userNos.add(bloggerUserAttention.getBloggerUserNo());
        }
        // 查询关注的博主的博客信息列表
        Page<Blog> page = dao.listByWeblogUserAttention(userNos, bo.getPageCurrent(), bo.getPageSize());
        Page<AuthBlogUserAttentionPageDTO> dto = PageUtil.transform(page, AuthBlogUserAttentionPageDTO.class);
        return Result.success(dto);
    }

    private String getBigField(String content) {
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        // 截取前100字符串
        content = StrUtil.subStrStart(content, 100);
        return content;
    }

}
