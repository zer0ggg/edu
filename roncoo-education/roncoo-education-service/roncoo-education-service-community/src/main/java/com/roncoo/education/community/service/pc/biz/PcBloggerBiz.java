package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.common.req.BloggerBatchSaveREQ;
import com.roncoo.education.community.common.req.BloggerEditREQ;
import com.roncoo.education.community.common.req.BloggerListREQ;
import com.roncoo.education.community.common.req.BloggerSaveREQ;
import com.roncoo.education.community.common.resp.BloggerListRESP;
import com.roncoo.education.community.common.resp.BloggerViewRESP;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 博主信息
 *
 * @author wujing
 */
@Component
public class PcBloggerBiz extends BaseBiz {

    @Autowired
    private BloggerDao dao;

    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 博主信息列表
     *
     * @param bloggerListREQ 博主信息分页查询参数
     * @return 博主信息分页查询结果
     */
    public Result<Page<BloggerListRESP>> list(BloggerListREQ bloggerListREQ) {
        BloggerExample example = new BloggerExample();
        Criteria c = example.createCriteria();
        if (bloggerListREQ.getMobile() != null) {
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setMobile(bloggerListREQ.getMobile());
            UserExtVO userExt = feignUserExt.getByMobile(userExtQO);
            if (ObjectUtil.isNotNull(userExt)) {
                c.andUserNoEqualTo(userExt.getUserNo());
            }else {
                //查无用户，使用不存在的编号查询，保证查不回数据
                c.andUserNoEqualTo(0L);
            }
        }
        if (bloggerListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(bloggerListREQ.getStatusId());
        }else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        example.setOrderByClause("sort asc, id desc");
        Page<Blogger> page = dao.listForPage(bloggerListREQ.getPageCurrent(), bloggerListREQ.getPageSize(), example);
        Page<BloggerListRESP> respPage = PageUtil.transform(page, BloggerListRESP.class);
        for (BloggerListRESP vo : respPage.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                vo.setMobile(userExtVO.getMobile());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 博主信息添加
     *
     * @param bloggerSaveREQ 博主信息
     * @return 添加结果
     */
    public Result<String> save(BloggerSaveREQ bloggerSaveREQ) {
        Blogger blogger = dao.getByBloggerUserNo(bloggerSaveREQ.getUserNo());
        if (ObjectUtil.isNotNull(blogger)) {
            return Result.error("博主已存在");
        }
        Blogger record = BeanUtil.copyProperties(bloggerSaveREQ, Blogger.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> batchSave(BloggerBatchSaveREQ bloggerBatchSaveREQ) {
        if (StrUtil.isBlank(bloggerBatchSaveREQ.getUserNos())) {
            return Result.error("添加的博主不能为空");
        }

        String[] userNoList = bloggerBatchSaveREQ.getUserNos().split(",");
        for (String userNo : userNoList) {
            UserExtVO userEducationInfoVO = feignUserExt.getByUserNo(Long.valueOf(userNo));
            Blogger blogger = dao.getByBloggerUserNo(userEducationInfoVO.getUserNo());
            if (ObjectUtil.isNotNull(blogger)) {
                throw new BaseException("博主已存在");
            }
            Blogger record = new Blogger();
            record.setUserNo(userEducationInfoVO.getUserNo());
            dao.save(record);
        }
        return Result.success("添加成功");
    }


    /**
     * 博主信息查看
     *
     * @param id 主键ID
     * @return 博主信息
     */
    public Result<BloggerViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), BloggerViewRESP.class));
    }


    /**
     * 博主信息修改
     *
     * @param bloggerEditREQ 博主信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(BloggerEditREQ bloggerEditREQ) {
        Blogger record = BeanUtil.copyProperties(bloggerEditREQ, Blogger.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 博主信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<BloggerViewRESP> getByUserNo(Long userNo) {
        Blogger record = dao.getByBloggerUserNo(userNo);
        if (ObjectUtil.isNull(record)) {
            return Result.error("找不到博主信息");
        }
        BloggerViewRESP resp = BeanUtil.copyProperties(record, BloggerViewRESP.class);
        UserExtVO userExtVO = feignUserExt.getByUserNo(record.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("找不到用户信息");
        }
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            resp.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
        } else {
            resp.setNickname(userExtVO.getNickname());
        }
        if (StringUtils.hasText(userExtVO.getHeadImgUrl())) {
            resp.setHeadImgUrl(userExtVO.getHeadImgUrl());
        }
        resp.setMobile(userExtVO.getMobile());
        return Result.success(resp);
    }
}
