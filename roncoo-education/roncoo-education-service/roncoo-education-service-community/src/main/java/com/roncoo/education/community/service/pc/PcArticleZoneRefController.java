package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.ArticleZoneRefListRESP;
import com.roncoo.education.community.common.resp.ArticleZoneRefViewRESP;
import com.roncoo.education.community.service.pc.biz.PcArticleZoneRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文章专区关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/article/zone/ref")
@Api(value = "community-文章专区关联", tags = {"community-文章专区关联"})
public class PcArticleZoneRefController {

    @Autowired
    private PcArticleZoneRefBiz biz;

    @ApiOperation(value = "分页", notes = "分页")
    @PostMapping(value = "/list")
    public Result<Page<ArticleZoneRefListRESP>> list(@RequestBody ArticleZoneRefListREQ articleZoneRefListREQ) {
        return biz.list(articleZoneRefListREQ);
    }


    @ApiOperation(value = "文章专区关联添加", notes = "文章专区关联添加")
    @SysLog(value = "文章专区关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ArticleZoneRefSaveREQ articleZoneRefSaveREQ) {
        return biz.save(articleZoneRefSaveREQ);
    }


    @ApiOperation(value = "批量关联", notes = "批量关联")
    @SysLog(value = "文章专区关联批量关联")
    @PostMapping(value = "/save/batch")
    public Result<String> saveForBatch(@RequestBody ArticleZoneRefSaveBatchREQ req) {
        return biz.saveBatch(req);
    }

    @ApiOperation(value = "文章专区关联查看", notes = "文章专区关联查看")
    @GetMapping(value = "/view")
    public Result<ArticleZoneRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "文章专区关联修改", notes = "文章专区关联修改")
    @SysLog(value = "文章专区关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ArticleZoneRefEditREQ articleZoneRefEditREQ) {
        return biz.edit(articleZoneRefEditREQ);
    }


    @ApiOperation(value = "文章专区关联删除", notes = "文章专区关联删除")
    @SysLog(value = "文章专区关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "状态修改", notes = "状态修改")
    @SysLog(value = "文章专区关联状态修改")
    @PutMapping(value = "/updateStatusId")
    public Result<String> updateStatusId(@RequestBody ArticleZoneRefUpdateStatusIdREQ req) {
        return biz.updateStatusId(req);
    }
}
