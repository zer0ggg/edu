package com.roncoo.education.community.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.QuestionsMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionsDaoImpl implements QuestionsDao {
	@Autowired
	private QuestionsMapper questionsMapper;

	@Override
	public int save(Questions record) {
		record.setId(IdWorker.getId());
		return this.questionsMapper.insertSelective(record);
	}

	@Override
	public int deleteById(Long id) {
		return this.questionsMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateById(Questions record) {
		return this.questionsMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByExampleSelective(Questions record, QuestionsExample example) {
		return this.questionsMapper.updateByExampleSelective(record, example);
	}

	@Override
	public Questions getById(Long id) {
		return this.questionsMapper.selectByPrimaryKey(id);
	}

	@Override
	public Page<Questions> listForPage(int pageCurrent, int pageSize, QuestionsExample example) {
		int count = this.questionsMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<Questions>(count, totalPage, pageCurrent, pageSize, this.questionsMapper.selectByExample(example));
	}

	@Override
	public List<Questions> listByUserNo(Long userNo) {
		QuestionsExample example = new QuestionsExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		return this.questionsMapper.selectByExample(example);
	}

	@Override
	public List<Questions> listByStatusId(Integer code) {
		QuestionsExample example = new QuestionsExample();
		Criteria criteria = example.createCriteria();
		criteria.andStatusIdEqualTo(code);
		return this.questionsMapper.selectByExample(example);
	}
}
