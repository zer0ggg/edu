package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.impl.mapper.ArticleZoneRefMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ArticleZoneRefDaoImpl implements ArticleZoneRefDao {
	@Autowired
	private ArticleZoneRefMapper articleZoneRefMapper;

	@Override
    public int save(ArticleZoneRef record) {
		record.setId(IdWorker.getId());
		return this.articleZoneRefMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.articleZoneRefMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(ArticleZoneRef record) {
		return this.articleZoneRefMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(ArticleZoneRef record, ArticleZoneRefExample example) {
		return this.articleZoneRefMapper.updateByExampleSelective(record, example);
	}

	@Override
    public ArticleZoneRef getById(Long id) {
		return this.articleZoneRefMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<ArticleZoneRef> listForPage(int pageCurrent, int pageSize, ArticleZoneRefExample example) {
		int count = this.articleZoneRefMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<ArticleZoneRef>(count, totalPage, pageCurrent, pageSize, this.articleZoneRefMapper.selectByExample(example));
	}

	@Override
	public ArticleZoneRef getByArticleZoneIdAndArticleIdAndPlatShow(Long articleZoneId, Long articleId, Integer platShow) {
		ArticleZoneRefExample example = new ArticleZoneRefExample();
		Criteria criteria = example.createCriteria();
		criteria.andArticleZoneIdEqualTo(articleZoneId);
		criteria.andArticleIdEqualTo(articleId);
		criteria.andPlatShowEqualTo(platShow);
		List<ArticleZoneRef> list = this.articleZoneRefMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<ArticleZoneRef> listByArticleZoneIdAndAtatusId(Long articleZoneId, Integer statusId) {
		ArticleZoneRefExample example = new ArticleZoneRefExample();
		Criteria criteria = example.createCriteria();
		criteria.andArticleZoneIdEqualTo(articleZoneId);
		criteria.andStatusIdEqualTo(statusId);
		example.setOrderByClause("sort asc, id desc ");
		return this.articleZoneRefMapper.selectByExample(example);
	}
}
