package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BloggerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public BloggerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNull() {
            addCriterion("introduction is null");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNotNull() {
            addCriterion("introduction is not null");
            return (Criteria) this;
        }

        public Criteria andIntroductionEqualTo(String value) {
            addCriterion("introduction =", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotEqualTo(String value) {
            addCriterion("introduction <>", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThan(String value) {
            addCriterion("introduction >", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThanOrEqualTo(String value) {
            addCriterion("introduction >=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThan(String value) {
            addCriterion("introduction <", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThanOrEqualTo(String value) {
            addCriterion("introduction <=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLike(String value) {
            addCriterion("introduction like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotLike(String value) {
            addCriterion("introduction not like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionIn(List<String> values) {
            addCriterion("introduction in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotIn(List<String> values) {
            addCriterion("introduction not in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionBetween(String value1, String value2) {
            addCriterion("introduction between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotBetween(String value1, String value2) {
            addCriterion("introduction not between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andFansAccountIsNull() {
            addCriterion("fans_account is null");
            return (Criteria) this;
        }

        public Criteria andFansAccountIsNotNull() {
            addCriterion("fans_account is not null");
            return (Criteria) this;
        }

        public Criteria andFansAccountEqualTo(Integer value) {
            addCriterion("fans_account =", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountNotEqualTo(Integer value) {
            addCriterion("fans_account <>", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountGreaterThan(Integer value) {
            addCriterion("fans_account >", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("fans_account >=", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountLessThan(Integer value) {
            addCriterion("fans_account <", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountLessThanOrEqualTo(Integer value) {
            addCriterion("fans_account <=", value, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountIn(List<Integer> values) {
            addCriterion("fans_account in", values, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountNotIn(List<Integer> values) {
            addCriterion("fans_account not in", values, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountBetween(Integer value1, Integer value2) {
            addCriterion("fans_account between", value1, value2, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andFansAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("fans_account not between", value1, value2, "fansAccount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountIsNull() {
            addCriterion("attention_acount is null");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountIsNotNull() {
            addCriterion("attention_acount is not null");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountEqualTo(Integer value) {
            addCriterion("attention_acount =", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountNotEqualTo(Integer value) {
            addCriterion("attention_acount <>", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountGreaterThan(Integer value) {
            addCriterion("attention_acount >", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("attention_acount >=", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountLessThan(Integer value) {
            addCriterion("attention_acount <", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountLessThanOrEqualTo(Integer value) {
            addCriterion("attention_acount <=", value, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountIn(List<Integer> values) {
            addCriterion("attention_acount in", values, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountNotIn(List<Integer> values) {
            addCriterion("attention_acount not in", values, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountBetween(Integer value1, Integer value2) {
            addCriterion("attention_acount between", value1, value2, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andAttentionAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("attention_acount not between", value1, value2, "attentionAcount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountIsNull() {
            addCriterion("weblog_account is null");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountIsNotNull() {
            addCriterion("weblog_account is not null");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountEqualTo(Integer value) {
            addCriterion("weblog_account =", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountNotEqualTo(Integer value) {
            addCriterion("weblog_account <>", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountGreaterThan(Integer value) {
            addCriterion("weblog_account >", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("weblog_account >=", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountLessThan(Integer value) {
            addCriterion("weblog_account <", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountLessThanOrEqualTo(Integer value) {
            addCriterion("weblog_account <=", value, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountIn(List<Integer> values) {
            addCriterion("weblog_account in", values, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountNotIn(List<Integer> values) {
            addCriterion("weblog_account not in", values, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountBetween(Integer value1, Integer value2) {
            addCriterion("weblog_account between", value1, value2, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andWeblogAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("weblog_account not between", value1, value2, "weblogAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountIsNull() {
            addCriterion("information_account is null");
            return (Criteria) this;
        }

        public Criteria andInformationAccountIsNotNull() {
            addCriterion("information_account is not null");
            return (Criteria) this;
        }

        public Criteria andInformationAccountEqualTo(Integer value) {
            addCriterion("information_account =", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountNotEqualTo(Integer value) {
            addCriterion("information_account <>", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountGreaterThan(Integer value) {
            addCriterion("information_account >", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("information_account >=", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountLessThan(Integer value) {
            addCriterion("information_account <", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountLessThanOrEqualTo(Integer value) {
            addCriterion("information_account <=", value, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountIn(List<Integer> values) {
            addCriterion("information_account in", values, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountNotIn(List<Integer> values) {
            addCriterion("information_account not in", values, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountBetween(Integer value1, Integer value2) {
            addCriterion("information_account between", value1, value2, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andInformationAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("information_account not between", value1, value2, "informationAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountIsNull() {
            addCriterion("questions_account is null");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountIsNotNull() {
            addCriterion("questions_account is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountEqualTo(Integer value) {
            addCriterion("questions_account =", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountNotEqualTo(Integer value) {
            addCriterion("questions_account <>", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountGreaterThan(Integer value) {
            addCriterion("questions_account >", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("questions_account >=", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountLessThan(Integer value) {
            addCriterion("questions_account <", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountLessThanOrEqualTo(Integer value) {
            addCriterion("questions_account <=", value, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountIn(List<Integer> values) {
            addCriterion("questions_account in", values, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountNotIn(List<Integer> values) {
            addCriterion("questions_account not in", values, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountBetween(Integer value1, Integer value2) {
            addCriterion("questions_account between", value1, value2, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andQuestionsAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("questions_account not between", value1, value2, "questionsAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountIsNull() {
            addCriterion("answer_account is null");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountIsNotNull() {
            addCriterion("answer_account is not null");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountEqualTo(Integer value) {
            addCriterion("answer_account =", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountNotEqualTo(Integer value) {
            addCriterion("answer_account <>", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountGreaterThan(Integer value) {
            addCriterion("answer_account >", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("answer_account >=", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountLessThan(Integer value) {
            addCriterion("answer_account <", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountLessThanOrEqualTo(Integer value) {
            addCriterion("answer_account <=", value, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountIn(List<Integer> values) {
            addCriterion("answer_account in", values, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountNotIn(List<Integer> values) {
            addCriterion("answer_account not in", values, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountBetween(Integer value1, Integer value2) {
            addCriterion("answer_account between", value1, value2, "answerAccount");
            return (Criteria) this;
        }

        public Criteria andAnswerAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("answer_account not between", value1, value2, "answerAccount");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}