package com.roncoo.education.community.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 标签
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class LabelListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "标签集合")
	private List<LabelDTO> list;

}
