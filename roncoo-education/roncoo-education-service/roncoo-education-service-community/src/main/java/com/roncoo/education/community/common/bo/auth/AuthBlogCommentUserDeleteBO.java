package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客评论表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogCommentUserDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private Long parentId;
    /**
     * 博客评论ID
     */
    @ApiModelProperty(value = "博客评论ID")
    private Long id;
    /**
     * 博客ID
     */
    @ApiModelProperty(value = "博客ID")
    private Long weblogId;
}
