package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class BlogComment implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long parentId;

    private Long blogId;

    private String title;

    private Long bloggerUserNo;

    private String bloggerNickname;

    private Long userNo;

    private String nickname;

    private String userIp;

    private String userTerminal;

    private Integer articleType;

    private String content;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Long getBloggerUserNo() {
        return bloggerUserNo;
    }

    public void setBloggerUserNo(Long bloggerUserNo) {
        this.bloggerUserNo = bloggerUserNo;
    }

    public String getBloggerNickname() {
        return bloggerNickname;
    }

    public void setBloggerNickname(String bloggerNickname) {
        this.bloggerNickname = bloggerNickname == null ? null : bloggerNickname.trim();
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp == null ? null : userIp.trim();
    }

    public String getUserTerminal() {
        return userTerminal;
    }

    public void setUserTerminal(String userTerminal) {
        this.userTerminal = userTerminal == null ? null : userTerminal.trim();
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", parentId=").append(parentId);
        sb.append(", blogId=").append(blogId);
        sb.append(", title=").append(title);
        sb.append(", bloggerUserNo=").append(bloggerUserNo);
        sb.append(", bloggerNickname=").append(bloggerNickname);
        sb.append(", userNo=").append(userNo);
        sb.append(", nickname=").append(nickname);
        sb.append(", userIp=").append(userIp);
        sb.append(", userTerminal=").append(userTerminal);
        sb.append(", articleType=").append(articleType);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}