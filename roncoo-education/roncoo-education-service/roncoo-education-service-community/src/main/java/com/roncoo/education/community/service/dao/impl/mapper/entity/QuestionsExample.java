package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public QuestionsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTagsNameIsNull() {
            addCriterion("tags_name is null");
            return (Criteria) this;
        }

        public Criteria andTagsNameIsNotNull() {
            addCriterion("tags_name is not null");
            return (Criteria) this;
        }

        public Criteria andTagsNameEqualTo(String value) {
            addCriterion("tags_name =", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotEqualTo(String value) {
            addCriterion("tags_name <>", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameGreaterThan(String value) {
            addCriterion("tags_name >", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameGreaterThanOrEqualTo(String value) {
            addCriterion("tags_name >=", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLessThan(String value) {
            addCriterion("tags_name <", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLessThanOrEqualTo(String value) {
            addCriterion("tags_name <=", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameLike(String value) {
            addCriterion("tags_name like", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotLike(String value) {
            addCriterion("tags_name not like", value, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameIn(List<String> values) {
            addCriterion("tags_name in", values, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotIn(List<String> values) {
            addCriterion("tags_name not in", values, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameBetween(String value1, String value2) {
            addCriterion("tags_name between", value1, value2, "tagsName");
            return (Criteria) this;
        }

        public Criteria andTagsNameNotBetween(String value1, String value2) {
            addCriterion("tags_name not between", value1, value2, "tagsName");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNull() {
            addCriterion("keywords is null");
            return (Criteria) this;
        }

        public Criteria andKeywordsIsNotNull() {
            addCriterion("keywords is not null");
            return (Criteria) this;
        }

        public Criteria andKeywordsEqualTo(String value) {
            addCriterion("keywords =", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotEqualTo(String value) {
            addCriterion("keywords <>", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThan(String value) {
            addCriterion("keywords >", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsGreaterThanOrEqualTo(String value) {
            addCriterion("keywords >=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThan(String value) {
            addCriterion("keywords <", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLessThanOrEqualTo(String value) {
            addCriterion("keywords <=", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsLike(String value) {
            addCriterion("keywords like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotLike(String value) {
            addCriterion("keywords not like", value, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsIn(List<String> values) {
            addCriterion("keywords in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotIn(List<String> values) {
            addCriterion("keywords not in", values, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsBetween(String value1, String value2) {
            addCriterion("keywords between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andKeywordsNotBetween(String value1, String value2) {
            addCriterion("keywords not between", value1, value2, "keywords");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andSummaryIsNull() {
            addCriterion("summary is null");
            return (Criteria) this;
        }

        public Criteria andSummaryIsNotNull() {
            addCriterion("summary is not null");
            return (Criteria) this;
        }

        public Criteria andSummaryEqualTo(String value) {
            addCriterion("summary =", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotEqualTo(String value) {
            addCriterion("summary <>", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryGreaterThan(String value) {
            addCriterion("summary >", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryGreaterThanOrEqualTo(String value) {
            addCriterion("summary >=", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLessThan(String value) {
            addCriterion("summary <", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLessThanOrEqualTo(String value) {
            addCriterion("summary <=", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryLike(String value) {
            addCriterion("summary like", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotLike(String value) {
            addCriterion("summary not like", value, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryIn(List<String> values) {
            addCriterion("summary in", values, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotIn(List<String> values) {
            addCriterion("summary not in", values, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryBetween(String value1, String value2) {
            addCriterion("summary between", value1, value2, "summary");
            return (Criteria) this;
        }

        public Criteria andSummaryNotBetween(String value1, String value2) {
            addCriterion("summary not between", value1, value2, "summary");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIsNull() {
            addCriterion("quality_score is null");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIsNotNull() {
            addCriterion("quality_score is not null");
            return (Criteria) this;
        }

        public Criteria andQualityScoreEqualTo(Integer value) {
            addCriterion("quality_score =", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotEqualTo(Integer value) {
            addCriterion("quality_score <>", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreGreaterThan(Integer value) {
            addCriterion("quality_score >", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("quality_score >=", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreLessThan(Integer value) {
            addCriterion("quality_score <", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreLessThanOrEqualTo(Integer value) {
            addCriterion("quality_score <=", value, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreIn(List<Integer> values) {
            addCriterion("quality_score in", values, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotIn(List<Integer> values) {
            addCriterion("quality_score not in", values, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreBetween(Integer value1, Integer value2) {
            addCriterion("quality_score between", value1, value2, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andQualityScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("quality_score not between", value1, value2, "qualityScore");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIsNull() {
            addCriterion("collection_acount is null");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIsNotNull() {
            addCriterion("collection_acount is not null");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountEqualTo(Integer value) {
            addCriterion("collection_acount =", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotEqualTo(Integer value) {
            addCriterion("collection_acount <>", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountGreaterThan(Integer value) {
            addCriterion("collection_acount >", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("collection_acount >=", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountLessThan(Integer value) {
            addCriterion("collection_acount <", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountLessThanOrEqualTo(Integer value) {
            addCriterion("collection_acount <=", value, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountIn(List<Integer> values) {
            addCriterion("collection_acount in", values, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotIn(List<Integer> values) {
            addCriterion("collection_acount not in", values, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountBetween(Integer value1, Integer value2) {
            addCriterion("collection_acount between", value1, value2, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andCollectionAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("collection_acount not between", value1, value2, "collectionAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIsNull() {
            addCriterion("admire_acount is null");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIsNotNull() {
            addCriterion("admire_acount is not null");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountEqualTo(Integer value) {
            addCriterion("admire_acount =", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotEqualTo(Integer value) {
            addCriterion("admire_acount <>", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountGreaterThan(Integer value) {
            addCriterion("admire_acount >", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("admire_acount >=", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountLessThan(Integer value) {
            addCriterion("admire_acount <", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountLessThanOrEqualTo(Integer value) {
            addCriterion("admire_acount <=", value, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountIn(List<Integer> values) {
            addCriterion("admire_acount in", values, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotIn(List<Integer> values) {
            addCriterion("admire_acount not in", values, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountBetween(Integer value1, Integer value2) {
            addCriterion("admire_acount between", value1, value2, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andAdmireAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("admire_acount not between", value1, value2, "admireAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIsNull() {
            addCriterion("comment_acount is null");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIsNotNull() {
            addCriterion("comment_acount is not null");
            return (Criteria) this;
        }

        public Criteria andCommentAcountEqualTo(Integer value) {
            addCriterion("comment_acount =", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotEqualTo(Integer value) {
            addCriterion("comment_acount <>", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountGreaterThan(Integer value) {
            addCriterion("comment_acount >", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("comment_acount >=", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountLessThan(Integer value) {
            addCriterion("comment_acount <", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountLessThanOrEqualTo(Integer value) {
            addCriterion("comment_acount <=", value, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountIn(List<Integer> values) {
            addCriterion("comment_acount in", values, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotIn(List<Integer> values) {
            addCriterion("comment_acount not in", values, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountBetween(Integer value1, Integer value2) {
            addCriterion("comment_acount between", value1, value2, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andCommentAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("comment_acount not between", value1, value2, "commentAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountIsNull() {
            addCriterion("read_acount is null");
            return (Criteria) this;
        }

        public Criteria andReadAcountIsNotNull() {
            addCriterion("read_acount is not null");
            return (Criteria) this;
        }

        public Criteria andReadAcountEqualTo(Integer value) {
            addCriterion("read_acount =", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotEqualTo(Integer value) {
            addCriterion("read_acount <>", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountGreaterThan(Integer value) {
            addCriterion("read_acount >", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("read_acount >=", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountLessThan(Integer value) {
            addCriterion("read_acount <", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountLessThanOrEqualTo(Integer value) {
            addCriterion("read_acount <=", value, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountIn(List<Integer> values) {
            addCriterion("read_acount in", values, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotIn(List<Integer> values) {
            addCriterion("read_acount not in", values, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountBetween(Integer value1, Integer value2) {
            addCriterion("read_acount between", value1, value2, "readAcount");
            return (Criteria) this;
        }

        public Criteria andReadAcountNotBetween(Integer value1, Integer value2) {
            addCriterion("read_acount not between", value1, value2, "readAcount");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNull() {
            addCriterion("is_top is null");
            return (Criteria) this;
        }

        public Criteria andIsTopIsNotNull() {
            addCriterion("is_top is not null");
            return (Criteria) this;
        }

        public Criteria andIsTopEqualTo(Integer value) {
            addCriterion("is_top =", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotEqualTo(Integer value) {
            addCriterion("is_top <>", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThan(Integer value) {
            addCriterion("is_top >", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_top >=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThan(Integer value) {
            addCriterion("is_top <", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopLessThanOrEqualTo(Integer value) {
            addCriterion("is_top <=", value, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopIn(List<Integer> values) {
            addCriterion("is_top in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotIn(List<Integer> values) {
            addCriterion("is_top not in", values, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopBetween(Integer value1, Integer value2) {
            addCriterion("is_top between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsTopNotBetween(Integer value1, Integer value2) {
            addCriterion("is_top not between", value1, value2, "isTop");
            return (Criteria) this;
        }

        public Criteria andIsRewardIsNull() {
            addCriterion("is_reward is null");
            return (Criteria) this;
        }

        public Criteria andIsRewardIsNotNull() {
            addCriterion("is_reward is not null");
            return (Criteria) this;
        }

        public Criteria andIsRewardEqualTo(Integer value) {
            addCriterion("is_reward =", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardNotEqualTo(Integer value) {
            addCriterion("is_reward <>", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardGreaterThan(Integer value) {
            addCriterion("is_reward >", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_reward >=", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardLessThan(Integer value) {
            addCriterion("is_reward <", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardLessThanOrEqualTo(Integer value) {
            addCriterion("is_reward <=", value, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardIn(List<Integer> values) {
            addCriterion("is_reward in", values, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardNotIn(List<Integer> values) {
            addCriterion("is_reward not in", values, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardBetween(Integer value1, Integer value2) {
            addCriterion("is_reward between", value1, value2, "isReward");
            return (Criteria) this;
        }

        public Criteria andIsRewardNotBetween(Integer value1, Integer value2) {
            addCriterion("is_reward not between", value1, value2, "isReward");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralIsNull() {
            addCriterion("reward_integral is null");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralIsNotNull() {
            addCriterion("reward_integral is not null");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralEqualTo(Integer value) {
            addCriterion("reward_integral =", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralNotEqualTo(Integer value) {
            addCriterion("reward_integral <>", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralGreaterThan(Integer value) {
            addCriterion("reward_integral >", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralGreaterThanOrEqualTo(Integer value) {
            addCriterion("reward_integral >=", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralLessThan(Integer value) {
            addCriterion("reward_integral <", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralLessThanOrEqualTo(Integer value) {
            addCriterion("reward_integral <=", value, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralIn(List<Integer> values) {
            addCriterion("reward_integral in", values, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralNotIn(List<Integer> values) {
            addCriterion("reward_integral not in", values, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralBetween(Integer value1, Integer value2) {
            addCriterion("reward_integral between", value1, value2, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andRewardIntegralNotBetween(Integer value1, Integer value2) {
            addCriterion("reward_integral not between", value1, value2, "rewardIntegral");
            return (Criteria) this;
        }

        public Criteria andIsCourseIsNull() {
            addCriterion("is_course is null");
            return (Criteria) this;
        }

        public Criteria andIsCourseIsNotNull() {
            addCriterion("is_course is not null");
            return (Criteria) this;
        }

        public Criteria andIsCourseEqualTo(Integer value) {
            addCriterion("is_course =", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseNotEqualTo(Integer value) {
            addCriterion("is_course <>", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseGreaterThan(Integer value) {
            addCriterion("is_course >", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_course >=", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseLessThan(Integer value) {
            addCriterion("is_course <", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseLessThanOrEqualTo(Integer value) {
            addCriterion("is_course <=", value, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseIn(List<Integer> values) {
            addCriterion("is_course in", values, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseNotIn(List<Integer> values) {
            addCriterion("is_course not in", values, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseBetween(Integer value1, Integer value2) {
            addCriterion("is_course between", value1, value2, "isCourse");
            return (Criteria) this;
        }

        public Criteria andIsCourseNotBetween(Integer value1, Integer value2) {
            addCriterion("is_course not between", value1, value2, "isCourse");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andIsGoodIsNull() {
            addCriterion("is_good is null");
            return (Criteria) this;
        }

        public Criteria andIsGoodIsNotNull() {
            addCriterion("is_good is not null");
            return (Criteria) this;
        }

        public Criteria andIsGoodEqualTo(Integer value) {
            addCriterion("is_good =", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodNotEqualTo(Integer value) {
            addCriterion("is_good <>", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodGreaterThan(Integer value) {
            addCriterion("is_good >", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_good >=", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodLessThan(Integer value) {
            addCriterion("is_good <", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodLessThanOrEqualTo(Integer value) {
            addCriterion("is_good <=", value, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodIn(List<Integer> values) {
            addCriterion("is_good in", values, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodNotIn(List<Integer> values) {
            addCriterion("is_good not in", values, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodBetween(Integer value1, Integer value2) {
            addCriterion("is_good between", value1, value2, "isGood");
            return (Criteria) this;
        }

        public Criteria andIsGoodNotBetween(Integer value1, Integer value2) {
            addCriterion("is_good not between", value1, value2, "isGood");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdIsNull() {
            addCriterion("good_comment_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdIsNotNull() {
            addCriterion("good_comment_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdEqualTo(Long value) {
            addCriterion("good_comment_id =", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdNotEqualTo(Long value) {
            addCriterion("good_comment_id <>", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdGreaterThan(Long value) {
            addCriterion("good_comment_id >", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdGreaterThanOrEqualTo(Long value) {
            addCriterion("good_comment_id >=", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdLessThan(Long value) {
            addCriterion("good_comment_id <", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdLessThanOrEqualTo(Long value) {
            addCriterion("good_comment_id <=", value, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdIn(List<Long> values) {
            addCriterion("good_comment_id in", values, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdNotIn(List<Long> values) {
            addCriterion("good_comment_id not in", values, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdBetween(Long value1, Long value2) {
            addCriterion("good_comment_id between", value1, value2, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodCommentIdNotBetween(Long value1, Long value2) {
            addCriterion("good_comment_id not between", value1, value2, "goodCommentId");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoIsNull() {
            addCriterion("good_user_no is null");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoIsNotNull() {
            addCriterion("good_user_no is not null");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoEqualTo(Long value) {
            addCriterion("good_user_no =", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoNotEqualTo(Long value) {
            addCriterion("good_user_no <>", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoGreaterThan(Long value) {
            addCriterion("good_user_no >", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("good_user_no >=", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoLessThan(Long value) {
            addCriterion("good_user_no <", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoLessThanOrEqualTo(Long value) {
            addCriterion("good_user_no <=", value, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoIn(List<Long> values) {
            addCriterion("good_user_no in", values, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoNotIn(List<Long> values) {
            addCriterion("good_user_no not in", values, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoBetween(Long value1, Long value2) {
            addCriterion("good_user_no between", value1, value2, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodUserNoNotBetween(Long value1, Long value2) {
            addCriterion("good_user_no not between", value1, value2, "goodUserNo");
            return (Criteria) this;
        }

        public Criteria andGoodContentIsNull() {
            addCriterion("good_content is null");
            return (Criteria) this;
        }

        public Criteria andGoodContentIsNotNull() {
            addCriterion("good_content is not null");
            return (Criteria) this;
        }

        public Criteria andGoodContentEqualTo(String value) {
            addCriterion("good_content =", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentNotEqualTo(String value) {
            addCriterion("good_content <>", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentGreaterThan(String value) {
            addCriterion("good_content >", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentGreaterThanOrEqualTo(String value) {
            addCriterion("good_content >=", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentLessThan(String value) {
            addCriterion("good_content <", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentLessThanOrEqualTo(String value) {
            addCriterion("good_content <=", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentLike(String value) {
            addCriterion("good_content like", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentNotLike(String value) {
            addCriterion("good_content not like", value, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentIn(List<String> values) {
            addCriterion("good_content in", values, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentNotIn(List<String> values) {
            addCriterion("good_content not in", values, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentBetween(String value1, String value2) {
            addCriterion("good_content between", value1, value2, "goodContent");
            return (Criteria) this;
        }

        public Criteria andGoodContentNotBetween(String value1, String value2) {
            addCriterion("good_content not between", value1, value2, "goodContent");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}