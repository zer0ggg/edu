package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class QuestionsSearchPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;
    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userNo;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String headImgUrl;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickname;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;
    /**
     * 问题标签
     */
    @ApiModelProperty(value = "问题标签")
    private String tagsName;
    /**
     * 问题关键词
     */
    @ApiModelProperty(value = "问题关键词")
    private String keywords;
    /**
     * 问题内容
     */
    @ApiModelProperty(value = "问题内容")
    private String content;
    /**
     * 问题摘要
     */
    @ApiModelProperty(value = "问题摘要")
    private String summary;
    /**
     * 质量度
     */
    @ApiModelProperty(value = "质量度")
    private Integer qualityScore;
    /**
     * 收藏人数
     */
    @ApiModelProperty(value = "收藏人数")
    private Integer collectionAcount;
    /**
     * 点赞人数
     */
    @ApiModelProperty(value = "点赞人数")
    private Integer admireAcount;
    /**
     * 评论人数
     */
    @ApiModelProperty(value = "评论人数")
    private Integer commentAcount;
    /**
     * 阅读人数
     */
    @ApiModelProperty(value = "阅读人数")
    private Integer readAcount;
    /**
     * 是否置顶(1置顶，0否)
     */
    @ApiModelProperty(value = "是否置顶(1置顶，0否)")
    private Integer isTop;
    /**
     * 是否悬赏积分(1悬赏，0不悬赏)
     */
    @ApiModelProperty(value = "是否悬赏积分(1悬赏，0不悬赏)")
    private Integer isReward;
    /**
     * 悬赏积分
     */
    @ApiModelProperty(value = "悬赏积分")
    private Integer rewardIntegral;
    /**
     * 是否关联课程(1关联，0不关联)
     */
    @ApiModelProperty(value = "是否关联课程(1关联，0不关联)")
    private Integer isCourse;
    /**
     * 课程ID
     */
    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long courseId;
    /**
     * 是否存在最佳回答(1有，0无)
     */
    @ApiModelProperty(value = "是否存在最佳回答(1有，0无)")
    private Integer isGood;
    /**
     * 最佳回答ID
     */
    @ApiModelProperty(value = "最佳回答ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodCommentId;
    /**
     * 最佳回答用户编号
     */
    @ApiModelProperty(value = "最佳回答用户编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long goodUserNo;
    /**
     * 最佳回答内容
     */
    @ApiModelProperty(value = "最佳回答内容")
    private String goodContent;
}
