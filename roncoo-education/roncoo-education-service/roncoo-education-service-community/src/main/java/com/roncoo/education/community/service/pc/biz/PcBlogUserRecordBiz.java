package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.common.req.BlogUserRecordEditREQ;
import com.roncoo.education.community.common.req.BlogUserRecordListREQ;
import com.roncoo.education.community.common.req.BlogUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.BlogUserRecordListRESP;
import com.roncoo.education.community.common.resp.BlogUserRecordViewRESP;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.BlogUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 博客与用户关联
 *
 * @author wujing
 */
@Component
public class PcBlogUserRecordBiz extends BaseBiz {

    @Autowired
    private BlogUserRecordDao dao;
    @Autowired
    private BlogDao blogDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 博客与用户关联列表
     *
     * @param blogUserRecordListREQ 博客与用户关联分页查询参数
     * @return 博客与用户关联分页查询结果
     */
    public Result<Page<BlogUserRecordListRESP>> list(BlogUserRecordListREQ blogUserRecordListREQ) {
        BlogUserRecordExample example = new BlogUserRecordExample();
        Criteria c = example.createCriteria();
        if (blogUserRecordListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(blogUserRecordListREQ.getUserNo());
        }
        if (blogUserRecordListREQ.getOpType() != null) {
            c.andOpTypeEqualTo(blogUserRecordListREQ.getOpType());
        }
        if (StringUtils.hasText(blogUserRecordListREQ.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(blogUserRecordListREQ.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(blogUserRecordListREQ.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(blogUserRecordListREQ.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" sort asc, id desc ");
        Page<BlogUserRecord> page = dao.listForPage(blogUserRecordListREQ.getPageCurrent(), blogUserRecordListREQ.getPageSize(), example);
        Page<BlogUserRecordListRESP> respPage = PageUtil.transform(page, BlogUserRecordListRESP.class);
        for (BlogUserRecordListRESP resp : respPage.getList()) {
            Blog blog = blogDao.getById(resp.getBlogId());
            if (ObjectUtil.isNull(blog)) {
                throw new BaseException("找不到博客信息");
            }
            resp.setTitle(blog.getTitle());

            UserExtVO userExtVO = feignUserExt.getByUserNo(resp.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    resp.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    resp.setNickname(userExtVO.getNickname());
                }
            }

        }
        return Result.success(respPage);
    }


    /**
     * 博客与用户关联添加
     *
     * @param blogUserRecordSaveREQ 博客与用户关联
     * @return 添加结果
     */
    public Result<String> save(BlogUserRecordSaveREQ blogUserRecordSaveREQ) {
        BlogUserRecord record = BeanUtil.copyProperties(blogUserRecordSaveREQ, BlogUserRecord.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 博客与用户关联查看
     *
     * @param id 主键ID
     * @return 博客与用户关联
     */
    public Result<BlogUserRecordViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), BlogUserRecordViewRESP.class));
    }


    /**
     * 博客与用户关联修改
     *
     * @param blogUserRecordEditREQ 博客与用户关联修改对象
     * @return 修改结果
     */
    public Result<String> edit(BlogUserRecordEditREQ blogUserRecordEditREQ) {
        BlogUserRecord record = BeanUtil.copyProperties(blogUserRecordEditREQ, BlogUserRecord.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 博客与用户关联删除
     *
     * @param id ID主键
     * @return 删除结果
     */

    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        BlogUserRecord blogUserRecord = dao.getById(id);
        if (ObjectUtil.isNull(blogUserRecord)) {
            return Result.error("ID不正确");
        }
        int resultNum = dao.deleteById(id);
        // 更新博客收藏人数和质量度
        if (resultNum > 0) {
            Blog blog = blogDao.getById(blogUserRecord.getBlogId());
            // 收藏人数-1
            blog.setCollectionAcount(blog.getCollectionAcount() - 1);
            // 质量度-1
            blog.setQualityScore(blog.getQualityScore() - 1);
            blogDao.updateById(blog);
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
