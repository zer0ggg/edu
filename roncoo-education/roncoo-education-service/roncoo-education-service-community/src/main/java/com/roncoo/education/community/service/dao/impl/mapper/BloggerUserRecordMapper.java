package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BloggerUserRecordMapper {
    int countByExample(BloggerUserRecordExample example);

    int deleteByExample(BloggerUserRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BloggerUserRecord record);

    int insertSelective(BloggerUserRecord record);

    List<BloggerUserRecord> selectByExample(BloggerUserRecordExample example);

    BloggerUserRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BloggerUserRecord record, @Param("example") BloggerUserRecordExample example);

    int updateByExample(@Param("record") BloggerUserRecord record, @Param("example") BloggerUserRecordExample example);

    int updateByPrimaryKeySelective(BloggerUserRecord record);

    int updateByPrimaryKey(BloggerUserRecord record);
}
