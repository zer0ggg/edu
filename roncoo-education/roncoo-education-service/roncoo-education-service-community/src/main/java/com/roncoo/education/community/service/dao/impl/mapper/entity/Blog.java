package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class Blog implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private Integer isImg;

    private String blogImg;

    private String title;

    private Integer articleType;

    private String summary;

    private String tagsName;

    private String content;

    private String mdContet;

    private Integer isMd;

    private Integer qualityScore;

    private String keywords;

    private Integer editMode;

    private Integer isOpen;

    private Integer isIssue;

    private Integer typeId;

    private Integer isTop;

    private Integer collectionAcount;

    private Integer admireAcount;

    private Integer commentAcount;

    private Integer readAcount;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Integer getIsImg() {
        return isImg;
    }

    public void setIsImg(Integer isImg) {
        this.isImg = isImg;
    }

    public String getBlogImg() {
        return blogImg;
    }

    public void setBlogImg(String blogImg) {
        this.blogImg = blogImg == null ? null : blogImg.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    public String getTagsName() {
        return tagsName;
    }

    public void setTagsName(String tagsName) {
        this.tagsName = tagsName == null ? null : tagsName.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getMdContet() {
        return mdContet;
    }

    public void setMdContet(String mdContet) {
        this.mdContet = mdContet == null ? null : mdContet.trim();
    }

    public Integer getIsMd() {
        return isMd;
    }

    public void setIsMd(Integer isMd) {
        this.isMd = isMd;
    }

    public Integer getQualityScore() {
        return qualityScore;
    }

    public void setQualityScore(Integer qualityScore) {
        this.qualityScore = qualityScore;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public Integer getEditMode() {
        return editMode;
    }

    public void setEditMode(Integer editMode) {
        this.editMode = editMode;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public Integer getIsIssue() {
        return isIssue;
    }

    public void setIsIssue(Integer isIssue) {
        this.isIssue = isIssue;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getCollectionAcount() {
        return collectionAcount;
    }

    public void setCollectionAcount(Integer collectionAcount) {
        this.collectionAcount = collectionAcount;
    }

    public Integer getAdmireAcount() {
        return admireAcount;
    }

    public void setAdmireAcount(Integer admireAcount) {
        this.admireAcount = admireAcount;
    }

    public Integer getCommentAcount() {
        return commentAcount;
    }

    public void setCommentAcount(Integer commentAcount) {
        this.commentAcount = commentAcount;
    }

    public Integer getReadAcount() {
        return readAcount;
    }

    public void setReadAcount(Integer readAcount) {
        this.readAcount = readAcount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", isImg=").append(isImg);
        sb.append(", blogImg=").append(blogImg);
        sb.append(", title=").append(title);
        sb.append(", articleType=").append(articleType);
        sb.append(", summary=").append(summary);
        sb.append(", tagsName=").append(tagsName);
        sb.append(", content=").append(content);
        sb.append(", mdContet=").append(mdContet);
        sb.append(", isMd=").append(isMd);
        sb.append(", qualityScore=").append(qualityScore);
        sb.append(", keywords=").append(keywords);
        sb.append(", editMode=").append(editMode);
        sb.append(", isOpen=").append(isOpen);
        sb.append(", isIssue=").append(isIssue);
        sb.append(", typeId=").append(typeId);
        sb.append(", isTop=").append(isTop);
        sb.append(", collectionAcount=").append(collectionAcount);
        sb.append(", admireAcount=").append(admireAcount);
        sb.append(", commentAcount=").append(commentAcount);
        sb.append(", readAcount=").append(readAcount);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}