package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignArticleRecommend;
import com.roncoo.education.community.feign.qo.ArticleRecommendQO;
import com.roncoo.education.community.feign.vo.ArticleRecommendVO;
import com.roncoo.education.community.service.feign.biz.FeignArticleRecommendBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章推荐
 *
 * @author wujing
 */
@RestController
public class FeignArticleRecommendController extends BaseController implements IFeignArticleRecommend {

	@Autowired
	private FeignArticleRecommendBiz biz;

	@Override
	public Page<ArticleRecommendVO> listForPage(@RequestBody ArticleRecommendQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ArticleRecommendQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ArticleRecommendQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public ArticleRecommendVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

}
