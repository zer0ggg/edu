package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignArticleZoneRef;
import com.roncoo.education.community.feign.qo.ArticleZoneRefQO;
import com.roncoo.education.community.feign.vo.ArticleZoneRefVO;
import com.roncoo.education.community.service.feign.biz.FeignArticleZoneRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@RestController
public class FeignArticleZoneRefController extends BaseController implements IFeignArticleZoneRef {

	@Autowired
	private FeignArticleZoneRefBiz biz;

	@Override
	public Page<ArticleZoneRefVO> listForPage(@RequestBody ArticleZoneRefQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ArticleZoneRefQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ArticleZoneRefQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public ArticleZoneRefVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public int remove(@PathVariable(value = "ids") String ids) {
		return biz.remove(ids);
	}

}
