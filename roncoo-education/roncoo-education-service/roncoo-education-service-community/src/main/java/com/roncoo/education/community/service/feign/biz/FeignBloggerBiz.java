package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.BloggerQO;
import com.roncoo.education.community.feign.vo.BloggerVO;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 博主信息表
 *
 * @author wujing
 */
@Component
public class FeignBloggerBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BloggerDao dao;

	public Page<BloggerVO> listForPage(BloggerQO qo) {
		BloggerExample example = new BloggerExample();
		Criteria c = example.createCriteria();
		if (qo.getMobile() != null) {
			UserExtQO userExtQO = new UserExtQO();
			userExtQO.setMobile(qo.getMobile());
			UserExtVO userExt = feignUserExt.getByMobile(userExtQO);
			if (ObjectUtil.isNotNull(userExt)) {
				c.andUserNoEqualTo(qo.getUserNo());
			}
		}
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc");
		Page<Blogger> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<BloggerVO> listForPage = PageUtil.transform(page, BloggerVO.class);
		for (BloggerVO vo : listForPage.getList()) {
			UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
			if (ObjectUtil.isNotNull(userExtVO)) {
				vo.setMobile(userExtVO.getMobile());
			}
		}
		return listForPage;
	}

	public int save(BloggerQO qo) {
		Blogger blogger = dao.getByBloggerUserNo(qo.getUserNo());
		if (ObjectUtil.isNotNull(blogger)) {
			throw new BaseException("博主已存在");
		}
		Blogger record = BeanUtil.copyProperties(qo, Blogger.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public BloggerVO getById(Long id) {
		Blogger record = dao.getById(id);
		return BeanUtil.copyProperties(record, BloggerVO.class);
	}

	public int updateById(BloggerQO qo) {
		Blogger record = BeanUtil.copyProperties(qo, Blogger.class);
		return dao.updateById(record);
	}

	public BloggerVO getByUserNo(BloggerQO qo) {
		Blogger record = dao.getByBloggerUserNo(qo.getUserNo());
		if (ObjectUtil.isNull(record)) {
			throw new BaseException("找不到博主信息");
		}
		BloggerVO vo = BeanUtil.copyProperties(record, BloggerVO.class);
		UserExtVO userExtVO = feignUserExt.getByUserNo(record.getUserNo());
		if (ObjectUtil.isNull(userExtVO)) {
			throw new BaseException("找不到用户信息");
		}
		if (StringUtils.isEmpty(userExtVO.getNickname())) {
			vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
		} else {
			vo.setNickname(userExtVO.getNickname());
		}
		if (StringUtils.hasText(userExtVO.getHeadImgUrl())) {
			vo.setHeadImgUrl(userExtVO.getHeadImgUrl());
		}
		vo.setMobile(userExtVO.getMobile());
		return vo;
	}

}
