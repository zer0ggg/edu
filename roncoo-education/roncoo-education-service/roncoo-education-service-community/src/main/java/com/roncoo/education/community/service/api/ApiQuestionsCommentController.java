package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.QuestionsCommentPageBO;
import com.roncoo.education.community.common.dto.QuestionsCommentPageDTO;
import com.roncoo.education.community.service.api.biz.ApiQuestionsCommentBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答评论表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/questions/comment")
public class ApiQuestionsCommentController {

    @Autowired
    private ApiQuestionsCommentBiz biz;

    /**
     * 问答评论展示接口
     * @param blogPageBO
     * @return
     */
	@ApiOperation(value = "问答评论展示接口", notes = "问答评论展示接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<QuestionsCommentPageDTO>> list(@RequestBody QuestionsCommentPageBO questionsCommentPageBO) {
		return biz.list(questionsCommentPageBO);
	}
}
