package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问答信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;
    /**
     * 问题标签
     */
    @ApiModelProperty(value = "问题标签")
    private String tagsName;
    /**
     * 问题内容
     */
    @ApiModelProperty(value = "问题内容")
    private String content;

}
