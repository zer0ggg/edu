package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.ArticleZoneCategoryDao;
import com.roncoo.education.community.service.dao.impl.mapper.ArticleZoneCategoryMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ArticleZoneCategoryDaoImpl implements ArticleZoneCategoryDao {
	@Autowired
	private ArticleZoneCategoryMapper articleZoneCategoryMapper;

	@Override
    public int save(ArticleZoneCategory record) {
		record.setId(IdWorker.getId());
		return this.articleZoneCategoryMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.articleZoneCategoryMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(ArticleZoneCategory record) {
		return this.articleZoneCategoryMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(ArticleZoneCategory record, ArticleZoneCategoryExample example) {
		return this.articleZoneCategoryMapper.updateByExampleSelective(record, example);
	}

	@Override
    public ArticleZoneCategory getById(Long id) {
		return this.articleZoneCategoryMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<ArticleZoneCategory> listForPage(int pageCurrent, int pageSize, ArticleZoneCategoryExample example) {
		int count = this.articleZoneCategoryMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<ArticleZoneCategory>(count, totalPage, pageCurrent, pageSize, this.articleZoneCategoryMapper.selectByExample(example));
	}

	@Override
	public ArticleZoneCategory getByArticleZoneName(String articleZoneName) {
		ArticleZoneCategoryExample example = new ArticleZoneCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andArticleZoneNameEqualTo(articleZoneName);
		List<ArticleZoneCategory> list = this.articleZoneCategoryMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<ArticleZoneCategory> listByPlatShowAndStatusId(Integer platShow, Integer statusId) {
		ArticleZoneCategoryExample example = new ArticleZoneCategoryExample();
		Criteria criteria = example.createCriteria();
		criteria.andPlatShowEqualTo(platShow);
		criteria.andStatusIdEqualTo(statusId);
		example.setOrderByClause("sort asc, id desc ");
		return this.articleZoneCategoryMapper.selectByExample(example);
	}
}
