package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 文章专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneRefSaveREQ", description="文章专区关联添加")
public class ArticleZoneRefSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "专区ID不能为空")
    @ApiModelProperty(value = "专区ID",required = true)
    private Long articleZoneId;

    @NotNull(message = "文章ID不能为空")
    @ApiModelProperty(value = "文章ID",required = true)
    private Long articleId;
}
