package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserCollectionPageBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserRefDeleteBO;
import com.roncoo.education.community.common.bo.auth.AuthBlogUserRefSaveBO;
import com.roncoo.education.community.common.dto.auth.AuthBlogUserCollectionPageDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthBlogUserRecordBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客与用户关联表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/blog/user/record")
public class AuthBlogUserRecordController {

	@Autowired
	private AuthBlogUserRecordBiz biz;

	/**
	 * 查看收藏、点赞(博客、资讯)列表
	 *
	 * @param weblogUserAttentionPageBO
	 * @return
	 */
	@ApiOperation(value = "查看收藏、点赞(博客、资讯)列表", notes = "查看收藏、点赞(博客、资讯)列表")
	@RequestMapping(value = "/collection/list", method = RequestMethod.POST)
	public Result<Page<AuthBlogUserCollectionPageDTO>> attentionList(@RequestBody AuthBlogUserCollectionPageBO authBlogUserCollectionPageBO) {
		return biz.collectionList(authBlogUserCollectionPageBO);
	}

	/**
	 * 添加收藏、点赞接口
	 *
	 * @param authBlogUserRefSaveBO
	 * @return
	 */
	@ApiOperation(value = "添加收藏、点赞接口", notes = "添加收藏、点赞接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthBlogUserRefSaveBO authBlogUserRefSaveBO) {
		return biz.save(authBlogUserRefSaveBO);
	}

	/**
	 * 取消收藏、点赞接口
	 *
	 * @param weblogUserRefDeleteBO
	 * @return
	 */
	@ApiOperation(value = "取消收藏、点赞接口", notes = "取消收藏、点赞接口")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthBlogUserRefDeleteBO authBlogUserRefDeleteBO) {
		return biz.delete(authBlogUserRefDeleteBO);
	}
}
