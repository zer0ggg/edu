package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.community.feign.qo.LabelQO;
import com.roncoo.education.community.feign.vo.LabelVO;
import com.roncoo.education.community.service.dao.LabelDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Label;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 标签
 *
 * @author wujing
 */
@Component
public class FeignLabelBiz extends BaseBiz {

	@Autowired
	private LabelDao dao;

	public Page<LabelVO> listForPage(LabelQO qo) {
		LabelExample example = new LabelExample();
		Criteria c = example.createCriteria();
		if (StringUtils.isNotBlank(qo.getLabelName())) {
			c.andLabelNameLike(PageUtil.like(qo.getLabelName()));
		}
		if (qo.getLabelType() != null) {
			c.andLabelTypeEqualTo(qo.getLabelType());
		}
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		} else {
			c.andStatusIdLessThan(Constants.FREEZE);
		}
		example.setOrderByClause("status_id desc, sort asc, id desc ");
		Page<Label> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, LabelVO.class);
	}

	public int save(LabelQO qo) {
		if (qo.getLabelType() == null) {
			throw new BaseException("标签类型不能为空");
		}
		Label courseLabel = dao.getByLabelTypeAndLabelName(qo.getLabelType(), qo.getLabelName());
		if (ObjectUtil.isNotNull(courseLabel)) {
			throw new BaseException("已存在该标签");
		}
		Label record = BeanUtil.copyProperties(qo, Label.class);
		record.setLabelType(qo.getLabelType());
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public LabelVO getById(Long id) {
		Label record = dao.getById(id);
		return BeanUtil.copyProperties(record, LabelVO.class);
	}

	public int updateById(LabelQO qo) {
		Label record = BeanUtil.copyProperties(qo, Label.class);
		return dao.updateById(record);
	}

	public List<LabelVO> listByAllAndLabelType(LabelQO qo) {
		List<Label> record = dao.listByAllAndLabelType(qo.getLabelType());
		return ArrayListUtil.copy(record, LabelVO.class);
	}

}
