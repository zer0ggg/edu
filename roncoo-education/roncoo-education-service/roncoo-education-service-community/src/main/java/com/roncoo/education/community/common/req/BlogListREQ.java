package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 博客信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BlogListREQ", description = "博客信息列表")
public class BlogListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "博客标题")
    private String title;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)")
    private Integer articleType;

    @ApiModelProperty(value = "博客标签")
    private String tagsName;

    @ApiModelProperty(value = "是否公开博客(1公开，0不公开)")
    private Integer isOpen;

    @ApiModelProperty(value = "是否已经发布(1发布，0没发布)")
    private Integer isIssue;

    @ApiModelProperty(value = "博客类型(1原创，2转载)")
    private Integer typeId;

    @ApiModelProperty(value = "是否置顶(1置顶，0否)")
    private Integer isTop;

    @ApiModelProperty(value = "开始修改时间")
    private Date beginGmtModified;

    @ApiModelProperty(value = "结束修改时间")
    private Date endGmtModified;

    @ApiModelProperty(value = "显示平台(1:PC端显示;2:微信端展示)")
    private Integer platShow;

    @ApiModelProperty(value = "专区文章ID")
    private Long articleZoneId;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
