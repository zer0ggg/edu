package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsCommentListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;
    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;
    /**
     * 问题ID
     */
    @ApiModelProperty(value = "问题ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long questionsId;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题")
    private String title;
    /**
     * 评论者用户编号
     */
    @ApiModelProperty(value = "评论者用户编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userNo;
    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容")
    private String content;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String headImgUrl;
    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nickname;
    /**
     * 被评论者用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "被评论者用户编号")
    private Long commentUserNo;
    /**
	 * 被评论者用户头像
	 */
	@ApiModelProperty(value = "被评论者用户头像")
	private String commentUserImg;
	/**
	 * 被评论者用户昵称
	 */
	@ApiModelProperty(value = "被评论者用户昵称")
	private String commentUickname;
}
