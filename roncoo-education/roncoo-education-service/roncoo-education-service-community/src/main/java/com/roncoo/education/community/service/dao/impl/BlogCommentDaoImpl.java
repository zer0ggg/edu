package com.roncoo.education.community.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.BlogCommentDao;
import com.roncoo.education.community.service.dao.impl.mapper.BlogCommentMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogCommentExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BlogCommentDaoImpl implements BlogCommentDao {
    @Autowired
    private BlogCommentMapper blogCommentMapper;

    @Override
    public int save(BlogComment record) {
        record.setId(IdWorker.getId());
        return this.blogCommentMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.blogCommentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(BlogComment record) {
        return this.blogCommentMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(BlogComment record, BlogCommentExample example) {
        return this.blogCommentMapper.updateByExampleSelective(record, example);
    }

    @Override
    public BlogComment getById(Long id) {
        return this.blogCommentMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<BlogComment> listForPage(int pageCurrent, int pageSize, BlogCommentExample example) {
        int count = this.blogCommentMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<BlogComment>(count, totalPage, pageCurrent, pageSize, this.blogCommentMapper.selectByExample(example));
    }

	@Override
	public List<BlogComment> listByParentIdAndStatusId(Long parentId, Integer statusId) {
		BlogCommentExample example = new BlogCommentExample();
		Criteria c = example.createCriteria();
		c.andParentIdEqualTo(parentId);
		c.andStatusIdEqualTo(statusId);
		return this.blogCommentMapper.selectByExample(example);
	}
}
