package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsUserRecordExample;

public interface QuestionsUserRecordDao {
    int save(QuestionsUserRecord record);

    int deleteById(Long id);

    int updateById(QuestionsUserRecord record);

    int updateByExampleSelective(QuestionsUserRecord record, QuestionsUserRecordExample example);

    QuestionsUserRecord getById(Long id);

    Page<QuestionsUserRecord> listForPage(int pageCurrent, int pageSize, QuestionsUserRecordExample example);

    /**
     * 问题id,用户编号、操作类型查找
     * @param questionsId
     * @param userNo
     * @param opType
     * @return
     */
	QuestionsUserRecord getByQuestionIdAndUserNoAndOpType(Long questionsId, Long userNo, Integer opType);
}