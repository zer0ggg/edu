package com.roncoo.education.community.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleRecommendPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
	private Integer articleType;

	@ApiModelProperty(value = "当前页")
	private Integer pageCurrent = 1;

	@ApiModelProperty(value = "每页记录数")
	private Integer pageSize = 20;
}
