package com.roncoo.education.community.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标签
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class LabelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "主键")
	private Long id;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 标签名称
	 */
	@ApiModelProperty(value = "标签名称")
	private String labelName;
	/**
	 * 标签类型（1:博客标签）
	 */
	@ApiModelProperty(value = "标签类型（1:博客标签）")
	private Integer labelType;
}
