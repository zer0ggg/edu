package com.roncoo.education.community.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneRefListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 文章专区关联集合
	 */
	@ApiModelProperty(value = "文章专区关联集合")
	private List<ArticleZoneRefDTO> list;

}
