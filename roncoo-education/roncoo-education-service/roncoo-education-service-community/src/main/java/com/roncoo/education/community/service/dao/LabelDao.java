package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Label;
import com.roncoo.education.community.service.dao.impl.mapper.entity.LabelExample;

import java.util.List;

public interface LabelDao {
	int save(Label record);

	int deleteById(Long id);

	int updateById(Label record);

	int updateByExampleSelective(Label record, LabelExample example);

	Label getById(Long id);

	Page<Label> listForPage(int pageCurrent, int pageSize, LabelExample example);

	List<Label> listByAllAndLabelType(Integer labelType);

	Label getByLabelTypeAndLabelName(Integer labelType, String labelName);

	/**
	 * 根据标签类型状态列出标签
	 *
	 * @param labelType
	 * @param statusId
	 * @return
	 */
	List<Label> listByAllAndLabelTypeAndStatusId(Integer labelType, Integer statusId);
}
