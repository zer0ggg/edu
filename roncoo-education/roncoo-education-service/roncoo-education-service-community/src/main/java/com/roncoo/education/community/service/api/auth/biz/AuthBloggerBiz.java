package com.roncoo.education.community.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.community.common.bo.auth.*;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserPageDTO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserViewDTO;
import com.roncoo.education.community.service.dao.BloggerDao;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blogger;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 博主信息表
 *
 * @author wujing
 */
@Component
public class AuthBloggerBiz extends BaseBiz {

    @Autowired
    private BloggerDao dao;
    @Autowired
    private BloggerUserRecordDao bloggerUserRecordDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    public Result<Page<AuthBloggerUserPageDTO>> recommendList(AuthBloggerUserPageBO bo) {
        BloggerExample example = new BloggerExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andWeblogAccountGreaterThanOrEqualTo(5);
        example.setOrderByClause(" status_id desc, sort asc, weblog_account desc, id desc");
        Page<Blogger> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthBloggerUserPageDTO> list = PageUtil.transform(page, AuthBloggerUserPageDTO.class);
        for (AuthBloggerUserPageDTO dto : list.getList()) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
            } else {
                dto.setBloggerNickname(userExtVO.getNickname());
            }
            dto.setBloggerUserImg(userExtVO.getHeadImgUrl());

            // 根据用户编号和博主ID查找博主与用户关注关联表
            BloggerUserRecord bloggerUserAttention = bloggerUserRecordDao.getByUserNoAndBloggerUserNo(ThreadContext.userNo(), dto.getUserNo());
            // 判断用户是否关注该博主
            if (ObjectUtils.isEmpty(bloggerUserAttention)) {
                dto.setIsAttention(0);
            } else {
                dto.setIsAttention(1);
            }
        }
        return Result.success(list);
    }

    public Result<AuthBloggerUserViewDTO> view(AuthBloggerUserViewBO bo) {
        if (bo.getBloggerUserNo() == null) {
            return Result.error("bloggerUserNo不能为空");
        }
        // 查看用户信息
        UserExtVO vo = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(vo)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(vo.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }

        AuthBloggerUserViewDTO dto = BeanUtil.copyProperties(vo, AuthBloggerUserViewDTO.class);
        // 查看博主信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getBloggerUserNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("博主异常");
        }

        Blogger blogger = dao.getByBloggerUserNo(bo.getBloggerUserNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("博主信息不存在!");
        }
        dto.setFansAccount(blogger.getFansAccount());
        dto.setAttentionAcount(blogger.getAttentionAcount());
        dto.setWeblogAccount(blogger.getWeblogAccount());
        dto.setQuestionsAccount(blogger.getQuestionsAccount());
        dto.setAnswerAccount(blogger.getAnswerAccount());

        dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
        } else {
            dto.setBloggerNickname(userExtVO.getNickname());
        }

        // 根据用户编号和博主ID查找博主与用户关注关联表
        BloggerUserRecord bloggerUserRecord = bloggerUserRecordDao.getByUserNoAndBloggerUserNo(ThreadContext.userNo(), bo.getBloggerUserNo());
        // 判断用户是否关注该博主
        if (ObjectUtil.isNull(bloggerUserRecord)) {
            dto.setIsAttention(0);
        } else {
            dto.setIsAttention(1);
        }
        return Result.success(dto);
    }

    public Result<AuthBloggerUserViewDTO> myself(AuthBloggerUserMyselfViewBO bo) {
        // 根据userNo查找博主教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户教育信息不存在");
        }
        AuthBloggerUserViewDTO dto = BeanUtil.copyProperties(userExtVO, AuthBloggerUserViewDTO.class);
        dto.setBloggerUserNo(userExtVO.getUserNo());
        dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
        } else {
            dto.setBloggerNickname(userExtVO.getNickname());
        }

        Blogger blogger = dao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("博主信息不存在!");
        }
        dto.setFansAccount(blogger.getFansAccount());
        dto.setAttentionAcount(blogger.getAttentionAcount());
        dto.setWeblogAccount(blogger.getWeblogAccount());
        dto.setQuestionsAccount(blogger.getQuestionsAccount());
        dto.setAnswerAccount(blogger.getAnswerAccount());
        dto.setIntroduction(blogger.getIntroduction());
        return Result.success(dto);
    }

    public Result<Integer> save(AuthBloggerUserSaveBO bo) {
        // 根据用户编号查询博主信息
        Blogger blogger = dao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotNull(blogger)) {
            //logger.error("用户的博主信息已经存在,用户编号为{}", bo.getUserNo());
            return Result.success(1);
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户教育信息不存在");
        }
        Blogger bloggerSave = BeanUtil.copyProperties(userExtVO, Blogger.class);
        bloggerSave.setUserNo(userExtVO.getUserNo());
        return Result.success(dao.save(bloggerSave));
    }

    public Result<Integer> update(AuthBloggerUpdateBO bo) {
        // 根据用户编号查询博主信息
        Blogger blogger = dao.getByBloggerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(blogger)) {
            return Result.error("找不到博主信息");
        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户教育信息不存在");
        }
        blogger.setIntroduction(bo.getIntroduction());
        int results = dao.updateById(blogger);
        if (results < 0) {
            return Result.error(ResultEnum.COMMUNITY_UPDATE_FAIL);
        }
        return Result.success(results);
    }

}
