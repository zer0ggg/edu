package com.roncoo.education.community.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客评论表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBlogCommentUserPageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客与用户关联表ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博客与用户关联表ID")
	private Long id;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date gmtCreate;
	/**
	 * 父ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "父ID")
	private Long parentId;
	/**
	 * 博客ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博客ID")
	private Long blogId;
	/**
	 * 博客标题
	 */
	@ApiModelProperty(value = "博客标题")
	private String title;
	/**
	 * 博客用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "被评论者用户编号")
	private Long bloggerUserNo;
	/**
	 * 博客用户头像
	 */
	@ApiModelProperty(value = "被评论者用户头像")
	private String bloggerUserImg;
	/**
	 * 博客用户昵称
	 */
	@ApiModelProperty(value = "被评论者用户昵称")
	private String bloggerNickname;
	/**
	 * 评论者用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "评论者用户编号")
	private Long userNo;
	/**
	 * 评论者用户头像
	 */
	@ApiModelProperty(value = "评论者用户头像")
	private String userImg;
	/**
	 * 评论者用户昵称
	 */
	@ApiModelProperty(value = "评论者用户昵称")
	private String nickname;
	/**
	 * 评论内容
	 */
	@ApiModelProperty(value = "评论内容")
	private String content;

}
