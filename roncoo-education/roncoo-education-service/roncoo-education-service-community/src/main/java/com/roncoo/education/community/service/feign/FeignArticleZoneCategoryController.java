package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignArticleZoneCategory;
import com.roncoo.education.community.feign.qo.ArticleZoneCategoryQO;
import com.roncoo.education.community.feign.vo.ArticleZoneCategoryVO;
import com.roncoo.education.community.service.feign.biz.FeignArticleZoneCategoryBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@RestController
public class FeignArticleZoneCategoryController extends BaseController implements IFeignArticleZoneCategory{

	@Autowired
	private FeignArticleZoneCategoryBiz biz;

	@Override
	public Page<ArticleZoneCategoryVO> listForPage(@RequestBody ArticleZoneCategoryQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody ArticleZoneCategoryQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@PathVariable(value = "id") Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody ArticleZoneCategoryQO qo){
		return biz.updateById(qo);
	}

    @Override
	public ArticleZoneCategoryVO getById(@PathVariable(value = "id") Long id){
		return biz.getById(id);
	}

}
