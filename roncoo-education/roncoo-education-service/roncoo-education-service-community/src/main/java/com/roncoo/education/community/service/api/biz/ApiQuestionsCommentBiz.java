package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.community.common.bo.QuestionsCommentPageBO;
import com.roncoo.education.community.common.dto.QuestionsCommentPageDTO;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Component
public class ApiQuestionsCommentBiz {

	@Autowired
	private QuestionsCommentDao dao;

	@Autowired
	private IFeignUserExt feignUserExt;

	public Result<Page<QuestionsCommentPageDTO>> list(QuestionsCommentPageBO bo) {
		QuestionsCommentExample example = new QuestionsCommentExample();
		Criteria c = example.createCriteria();
		c.andQuestionsIdEqualTo(bo.getQuestionsId());
		c.andParentIdEqualTo(0L);
		c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
		example.setOrderByClause(" sort asc, id desc ");
		Page<QuestionsComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		Page<QuestionsCommentPageDTO> dtoPage = PageUtil.transform(page, QuestionsCommentPageDTO.class);
		for (QuestionsCommentPageDTO dto : dtoPage.getList()) {
			QuestionsComment comment = dao.getById(dto.getId());
			dto.setContent(comment.getContent());
			// 评论者用户
			UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
			if(StringUtils.isEmpty(dto.getNickname())){
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					dto.setNickname(userExtVO.getNickname());
				}
			}
			if (StringUtils.hasText(userExtVO.getHeadImgUrl())) {
				dto.setUserImg(userExtVO.getHeadImgUrl());
			}
			//被评论者用户信息
			UserExtVO vo = feignUserExt.getByUserNo(dto.getCommentUserNo());
			if (StringUtils.isEmpty(vo.getNickname())) {
				dto.setCommentUickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
			} else {
				dto.setCommentUickname(vo.getNickname());
			}
			if (StringUtils.hasText(vo.getHeadImgUrl())) {
				dto.setCommentUserImg(vo.getHeadImgUrl());
			}

			dto.setList(recursion(dto.getId()));
		}
		return Result.success(dtoPage);
	}

	private List<QuestionsCommentPageDTO> recursion(Long parentId) {
		List<QuestionsComment> questionsCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
		List<QuestionsCommentPageDTO> list = ArrayListUtil.copy(questionsCommentList, QuestionsCommentPageDTO.class);
		if (CollectionUtils.isNotEmpty(questionsCommentList)) {
			for (QuestionsCommentPageDTO dto : list) {
				// 获取用户信息
				UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
				if (ObjectUtil.isNotNull(userExtVO)) {
					if(StringUtils.isEmpty(dto.getNickname())){
						if (StringUtils.isEmpty(userExtVO.getNickname())) {
							dto.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
						} else {
							dto.setNickname(userExtVO.getNickname());
						}
					}
					if (StringUtils.hasText(userExtVO.getHeadImgUrl())) {
						dto.setUserImg(userExtVO.getHeadImgUrl());
					}
				}
				//被评论者用户信息
				UserExtVO vo = feignUserExt.getByUserNo(dto.getCommentUserNo());
				if (ObjectUtil.isNotNull(vo)) {
					if(StringUtils.isEmpty(dto.getNickname())){
						if (StringUtils.isEmpty(vo.getNickname())) {
							dto.setCommentUickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
						} else {
							dto.setCommentUickname(vo.getNickname());
						}
					}
					if (StringUtils.hasText(vo.getHeadImgUrl())) {
						dto.setCommentUserImg(vo.getHeadImgUrl());
					}
				}
				// 获取大字段博客内容content
				String content = getBigField(dto);
				dto.setContent(content);
				dto.setList(recursion(dto.getId()));
			}
		}
		return list;
	}

	private String getBigField(QuestionsCommentPageDTO dto) {
		// 获取大字段博客内容content
		QuestionsComment questionsComment = dao.getById(dto.getId());
		// 截取评论摘要
		String content = questionsComment.getContent();
		// 过滤文章内容中的html
		content = content.replaceAll("</?[^<]+>", "");
		// 去除字符串中的空格 回车 换行符 制表符 等
		content = content.replaceAll("\\s*|\t|\r|\n", "");
		// 去除空格
		content = content.replaceAll("&nbsp;", "");
		return content;
	}

}
