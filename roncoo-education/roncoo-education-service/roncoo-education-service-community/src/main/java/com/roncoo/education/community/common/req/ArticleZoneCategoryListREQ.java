package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 文章专区首页分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneCategoryListREQ", description="文章专区首页分类列表")
public class ArticleZoneCategoryListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "文章专区名称")
    private String articleZoneName;

    @ApiModelProperty(value = "显示平台(1:PC端显示;2:微信端展示)")
    private Integer platShow;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
