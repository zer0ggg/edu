package com.roncoo.education.community.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 文章专区首页分页列出
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ArticleZoneCategoryListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 专区集合
	 */
	@ApiModelProperty(value = "专区集合")
	private List<ArticleZoneCategoryDTO> list = new ArrayList<>();
}
