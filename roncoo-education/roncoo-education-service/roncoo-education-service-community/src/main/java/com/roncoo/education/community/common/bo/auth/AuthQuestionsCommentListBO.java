package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsCommentListBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论类型(1我评论别人的，2别人评论我的)
     */
    @ApiModelProperty(value = "评论类型评论类型(1我评论别人的，2别人评论我的)", required = true)
    private Integer commentType;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize = 20;
}
