package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.QuestionsCommentQO;
import com.roncoo.education.community.feign.vo.QuestionsCommentVO;
import com.roncoo.education.community.feign.vo.QuestionsStatisticalVO;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Component
public class FeignQuestionsCommentBiz {
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private QuestionsCommentDao dao;
    @Autowired
    private QuestionsDao questionsDao;

    public Page<QuestionsCommentVO> listForPage(QuestionsCommentQO qo) {
        QuestionsCommentExample example = new QuestionsCommentExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        if (qo.getParentId() != null) {
            c.andParentIdEqualTo(qo.getParentId());
        } else {
            c.andParentIdEqualTo(0L);
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<QuestionsComment> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<QuestionsCommentVO> listForPage = PageUtil.transform(page, QuestionsCommentVO.class);
        for (QuestionsCommentVO vo : listForPage.getList()) {
            // 获取被评论用户信息
            UserExtVO commentUser = feignUserExt.getByUserNo(vo.getCommentUserNo());
            if (ObjectUtil.isNotNull(commentUser)) {
                if (StringUtils.isEmpty(vo.getNickname())) {
                    if (StringUtils.isEmpty(commentUser.getNickname())) {
                        vo.setCommentNickname(commentUser.getMobile().substring(0, 3) + "****" + commentUser.getMobile().substring(7));
                    } else {
                        vo.setCommentNickname(commentUser.getNickname());
                    }

                }

            }
            // 获取评论用户信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(vo.getCommentNickname())) {
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        vo.setNickname(userExtVO.getNickname());
                    }
                }
            }
            // 获取大字段博客内容content
            String content = getBigField(vo);
            vo.setContent(content);
            vo.setList(recursionList(vo.getId()));
        }
        return listForPage;
    }

    public int save(QuestionsCommentQO qo) {
        QuestionsComment record = BeanUtil.copyProperties(qo, QuestionsComment.class);
        return dao.save(record);
    }

    @Transactional(rollbackFor = Exception.class)
    public int deleteById(Long id) {
        QuestionsComment record = dao.getById(id);
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("找不到评论信息");
        }

        Questions questions = questionsDao.getById(record.getQuestionsId());
        if (ObjectUtil.isNull(questions)) {
            throw new BaseException("找不到问题信息");
        }
        dao.deleteById(id);
        int count = recursionDelete(id, 1);
        // 更新提问评论数
        questions.setCommentAcount(questions.getCommentAcount() - count);
        return questionsDao.updateById(questions);
    }

    public QuestionsCommentVO getById(Long id) {
        QuestionsComment record = dao.getById(id);
        return BeanUtil.copyProperties(record, QuestionsCommentVO.class);
    }

    public int updateById(QuestionsCommentQO qo) {
        QuestionsComment record = BeanUtil.copyProperties(qo, QuestionsComment.class);
        return dao.updateById(record);
    }

    public QuestionsStatisticalVO statisticalByUserNo(Long userNo) {
        UserExtVO vo = feignUserExt.getByUserNo(userNo);
        QuestionsStatisticalVO record;
        record = BeanUtil.copyProperties(vo, QuestionsStatisticalVO.class);
        // 获取用户问答数
        List<Questions> list = questionsDao.listByUserNo(userNo);
        if (CollectionUtils.isNotEmpty(list)) {
            record.setQuestionsAccount(list.size());
        } else {
            record.setQuestionsAccount(0);
        }
        // 获取提问处
        List<QuestionsComment> commentList = dao.listByUserNo(userNo);
        if (CollectionUtils.isNotEmpty(commentList)) {
            record.setQuestionsResponseAccount(commentList.size());
        } else {
            record.setQuestionsResponseAccount(0);
        }
        return record;
    }

    /**
     * 递归删除评论信息
     *
     * @param parentId
     * @param i
     * @return
     */
    private Integer recursionDelete(Long parentId, int i) {
        List<QuestionsComment> questionsCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        List<QuestionsCommentVO> list = ArrayListUtil.copy(questionsCommentList, QuestionsCommentVO.class);
        if (CollectionUtils.isNotEmpty(questionsCommentList)) {
            for (QuestionsCommentVO vo : list) {
                dao.deleteById(vo.getId());
                i++;
                recursionDelete(vo.getId(), i);
            }
        }
        return i;
    }

    /**
     * 递归展示分类
     */
    private List<QuestionsCommentVO> recursionList(Long parentId) {
        List<QuestionsComment> questionsCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        List<QuestionsCommentVO> list = ArrayListUtil.copy(questionsCommentList, QuestionsCommentVO.class);
        if (CollectionUtils.isNotEmpty(questionsCommentList)) {
            for (QuestionsCommentVO vo : list) {
                // 获取被评论用户信息
                UserExtVO commentUser = feignUserExt.getByUserNo(vo.getCommentUserNo());
                if (ObjectUtil.isNotNull(commentUser)) {
                    if (StringUtils.isEmpty(vo.getNickname())) {
                        if (StringUtils.isEmpty(commentUser.getNickname())) {
                            vo.setCommentNickname(commentUser.getMobile().substring(0, 3) + "****" + commentUser.getMobile().substring(7));
                        } else {
                            vo.setCommentNickname(commentUser.getNickname());
                        }
                    }

                }
                // 获取评论用户信息
                UserExtVO userExtVO = feignUserExt.getByUserNo(vo.getUserNo());
                if (ObjectUtil.isNotNull(userExtVO)) {
                    if (StringUtils.isEmpty(vo.getCommentNickname())) {
                        if (StringUtils.isEmpty(userExtVO.getNickname())) {
                            vo.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                        } else {
                            vo.setNickname(userExtVO.getNickname());
                        }
                    }
                }

                // 获取大字段博客内容content
                String content = getBigField(vo);
                vo.setContent(content);
                vo.setList(recursionList(vo.getId()));
            }
        }
        return list;
    }

    /**
     * 获取大字段博客内容content
     *
     * @param questionsCommentVO
     * @return
     */
    private String getBigField(QuestionsCommentVO questionsCommentVO) {
        // 获取大字段博客内容content
        QuestionsComment questionsComment = dao.getById(questionsCommentVO.getId());
        // 截取评论摘要
        String content = questionsComment.getContent();
        // 过滤文章内容中的html
        content = content.replaceAll("</?[^<]+>", "");
        // 去除字符串中的空格 回车 换行符 制表符 等
        content = content.replaceAll("\\s*|\t|\r|\n", "");
        // 去除空格
        content = content.replaceAll("&nbsp;", "");
        return content;
    }

}
