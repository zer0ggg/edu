package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogUserRecordExample;

import java.util.List;

public interface BlogUserRecordDao {
    int save(BlogUserRecord record);

    int deleteById(Long id);

    int updateById(BlogUserRecord record);

    int updateByExampleSelective(BlogUserRecord record, BlogUserRecordExample example);

    BlogUserRecord getById(Long id);

    Page<BlogUserRecord> listForPage(int pageCurrent, int pageSize, BlogUserRecordExample example);

    /**
     * 根据用户编号、博客id、类型查询
     * @param userNo
     * @param id
     * @param code
     * @return
     */
	BlogUserRecord getByUserNoAndBlogIdAndOpType(Long userNo, Long blogId, Integer opType);

	/**
	 * 根据用户编号查询关注列表
	 * @param userNo
	 * @param code
	 * @return
	 */
	List<BlogUserRecord> listByUserNoAndStatusId(Long userNo, Integer statusId);

}
