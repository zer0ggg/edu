package com.roncoo.education.community.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.common.req.BloggerUserRecordEditREQ;
import com.roncoo.education.community.common.req.BloggerUserRecordListREQ;
import com.roncoo.education.community.common.req.BloggerUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.BloggerUserRecordListRESP;
import com.roncoo.education.community.common.resp.BloggerUserRecordViewRESP;
import com.roncoo.education.community.service.dao.BloggerUserRecordDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecord;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BloggerUserRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 博主与用户关注关联
 *
 * @author wujing
 */
@Component
public class PcBloggerUserRecordBiz extends BaseBiz {

    @Autowired
    private BloggerUserRecordDao dao;

    /**
    * 博主与用户关注关联列表
    *
    * @param bloggerUserRecordListREQ 博主与用户关注关联分页查询参数
    * @return 博主与用户关注关联分页查询结果
    */
    public Result<Page<BloggerUserRecordListRESP>> list(BloggerUserRecordListREQ bloggerUserRecordListREQ) {
        BloggerUserRecordExample example = new BloggerUserRecordExample();
        Criteria c = example.createCriteria();
        if(bloggerUserRecordListREQ.getBloggerUserNo() != null){
            c.andBloggerUserNoEqualTo(bloggerUserRecordListREQ.getBloggerUserNo());
        }
        if (bloggerUserRecordListREQ.getUserNo() != null) {
            c.andUserNoEqualTo(bloggerUserRecordListREQ.getUserNo());
        }
        if (bloggerUserRecordListREQ.getOpType() != null) {
            c.andOpTypeEqualTo(bloggerUserRecordListREQ.getOpType());
        }
        if (StringUtils.hasText(bloggerUserRecordListREQ.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(bloggerUserRecordListREQ.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(bloggerUserRecordListREQ.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(bloggerUserRecordListREQ.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" id desc ");
        Page<BloggerUserRecord> page = dao.listForPage(bloggerUserRecordListREQ.getPageCurrent(), bloggerUserRecordListREQ.getPageSize(), example);
        Page<BloggerUserRecordListRESP> respPage = PageUtil.transform(page, BloggerUserRecordListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 博主与用户关注关联添加
    *
    * @param bloggerUserRecordSaveREQ 博主与用户关注关联
    * @return 添加结果
    */
    public Result<String> save(BloggerUserRecordSaveREQ bloggerUserRecordSaveREQ) {
        BloggerUserRecord record = BeanUtil.copyProperties(bloggerUserRecordSaveREQ, BloggerUserRecord.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 博主与用户关注关联查看
    *
    * @param id 主键ID
    * @return 博主与用户关注关联
    */
    public Result<BloggerUserRecordViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), BloggerUserRecordViewRESP.class));
    }


    /**
    * 博主与用户关注关联修改
    *
    * @param bloggerUserRecordEditREQ 博主与用户关注关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(BloggerUserRecordEditREQ bloggerUserRecordEditREQ) {
        BloggerUserRecord record = BeanUtil.copyProperties(bloggerUserRecordEditREQ, BloggerUserRecord.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 博主与用户关注关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
