package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class Questions implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private String title;

    private String tagsName;

    private String keywords;

    private String content;

    private String summary;

    private Integer qualityScore;

    private Integer collectionAcount;

    private Integer admireAcount;

    private Integer commentAcount;

    private Integer readAcount;

    private Integer isTop;

    private Integer isReward;

    private Integer rewardIntegral;

    private Integer isCourse;

    private Long courseId;

    private Integer isGood;

    private Long goodCommentId;

    private Long goodUserNo;

    private String goodContent;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTagsName() {
        return tagsName;
    }

    public void setTagsName(String tagsName) {
        this.tagsName = tagsName == null ? null : tagsName.trim();
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    public Integer getQualityScore() {
        return qualityScore;
    }

    public void setQualityScore(Integer qualityScore) {
        this.qualityScore = qualityScore;
    }

    public Integer getCollectionAcount() {
        return collectionAcount;
    }

    public void setCollectionAcount(Integer collectionAcount) {
        this.collectionAcount = collectionAcount;
    }

    public Integer getAdmireAcount() {
        return admireAcount;
    }

    public void setAdmireAcount(Integer admireAcount) {
        this.admireAcount = admireAcount;
    }

    public Integer getCommentAcount() {
        return commentAcount;
    }

    public void setCommentAcount(Integer commentAcount) {
        this.commentAcount = commentAcount;
    }

    public Integer getReadAcount() {
        return readAcount;
    }

    public void setReadAcount(Integer readAcount) {
        this.readAcount = readAcount;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getIsReward() {
        return isReward;
    }

    public void setIsReward(Integer isReward) {
        this.isReward = isReward;
    }

    public Integer getRewardIntegral() {
        return rewardIntegral;
    }

    public void setRewardIntegral(Integer rewardIntegral) {
        this.rewardIntegral = rewardIntegral;
    }

    public Integer getIsCourse() {
        return isCourse;
    }

    public void setIsCourse(Integer isCourse) {
        this.isCourse = isCourse;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Integer getIsGood() {
        return isGood;
    }

    public void setIsGood(Integer isGood) {
        this.isGood = isGood;
    }

    public Long getGoodCommentId() {
        return goodCommentId;
    }

    public void setGoodCommentId(Long goodCommentId) {
        this.goodCommentId = goodCommentId;
    }

    public Long getGoodUserNo() {
        return goodUserNo;
    }

    public void setGoodUserNo(Long goodUserNo) {
        this.goodUserNo = goodUserNo;
    }

    public String getGoodContent() {
        return goodContent;
    }

    public void setGoodContent(String goodContent) {
        this.goodContent = goodContent == null ? null : goodContent.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", title=").append(title);
        sb.append(", tagsName=").append(tagsName);
        sb.append(", keywords=").append(keywords);
        sb.append(", content=").append(content);
        sb.append(", summary=").append(summary);
        sb.append(", qualityScore=").append(qualityScore);
        sb.append(", collectionAcount=").append(collectionAcount);
        sb.append(", admireAcount=").append(admireAcount);
        sb.append(", commentAcount=").append(commentAcount);
        sb.append(", readAcount=").append(readAcount);
        sb.append(", isTop=").append(isTop);
        sb.append(", isReward=").append(isReward);
        sb.append(", rewardIntegral=").append(rewardIntegral);
        sb.append(", isCourse=").append(isCourse);
        sb.append(", courseId=").append(courseId);
        sb.append(", isGood=").append(isGood);
        sb.append(", goodCommentId=").append(goodCommentId);
        sb.append(", goodUserNo=").append(goodUserNo);
        sb.append(", goodContent=").append(goodContent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}