package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 文章专区首页分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneCategorySaveREQ", description="文章专区首页分类添加")
public class ArticleZoneCategorySaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotNull(message = "名称不能为空")
    @ApiModelProperty(value = "文章专区名称",required = true)
    private String articleZoneName;

    @ApiModelProperty(value = "专区描述")
    private String articleZoneDesc;

    @ApiModelProperty(value = "显示平台(1:PC端显示;2:微信端展示)")
    private Integer platShow;
}
