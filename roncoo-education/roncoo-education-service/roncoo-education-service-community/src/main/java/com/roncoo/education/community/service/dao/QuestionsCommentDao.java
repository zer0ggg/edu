package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;

import java.util.List;

public interface QuestionsCommentDao {
    int save(QuestionsComment record);

    int deleteById(Long id);

    int updateById(QuestionsComment record);

    int updateByExampleSelective(QuestionsComment record, QuestionsCommentExample example);

    QuestionsComment getById(Long id);

    Page<QuestionsComment> listForPage(int pageCurrent, int pageSize, QuestionsCommentExample example);

    /**
     * 根据提问ID、状态获评论信息
     *
     * @param parentId
     * @param statusId
     * @return
     */
    List<QuestionsComment> listByParentIdAndStatusId(Long parentId, Integer statusId);

    /**
     * 根据评论
     *
     * @param userNo
     * @return
     */
    List<QuestionsComment> listByUserNo(Long userNo);

    /**
     * 根据问答id删除评论信息
     *
     * @param id
     * @return
     */
    int deleteByQuestionId(Long quesionsId);

    /**
     * 获取最新评论
     *
     * @return
     */
    QuestionsComment getByNewest();
}
