package com.roncoo.education.community.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthBlogCommentBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常;0:禁用)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 机构号
     */
    private String orgNo;
    /**
     * 父ID
     */
    private Long parentId;
    /**
     * 博客ID
     */
    private Long blogId;
    /**
     * 博客标题
     */
    private String title;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 评论者IP
     */
    private String userIp;
    /**
     * 评论者终端
     */
    private String userTerminal;

}
