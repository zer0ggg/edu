package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.elasticsearch.EsPageUtil;
import com.roncoo.education.community.common.bo.BlogPageBO;
import com.roncoo.education.community.common.bo.BlogSearchPageBO;
import com.roncoo.education.community.common.bo.BlogViewBO;
import com.roncoo.education.community.common.dto.BlogPageDTO;
import com.roncoo.education.community.common.dto.BlogSearchPageDTO;
import com.roncoo.education.community.common.dto.BlogViewDTO;
import com.roncoo.education.community.common.es.EsBlog;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 博客信息表
 *
 * @author wujing
 */
@Component
public class ApiBlogBiz {

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private BlogDao dao;

    public Result<Page<BlogPageDTO>> list(BlogPageBO bo) {
        if (bo.getArticleType() == null) {
            return Result.error("文章类型不能为空");
        }
        BlogExample example = new BlogExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andIsIssueEqualTo(IsIssueEnum.YES.getCode());
        c.andIsOpenEqualTo(IsOpenEnum.YES.getCode());
        if (bo.getBloggerUserNo() != null) {
            c.andUserNoEqualTo(bo.getBloggerUserNo());
        }
        c.andArticleTypeEqualTo(bo.getArticleType());
        if (!StringUtils.isEmpty(bo.getTagsName())) {
            c.andTagsNameLike(PageUtil.like(bo.getTagsName()));
        }
        if (!StringUtils.isEmpty(bo.getTitle())) {
            c.andTitleLike(PageUtil.like(bo.getTitle()));
        }
        // 推荐文章、热门文章按照一季度时间内进行排序
        if (SectionsEnum.RECOMMEND.getCode().equals(bo.getSections()) || SectionsEnum.HOT.getCode().equals(bo.getSections())) {
            // 当前时间
            Date date = DateUtil.parseDate(DateUtil.formatToString("yyyy-MM-dd", System.currentTimeMillis()));
            // 一季度内的时间
            c.andGmtModifiedGreaterThanOrEqualTo(DateUtil.subDate(date, 90));
            c.andGmtModifiedLessThanOrEqualTo(date);
        }
        if (bo.getSections() == null || bo.getSections() == 0) {
            // 默认
            example.setOrderByClause(" is_top desc, sort asc, id desc ");
        } else if (bo.getSections().equals(SectionsEnum.NEW.getCode())) {
            // 最新文章
            example.setOrderByClause(" is_top desc, id desc ");
        } else if (bo.getSections().equals(SectionsEnum.HOT.getCode())) {
            // 热门文章
            example.setOrderByClause(" quality_score desc, read_acount desc, id desc ");
        } else if (bo.getSections().equals(SectionsEnum.RECOMMEND.getCode())) {
            // 推荐文章
            example.setOrderByClause(" sort asc, id desc ");
        }
        Page<Blog> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<BlogPageDTO> pageDto = PageUtil.transform(page, BlogPageDTO.class);

        if (pageDto.getList().isEmpty()) {
            return Result.success(pageDto);
        }

        if (ArticleTypeEnum.BLOG.getCode().equals(bo.getArticleType())) {
            Set<Long> userNos = pageDto.getList().stream().map(BlogPageDTO::getUserNo).collect(Collectors.toSet());
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setUserNos(new ArrayList<>(userNos));
            List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
            if (CollectionUtil.isNotEmpty(userExtVOList)) {
                Map<Long, UserExtVO> userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item));
                pageDto.getList().stream().forEach(item -> {
                    UserExtVO userExtVO = userExtVOMap.get(item.getUserNo());
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        item.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        item.setBloggerNickname(userExtVO.getNickname());
                    }
                    item.setBloggerUserImg(userExtVO.getHeadImgUrl());
                });
            }
        }
        return Result.success(pageDto);
    }

    public Result<BlogViewDTO> view(BlogViewBO bo) {
        if (bo.getId() == null) {
            return Result.error("id不能为空");
        }
        Blog blog = dao.getById(bo.getId());
        if (ObjectUtils.isEmpty(blog)) {
            return Result.error("找不到博客信息");
        }

        blog.setReadAcount(blog.getReadAcount() + 1);
        blog.setQualityScore(blog.getQualityScore() + 1);
        BlogViewDTO dto = BeanUtil.copyProperties(blog, BlogViewDTO.class);
        if (ArticleTypeEnum.BLOG.getCode().equals(blog.getArticleType())) {
            // 查询博客的用户信息是否存在
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (StringUtils.isEmpty(userExtVO.getNickname())) {
                dto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
            } else {
                dto.setBloggerNickname(userExtVO.getNickname());
                dto.setBloggerUserImg(userExtVO.getHeadImgUrl());
            }
        }
        // 更新博客信息
        Blog blogUpdate = new Blog();
        blogUpdate.setId(blog.getId());
        blogUpdate.setReadAcount(blog.getReadAcount());
        blogUpdate.setQualityScore(blog.getQualityScore());
        dao.updateById(blogUpdate);
        return Result.success(dto);
    }

    public Result<Page<BlogSearchPageDTO>> searchList(BlogSearchPageBO bo) {

        NativeSearchQueryBuilder nsb = new NativeSearchQueryBuilder();
        if (bo.getIsHfield() != null && bo.getIsHfield().equals(IsHfield.YES.getCode())) {
            // 高亮字段
            String heightField = "title";
            HighlightBuilder.Field hfield = new HighlightBuilder.Field(heightField).preTags("<mark>").postTags("</mark>");
            nsb.withHighlightFields(hfield);
        }
        // 评分排序（_source）
        nsb.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
        // 更新时间排序（gmtModified）
        nsb.withSort(new FieldSortBuilder("gmtModified").order(SortOrder.DESC));
        // 观看人数排序（readAcount）
        nsb.withSort(new FieldSortBuilder("readAcount").order(SortOrder.DESC));
        nsb.withPageable(PageRequest.of(bo.getPageCurrent() - 1, bo.getPageSize()));
        // 复合查询，外套boolQuery
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        // 精确查询termQuery不分词，must参数等价于AND
        if (bo.getBloggerUserNo() != null) {
            qb.must(QueryBuilders.termQuery("userNo", bo.getBloggerUserNo()));
        }
        if (bo.getArticleType() != null) {
            qb.must(QueryBuilders.termQuery("articleType", bo.getArticleType()));
        }
        if (StringUtils.hasText(bo.getTagsName())) {
            // 多字段匹配
            qb.must(QueryBuilders.multiMatchQuery(bo.getTagsName(), "tagsName", "summary").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));

        }
        qb.must(QueryBuilders.termQuery("isIssue", IsIssueEnum.YES.getCode()));
        qb.must(QueryBuilders.termQuery("isOpen", IsOpenEnum.YES.getCode()));
        // 模糊查询multiMatchQuery，最佳字段best_fields
        if (StringUtils.hasText(bo.getTitle())) {
            // 多字段匹配
            qb.must(QueryBuilders.multiMatchQuery(bo.getTitle(), "title", "summary").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));

        }
        nsb.withQuery(qb);
        SearchHits<EsBlog> page = elasticsearchRestTemplate.search(nsb.build(), EsBlog.class, IndexCoordinates.of(EsBlog.BLOG));
        return Result.success(EsPageUtil.transform(page, bo.getPageCurrent(), bo.getPageSize(), BlogSearchPageDTO.class));
    }
}
