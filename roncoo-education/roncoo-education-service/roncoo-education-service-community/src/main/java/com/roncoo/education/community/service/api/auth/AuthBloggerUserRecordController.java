package com.roncoo.education.community.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserAttentionUserSaveBO;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserRecordFansListBO;
import com.roncoo.education.community.common.bo.auth.AuthBloggerUserRecordListBO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerFansListDTO;
import com.roncoo.education.community.common.dto.auth.AuthBloggerUserRecordListDTO;
import com.roncoo.education.community.service.api.auth.biz.AuthBloggerUserRecordBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博主与用户关注关联表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/auth/blogger/user/record")
public class AuthBloggerUserRecordController {

	@Autowired
	private AuthBloggerUserRecordBiz biz;

	/**
	 * 博主关注或者取消关注接口
	 *
	 * @param bloggerUserAttentionUserSaveBO
	 * @return
	 */
	@ApiOperation(value = "博主关注或者取消关注接口", notes = "用户对博主关注或者取消关注")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthBloggerUserAttentionUserSaveBO authBloggerUserAttentionUserSaveBO) {
		return biz.save(authBloggerUserAttentionUserSaveBO);
	}

	/**
	 * 查看我关注的博主接口
	 *
	 * @param authBloggerUserRecordListBO
	 * @return
	 */
	@ApiOperation(value = "查看我关注的博主接口", notes = "查看我关注的博主集合信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthBloggerUserRecordListDTO>> list(@RequestBody AuthBloggerUserRecordListBO authBloggerUserRecordListBO) {
		return biz.list(authBloggerUserRecordListBO);
	}

	/**
	 * 查看关注我的粉丝接口
	 *
	 * @param bloggerUserAttentionUserAttBO
	 * @return
	 */
	@ApiOperation(value = "查看关注我的粉丝接口", notes = "查看关注的我的博主集合信息")
	@RequestMapping(value = "/fans/list", method = RequestMethod.POST)
	public Result<Page<AuthBloggerFansListDTO>> fansList(@RequestBody AuthBloggerUserRecordFansListBO authBloggerUserRecordFansListBO) {
		return biz.fansList(authBloggerUserRecordFansListBO);
	}
}
