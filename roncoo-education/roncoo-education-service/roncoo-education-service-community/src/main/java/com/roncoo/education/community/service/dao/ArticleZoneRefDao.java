package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample;

import java.util.List;

public interface ArticleZoneRefDao {
	int save(ArticleZoneRef record);

	int deleteById(Long id);

	int updateById(ArticleZoneRef record);

	int updateByExampleSelective(ArticleZoneRef record, ArticleZoneRefExample example);

	ArticleZoneRef getById(Long id);

	Page<ArticleZoneRef> listForPage(int pageCurrent, int pageSize, ArticleZoneRefExample example);

	/**
	 * 根据专区ID、文章ID、显示平台获取文章专区信息
	 *
	 * @param articleZoneId
	 * @param articleId
	 * @param platShow
	 * @return
	 */
	ArticleZoneRef getByArticleZoneIdAndArticleIdAndPlatShow(Long articleZoneId, Long articleId, Integer platShow);

	/**
	 * 根据专区ID、状态获取文章专区信息
	 *
	 * @param articleZoneId
	 * @param statusId
	 * @return
	 */
	List<ArticleZoneRef> listByArticleZoneIdAndAtatusId(Long articleZoneId, Integer statusId);
}
