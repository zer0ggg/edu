package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主与用户关注关联表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUserAttentionUserSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 博主用户编号
     */
    @ApiModelProperty(value = "博主用户编号")
    private Long bloggerUserNo;
    /**
     * 用户IP
     */
    @ApiModelProperty(value = "用户IP")
    private String userIp = "127.0.1";
    /**
     * 用户终端
     */
    @ApiModelProperty(value = "用户终端")
    private String userTerminal;

}
