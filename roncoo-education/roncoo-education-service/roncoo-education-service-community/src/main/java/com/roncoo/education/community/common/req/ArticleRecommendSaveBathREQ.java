package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 文章推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ArticleRecommendSaveBathREQ", description = "文章推荐  批量添加")
public class ArticleRecommendSaveBathREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "文章IDs不能为空")
    @ApiModelProperty(value = "文章IDs",required = true)
    private String articleIds;

}
