package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ArticleTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.common.bo.ArticleZoneRefListBO;
import com.roncoo.education.community.common.dto.ArticleZoneRefDTO;
import com.roncoo.education.community.common.dto.ArticleZoneRefListDTO;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@Component
public class ApiArticleZoneRefBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BlogDao blogDao;
	@Autowired
	private ArticleZoneRefDao articleZoneRefDao;

	public Result<ArticleZoneRefListDTO> list(ArticleZoneRefListBO bo) {
		if (StringUtils.isEmpty(bo.getArticleZoneId())) {
			return Result.error("文章专区ID");
		}
		List<ArticleZoneRef> articleZoneRefList = articleZoneRefDao.listByArticleZoneIdAndAtatusId(bo.getArticleZoneId(), StatusIdEnum.YES.getCode());
		ArticleZoneRefListDTO dto = new ArticleZoneRefListDTO();
		if (CollectionUtil.isNotEmpty(articleZoneRefList)) {
			List<ArticleZoneRefDTO> zoneRefList = new ArrayList<>();
			for (ArticleZoneRef articleZoneRef : articleZoneRefList) {
				// 获取文章信息
				getArticle(zoneRefList, articleZoneRef);
			}
			dto.setList(zoneRefList);
		}
		return Result.success(dto);

	}

	private void getArticle(List<ArticleZoneRefDTO> zoneRefList, ArticleZoneRef articleZoneRef) {
		// 获取文库文章信息
		Blog blog = blogDao.getById(articleZoneRef.getArticleId());
		if (ObjectUtil.isNull(blog)) {
			return;
		}
		ArticleZoneRefDTO zoneRefDto = BeanUtil.copyProperties(blog, ArticleZoneRefDTO.class);
		// 获取作者信息
		if (ArticleTypeEnum.BLOG.getCode().equals(blog.getArticleType())) {
			UserExtVO userExtVO = feignUserExt.getByUserNo(zoneRefDto.getUserNo());
			if (ObjectUtil.isNotNull(userExtVO)) {
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					zoneRefDto.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					zoneRefDto.setBloggerNickname(userExtVO.getNickname());
				}
				zoneRefDto.setBloggerUserImg(userExtVO.getHeadImgUrl());
			}

		}
		zoneRefList.add(zoneRefDto);
	}
}
