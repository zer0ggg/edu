package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.ArticleZoneRefListBO;
import com.roncoo.education.community.common.dto.ArticleZoneRefListDTO;
import com.roncoo.education.community.service.api.biz.ApiArticleZoneRefBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/article/zone/ref")
public class ApiArticleZoneRefController {

	@Autowired
	private ApiArticleZoneRefBiz biz;

	/**
	 * 资讯专区文章列表接口
	 *
	 * @param articleZoneRefListBO
	 */
	@ApiOperation(value = "资讯专区文章列表接口", notes = "资讯专区文章列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<ArticleZoneRefListDTO> list(@RequestBody ArticleZoneRefListBO articleZoneRefListBO) {
		return biz.list(articleZoneRefListBO);
	}

}
