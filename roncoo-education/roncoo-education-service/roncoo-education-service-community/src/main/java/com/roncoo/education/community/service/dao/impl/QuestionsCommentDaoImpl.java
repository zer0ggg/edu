package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.impl.mapper.QuestionsCommentMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsCommentExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionsCommentDaoImpl implements QuestionsCommentDao {
	@Autowired
	private QuestionsCommentMapper questionsCommentMapper;

	@Override
	public int save(QuestionsComment record) {
		record.setId(IdWorker.getId());
		return this.questionsCommentMapper.insertSelective(record);
	}

	@Override
	public int deleteById(Long id) {
		return this.questionsCommentMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateById(QuestionsComment record) {
		return this.questionsCommentMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByExampleSelective(QuestionsComment record, QuestionsCommentExample example) {
		return this.questionsCommentMapper.updateByExampleSelective(record, example);
	}

	@Override
	public QuestionsComment getById(Long id) {
		return this.questionsCommentMapper.selectByPrimaryKey(id);
	}

	@Override
	public Page<QuestionsComment> listForPage(int pageCurrent, int pageSize, QuestionsCommentExample example) {
		int count = this.questionsCommentMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<QuestionsComment>(count, totalPage, pageCurrent, pageSize, this.questionsCommentMapper.selectByExample(example));
	}

	@Override
	public List<QuestionsComment> listByParentIdAndStatusId(Long parentId, Integer statusId) {
		QuestionsCommentExample example = new QuestionsCommentExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(parentId);
		criteria.andStatusIdEqualTo(statusId);
		return this.questionsCommentMapper.selectByExample(example);
	}

	@Override
	public List<QuestionsComment> listByUserNo(Long userNo) {
		QuestionsCommentExample example = new QuestionsCommentExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		return this.questionsCommentMapper.selectByExample(example);
	}

	@Override
	public int deleteByQuestionId(Long quesionsId) {
		QuestionsCommentExample example = new QuestionsCommentExample();
		Criteria criteria = example.createCriteria();
		criteria.andQuestionsIdEqualTo(quesionsId);
		return this.questionsCommentMapper.deleteByExample(example);
	}

    @Override
    public QuestionsComment getByNewest() {
		QuestionsCommentExample example = new QuestionsCommentExample();
		Criteria criteria = example.createCriteria();
		criteria.andParentIdEqualTo(0L);
		example.setOrderByClause(" id desc ");
		List<QuestionsComment> list = this.questionsCommentMapper.selectByExampleWithBLOBs(example);
		if (CollUtil.isEmpty(list)){
			return null;
		}
		return list.get(0);
    }
}
