package com.roncoo.education.community.common.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Document(indexName = EsBlog.BLOG)
public class EsBlog implements Serializable {

	public static final String BLOG = "edu_blog";

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 用户编号
	 */
	private Long userNo;
	/**
	 * 用户头像
	 */
	private String bloggerUserImg;
	/**
	 * 用户昵称
	 */
	private String bloggerNickname;
	/**
	 * 是否存在LOGO(1,存在；0,不存在)
	 */
	private Integer isImg;
	/**
	 * 封面
	 */
	private String blogImg;
	/**
	 * 博客标题
	 */
	private String title;
	/**
	 * 文章类型(1:博客;2:资讯)
	 */
	private Integer articleType;
	/**
	 * 博客摘要
	 */
	private String summary;
	/**
	 * 博客标签
	 */
	private String tagsName;
	/**
	 * 质量度
	 */
	private Integer qualityScore;
	/**
	 * 博客关键词
	 */
	private String keywords;
	/**
	 * 是否公开博客(1公开，0不公开)
	 */
	private Integer isOpen;
	/**
	 * 是否已经发布(1发布，0没发布)
	 */
	private Integer isIssue;
	/**
	 * 博客类型(1原创，2转载)
	 */
	private Integer typeId;
	/**
	 * 是否置顶(1置顶，0否)
	 */
	private Integer isTop;
	/**
	 * 收藏人数
	 */
	private Integer collectionAcount;
	/**
	 * 点赞人数
	 */
	private Integer admireAcount;
	/**
	 * 评论人数
	 */
	private Integer commentAcount;
	/**
	 * 阅读人数
	 */
	private Integer readAcount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getUserNo() {
		return userNo;
	}

	public void setUserNo(Long userNo) {
		this.userNo = userNo;
	}

	public String getBloggerUserImg() {
		return bloggerUserImg;
	}

	public void setBloggerUserImg(String bloggerUserImg) {
		this.bloggerUserImg = bloggerUserImg;
	}

	public String getBloggerNickname() {
		return bloggerNickname;
	}

	public void setBloggerNickname(String bloggerNickname) {
		this.bloggerNickname = bloggerNickname;
	}

	public Integer getIsImg() {
		return isImg;
	}

	public void setIsImg(Integer isImg) {
		this.isImg = isImg;
	}

	public String getBlogImg() {
		return blogImg;
	}

	public void setBlogImg(String blogImg) {
		this.blogImg = blogImg;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getArticleType() {
		return articleType;
	}

	public void setArticleType(Integer articleType) {
		this.articleType = articleType;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTagsName() {
		return tagsName;
	}

	public void setTagsName(String tagsName) {
		this.tagsName = tagsName;
	}

	public Integer getQualityScore() {
		return qualityScore;
	}

	public void setQualityScore(Integer qualityScore) {
		this.qualityScore = qualityScore;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Integer getIsIssue() {
		return isIssue;
	}

	public void setIsIssue(Integer isIssue) {
		this.isIssue = isIssue;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getIsTop() {
		return isTop;
	}

	public void setIsTop(Integer isTop) {
		this.isTop = isTop;
	}

	public Integer getCollectionAcount() {
		return collectionAcount;
	}

	public void setCollectionAcount(Integer collectionAcount) {
		this.collectionAcount = collectionAcount;
	}

	public Integer getAdmireAcount() {
		return admireAcount;
	}

	public void setAdmireAcount(Integer admireAcount) {
		this.admireAcount = admireAcount;
	}

	public Integer getCommentAcount() {
		return commentAcount;
	}

	public void setCommentAcount(Integer commentAcount) {
		this.commentAcount = commentAcount;
	}

	public Integer getReadAcount() {
		return readAcount;
	}

	public void setReadAcount(Integer readAcount) {
		this.readAcount = readAcount;
	}

	@Override
	public String toString() {
		return "EsWeblog [id=" + id + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + ", sort=" + sort + ", userNo=" + userNo + ", bloggerUserImg=" + bloggerUserImg + ", bloggerNickname=" + bloggerNickname + ", isImg=" + isImg + ", blogImg=" + blogImg + ", title=" + title + ", articleType=" + articleType + ", summary=" + summary + ", tagsName=" + tagsName + ", qualityScore=" + qualityScore + ", keywords=" + keywords + ", isOpen=" + isOpen + ", isIssue=" + isIssue + ", typeId=" + typeId + ", isTop=" + isTop + ", collectionAcount=" + collectionAcount + ", admireAcount=" + admireAcount + ", commentAcount=" + commentAcount + ", readAcount=" + readAcount + "]";
	}

}
