package com.roncoo.education.community.common.bo.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博主信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthBloggerUserMyselfViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
