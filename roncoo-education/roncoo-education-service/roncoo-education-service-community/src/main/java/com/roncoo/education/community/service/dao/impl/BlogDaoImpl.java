package com.roncoo.education.community.service.dao.impl;

import cn.hutool.core.collection.CollUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.BlogMapper;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.BlogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BlogDaoImpl implements BlogDao {
    @Autowired
    private BlogMapper blogMapper;

    @Autowired
	private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Blog record) {
        return this.blogMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.blogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Blog record) {
        return this.blogMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(Blog record, BlogExample example) {
        return this.blogMapper.updateByExampleSelective(record, example);
    }

    @Override
    public Blog getById(Long id) {
        return this.blogMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Blog> listForPage(int pageCurrent, int pageSize, BlogExample example) {
        int count = this.blogMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<Blog>(count, totalPage, pageCurrent, pageSize, this.blogMapper.selectByExample(example));
    }

	@Override
	public Page<Blog> listByWeblogUserAttention(List<Long> userNos, int pageCurrent, int pageSize) {
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(*) from blog where is_issue = 1 and status_id = 1 and is_open = 1");
		countSql.append(" and");
		countSql.append(" user_no = " + userNos.get(0));
		if (userNos.size() > 1) {
			for (int i = 1; i < userNos.size(); i++) {
				countSql.append(" or");
				countSql.append(" user_no = " + userNos.get(i));
			}
		}
		Integer result = jdbcTemplate.queryForObject(countSql.toString(), Integer.class);
		int count = result;
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);

		StringBuilder selectSql = new StringBuilder();
		selectSql.append("select * from blog where is_issue = 1 and status_id = 1 and is_open = 1");
		selectSql.append(" and");
		selectSql.append(" user_no = " + userNos.get(0));
		if (userNos.size() > 1) {
			for (int i = 1; i < userNos.size(); i++) {
				selectSql.append(" or");
				selectSql.append(" user_no = " + userNos.get(i));
			}
		}
		selectSql.append(" order by is_top desc, sort asc, gmt_modified desc, id desc");
		String sql = selectSql.toString() + PageUtil.limitSql(count, pageCurrent, pageSize);
		List<Blog> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Blog>(Blog.class));
		return new Page<Blog>(count, totalPage, pageCurrent, pageSize, list);
	}



	@Override
	public List<Blog> listByUserNoAndIsOpenAndIsIssue(Long bloggerUserNo, Integer isOpen, Integer isIssue) {
		BlogExample example = new BlogExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(bloggerUserNo);
		criteria.andIsOpenEqualTo(isOpen);
		criteria.andIsIssueEqualTo(isIssue);
		return this.blogMapper.selectByExample(example);
	}

	@Override
	public List<Blog> listByTitle(String title) {
		BlogExample example = new BlogExample();
		Criteria criteria = example.createCriteria();
		criteria.andTitleLike(PageUtil.like(title));
		return this.blogMapper.selectByExample(example);
	}

    @Override
    public List<Blog> listByArticleType(Integer articleType) {
        BlogExample example = new BlogExample();
        Criteria criteria = example.createCriteria();
        criteria.andArticleTypeEqualTo(articleType);
        List<Blog> blogList = this.blogMapper.selectByExample(example);
        if (CollUtil.isEmpty(blogList)){
            return null;
        }
        return blogList;
    }

    @Override
    public List<Blog> getBlogList() {
        BlogExample example = new BlogExample();
        List<Blog> blogList = this.blogMapper.selectByExample(example);
        if (CollUtil.isEmpty(blogList)){
            return null;
        }
        return blogList;
    }

    @Override
    public List<Blog> listByStatusId(Integer statusId) {
        BlogExample example = new BlogExample();
        Criteria criteria = example.createCriteria();
        criteria.andStatusIdEqualTo(statusId);
        List<Blog> blogList = this.blogMapper.selectByExample(example);
        if (CollUtil.isEmpty(blogList)){
            return null;
        }
        return blogList;
    }

    @Override
    public Blog getByIdAndStatusId(Long id ,Integer statusId) {
        BlogExample example = new BlogExample();
        Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(id);
        criteria.andStatusIdEqualTo(statusId);
        List<Blog> blogList = this.blogMapper.selectByExample(example);
        return CollUtil.isEmpty(blogList) ? null : blogList.get(0);
    }


}
