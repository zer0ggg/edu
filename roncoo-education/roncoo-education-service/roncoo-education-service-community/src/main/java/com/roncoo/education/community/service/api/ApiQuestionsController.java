package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.QuestionsPageBO;
import com.roncoo.education.community.common.bo.QuestionsSearchPageBO;
import com.roncoo.education.community.common.bo.QuestionsViewBO;
import com.roncoo.education.community.common.dto.QuestionsPageDTO;
import com.roncoo.education.community.common.dto.QuestionsSearchPageDTO;
import com.roncoo.education.community.common.dto.QuestionsViewDTO;
import com.roncoo.education.community.service.api.biz.ApiQuestionsBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 问答信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/questions")
public class ApiQuestionsController {

    @Autowired
    private ApiQuestionsBiz biz;

    /**
     * 问答列表展示接口
     * @param blogPageBO
     * @return
     */
	@ApiOperation(value = "问答列表展示接口", notes = "问答列表展示接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<QuestionsPageDTO>> list(@RequestBody QuestionsPageBO questionsPageBO) {
		return biz.list(questionsPageBO);
	}

	/**
     * 问答详情接口
     * @return
     */
	@ApiOperation(value = "问答详情接口", notes = "问答详情接口")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<QuestionsViewDTO> list(@RequestBody QuestionsViewBO questionsViewBO) {
		return biz.view(questionsViewBO);
	}

	/**
	 * ES搜索问答接口
	 * @return
	 */
	@ApiOperation(value = "搜索问答接口", notes = "ES搜索问答接口")
	@RequestMapping(value = "/search/list", method = RequestMethod.POST)
	public Result<Page<QuestionsSearchPageDTO>> searchList(@RequestBody QuestionsSearchPageBO questionsSearchPageBO) {
		return biz.searchList(questionsSearchPageBO);
	}


}
