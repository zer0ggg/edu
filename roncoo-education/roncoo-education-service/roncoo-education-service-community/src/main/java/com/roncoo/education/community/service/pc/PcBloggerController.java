package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.BloggerBatchSaveREQ;
import com.roncoo.education.community.common.req.BloggerEditREQ;
import com.roncoo.education.community.common.req.BloggerListREQ;
import com.roncoo.education.community.common.req.BloggerSaveREQ;
import com.roncoo.education.community.common.resp.BloggerListRESP;
import com.roncoo.education.community.common.resp.BloggerViewRESP;
import com.roncoo.education.community.service.pc.biz.PcBloggerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 博主信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/blogger")
@Api(value = "community-博主信息", tags = {"community-博主信息"})
public class PcBloggerController {

    @Autowired
    private PcBloggerBiz biz;

    @ApiOperation(value = "博主信息列表", notes = "博主信息列表")
    @PostMapping(value = "/list")
    public Result<Page<BloggerListRESP>> list(@RequestBody BloggerListREQ bloggerListREQ) {
        return biz.list(bloggerListREQ);
    }


    @ApiOperation(value = "博主信息添加", notes = "博主信息添加")
    @SysLog(value = "博主信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody BloggerSaveREQ bloggerSaveREQ) {
        return biz.save(bloggerSaveREQ);
    }


    @ApiOperation(value = "博主信息批量添加", notes = "博主信息批量添加")
    @SysLog(value = "博主信息批量添加")
    @PostMapping(value = "/batch/save")
    public Result<String> batchSave(@RequestBody BloggerBatchSaveREQ bloggerBatchSaveREQ) {
        return biz.batchSave(bloggerBatchSaveREQ);
    }

    @ApiOperation(value = "博主信息查看", notes = "博主信息查看")
    @GetMapping(value = "/view")
    public Result<BloggerViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "博主信息修改", notes = "博主信息修改")
    @SysLog(value = "博主信息删除")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody BloggerEditREQ bloggerEditREQ) {
        return biz.edit(bloggerEditREQ);
    }


    @ApiOperation(value = "博主信息删除", notes = "博主信息删除")
    @SysLog(value = "博主信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
