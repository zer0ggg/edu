package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 博客评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BlogCommentListREQ", description = "博客评论列表")
public class BlogCommentListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "博客ID")
    private Long blogId;

    @ApiModelProperty(value = "状态(1:正常，2:禁用)")
    private Integer statusId;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
