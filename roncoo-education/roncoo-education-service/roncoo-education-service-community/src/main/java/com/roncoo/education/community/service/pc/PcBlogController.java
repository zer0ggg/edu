package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.*;
import com.roncoo.education.community.common.resp.BlogListRESP;
import com.roncoo.education.community.common.resp.BlogViewRESP;
import com.roncoo.education.community.service.pc.biz.PcBlogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 博客信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/blog")
@Api(value = "community-博客信息", tags = {"community-博客信息"})
public class PcBlogController {

    @Autowired
    private PcBlogBiz biz;

    @ApiOperation(value = "博客信息列表", notes = "博客信息列表")
    @PostMapping(value = "/list")
    public Result<Page<BlogListRESP>> list(@RequestBody BlogListREQ blogListREQ) {
        return biz.list(blogListREQ);
    }


    @ApiOperation(value = "博客信息添加", notes = "博客信息添加")
    @SysLog(value = "博客信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody BlogSaveREQ blogSaveREQ) {
        return biz.save(blogSaveREQ);
    }

    @ApiOperation(value = "博客信息查看", notes = "博客信息查看")
    @GetMapping(value = "/view")
    public Result<BlogViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "博客信息修改", notes = "博客信息修改")
    @SysLog(value = "博客信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody BlogEditREQ blogEditREQ) {
        return biz.edit(blogEditREQ);
    }

    @SysLog(value = "博客信息状态修改")
    @ApiOperation(value = "博客信息状态修改", notes = "博客信息状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody BlogUpdateStatusREQ blogUpdateStatusREQ) {
        return biz.updateStatus(blogUpdateStatusREQ);
    }


    @ApiOperation(value = "博客信息添加Es", notes = "博客信息添加ES")
    @SysLog(value = "博客信息添加Es")
    @PostMapping(value = "/add/es")
    public Result<String> addEs(@RequestBody BlogAddEsREQ blogAddEsREQ) {
        return biz.addEs(blogAddEsREQ);
    }


    @ApiOperation(value = "博客信息删除", notes = "博客信息删除")
    @SysLog(value = "博客信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "上传文件", notes = "上传文件")
    @SysLog(value = "博客信息上传文件")
    @PostMapping(value = "/upload")
    public Result<String> upload(@RequestParam("file") MultipartFile multipartFile) {
        return biz.upload(multipartFile);
    }
}
