package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.BloggerUserRecordEditREQ;
import com.roncoo.education.community.common.req.BloggerUserRecordListREQ;
import com.roncoo.education.community.common.req.BloggerUserRecordSaveREQ;
import com.roncoo.education.community.common.resp.BloggerUserRecordListRESP;
import com.roncoo.education.community.common.resp.BloggerUserRecordViewRESP;
import com.roncoo.education.community.service.pc.biz.PcBloggerUserRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 博主与用户关注关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/blogger/user/record")
@Api(value = "community-博主与用户关注关联", tags = {"community-博主与用户关注关联"})
public class PcBloggerUserRecordController {

    @Autowired
    private PcBloggerUserRecordBiz biz;

    @ApiOperation(value = "博主与用户关注关联列表", notes = "博主与用户关注关联列表")
    @PostMapping(value = "/list")
    public Result<Page<BloggerUserRecordListRESP>> list(@RequestBody BloggerUserRecordListREQ bloggerUserRecordListREQ) {
        return biz.list(bloggerUserRecordListREQ);
    }


    @ApiOperation(value = "博主与用户关注关联添加", notes = "博主与用户关注关联添加")
    @SysLog(value = "博主与用户关注关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody BloggerUserRecordSaveREQ bloggerUserRecordSaveREQ) {
        return biz.save(bloggerUserRecordSaveREQ);
    }

    @ApiOperation(value = "博主与用户关注关联查看", notes = "博主与用户关注关联查看")
    @GetMapping(value = "/view")
    public Result<BloggerUserRecordViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "博主与用户关注关联修改", notes = "博主与用户关注关联修改")
    @SysLog(value = "博主与用户关注关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody BloggerUserRecordEditREQ bloggerUserRecordEditREQ) {
        return biz.edit(bloggerUserRecordEditREQ);
    }


    @ApiOperation(value = "博主与用户关注关联删除", notes = "博主与用户关注关联删除")
    @SysLog(value = "博主与用户关注关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
