package com.roncoo.education.community.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.feign.interfaces.IFeignBlogUserRecord;
import com.roncoo.education.community.feign.qo.BlogUserRecordQO;
import com.roncoo.education.community.feign.vo.BlogUserRecordVO;
import com.roncoo.education.community.service.feign.biz.FeignBlogUserRecordBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客与用户关联表
 *
 * @author wujing
 */
@RestController
public class FeignBlogUserRecordController extends BaseController implements IFeignBlogUserRecord {

	@Autowired
	private FeignBlogUserRecordBiz biz;

	@Override
	public Page<BlogUserRecordVO> listForPage(@RequestBody BlogUserRecordQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody BlogUserRecordQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody BlogUserRecordQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public BlogUserRecordVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

}
