package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 博主与用户关注关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BloggerUserRecordListREQ", description = "博主与用户关注关联列表")
public class BloggerUserRecordListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "博主用户编号")
    private Long bloggerUserNo;

    @ApiModelProperty(value = "操作类型(1收藏，2点赞)")
    private Integer opType;

    @ApiModelProperty(value = "关注者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "开始创建时间")
    private String beginGmtCreate;

    @ApiModelProperty(value = "结束创建时间")
    private String endGmtCreate;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
