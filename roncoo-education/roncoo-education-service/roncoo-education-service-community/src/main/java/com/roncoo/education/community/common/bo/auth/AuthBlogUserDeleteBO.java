package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 博客删除
 *
 */
@Data
@Accessors(chain = true)
public class AuthBlogUserDeleteBO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 博客ID
	 */
	@ApiModelProperty(value = "博客ID", required = true)
	private Long id;
}
