package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.community.common.bo.ArticleZoneCategoryPageBO;
import com.roncoo.education.community.common.dto.ArticleZoneCategoryDTO;
import com.roncoo.education.community.common.dto.ArticleZoneCategoryListDTO;
import com.roncoo.education.community.service.dao.ArticleZoneCategoryDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文章专区首页分类
 *
 * @author wujing
 */
@Component
public class ApiArticleZoneCategoryBiz {

	@Autowired
	private ArticleZoneCategoryDao articleZoneCategoryDao;

	public Result<ArticleZoneCategoryListDTO> list(ArticleZoneCategoryPageBO bo) {
		if (bo.getPlatShow() == null) {
			return Result.error("platShow不能为空");
		}
		ArticleZoneCategoryListDTO dto = new ArticleZoneCategoryListDTO();
		// 获取专区信息
		List<ArticleZoneCategory> list = articleZoneCategoryDao.listByPlatShowAndStatusId(bo.getPlatShow(), StatusIdEnum.YES.getCode());
		if (CollectionUtil.isNotEmpty(list)) {
			dto.setList(ArrayListUtil.copy(list, ArticleZoneCategoryDTO.class));
		}
		return Result.success(dto);

	}

}
