package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.BlogPageBO;
import com.roncoo.education.community.common.bo.BlogSearchPageBO;
import com.roncoo.education.community.common.bo.BlogViewBO;
import com.roncoo.education.community.common.dto.BlogPageDTO;
import com.roncoo.education.community.common.dto.BlogSearchPageDTO;
import com.roncoo.education.community.common.dto.BlogViewDTO;
import com.roncoo.education.community.service.api.biz.ApiBlogBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博客信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/blog")
public class ApiBlogController {

	@Autowired
	private ApiBlogBiz biz;

	/**
	 * 博客信息列表展示接口
	 *
	 * @return
	 */
	@ApiOperation(value = "博客信息列表展示接口", notes = "博客信息列表展示(需要查看该标签的博客传tagsName，需要查看该博客栏目的博客传sections，需要查看该博主的博客传bloggerUserNo)")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<BlogPageDTO>> list(@RequestBody BlogPageBO blogPageBO) {
		return biz.list(blogPageBO);
	}

	/**
	 * 博客信息详情查看接口
	 *
	 * @return
	 */
	@ApiOperation(value = "博客信息详情查看接口", notes = "博客信息详情查看")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<BlogViewDTO> view(@RequestBody BlogViewBO blogViewBO) {
		return biz.view(blogViewBO);
	}


	/**
	 * 搜索博客接口
	 *
	 * @param blogSearchPageBO
	 * @return
	 */
	@ApiOperation(value = "搜索博客接口", notes = "根据博客标题查询博客信息列表")
	@RequestMapping(value = "/search/list", method = RequestMethod.POST)
	public Result<Page<BlogSearchPageDTO>> searchList(@RequestBody BlogSearchPageBO blogSearchPageBO) {
		return biz.searchList(blogSearchPageBO);
	}


}
