package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.BlogCommentStatusDelREQ;
import com.roncoo.education.community.common.req.QuestionsCommentEditREQ;
import com.roncoo.education.community.common.req.QuestionsCommentListREQ;
import com.roncoo.education.community.common.req.QuestionsCommentSaveREQ;
import com.roncoo.education.community.common.resp.QuestionsCommentListRESP;
import com.roncoo.education.community.common.resp.QuestionsCommentViewRESP;
import com.roncoo.education.community.service.pc.biz.PcQuestionsCommentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 问答评论 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/questions/comment")
@Api(value = "community-问答评论", tags = {"community-问答评论"})
public class PcQuestionsCommentController {

    @Autowired
    private PcQuestionsCommentBiz biz;

    @ApiOperation(value = "问答评论列表", notes = "问答评论列表")
    @PostMapping(value = "/list")
    public Result<Page<QuestionsCommentListRESP>> list(@RequestBody QuestionsCommentListREQ questionsCommentListREQ) {
        return biz.list(questionsCommentListREQ);
    }


    @ApiOperation(value = "问答评论添加", notes = "问答评论添加")
    @SysLog(value = "问答评论添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody QuestionsCommentSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "问答评论查看", notes = "问答评论查看")
    @GetMapping(value = "/view")
    public Result<QuestionsCommentViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "问答评论修改", notes = "问答评论修改")
    @SysLog(value = "问答评论修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody QuestionsCommentEditREQ req) {
        return biz.edit(req);
    }


    @ApiOperation(value = "问答评论删除", notes = "问答评论删除")
    @SysLog(value = "问答评论删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "评论状态删除", notes = "评论状态删除")
    @SysLog(value = "评论状态删除")
    @PostMapping(value = "/del")
    public Result<String> statusDel(@RequestBody BlogCommentStatusDelREQ req) {
        return biz.statusDel(req);
    }

}
