package com.roncoo.education.community.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class Blogger implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private String introduction;

    private Integer fansAccount;

    private Integer attentionAcount;

    private Integer weblogAccount;

    private Integer informationAccount;

    private Integer questionsAccount;

    private Integer answerAccount;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }

    public Integer getFansAccount() {
        return fansAccount;
    }

    public void setFansAccount(Integer fansAccount) {
        this.fansAccount = fansAccount;
    }

    public Integer getAttentionAcount() {
        return attentionAcount;
    }

    public void setAttentionAcount(Integer attentionAcount) {
        this.attentionAcount = attentionAcount;
    }

    public Integer getWeblogAccount() {
        return weblogAccount;
    }

    public void setWeblogAccount(Integer weblogAccount) {
        this.weblogAccount = weblogAccount;
    }

    public Integer getInformationAccount() {
        return informationAccount;
    }

    public void setInformationAccount(Integer informationAccount) {
        this.informationAccount = informationAccount;
    }

    public Integer getQuestionsAccount() {
        return questionsAccount;
    }

    public void setQuestionsAccount(Integer questionsAccount) {
        this.questionsAccount = questionsAccount;
    }

    public Integer getAnswerAccount() {
        return answerAccount;
    }

    public void setAnswerAccount(Integer answerAccount) {
        this.answerAccount = answerAccount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", introduction=").append(introduction);
        sb.append(", fansAccount=").append(fansAccount);
        sb.append(", attentionAcount=").append(attentionAcount);
        sb.append(", weblogAccount=").append(weblogAccount);
        sb.append(", informationAccount=").append(informationAccount);
        sb.append(", questionsAccount=").append(questionsAccount);
        sb.append(", answerAccount=").append(answerAccount);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}