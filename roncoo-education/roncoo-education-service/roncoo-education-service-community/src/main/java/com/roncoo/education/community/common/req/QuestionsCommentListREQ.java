package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 问答评论
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="QuestionsCommentListREQ", description="问答评论列表")
public class QuestionsCommentListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "问题ID")
    private Long questionsId;

    @ApiModelProperty(value = "状态(1:正常，2:禁用)")
    private Integer statusId;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
