package com.roncoo.education.community.service.dao.impl.mapper;

import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleRecommendMapper {
    int countByExample(ArticleRecommendExample example);

    int deleteByExample(ArticleRecommendExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ArticleRecommend record);

    int insertSelective(ArticleRecommend record);

    List<ArticleRecommend> selectByExample(ArticleRecommendExample example);

    ArticleRecommend selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ArticleRecommend record, @Param("example") ArticleRecommendExample example);

    int updateByExample(@Param("record") ArticleRecommend record, @Param("example") ArticleRecommendExample example);

    int updateByPrimaryKeySelective(ArticleRecommend record);

    int updateByPrimaryKey(ArticleRecommend record);
}
