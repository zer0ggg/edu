package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.community.feign.qo.ArticleRecommendQO;
import com.roncoo.education.community.feign.vo.ArticleRecommendVO;
import com.roncoo.education.community.service.dao.ArticleRecommendDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Component
public class FeignArticleRecommendBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private BlogDao blogDao;
	@Autowired
	private ArticleRecommendDao dao;

	public Page<ArticleRecommendVO> listForPage(ArticleRecommendQO qo) {
		ArticleRecommendExample example = new ArticleRecommendExample();
		Criteria c = example.createCriteria();
		if (qo.getArticleType() != null) {
			c.andArticleTypeEqualTo(qo.getArticleType());
		}
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		}
		example.setOrderByClause("status_id desc, sort asc, id desc ");
		Page<ArticleRecommend> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<ArticleRecommendVO> listForPage = PageUtil.transform(page, ArticleRecommendVO.class);

		for (ArticleRecommendVO vo : listForPage.getList()) {
			// 获取博客信息
			Blog blog = blogDao.getById(vo.getArtcleId());
			if (ObjectUtil.isNotNull(blog)) {
				vo.setUserNo(blog.getUserNo());
				vo.setTitle(blog.getTitle());
				// 获取作者信息
				UserExtVO userExtVO = feignUserExt.getByUserNo(blog.getUserNo());
				if (StringUtils.isEmpty(userExtVO.getNickname())) {
					vo.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
				} else {
					vo.setBloggerNickname(userExtVO.getNickname());
				}
			}
		}
		return listForPage;
	}

	public int save(ArticleRecommendQO qo) {
		if (qo.getArtcleId() == null) {
			throw new BaseException("ArtcleId不能为空");
		}
		if (qo.getArticleType() == null) {
			throw new BaseException("ArticleType不能为空");
		}
		ArticleRecommend articleRecommend = dao.getByArtcleId(qo.getArtcleId());
		if (ObjectUtil.isNotNull(articleRecommend)) {
			throw new BaseException("已添加文章推荐");
		}
		ArticleRecommend record = BeanUtil.copyProperties(qo, ArticleRecommend.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ArticleRecommendVO getById(Long id) {
		ArticleRecommend record = dao.getById(id);
		return BeanUtil.copyProperties(record, ArticleRecommendVO.class);
	}

	public int updateById(ArticleRecommendQO qo) {
		ArticleRecommend record = BeanUtil.copyProperties(qo, ArticleRecommend.class);
		return dao.updateById(record);
	}

}
