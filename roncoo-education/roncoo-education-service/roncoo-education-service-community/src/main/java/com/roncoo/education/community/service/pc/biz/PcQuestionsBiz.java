package com.roncoo.education.community.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.IsGoodEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.common.es.EsQuestions;
import com.roncoo.education.community.common.req.QuestionsEditREQ;
import com.roncoo.education.community.common.req.QuestionsListREQ;
import com.roncoo.education.community.common.req.QuestionsSaveREQ;
import com.roncoo.education.community.common.req.QuestionsStatusUpdateREQ;
import com.roncoo.education.community.common.resp.QuestionsListRESP;
import com.roncoo.education.community.common.resp.QuestionsViewRESP;
import com.roncoo.education.community.service.dao.QuestionsCommentDao;
import com.roncoo.education.community.service.dao.QuestionsDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Questions;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsComment;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.QuestionsExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 问答信息
 *
 * @author wujing
 */
@Component
public class PcQuestionsBiz extends BaseBiz {

    @Autowired
    private QuestionsDao dao;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private QuestionsCommentDao questionsCommentDao;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 问答信息列表
     *
     * @param req 问答信息分页查询参数
     * @return 问答信息分页查询结果
     */
    public Result<Page<QuestionsListRESP>> list(QuestionsListREQ req) {
        QuestionsExample example = new QuestionsExample();
        Criteria c = example.createCriteria();
        if (req.getUserNo() != null) {
            c.andUserNoEqualTo(req.getUserNo());
        }
        if (StringUtils.hasText(req.getTagsName())) {
            c.andTagsNameLike(PageUtil.like(req.getTagsName()));
        }
        if (!StringUtils.isEmpty(req.getTitle())) {
            c.andTitleLike(PageUtil.like(req.getTitle()));
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        } else {
            c.andStatusIdLessThan(Constants.FREEZE);
        }
        if (req.getIsTop() != null) {
            c.andIsTopEqualTo(req.getIsTop());
        }
        if (StringUtils.hasText(req.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(req.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.hasText(req.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(req.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause(" is_top desc,sort asc,status_id desc,id desc ");
        Page<Questions> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<QuestionsListRESP> respPage = PageUtil.transform(page, QuestionsListRESP.class);
        for (QuestionsListRESP resp : respPage.getList()) {
			/*if (resp.getCourseId() != null) {
				 CourseVO courseVO = feignCourse.getById(vo.getCourseId());
				 if (ObjectUtil.isNotNull(courseVO)) {
				 vo.setCourseName(courseVO.getCourseName());
				 }
			}*/
            // 获取用户信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(resp.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    resp.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7, userExtVO.getMobile().length()));
                } else {
                    resp.setNickname(userExtVO.getNickname());
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 问答信息添加
     *
     * @param questionsSaveREQ 问答信息
     * @return 添加结果
     */
    public Result<String> save(QuestionsSaveREQ questionsSaveREQ) {
        Questions record = BeanUtil.copyProperties(questionsSaveREQ, Questions.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 问答信息查看
     *
     * @param id 主键ID
     * @return 问答信息
     */
    public Result<QuestionsViewRESP> view(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        Questions record = dao.getById(id);
        QuestionsViewRESP resp = BeanUtil.copyProperties(record, QuestionsViewRESP.class);
        // 存在最佳回答
        if (IsGoodEnum.YES.getCode().equals(resp.getIsGood())) {
            // 查询评论信息
            QuestionsComment questionsComment = questionsCommentDao.getById(resp.getGoodCommentId());
            if (ObjectUtil.isNull(questionsComment)) {
                return Result.error("评论表里查询不到最佳评论");
            }
        }
        return Result.success(resp);
    }

    /**
     * 问答信息修改
     *
     * @param req 问答信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(QuestionsEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Questions questions = dao.getById(req.getId());
        if (ObjectUtil.isNull(questions)) {
            return Result.error("找不到问题信息");
        }
        // 处理标签
//        if(CollectionUtils.isEmpty(req.getLabelNameArr())){
//            StringBuilder stringBuilder = new
//        }
        // 根据userNo查找用户教育信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(questions.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系管理员");
        }
        Questions record = BeanUtil.copyProperties(req, Questions.class);
        int results = dao.updateById(record);
        if (results > 0) {
            if (StatusIdEnum.YES.getCode().equals(record.getStatusId())) {
                try {
                    // 插入els或者更新els
                    EsQuestions esQuestions = BeanUtil.copyProperties(dao.getById(questions.getId()), EsQuestions.class);
                    esQuestions.setHeadImgUrl(userExtVO.getHeadImgUrl());
                    if (org.springframework.util.StringUtils.isEmpty(userExtVO.getNickname())) {
                        esQuestions.setNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7, userExtVO.getMobile().length()));
                    } else {
                        esQuestions.setNickname(userExtVO.getNickname());
                    }
                    IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                    elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsQuestions.QUESTIONS));
                } catch (Exception e) {
                    logger.error("elasticsearch更新数据失败", e);
                    throw new BaseException("编辑失败");
                }
            }
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 问答信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        Questions bean = new Questions();
        bean.setId(id);
        bean.setStatusId(Constants.FREEZE);
        if (dao.updateById(bean) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> updateStatusId(QuestionsStatusUpdateREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Questions record = BeanUtil.copyProperties(req, Questions.class);
        if (dao.updateById(record) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> addEs() {
        try {
            List<IndexQuery> queries = new ArrayList<>();
            // 查询全部，删除索引重建
            List<Questions> list = dao.listByStatusId(StatusIdEnum.YES.getCode());
            for (Questions questions : list) {
                EsQuestions esQuestions = BeanUtil.copyProperties(questions, EsQuestions.class);
                IndexQuery query = new IndexQueryBuilder().withObject(esQuestions).build();
                queries.add(query);
            }
            elasticsearchRestTemplate.indexOps(EsQuestions.class).delete();
            elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsQuestions.QUESTIONS));
            return Result.success("添加ES成功");
        } catch (Exception e) {
            logger.error("elasticsearch导入数据失败", e);
            return Result.error("添加ES失败");
        }
    }
}
