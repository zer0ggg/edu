package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 博客信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "BlogAddEsREQ", description = "博客信息导入ES")
public class BlogAddEsREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章类型(1:博客;2:资讯)", required = true)
    private Integer articleType;
}
