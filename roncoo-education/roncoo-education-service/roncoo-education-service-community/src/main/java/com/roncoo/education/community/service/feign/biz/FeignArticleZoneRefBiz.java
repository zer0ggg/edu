package com.roncoo.education.community.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.community.feign.qo.ArticleZoneRefQO;
import com.roncoo.education.community.feign.vo.ArticleZoneRefVO;
import com.roncoo.education.community.service.dao.ArticleZoneRefDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRef;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneRefExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 文章专区关联表
 *
 * @author wujing
 */
@Component
public class FeignArticleZoneRefBiz {
    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private BlogDao blogDao;
    @Autowired
    private ArticleZoneRefDao dao;

    public Page<ArticleZoneRefVO> listForPage(ArticleZoneRefQO qo) {
        if (qo.getArticleZoneId() == null) {
            throw new BaseException("专区ID不能为空");
        }
        if (qo.getPlatShow() == null) {
            throw new BaseException("显示平台不能为空");
        }
        ArticleZoneRefExample example = new ArticleZoneRefExample();
        Criteria c = example.createCriteria();
        c.andArticleZoneIdEqualTo(qo.getArticleZoneId());
        c.andPlatShowEqualTo(qo.getPlatShow());
        if (StringUtils.isNotBlank(qo.getBeginGmtCreate())) {
            c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(qo.getBeginGmtCreate(), "yyyy-MM-dd"));
        }
        if (StringUtils.isNotBlank(qo.getEndGmtCreate())) {
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(qo.getEndGmtCreate(), "yyyy-MM-dd"), 1));
        }
        example.setOrderByClause("status_id desc, sort asc,  id desc ");
        Page<ArticleZoneRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<ArticleZoneRefVO> listForPage = PageUtil.transform(page, ArticleZoneRefVO.class);
        for (ArticleZoneRefVO vo : listForPage.getList()) {
            // 获取文章信息
            getArticle(vo);
        }
        return listForPage;
    }

    public int save(ArticleZoneRefQO qo) {
        // 根据文章专区ID和文章ID查询文章专区关联表 （专区里面不能存在重复的文章）
        ArticleZoneRef articleZoneRef = dao.getByArticleZoneIdAndArticleIdAndPlatShow(qo.getArticleZoneId(), qo.getArticleId(), qo.getPlatShow());
        if (!ObjectUtils.isEmpty(articleZoneRef)) {
            throw new BaseException("专区已存在该文章，请勿重复添加");
        }
        ArticleZoneRef record = BeanUtil.copyProperties(qo, ArticleZoneRef.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public ArticleZoneRefVO getById(Long id) {
        ArticleZoneRef record = dao.getById(id);
        return BeanUtil.copyProperties(record, ArticleZoneRefVO.class);
    }

    public int updateById(ArticleZoneRefQO qo) {
        ArticleZoneRef record = BeanUtil.copyProperties(qo, ArticleZoneRef.class);
        return dao.updateById(record);
    }

    @Transactional(rollbackFor = Exception.class)
    public int remove(String ids) {
        if (StringUtils.isNotBlank(ids)) {
            String[] id = ids.split(",");
            for (String i : id) {
                dao.deleteById(Long.valueOf(i));
            }
        }
        return 1;
    }

    private void getArticle(ArticleZoneRefVO vo) {
        Blog blog = blogDao.getById(vo.getArticleId());
        if (ObjectUtil.isNotNull(blog)) {
            // 获取作者信息
            vo.setUserNo(blog.getUserNo());
            vo.setTitle(blog.getTitle());
            UserExtVO userExtVO = feignUserExt.getByUserNo(blog.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    vo.setBloggerNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    vo.setBloggerNickname(userExtVO.getNickname());
                }
            }
        }
    }
}
