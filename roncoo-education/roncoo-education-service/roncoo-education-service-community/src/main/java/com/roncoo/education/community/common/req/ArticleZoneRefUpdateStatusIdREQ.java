package com.roncoo.education.community.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 文章专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ArticleZoneRefEditREQ", description="文章专区关联修改状态")
public class ArticleZoneRefUpdateStatusIdREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id",required = true)
    private Long id;

    @NotNull(message = "状态不能为空")
    @ApiModelProperty(value = "状态(1:正常，0:禁用)",required = true)
    private Integer statusId;

}
