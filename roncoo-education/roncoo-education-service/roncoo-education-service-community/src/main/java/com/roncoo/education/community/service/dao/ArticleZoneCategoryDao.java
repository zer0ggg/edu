package com.roncoo.education.community.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategory;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleZoneCategoryExample;

import java.util.List;

public interface ArticleZoneCategoryDao {
	int save(ArticleZoneCategory record);

	int deleteById(Long id);

	int updateById(ArticleZoneCategory record);

	int updateByExampleSelective(ArticleZoneCategory record, ArticleZoneCategoryExample example);

	ArticleZoneCategory getById(Long id);

	Page<ArticleZoneCategory> listForPage(int pageCurrent, int pageSize, ArticleZoneCategoryExample example);

	/**
	 * 根据文章专区名称获取文章专区信息
	 *
	 * @param articleZoneName
	 * @return
	 */
	ArticleZoneCategory getByArticleZoneName(String articleZoneName);

	/**
	 * 根据显示类型、状态获取文章专区信息
	 *
	 * @param platShow
	 * @param statusId
	 * @return
	 */
	List<ArticleZoneCategory> listByPlatShowAndStatusId(Integer platShow, Integer statusId);
}
