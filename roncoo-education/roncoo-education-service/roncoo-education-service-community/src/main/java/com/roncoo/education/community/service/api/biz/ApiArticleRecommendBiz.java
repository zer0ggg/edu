package com.roncoo.education.community.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.community.common.bo.ArticleRecommendPageBO;
import com.roncoo.education.community.common.dto.ArticleRecommendPageDTO;
import com.roncoo.education.community.service.dao.ArticleRecommendDao;
import com.roncoo.education.community.service.dao.BlogDao;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommend;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample;
import com.roncoo.education.community.service.dao.impl.mapper.entity.ArticleRecommendExample.Criteria;
import com.roncoo.education.community.service.dao.impl.mapper.entity.Blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文章推荐
 *
 * @author wujing
 */
@Component
public class ApiArticleRecommendBiz {

	@Autowired
	private BlogDao blogDao;

	@Autowired
	private ArticleRecommendDao articleRecommendDao;

	public Result<Page<ArticleRecommendPageDTO>> list(ArticleRecommendPageBO bo) {
		if (bo.getArticleType() == null) {
			return Result.error("articleType不能为空");
		}

		ArticleRecommendExample example = new ArticleRecommendExample();
		Criteria criteria = example.createCriteria();
		criteria.andArticleTypeEqualTo(bo.getArticleType());
		criteria.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
		example.setOrderByClause("sort asc, id desc ");
		Page<ArticleRecommend> page = articleRecommendDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		Page<ArticleRecommendPageDTO> listForPage = PageUtil.transform(page, ArticleRecommendPageDTO.class);
		for (ArticleRecommendPageDTO dto : listForPage.getList()) {
			Blog blog = blogDao.getById(dto.getArtcleId());
			if (ObjectUtil.isNotNull(blog)) {
				dto.setTitle(blog.getTitle());
				dto.setReadAcount(blog.getReadAcount());
			}
		}
		return Result.success(listForPage);
	}

}
