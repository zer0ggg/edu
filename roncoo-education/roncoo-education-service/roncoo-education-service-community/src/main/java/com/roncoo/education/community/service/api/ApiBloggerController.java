package com.roncoo.education.community.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.community.common.bo.BloggerViewBO;
import com.roncoo.education.community.common.dto.BloggerViewDTO;
import com.roncoo.education.community.service.api.biz.ApiBloggerBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 博主信息表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/community/api/blogger")
public class ApiBloggerController {

	@Autowired
	private ApiBloggerBiz biz;

	/**
	 * 博主信息查看接口
	 *
	 * @param bloggerViewBO
	 * @author wuyun
	 */
	@ApiOperation(value = "获取博主信息接口", notes = "获取博主信息")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<BloggerViewDTO> view(@RequestBody BloggerViewBO bloggerViewBO) {
		return biz.view(bloggerViewBO);
	}
}
