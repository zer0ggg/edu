package com.roncoo.education.community.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 问答评论表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthQuestionsCommentSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private Long parentId;
    /**
     * 问题ID
     */
    @ApiModelProperty(value = "问题ID", required = true)
    private Long questionsId;
    /**
     * 问题标题
     */
    @ApiModelProperty(value = "问题标题", required = true)
    private String title;
    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容", required = true)
    private String content;
    /**
     * 用户IP
     */
    @ApiModelProperty(value = "用户IP")
    private String userIp = "127.0.1";
    /**
     * 用户终端
     */
    @ApiModelProperty(value = "用户终端")
    private String userTerminal = "PC";

}
