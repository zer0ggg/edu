package com.roncoo.education.community.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.community.common.req.LabelEditREQ;
import com.roncoo.education.community.common.req.LabelListREQ;
import com.roncoo.education.community.common.req.LabelSaveREQ;
import com.roncoo.education.community.common.req.LabelUpdateStatusREQ;
import com.roncoo.education.community.common.resp.LabelListRESP;
import com.roncoo.education.community.common.resp.LabelViewRESP;
import com.roncoo.education.community.service.pc.biz.PcLabelBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标签 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/community/pc/label")
@Api(value = "community-标签", tags = {"community-标签"})
public class PcLabelController {

    @Autowired
    private PcLabelBiz biz;

    @ApiOperation(value = "标签列表", notes = "标签列表")
    @PostMapping(value = "/list")
    public Result<Page<LabelListRESP>> list(@RequestBody LabelListREQ labelListREQ) {
        return biz.list(labelListREQ);
    }

    @ApiOperation(value = "标签列表，纯名称集合", notes = "标签列表，纯名称集合")
    @PostMapping(value = "/list/only/name")
    public Result<List<String>> listOnlyName(@RequestBody LabelListREQ labelListREQ) {
        return biz.listName(labelListREQ);
    }


    @ApiOperation(value = "标签添加", notes = "标签添加")
    @SysLog(value = "标签添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody LabelSaveREQ labelSaveREQ) {
        return biz.save(labelSaveREQ);
    }

    @ApiOperation(value = "标签查看", notes = "标签查看")
    @GetMapping(value = "/view")
    public Result<LabelViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "根据类型获取标签", notes = "根据类型获取标签")
    @GetMapping(value = "/get/type")
    public Result<List<LabelListRESP>> listByAllAndLabelType(@RequestParam Integer labelType) {
        return biz.listByAllAndLabelType(labelType);
    }


    @ApiOperation(value = "标签修改", notes = "标签修改")
    @SysLog(value = "标签修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody LabelEditREQ labelEditREQ) {
        return biz.edit(labelEditREQ);
    }


    @ApiOperation(value = "标签状态修改", notes = "标签状态修改")
    @SysLog(value = "标签状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody LabelUpdateStatusREQ labelUpdateStatusREQ) {
        return biz.updateStatus(labelUpdateStatusREQ);
    }


    @ApiOperation(value = "标签删除", notes = "标签删除")
    @SysLog(value = "标签删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
