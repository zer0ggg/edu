package com.roncoo.education.marketing.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsUseEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.marketing.common.bo.auth.AuthActCouponBindBO;
import com.roncoo.education.marketing.common.bo.auth.AuthActCouponUserBO;
import com.roncoo.education.marketing.common.dto.auth.AuthActCouponUserBindDTO;
import com.roncoo.education.marketing.common.dto.auth.AuthActCouponUserListDTO;
import com.roncoo.education.marketing.service.dao.ActCouponDao;
import com.roncoo.education.marketing.service.dao.ActCouponUserDao;
import com.roncoo.education.marketing.service.dao.ActDao;
import com.roncoo.education.marketing.service.dao.ActZoneCourseDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 优惠券用户关联
 *
 * @author wujing
 */
@Component
public class AuthActCouponUserBiz extends BaseBiz {

    @Autowired
    private ActDao actDao;
    @Autowired
    private ActCouponDao actCouponDao;
    @Autowired
    private ActCouponUserDao dao;
    @Autowired
    private ActZoneCourseDao actZoneCourseDao;

    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 领取优惠劵
     *
     * @param bo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthActCouponUserBindDTO> bind(AuthActCouponBindBO bo) {
        // 核实用户编号
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户已禁用");
        }
        // 校验活动存在获取过期
        Act act = actDao.getByIdAndStatusIdAndTime(bo.getActId(), StatusIdEnum.YES.getCode(), new Date());
        if (ObjectUtil.isNull(act)) {
            return Result.error("活动不存在");
        }
        // 核实活动
        ActCoupon actCoupon = actCouponDao.getByActIdAndZondIdAndCourseIdAndStatusId(bo.getActId(), null, bo.getCourseId(), StatusIdEnum.YES.getCode());
        if (ObjectUtil.isNull(actCoupon)) {
            return Result.error("领券不可用");
        }
        if (actCoupon.getTotal() != 0 && actCoupon.getTotal() <= actCoupon.getReceiveNum()) {
            return Result.error("领券数量已达上限");
        }
        ActCouponUser record = dao.getByUserNoAndCouponIdAndTimeAndIsUse(userExtVO.getUserNo(), actCoupon.getId(), new Date(), IsUseEnum.NO.getCode());
        if (ObjectUtil.isNotNull(record)) {
            return Result.error("已领取，无需重复领取");
        }
        AuthActCouponUserBindDTO actCouponUserBind = new AuthActCouponUserBindDTO();
        actCouponUserBind.setActId(bo.getActId());
        actCouponUserBind.setUserNo(userExtVO.getUserNo());
        actCouponUserBind.setPastTime(act.getEndTime());
        actCouponUserBind.setCouponId(actCoupon.getId());
        actCouponUserBind.setIsUse(IsUseEnum.NO.getCode());
        actCouponUserBind.setMobile(userExtVO.getMobile());
        actCouponUserBind.setCouponPrice(actCoupon.getCouponPrice());
        // 优惠券只允许领取一次
        int num = dao.countByCouponIdAndUserNo(actCoupon.getId(), userExtVO.getUserNo());
        if (num < 1) {
            record = BeanUtil.copyProperties(actCouponUserBind, ActCouponUser.class);
            record.setZoneId(actCoupon.getZoneId());
            if (dao.save(record) < 1) {
                return Result.error("领取失败，请联系客服");
            }
            // 刷新优惠卷领取数量
            actCoupon.setReceiveNum(Integer.sum(actCoupon.getReceiveNum(), 1));
            if (actCouponDao.updateById(actCoupon) < 1) {
                return Result.error("数量更新失败");
            }
            return Result.success(actCouponUserBind);
        } else {
            return Result.error("您已经领取过了,赶快去下单吧");
        }
    }


    public Result<Page<AuthActCouponUserListDTO>> owned(AuthActCouponUserBO bo) {
        // 核实用户编号
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户已禁用");
        }
        ActCouponUserExample example = new ActCouponUserExample();
        ActCouponUserExample.Criteria c = example.createCriteria();
        if (StatusIdEnum.YES.getCode().equals(bo.getStatusId())) {
            if (bo.getIsUse() == 0) {
                c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
                c.andPastTimeGreaterThan(new Date());
                c.andIsUseEqualTo(bo.getIsUse());
            } else {
                c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
                c.andIsUseEqualTo(bo.getIsUse());
            }
        } else {
            // 展示过期优惠券，未使用且活动过期
            //c.andStatusIdEqualTo(bo.getStatusId());
            c.andPastTimeLessThan(new Date());
            c.andIsUseEqualTo(IsUseEnum.NO.getCode());
            // 我的优惠券，X 天后不显示过期
            c.andGmtModifiedGreaterThan(DateUtil.addDate(new Date(), -30));
        }
        c.andUserNoEqualTo(userExtVO.getUserNo());
        example.setOrderByClause(" status_id desc, is_use asc , sort desc ");
        Page<ActCouponUser> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthActCouponUserListDTO> dtoPage = PageUtil.transform(page, AuthActCouponUserListDTO.class);
        for (AuthActCouponUserListDTO dto : dtoPage.getList()) {
            // 通过优惠券ID查找活动编号
            ActCoupon actCoupon = actCouponDao.getById(dto.getCouponId());
            if (ObjectUtil.isNull(actCoupon)) {
                return Result.error("找不到优惠券活动信息");
            }
            dto.setCourseId(actCoupon.getCourseId());
            ActZoneCourse actZoneCourse = actZoneCourseDao.getByZoneIdAndCourseId(dto.getZoneId(), dto.getCourseId());
            if (ObjectUtil.isNull(actZoneCourse)) {
                return Result.error("找不到专区课程");
            }
            dto.setCourseOriginal(actZoneCourse.getCourseOriginal());
            dto.setCourseDiscount(actZoneCourse.getCourseDiscount());

            CourseVO courseVO = feignCourse.getByCourseId(actCoupon.getCourseId());
            if (ObjectUtil.isNotNull(courseVO)) {
                dto.setCourseName(courseVO.getCourseName());
                dto.setCourseImg(courseVO.getCourseLogo());
                dto.setCourseCategory(courseVO.getCourseCategory());
            }

            // 通过活动查找活动信息
            Act act = actDao.getById(actCoupon.getActId());
            if (ObjectUtil.isNull(act)) {
                return Result.error("找不到活动信息");
            }
            // 填活动时间
            dto.setEndTime(act.getEndTime());
        }
        return Result.success(dtoPage);

    }
}
