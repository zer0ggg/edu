package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample;

import java.util.List;

/**
 * 课程秒杀活动 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActSeckillDao {

    /**
     * 保存课程秒杀活动
     *
     * @param record 课程秒杀活动
     * @return 影响记录数
     */
    int save(ActSeckill record);

    /**
     * 根据ID删除课程秒杀活动
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 根据专区id和课程id 删除课程秒杀活动
     *
     * @param zoneId
     * @param courseId
     * @return
     */
    int deleteByZoneIdCourseId(Long zoneId, Long courseId);

    /**
     * 修改秒杀活动
     *
     * @param record 课程秒杀活动
     * @return 影响记录数
     */
    int updateById(ActSeckill record);

    /**
     * 根据ID获取课程秒杀活动
     *
     * @param id 主键ID
     * @return 课程秒杀活动
     */
    ActSeckill getById(Long id);

    /**
     * 课程秒杀活动--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ActSeckill> listForPage(int pageCurrent, int pageSize, ActSeckillExample example);

    /**
     * 根据活动id、专区id获得列表
     * @param actId
     * @param zoneId
     * @return
     */
    List<ActSeckill> listByActIdAndZoneId(Long actId, Long zoneId);

    /**
     * 根据专区ID、课程ID、状态查询秒杀活动课程信息
     *
     * @param zoneId   专区ID
     * @param courseId 课程ID
     * @param statusId 状态
     * @return 结果
     */
    ActSeckill getByZoneIdAndCourseIdAndStatusId(Long zoneId, Long courseId, Integer statusId);

    /**
     * 根据专区ID、课程ID查询秒杀活动课程信息
     *
     * @param zoneId   专区ID
     * @param courseId 课程ID
     * @return 结果
     */
    ActSeckill getByZoneIdAndCourseId(Long zoneId, Long courseId);
}

