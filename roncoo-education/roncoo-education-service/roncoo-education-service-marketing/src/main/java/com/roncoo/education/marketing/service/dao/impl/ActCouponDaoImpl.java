package com.roncoo.education.marketing.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActCouponDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActCouponMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 优惠券活动 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActCouponDaoImpl implements ActCouponDao {

    @Autowired
    private ActCouponMapper mapper;

    @Override
    public int save(ActCoupon record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ActCoupon record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ActCoupon getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public ActCoupon getByZoneIdAndCourseId(Long zoneId, Long courseId) {
        ActCouponExample example = new ActCouponExample();
        ActCouponExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(courseId);
        List<ActCoupon> list =  this.mapper.selectByExample(example);
        if(!CollectionUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    @Override
    public Page<ActCoupon> listForPage(int pageCurrent, int pageSize, ActCouponExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ActCoupon> listByActIdAndZoneId(Long actId, Long zoneId) {
        ActCouponExample example = new ActCouponExample();
        ActCouponExample.Criteria c  = example.createCriteria();
        if(actId != null){
            c.andActIdEqualTo(actId);
        }
        if(zoneId != null){
            c.andZoneIdEqualTo(zoneId);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public ActCoupon getByActIdAndZondIdAndCourseIdAndStatusId(Long actId, Long zoneId,Long courseId, Integer statusId) {
        ActCouponExample example = new ActCouponExample();
        ActCouponExample.Criteria c = example.createCriteria();
        if(actId != null){
            c.andActIdEqualTo(actId);
        }
        if(zoneId != null){
            c.andZoneIdEqualTo(zoneId);
        }
        if(courseId != null){
            c.andCourseIdEqualTo(courseId);
        }
        if(statusId != null){
            c.andStatusIdEqualTo(statusId);
        }
        List<ActCoupon> list =  this.mapper.selectByExample(example);
        if(!CollectionUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }


}
