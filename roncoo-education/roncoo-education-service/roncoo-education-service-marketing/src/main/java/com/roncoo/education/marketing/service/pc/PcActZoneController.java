package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.marketing.common.req.*;
import com.roncoo.education.marketing.common.resp.ActZoneListRESP;
import com.roncoo.education.marketing.common.resp.ActZoneViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActZoneBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 活动专区 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-活动专区")
@RestController
@RequestMapping("/marketing/pc/act/zone")
public class PcActZoneController {

    @Autowired
    private PcActZoneBiz biz;

    @ApiOperation(value = "活动专区列表", notes = "活动专区列表")
    @PostMapping(value = "/list")
    public Result<Page<ActZoneListRESP>> list(@RequestBody ActZoneListREQ actZoneListREQ) {
        return biz.list(actZoneListREQ);
    }


    @ApiOperation(value = "活动专区添加", notes = "活动专区添加")
    @SysLog(value = "活动专区添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActZoneSaveREQ actZoneSaveREQ) {
        return biz.save(actZoneSaveREQ);
    }

    @ApiOperation(value = "活动专区查看", notes = "活动专区查看")
    @GetMapping(value = "/view")
    public Result<ActZoneViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "活动专区修改", notes = "活动专区修改")
    @SysLog(value = "活动专区修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActZoneEditREQ actZoneEditREQ) {
        return biz.edit(actZoneEditREQ);
    }


    @ApiOperation(value = "活动专区删除", notes = "活动专区删除")
    @SysLog(value = "活动专区删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam  Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "活动专区状态修改", notes = "活动专区状态修改")
    @SysLog(value = "活动专区状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid ActZoneUpdateStatusREQ actZoneUpdateStatusREQ) {
        return biz.updateStatus(actZoneUpdateStatusREQ);
    }


    @ApiOperation(value = "秒杀专区复制", notes = "秒杀专区复制")
    @SysLog(value = "秒杀专区复制")
    @PostMapping(value = "/copy")
    public Result<String> copy(@RequestBody ActZoneCopyREQ req) {
        return biz.copy(req);
    }
}
