package com.roncoo.education.marketing.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActViewBO", description="活动信息表")
public class ActViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;
}
