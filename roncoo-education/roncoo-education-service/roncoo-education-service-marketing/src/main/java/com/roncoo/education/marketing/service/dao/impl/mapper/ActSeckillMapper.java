package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActSeckillMapper {
    int countByExample(ActSeckillExample example);

    int deleteByExample(ActSeckillExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ActSeckill record);

    int insertSelective(ActSeckill record);

    List<ActSeckill> selectByExample(ActSeckillExample example);

    ActSeckill selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ActSeckill record, @Param("example") ActSeckillExample example);

    int updateByExample(@Param("record") ActSeckill record, @Param("example") ActSeckillExample example);

    int updateByPrimaryKeySelective(ActSeckill record);

    int updateByPrimaryKey(ActSeckill record);
}
