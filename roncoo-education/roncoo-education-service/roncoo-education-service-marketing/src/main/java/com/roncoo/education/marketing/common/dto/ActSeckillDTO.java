package com.roncoo.education.marketing.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 课程秒杀活动
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActSeckillDTO", description="课程秒杀活动")
public class ActSeckillDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "活动Id")
    private Long actId;

    @ApiModelProperty(value = "课程Id")
    private Long courseId;

    @ApiModelProperty(value = "秒杀价")
    private BigDecimal seckillPrice;

    @ApiModelProperty(value = "是否限购(1:是;0:否)")
    private Integer isLimit;

    @ApiModelProperty(value = "限购人数")
    private Integer buyer;

    @ApiModelProperty(value = "已购人数")
    private Integer hasBuy;

    /**
     * 开始时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;

    /**
     * 是否秒杀开始(1: 未开始; 2: 活动中; 3:结束)
     */
    @ApiModelProperty(value = "是否秒杀开始(1: 未开始; 2: 活动中; 3:结束)")
    private Integer isStart;
}
