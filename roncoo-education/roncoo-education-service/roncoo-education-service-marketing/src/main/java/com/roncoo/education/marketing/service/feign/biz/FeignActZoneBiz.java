package com.roncoo.education.marketing.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.feign.qo.ActZoneQO;
import com.roncoo.education.marketing.feign.vo.ActZoneVO;
import com.roncoo.education.marketing.service.dao.ActZoneDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZone;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 活动专区
 *
 * @author wujing
 */
@Component
public class FeignActZoneBiz extends BaseBiz {

    @Autowired
    private ActZoneDao dao;

	public Page<ActZoneVO> listForPage(ActZoneQO qo) {
	    ActZoneExample example = new ActZoneExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ActZone> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ActZoneVO.class);
	}

	public int save(ActZoneQO qo) {
		ActZone record = BeanUtil.copyProperties(qo, ActZone.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ActZoneVO getById(Long id) {
		ActZone record = dao.getById(id);
		return BeanUtil.copyProperties(record, ActZoneVO.class);
	}

	public int updateById(ActZoneQO qo) {
		ActZone record = BeanUtil.copyProperties(qo, ActZone.class);
		return dao.updateById(record);
	}

}
