package com.roncoo.education.marketing.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.bo.auth.AuthActCouponBindBO;
import com.roncoo.education.marketing.common.bo.auth.AuthActCouponUserBO;
import com.roncoo.education.marketing.common.dto.auth.AuthActCouponUserBindDTO;
import com.roncoo.education.marketing.common.dto.auth.AuthActCouponUserListDTO;
import com.roncoo.education.marketing.service.api.auth.biz.AuthActCouponUserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 优惠券用户关联 UserApi接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@Api(tags = "API-AUTH-优惠券用户关联")
@RestController
@RequestMapping("/marketing/auth/actCouponUser")
public class AuthActCouponUserController {

    @Autowired
    private AuthActCouponUserBiz biz;

    /**
     * 用户领券
     * @param actCouponBindBO
     * @return
     */
    @ApiOperation(value = "用户领券", notes = "用户领券")
    @RequestMapping(value = "/bind", method = RequestMethod.POST)
    public Result<AuthActCouponUserBindDTO> bind(@RequestBody @Valid AuthActCouponBindBO actCouponBindBO) {
        return biz.bind(actCouponBindBO);
    }

    /**
     * 查找用户已有优惠券
     * @return
     */
    @ApiOperation(value = " 查找用户已有优惠券", notes = " 查找用户已有优惠券")
    @RequestMapping(value = "/owned", method = RequestMethod.POST)
    public Result<Page<AuthActCouponUserListDTO>> owned(@RequestBody @Valid AuthActCouponUserBO authActCouponUserBO) {
        return biz.owned(authActCouponUserBO);
    }

}
