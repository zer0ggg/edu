package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUser;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample;

import java.util.Date;

/**
 * 优惠券用户关联 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActCouponUserDao {

    /**
     * 保存优惠券用户关联
     *
     * @param record 优惠券用户关联
     * @return 影响记录数
     */
    int save(ActCouponUser record);

    /**
     * 根据ID删除优惠券用户关联
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 优惠券用户关联
     * @return 影响记录数
     */
    int updateById(ActCouponUser record);

    /**
     * 根据ID获取优惠券用户关联
     *
     * @param id 主键ID
     * @return 优惠券用户关联
     */
    ActCouponUser getById(Long id);

    /**
     * 优惠券用户关联--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ActCouponUser> listForPage(int pageCurrent, int pageSize, ActCouponUserExample example);

    /**
     * 根据用户编号和优惠券ID获取当前日期用户未过期可使用的优惠券信息
     *
     * @param userNo
     * @param couponId
     * @param date
     * @param isUse
     * @return
     */
    ActCouponUser getByUserNoAndCouponIdAndTimeAndIsUse(Long userNo, Long couponId, Date date, Integer isUse);

    /**
     * 根据优惠卷id和用户no，查询用户领卷数量
     *
     * @param couponId
     * @param userNo
     * @return
     */
    int countByCouponIdAndUserNo(Long couponId, Long userNo);

    /**
     * 根据优惠卷id更新 是否使用状态
     * @param couponId
     * @param isUse
     * @return
     */
    int updateByIsUse(Long couponId, Integer isUse);
}
