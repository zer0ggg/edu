package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.marketing.common.req.ActZoneCourseEditREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseListREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseSaveREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseUpadateStatusREQ;
import com.roncoo.education.marketing.common.resp.ActZoneCourseListRESP;
import com.roncoo.education.marketing.common.resp.ActZoneCourseViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActZoneCourseBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 活动专区课程 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-活动专区课程")
@RestController
@RequestMapping("/marketing/pc/act/zone/course")
public class PcActZoneCourseController {

    @Autowired
    private PcActZoneCourseBiz biz;

    @ApiOperation(value = "活动专区课程列表", notes = "活动专区课程列表")
    @PostMapping(value = "/list")
    public Result<Page<ActZoneCourseListRESP>> list(@RequestBody ActZoneCourseListREQ req) {
        return biz.list(req);
    }


    @ApiOperation(value = "活动专区课程添加", notes = "活动专区课程添加")
    @SysLog(value = "活动专区课程添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActZoneCourseSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "活动专区课程查看", notes = "活动专区课程查看")
    @GetMapping(value = "/view")
    public Result<ActZoneCourseViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "活动专区课程修改", notes = "活动专区课程修改")
    @SysLog(value = "活动专区课程修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActZoneCourseEditREQ req) {
        return biz.edit(req);
    }


    @ApiOperation(value = "活动专区课程 状态修改", notes = "活动专区课程 状态修改")
    @SysLog(value = "活动专区课程 状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ActZoneCourseUpadateStatusREQ req) {
        return biz.updateStatus(req);
    }


    @ApiOperation(value = "活动专区课程删除", notes = "活动专区课程删除")
    @SysLog(value = "活动专区课程删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
