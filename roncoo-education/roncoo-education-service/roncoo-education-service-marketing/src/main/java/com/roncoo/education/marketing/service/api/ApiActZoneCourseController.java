package com.roncoo.education.marketing.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.bo.ActZoneCourseViewBO;
import com.roncoo.education.marketing.common.dto.ActZoneCourseViewDTO;
import com.roncoo.education.marketing.service.api.biz.ApiActZoneCourseBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * 活动专区课程 Api接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@Api(tags = "API-活动专区课程")
@RestController
@RequestMapping("/marketing/api/act/zone/course")
public class ApiActZoneCourseController {

    @Autowired
    private ApiActZoneCourseBiz biz;


    /**
     * 课程详情查看活动接口
     * @return
     */
    @ApiOperation(value = "课程详情查看活动接口", notes = "课程详情查看活动接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<ActZoneCourseViewDTO> list(@RequestBody ActZoneCourseViewBO bo) {
        return biz.view(bo);
    }




}
