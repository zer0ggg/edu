package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.req.ActCouponUserEditREQ;
import com.roncoo.education.marketing.common.req.ActCouponUserListREQ;
import com.roncoo.education.marketing.common.req.ActCouponUserSaveREQ;
import com.roncoo.education.marketing.common.resp.ActCouponUserListRESP;
import com.roncoo.education.marketing.common.resp.ActCouponUserViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActCouponUserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 优惠券用户关联 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-优惠券用户关联")
@RestController
@RequestMapping("/marketing/pc/act/coupon/user")
public class PcActCouponUserController {

    @Autowired
    private PcActCouponUserBiz biz;

    @ApiOperation(value = "优惠券用户关联列表", notes = "优惠券用户关联列表")
    @PostMapping(value = "/list")
    public Result<Page<ActCouponUserListRESP>> list(@RequestBody ActCouponUserListREQ actCouponUserListREQ) {
        return biz.list(actCouponUserListREQ);
    }

    @ApiOperation(value = "优惠券用户关联添加", notes = "优惠券用户关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActCouponUserSaveREQ actCouponUserSaveREQ) {
        return biz.save(actCouponUserSaveREQ);
    }

    @ApiOperation(value = "优惠券用户关联查看", notes = "优惠券用户关联查看")
    @GetMapping(value = "/view")
    public Result<ActCouponUserViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "优惠券用户关联修改", notes = "优惠券用户关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActCouponUserEditREQ actCouponUserEditREQ) {
        return biz.edit(actCouponUserEditREQ);
    }

    @ApiOperation(value = "优惠券用户关联删除", notes = "优惠券用户关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
