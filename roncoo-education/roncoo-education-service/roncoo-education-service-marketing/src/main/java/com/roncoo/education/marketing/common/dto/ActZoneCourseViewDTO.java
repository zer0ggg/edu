package com.roncoo.education.marketing.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 活动专区课程
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneCourseViewDTO", description="活动专区课程")
public class ActZoneCourseViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "活动Id")
    private Long actId;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "优惠券信息")
    private ActCouponDTO actCouponDTO;

    @ApiModelProperty(value = "秒杀信息")
    private List<ActSeckillDTO> actSeckillDTOList;
}
