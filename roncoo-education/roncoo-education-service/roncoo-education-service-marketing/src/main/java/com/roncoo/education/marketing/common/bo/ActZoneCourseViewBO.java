package com.roncoo.education.marketing.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 活动专区课程
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneCourseViewBO", description="活动专区课程")
public class ActZoneCourseViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程Id")
    private Long courseId;
}
