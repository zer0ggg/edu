package com.roncoo.education.marketing.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 活动专区课程
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneCourseEditREQ", description="活动专区课程修改")
public class ActZoneCourseEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空")
    @ApiModelProperty(value = "专区课程主键",required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @NotNull(message = "价格不能为空")
    @ApiModelProperty(value = "活动价格",required = true)
    private BigDecimal actPrice;

    //优惠卷信息
    @ApiModelProperty(value = "优惠券数量")
    private Integer total;

    //秒杀信息
    @ApiModelProperty(value = "是否限购(1:是;0:否)")
    private Integer isLimit;

    @ApiModelProperty(value = "限购人数")
    private Integer buyer;


}
