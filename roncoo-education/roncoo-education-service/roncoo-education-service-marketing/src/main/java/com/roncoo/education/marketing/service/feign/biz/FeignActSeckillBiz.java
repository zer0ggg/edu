package com.roncoo.education.marketing.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.feign.qo.ActSeckillQO;
import com.roncoo.education.marketing.feign.vo.ActSeckillVO;
import com.roncoo.education.marketing.service.dao.ActSeckillDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程秒杀活动
 *
 * @author wujing
 */
@Component
public class FeignActSeckillBiz extends BaseBiz {

    @Autowired
    private ActSeckillDao dao;

	public Page<ActSeckillVO> listForPage(ActSeckillQO qo) {
	    ActSeckillExample example = new ActSeckillExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ActSeckill> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ActSeckillVO.class);
	}

	public int save(ActSeckillQO qo) {
		ActSeckill record = BeanUtil.copyProperties(qo, ActSeckill.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ActSeckillVO getById(Long id) {
		ActSeckill record = dao.getById(id);
		return BeanUtil.copyProperties(record, ActSeckillVO.class);
	}

	public int updateById(ActSeckillQO qo) {
		ActSeckill record = BeanUtil.copyProperties(qo, ActSeckill.class);
		return dao.updateById(record);
	}

}
