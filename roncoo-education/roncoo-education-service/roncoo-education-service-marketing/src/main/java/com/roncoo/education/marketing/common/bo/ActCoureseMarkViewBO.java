package com.roncoo.education.marketing.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 课程参与的活动集合
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActCoureseMarkViewBO", description="课程参与的活动集合")
public class ActCoureseMarkViewBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "课程id集合", required = true)
	private List<String> courseIds;

}
