package com.roncoo.education.marketing.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneSaveREQ", description="活动专区添加")
public class ActZoneSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @NotNull(message = "活动Id不能为空")
    @ApiModelProperty(value = "活动Id",required = true)
    private Long actId;

    @NotEmpty(message = "专区标题不能为空")
    @ApiModelProperty(value = "专区标题",required = true)
    private String zoneTitle;

    @ApiModelProperty(value = "秒杀开始时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;

}
