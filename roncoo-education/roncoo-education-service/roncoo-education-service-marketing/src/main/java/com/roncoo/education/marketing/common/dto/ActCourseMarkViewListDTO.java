package com.roncoo.education.marketing.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 活动集合
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActCourseMarkViewListDTO", description="课程集合的活动信息")
public class ActCourseMarkViewListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程集合的活动信息", required = true)
    private List<ActCourseMarkViewDTO> courseIdList = new ArrayList<>();

}
