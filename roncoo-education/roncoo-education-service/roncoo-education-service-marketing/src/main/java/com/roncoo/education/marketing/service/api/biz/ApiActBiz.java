package com.roncoo.education.marketing.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.marketing.common.bo.ActCoureseMarkViewBO;
import com.roncoo.education.marketing.common.bo.ActViewBO;
import com.roncoo.education.marketing.common.dto.*;
import com.roncoo.education.marketing.service.dao.ActDao;
import com.roncoo.education.marketing.service.dao.ActZoneCourseDao;
import com.roncoo.education.marketing.service.dao.ActZoneDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.Act;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZone;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 活动信息表
 *
 * @author wujing
 */
@Component
public class ApiActBiz extends BaseBiz {

    @Autowired
    private ActDao dao;

    @Autowired
    private ActZoneCourseDao actZoneCourseDao;
    @Autowired
    private ActZoneDao actZoneDao;

    public Result<ActViewDTO> view(ActViewBO actViewBO) {

        ActViewDTO dto = new ActViewDTO();
        //查找该类型活动信息
        Act act = dao.listByTimeAndActTypeAndStatusId(new Date(), actViewBO.getActType(), StatusIdEnum.YES.getCode());
        if (ObjectUtil.isNull(act)) {
            return Result.success(dto);
        }
        dto = BeanUtil.copyProperties(act, ActViewDTO.class);
        // 根据活动ID获取可用活动专区
        List<ActZone> actZoneList = actZoneDao.listByActIdAndStatusId(act.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(actZoneList)) {
            List<ActZoneListDTO> actZoneListDTO = ArrayListUtil.copy(actZoneList, ActZoneListDTO.class);
            for (ActZoneListDTO actZoneDTO : actZoneListDTO) {
                // 根据专区ID获取可用活动专区课程
                List<ActZoneCourse> actZoneCourseList = actZoneCourseDao.listByZoneIdAndStatusId(actZoneDTO.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(actZoneCourseList)) {
                    actZoneDTO.setAoneCourseList(ArrayListUtil.copy(actZoneCourseList, ActZoneCourseListDTO.class));
                }
            }
            dto.setActZoneList(actZoneListDTO);
        }
        return Result.success(dto);
    }


    public Result<ActCourseMarkViewListDTO> mark(ActCoureseMarkViewBO bo) {
        ActCourseMarkViewListDTO dto = new ActCourseMarkViewListDTO();
        List<ActCourseMarkViewDTO> courseNoList = new ArrayList<>();
        // 查找可用的活动
        // 查找活动的具体信息
        if (bo.getCourseIds().isEmpty()) {
            return Result.success(dto);
        }
        //  查找当前时间下可用的活动信息
        List<Act> actList = dao.listByTimeAndStatusId(new Date(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(actList)) {
            return Result.success(dto);
        }
        for (String i : bo.getCourseIds()) {
            ActCourseMarkViewDTO courseDto = new ActCourseMarkViewDTO();
            courseDto.setCourseId(Long.valueOf(i));

            // 根据课程ID获取活动专区课程
            List<ActZoneCourse> course = actZoneCourseDao.listByCourseIdAndStatusId(Long.valueOf(i), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(course)) {
                List<ActZoneCourseUserViewDTO> list = new ArrayList<>();
                courseDto.setActZoneCourseUserViewDTO(course(list, course));
            }
            courseNoList.add(courseDto);
        }
        dto.setCourseIdList(courseNoList);
        return Result.success(dto);
    }

    private List<ActZoneCourseUserViewDTO> course(List<ActZoneCourseUserViewDTO> list, List<ActZoneCourse> course) {
        for (ActZoneCourse zoneCourse : course) {
            Act act = dao.getByIdAndStatusIdAndTime(zoneCourse.getActId(), StatusIdEnum.YES.getCode(), new Date());
            if (ObjectUtil.isNotNull(act)) {
                // 获取课程参与的活动信息
                ActZoneCourseUserViewDTO dto1 = zoneCourseUser(zoneCourse);
                if (dto1 != null) {
                    list.add(dto1);
                }
            }
        }
        return list;

    }

    /**
     * 获取课程参与的活动信息
     *
     * @param zoneCourse 专区课程
     * @return 专区课程信息
     */
    private ActZoneCourseUserViewDTO zoneCourseUser(ActZoneCourse zoneCourse) {
        if (ActTypeEnum.COUPON.getCode().equals(zoneCourse.getActType())) {
            ActZone actZone = actZoneDao.getById(zoneCourse.getZoneId());
            if (ObjectUtil.isNotNull(actZone) && StatusIdEnum.YES.getCode().equals(actZone.getStatusId())) {
                return BeanUtil.copyProperties(zoneCourse, ActZoneCourseUserViewDTO.class);
            }
        }
        if (ActTypeEnum.SECKILL.getCode().equals(zoneCourse.getActType())) {
            ActZone actZone = actZoneDao.getById(zoneCourse.getZoneId());
            if (ObjectUtil.isNotNull(actZone) && StatusIdEnum.YES.getCode().equals(actZone.getStatusId())) {
                // 获取秒杀活动秒杀状态 正在秒杀活动
                if (seckill(actZone) == 2) {
                    return BeanUtil.copyProperties(zoneCourse, ActZoneCourseUserViewDTO.class);
                }
            }
        }
        return null;
    }

    /**
     * 秒杀活动
     * isStart（1：未开始；2：正在秒杀；3：秒杀活动结束）
     *
     * @param actZone
     * @return
     */
    private int seckill(ActZone actZone) {
        int isStart;
        if (time().before(actZone.getBeginTime())) {
            // 秒杀活动未开始
            isStart = 1;
        } else if (time().after(actZone.getBeginTime()) && time().before(actZone.getEndTime())) {
            // 正在秒杀活动
            isStart = 2;
        } else {
            // 秒杀活动结束
            isStart = 3;
        }
        return isStart;

    }

    private Date time() {
        //创建日期转换对
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            //将字符串转换为date类型
            return df.parse(DateUtil.getTime(new Date()));
        } catch (ParseException e) {
            return null;
        }
    }
}
