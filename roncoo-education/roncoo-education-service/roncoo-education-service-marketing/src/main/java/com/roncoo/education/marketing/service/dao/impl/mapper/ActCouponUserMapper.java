package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUser;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActCouponUserMapper {
    int countByExample(ActCouponUserExample example);

    int deleteByExample(ActCouponUserExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ActCouponUser record);

    int insertSelective(ActCouponUser record);

    List<ActCouponUser> selectByExample(ActCouponUserExample example);

    ActCouponUser selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ActCouponUser record, @Param("example") ActCouponUserExample example);

    int updateByExample(@Param("record") ActCouponUser record, @Param("example") ActCouponUserExample example);

    int updateByPrimaryKeySelective(ActCouponUser record);

    int updateByPrimaryKey(ActCouponUser record);
}
