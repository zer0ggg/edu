package com.roncoo.education.marketing.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.IsUseEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.marketing.common.bo.auth.AuthActUserCoureseBO;
import com.roncoo.education.marketing.common.dto.auth.AuthActUserCourseViewDTO;
import com.roncoo.education.marketing.service.dao.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 活动信息表
 *
 * @author wujing
 */
@Component
public class AuthActBiz extends BaseBiz {

    @Autowired
    private ActDao actDao;
    @Autowired
    private ActZoneDao actZoneDao;
    @Autowired
    private ActZoneCourseDao actZoneCourseDao;
    @Autowired
    private ActCouponDao actCouponDao;
    @Autowired
    private ActCouponUserDao actCouponUserDao;
    @Autowired
    private ActSeckillDao actSeckillDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    public Result<AuthActUserCourseViewDTO> view(AuthActUserCoureseBO bo) {
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户已禁用");
        }

        // 查找活动的具体信息
        List<ActZoneCourse> courseList = actZoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(null, null, bo.getCourseId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(courseList)) {
            return Result.success(null);
        }
        AuthActUserCourseViewDTO dto = new AuthActUserCourseViewDTO();
        for (ActZoneCourse course : courseList) {
            // 校验活动基本信息
            Act act = actDao.getByIdAndStatusIdAndTime(course.getActId(), StatusIdEnum.YES.getCode(), new Date());
            if (ObjectUtil.isNotNull(act)) {
                AuthActUserCourseViewDTO viewDTO = course(course, userExtVO.getUserNo());
                if (viewDTO != null) {
                    // 添加这条课程记录
                    dto = viewDTO;
                }
            }
        }
        if (dto.getActId() == null) {
            return Result.success(null);
        }
        return Result.success(dto);
    }


    private AuthActUserCourseViewDTO course(ActZoneCourse course, Long userNo) {
        if (ActTypeEnum.COUPON.getCode().equals(course.getActType())) {
            // 优惠卷活动：存在且已经领卷，则保留该记录，否则不保留该条记录
            ActCoupon coupon = actCouponDao.getByActIdAndZondIdAndCourseIdAndStatusId(course.getActId(), null, course.getCourseId(), StatusIdEnum.YES.getCode());
            if (ObjectUtil.isNotNull(coupon)) {
                ActCouponUser couponUser = actCouponUserDao.getByUserNoAndCouponIdAndTimeAndIsUse(userNo, coupon.getId(), new Date(), IsUseEnum.NO.getCode());
                if (ObjectUtil.isNotNull(couponUser)) {
                    return BeanUtil.copyProperties(course, AuthActUserCourseViewDTO.class);
                }
            }
            return null;
        } else {
            // 秒杀：判定当前时间不在秒杀时间内，跳过本课程,符合则补充秒杀时间
            ActSeckill actSeckill = actSeckillDao.getByZoneIdAndCourseIdAndStatusId(course.getZoneId(), course.getCourseId(), StatusIdEnum.YES.getCode());
            if (ObjectUtil.isNull(actSeckill)) {
                return null;
            }
            ActZone actZone = actZoneDao.getById(course.getZoneId());
            if (ObjectUtil.isNull(actZone) || !StatusIdEnum.YES.getCode().equals(actZone.getStatusId())) {
                return null;
            }
            //创建日期转换对
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            try {
                //将字符串转换为date类型
                Date dt1 = df.parse(DateUtil.getTime(new Date()));
                if (dt1.after(actZone.getBeginTime()) && dt1.before(actZone.getEndTime())) {
                    // 正在秒杀活动
                    return BeanUtil.copyProperties(course, AuthActUserCourseViewDTO.class);
                } else {
                    return null;
                }
            } catch (ParseException e) {
                logger.error("校验秒杀活动是否正在秒杀！", e);
                return null;
            }
        }
    }
}
