package com.roncoo.education.marketing.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.bo.ActCoureseMarkViewBO;
import com.roncoo.education.marketing.common.bo.ActViewBO;
import com.roncoo.education.marketing.common.dto.ActCourseMarkViewListDTO;
import com.roncoo.education.marketing.common.dto.ActViewDTO;
import com.roncoo.education.marketing.service.api.biz.ApiActBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * 活动信息表 Api接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@Api(tags = "API-活动信息表")
@RestController
@RequestMapping("/marketing/api/act")
public class ApiActController {

    @Autowired
    private ApiActBiz biz;

    /**
     * 获取正在进行活动
     * @return
     */
    @ApiOperation(value = "获取正在进行活动", notes = "获取正在进行活动")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<ActViewDTO> view(@RequestBody ActViewBO actViewBO) {
        return biz.view(actViewBO);
    }


    /**
     * 课程参与活动的集合，为前端提供角标
     * @return
     */
    @ApiOperation(value = "课程参与活动的集合，为前端提供角标", notes = "课程参与活动的集合，为前端提供角标")
    @RequestMapping(value = "/mark", method = RequestMethod.POST)
    public Result<ActCourseMarkViewListDTO> mark(@RequestBody ActCoureseMarkViewBO bo) {
        return biz.mark(bo);
    }

}
