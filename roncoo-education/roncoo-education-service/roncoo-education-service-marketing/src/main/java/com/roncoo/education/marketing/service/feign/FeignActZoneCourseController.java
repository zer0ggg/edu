package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignActZoneCourse;
import com.roncoo.education.marketing.feign.qo.ActZoneCourseQO;
import com.roncoo.education.marketing.feign.vo.ActZoneCourseVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActZoneCourseBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 活动专区课程
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActZoneCourseController extends BaseController implements IFeignActZoneCourse{

    @Autowired
    private FeignActZoneCourseBiz biz;

	@Override
	public Page<ActZoneCourseVO> listForPage(@RequestBody ActZoneCourseQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActZoneCourseQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActZoneCourseQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActZoneCourseVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public int updateByOrder(@RequestBody ActZoneCourseQO actZoneCourseQO) {
		return biz.updateByOrder(actZoneCourseQO);
	}
}
