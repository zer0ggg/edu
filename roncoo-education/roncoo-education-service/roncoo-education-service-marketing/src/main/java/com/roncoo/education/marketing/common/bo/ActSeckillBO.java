package com.roncoo.education.marketing.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 课程秒杀活动
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActSeckillBO", description="课程秒杀活动")
public class ActSeckillBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "活动Id")
    private Long actId;

    @ApiModelProperty(value = "课程Id")
    private Long courseId;

    @ApiModelProperty(value = "秒杀价")
    private BigDecimal seckillPrice;

    @ApiModelProperty(value = "是否限购(1:是;0:否)")
    private Integer isLimit;

    @ApiModelProperty(value = "限购人数")
    private Integer buyer;

    @ApiModelProperty(value = "剩余数量")
    private Integer surplus;
}
