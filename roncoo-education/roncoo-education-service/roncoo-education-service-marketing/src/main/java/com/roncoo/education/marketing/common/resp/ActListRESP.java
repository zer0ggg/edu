package com.roncoo.education.marketing.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ActListRESP", description = "活动信息表列表")
public class ActListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "活动标题")
    private String actTitle;

    @ApiModelProperty(value = "活动图片")
    private String actImg;

    @ApiModelProperty(value = "活动开始时间")
    private Date beginTime;

    @ApiModelProperty(value = "活动结束时间")
    private Date endTime;

    @ApiModelProperty(value = "活动背景图片")
    private String actBackImg;

    @ApiModelProperty(value = "活动背景颜色")
    private String actBackColor;

    @ApiModelProperty(value = "专区集合")
    private List<ActZoneListRESP> children;

    @ApiModelProperty(value = "活动开始时间,格式yyyy-MM-dd HH:mm:ss")
    private String beginTimeStr;

    @ApiModelProperty(value = "活动结束时间,格式yyyy-MM-dd HH:mm:ss")
    private String endTimeStr;
}
