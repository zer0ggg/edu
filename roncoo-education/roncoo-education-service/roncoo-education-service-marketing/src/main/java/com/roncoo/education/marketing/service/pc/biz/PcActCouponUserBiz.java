package com.roncoo.education.marketing.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.common.req.ActCouponUserEditREQ;
import com.roncoo.education.marketing.common.req.ActCouponUserListREQ;
import com.roncoo.education.marketing.common.req.ActCouponUserSaveREQ;
import com.roncoo.education.marketing.common.resp.ActCouponUserListRESP;
import com.roncoo.education.marketing.common.resp.ActCouponUserViewRESP;
import com.roncoo.education.marketing.service.dao.ActCouponUserDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUser;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 优惠券用户关联
 *
 * @author wujing
 */
@Component
public class PcActCouponUserBiz extends BaseBiz {

    @Autowired
    private ActCouponUserDao dao;

    /**
    * 优惠券用户关联列表
    *
    * @param actCouponUserListREQ 优惠券用户关联分页查询参数
    * @return 优惠券用户关联分页查询结果
    */
    public Result<Page<ActCouponUserListRESP>> list(ActCouponUserListREQ actCouponUserListREQ) {
        ActCouponUserExample example = new ActCouponUserExample();
        Criteria c = example.createCriteria();
        Page<ActCouponUser> page = dao.listForPage(actCouponUserListREQ.getPageCurrent(), actCouponUserListREQ.getPageSize(), example);
        Page<ActCouponUserListRESP> respPage = PageUtil.transform(page, ActCouponUserListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 优惠券用户关联添加
    *
    * @param actCouponUserSaveREQ 优惠券用户关联
    * @return 添加结果
    */
    public Result<String> save(ActCouponUserSaveREQ actCouponUserSaveREQ) {
        ActCouponUser record = BeanUtil.copyProperties(actCouponUserSaveREQ, ActCouponUser.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 优惠券用户关联查看
    *
    * @param id 主键ID
    * @return 优惠券用户关联
    */
    public Result<ActCouponUserViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActCouponUserViewRESP.class));
    }


    /**
    * 优惠券用户关联修改
    *
    * @param actCouponUserEditREQ 优惠券用户关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(ActCouponUserEditREQ actCouponUserEditREQ) {
        ActCouponUser record = BeanUtil.copyProperties(actCouponUserEditREQ, ActCouponUser.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 优惠券用户关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
