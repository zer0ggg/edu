package com.roncoo.education.marketing.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 优惠券活动
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActCouponDTO", description="优惠券活动")
public class ActCouponDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "发放卷总量")
    private Integer total;

    @ApiModelProperty(value = "已使用数量")
    private Integer usedNum;

    @ApiModelProperty(value = "领取数量")
    private Integer receiveNum;

    @ApiModelProperty(value = "领取限制（0不限制，其他代表张数）")
    private Integer limitReceive;

    @ApiModelProperty(value = "课程Id")
    private Long courseId;

    @ApiModelProperty(value = "优惠价格")
    private BigDecimal couponPrice;

    @ApiModelProperty(value = "活动Id")
    private Long actId;
}
