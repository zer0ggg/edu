package com.roncoo.education.marketing.service.pc.biz;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.marketing.common.req.*;
import com.roncoo.education.marketing.common.resp.ActListRESP;
import com.roncoo.education.marketing.common.resp.ActViewRESP;
import com.roncoo.education.marketing.service.dao.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 活动信息表
 *
 * @author wujing
 */
@Component
public class PcActBiz extends BaseBiz {

    @Autowired
    private ActDao dao;

    @Autowired
    private ActZoneDao actZoneDao;
    @Autowired
    private ActZoneCourseDao zoneCourseDao;
    @Autowired
    private ActCouponDao couponDao;
    @Autowired
    private ActSeckillDao seckillDao;

    /**
     * 活动信息表列表
     *
     * @param req 活动信息表分页查询参数
     * @return 活动信息表分页查询结果
     */
    public Result<Page<ActListRESP>> list(ActListREQ req) {
        ActExample example = new ActExample();
        Criteria c = example.createCriteria();
        if (StrUtil.isNotEmpty(req.getActTitle())) {
            c.andActTitleLike(PageUtil.like(req.getActTitle()));
        }
        if (req.getActType() != null) {
            c.andActTypeEqualTo(req.getActType());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<Act> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ActListRESP> respPage = PageUtil.transform(page, ActListRESP.class);
        // 将展示的时间标准化
        for(ActListRESP resp: respPage.getList()){
            resp.setBeginTimeStr(DateUtil.format(resp.getBeginTime(),"yyyy-MM-dd HH:mm:ss"));
            resp.setEndTimeStr(DateUtil.format(resp.getEndTime(),"yyyy-MM-dd HH:mm:ss"));
        }
        return Result.success(respPage);
    }


    /**
     * 活动信息表添加
     *
     * @param req 活动信息表
     * @return 添加结果
     */
    public Result<String> save(ActSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //检验
        if (req.getBeginTime().after(req.getEndTime())) {
            return Result.error("活动时间开始时间必须小于结束时间");
        }
        //同一时间段，不允许出现相同类型的活动
        List<Act> list = dao.listByActType(req.getActType());
        for (Act act : list) {
            //活动开始时间 介于已存在活动时间期间
            if (req.getBeginTime().compareTo(act.getBeginTime()) >= 0 && req.getBeginTime().compareTo(act.getEndTime()) < 0) {
                return Result.error("同时间段内只能添加一个活动类型：已存在活动【" + act.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(act.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
            //活动结束时间 介于已存在活动时间期间
            if (req.getEndTime().compareTo(act.getBeginTime()) > 0 && req.getEndTime().compareTo(act.getEndTime()) <= 0) {
                return Result.error("同时间段内只能添加一个活动类型：已存在活动【" + act.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(act.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
        }
        Act record = BeanUtil.copyProperties(req, Act.class);

        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 活动信息表查看
     *
     * @param id 主键ID
     * @return 活动信息表
     */
    public Result<ActViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActViewRESP.class));
    }


    /**
     * 活动信息表修改
     *
     * @param req 活动信息表修改对象
     * @return 修改结果
     */
    public Result<String> edit(ActEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Act record = dao.getById(req.getId());
        if (ObjectUtil.isEmpty(record)) {
            return Result.error("找不到活动信息");
        }
        Act newRecord = BeanUtil.copyProperties(req, Act.class);
        //校验重复时间
        if (req.getBeginTime().after(req.getEndTime())) {
            return Result.error("活动时间开始时间必须小于结束时间");
        }
        List<Act> list = dao.listByBeginTimeAndAndEndTimeAndActTypeAndNotId(req.getId(), req.getBeginTime(), req.getEndTime(), record.getActType());
        if (CollectionUtil.isNotEmpty(list)) {
            return Result.error("同时间段内只能添加一个活动类型");
        }
        List<ActZoneCourse> courseList = zoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(record.getId(), null, null, null);
        if (CollectionUtil.isNotEmpty(courseList)) {
            for (ActZoneCourse act : courseList) {
                actZoneCourseCheck(act, req.getBeginTime(), req.getEndTime(), record.getId());
            }
        }

        if (dao.updateById(newRecord) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    private void actZoneCourseCheck(ActZoneCourse act, Date beginTime, Date endTime, Long id) {
        List<ActZoneCourse> courseList = zoneCourseDao.listByCourseId(act.getCourseId());
        for (ActZoneCourse course : courseList) {
            if (course.getActId().longValue() != id.longValue()) {
                Act act1 = dao.getById(course.getActId());
                //活动开始时间 介于已存在活动时间期间
                if (beginTime.compareTo(act1.getBeginTime()) >= 0 && beginTime.compareTo(act1.getEndTime()) < 0) {
                    throw new BaseException("同时间段内课程不能重复添加 【课程名称：" + act.getCourseName() + "】");
                }
                //活动结束时间 介于已存在活动时间期间
                if (endTime.compareTo(act1.getBeginTime()) > 0 && endTime.compareTo(act1.getEndTime()) <= 0) {
                    throw new BaseException("同时间段内课程不能重复添加 【课程名称：" + act.getCourseName() + "】");
                }
            }
        }
    }

    /**
     * 活动信息表删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        Act act = dao.getById(id);
        if (ObjectUtil.isNull(act)) {
            return Result.error("活动不存在");
        }
        List<ActZone> zoneList = actZoneDao.listByActId(id);
        if (CollUtil.isNotEmpty(zoneList)) {
            return Result.error("活动下存在专区，请先专区");
        }

        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 修改状态
     *
     * @param actUpdateStatusREQ 状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(ActUpdateStatusREQ actUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(actUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(actUpdateStatusREQ.getId()))) {
            return Result.error("渠道不存在");
        }
        Act record = BeanUtil.copyProperties(actUpdateStatusREQ, Act.class);
        if (dao.updateById(record) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> copy(ActCopyREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Act sourceAct = dao.getById(req.getId());
        if (sourceAct == null) {
            return Result.error("活动不存在");
        }
        // 检验活动
        validTimeForCopyAct(sourceAct, req.getBeginTime(), req.getEndTime());

        //拷贝 活动，专区，课程
        Act act = BeanUtil.copyProperties(sourceAct, Act.class);
        act.setActTitle(req.getActTitle());
        act.setBeginTime(req.getBeginTime());
        act.setEndTime(req.getEndTime());
        if (!StringUtils.isEmpty(req.getRemark())) {
            act.setRemark(req.getRemark());
        }
        if (!StringUtils.isEmpty(req.getActImg())) {
            act.setActImg(req.getActImg());
        }
        if (req.getSort() != null) {
            act.setSort(req.getSort());
        }
        dao.save(act);

        //查出  活动下所有 专区、课程、附加信息
        List<ActZone> sourZoneList = actZoneDao.listByActId(sourceAct.getId());
        List<ActZoneCourse> sourceZoneCourseList = zoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(sourceAct.getId(), null, null, null);
        List<ActCoupon> sourceCouponList = couponDao.listByActIdAndZoneId(sourceAct.getId(), null);
        List<ActSeckill> sourceSeckillList = seckillDao.listByActIdAndZoneId(sourceAct.getId(), null);

        //按专区不同  拷贝 课程和附加信息
        for (ActZone sourceZone : sourZoneList) {
            ActZone zone = BeanUtil.copyProperties(sourceZone, ActZone.class);
            zone.setActId(act.getId());
            zone.setId(null);
            actZoneDao.save(zone);
            List<ActZoneCourse> matchZoneList = sourceZoneCourseList.stream().filter(x -> x.getZoneId().equals(sourceZone.getId())).collect(Collectors.toList());
            for (ActZoneCourse sourceCourse : matchZoneList) {
                ActZoneCourse zoneCourse = BeanUtil.copyProperties(sourceCourse, ActZoneCourse.class);
                zoneCourse.setActId(act.getId());
                zoneCourse.setZoneId(zone.getId());
                zoneCourse.setId(null);
                zoneCourseDao.save(zoneCourse);
            }

            List<ActCoupon> matchCouponList = sourceCouponList.stream().filter(x -> x.getZoneId().equals(sourceZone.getId())).collect(Collectors.toList());
            for (ActCoupon sourceCoupon : matchCouponList) {
                ActCoupon coupon = BeanUtil.copyProperties(sourceCoupon, ActCoupon.class);
                coupon.setActId(act.getId());
                coupon.setZoneId(zone.getId());
                coupon.setUsedNum(0);
                coupon.setReceiveNum(0);
                coupon.setId(null);
                couponDao.save(coupon);
            }

            List<ActSeckill> matchSeckillList = sourceSeckillList.stream().filter(x -> x.getZoneId().equals(sourceZone.getId())).collect(Collectors.toList());
            for (ActSeckill sourceSeckill : matchSeckillList) {
                ActSeckill seckill = BeanUtil.copyProperties(sourceSeckill, ActSeckill.class);
                seckill.setActId(act.getId());
                seckill.setZoneId(zone.getId());
                seckill.setId(null);
                seckill.setHasBuy(0);
                seckillDao.save(seckill);
            }

        }
        return Result.success("操作成功");
    }


    /**
     * 检验活动
     *
     * @param sourceAct
     * @param beginTime
     * @param endTime
     */
    private void validTimeForCopyAct(Act sourceAct, Date beginTime, Date endTime) {
        //检验
        List<Act> sameTypeActList = null;//同类型活动集合
        List<Act> differentTypeActList = null;//不同类型活动集合
        if (ActTypeEnum.COUPON.getCode().equals(sourceAct.getActType())) {
            sameTypeActList = dao.listByActType(ActTypeEnum.COUPON.getCode());
            differentTypeActList = dao.listByActType(ActTypeEnum.SECKILL.getCode());
        } else {
            sameTypeActList = dao.listByActType(ActTypeEnum.SECKILL.getCode());
            differentTypeActList = dao.listByActType(ActTypeEnum.COUPON.getCode());
        }
        // 同一类型活动，时间不重复
        for (Act act : sameTypeActList) {
            //活动开始时间 介于已存在活动时间期间
            if (beginTime.compareTo(act.getBeginTime()) >= 0 && beginTime.compareTo(act.getEndTime()) < 0) {
                throw new BaseException("同时间段内只能添加一个活动类型：已存在活动【" + act.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(act.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
            //活动结束时间 介于已存在活动时间期间
            if (endTime.compareTo(act.getBeginTime()) > 0 && endTime.compareTo(act.getEndTime()) <= 0) {
                throw new BaseException("同时间段内只能添加一个活动类型：已存在活动【" + act.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(act.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");

            }
        }

        //不同类型活动，有重叠时，课程不能重叠
        List<ActZoneCourse> soureActCourseList = zoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(sourceAct.getId(), null, null, null);
        List<Long> soureCourseIds = soureActCourseList.stream().map(ActZoneCourse::getCourseId).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(soureCourseIds)) {
            //原活动未关联课程，不校验课程
            return;
        }
        for (Act act : differentTypeActList) {
            //找时间重叠 的活动
            // 开始时间>=已存在活动的结束时间 或 结束时间<=已存在活动的开始时间 则不存在时间重叠，跳过
            if (beginTime.after(act.getEndTime()) || endTime.before(act.getBeginTime())) {
                continue;
            }
            //检验课程
            List<ActZoneCourse> targetActCourseList = zoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(act.getId(), null, null, null);
            List<Long> targetCourseIds = targetActCourseList.stream().map(ActZoneCourse::getCourseId).collect(Collectors.toList());
            if (CollectionUtil.isEmpty(targetCourseIds)) {
                //目标活动未关联课程，跳过
                continue;
            }
            boolean repeat = targetCourseIds.retainAll(soureCourseIds);
            if (repeat) {
                throw new BaseException("课程重复，活动【" + act.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(act.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
        }

    }
}
