package com.roncoo.education.marketing.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.feign.qo.ActCouponQO;
import com.roncoo.education.marketing.feign.vo.ActCouponVO;
import com.roncoo.education.marketing.service.dao.ActCouponDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 优惠券活动
 *
 * @author wujing
 */
@Component
public class FeignActCouponBiz extends BaseBiz {

    @Autowired
    private ActCouponDao dao;

	public Page<ActCouponVO> listForPage(ActCouponQO qo) {
	    ActCouponExample example = new ActCouponExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ActCoupon> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ActCouponVO.class);
	}

	public int save(ActCouponQO qo) {
		ActCoupon record = BeanUtil.copyProperties(qo, ActCoupon.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ActCouponVO getById(Long id) {
		ActCoupon record = dao.getById(id);
		return BeanUtil.copyProperties(record, ActCouponVO.class);
	}

	public int updateById(ActCouponQO qo) {
		ActCoupon record = BeanUtil.copyProperties(qo, ActCoupon.class);
		return dao.updateById(record);
	}

}
