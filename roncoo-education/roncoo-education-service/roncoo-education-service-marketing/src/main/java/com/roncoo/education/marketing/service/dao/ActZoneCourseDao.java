package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourse;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample;

import java.util.List;

/**
 * 活动专区课程 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActZoneCourseDao {

    /**
    * 保存活动专区课程
    *
    * @param record 活动专区课程
    * @return 影响记录数
    */
    int save(ActZoneCourse record);

    /**
    * 根据ID删除活动专区课程
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 活动专区课程
    * @return 影响记录数
    */
    int updateById(ActZoneCourse record);

    /**
    * 根据ID获取活动专区课程
    *
    * @param id 主键ID
    * @return 活动专区课程
    */
    ActZoneCourse getById(Long id);

    /**
     * 根据专区id和课程id获取活动专区课程
     * @param zoneId
     * @param courseId
     * @return
     */
    ActZoneCourse getByZoneIdAndCourseId(Long zoneId, Long courseId);

    /**
    * 活动专区课程--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ActZoneCourse> listForPage(int pageCurrent, int pageSize, ActZoneCourseExample example);

    /**
     * 根据专区ID获取专区课程信息
     * @param zoneId
     * @return
     */
    List<ActZoneCourse> listByZoneId(Long zoneId);

    /**
     * 根据课程ID获取专区列表
     * @param courseId
     * @return
     */
    List<ActZoneCourse> listByCourseId(Long courseId);
    /**
     * 根据课程id、状态专区课程信息
     *
     * @param courseId
     * @param statusId
     * @return
     */
    List<ActZoneCourse> listByCourseIdAndStatusId(Long courseId, Integer statusId);

    /**
     * 根据活动ID、专区id、课程id、状态专区课程信息
     *
     * @param actId
     * @param zoneId
     * @param courseId
     * @param statusId
     * @return
     */
    List<ActZoneCourse> listByActIdAndZoneIdAndCourseIdAndStatusId(Long actId, Long zoneId, Long courseId,Integer statusId);

    /**
     * 根据活动类型、课程id、状态专区课程信息
     *
     * @param actType
     * @param courseId
     * @param statusId
     * @return
     */
    List<ActZoneCourse> listByActTypeAndCourseIdAndStatusId(Integer actType, Long courseId, Integer statusId);

    /**
     * 根据专区ID、状态专区课程信息
     *
     * @param zoneId
     * @return
     */
    List<ActZoneCourse> listByZoneIdAndStatusId(Long zoneId, Integer statusId);

}
