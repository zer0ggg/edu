package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZone;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample;

import java.util.Date;
import java.util.List;

/**
 * 活动专区 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActZoneDao {

    /**
     * 保存活动专区
     *
     * @param record 活动专区
     * @return 影响记录数
     */
    int save(ActZone record);

    /**
     * 根据ID删除活动专区
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 活动专区
     * @return 影响记录数
     */
    int updateById(ActZone record);

    /**
     * 根据ID获取活动专区
     *
     * @param id 主键ID
     * @return 活动专区
     */
    ActZone getById(Long id);

    /**
     * 活动专区--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ActZone> listForPage(int pageCurrent, int pageSize, ActZoneExample example);


    /**
     * 根据活动ID获取活动专区
     *
     * @param actId 活动ID
     * @return 活动专区集合
     */
    List<ActZone> listByActId(Long actId);

    /**
     * 根据活动ID, 状态获取活动专区
     *
     * @param actId
     * @param statusId
     * @return
     */
    List<ActZone> listByActIdAndStatusId(Long actId, Integer statusId);

    /**
     * 根据活动ID获取可用正在秒杀的专区
     *
     * @param actId
     * @param statusId
     * @param date
     * @return
     */
    ActZone listByActIdAndStatusIdAndDate(Long actId, Integer statusId, Date date);
    /**
     * 根据专区ID获取可用正在秒杀的专区
     *
     * @param zoneId
     * @param statusId
     * @param time
     * @return
     */
    ActZone getByIdAndStatusIdAndTime(Long zoneId, Integer statusId, Date time);
}

