package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample;

import java.util.List;

/**
 * 优惠券活动 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActCouponDao {

    /**
    * 保存优惠券活动
    *
    * @param record 优惠券活动
    * @return 影响记录数
    */
    int save(ActCoupon record);

    /**
    * 根据ID删除优惠券活动
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 优惠券活动
    * @return 影响记录数
    */
    int updateById(ActCoupon record);

    /**
    * 根据ID获取优惠券活动
    *
    * @param id 主键ID
    * @return 优惠券活动
    */
    ActCoupon getById(Long id);

    /**
     * 根据ID获取优惠券活动
     *
     * @param id 主键ID
     * @return 优惠券活动
     */
    /**
     * 根据活专区id和课程id获取优惠券活动
     * @param zoneId
     * @param courseId
     * @return
     */
    ActCoupon getByZoneIdAndCourseId(Long zoneId, Long courseId);

    /**
    * 优惠券活动--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ActCoupon> listForPage(int pageCurrent, int pageSize, ActCouponExample example);

    /**
     *
     * @param zoneId
     * @return
     */
    /**
     * 根据专区id和活动id查询
     * @param actId
     * @param zoneId
     * @return
     */
    List<ActCoupon> listByActIdAndZoneId(Long actId, Long zoneId);

    /**
     * 根据活动id、专区id、课程id、状态查询秒杀信息
     * @param actId
     * @param zoneId
     * @param courseId
     * @param statusId
     * @return
     */
    ActCoupon getByActIdAndZondIdAndCourseIdAndStatusId(Long actId, Long zoneId,Long courseId, Integer statusId);
}
