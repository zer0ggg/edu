package com.roncoo.education.marketing.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActSaveREQ", description="活动信息表添加")
public class ActSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @NotNull(message = "活动类型(1优惠券,2秒杀)不能为空")
    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)",required = true)
    private Integer actType;

    @NotEmpty(message = "活动标题不能为空")
    @ApiModelProperty(value = "活动标题",required = true)
    private String actTitle;

    @NotEmpty(message = "活动图片不能为空")
    @ApiModelProperty(value = "活动图片",required = true)
    private String actImg;

    @NotNull(message = "活动开始时间不能为空")
    @ApiModelProperty(value = "活动开始时间",required = true)
    private Date beginTime;

    @NotNull(message = "活动结束时间不能为空")
    @ApiModelProperty(value = "活动结束时间",required = true)
    private Date endTime;

    @ApiModelProperty(value = "活动背景图片")
    private String actBackImg;

    @ApiModelProperty(value = "活动背景颜色")
    private String actBackColor;

}
