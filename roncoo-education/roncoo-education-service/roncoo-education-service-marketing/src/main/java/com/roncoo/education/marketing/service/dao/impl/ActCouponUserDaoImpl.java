package com.roncoo.education.marketing.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActCouponUserDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActCouponUserMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUser;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 优惠券用户关联 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActCouponUserDaoImpl implements ActCouponUserDao {

    @Autowired
    private ActCouponUserMapper mapper;

    @Override
    public int save(ActCouponUser record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ActCouponUser record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ActCouponUser getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ActCouponUser> listForPage(int pageCurrent, int pageSize, ActCouponUserExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ActCouponUser getByUserNoAndCouponIdAndTimeAndIsUse(Long userNo, Long couponId, Date date, Integer isUse) {
        ActCouponUserExample example = new ActCouponUserExample();
        ActCouponUserExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andCouponIdEqualTo(couponId);
        c.andPastTimeGreaterThan(date);
        c.andIsUseEqualTo(isUse);
        List<ActCouponUser> list = this.mapper.selectByExample(example);
        if (!CollectionUtil.isEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public int countByCouponIdAndUserNo(Long couponId, Long userNo) {
        ActCouponUserExample example = new ActCouponUserExample();
        ActCouponUserExample.Criteria c = example.createCriteria();
        c.andCouponIdEqualTo(couponId);
        c.andUserNoEqualTo(userNo);
        return this.mapper.countByExample(example);
    }

    @Override
    public int updateByIsUse(Long couponId, Integer isUse) {
        ActCouponUserExample example = new ActCouponUserExample();
        ActCouponUserExample.Criteria c = example.createCriteria();
        c.andCouponIdEqualTo(couponId);
        ActCouponUser record = new ActCouponUser();
        record.setCouponId(couponId);
        record.setIsUse(isUse);
        return this.mapper.updateByExampleSelective(record, example);
    }


}
