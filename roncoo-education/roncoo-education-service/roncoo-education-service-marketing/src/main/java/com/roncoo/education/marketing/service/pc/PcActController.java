package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.marketing.common.req.*;
import com.roncoo.education.marketing.common.resp.ActListRESP;
import com.roncoo.education.marketing.common.resp.ActViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 活动信息表 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-活动信息表")
@RestController
@RequestMapping("/marketing/pc/act")
public class PcActController {

    @Autowired
    private PcActBiz biz;

    @ApiOperation(value = "活动信息表列表", notes = "活动信息表列表")
    @PostMapping(value = "/list")
    public Result<Page<ActListRESP>> list(@RequestBody ActListREQ actListREQ) {
        return biz.list(actListREQ);
    }


    @ApiOperation(value = "活动信息表添加", notes = "活动信息表添加")
    @SysLog(value = "活动信息表添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActSaveREQ actSaveREQ) {
        return biz.save(actSaveREQ);
    }


    @ApiOperation(value = "复制", notes = "复制")
    @SysLog(value = "活动信息表复制")
    @PostMapping(value = "/copy")
    public Result<String> copy(@RequestBody ActCopyREQ req) {
        return biz.copy(req);
    }

    @ApiOperation(value = "活动信息表查看", notes = "活动信息表查看")
    @GetMapping(value = "/view")
    public Result<ActViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "活动信息表修改", notes = "活动信息表修改")
    @SysLog(value = "活动信息表修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActEditREQ actEditREQ) {
        return biz.edit(actEditREQ);
    }


    @ApiOperation(value = "活动信息表删除", notes = "活动信息表删除")
    @SysLog(value = "活动信息表删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "活动状态修改", notes = "活动状态修改")
    @SysLog(value = "活动状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid ActUpdateStatusREQ actUpdateStatusREQ) {
        return biz.updateStatus(actUpdateStatusREQ);
    }
}
