package com.roncoo.education.marketing.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 活动集合
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthActUserCourseViewDTO", description="专区课程的集合信息")
public class AuthActUserCourseViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动类型(1优惠券,2秒杀)
     */
    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;
    /**
     * 活动ID
     */
    @ApiModelProperty(value = "活动ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;
    /**
     * 专区ID
     */
    @ApiModelProperty(value = "专区ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long zoneId;
    /**
     * 课程ID
     */
    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long courseId;
    /**
     * 课程名称
     */
    @ApiModelProperty(value = "课程名称")
    private String courseName;

    /**
     * 原价
     */
    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;
    /**
     * 优惠价格
     */
    @ApiModelProperty(value = "会员价格")
    private BigDecimal courseDiscount;
    /**
     * 活动价格
     */
    @ApiModelProperty(value = "活动价格")
    private BigDecimal actPrice;


}
