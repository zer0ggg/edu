/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.marketing.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户可以参与的课程的活动集合
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthActUserCoureseBO", description = "用户可以参与的课程的活动集合")
public class AuthActUserCoureseBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "课程ID不能为空")
    @ApiModelProperty(value = "课程id", required = true)
    private Long courseId;
}
