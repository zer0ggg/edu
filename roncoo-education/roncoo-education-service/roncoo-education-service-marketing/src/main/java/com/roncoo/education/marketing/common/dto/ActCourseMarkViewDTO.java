package com.roncoo.education.marketing.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 活动课程关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActCourseMarkViewDTO", description="活动课程关联")
public class ActCourseMarkViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long courseId;



    /**
     * 活动信息
     */
    @ApiModelProperty(value = "活动信息", required = true)
    private List<ActZoneCourseUserViewDTO> actZoneCourseUserViewDTO = new ArrayList<>();
	
}
