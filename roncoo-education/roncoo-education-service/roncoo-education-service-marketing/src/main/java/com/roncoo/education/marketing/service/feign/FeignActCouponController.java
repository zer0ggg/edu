package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignActCoupon;
import com.roncoo.education.marketing.feign.qo.ActCouponQO;
import com.roncoo.education.marketing.feign.vo.ActCouponVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActCouponBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 优惠券活动
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActCouponController extends BaseController implements IFeignActCoupon{

    @Autowired
    private FeignActCouponBiz biz;

	@Override
	public Page<ActCouponVO> listForPage(@RequestBody ActCouponQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActCouponQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActCouponQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActCouponVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
