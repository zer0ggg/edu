package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.Act;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActMapper {
    int countByExample(ActExample example);

    int deleteByExample(ActExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Act record);

    int insertSelective(Act record);

    List<Act> selectByExample(ActExample example);

    Act selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Act record, @Param("example") ActExample example);

    int updateByExample(@Param("record") Act record, @Param("example") ActExample example);

    int updateByPrimaryKeySelective(Act record);

    int updateByPrimaryKey(Act record);
}
