package com.roncoo.education.marketing.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActViewDTO", description="活动信息表")
public class ActViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "活动标题")
    private String actTitle;

    @ApiModelProperty(value = "活动图片")
    private String actImg;

    @ApiModelProperty(value = "活动背景图片")
    private String actBackImg;

    @ApiModelProperty(value = "活动背景颜色")
    private String actBackColor;

    @ApiModelProperty(value = "活动开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "活动结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "活动专区集合")
    private List<ActZoneListDTO> actZoneList = new ArrayList<>();
}
