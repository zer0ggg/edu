package com.roncoo.education.marketing.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.Act;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 活动信息表 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActDaoImpl implements ActDao {

    @Autowired
    private ActMapper mapper;

    @Override
    public int save(Act record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Act record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Act getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Act> listForPage(int pageCurrent, int pageSize, ActExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<Act> listByActType(Integer actType) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andActTypeEqualTo(actType);
        example.setOrderByClause("sort desc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<Act> listByActIdListAndStatusId(List<Long> actIdList,Integer statusId) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andIdIn(actIdList);
        if(statusId != null){
            c.andStatusIdEqualTo(statusId);
        }
        example.setOrderByClause("sort desc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<Act> listByBeginTimeAndAndEndTimeAndActType(Date beginTime, Date endTime, Integer actType) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andBeginTimeLessThanOrEqualTo(beginTime);
        c.andEndTimeGreaterThanOrEqualTo(endTime);
        c.andActTypeEqualTo(actType);
        example.setOrderByClause("sort desc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public Act getByIdAndStatusIdAndTime(Long actId, Integer statusId, Date date) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(actId);
        c.andStatusIdEqualTo(statusId);
        c.andBeginTimeLessThanOrEqualTo(date);
        c.andEndTimeGreaterThanOrEqualTo(date);
        List<Act> list = this.mapper.selectByExample(example);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<Act> listByTimeAndStatusId(Date date, Integer statusId) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andBeginTimeLessThanOrEqualTo(date);
        c.andEndTimeGreaterThanOrEqualTo(date);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort desc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public Act listByTimeAndActTypeAndStatusId(Date date, Integer actType, Integer statusId) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andActTypeEqualTo(actType);
        c.andStatusIdEqualTo(statusId);
        c.andBeginTimeLessThanOrEqualTo(date);
        c.andEndTimeGreaterThanOrEqualTo(date);
        List<Act> list = this.mapper.selectByExample(example);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<Act> listByBeginTimeAndAndEndTimeAndActTypeAndNotId(Long actId, Date beginTime, Date endTime, Integer actType) {
        ActExample example = new ActExample();
        ActExample.Criteria c = example.createCriteria();
        c.andIdNotEqualTo(actId);
        c.andActTypeEqualTo(actType);
        c.andBeginTimeLessThanOrEqualTo(beginTime);
        c.andEndTimeGreaterThanOrEqualTo(endTime);
        return this.mapper.selectByExample(example);
    }


}
