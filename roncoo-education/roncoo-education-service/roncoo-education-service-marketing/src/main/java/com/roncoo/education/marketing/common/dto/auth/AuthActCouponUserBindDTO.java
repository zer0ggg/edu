package com.roncoo.education.marketing.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthActCouponUserBindDTO", description="优惠券用户关联表")
public class AuthActCouponUserBindDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 活动ID
     */
    @ApiModelProperty(value = "活动ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;
    /**
     * 优惠券ID
     */
    @ApiModelProperty(value = "优惠券ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long couponId;
    /**
     * 优惠价格
     */
    @ApiModelProperty(value = "优惠价格")
    private BigDecimal couponPrice;
    /**
     * 用户编号
     */
	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
    private Long userNo;
    /**
     * 过期时间
     */
	@ApiModelProperty(value = "过期时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date pastTime;
    /**
     * 是否使用
     */
	@ApiModelProperty(value = "是否使用(1使用,0未使用)")
    private Integer isUse;
	/**
	 * 手机号码
	 */
	@ApiModelProperty(value = "手机号码")
	private String mobile;
}
