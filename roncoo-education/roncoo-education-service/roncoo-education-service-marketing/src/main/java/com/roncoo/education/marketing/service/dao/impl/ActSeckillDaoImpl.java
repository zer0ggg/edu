package com.roncoo.education.marketing.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActSeckillDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActSeckillMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 课程秒杀活动 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActSeckillDaoImpl implements ActSeckillDao {

    @Autowired
    private ActSeckillMapper mapper;

    @Override
    public int save(ActSeckill record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteByZoneIdCourseId(Long zoneId, Long courseId) {
        ActSeckillExample example = new ActSeckillExample();
        ActSeckillExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(courseId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int updateById(ActSeckill record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ActSeckill getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ActSeckill> listForPage(int pageCurrent, int pageSize, ActSeckillExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ActSeckill> listByActIdAndZoneId(Long actId, Long zoneId) {
        ActSeckillExample example = new ActSeckillExample();
        ActSeckillExample.Criteria c  = example.createCriteria();
        if(actId != null){
            c.andActIdEqualTo(actId);
        }
        if(zoneId != null){
            c.andZoneIdEqualTo(zoneId);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public ActSeckill getByZoneIdAndCourseIdAndStatusId(Long zoneId, Long courseId, Integer statusId) {
        ActSeckillExample example = new ActSeckillExample();
        ActSeckillExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(courseId);
        c.andStatusIdEqualTo(statusId);
        List<ActSeckill> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public ActSeckill getByZoneIdAndCourseId(Long zoneId, Long courseId) {
        ActSeckillExample example = new ActSeckillExample();
        ActSeckillExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(courseId);
        List<ActSeckill> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
