package com.roncoo.education.marketing.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActSaveREQ", description="活动信息表添加")
public class ActZoneCopyREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "被复制专区ID不能为空")
    @ApiModelProperty(value = "被复制专区ID", required = true)
    private Long id;

    @NotEmpty(message = "专区标题不能为空")
    @ApiModelProperty(value = "专区标题", required = true)
    private String zoneTitle;


    @ApiModelProperty(value = "秒杀开始时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

}
