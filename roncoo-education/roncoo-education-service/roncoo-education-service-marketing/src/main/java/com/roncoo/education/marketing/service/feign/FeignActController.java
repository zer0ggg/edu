package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignAct;
import com.roncoo.education.marketing.feign.qo.ActOrderPayBO;
import com.roncoo.education.marketing.feign.qo.ActQO;
import com.roncoo.education.marketing.feign.vo.ActOrderVO;
import com.roncoo.education.marketing.feign.vo.ActVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 活动信息表
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActController extends BaseController implements IFeignAct{

    @Autowired
    private FeignActBiz biz;

	@Override
	public Page<ActVO> listForPage(@RequestBody ActQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

    @Override
    public ActOrderVO order(@RequestBody ActOrderPayBO actOrderPayBO) {
        return biz.order(actOrderPayBO);
    }
}
