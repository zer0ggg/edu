package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignActSeckill;
import com.roncoo.education.marketing.feign.qo.ActSeckillQO;
import com.roncoo.education.marketing.feign.vo.ActSeckillVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActSeckillBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程秒杀活动
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActSeckillController extends BaseController implements IFeignActSeckill{

    @Autowired
    private FeignActSeckillBiz biz;

	@Override
	public Page<ActSeckillVO> listForPage(@RequestBody ActSeckillQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActSeckillQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActSeckillQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActSeckillVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
