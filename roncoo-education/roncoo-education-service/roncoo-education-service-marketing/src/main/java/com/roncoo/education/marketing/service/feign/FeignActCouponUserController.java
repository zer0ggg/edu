package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignActCouponUser;
import com.roncoo.education.marketing.feign.qo.ActCouponUserQO;
import com.roncoo.education.marketing.feign.vo.ActCouponUserVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActCouponUserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 优惠券用户关联
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActCouponUserController extends BaseController implements IFeignActCouponUser{

    @Autowired
    private FeignActCouponUserBiz biz;

	@Override
	public Page<ActCouponUserVO> listForPage(@RequestBody ActCouponUserQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActCouponUserQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActCouponUserQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActCouponUserVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

    @Override
    public int updateByIsUse(@RequestBody ActCouponUserQO actCouponUserQO) {
        return biz.updateByIsUse(actCouponUserQO);
    }
}
