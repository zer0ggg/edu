package com.roncoo.education.marketing.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 优惠券用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthActCouponUserListDTO", description="优惠券用户关联表")
public class AuthActCouponUserListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "专区ID")
    private Long zoneId;

    @ApiModelProperty(value = "优惠券ID")
    private Long couponId;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程图像")
    private String courseImg;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "会员价格")
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "优惠券价格")
    private BigDecimal couponPrice;

    @ApiModelProperty(value = "有效时间-结束")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "优惠券状态(1正常,3已过期)")
    private Integer statusId;

    @ApiModelProperty(value = "是否已使用(1使用,0未使用)")
    private Integer isUse;

}
