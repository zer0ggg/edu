package com.roncoo.education.marketing.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActSaveREQ", description="活动信息表添加")
public class ActCopyREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "被复制活动id不能为空")
    @ApiModelProperty(value = "被复制活动ID",required = true)
    private Long id;

    @NotEmpty(message = "活动标题不能为空")
    @ApiModelProperty(value = "活动标题",required = true)
    private String actTitle;

    @NotNull(message = "活动开始时间不能为空")
    @ApiModelProperty(value = "活动开始时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @NotNull(message = "活动结束时间不能为空")
    @ApiModelProperty(value = "活动结束时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "活动图片")
    private String actImg;
}
