package com.roncoo.education.marketing.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.Act;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActExample;

import java.util.Date;
import java.util.List;

/**
 * 活动信息表 服务类
 *
 * @author wujing
 * @date 2020-06-23
 */
public interface ActDao {

    /**
     * 保存活动信息表
     *
     * @param record 活动信息表
     * @return 影响记录数
     */
    int save(Act record);

    /**
     * 根据ID删除活动信息表
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 活动信息表
     * @return 影响记录数
     */
    int updateById(Act record);

    /**
     * 根据ID获取活动信息表
     *
     * @param id 主键ID
     * @return 活动信息表
     */
    Act getById(Long id);

    /**
     * 活动信息表--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<Act> listForPage(int pageCurrent, int pageSize, ActExample example);

    /**
     * 根据活动类型获取活动信息
     *
     * @param actType
     * @return
     */
    List<Act> listByActType(Integer actType);

    /**
     * 根据活动类型获取活动信息
     * @param actType
     * @return
     */
    /**
     * 根据活动id集合和状态 查询
     *
     * @param actIdList
     * @param statusId
     * @return
     */
    List<Act> listByActIdListAndStatusId(List<Long> actIdList, Integer statusId);

    /**
     * 根据时间段获取、活动类型获取活动信息
     *
     * @param beginTime
     * @param endTime
     * @param actType
     * @return
     */
    List<Act> listByBeginTimeAndAndEndTimeAndActType(Date beginTime, Date endTime, Integer actType);

    /**
     * 根据id、状态、时间查询活动信息
     *
     * @param actId
     * @param statusId
     * @param date
     * @return
     */
    Act getByIdAndStatusIdAndTime(Long actId, Integer statusId, Date date);

    /**
     * 查找当前时间下可用的活动信息
     *
     * @param statusId
     * @param date
     * @return
     */
    List<Act> listByTimeAndStatusId(Date date, Integer statusId);

    /**
     * 查找当前时间下指定活动类型的可用的活动信息
     *
     * @param date
     * @param actType
     * @param statusId
     * @return
     */
    Act listByTimeAndActTypeAndStatusId(Date date, Integer actType, Integer statusId);

    /**
     * 获取同时间段同类型的活动信息
     *
     * @param id
     * @param beginTime
     * @param endTime
     * @param actType
     * @return
     */
    List<Act> listByBeginTimeAndAndEndTimeAndActTypeAndNotId(Long id, Date beginTime, Date endTime, Integer actType);
}
