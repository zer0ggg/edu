package com.roncoo.education.marketing.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.bo.auth.AuthActUserCoureseBO;
import com.roncoo.education.marketing.common.dto.auth.AuthActUserCourseViewDTO;
import com.roncoo.education.marketing.service.api.auth.biz.AuthActBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 活动信息表 UserApi接口
 *
 * @author wujing
 * @date 2020-06-23
 */
@Api(tags = "API-AUTH-活动信息表")
@RestController
@RequestMapping("/marketing/auth/act")
public class AuthActController {

    @Autowired
    private AuthActBiz biz;

    /**
     * 用户可以参与课程的活动列表接口
     *
     * @param authactUserCoureseBO
     * @author
     */
    @ApiOperation(value = "用户可以参与课程的活动列表接口", notes = "用户可以参与课程的活动列表接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<AuthActUserCourseViewDTO> view(@RequestBody @Valid AuthActUserCoureseBO authactUserCoureseBO) {
        return biz.view(authactUserCoureseBO);
    }

}
