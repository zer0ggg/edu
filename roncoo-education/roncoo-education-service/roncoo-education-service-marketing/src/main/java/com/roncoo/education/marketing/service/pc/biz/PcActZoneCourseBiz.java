package com.roncoo.education.marketing.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.IsLimitEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.marketing.common.req.ActZoneCourseEditREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseListREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseSaveREQ;
import com.roncoo.education.marketing.common.req.ActZoneCourseUpadateStatusREQ;
import com.roncoo.education.marketing.common.resp.ActZoneCourseListRESP;
import com.roncoo.education.marketing.common.resp.ActZoneCourseViewRESP;
import com.roncoo.education.marketing.service.dao.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 活动专区课程
 *
 * @author wujing
 */
@Component
public class PcActZoneCourseBiz extends BaseBiz {

    @Autowired
    private ActZoneCourseDao dao;
    @Autowired
    private ActDao actDao;
    @Autowired
    private ActZoneDao actZoneDao;
    @Autowired
    private ActSeckillDao seckillDao;
    @Autowired
    private ActCouponDao couponDao;
    @Autowired
    private IFeignCourse feignCourse;

    /**
     * 活动专区课程列表
     *
     * @param req 活动专区课程分页查询参数
     * @return 活动专区课程分页查询结果
     */
    public Result<Page<ActZoneCourseListRESP>> list(ActZoneCourseListREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ActZone zone = actZoneDao.getById(req.getZoneId());
        if (zone == null) {
            return Result.error("专区id不正确");
        }
        ActZoneCourseExample example = new ActZoneCourseExample();
        Criteria c = example.createCriteria();
        if (req.getZoneId() != null) {
            c.andZoneIdEqualTo(req.getZoneId());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<ActZoneCourse> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ActZoneCourseListRESP> respPage = PageUtil.transform(page, ActZoneCourseListRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }
        //补充专区信息
        if (ActTypeEnum.COUPON.getCode().equals(zone.getActType())) {
            List<ActCoupon> list = couponDao.listByActIdAndZoneId(zone.getActId(), req.getZoneId());
            Map<Long, ActCoupon> map = list.stream().collect(Collectors.toMap(ActCoupon::getCourseId, actCoupon -> actCoupon));
            for (ActZoneCourseListRESP zoneCourse : respPage.getList()) {
                zoneCourse.setCoupon(map.get(zoneCourse.getCourseId()));
            }
        } else {
            List<ActSeckill> list = seckillDao.listByActIdAndZoneId(zone.getActId(), req.getZoneId());
            Map<Long, ActSeckill> map = list.stream().collect(Collectors.toMap(ActSeckill::getCourseId, actSeckill -> actSeckill));
            for (ActZoneCourseListRESP zoneCourse : respPage.getList()) {
                zoneCourse.setSeckill(map.get(zoneCourse.getCourseId()));
            }
        }
        return Result.success(respPage);
    }


    /**
     * 活动专区课程添加
     *
     * @param req 活动专区课程
     * @return 添加结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(ActZoneCourseSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //校验活动和专区
        ActZone zone = actZoneDao.getById(req.getZoneId());
        if (ObjectUtil.isEmpty(zone)) {
            return Result.error("专区不存在");
        }

        Act act = actDao.getById(zone.getActId());
        if (ObjectUtil.isEmpty(zone)) {
            return Result.error("活动不存在");
        }
        //校验课程
        CourseVO course = feignCourse.getById(req.getCourseId());
        if (ObjectUtil.isEmpty(course)) {
            return Result.error("课程Id不正确");
        }
        //价格
        if (course.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) <= 0) {
            return Result.error("课程原价大于0的课程方可以参与活动");
        }
        if (course.getCourseDiscount().compareTo(BigDecimal.valueOf(0)) <= 0) {
            return Result.error("课程会员价大于0的课程方可以参与活动");
        }
        if (req.getActPrice().compareTo(course.getCourseDiscount()) >= 0) {
            return Result.error("价格必须小于会员价");
        }
        if (req.getActPrice().compareTo(BigDecimal.valueOf(0)) <= 0) {
            return Result.error("价格必须大于0");
        }

        // 保存专区课程时，校验时间
        validTimeForSaveCourse(act, zone, req.getCourseId());

        // 保存
        ActZoneCourse record = BeanUtil.copyProperties(req, ActZoneCourse.class);
        record.setActId(zone.getActId());
        record.setActType(zone.getActType());
        record.setCourseCategory(course.getCourseCategory());
        record.setCourseOriginal(course.getCourseOriginal());
        record.setCourseDiscount(course.getCourseDiscount());
        record.setCourseLogo(course.getCourseLogo());
        record.setCourseName(course.getCourseName());
        dao.save(record);

        //保存 附属
        if (ActTypeEnum.COUPON.getCode().equals(record.getActType())) {
            ActCoupon coupon = new ActCoupon();
            coupon.setActId(record.getActId());
            coupon.setZoneId(record.getZoneId());
            coupon.setCourseId(record.getCourseId());
            coupon.setCouponPrice(req.getActPrice());
            coupon.setTotal(req.getTotal());
            couponDao.save(coupon);
        } else if (ActTypeEnum.SECKILL.getCode().equals(record.getActType())) {
            ActSeckill seckill = new ActSeckill();
            seckill.setActId(record.getActId());
            seckill.setZoneId(record.getZoneId());
            seckill.setCourseId(record.getCourseId());
            seckill.setSeckillPrice(req.getActPrice());
            seckill.setIsLimit(req.getIsLimit());
            seckill.setBuyer(req.getBuyer());
            seckillDao.save(seckill);
        }

        return Result.success("添加成功");
    }

    /**
     * 保存专区课程时，校验时间
     * 同一优惠券活动下，不能有相同的课程；
     * 同一秒杀活动下，不同专区可以有相同的课程，但同一专区下不能有相同的课程；
     * 优惠券和秒杀活动，课程只能参加一个
     */
    private void validTimeForSaveCourse(Act act, ActZone zone, Long courseId) {
        //同一活动下检验
        if (ActTypeEnum.COUPON.getCode().equals(act.getActType())) {
            List<ActZoneCourse> list = dao.listByActIdAndZoneIdAndCourseIdAndStatusId(act.getId(), zone.getId(), courseId, null);
            if (!CollectionUtil.isEmpty(list)) {
                ActZone z1 = actZoneDao.getById(list.get(0).getZoneId());
                throw new BaseException("同一优惠券活动下，不能有相同的课程【专区:" + z1.getZoneTitle() + "】");
            }
        } else {
            ActZoneCourse actZoneCourse = dao.getByZoneIdAndCourseId(zone.getId(), courseId);
            if (ObjectUtil.isNotNull(actZoneCourse)) {
                throw new BaseException("秒杀活动同一专区下，不能有相同的课程");
            }

        }
        //不同活动 检验
        //查询活动
        List<ActZoneCourse> otherActs = null;
        if (ActTypeEnum.COUPON.getCode().equals(act.getActType())) {
            otherActs = dao.listByActTypeAndCourseIdAndStatusId(ActTypeEnum.SECKILL.getCode(), courseId, null);
        } else {
            otherActs = dao.listByActTypeAndCourseIdAndStatusId(ActTypeEnum.COUPON.getCode(), courseId, null);
        }
        List<Long> otherActIds = otherActs.stream().map(ActZoneCourse::getActId).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(otherActs)) {
            return;
        }
        List<Act> otherActList = actDao.listByActIdListAndStatusId(otherActIds, null);
        //比对 活动时间不可以有交集
        for (Act otherAct : otherActList) {
            //活动开始时间 介于已存在活动时间期间
            if (act.getBeginTime().compareTo(otherAct.getBeginTime()) >= 0 && act.getBeginTime().compareTo(otherAct.getEndTime()) < 0) {
                throw new BaseException("同时间段内不能参加多个活动：已加入活动【" + otherAct.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(otherAct.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
            //活动结束时间 介于已存在活动时间期间
            if (act.getEndTime().compareTo(otherAct.getBeginTime()) > 0 && act.getEndTime().compareTo(otherAct.getEndTime()) <= 0) {
                throw new BaseException("同时间段内不能参加多个活动：已加入活动【" + otherAct.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(otherAct.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
            //活动开始时间小于 已存在活动开始时间且  活动结束时间大于已存在活动结束时间
            if (act.getBeginTime().compareTo(otherAct.getBeginTime()) <= 0 && act.getEndTime().compareTo(otherAct.getEndTime()) >= 0) {
                throw new BaseException("同时间段内不能参加多个活动：已加入活动【" + otherAct.getActTitle() + ":" + DateUtil.format(act.getBeginTime(), DatePattern.NORM_DATETIME_PATTERN) + "~" + DateUtil.format(otherAct.getEndTime(), DatePattern.NORM_DATETIME_PATTERN) + "】");
            }
        }
    }

    /**
     * 活动专区课程查看
     *
     * @param id 主键ID
     * @return 活动专区课程
     */
    public Result<ActZoneCourseViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActZoneCourseViewRESP.class));
    }


    /**
     * 活动专区课程修改
     *
     * @param req 活动专区课程修改对象
     * @return 修改结果
     */
    public Result<String> edit(ActZoneCourseEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ActZoneCourse zoneCourse = dao.getById(req.getId());
        if (zoneCourse == null) {
            return Result.error("id不正确");
        }
        //核定    价格
        if (req.getActPrice() != null && req.getActPrice().compareTo(zoneCourse.getActPrice()) != 0) {

            if (req.getActPrice().compareTo(zoneCourse.getCourseDiscount()) >= 0) {
                return Result.error("价格必须小于会员价");
            }
            if (req.getActPrice().compareTo(BigDecimal.valueOf(0)) <= 0) {
                return Result.error("价格必须大于0");
            }
            zoneCourse.setActPrice(req.getActPrice());
        }

        //核定    优惠券发放总量，核定秒杀限购人数
        ActSeckill seckill = null;
        ActCoupon coupon = null;
        if (ActTypeEnum.COUPON.getCode().equals(zoneCourse.getActType())) {
            coupon = couponDao.getByZoneIdAndCourseId(zoneCourse.getZoneId(), zoneCourse.getCourseId());
            if (ObjectUtil.isEmpty(coupon)) {
                return Result.error("优惠券信息不正确");
            }
            //发放券数 不能小于 领取券数量
            if (req.getTotal() != null && req.getTotal().intValue() < coupon.getReceiveNum().intValue()) {
                return Result.error("发放券数量【" + req.getTotal() + "】不得小于已领取券数量【" + coupon.getReceiveNum() + "】");
            }
        } else {
            seckill = seckillDao.getByZoneIdAndCourseId(zoneCourse.getZoneId(), zoneCourse.getCourseId());
            if (ObjectUtil.isEmpty(seckill)) {
                return Result.error("秒杀信息不正确");
            }
            //不限购，不做处理
            //有限购，需要保证限购人数 >= 已购买人数
            if (req.getIsLimit() != null && IsLimitEnum.YES.getCode().equals(req.getIsLimit()) && req.getBuyer().intValue() < seckill.getHasBuy().intValue()) {
                return Result.error("限购人数【" + req.getBuyer() + "】不得小于已购买人数【" + seckill.getHasBuy() + "】");
            }
        }

        //更新数据
        zoneCourse.setSort(req.getSort());
        dao.updateById(zoneCourse);
        if (ActTypeEnum.COUPON.getCode().equals(zoneCourse.getActType())) {
            if (null != coupon) {
                coupon.setCouponPrice(req.getActPrice());
                coupon.setTotal(req.getTotal() != null ? req.getTotal() : coupon.getTotal());
                couponDao.updateById(coupon);
            }
        } else {
            if (null != seckill) {
                seckill.setSeckillPrice(req.getActPrice());
                if (req.getIsLimit() != null && IsLimitEnum.YES.getCode().equals(req.getIsLimit())) {
                    seckill.setIsLimit(req.getIsLimit());
                    seckill.setBuyer(req.getBuyer());
                }
                seckillDao.updateById(seckill);
            }
        }
        return Result.success("编辑成功");
    }

    /**
     * 活动专区课程删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        ActZoneCourse zoneCourse = dao.getById(id);
        if (zoneCourse == null) {
            return Result.error("id不正确");
        }
        if (ActTypeEnum.COUPON.getCode().equals(zoneCourse.getActType())) {
            // 已经领取优惠券，则不允许删除优惠券
            ActCoupon coupon = couponDao.getByZoneIdAndCourseId(zoneCourse.getZoneId(), zoneCourse.getCourseId());
            if (coupon == null) {
                return Result.error("优惠券不存在，请联系管理员");
            }
            if (coupon.getReceiveNum().compareTo(Integer.valueOf(0)) > 0) {
                return Result.error("优惠券已被领取，不可删除");
            }
            //删除优惠券附属信息
            couponDao.deleteById(coupon.getId());
        } else {
            // 删除秒杀附属信息，todo 检验
            seckillDao.deleteByZoneIdCourseId(zoneCourse.getZoneId(), zoneCourse.getCourseId());
        }
        //删除 专区课程
        dao.deleteById(id);

        return Result.success("删除成功");
    }

    public Result<String> updateStatus(ActZoneCourseUpadateStatusREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ActZoneCourse zoneCourse = dao.getById(req.getId());
        if (zoneCourse == null) {
            return Result.error("id不正确");
        }
        zoneCourse.setStatusId(req.getStatusId());
        if (dao.updateById(zoneCourse) > 0) {
            return Result.success("修改成功");
        }
        ;
        return Result.error("修改失败");
    }
}
