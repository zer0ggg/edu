package com.roncoo.education.marketing.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActEditREQ", description="活动信息表修改")
public class ActEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键" ,required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "活动标题")
    private String actTitle;

    @ApiModelProperty(value = "活动图片")
    private String actImg;

    @NotNull(message = "活动开始时间不能为空")
    @ApiModelProperty(value = "活动开始时间")
    private Date beginTime;

    @NotNull(message = "活动结束时间不能为空")
    @ApiModelProperty(value = "活动结束时间")
    private Date endTime;

    @ApiModelProperty(value = "活动背景图片")
    private String actBackImg;

    @ApiModelProperty(value = "活动背景颜色")
    private String actBackColor;
}
