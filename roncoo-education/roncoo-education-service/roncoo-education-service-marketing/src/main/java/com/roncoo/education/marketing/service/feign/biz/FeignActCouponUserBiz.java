package com.roncoo.education.marketing.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.feign.qo.ActCouponUserQO;
import com.roncoo.education.marketing.feign.vo.ActCouponUserVO;
import com.roncoo.education.marketing.service.dao.ActCouponUserDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUser;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 优惠券用户关联
 *
 * @author wujing
 */
@Component
public class FeignActCouponUserBiz extends BaseBiz {

    @Autowired
    private ActCouponUserDao dao;

	public Page<ActCouponUserVO> listForPage(ActCouponUserQO qo) {
	    ActCouponUserExample example = new ActCouponUserExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ActCouponUser> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ActCouponUserVO.class);
	}

	public int save(ActCouponUserQO qo) {
		ActCouponUser record = BeanUtil.copyProperties(qo, ActCouponUser.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ActCouponUserVO getById(Long id) {
		ActCouponUser record = dao.getById(id);
		return BeanUtil.copyProperties(record, ActCouponUserVO.class);
	}

	public int updateById(ActCouponUserQO qo) {
		ActCouponUser record = BeanUtil.copyProperties(qo, ActCouponUser.class);
		return dao.updateById(record);
	}

	public int updateByIsUse(ActCouponUserQO actCouponUserQO) {
		return dao.updateByIsUse(actCouponUserQO.getCouponId(), actCouponUserQO.getIsUse());
	}
}
