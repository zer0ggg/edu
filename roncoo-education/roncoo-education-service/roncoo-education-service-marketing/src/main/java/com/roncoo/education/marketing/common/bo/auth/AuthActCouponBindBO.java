package com.roncoo.education.marketing.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户领卷
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthActCouponBindBO", description = "用户领卷")
public class AuthActCouponBindBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "活动ID不能为空")
    @ApiModelProperty(value = "活动ID", required = true)
    private Long actId;

    @NotNull(message = "课程ID不能为空")
    @ApiModelProperty(value = "课程ID", required = true)
    private Long courseId;
}
