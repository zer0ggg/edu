package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.req.ActCouponEditREQ;
import com.roncoo.education.marketing.common.req.ActCouponListREQ;
import com.roncoo.education.marketing.common.req.ActCouponSaveREQ;
import com.roncoo.education.marketing.common.resp.ActCouponListRESP;
import com.roncoo.education.marketing.common.resp.ActCouponViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActCouponBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 优惠券活动 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-优惠券活动")
@RestController
@RequestMapping("/marketing/pc/act/coupon")
public class PcActCouponController {

    @Autowired
    private PcActCouponBiz biz;

    @ApiOperation(value = "优惠券活动列表", notes = "优惠券活动列表")
    @PostMapping(value = "/list")
    public Result<Page<ActCouponListRESP>> list(@RequestBody ActCouponListREQ actCouponListREQ) {
        return biz.list(actCouponListREQ);
    }


    @ApiOperation(value = "优惠券活动添加", notes = "优惠券活动添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActCouponSaveREQ actCouponSaveREQ) {
        return biz.save(actCouponSaveREQ);
    }

    @ApiOperation(value = "优惠券活动查看", notes = "优惠券活动查看")
    @GetMapping(value = "/view")
    public Result<ActCouponViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "优惠券活动修改", notes = "优惠券活动修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActCouponEditREQ actCouponEditREQ) {
        return biz.edit(actCouponEditREQ);
    }

    @ApiOperation(value = "优惠券活动删除", notes = "优惠券活动删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
