package com.roncoo.education.marketing.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneEditREQ", description="活动专区修改")
public class ActZoneEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "专区标题")
    private String zoneTitle;

    @ApiModelProperty(value = "秒杀开始时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "秒杀开始时间字符串,格式HH:mm")
    private String beginTimeStr;

    @ApiModelProperty(value = "秒杀结束时间字符串,格式HH:mm")
    private String endTimeStr;
}
