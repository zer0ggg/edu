package com.roncoo.education.marketing.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.common.req.ActCouponEditREQ;
import com.roncoo.education.marketing.common.req.ActCouponListREQ;
import com.roncoo.education.marketing.common.req.ActCouponSaveREQ;
import com.roncoo.education.marketing.common.resp.ActCouponListRESP;
import com.roncoo.education.marketing.common.resp.ActCouponViewRESP;
import com.roncoo.education.marketing.service.dao.ActCouponDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 优惠券活动
 *
 * @author wujing
 */
@Component
public class PcActCouponBiz extends BaseBiz {

    @Autowired
    private ActCouponDao dao;

    /**
    * 优惠券活动列表
    *
    * @param actCouponListREQ 优惠券活动分页查询参数
    * @return 优惠券活动分页查询结果
    */
    public Result<Page<ActCouponListRESP>> list(ActCouponListREQ actCouponListREQ) {
        ActCouponExample example = new ActCouponExample();
        Criteria c = example.createCriteria();
        Page<ActCoupon> page = dao.listForPage(actCouponListREQ.getPageCurrent(), actCouponListREQ.getPageSize(), example);
        Page<ActCouponListRESP> respPage = PageUtil.transform(page, ActCouponListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 优惠券活动添加
    *
    * @param actCouponSaveREQ 优惠券活动
    * @return 添加结果
    */
    public Result<String> save(ActCouponSaveREQ actCouponSaveREQ) {
        ActCoupon record = BeanUtil.copyProperties(actCouponSaveREQ, ActCoupon.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 优惠券活动查看
    *
    * @param id 主键ID
    * @return 优惠券活动
    */
    public Result<ActCouponViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActCouponViewRESP.class));
    }


    /**
    * 优惠券活动修改
    *
    * @param actCouponEditREQ 优惠券活动修改对象
    * @return 修改结果
    */
    public Result<String> edit(ActCouponEditREQ actCouponEditREQ) {
        ActCoupon record = BeanUtil.copyProperties(actCouponEditREQ, ActCoupon.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 优惠券活动删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
