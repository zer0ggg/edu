package com.roncoo.education.marketing.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.marketing.common.req.ActSeckillEditREQ;
import com.roncoo.education.marketing.common.req.ActSeckillListREQ;
import com.roncoo.education.marketing.common.req.ActSeckillSaveREQ;
import com.roncoo.education.marketing.common.resp.ActSeckillListRESP;
import com.roncoo.education.marketing.common.resp.ActSeckillViewRESP;
import com.roncoo.education.marketing.service.dao.ActSeckillDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckillExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程秒杀活动
 *
 * @author wujing
 */
@Component
public class PcActSeckillBiz extends BaseBiz {

    @Autowired
    private ActSeckillDao dao;

    @Autowired
    private IFeignCourse feignCourse;

    /**
    * 课程秒杀活动列表
    *
    * @param actSeckillListREQ 课程秒杀活动分页查询参数
    * @return 课程秒杀活动分页查询结果
    */
    public Result<Page<ActSeckillListRESP>> list(ActSeckillListREQ actSeckillListREQ) {
        ActSeckillExample example = new ActSeckillExample();
        Criteria c = example.createCriteria();
        // 课程ID
        if (ObjectUtil.isNotEmpty(actSeckillListREQ.getCourseId())) {
            c.andCourseIdEqualTo(actSeckillListREQ.getCourseId());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<ActSeckill> page = dao.listForPage(actSeckillListREQ.getPageCurrent(), actSeckillListREQ.getPageSize(), example);
        Page<ActSeckillListRESP> respPage = PageUtil.transform(page, ActSeckillListRESP.class);
        for (ActSeckillListRESP actSeckillListRESP : respPage.getList()) {
            actSeckillListRESP.setCourseInfo(feignCourse.getById(actSeckillListRESP.getCourseId()));
        }
        return Result.success(respPage);
    }


    /**
    * 课程秒杀活动添加
    *
    * @param actSeckillSaveREQ 课程秒杀活动
    * @return 添加结果
    */
    public Result<String> save(ActSeckillSaveREQ actSeckillSaveREQ) {
        ActSeckill record = BeanUtil.copyProperties(actSeckillSaveREQ, ActSeckill.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课程秒杀活动查看
    *
    * @param id 主键ID
    * @return 课程秒杀活动
    */
    public Result<ActSeckillViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActSeckillViewRESP.class));
    }


    /**
    * 课程秒杀活动修改
    *
    * @param actSeckillEditREQ 课程秒杀活动修改对象
    * @return 修改结果
    */
    public Result<String> edit(ActSeckillEditREQ actSeckillEditREQ) {
        ActSeckill record = BeanUtil.copyProperties(actSeckillEditREQ, ActSeckill.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课程秒杀活动删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
