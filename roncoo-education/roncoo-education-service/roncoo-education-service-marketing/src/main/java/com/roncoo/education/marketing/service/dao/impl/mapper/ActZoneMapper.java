package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZone;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActZoneMapper {
    int countByExample(ActZoneExample example);

    int deleteByExample(ActZoneExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ActZone record);

    int insertSelective(ActZone record);

    List<ActZone> selectByExample(ActZoneExample example);

    ActZone selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ActZone record, @Param("example") ActZoneExample example);

    int updateByExample(@Param("record") ActZone record, @Param("example") ActZoneExample example);

    int updateByPrimaryKeySelective(ActZone record);

    int updateByPrimaryKey(ActZone record);
}
