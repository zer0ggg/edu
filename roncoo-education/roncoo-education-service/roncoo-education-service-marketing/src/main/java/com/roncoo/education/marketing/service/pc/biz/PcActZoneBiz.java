package com.roncoo.education.marketing.service.pc.biz;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.marketing.common.req.*;
import com.roncoo.education.marketing.common.resp.ActZoneListRESP;
import com.roncoo.education.marketing.common.resp.ActZoneViewRESP;
import com.roncoo.education.marketing.service.dao.ActDao;
import com.roncoo.education.marketing.service.dao.ActSeckillDao;
import com.roncoo.education.marketing.service.dao.ActZoneCourseDao;
import com.roncoo.education.marketing.service.dao.ActZoneDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample.Criteria;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 活动专区
 *
 * @author wujing
 */
@Component
public class PcActZoneBiz extends BaseBiz {

    @Autowired
    private ActZoneDao dao;
    @Autowired
    private ActDao actDao;
    @Autowired
    private ActZoneCourseDao actZoneCourseDao;
    @Autowired
    private ActSeckillDao actSeckillDao;

    /**
     * 活动专区列表
     *
     * @param req 活动专区分页查询参数
     * @return 活动专区分页查询结果
     */
    public Result<Page<ActZoneListRESP>> list(ActZoneListREQ req) {
        ActZoneExample example = new ActZoneExample();
        Criteria c = example.createCriteria();
        if (StrUtil.isNotEmpty(req.getZoneTitle())) {
            c.andZoneTitleLike(PageUtil.like(req.getZoneTitle()));
        }
        if (req.getActId() != null) {
            c.andActIdEqualTo(req.getActId());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<ActZone> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ActZoneListRESP> respPage = PageUtil.transform(page, ActZoneListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 活动专区添加
     *
     * @param req 活动专区
     * @return 添加结果
     */
    public Result<String> save(ActZoneSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //检验
        Act act = actDao.getById(req.getActId());
        if (ObjectUtil.isNull(act)) {
            throw new BaseException("找不到活动信息");
        }
        ActZone record = BeanUtil.copyProperties(req, ActZone.class);
        record.setActType(act.getActType());

        //秒杀专区，同一活动内专区时间不可重复
        if (ActTypeEnum.SECKILL.getCode().equals(act.getActType())) {
            if (req.getBeginTime() == null || req.getEndTime() == null || req.getBeginTime().after(req.getEndTime())) {
                return Result.error("秒杀时间段设置错误");
            }
            List<ActZone> zoneList = dao.listByActId(req.getActId());
            for (ActZone zone : zoneList) {
                //开始时间 介于已存在的时间期间
                if (req.getBeginTime().compareTo(zone.getBeginTime()) >= 0 && req.getBeginTime().compareTo(zone.getEndTime()) < 0) {
                    return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
                }
                //活动结束时间 介于已存在活动时间期间
                if (req.getEndTime().compareTo(zone.getBeginTime()) > 0 && req.getEndTime().compareTo(zone.getEndTime()) <= 0) {
                    return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
                }
            }
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 活动专区查看
     *
     * @param id 主键ID
     * @return 活动专区
     */
    public Result<ActZoneViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ActZoneViewRESP.class));
    }


    /**
     * 活动专区修改
     *
     * @param req 活动专区修改对象
     * @return 修改结果
     */
    public Result<String> edit(ActZoneEditREQ req) {
        ActZone actZone = dao.getById(req.getId());
        if (actZone == null) {
            return Result.error("没有查询到专区信息");
        }
        //秒杀专区，同一活动内专区时间不可重复
        if (ActTypeEnum.SECKILL.getCode().equals(actZone.getActType())) {
            if (req.getBeginTime() == null || req.getEndTime() == null || req.getBeginTime().after(req.getEndTime())) {
                return Result.error("秒杀时间段设置错误");
            }
            List<ActZone> zoneList = dao.listByActId(actZone.getActId());
            for (ActZone zone : zoneList) {
                //跳过对自己记录的时间比对
                if (zone.getId().equals(req.getId())) {
                    continue;
                }
                //开始时间 介于已存在的时间期间
                if (req.getBeginTime().compareTo(zone.getBeginTime()) >= 0 && req.getBeginTime().compareTo(zone.getEndTime()) < 0) {
                    return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
                }
                //活动结束时间 介于已存在活动时间期间
                if (req.getEndTime().compareTo(zone.getBeginTime()) > 0 && req.getEndTime().compareTo(zone.getEndTime()) <= 0) {
                    return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
                }
            }
        }
        //编辑
        ActZone record = BeanUtil.copyProperties(req, ActZone.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 活动专区删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        ActZone record = dao.getById(id);
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("找不到活动专区信息");
        }
        List<ActZoneCourse> list = actZoneCourseDao.listByZoneId(id);
        if (CollectionUtils.isNotEmpty(list)) {
            throw new BaseException("专区下存在可用课程，请先删除课程");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 修改状态
     *
     * @param actZoneUpdateStatusREQ 状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(ActZoneUpdateStatusREQ actZoneUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(actZoneUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(actZoneUpdateStatusREQ.getId()))) {
            return Result.error("渠道不存在");
        }
        ActZone record = BeanUtil.copyProperties(actZoneUpdateStatusREQ, ActZone.class);
        if (dao.updateById(record) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> copy(ActZoneCopyREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }

        ActZone record = dao.getById(req.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("找不到活动专区信息");
        }
        List<ActZone> zoneList = dao.listByActId(record.getActId());
        for (ActZone zone : zoneList) {
            //开始时间 介于已存在的时间期间
            if (req.getBeginTime().compareTo(zone.getBeginTime()) >= 0 && req.getBeginTime().compareTo(zone.getEndTime()) < 0) {
                return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
            }
            //活动结束时间 介于已存在活动时间期间
            if (req.getEndTime().compareTo(zone.getBeginTime()) > 0 && req.getEndTime().compareTo(zone.getEndTime()) <= 0) {
                return Result.error("秒杀专区时间不能重叠：秒杀专区【" + zone.getZoneTitle() + ":" + DateUtil.format(zone.getBeginTime(), DatePattern.NORM_TIME_PATTERN) + "~" + DateUtil.format(zone.getEndTime(), DatePattern.NORM_TIME_PATTERN) + "】");
            }
        }

        List<ActZoneCourse> sourceZoneCourseList = actZoneCourseDao.listByActIdAndZoneIdAndCourseIdAndStatusId(null, record.getId(), null, null);
        List<ActSeckill> sourceSeckillList = actSeckillDao.listByActIdAndZoneId(null, record.getId());

        ActZone zone = BeanUtil.copyProperties(req, ActZone.class);
        zone.setActId(record.getActId());
        zone.setActType(record.getActType());
        zone.setId(null);
        dao.save(zone);
        for (ActZoneCourse sourceCourse : sourceZoneCourseList) {
            ActZoneCourse zoneCourse = BeanUtil.copyProperties(sourceCourse, ActZoneCourse.class);
            zoneCourse.setActId(record.getActId());
            zoneCourse.setZoneId(zone.getId());
            zoneCourse.setGmtCreate(null);
            zoneCourse.setGmtModified(null);
            zoneCourse.setId(null);
            actZoneCourseDao.save(zoneCourse);
        }

        List<ActSeckill> matchSeckillList = sourceSeckillList.stream().filter(x -> x.getZoneId().equals(record.getId())).collect(Collectors.toList());
        for (ActSeckill sourceSeckill : matchSeckillList) {
            ActSeckill seckill = BeanUtil.copyProperties(sourceSeckill, ActSeckill.class);
            seckill.setActId(record.getActId());
            seckill.setZoneId(zone.getId());
            seckill.setId(null);
            seckill.setGmtCreate(null);
            seckill.setGmtModified(null);
            seckill.setHasBuy(0);
            actSeckillDao.save(seckill);
        }
        return Result.success("操作成功");

    }
}
