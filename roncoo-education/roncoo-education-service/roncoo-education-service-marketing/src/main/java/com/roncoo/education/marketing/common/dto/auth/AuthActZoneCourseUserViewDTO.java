package com.roncoo.education.marketing.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 活动专区课程关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthActZoneCourseUserViewDTO", description="活动专区课程关联表")
public class AuthActZoneCourseUserViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 活动类型(1优惠券,2秒杀)
	 */
	@ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
	private Integer actType;
	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long actId;
	/**
	 * 专区ID
	 */
	@ApiModelProperty(value = "专区ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long zoneId;
	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;

	/**
	 * 原价
	 */
	@ApiModelProperty(value = "原价")
	private BigDecimal courseOriginal;
	/**
	 * 优惠价格
	 */
	@ApiModelProperty(value = "会员价格")
	private BigDecimal courseDiscount;
	/**
	 * 活动价格
	 */
	@ApiModelProperty(value = "活动价格")
	private BigDecimal actPrice;

	// 秒杀活动需要增加时间限制

	/**
	 * 是否秒杀开始(1: 未开始; 2: 活动中; 3:结束)
	 */
	@ApiModelProperty(value = "是否秒杀开始(1: 未开始; 2: 活动中; 3:结束)")
	private Integer isStart;
	/**
	 * 秒杀开始时间
	 */
	@ApiModelProperty(value = "秒杀开始时间")
	@JsonFormat(pattern = "HH:mm:ss")
	private Date beginTime;
	/**
	 * 秒杀结束时间
	 */
	@ApiModelProperty(value = "秒杀结束时间")
	@JsonFormat(pattern = "HH:mm:ss")
	private Date endTime;

}
