package com.roncoo.education.marketing.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.IsLimitEnum;
import com.roncoo.education.common.core.enums.IsUseEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.marketing.feign.qo.ActOrderPayBO;
import com.roncoo.education.marketing.feign.qo.ActQO;
import com.roncoo.education.marketing.feign.vo.ActOrderVO;
import com.roncoo.education.marketing.feign.vo.ActVO;
import com.roncoo.education.marketing.service.dao.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 活动信息表
 *
 * @author wujing
 */
@Component
public class FeignActBiz extends BaseBiz {

    @Autowired
    private ActDao dao;
    @Autowired
    private ActZoneDao actZoneDao;
    @Autowired
    private ActZoneCourseDao actZoneCourseDao;
    @Autowired
    private ActCouponDao actCouponDao;
    @Autowired
    private ActCouponUserDao actCouponUserDao;
    @Autowired
    private ActSeckillDao actSeckillDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<ActVO> listForPage(ActQO qo) {
        ActExample example = new ActExample();
        Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<Act> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, ActVO.class);
    }

    public int save(ActQO qo) {
        Act record = BeanUtil.copyProperties(qo, Act.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public ActVO getById(Long id) {
        Act record = dao.getById(id);
        return BeanUtil.copyProperties(record, ActVO.class);
    }

    public int updateById(ActQO qo) {
        Act record = BeanUtil.copyProperties(qo, Act.class);
        return dao.updateById(record);
    }

    /**
     * 活动下单
     *
     * @param actOrderPayBO
     * @return
     */
    public ActOrderVO order(ActOrderPayBO actOrderPayBO) {
        // 核实用户编号
        UserExtVO userExt = feignUserExt.getByUserNo(actOrderPayBO.getUserNo());
        if (ObjectUtil.isNull(userExt)) {
            throw new BaseException("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userExt.getStatusId())) {
            throw new BaseException("用户已禁用");
        }
        // 校验活动基本信息
        Act act = dao.getByIdAndStatusIdAndTime(actOrderPayBO.getActId(), StatusIdEnum.YES.getCode(), new Date());
        if (ObjectUtil.isNull(act)) {
            throw new BaseException("活动不存在");
        }
        ActOrderVO vo = new ActOrderVO();
        vo.setActType(act.getActType());
        if (ActTypeEnum.COUPON.getCode().equals(act.getActType())) {
            ActCoupon actCoupon = actCouponDao.getByActIdAndZondIdAndCourseIdAndStatusId(actOrderPayBO.getActId(), null, actOrderPayBO.getProductId(), StatusIdEnum.YES.getCode());
            if (ObjectUtil.isNull(actCoupon)) {
                logger.debug("该课程尚未参加活动");
                return null;
            }
            // 判断课程是否存在
            ActZoneCourse actZoneCourse = actZoneCourseDao.getByZoneIdAndCourseId(actCoupon.getZoneId(), actOrderPayBO.getProductId());
            if (ObjectUtil.isNull(actZoneCourse)) {
                logger.debug("该课程尚未参加活动");
                return null;
            }
            // 判断专区是否存在
            ActZone actZone = actZoneDao.getById(actZoneCourse.getZoneId());
            if (ObjectUtil.isNull(actZone)) {
                logger.debug("专区不存在");
                return null;
            }
            ActCouponUser actCouponUser = actCouponUserDao.getByUserNoAndCouponIdAndTimeAndIsUse(actOrderPayBO.getUserNo(), actCoupon.getId(), new Date(), IsUseEnum.NO.getCode());
            if (ObjectUtil.isNull(actCouponUser)) {
                logger.debug("用户未领取优惠券");
                return null;
            }

            vo.setActTypeId(actCoupon.getId());

            BigDecimal pricePaid = actZoneCourse.getCourseOriginal().subtract(actCoupon.getCouponPrice()).setScale(2, BigDecimal.ROUND_HALF_UP);
            if (VipCheckUtil.checkIsVip(userExt.getIsVip(), userExt.getExpireTime())) {
                // 如果是会员且在有效期，// 实付金额 = 优惠券优惠金额-(课程的原价-课程会员价)
                pricePaid = pricePaid.subtract(actZoneCourse.getCourseOriginal().subtract(actZoneCourse.getCourseDiscount()));
            }
            vo.setActPricePaid(pricePaid);
            // 锁定优惠卷,用户支付结果，确认失败前，一直是不可用状态
            lockCouponUserById(actCouponUser.getId());
            return vo;
        } else {
            // 秒杀活动
            //创建日期转换对
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date dt1 = new Date();
            try {
                //将字符串转换为date类型
                dt1 = df.parse(DateUtil.getTime(new Date()));
            } catch (ParseException e) {
                logger.warn("获取当前时间！", e);
            }
            // 根据活动ID获取可用正在秒杀的专区
            ActZone actZone = actZoneDao.listByActIdAndStatusIdAndDate(act.getId(), StatusIdEnum.YES.getCode(), dt1);
            if (ObjectUtil.isNull(actZone)) {
                logger.debug("活动已经结束");
                return null;
            }
            // 判断课程是否存在
            ActZoneCourse actZoneCourse = actZoneCourseDao.getByZoneIdAndCourseId(actZone.getId(), actOrderPayBO.getProductId());
            if (ObjectUtil.isNull(actZoneCourse)) {
                logger.debug("该课程尚未参加活动");
                return null;
            }

            // 加锁
            // 超时时间 10s
            Long time = System.currentTimeMillis() + 10 * 1000;
            String stringTime = time.toString();
            if (!lock(actZone.getId().toString() + actOrderPayBO.getProductId().toString(), stringTime)) {
                logger.error("[Redis分布式锁] 获取失败");
                throw new BaseException("很抱歉，人太多了，请稍后再试");
            }

            ActSeckill seckill = actSeckillDao.getByZoneIdAndCourseIdAndStatusId(actZone.getId(), actOrderPayBO.getProductId(), StatusIdEnum.YES.getCode());
            if (ObjectUtil.isNull(seckill)) {
                return null;
            }

            vo.setActPricePaid(seckill.getSeckillPrice());
            vo.setActTypeId(seckill.getId());

            if (IsLimitEnum.NO.getCode().equals(seckill.getIsLimit()) || seckill.getBuyer() == null || seckill.getBuyer() == 0) {
                // 解锁
                unlock(seckill.getId().toString() + actOrderPayBO.getProductId().toString(), String.valueOf(time));
                // 不限购
                return vo;
            }
            // 限购
            if (seckill.getBuyer() <= seckill.getHasBuy()) {
                // 限购人数小于已购买人数，原价购买
                // 解锁
                unlock(seckill.getId().toString() + actOrderPayBO.getProductId().toString(), String.valueOf(time));
                return null;
            }
            // 更新活动课程库存
            seckill.setHasBuy(seckill.getHasBuy() + 1);
            if (actSeckillDao.updateById(seckill) < 1) {
                logger.error("更新库存失败");
                throw new BaseException("请求繁忙");
            }

            // 解锁
            unlock(seckill.getId().toString() + actOrderPayBO.getProductId().toString(), String.valueOf(time));
            return vo;
        }
    }

    /**
     * 使用使用消费卷后注销(锁定)优惠卷的使用性
     *
     * @return
     * @author:
     */
    private int lockCouponUserById(Long id) {
        int result;
        if (ObjectUtil.isNull(id)) {
            throw new BaseException("id不能为空");
        }
        // 修改优惠卷记录
        ActCouponUser actCouponUser = actCouponUserDao.getById(id);
        if (ObjectUtil.isNull(actCouponUser)) {
            throw new BaseException("优惠劵不存在");
        }
        actCouponUser.setIsUse(IsUseEnum.YES.getCode());
        result = actCouponUserDao.updateById(actCouponUser);
        if (result == 0) {
            throw new BaseException("锁定优惠卷异常");
        }
        return result;
    }

    /**
     * 解锁
     *
     * @param key
     * @param value
     * @author wuyun
     */
    public void unlock(String key, String value) {
        try {
            String currentValue = myRedisTemplate.get(key);
            if (!StringUtils.isEmpty(currentValue) && currentValue.equals(value)) {
                // 删除key
                myRedisTemplate.delete(key);
            }
        } catch (Exception e) {
            logger.error("[Redis分布式锁] 解锁出现异常了！", e);
        }
    }


    /**
     * 加锁
     *
     * @param key   id - 商品的唯一标志
     * @param value 当前时间+超时时间 也就是时间戳
     * @return 处理结果
     * @author wuyun
     */
    public boolean lock(String key, String value) {
        // 对应setnx命令
        if (myRedisTemplate.setIfAbsent(key, value)) {
            // 可以成功设置,也就是key不存在
            return true;
        }
        // 判断锁超时 - 防止原来的操作异常，没有运行解锁操作 防止死锁
        String currentValue = myRedisTemplate.get(key);

        // 如果锁过期
        // currentValue不为空且小于当前时间
        if (!StringUtils.isEmpty(currentValue) && Long.parseLong(currentValue) < System.currentTimeMillis()) {
            // 获取上一个锁的时间value
            // 对应getAndSet，如果key存在
            String oldValue = myRedisTemplate.getAndSet(key, value);

            // 假设两个线程同时进来这里，因为key被占用了，而且锁过期了。获取的值currentValue=A(get取的旧的值肯定是一样的),两个线程的value都是B,key都是K.锁时间已经过期了。
            // 而这里面的getAndSet一次只会一个执行，也就是一个执行之后，上一个的value已经变成了B。只有一个线程获取的上一个值会是A，另一个线程拿到的值是B。
            // oldValue不为空且oldValue等于currentValue，也就是校验是不是上个对应的商品时间戳，也是防止并发
            return !StringUtils.isEmpty(oldValue) && oldValue.equals(currentValue);
        }
        return false;
    }
}
