package com.roncoo.education.marketing.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActZoneDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActZoneMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZone;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * 活动专区 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActZoneDaoImpl implements ActZoneDao {

    @Autowired
    private ActZoneMapper mapper;

    @Override
    public int save(ActZone record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ActZone record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ActZone getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ActZone> listForPage(int pageCurrent, int pageSize, ActZoneExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ActZone> listByActId(Long actId) {
        ActZoneExample example = new ActZoneExample();
        Criteria criteria = example.createCriteria();
        criteria.andActIdEqualTo(actId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZone> listByActIdAndStatusId(Long actId, Integer statusId) {
        ActZoneExample example = new ActZoneExample();
        Criteria criteria = example.createCriteria();
        criteria.andActIdEqualTo(actId);
        criteria.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public ActZone listByActIdAndStatusIdAndDate(Long actId, Integer statusId, Date date) {
        ActZoneExample example = new ActZoneExample();
        Criteria c = example.createCriteria();
        c.andActIdEqualTo(actId);
        c.andStatusIdEqualTo(statusId);
        c.andBeginTimeLessThanOrEqualTo(date);
        c.andEndTimeGreaterThanOrEqualTo(date);
        List<ActZone> list = this.mapper.selectByExample(example);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public ActZone getByIdAndStatusIdAndTime(Long zoneId, Integer statusId, Date time) {
        ActZoneExample example = new ActZoneExample();
        Criteria c = example.createCriteria();
        c.andIdEqualTo(zoneId);
        c.andStatusIdEqualTo(statusId);
        c.andBeginTimeLessThanOrEqualTo(time);
        c.andEndTimeGreaterThanOrEqualTo(time);
        List<ActZone> list = this.mapper.selectByExample(example);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }


}
