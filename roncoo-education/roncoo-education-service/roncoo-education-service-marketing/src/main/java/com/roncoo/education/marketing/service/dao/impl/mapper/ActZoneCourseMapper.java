package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourse;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActZoneCourseMapper {
    int countByExample(ActZoneCourseExample example);

    int deleteByExample(ActZoneCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ActZoneCourse record);

    int insertSelective(ActZoneCourse record);

    List<ActZoneCourse> selectByExample(ActZoneCourseExample example);

    ActZoneCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ActZoneCourse record, @Param("example") ActZoneCourseExample example);

    int updateByExample(@Param("record") ActZoneCourse record, @Param("example") ActZoneCourseExample example);

    int updateByPrimaryKeySelective(ActZoneCourse record);

    int updateByPrimaryKey(ActZoneCourse record);
}
