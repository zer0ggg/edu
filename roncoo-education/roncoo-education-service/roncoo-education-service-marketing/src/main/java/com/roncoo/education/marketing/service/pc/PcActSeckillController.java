package com.roncoo.education.marketing.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.marketing.common.req.ActSeckillEditREQ;
import com.roncoo.education.marketing.common.req.ActSeckillListREQ;
import com.roncoo.education.marketing.common.req.ActSeckillSaveREQ;
import com.roncoo.education.marketing.common.resp.ActSeckillListRESP;
import com.roncoo.education.marketing.common.resp.ActSeckillViewRESP;
import com.roncoo.education.marketing.service.pc.biz.PcActSeckillBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程秒杀活动 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-课程秒杀活动")
@RestController
@RequestMapping("/marketing/pc/act/seckill")
public class PcActSeckillController {

    @Autowired
    private PcActSeckillBiz biz;

    @ApiOperation(value = "课程秒杀活动列表", notes = "课程秒杀活动列表")
    @PostMapping(value = "/list")
    public Result<Page<ActSeckillListRESP>> list(@RequestBody ActSeckillListREQ actSeckillListREQ) {
        return biz.list(actSeckillListREQ);
    }

    @ApiOperation(value = "课程秒杀活动添加", notes = "课程秒杀活动添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ActSeckillSaveREQ actSeckillSaveREQ) {
        return biz.save(actSeckillSaveREQ);
    }

    @ApiOperation(value = "课程秒杀活动查看", notes = "课程秒杀活动查看")
    @GetMapping(value = "/view")
    public Result<ActSeckillViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程秒杀活动修改", notes = "课程秒杀活动修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ActSeckillEditREQ actSeckillEditREQ) {
        return biz.edit(actSeckillEditREQ);
    }

    @ApiOperation(value = "课程秒杀活动删除", notes = "课程秒杀活动删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
