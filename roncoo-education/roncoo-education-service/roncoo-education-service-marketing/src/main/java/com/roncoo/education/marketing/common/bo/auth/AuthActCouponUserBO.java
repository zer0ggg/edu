package com.roncoo.education.marketing.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 优惠券用户关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthActCouponUserBO", description = "用户领卷")
public class AuthActCouponUserBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "优惠券状态不能为空")
    @ApiModelProperty(value = "优惠券状态(1[全部,当前可用,已使用],3[已过期])", required = true)
    private Integer statusId;

    @ApiModelProperty(value = "是否使用(0当前可用,1已使用)")
    private Integer isUse;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize = 20;
}
