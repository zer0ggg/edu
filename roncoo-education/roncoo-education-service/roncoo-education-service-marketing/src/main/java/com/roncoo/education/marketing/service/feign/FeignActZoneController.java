package com.roncoo.education.marketing.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.marketing.feign.interfaces.IFeignActZone;
import com.roncoo.education.marketing.feign.qo.ActZoneQO;
import com.roncoo.education.marketing.feign.vo.ActZoneVO;
import com.roncoo.education.marketing.service.feign.biz.FeignActZoneBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 活动专区
 *
 * @author wujing
 * @date 2020-06-23
 */
@RestController
public class FeignActZoneController extends BaseController implements IFeignActZone{

    @Autowired
    private FeignActZoneBiz biz;

	@Override
	public Page<ActZoneVO> listForPage(@RequestBody ActZoneQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ActZoneQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ActZoneQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ActZoneVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
