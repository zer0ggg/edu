package com.roncoo.education.marketing.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.marketing.feign.qo.ActZoneCourseQO;
import com.roncoo.education.marketing.feign.vo.ActZoneCourseVO;
import com.roncoo.education.marketing.service.dao.ActCouponDao;
import com.roncoo.education.marketing.service.dao.ActSeckillDao;
import com.roncoo.education.marketing.service.dao.ActZoneCourseDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActSeckill;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourse;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 活动专区课程
 *
 * @author wujing
 */
@Component
public class FeignActZoneCourseBiz extends BaseBiz {

    @Autowired
    private ActZoneCourseDao dao;

    @Autowired
    private ActCouponDao actCouponDao;

    @Autowired
    private ActSeckillDao actSeckillDao;


    public Page<ActZoneCourseVO> listForPage(ActZoneCourseQO qo) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<ActZoneCourse> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, ActZoneCourseVO.class);
    }

    public int save(ActZoneCourseQO qo) {
        ActZoneCourse record = BeanUtil.copyProperties(qo, ActZoneCourse.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public ActZoneCourseVO getById(Long id) {
        ActZoneCourse record = dao.getById(id);
        return BeanUtil.copyProperties(record, ActZoneCourseVO.class);
    }

    public int updateById(ActZoneCourseQO qo) {
        ActZoneCourse record = BeanUtil.copyProperties(qo, ActZoneCourse.class);
        return dao.updateById(record);
    }

    public int updateByOrder(ActZoneCourseQO qo) {
        if (qo.getActTypeId() != null && qo.getActType() != null) {
            if (ActTypeEnum.SECKILL.getCode().equals(qo.getActType())) {
                ActSeckill actSeckill = actSeckillDao.getById(qo.getActTypeId());
                actSeckill.setHasBuy(actSeckill.getHasBuy() + 1);
                return actSeckillDao.updateById(actSeckill);
            } else {
                ActCoupon actCoupon = actCouponDao.getById(qo.getActTypeId());
                actCoupon.setUsedNum(actCoupon.getUsedNum() + 1);
                return actCouponDao.updateById(actCoupon);
            }
        }
        return 1;
    }
}
