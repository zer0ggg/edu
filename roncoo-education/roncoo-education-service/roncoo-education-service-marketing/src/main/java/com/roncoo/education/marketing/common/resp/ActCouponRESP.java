package com.roncoo.education.marketing.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 优惠券活动
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActCouponRESP", description="优惠券活动")
public class ActCouponRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "发放卷总量")
    private Integer total;

    @ApiModelProperty(value = "已使用数量")
    private Integer usedNum;

    @ApiModelProperty(value = "领取数量")
    private Integer receiveNum;

    @ApiModelProperty(value = "领取限制（0不限制，其他代表张数）")
    private Integer limitReceive;

    @ApiModelProperty(value = "课程Id")
    private Long courseId;

    @ApiModelProperty(value = "优惠价格")
    private BigDecimal couponPrice;

    @ApiModelProperty(value = "活动Id")
    private Long actId;
}
