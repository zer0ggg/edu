package com.roncoo.education.marketing.service.dao.impl.mapper;

import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCoupon;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActCouponExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ActCouponMapper {
    int countByExample(ActCouponExample example);

    int deleteByExample(ActCouponExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ActCoupon record);

    int insertSelective(ActCoupon record);

    List<ActCoupon> selectByExample(ActCouponExample example);

    ActCoupon selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ActCoupon record, @Param("example") ActCouponExample example);

    int updateByExample(@Param("record") ActCoupon record, @Param("example") ActCouponExample example);

    int updateByPrimaryKeySelective(ActCoupon record);

    int updateByPrimaryKey(ActCoupon record);
}
