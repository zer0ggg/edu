package com.roncoo.education.marketing.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动信息表
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActREQ", description="活动信息表")
public class ActREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "活动标题")
    private String actTitle;

    @ApiModelProperty(value = "活动图片")
    private String actImg;

    @ApiModelProperty(value = "活动开始时间")
    private Date beginTime;

    @ApiModelProperty(value = "活动结束时间")
    private Date endTime;
}
