package com.roncoo.education.marketing.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 活动专区
 * </p>
 *
 * @author wujing
 * @date 2020-06-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneDTO", description="活动专区")
public class ActZoneListDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "活动Id")
    private Long actId;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "专区标题")
    private String zoneTitle;

    @ApiModelProperty(value = "秒杀开始时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "专区课程集合")
    private List<ActZoneCourseListDTO> aoneCourseList = new ArrayList<>();
}
