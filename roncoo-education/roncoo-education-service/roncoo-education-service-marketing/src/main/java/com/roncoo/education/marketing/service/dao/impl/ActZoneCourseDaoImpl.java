package com.roncoo.education.marketing.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.marketing.service.dao.ActZoneCourseDao;
import com.roncoo.education.marketing.service.dao.impl.mapper.ActZoneCourseMapper;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourse;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.ActZoneCourseExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 活动专区课程 服务实现类
 *
 * @author wujing
 * @date 2020-06-23
 */
@Repository
public class ActZoneCourseDaoImpl implements ActZoneCourseDao {

    @Autowired
    private ActZoneCourseMapper mapper;

    @Override
    public int save(ActZoneCourse record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ActZoneCourse record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ActZoneCourse getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public ActZoneCourse getByZoneIdAndCourseId(Long zoneId, Long courseId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(courseId);
        List<ActZoneCourse> list = this.mapper.selectByExample(example);
        if(!CollectionUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    @Override
    public Page<ActZoneCourse> listForPage(int pageCurrent, int pageSize, ActZoneCourseExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ActZoneCourse> listByZoneId(Long zoneId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZoneCourse> listByCourseId(Long courseId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(courseId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZoneCourse> listByCourseIdAndStatusId(Long courseId, Integer statusId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(courseId);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZoneCourse> listByActIdAndZoneIdAndCourseIdAndStatusId(Long actId, Long zoneId, Long courseId,Integer statusId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        if(actId != null){
            c.andActIdEqualTo(actId);
        }
        if(zoneId != null){
            c.andZoneIdEqualTo(zoneId);
        }
        if(courseId != null){
            c.andCourseIdEqualTo(courseId);
        }
        if(statusId != null){
            c.andStatusIdEqualTo(statusId);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZoneCourse> listByActTypeAndCourseIdAndStatusId(Integer actType, Long courseId, Integer statusId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andActTypeEqualTo(actType);
        c.andCourseIdEqualTo(courseId);
        if(statusId != null){
            c.andStatusIdEqualTo(statusId);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ActZoneCourse> listByZoneIdAndStatusId(Long zoneId, Integer statusId) {
        ActZoneCourseExample example = new ActZoneCourseExample();
        ActZoneCourseExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }


}
