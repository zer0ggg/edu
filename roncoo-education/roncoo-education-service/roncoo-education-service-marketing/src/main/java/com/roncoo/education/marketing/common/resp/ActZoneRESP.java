package com.roncoo.education.marketing.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 活动专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ActZoneRESP", description="活动专区")
public class ActZoneRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "活动Id")
    private Long actId;

    @ApiModelProperty(value = "活动类型(1优惠券,2秒杀)")
    private Integer actType;

    @ApiModelProperty(value = "专区标题")
    private String zoneTitle;

    @ApiModelProperty(value = "秒杀开始时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "秒杀结束时间")
    @JsonFormat(pattern = "HH:mm:ss")
    private Date endTime;
}
