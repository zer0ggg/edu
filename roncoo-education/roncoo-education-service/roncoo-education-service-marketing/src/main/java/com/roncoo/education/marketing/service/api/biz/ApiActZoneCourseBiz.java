package com.roncoo.education.marketing.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ActTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.marketing.common.bo.ActZoneCourseViewBO;
import com.roncoo.education.marketing.common.dto.ActCouponDTO;
import com.roncoo.education.marketing.common.dto.ActSeckillDTO;
import com.roncoo.education.marketing.common.dto.ActZoneCourseViewDTO;
import com.roncoo.education.marketing.service.dao.*;
import com.roncoo.education.marketing.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 活动专区课程
 *
 * @author wujing
 */
@Component
public class ApiActZoneCourseBiz extends BaseBiz {

    @Autowired
    private ActZoneCourseDao dao;
    @Autowired
    private ActDao actDao;
    @Autowired
    private ActCouponDao actCouponDao;
    @Autowired
    private ActZoneDao actZoneDao;

    @Autowired
    private ActSeckillDao actSeckillDao;

    public Result<ActZoneCourseViewDTO> view(ActZoneCourseViewBO bo) {
        ActZoneCourseViewDTO dto = new ActZoneCourseViewDTO();
        List<ActZoneCourse> actZoneCourseList = dao.listByCourseIdAndStatusId(bo.getCourseId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(actZoneCourseList)) {
            return Result.success(dto);
        }
        List<ActSeckillDTO> list = new ArrayList<>();
        for (ActZoneCourse actZoneCourse : actZoneCourseList) {
            Act act = actDao.getByIdAndStatusIdAndTime(actZoneCourse.getActId(), StatusIdEnum.YES.getCode(), new Date());
            // 拿在用活动，仅会有一个在用活动
            if (ObjectUtil.isNotNull(act)) {
                dto.setActId(actZoneCourse.getActId());
                dto.setActType(actZoneCourse.getActType());
                // 补充活动类型
                if (ActTypeEnum.COUPON.getCode().equals(actZoneCourse.getActType())) {
                    //优惠卷信息
                    ActCoupon actCoupon = actCouponDao.getByActIdAndZondIdAndCourseIdAndStatusId(act.getId(),null, bo.getCourseId(), StatusIdEnum.YES.getCode());
                    ActCouponDTO actCouponDTO = BeanUtil.copyProperties(actCoupon, ActCouponDTO.class);
                    dto.setActCouponDTO(actCouponDTO);
                } else {
                    //秒杀信息
                    list = actSeckill(list, actZoneCourse.getZoneId(), bo.getCourseId());
                }
            }
        }
        dto.setActSeckillDTOList(list);
        return Result.success(dto);
    }

    private List<ActSeckillDTO> actSeckill(List<ActSeckillDTO> list, Long zoneId, Long courseId) {
        //秒杀信息
        ActSeckill actSeckill = actSeckillDao.getByZoneIdAndCourseIdAndStatusId(zoneId, courseId, StatusIdEnum.YES.getCode());
        ActZone actZone = actZoneDao.getById(zoneId);
        if (ObjectUtil.isNotNull(actZone) && StatusIdEnum.YES.getCode().equals(actZone.getStatusId())) {
            ActSeckillDTO actSeckillDTO = BeanUtil.copyProperties(actSeckill, ActSeckillDTO.class);
            actSeckillDTO.setBeginTime(actZone.getBeginTime());
            actSeckillDTO.setEndTime(actZone.getEndTime());
            actSeckillDTO.setIsStart(time(actZone.getBeginTime(), actZone.getEndTime()));
            list.add(actSeckillDTO);
        }
        return list;
    }

    /**
     * 秒杀活动
     * isStart（1：未开始；2：正在秒杀；3：秒杀活动结束）
     *
     * @return
     */
    private int time(Date beginTime, Date endTime) {
        //创建日期转换对
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        int isStart = 1;
        try {
            //将字符串转换为date类型
            Date dt1 = df.parse(DateUtil.getTime(new Date()));

            if (dt1.before(beginTime)) {
                // 秒杀活动未开始
                isStart = 1;
            } else if (dt1.after(beginTime) && dt1.before(endTime)) {
                // 正在秒杀活动
                isStart = 2;
            } else {
                // 秒杀活动结束
                isStart = 3;
            }
            return isStart;
        } catch (ParseException e) {
            return isStart;
        }
    }

}
