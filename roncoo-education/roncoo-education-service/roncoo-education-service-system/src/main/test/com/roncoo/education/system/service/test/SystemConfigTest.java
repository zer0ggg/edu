package com.roncoo.education.system.service.test;

import cn.hutool.core.bean.BeanUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.system.common.dto.WebsiteDTO;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.feign.biz.FeignSysConfigBiz;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Quanf
 * 2020/4/30 15:57
 */
@Log4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SystemConfigTest {

    @Autowired
    private SysConfigDao sysConfigDao;

    @Autowired
    private FeignSysConfigBiz feignSysConfigBiz;

    @Test
    public void comparisonWebsite() {
//        Website website = websiteDao.getByStatusId(StatusIdEnum.YES.getCode());
//        WebsiteDTO dto = com.roncoo.education.common.core.tools.BeanUtil.copyProperties(website, WebsiteDTO.class);
//        if (StringUtils.hasText(dto.getPrn())) {
//            // 公安网备案号处理
//            String regEx = "[^0-9]";
//            Pattern p = Pattern.compile(regEx);
//            Matcher m = p.matcher(dto.getPrn());
//            dto.setPrnNo(m.replaceAll("").trim());
//        }

        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        WebsiteDTO newDto = BeanUtil.mapToBean(configMap, WebsiteDTO.class, true);
        if (StringUtils.hasText(newDto.getPrn())) {
            // 公安网备案号处理
            String regEx = "[^0-9]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(newDto.getPrn());
            newDto.setPrnNo(m.replaceAll("").trim());
        }
        log.info("旧数据-----------------------------------------------");
//        log.info(dto);
        log.info("新数据-----------------------------------------------");
        log.info(newDto);
    }

    @Test
    public void comparisonSys() {
//        Sys sys = sysDao.getSys();
//        SysVO vo = com.roncoo.education.common.core.tools.BeanUtil.copyProperties(sys, SysVO.class);

        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
//        SysVO newVo = BeanUtil.mapToBean(configMap, SysVO.class, true);
        log.info("旧数据-----------------------------------------------");
//        log.info(vo);
        log.info("新数据-----------------------------------------------");
//        log.info(newVo);
    }

    @Test
    public void addSysData() {
        // 过滤字段
        Map<String, String> fieldMap = new HashMap<>();
        fieldMap.put("id", "");
        fieldMap.put("gmtCreate", "");
        fieldMap.put("gmtModified", "");
        fieldMap.put("statusId", "");
        fieldMap.put("sort", "");

        // 获取备注
//        SysDTO sysDTO = new SysDTO();
//        Map<String, String> nameMap = getDeclaredFieldsInfo(sysDTO, new HashMap<>());

        // 获取数据源
        /*Sys sys = sysDao.getSys();
        Map<String, Object> sysMap = BeanUtil.beanToMap(sys);
        System.out.println(sysMap);
        for (Map.Entry<String, Object> stringObjectEntry : sysMap.entrySet()) {
            if (!fieldMap.containsKey(stringObjectEntry.getKey())) {
                stringObjectEntry.getKey();
                SysConfig config = new SysConfig();
                config.setConfigName(nameMap.get(stringObjectEntry.getKey()));
                config.setConfigKey(stringObjectEntry.getKey());
                config.setConfigValue(String.valueOf(stringObjectEntry.getValue()));
                config.setAllowDelete(AllowDeleteEnum.NOT_ALLOW.getCode());
                sysConfigDao.save(config);
            }
        }*/
    }


    @Test
    public void addWebsiteData() {
        // 过滤字段
        Map<String, String> fieldMap = new HashMap<>();
        fieldMap.put("id", "");
        fieldMap.put("gmtCreate", "");
        fieldMap.put("gmtModified", "");
        fieldMap.put("statusId", "");
        fieldMap.put("sort", "");

        // 获取备注
        WebsiteDTO websiteDTO = new WebsiteDTO();
        Map<String, String> nameMap = getDeclaredFieldsInfo(websiteDTO, new HashMap<>());

        // 获取数据源
        /*Website website = websiteDao.getWebsite();
        Map<String, Object> websiteMap = BeanUtil.beanToMap(website);
        System.out.println(websiteMap);
        for (Map.Entry<String, Object> stringObjectEntry : websiteMap.entrySet()) {
            if (!fieldMap.containsKey(stringObjectEntry.getKey())) {
                stringObjectEntry.getKey();
                SysConfig config = new SysConfig();
                config.setConfigName(nameMap.get(stringObjectEntry.getKey()));
                config.setConfigKey(stringObjectEntry.getKey());
                config.setConfigValue(String.valueOf(stringObjectEntry.getValue()));
                config.setAllowDelete(AllowDeleteEnum.NOT_ALLOW.getCode());
                sysConfigDao.save(config);
            }
        }*/
    }

    /**
     * 获取备注ApiModelProperty
     *
     * @param instance
     * @param fieldMap
     * @return
     */
    public static Map<String, String> getDeclaredFieldsInfo(Object instance, Map<String, String> fieldMap) {
        Map<String, String> map = new HashMap();
        Class<?> clazz = instance.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // 除过fieldMap中的属性，其他属性都获取
            if (!fieldMap.containsValue(field.getName())) {
                // Field field=clazz.getDeclaredField(fields[i].getName());
                boolean annotationPresent = field.isAnnotationPresent(ApiModelProperty.class);
                if (annotationPresent) {
                    // 获取注解值
                    String name = field.getAnnotation(ApiModelProperty.class).value();
                    map.put(field.getName(), name);
                }
            }
        }
        return map;
    }
}
