package com.roncoo.education.system.service.test;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.roncoo.education.common.core.enums.MenuTypeEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysMenuDao;
import com.roncoo.education.system.service.dao.SysPermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author Quanf
 * 2020/4/30 15:57
 */
@Log4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SystemMenuTest {

    @Autowired
    private SysMenuDao sysMenuDao;
    @Autowired
    private SysPermissionDao sysPermissionDao;

    @Test
    public static void main(String[] args) throws ClassNotFoundException {
//        JSONObject jsonObject = JSONUtil.parseObj("{\"controller\":\"com.roncoo.education.exam.service.pc.PcRecommendController\",\"name\":\"推荐管理\",\"id\":1265493622220308481}\n");//        String[] menuArr = "/user/pay/rule/list".split("/");
//        String name = menuArr[1];
//        StringBuilder sb = new StringBuilder("com.roncoo.education.").append(name).append(".service.pc.");
//        StringBuilder controller = new StringBuilder("Pc");
//        for (int i = 2; i < menuArr.length - 1; i++) {
//            controller.append(StrUtil.upperFirst(menuArr[i]));
//        }
//        System.out.println(sb.append(controller).append("Controller").toString());
        setPermissionValue("com.roncoo.education.course.service.pc.PcCourseController", 0L);
    }

    @Test
    public void addSysPermission() throws ClassNotFoundException {
        List<SysMenu> menuList = sysMenuDao.listAll();
        JSONArray jsonArray = new JSONArray();
        for (SysMenu menu : menuList) {
            if (MenuTypeEnum.MENU.getCode().equals(menu.getMenuType())) {
                String[] menuArr = menu.getRouterUrl().split("/");
                String name = menuArr[1];
                StringBuilder sb = new StringBuilder("com.roncoo.education.").append(name).append(".service.pc.");
                StringBuilder controller = new StringBuilder("Pc");
                for (int i = 2; i < menuArr.length - 1; i++) {
                    controller.append(StrUtil.upperFirst(menuArr[i]));
                }
                String controllerName = sb.append(controller).append("Controller").toString();
                System.out.println();
                System.out.println(menu.getMenuName());
                System.out.println(menu.getId());
                System.out.println(controllerName);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", menu.getMenuName());
                jsonObject.put("id", menu.getId());
                jsonObject.put("controller", controllerName);
                jsonArray.add(jsonObject);
                System.out.println("---------------------------");
//                setPermissionValue(controllerName, menu.getId());
            }
        }
        System.out.println(jsonArray);

    }

    public static void setPermissionValue(String className, Long menuId) throws ClassNotFoundException {
        //通过反射获取到类，填入类名
        Class cl1 = Class.forName(className);
        //获取RequestMapping注解
        RequestMapping anno = (RequestMapping) cl1.getAnnotation(RequestMapping.class);
        //获取类注解的value值
        String headUrl = anno.value()[0];

        //获取类中所有的方法
        Method[] methods = cl1.getDeclaredMethods();
        for (Method method : methods) {
            String pValue = headUrl;
            ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
            GetMapping getRequestMothed = method.getAnnotation(GetMapping.class);
            PutMapping putRequestMothed = method.getAnnotation(PutMapping.class);
            PostMapping postRequestMothed = method.getAnnotation(PostMapping.class);
            DeleteMapping deleteRequestMothed = method.getAnnotation(DeleteMapping.class);
            RequestMapping requestMappingMothed = method.getAnnotation(RequestMapping.class);
            //获取类注解的value值
            String pName = null;
            if (apiOperation != null) {
                pName = apiOperation.value();
            }
            if (getRequestMothed != null) {
                pValue = pValue + getRequestMothed.value()[0];
            }
            if (putRequestMothed != null) {
                pValue = pValue + putRequestMothed.value()[0];
            }
            if (postRequestMothed != null) {
                pValue = pValue + postRequestMothed.value()[0];
            }
            if (deleteRequestMothed != null) {
                pValue = pValue + deleteRequestMothed.value()[0];
            }
            if (requestMappingMothed != null) {
                pValue = pValue + requestMappingMothed.value()[0];
            }
            if (StrUtil.isNotEmpty(pName) && StrUtil.isNotEmpty(pValue)) {
                pValue = pValue.replace("/", ":");
                pValue = pValue.substring(1);
                SysPermission record = new SysPermission();
                record.setMenuId(menuId);
                record.setPermissionName(pName);
                record.setPermissionValue(pValue);
                System.out.println(pName);
                System.out.println(pValue);
                System.out.println("--------------");
//            sysPermissionDao.save(record);
            }
        }
    }

    public void menuArray(JSONArray menuJsonArray, Long parentId) {
        for (int i = 0; i < menuJsonArray.size(); i += 1) {
            JSONObject object = menuJsonArray.getJSONObject(i);
            SysMenu sysMenu = new SysMenu();
            sysMenu.setId(IdWorker.getId());
            sysMenu.setParentId(parentId);
            sysMenu.setMenuType(MenuTypeEnum.MENU.getCode());
            sysMenu.setMenuName(String.valueOf(object.get("name")));
            sysMenu.setRouterUrl(String.valueOf(object.get("path")));
            sysMenu.setIcon(String.valueOf(object.get("targetName")));
            if (parentId == 0) {
                parentId = IdWorker.getId();
                sysMenu.setId(parentId);
            }
            if (ObjectUtil.isNotNull(object.get("children"))) {
                sysMenu.setMenuType(MenuTypeEnum.DIRECTORY.getCode());
            }
            sysMenuDao.save(sysMenu);
            if (MenuTypeEnum.MENU.getCode().equals(sysMenu.getMenuType())) {
                String pName = sysMenu.getMenuName();
                String pValue = sysMenu.getRouterUrl().replace("/", ":");
                pValue = pValue.substring(1);
                //list
                SysPermission record = new SysPermission();
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "列表");
                record.setPermissionValue(pValue);
                sysPermissionDao.save(record);
                //save
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "添加");
                record.setPermissionValue(pValue.replace("list", "save"));
                sysPermissionDao.save(record);
                //view
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "查看");
                record.setPermissionValue(pValue.replace("list", "view"));
                sysPermissionDao.save(record);
                //edit
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "修改");
                record.setPermissionValue(pValue.replace("list", "edit"));
                sysPermissionDao.save(record);
                //delete
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "删除");
                record.setPermissionValue(pValue.replace("list", "delete"));
                sysPermissionDao.save(record);
                //update/status
                record.setMenuId(sysMenu.getId());
                record.setPermissionName(pName + "状态修改");
                record.setPermissionValue(pValue.replace("list", "update:status"));
                sysPermissionDao.save(record);
            }
            if (ObjectUtil.isNotNull(object.get("children"))) {
                JSONArray childrenJsonArray = JSONUtil.parseArray(object.get("children"));
                menuArray(childrenJsonArray, parentId);
                parentId = 0L;
//                if (MenuTypeEnum.MENU.getCode().equals(sysMenu.getMenuType())) {
//                }
            }
        }
    }

    @Test
    public void addPermissionData() throws ClassNotFoundException {
//        String permissionStr = "[{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1260809267679563778},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderController\",\"name\":\"会员订单\",\"id\":1260809270137425922},{\"controller\":\"com.roncoo.education.system.service.pc.PcNavBarController\",\"name\":\"头部导航\",\"id\":1260809274948292610},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvTopPcController\",\"name\":\"顶部广告\",\"id\":1260809283462729729},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvCarouselPcController\",\"name\":\"轮播广告\",\"id\":1260809284435808257},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvTopAppController\",\"name\":\"顶部广告\",\"id\":1260809286088364034},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvCarouselAppController\",\"name\":\"轮播广告\",\"id\":1260809286843338754},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseRecordController\",\"name\":\"点播课程列表\",\"id\":1260809288458145793},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditRecordController\",\"name\":\"点播课程审核\",\"id\":1260809289196343297},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryRecordController\",\"name\":\"点播课程分类\",\"id\":1260809289938735106},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseLiveController\",\"name\":\"直播课程列表\",\"id\":1260809290739847169},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditLiveController\",\"name\":\"直播课程审核\",\"id\":1260809291494821889},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryLiveController\",\"name\":\"直播课程分类\",\"id\":1260809292224630785},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamController\",\"name\":\"试卷列表管理\",\"id\":1260809293059297281},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamAuditController\",\"name\":\"试卷审核管理\",\"id\":1260809293763940354},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryExamListCourseController\",\"name\":\"科目管理\",\"id\":1260809294602801153},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryExamListYearController\",\"name\":\"年份管理\",\"id\":1260809295320027137},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryExamListSourceController\",\"name\":\"来源管理\",\"id\":1260809296020475905},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryExamListDifficultyController\",\"name\":\"难度管理\",\"id\":1260809296720924674},{\"controller\":\"com.roncoo.education.exam.service.pc.PcQuestionController\",\"name\":\"试题列表管理\",\"id\":1260809297597534210},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryQuestionListCourseController\",\"name\":\"科目管理\",\"id\":1260809299224924162},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryQuestionListYearController\",\"name\":\"年份管理\",\"id\":1260809299988287490},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryQuestionListSourceController\",\"name\":\"来源管理\",\"id\":1260809300697124866},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryQuestionListDifficultyController\",\"name\":\"难度管理\",\"id\":1260809301636648961},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryQuestionListTopController\",\"name\":\"题类管理\",\"id\":1260809302525841410},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseResourceController\",\"name\":\"资源列表\",\"id\":1260809303368896513},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditResourceController\",\"name\":\"资源审核\",\"id\":1260809304119676929},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryResourceController\",\"name\":\"资源分类\",\"id\":1260809304874651649},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseResourceRecommendController\",\"name\":\"资源推荐\",\"id\":1260809305608654849},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogMessageController\",\"name\":\"资讯列表\",\"id\":1260809306413961218},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogBlogController\",\"name\":\"博客列表\",\"id\":1260809310239166466},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelBlogController\",\"name\":\"标签列表\",\"id\":1260809310956392449},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendBlogController\",\"name\":\"推荐列表\",\"id\":1260809311757504513},{\"controller\":\"com.roncoo.education.community.service.pc.PcBloggerController\",\"name\":\"博主管理\",\"id\":1260809312525062145},{\"controller\":\"com.roncoo.education.community.service.pc.PcQuestionsController\",\"name\":\"问答列表\",\"id\":1260809313389088770},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelQuestionsController\",\"name\":\"标签列表\",\"id\":1260809314123091970},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerController\",\"name\":\"讲师管理列表\",\"id\":1260809314924204034},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerAuditController\",\"name\":\"讲师审核列表\",\"id\":1260809315603681282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserAccountExtractLogController\",\"name\":\"打款进度管理\",\"id\":1260809316308324354},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserExtController\",\"name\":\"学员信息管理\",\"id\":1260809317142990850},{\"controller\":\"com.roncoo.education.system.service.pc.PcVipSetController\",\"name\":\"会员等级设置\",\"id\":1260809317872799746},{\"controller\":\"com.roncoo.education.sys.service.pc.PcUserController\",\"name\":\"用户管理\",\"id\":1260809318736826369},{\"controller\":\"com.roncoo.education.sys.service.pc.PcRoleController\",\"name\":\"角色管理\",\"id\":1260809319516966914},{\"controller\":\"com.roncoo.education.sys.service.pc.PcMenuController\",\"name\":\"菜单权限\",\"id\":1260809320297107457},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysConfigController\",\"name\":\"参数配置\",\"id\":1260809321161134082},{\"controller\":\"com.roncoo.education.user.service.pc.PcMsgController\",\"name\":\"站内消息管理\",\"id\":1260809323438641153},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserMsgController\",\"name\":\"消息查看日志\",\"id\":1260809324151672833},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogLoginController\",\"name\":\"用户分析\",\"id\":1260809325040865282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogSmsController\",\"name\":\"短信分析\",\"id\":1260809325749702658},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderLogController\",\"name\":\"支付日志\",\"id\":1260809326462734337},{\"controller\":\"com.roncoo.education.system.service.pc.PcZonePcController\",\"name\":\"专区设置\",\"id\":1264735553928556546},{\"controller\":\"com.roncoo.education.system.service.pc.PcZoneAppController\",\"name\":\"专区设置\",\"id\":1265920753689632770},{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1266177817438920706},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryHomeController\",\"name\":\"分类推荐\",\"id\":1260809272863723522},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelMessageController\",\"name\":\"标签列表\",\"id\":1260809307189907457},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayRuleController\",\"name\":\"支付方式\",\"id\":1260809322616557569},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteNavController\",\"name\":\"底部文章\",\"id\":1260809277649424385},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvMessagePcController\",\"name\":\"轮播广告\",\"id\":1260809308653719553},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayChannelController\",\"name\":\"支付配置\",\"id\":1260809321874165762},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteLinkController\",\"name\":\"友情链接\",\"id\":1260809280451219457},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleZoneCategoryController\",\"name\":\"资讯分栏\",\"id\":1260809309417082882},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendMessageController\",\"name\":\"推荐列表\",\"id\":1260809307932299266},{\"controller\":\"com.roncoo.education.user.service.pc.PcChannelController\",\"name\":\"频道管理列表\",\"id\":1264745155168563202},{\"controller\":\"com.roncoo.education.exam.service.pc.PcRecommendController\",\"name\":\"推荐管理\",\"id\":1265493622220308481}]\n";
        String permissionStr = "[{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1260809267679563778},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderController\",\"name\":\"会员订单\",\"id\":1260809270137425922},{\"controller\":\"com.roncoo.education.system.service.pc.PcNavBarController\",\"name\":\"头部导航\",\"id\":1260809274948292610},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"顶部广告\",\"id\":1260809283462729729},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809284435808257},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"顶部广告\",\"id\":1260809286088364034},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809286843338754},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"点播课程列表\",\"id\":1260809288458145793},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"点播课程审核\",\"id\":1260809289196343297},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"点播课程分类\",\"id\":1260809289938735106},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"直播课程列表\",\"id\":1260809290739847169},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"直播课程审核\",\"id\":1260809291494821889},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"直播课程分类\",\"id\":1260809292224630785},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamInfoController\",\"name\":\"试卷列表管理\",\"id\":1260809293059297281},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamInfoAuditController\",\"name\":\"试卷审核管理\",\"id\":1260809293763940354},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"科目管理\",\"id\":1260809294602801153},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"年份管理\",\"id\":1260809295320027137},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"来源管理\",\"id\":1260809296020475905},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"难度管理\",\"id\":1260809296720924674},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamQuestionController\",\"name\":\"试题列表管理\",\"id\":1260809297597534210},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"科目管理\",\"id\":1260809299224924162},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"年份管理\",\"id\":1260809299988287490},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"来源管理\",\"id\":1260809300697124866},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"难度管理\",\"id\":1260809301636648961},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"题类管理\",\"id\":1260809302525841410},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"资源列表\",\"id\":1260809303368896513},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"资源审核\",\"id\":1260809304119676929},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"资源分类\",\"id\":1260809304874651649},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseRecommendController\",\"name\":\"资源推荐\",\"id\":1260809305608654849},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogController\",\"name\":\"资讯列表\",\"id\":1260809306413961218},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogController\",\"name\":\"博客列表\",\"id\":1260809310239166466},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809310956392449},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendController\",\"name\":\"推荐列表\",\"id\":1260809311757504513},{\"controller\":\"com.roncoo.education.community.service.pc.PcBloggerController\",\"name\":\"博主管理\",\"id\":1260809312525062145},{\"controller\":\"com.roncoo.education.community.service.pc.PcQuestionsController\",\"name\":\"问答列表\",\"id\":1260809313389088770},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809314123091970},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerController\",\"name\":\"讲师管理列表\",\"id\":1260809314924204034},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerAuditController\",\"name\":\"讲师审核列表\",\"id\":1260809315603681282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserAccountExtractLogController\",\"name\":\"打款进度管理\",\"id\":1260809316308324354},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserExtController\",\"name\":\"学员信息管理\",\"id\":1260809317142990850},{\"controller\":\"com.roncoo.education.system.service.pc.PcVipSetController\",\"name\":\"会员等级设置\",\"id\":1260809317872799746},{\"controller\":\"com.roncoo.education.sys.service.pc.PcSysUserController\",\"name\":\"用户管理\",\"id\":1260809318736826369},{\"controller\":\"com.roncoo.education.sys.service.pc.PcSysRoleController\",\"name\":\"角色管理\",\"id\":1260809319516966914},{\"controller\":\"com.roncoo.education.sys.service.pc.PcSysMenuController\",\"name\":\"菜单权限\",\"id\":1260809320297107457},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysConfigController\",\"name\":\"参数配置\",\"id\":1260809321161134082},{\"controller\":\"com.roncoo.education.user.service.pc.PcMsgController\",\"name\":\"站内消息管理\",\"id\":1260809323438641153},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserMsgController\",\"name\":\"消息查看日志\",\"id\":1260809324151672833},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogLoginController\",\"name\":\"用户分析\",\"id\":1260809325040865282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogSmsController\",\"name\":\"短信分析\",\"id\":1260809325749702658},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderLogController\",\"name\":\"支付日志\",\"id\":1260809326462734337},{\"controller\":\"com.roncoo.education.system.service.pc.PcZoneController\",\"name\":\"专区设置\",\"id\":1264735553928556546},{\"controller\":\"com.roncoo.education.system.service.pc.PcZoneController\",\"name\":\"专区设置\",\"id\":1265920753689632770},{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1266177817438920706},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"分类推荐\",\"id\":1260809272863723522},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809307189907457},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayRuleController\",\"name\":\"支付方式\",\"id\":1260809322616557569},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteNavController\",\"name\":\"底部文章\",\"id\":1260809277649424385},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809308653719553},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayChannelController\",\"name\":\"支付配置\",\"id\":1260809321874165762},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteLinkController\",\"name\":\"友情链接\",\"id\":1260809280451219457},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleZoneCategoryController\",\"name\":\"资讯分栏\",\"id\":1260809309417082882},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendController\",\"name\":\"推荐列表\",\"id\":1260809307932299266},{\"controller\":\"com.roncoo.education.user.service.pc.PcLiveChannelController\",\"name\":\"频道管理列表\",\"id\":1264745155168563202},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamRecommendController\",\"name\":\"推荐管理\",\"id\":1265493622220308481}]";
        JSONArray permissionArray = JSONUtil.parseArray(permissionStr);
        for (int i = 0; i < permissionArray.size(); i += 1) {
            JSONObject object = permissionArray.getJSONObject(i);
            String className = String.valueOf(object.get("controller"));
            System.out.println(String.valueOf(object.get("name")));
            String.valueOf(object.get("id"));
            Class cl1 = Class.forName(className);
        }
    }

    @Test
    public void addSysData() {
        String menuStr = "[\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"订单管理\",\n" +
                "    \"path\": \"often\",\n" +
                "    \"targetName\": 'often',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"课程订单\",\n" +
                "        \"path\": \"/user/order/info/list\",\n" +
                "        \"targetName\": \"orderInfo\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"会员订单\",\n" +
                "        \"path\": \"/user/user/order/list\",\n" +
                "        \"targetName\": \"userOrder\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"首页设置\",\n" +
                "    \"path\": \"home\",\n" +
                "    \"targetName\": 'ad',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"分类推荐\",\n" +
                "        \"path\": \"/course/course/category/home/list\",\n" +
                "        \"targetName\": \"category\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"头部导航\",\n" +
                "        \"path\": \"/system/nav/bar/list\",\n" +
                "        \"targetName\": \"navBar\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"底部导航\",\n" +
                "        \"path\": \"/system/website/nav/list\",\n" +
                "        \"targetName\": \"websiteNav\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"友情链接\",\n" +
                "        \"path\": \"/system/website/link/list\",\n" +
                "        \"targetName\": \"websiteLink\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"电脑端\",\n" +
                "        \"path\": \"pc\",\n" +
                "        \"targetName\": \"pc\",\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"顶部广告\",\n" +
                "            \"path\": \"/system/adv/top/pc/list\",\n" +
                "            \"targetName\": \"topAD\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"轮播广告\",\n" +
                "            \"path\": \"/system/adv/carousel/pc/list\",\n" +
                "            \"targetName\": \"carousel\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"专区设置\",\n" +
                "            \"path\": \"/course/zone/pc/list\",\n" +
                "            \"targetName\": \"zone\"\n" +
                "          }\n" +
                "         ]\n" +
                "       },\n" +
                "       {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"移动端\",\n" +
                "        \"path\": \"app\",\n" +
                "        \"targetName\": \"app\",\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"顶部广告\",\n" +
                "            \"path\": \"/system/adv/top/app/list\",\n" +
                "            \"targetName\": \"topAD\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"轮播广告\",\n" +
                "            \"path\": \"/system/adv/carousel/app/list\",\n" +
                "            \"targetName\": \"carousel\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"专区设置\",\n" +
                "            \"path\": \"/course/zone/app/list\",\n" +
                "            \"targetName\": \"zone\"\n" +
                "          }\n" +
                "          ]\n" +
                "       },\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"点播课程\",\n" +
                "    \"path\": \"record\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"点播课程列表\",\n" +
                "        \"path\": \"/course/course/record/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"点播课程审核\",\n" +
                "        \"path\": \"/course/course/audit/record/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"点播课程分类\",\n" +
                "        \"path\": \"/course/course/category/record/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"直播课程\",\n" +
                "    \"path\": \"live\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"直播课程列表\",\n" +
                "        \"path\": \"/course/course/live/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"直播课程审核\",\n" +
                "        \"path\": \"/course/course/audit/live/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"直播课程分类\",\n" +
                "        \"path\": \"/course/course/category/live/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"试卷管理\",\n" +
                "    \"path\": \"exam\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"试卷列表管理\",\n" +
                "        \"path\": \"/exam/exam/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"试卷审核管理\",\n" +
                "        \"path\": \"/exam/exam/audit/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"分类管理\",\n" +
                "        \"path\": \"/exam/exam/category/examList\",\n" +
                "        \"targetName\": \"agent\",\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"科目管理\",\n" +
                "            \"path\": \"/exam/exam/category/examList/course/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 2,\n" +
                "            \"name\": \"年份管理\",\n" +
                "            \"path\": \"/exam/exam/category/examList/year/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 3,\n" +
                "            \"name\": \"来源管理\",\n" +
                "            \"path\": \"/exam/exam/category/examList/source/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 4,\n" +
                "            \"name\": \"难度管理\",\n" +
                "            \"path\": \"/exam/exam/category/examList/difficulty/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"试题管理\",\n" +
                "    \"path\": \"subject\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"试题列表管理\",\n" +
                "        \"path\": \"/exam/subject/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"试题审核管理\",\n" +
                "        \"path\": \"/exam/subject/audit/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"分类管理\",\n" +
                "        \"path\": \"/exam/exam/category/subjectList\",\n" +
                "        \"targetName\": \"agent\",\n" +
                "        \"children\": [\n" +
                "          {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"科目\",\n" +
                "            \"path\": \"/exam/exam/category/subjectList/course/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 2,\n" +
                "            \"name\": \"年份\",\n" +
                "            \"path\": \"/exam/exam/category/subjectList/year/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 3,\n" +
                "            \"name\": \"来源\",\n" +
                "            \"path\": \"/exam/exam/category/subjectList/source/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 4,\n" +
                "            \"name\": \"难度\",\n" +
                "            \"path\": \"/exam/exam/category/subjectList/difficulty/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id\": 4,\n" +
                "            \"name\": \"题类\",\n" +
                "            \"path\": \"/exam/exam/category/subjectList/top/list\",\n" +
                "            \"targetName\": \"agent\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"资源中心\",\n" +
                "    \"path\": \"resource\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"资源列表\",\n" +
                "        \"path\": \"/course/course/resource/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"资源审核\",\n" +
                "        \"path\": \"/course/course/audit/resource/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"资源分类\",\n" +
                "        \"path\": \"/course/course/category/resource/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"资源推荐\",\n" +
                "        \"path\": \"/course/course/resource/recommend/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"新闻资讯\",\n" +
                "    \"path\": \"info\",\n" +
                "    \"targetName\": \"agent\",\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"资讯列表\",\n" +
                "        \"path\": \"/community/blog/message/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"标签列表\",\n" +
                "        \"path\": \"/community/label/message/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"推荐列表\",\n" +
                "        \"path\": \"/community/article/recommend/message/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"轮播广告\",\n" +
                "        \"path\": \"/system/adv/message/pc/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"专区设置\",\n" +
                "        \"path\": \"/community/article/zone/category/list\",\n" +
                "        \"targetName\": \"agent\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"博客管理\",\n" +
                "    \"path\": \"blog\",\n" +
                "    \"targetName\": 'community',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"博客列表\",\n" +
                "        \"path\": \"/community/blog/blog/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"标签列表\",\n" +
                "        \"path\": \"/community/label/blog/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"推荐列表\",\n" +
                "        \"path\": \"/community/article/recommend/blog/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"博主管理\",\n" +
                "        \"path\": \"/community/blogger/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"问答管理\",\n" +
                "    \"path\": \"questions\",\n" +
                "    \"targetName\": 'community',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"问答列表\",\n" +
                "        \"path\": \"/community/questions/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"标签列表\",\n" +
                "        \"path\": \"/community/label/questions/list\",\n" +
                "        \"targetName\": \"community\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"讲师管理\",\n" +
                "    \"path\": \"lecturer\",\n" +
                "    \"targetName\": 'lecturer',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"讲师列表\",\n" +
                "        \"path\": \"/user/lecturer/list\",\n" +
                "        \"targetName\": \"lecturer\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"讲师审核列表\",\n" +
                "        \"path\": \"/user/lecturer/audit/list\",\n" +
                "        \"targetName\": \"lecturer\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"打款进度管理\",\n" +
                "        \"path\": \"/user/user/account/extract/log/list\",\n" +
                "        \"targetName\": \"lecturer\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"学员管理\",\n" +
                "    \"path\": \"user\",\n" +
                "    \"targetName\": 'user',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"学员列表\",\n" +
                "        \"path\": \"/user/user/ext/list\",\n" +
                "        \"targetName\": \"user\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"VIP等级设置\",\n" +
                "        \"path\": \"/system/vip/set/list\",\n" +
                "        \"targetName\": \"user\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"系统管理\",\n" +
                "    \"path\": \"system\",\n" +
                "    \"targetName\": 'system',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"用户管理\",\n" +
                "        \"path\": \"/sys/user/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"角色管理\",\n" +
                "        \"path\": \"/sys/role/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"菜单权限\",\n" +
                "        \"path\": \"/sys/menu/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"参数配置\",\n" +
                "        \"path\": \"/system/sys/config/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"支付配置\",\n" +
                "        \"path\": \"/user/pay/channel/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"交易配置\",\n" +
                "        \"path\": \"/user/pay/rule/list\",\n" +
                "        \"targetName\": \"system\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"消息管理\",\n" +
                "    \"path\": \"msg\",\n" +
                "    \"targetName\": 'msg',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"站内消息管理\",\n" +
                "        \"path\": \"/user/msg/list\",\n" +
                "        \"targetName\": \"msg\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"消息查看日志\",\n" +
                "        \"path\": \"/user/user/msg/list\",\n" +
                "        \"targetName\": \"msg\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"数据中心\",\n" +
                "    \"path\": \"count\",\n" +
                "    \"targetName\": 'count',\n" +
                "    \"children\": [\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"用户分析\",\n" +
                "        \"path\": \"/user/user/log/login/list\",\n" +
                "        \"targetName\": \"count\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"短信分析\",\n" +
                "        \"path\": \"/user/user/log/sms/list\",\n" +
                "        \"targetName\": \"count\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"支付日志\",\n" +
                "        \"path\": \"/user/user/order/log/list\",\n" +
                "        \"targetName\": \"count\"\n" +
                "      },\n" +
                "    ]\n" +
                "  }\n" +
                "]";
        JSONArray menuJsonArray = JSONUtil.parseArray(menuStr);
        menuArray(menuJsonArray, 0L);
    }
}
