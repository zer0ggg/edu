package com.roncoo.education.system.service.test;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.roncoo.education.system.feign.qo.SysPermissionQO;
import io.swagger.annotations.ApiOperation;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * @author Quanf
 * 2020/5/29 11:54
 */
public class SysPermission {

    @Test
    public void permission() throws ClassNotFoundException {
//        String permissionStr = "[{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1260809267679563778},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderController\",\"name\":\"会员订单\",\"id\":1260809270137425922},{\"controller\":\"com.roncoo.education.system.service.pc.PcNavBarController\",\"name\":\"头部导航\",\"id\":1260809274948292610},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"顶部广告\",\"id\":1260809283462729729},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809284435808257},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"顶部广告\",\"id\":1260809286088364034},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809286843338754},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"点播课程列表\",\"id\":1260809288458145793},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"点播课程审核\",\"id\":1260809289196343297},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"点播课程分类\",\"id\":1260809289938735106},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"直播课程列表\",\"id\":1260809290739847169},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"直播课程审核\",\"id\":1260809291494821889},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"直播课程分类\",\"id\":1260809292224630785},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamInfoController\",\"name\":\"试卷列表管理\",\"id\":1260809293059297281},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamInfoAuditController\",\"name\":\"试卷审核管理\",\"id\":1260809293763940354},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"科目管理\",\"id\":1260809294602801153},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"年份管理\",\"id\":1260809295320027137},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"来源管理\",\"id\":1260809296020475905},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"难度管理\",\"id\":1260809296720924674},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamQuestionController\",\"name\":\"试题列表管理\",\"id\":1260809297597534210},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"科目管理\",\"id\":1260809299224924162},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"年份管理\",\"id\":1260809299988287490},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"来源管理\",\"id\":1260809300697124866},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"难度管理\",\"id\":1260809301636648961},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamCategoryController\",\"name\":\"题类管理\",\"id\":1260809302525841410},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseController\",\"name\":\"资源列表\",\"id\":1260809303368896513},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseAuditController\",\"name\":\"资源审核\",\"id\":1260809304119676929},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"资源分类\",\"id\":1260809304874651649},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseRecommendController\",\"name\":\"资源推荐\",\"id\":1260809305608654849},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogController\",\"name\":\"资讯列表\",\"id\":1260809306413961218},{\"controller\":\"com.roncoo.education.community.service.pc.PcBlogController\",\"name\":\"博客列表\",\"id\":1260809310239166466},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809310956392449},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendController\",\"name\":\"推荐列表\",\"id\":1260809311757504513},{\"controller\":\"com.roncoo.education.community.service.pc.PcBloggerController\",\"name\":\"博主管理\",\"id\":1260809312525062145},{\"controller\":\"com.roncoo.education.community.service.pc.PcQuestionsController\",\"name\":\"问答列表\",\"id\":1260809313389088770},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809314123091970},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerController\",\"name\":\"讲师管理列表\",\"id\":1260809314924204034},{\"controller\":\"com.roncoo.education.user.service.pc.PcLecturerAuditController\",\"name\":\"讲师审核列表\",\"id\":1260809315603681282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserAccountExtractLogController\",\"name\":\"打款进度管理\",\"id\":1260809316308324354},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserExtController\",\"name\":\"学员信息管理\",\"id\":1260809317142990850},{\"controller\":\"com.roncoo.education.system.service.pc.PcVipSetController\",\"name\":\"会员等级设置\",\"id\":1260809317872799746},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysUserController\",\"name\":\"用户管理\",\"id\":1260809318736826369},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysRoleController\",\"name\":\"角色管理\",\"id\":1260809319516966914},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysMenuController\",\"name\":\"菜单权限\",\"id\":1260809320297107457},{\"controller\":\"com.roncoo.education.system.service.pc.PcSysConfigController\",\"name\":\"参数配置\",\"id\":1260809321161134082},{\"controller\":\"com.roncoo.education.user.service.pc.PcMsgController\",\"name\":\"站内消息管理\",\"id\":1260809323438641153},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserMsgController\",\"name\":\"消息查看日志\",\"id\":1260809324151672833},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogLoginController\",\"name\":\"用户分析\",\"id\":1260809325040865282},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserLogSmsController\",\"name\":\"短信分析\",\"id\":1260809325749702658},{\"controller\":\"com.roncoo.education.user.service.pc.PcUserOrderLogController\",\"name\":\"支付日志\",\"id\":1260809326462734337},{\"controller\":\"com.roncoo.education.system.service.pc.PcZoneController\",\"name\":\"专区设置\",\"id\":1264735553928556546},{\"controller\":\"com.roncoo.education.system.service.pc.PcZoneController\",\"name\":\"专区设置\",\"id\":1265920753689632770},{\"controller\":\"com.roncoo.education.user.service.pc.PcOrderInfoController\",\"name\":\"课程订单\",\"id\":1266177817438920706},{\"controller\":\"com.roncoo.education.course.service.pc.PcCourseCategoryController\",\"name\":\"分类推荐\",\"id\":1260809272863723522},{\"controller\":\"com.roncoo.education.community.service.pc.PcLabelController\",\"name\":\"标签列表\",\"id\":1260809307189907457},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayRuleController\",\"name\":\"支付方式\",\"id\":1260809322616557569},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteNavController\",\"name\":\"底部文章\",\"id\":1260809277649424385},{\"controller\":\"com.roncoo.education.system.service.pc.PcAdvController\",\"name\":\"轮播广告\",\"id\":1260809308653719553},{\"controller\":\"com.roncoo.education.user.service.pc.PcPayChannelController\",\"name\":\"支付配置\",\"id\":1260809321874165762},{\"controller\":\"com.roncoo.education.system.service.pc.PcWebsiteLinkController\",\"name\":\"友情链接\",\"id\":1260809280451219457},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleZoneCategoryController\",\"name\":\"资讯分栏\",\"id\":1260809309417082882},{\"controller\":\"com.roncoo.education.community.service.pc.PcArticleRecommendController\",\"name\":\"推荐列表\",\"id\":1260809307932299266},{\"controller\":\"com.roncoo.education.user.service.pc.PcLiveChannelController\",\"name\":\"频道管理列表\",\"id\":1264745155168563202},{\"controller\":\"com.roncoo.education.exam.service.pc.PcExamRecommendController\",\"name\":\"推荐管理\",\"id\":1265493622220308481}]";
//        JSONArray permissionArray = JSONUtil.parseArray(permissionStr);
//        for (int i = 0; i < permissionArray.size(); i += 1) {
//            JSONObject object = permissionArray.getJSONObject(i);
//            String className =String.valueOf(object.get("controller"));
//            System.out.println(object.get("name"));
//            setPermissionValue(className, (Long) object.get("id"));
//        }
        String name = "com.roncoo.education.marketing.service.pc.PcActSeckillController";
        Long menuId = 1274222495225290753L;
        setPermissionValue(name, menuId);
    }

    public void setPermissionValue(String className, Long menuId) throws ClassNotFoundException {
        //通过反射获取到类，填入类名
        Class cl1 = Class.forName(className);
        //获取RequestMapping注解
        RequestMapping anno = (RequestMapping) cl1.getAnnotation(RequestMapping.class);
        //获取类注解的value值
        String headUrl = anno.value()[0];

        //获取类中所有的方法
        Method[] methods = cl1.getDeclaredMethods();
        for (Method method : methods) {
            String pValue = headUrl;
            ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
            GetMapping getRequestMothed = method.getAnnotation(GetMapping.class);
            PutMapping putRequestMothed = method.getAnnotation(PutMapping.class);
            PostMapping postRequestMothed = method.getAnnotation(PostMapping.class);
            DeleteMapping deleteRequestMothed = method.getAnnotation(DeleteMapping.class);
            RequestMapping requestMappingMothed = method.getAnnotation(RequestMapping.class);
            //获取类注解的value值
            String pName = null;
            if (apiOperation != null) {
                pName = apiOperation.value();
            }
            if (getRequestMothed != null) {
                pValue = pValue + getRequestMothed.value()[0];
            }
            if (putRequestMothed != null) {
                pValue = pValue + putRequestMothed.value()[0];
            }
            if (postRequestMothed != null) {
                pValue = pValue + postRequestMothed.value()[0];
            }
            if (deleteRequestMothed != null) {
                pValue = pValue + deleteRequestMothed.value()[0];
            }
            if (requestMappingMothed != null) {
                pValue = pValue + requestMappingMothed.value()[0];
            }
            if (StrUtil.isNotEmpty(pName) && StrUtil.isNotEmpty(pValue)) {
                pValue = pValue.replace("/", ":");
                pValue = pValue.substring(1);
                SysPermissionQO record = new SysPermissionQO();
                record.setMenuId(menuId);
                record.setPermissionName(pName);
                record.setPermissionValue(pValue);
                String req = JSONUtil.toJsonStr(record);
                String str = HttpUtil.post("http://10.65.2.97:8710/system/sysPermission/save", req);
                System.out.println(str);
            }
        }
    }
}

