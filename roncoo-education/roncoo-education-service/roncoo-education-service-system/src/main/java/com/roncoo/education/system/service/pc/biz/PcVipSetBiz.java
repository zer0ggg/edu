package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.VipSetEditREQ;
import com.roncoo.education.system.common.req.VipSetListREQ;
import com.roncoo.education.system.common.req.VipSetSaveREQ;
import com.roncoo.education.system.common.req.VipSetUpdateStatusREQ;
import com.roncoo.education.system.common.resp.VipSetListRESP;
import com.roncoo.education.system.common.resp.VipSetViewRESP;
import com.roncoo.education.system.service.dao.VipSetDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 会员设置
 *
 * @author wujing
 */
@Component
public class PcVipSetBiz extends BaseBiz {

    @Autowired
    private VipSetDao dao;

    /**
     * 会员设置列表
     *
     * @param vipSetListREQ 会员设置分页查询参数
     * @return 会员设置分页查询结果
     */
    public Result<Page<VipSetListRESP>> list(VipSetListREQ vipSetListREQ) {
        VipSetExample example = new VipSetExample();
        Criteria c = example.createCriteria();
        if (vipSetListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(vipSetListREQ.getStatusId());
        }
        if (vipSetListREQ.getSetType() != null) {
            c.andSetTypeEqualTo(vipSetListREQ.getSetType());
        }
        example.setOrderByClause(" sort asc, id desc ");
        Page<VipSet> page = dao.listForPage(vipSetListREQ.getPageCurrent(), vipSetListREQ.getPageSize(), example);
        Page<VipSetListRESP> respPage = PageUtil.transform(page, VipSetListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 会员设置添加
     *
     * @param vipSetSaveREQ 会员设置
     * @return 添加结果
     */
    public Result<String> save(VipSetSaveREQ vipSetSaveREQ) {
        // 校验
        if (vipSetSaveREQ.getSetType() == null) {
            throw new BaseException("请选择会员类型");
        }
        check(vipSetSaveREQ);
        VipSet vipSet = dao.getBySetType(vipSetSaveREQ.getSetType());
        if (ObjectUtil.isNotNull(vipSet)) {
            throw new BaseException("已添加该会员");
        }
        VipSet record = BeanUtil.copyProperties(vipSetSaveREQ, VipSet.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 会员设置查看
     *
     * @param id 主键ID
     * @return 会员设置
     */
    public Result<VipSetViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), VipSetViewRESP.class));
    }


    /**
     * 会员设置修改
     *
     * @param vipSetEditREQ 会员设置修改对象
     * @return 修改结果
     */
    public Result<String> edit(VipSetEditREQ vipSetEditREQ) {
        VipSetSaveREQ vipSetSaveREQ = BeanUtil.copyProperties(vipSetEditREQ, VipSetSaveREQ.class);
        // 校验
        check(vipSetSaveREQ);
        VipSet record = BeanUtil.copyProperties(vipSetEditREQ, VipSet.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 会员设置删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 会员设置校验
     *
     * @param vipSetSaveREQ
     */
    private void check(VipSetSaveREQ vipSetSaveREQ) {
        if (vipSetSaveREQ.getOrgPrice() == null) {
            throw new BaseException("请输入原价");
        }
        if (vipSetSaveREQ.getFabPrice() == null) {
            throw new BaseException("请输入优惠价格");
        }
        if (vipSetSaveREQ.getRenewalPrice() == null) {
            throw new BaseException("请输入续费价格");
        }
        if (vipSetSaveREQ.getOrgPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("原价不能为负数");
        }
        if (vipSetSaveREQ.getFabPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("优惠不能为负数");
        }
        if (vipSetSaveREQ.getRenewalPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("续费不能为负数");
        }
        if (vipSetSaveREQ.getFabPrice().compareTo(vipSetSaveREQ.getOrgPrice()) > 0) {
            throw new BaseException("优惠价不能大于原价");
        }
        if (vipSetSaveREQ.getRenewalPrice().compareTo(vipSetSaveREQ.getFabPrice()) > 0) {
            throw new BaseException("续费价不能大于优惠价");
        }
    }

    /**
     * 修改状态
     *
     * @param vipSetUpdateStatusREQ 会员设置修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(VipSetUpdateStatusREQ vipSetUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(vipSetUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(vipSetUpdateStatusREQ.getId()))) {
            return Result.error("会员等级不存在");
        }
        VipSet record = BeanUtil.copyProperties(vipSetUpdateStatusREQ, VipSet.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
