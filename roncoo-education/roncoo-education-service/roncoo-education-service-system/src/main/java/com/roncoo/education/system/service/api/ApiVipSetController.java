package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.dto.VipSetListDTO;
import com.roncoo.education.system.service.api.biz.ApiVipSetBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 会员设置
 *
 * @author wujing
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping(value = "/system/api/vip/set")
public class ApiVipSetController {

	@Autowired
	private ApiVipSetBiz biz;

	/**
	 * 获取会员列表接口
	 *
	 * @param
	 * @return
	 */
	@Cacheable
	@ApiOperation(value = "获取会员列表接口", notes = "获取会员列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<VipSetListDTO> list() {
		return biz.list();
	}
}
