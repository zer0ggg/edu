package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.LoginStatusEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.system.common.req.SysUserAuthenticateREQ;
import com.roncoo.education.system.common.req.SysUserLoginREQ;
import com.roncoo.education.system.common.resp.SysUserLoginRESP;
import com.roncoo.education.system.service.dao.SysUserDao;
import com.roncoo.education.system.service.dao.SysUserLoginLogDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUser;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class PcSysUserLoginBiz extends BaseBiz {

    /**
     * 签名前缀
     */
    private static final String SIGN_PREFIX = "login";

    /**
     * 拼接字符
     */
    private static final String SPLICE_STR = "lingke";

    /**
     * 过期时间 30秒
     */
    private static final Integer EXPIRE_DATE = 120;

    @Autowired
    private SysUserDao userDao;
    @Autowired
    private SysUserLoginLogDao userLoginLogDao;
    @Autowired
    private PcSysUserBiz userBiz;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 用户登录
     *
     * @param loginREQ 登录参数
     * @return 登录结果
     */
    public Result<SysUserLoginRESP> login(SysUserLoginREQ loginREQ, HttpServletRequest request) {
        // 用户校验
        SysUser sysUser = userDao.getByLoginName(loginREQ.getLoginName());
        if (null == sysUser) {
            loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "登录账号不存在");
            return Result.error("账号或密码不正确");
        }
        // 校验状态
        if (!StatusIdEnum.YES.getCode().equals(sysUser.getStatusId())) {
            loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "账号状态不可用：当前状态：" + sysUser.getStatusId());
            return Result.error("账号不可用");
        }
        // 密码校验
        if (!SystemSignUtil.verifySign(loginREQ.getLoginPassword(), loginREQ.getLoginName(), sysUser.getLoginPassword())) {
            loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "密码错误");
            return Result.error("账号或密码不正确");
        }
//        // 校验sign todo 滑动校验
//        String usedRedisValue = myRedisTemplate.get(RedisPreEnum.SYS_USER_MAN_MACHINE_VERIFICATION_USED.getCode().concat(loginREQ.getSign()));
//        if (StrUtil.isNotBlank(usedRedisValue)) {
//            loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "Sign已被使用，不能重复使用");
//            return Result.error("当前签名字符串已使用");
//        }
//        String redisLoginName = myRedisTemplate.get(RedisPreEnum.SYS_USER_MAN_MACHINE_VERIFICATION.getCode().concat(loginREQ.getSign()));
//        if (StrUtil.isBlank(redisLoginName)) {
//            loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "Sign不存在或已失效");
//            return Result.error("当前签名字符串不存在/已失效");
//        }
//		// 判断sign是否属于当前用户
//		if (!sysUser.getLoginName().equals(redisLoginName)) {
//			loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.FAIL, "Sign校验不通过");
//			return Result.error("当前签名字符串无效");
//		}

        // 封装返回数据
        SysUserLoginRESP resp = new SysUserLoginRESP();
        resp.setLoginName(sysUser.getLoginName());
        resp.setNickname(sysUser.getNickname());
        resp.setToken(JWTUtil.createByLoginName(sysUser.getLoginName(), JWTUtil.DATE, Constants.Platform.ADMIN));

        // 获取权限(方法内已经缓存到redis)
        userBiz.getUserPermissionValueList(sysUser);

        // 缓存TOKEN 有效时间1小时
        myRedisTemplate.set(RedisPreEnum.SYS_USER.getCode().concat(sysUser.getLoginName()), resp.getToken(), 1, TimeUnit.DAYS);

        // 标记当前sign已使用 有效时间5分钟
//		myRedisTemplate.set(RedisPreEnum.SYS_USER_MAN_MACHINE_VERIFICATION_USED.getCode().concat(loginREQ.getSign()), sysUser.getLoginName(), 5, TimeUnit.MINUTES);
        loginLogHandle(loginREQ.getLoginName(), request, LoginStatusEnum.SUCCESS, "登录成功");
        return Result.success(resp);
    }

    /**
     * 人机验证
     *
     * @param authenticateREQ 人机验证参数
     * @return 人机验证结果
     */
    public Result<String> manMachineVerification(SysUserAuthenticateREQ authenticateREQ) {
        // 用户校验
        SysUser sysUser = userDao.getByLoginName(authenticateREQ.getLoginName());
        if (null == sysUser) {
            return Result.error("用户不存在");
        }

        // 校验当前签名是否在有效期范围
        Date requestTime = new Date(authenticateREQ.getTimestamp());
        long timeDifference = DateUtil.between(new Date(), DateUtil.offsetSecond(requestTime, EXPIRE_DATE), DateUnit.SECOND, false);
        if (timeDifference < 0) {
            return Result.error("校验请求已失效");
        }

        // 拼接签名原文
        String param = SIGN_PREFIX + SPLICE_STR + authenticateREQ.getLoginName() + SPLICE_STR + authenticateREQ.getTimestamp() + SPLICE_STR + authenticateREQ.getSignatureNonce();
        // MD5签名加密
        String sign = MD5Util.MD5(param);
        if (!sign.equals(authenticateREQ.getSign())) {
            return Result.error("校验不通过");
        }

        // 生成返回随机串
        String resultSign = IdUtil.simpleUUID();

        // 放入缓存 有效期5分钟
        myRedisTemplate.set(RedisPreEnum.SYS_USER_MAN_MACHINE_VERIFICATION.getCode().concat(resultSign), sysUser.getLoginName(), 5, TimeUnit.MINUTES);
        return Result.success(resultSign);
    }

    /**
     * 登录日志处理
     *
     * @param loginName        登录账号
     * @param request          访问请求
     * @param loginStatusEnum  登录状态
     * @param loginDescription 登录描述
     */
    private void loginLogHandle(String loginName, HttpServletRequest request, LoginStatusEnum loginStatusEnum, String loginDescription) {
        SysUserLoginLog loginLog = new SysUserLoginLog();
        try {
            loginLog.setLoginName(loginName);
            loginLog.setLoginStatus(loginStatusEnum.getCode());
            loginLog.setLoginDescription(loginDescription);

            // 获取IP
            String loginIp = IPUtil.getIpAddr(request);
            loginLog.setLoginIp(loginIp);
            userLoginLogDao.save(loginLog);
        } catch (Exception e) {
            logger.error("保存登录日志错误！日志参数：{}", loginLog, e);
        }
    }
}
