package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignVipSet;
import com.roncoo.education.system.feign.qo.VipSetQO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.system.service.feign.biz.FeignVipSetBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 会员设置
 *
 * @author wujing
 */
@RestController
public class FeignVipSetController extends BaseController implements IFeignVipSet {

    @Autowired
    private FeignVipSetBiz biz;

    @Override
    public Page<VipSetVO> listForPage(@RequestBody VipSetQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody VipSetQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody VipSetQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public VipSetVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

    @Override
    public int updateStatusId(@RequestBody VipSetQO qo) {
        return biz.updateStatusId(qo);
    }

    @Override
    public VipSetVO getByVipTypeAndStatusId(VipSetQO qo) {
        return biz.getByVipTypeAndStatusId(qo.getSetType(), qo.getStatusId());
    }

}
