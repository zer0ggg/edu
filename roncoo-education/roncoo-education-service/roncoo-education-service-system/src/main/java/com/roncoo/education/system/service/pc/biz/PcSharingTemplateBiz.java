package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsUseTemplateEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SharingTemplateEditREQ;
import com.roncoo.education.system.common.req.SharingTemplateListREQ;
import com.roncoo.education.system.common.req.SharingTemplateSaveREQ;
import com.roncoo.education.system.common.req.SharingTemplateUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SharingTemplateListRESP;
import com.roncoo.education.system.common.resp.SharingTemplateViewRESP;
import com.roncoo.education.system.service.dao.SharingTemplateDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplate;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 分享图片模板
 *
 * @author wujing
 */
@Component
public class PcSharingTemplateBiz extends BaseBiz {

    @Autowired
    private SharingTemplateDao dao;

    /**
    * 分享图片模板列表
    *
    * @param sharingTemplateListREQ 分享图片模板分页查询参数
    * @return 分享图片模板分页查询结果
    */
    public Result<Page<SharingTemplateListRESP>> list(SharingTemplateListREQ sharingTemplateListREQ) {
        SharingTemplateExample example = new SharingTemplateExample();
        Criteria c = example.createCriteria();
        Page<SharingTemplate> page = dao.listForPage(sharingTemplateListREQ.getPageCurrent(), sharingTemplateListREQ.getPageSize(), example);
        Page<SharingTemplateListRESP> respPage = PageUtil.transform(page, SharingTemplateListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 分享图片模板添加
    *
    * @param req 分享图片模板
    * @return 添加结果
    */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(SharingTemplateSaveREQ req) {
        if (req.getTemplateUrl() == null) {
            return Result.success("请上传模板图片");
        }
        if (req.getTemplateType() == null) {
            return Result.success("请选择模板类型");
        }
        if (req.getPicx() == null) {
            return Result.success("请输入X轴坐标数");
        }
        if (req.getPicy() == null) {
            return Result.success("请输入Y轴坐标数");
        }
        if (req.getIsUseTemplate() == null) {
            return Result.success("请选择是否使用模板");
        }
        SharingTemplate record = BeanUtil.copyProperties(req, SharingTemplate.class);
        if (IsUseTemplateEnum.YES.getCode().equals(req.getIsUseTemplate())) {
            SharingTemplate sharingTemplate = dao.getIsUseTemplateAndTemplateType(req.getIsUseTemplate(), req.getTemplateType());
            if (ObjectUtil.isNotNull(sharingTemplate)) {
                sharingTemplate.setIsUseTemplate(IsUseTemplateEnum.NO.getCode());
                dao.updateById(sharingTemplate);
            }
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 分享图片模板查看
    *
    * @param id 主键ID
    * @return 分享图片模板
    */
    public Result<SharingTemplateViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SharingTemplateViewRESP.class));
    }


    /**
    * 分享图片模板修改
    *
    * @param req 分享图片模板修改对象
    * @return 修改结果
    */
    @Transactional
    public Result<String> edit(SharingTemplateEditREQ req) {
        if (req.getIsUseTemplate() != null || req.getTemplateType() != null) {
            SharingTemplate sharingTemplate = dao.getIsUseTemplateAndTemplateType(req.getIsUseTemplate(), req.getTemplateType());
            if (ObjectUtil.isNotNull(sharingTemplate)) {
                sharingTemplate.setIsUseTemplate(IsUseTemplateEnum.NO.getCode());
                dao.updateById(sharingTemplate);
            }
        }
        SharingTemplate record = BeanUtil.copyProperties(req, SharingTemplate.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 分享图片模板删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> updateStatus(SharingTemplateUpdateStatusREQ req) {
        SharingTemplate sharingTemplate = dao.getById(req.getId());
        if (ObjectUtil.isNull(sharingTemplate)) {
            return Result.error("配置不存在");
        }
        sharingTemplate.setStatusId(req.getStatusId());
        if (dao.updateById(sharingTemplate) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
