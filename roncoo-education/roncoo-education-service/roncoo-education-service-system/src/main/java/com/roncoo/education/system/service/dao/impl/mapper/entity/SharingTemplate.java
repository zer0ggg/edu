package com.roncoo.education.system.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class SharingTemplate implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private String templateUrl;

    private Integer templateType;

    private Integer isUseTemplate;

    private Integer picx;

    private Integer picy;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl == null ? null : templateUrl.trim();
    }

    public Integer getTemplateType() {
        return templateType;
    }

    public void setTemplateType(Integer templateType) {
        this.templateType = templateType;
    }

    public Integer getIsUseTemplate() {
        return isUseTemplate;
    }

    public void setIsUseTemplate(Integer isUseTemplate) {
        this.isUseTemplate = isUseTemplate;
    }

    public Integer getPicx() {
        return picx;
    }

    public void setPicx(Integer picx) {
        this.picx = picx;
    }

    public Integer getPicy() {
        return picy;
    }

    public void setPicy(Integer picy) {
        this.picy = picy;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", templateUrl=").append(templateUrl);
        sb.append(", templateType=").append(templateType);
        sb.append(", isUseTemplate=").append(isUseTemplate);
        sb.append(", picx=").append(picx);
        sb.append(", picy=").append(picy);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}