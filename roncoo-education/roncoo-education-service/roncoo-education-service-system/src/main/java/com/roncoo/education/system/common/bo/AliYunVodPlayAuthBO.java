package com.roncoo.education.system.common.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 阿里云点播播放授权参数
 *
 * @author LYQ
 */
@Data
public class AliYunVodPlayAuthBO implements Serializable {

    private static final long serialVersionUID = -8871501970276822094L;

    /**
     * 播放令牌
     */
    private String token;
}
