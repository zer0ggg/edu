package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysRolePermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysRolePermissionMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统角色权限 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysRolePermissionDaoImpl implements SysRolePermissionDao {

    @Autowired
    private SysRolePermissionMapper mapper;

    @Override
    public int save(SysRolePermission record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysRolePermission record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysRolePermission getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysRolePermission> listForPage(int pageCurrent, int pageSize, SysRolePermissionExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<SysRolePermission> listByRoleId(Long roleId) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        SysRolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysRolePermission> listByPermissionId(Long permissionId) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        SysRolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andPermissionIdEqualTo(permissionId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int countByRoleId(Long roleId) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        SysRolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.countByExample(example);
    }

    @Override
    public int deleteByRoleId(Long roleId) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        SysRolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public List<SysRolePermission> listInRoleIdList(List<Long> roleIdList) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        SysRolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdIn(roleIdList);
        return this.mapper.selectByExample(example);
    }


}
