package com.roncoo.education.system.common.bo;

import com.roncoo.education.common.core.enums.PlatEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 站点信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class WebsiteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "位置(1电脑端，2微信端)不能为空")
    @ApiModelProperty(value = "位置(1电脑端，2微信端)", required = true)
    private Integer platShow = PlatEnum.PC.getCode();
}
