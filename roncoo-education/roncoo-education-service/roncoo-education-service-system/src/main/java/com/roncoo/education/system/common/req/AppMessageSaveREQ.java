package com.roncoo.education.system.common.req;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * app推送
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AppMessageSaveREQ", description="app推送添加")
public class AppMessageSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "请输入标题")
    @ApiModelProperty(value = "标题")
    private String title;

    @NotEmpty(message = "请输入内容")
    @ApiModelProperty(value = "内容")
    private String content;

    @NotNull(message = "产品ID不能为空")
    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @NotNull(message = "请选择跳转分类")
    @ApiModelProperty(value = "跳转分类(1:点播;2:直播,3文库,4试卷,;5:资讯)")
    private Integer jumpCategory;

    @NotNull(message = "请选择是否立即已发送")
    @ApiModelProperty(value = "是否立即已发送(1立即发送;0不发送)")
    private Integer isPromptlySend;
}
