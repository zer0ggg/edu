package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysRoleMenuEditREQ;
import com.roncoo.education.system.common.req.SysRoleMenuListREQ;
import com.roncoo.education.system.common.req.SysRoleMenuSaveREQ;
import com.roncoo.education.system.common.resp.SysRoleMenuListRESP;
import com.roncoo.education.system.common.resp.SysRoleMenuViewRESP;
import com.roncoo.education.system.service.dao.SysRoleMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统角色菜单
 *
 * @author wujing
 */
@Component
public class PcSysRoleMenuBiz extends BaseBiz {

    @Autowired
    private SysRoleMenuDao dao;

    /**
    * 系统角色菜单列表
    *
    * @param sysRoleMenuListREQ 系统角色菜单分页查询参数
    * @return 系统角色菜单分页查询结果
    */
    public Result<Page<SysRoleMenuListRESP>> list(SysRoleMenuListREQ sysRoleMenuListREQ) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        Criteria c = example.createCriteria();
        Page<SysRoleMenu> page = dao.listForPage(sysRoleMenuListREQ.getPageCurrent(), sysRoleMenuListREQ.getPageSize(), example);
        Page<SysRoleMenuListRESP> respPage = PageUtil.transform(page, SysRoleMenuListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 系统角色菜单添加
    *
    * @param sysRoleMenuSaveREQ 系统角色菜单
    * @return 添加结果
    */
    public Result<String> save(SysRoleMenuSaveREQ sysRoleMenuSaveREQ) {
        SysRoleMenu record = BeanUtil.copyProperties(sysRoleMenuSaveREQ, SysRoleMenu.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 系统角色菜单查看
    *
    * @param id 主键ID
    * @return 系统角色菜单
    */
    public Result<SysRoleMenuViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysRoleMenuViewRESP.class));
    }


    /**
    * 系统角色菜单修改
    *
    * @param sysRoleMenuEditREQ 系统角色菜单修改对象
    * @return 修改结果
    */
    public Result<String> edit(SysRoleMenuEditREQ sysRoleMenuEditREQ) {
        SysRoleMenu record = BeanUtil.copyProperties(sysRoleMenuEditREQ, SysRoleMenu.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 系统角色菜单删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
