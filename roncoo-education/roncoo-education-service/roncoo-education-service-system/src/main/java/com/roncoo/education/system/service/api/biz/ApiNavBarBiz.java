package com.roncoo.education.system.service.api.biz;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.NavEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.system.common.bo.NavBarListBO;
import com.roncoo.education.system.common.dto.NavBarDTO;
import com.roncoo.education.system.common.dto.NavBarListDTO;
import com.roncoo.education.system.service.dao.NavBarDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.NavBar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 头部导航
 *
 * @author wuyun
 */
@Component
public class ApiNavBarBiz {

	@Autowired
	private NavBarDao navBarDao;

	public Result<NavBarListDTO> list(NavBarListBO bo) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(bo);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		List<NavBar> list = navBarDao.listByPlatShowAndStatusId(bo.getPlatShow(),StatusIdEnum.YES.getCode());
		for (NavBar nb : list) {
			if (nb.getNavUrl().equals(NavEnum.LINK.getCode())) {
				nb.setNavUrl(nb.getRemark());
			}
		}
		NavBarListDTO dto = new NavBarListDTO();
		dto.setList(ArrayListUtil.copy(list, NavBarDTO.class));
		return Result.success(dto);
	}

}
