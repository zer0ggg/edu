package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneEditREQ", description="专区修改")
public class ZoneEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotEmpty(message = "名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String zoneName;

    @ApiModelProperty(value = "描述")
    private String zoneDesc;

    @ApiModelProperty(value = "展示方式(1横向，2纵向；3轮播; 4推荐)")
    private Integer showType;

    @ApiModelProperty(value = "产品ID",required = true)
    private Long productId;

    @ApiModelProperty(value = "外部链接",required = true)
    private String url;

    @ApiModelProperty(value = "图片",required = true)
    private String img;
}
