package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 系统用户人机验证参数
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SysUserManMachineVerificationREQ", description = "系统用户人机验证参数")
public class SysUserAuthenticateREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "登录账号不能为空")
    @ApiModelProperty(value = "登录账号", required = true)
    private String loginName;

    @NotNull(message = "时间戳不能为空")
    @ApiModelProperty(value = "当前时间戳", required = true)
    private Long timestamp;

    @NotBlank(message = "随机数不能为空")
    @ApiModelProperty(value = "唯一随机数", required = true)
    private String signatureNonce;

    @NotBlank(message = "签名结果串不能为空")
    @ApiModelProperty(value = "签名结果串", required = true)
    private String sign;
}
