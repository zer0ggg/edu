package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 敏感词-更新
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SensitiveWordLibraryUpdateREQ", description="敏感词-更新")
public class SensitiveWordLibraryUpdateREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "敏感词")
    private String badword;
}
