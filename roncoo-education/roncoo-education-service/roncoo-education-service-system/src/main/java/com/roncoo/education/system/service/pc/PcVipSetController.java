package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.VipSetEditREQ;
import com.roncoo.education.system.common.req.VipSetListREQ;
import com.roncoo.education.system.common.req.VipSetSaveREQ;
import com.roncoo.education.system.common.req.VipSetUpdateStatusREQ;
import com.roncoo.education.system.common.resp.VipSetListRESP;
import com.roncoo.education.system.common.resp.VipSetViewRESP;
import com.roncoo.education.system.service.pc.biz.PcVipSetBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 会员设置 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/vip/set")
@Api(value = "system-会员设置", tags = {"system-会员设置"})
public class PcVipSetController {

    @Autowired
    private PcVipSetBiz biz;

    @ApiOperation(value = "会员设置列表", notes = "会员设置列表")
    @PostMapping(value = "/list")
    public Result<Page<VipSetListRESP>> list(@RequestBody VipSetListREQ vipSetListREQ) {
        return biz.list(vipSetListREQ);
    }


    @ApiOperation(value = "会员设置添加", notes = "会员设置添加")
    @SysLog(value = "会员设置添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody VipSetSaveREQ vipSetSaveREQ) {
        return biz.save(vipSetSaveREQ);
    }

    @ApiOperation(value = "会员设置查看", notes = "会员设置查看")
    @GetMapping(value = "/view")
    public Result<VipSetViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "会员设置修改", notes = "会员设置修改")
    @SysLog(value = "会员设置修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody VipSetEditREQ vipSetEditREQ) {
        return biz.edit(vipSetEditREQ);
    }


    @ApiOperation(value = "会员设置删除", notes = "会员设置删除")
    @SysLog(value = "会员设置删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "会员设置状态修改", notes = "会员设置状态修改")
    @SysLog(value = "会员设置状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid VipSetUpdateStatusREQ vipSetUpdateStatusREQ) {
        return biz.updateStatus(vipSetUpdateStatusREQ);
    }
}
