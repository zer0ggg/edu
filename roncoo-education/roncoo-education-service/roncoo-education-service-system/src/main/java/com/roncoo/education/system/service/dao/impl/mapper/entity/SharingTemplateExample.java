package com.roncoo.education.system.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SharingTemplateExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public SharingTemplateExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlIsNull() {
            addCriterion("template_url is null");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlIsNotNull() {
            addCriterion("template_url is not null");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlEqualTo(String value) {
            addCriterion("template_url =", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlNotEqualTo(String value) {
            addCriterion("template_url <>", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlGreaterThan(String value) {
            addCriterion("template_url >", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlGreaterThanOrEqualTo(String value) {
            addCriterion("template_url >=", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlLessThan(String value) {
            addCriterion("template_url <", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlLessThanOrEqualTo(String value) {
            addCriterion("template_url <=", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlLike(String value) {
            addCriterion("template_url like", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlNotLike(String value) {
            addCriterion("template_url not like", value, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlIn(List<String> values) {
            addCriterion("template_url in", values, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlNotIn(List<String> values) {
            addCriterion("template_url not in", values, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlBetween(String value1, String value2) {
            addCriterion("template_url between", value1, value2, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateUrlNotBetween(String value1, String value2) {
            addCriterion("template_url not between", value1, value2, "templateUrl");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeIsNull() {
            addCriterion("template_type is null");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeIsNotNull() {
            addCriterion("template_type is not null");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeEqualTo(Integer value) {
            addCriterion("template_type =", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeNotEqualTo(Integer value) {
            addCriterion("template_type <>", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeGreaterThan(Integer value) {
            addCriterion("template_type >", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("template_type >=", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeLessThan(Integer value) {
            addCriterion("template_type <", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeLessThanOrEqualTo(Integer value) {
            addCriterion("template_type <=", value, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeIn(List<Integer> values) {
            addCriterion("template_type in", values, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeNotIn(List<Integer> values) {
            addCriterion("template_type not in", values, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeBetween(Integer value1, Integer value2) {
            addCriterion("template_type between", value1, value2, "templateType");
            return (Criteria) this;
        }

        public Criteria andTemplateTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("template_type not between", value1, value2, "templateType");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateIsNull() {
            addCriterion("is_use_template is null");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateIsNotNull() {
            addCriterion("is_use_template is not null");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateEqualTo(Integer value) {
            addCriterion("is_use_template =", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateNotEqualTo(Integer value) {
            addCriterion("is_use_template <>", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateGreaterThan(Integer value) {
            addCriterion("is_use_template >", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_use_template >=", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateLessThan(Integer value) {
            addCriterion("is_use_template <", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateLessThanOrEqualTo(Integer value) {
            addCriterion("is_use_template <=", value, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateIn(List<Integer> values) {
            addCriterion("is_use_template in", values, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateNotIn(List<Integer> values) {
            addCriterion("is_use_template not in", values, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateBetween(Integer value1, Integer value2) {
            addCriterion("is_use_template between", value1, value2, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andIsUseTemplateNotBetween(Integer value1, Integer value2) {
            addCriterion("is_use_template not between", value1, value2, "isUseTemplate");
            return (Criteria) this;
        }

        public Criteria andPicxIsNull() {
            addCriterion("picx is null");
            return (Criteria) this;
        }

        public Criteria andPicxIsNotNull() {
            addCriterion("picx is not null");
            return (Criteria) this;
        }

        public Criteria andPicxEqualTo(Integer value) {
            addCriterion("picx =", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxNotEqualTo(Integer value) {
            addCriterion("picx <>", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxGreaterThan(Integer value) {
            addCriterion("picx >", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxGreaterThanOrEqualTo(Integer value) {
            addCriterion("picx >=", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxLessThan(Integer value) {
            addCriterion("picx <", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxLessThanOrEqualTo(Integer value) {
            addCriterion("picx <=", value, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxIn(List<Integer> values) {
            addCriterion("picx in", values, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxNotIn(List<Integer> values) {
            addCriterion("picx not in", values, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxBetween(Integer value1, Integer value2) {
            addCriterion("picx between", value1, value2, "picx");
            return (Criteria) this;
        }

        public Criteria andPicxNotBetween(Integer value1, Integer value2) {
            addCriterion("picx not between", value1, value2, "picx");
            return (Criteria) this;
        }

        public Criteria andPicyIsNull() {
            addCriterion("picy is null");
            return (Criteria) this;
        }

        public Criteria andPicyIsNotNull() {
            addCriterion("picy is not null");
            return (Criteria) this;
        }

        public Criteria andPicyEqualTo(Integer value) {
            addCriterion("picy =", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyNotEqualTo(Integer value) {
            addCriterion("picy <>", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyGreaterThan(Integer value) {
            addCriterion("picy >", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyGreaterThanOrEqualTo(Integer value) {
            addCriterion("picy >=", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyLessThan(Integer value) {
            addCriterion("picy <", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyLessThanOrEqualTo(Integer value) {
            addCriterion("picy <=", value, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyIn(List<Integer> values) {
            addCriterion("picy in", values, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyNotIn(List<Integer> values) {
            addCriterion("picy not in", values, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyBetween(Integer value1, Integer value2) {
            addCriterion("picy between", value1, value2, "picy");
            return (Criteria) this;
        }

        public Criteria andPicyNotBetween(Integer value1, Integer value2) {
            addCriterion("picy not between", value1, value2, "picy");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}