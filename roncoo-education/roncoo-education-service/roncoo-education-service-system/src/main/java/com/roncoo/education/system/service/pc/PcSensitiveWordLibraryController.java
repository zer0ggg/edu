package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SensitiveWordLibraryREQ;
import com.roncoo.education.system.common.req.SensitiveWordLibrarySaveREQ;
import com.roncoo.education.system.common.req.SensitiveWordLibraryUpdateREQ;
import com.roncoo.education.system.common.resp.SensitiveWordLibaryListRESP;
import com.roncoo.education.system.service.pc.biz.PcSensitiveWordLibraryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 敏感词管理
 */
@RestController
@RequestMapping("/system/pc/sensitive/word/library")
@Api(value = "system-敏感词管理", tags = { "system-敏感词管理" })
public class PcSensitiveWordLibraryController {

	@Autowired
	private PcSensitiveWordLibraryBiz biz;

	@ApiOperation(value = "敏感词列出", notes = "敏感词列出")
	@PostMapping(value = "/list")
	public Result<Page<SensitiveWordLibaryListRESP>> list(@RequestBody SensitiveWordLibraryREQ req) {
		return biz.list(req);
	}


	@ApiOperation(value = "敏感词添加", notes = "敏感词添加")
	@SysLog(value = "敏感词添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody SensitiveWordLibrarySaveREQ req) {
		return biz.save(req);
	}


	@ApiOperation(value = "敏感词删除", notes = "敏感词删除")
	@SysLog(value = "敏感词删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}


	@ApiOperation(value = "敏感词更新", notes = "敏感词更新")
	@SysLog(value = "敏感词更新")
	@PutMapping(value = "/update")
	public Result<String> update(@RequestBody SensitiveWordLibraryUpdateREQ req) {
		return biz.update(req);
	}


	@ApiOperation(value = "文档导入敏感词", notes = "文档导入敏感词")
	@SysLog(value = "敏感词文档导入敏感词")
	@PostMapping(value = "/txt/import")
	public Result<String> txtImport(@RequestParam("file") MultipartFile multipartFile) {
		return biz.txtImport(multipartFile);
	}

	@ApiOperation(value = "清空缓存", notes = "清空缓存")
	@SysLog(value = "清空缓存")
	@PutMapping(value = "/delete/redis")
	public Result<String> deleteRedis() {
		return biz.deleteRedis();
	}


}
