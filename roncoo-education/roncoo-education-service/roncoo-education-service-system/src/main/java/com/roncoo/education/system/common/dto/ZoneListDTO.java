package com.roncoo.education.system.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ZoneDTO", description = "专区")
public class ZoneListDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "专区编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "名称")
    private String zoneName;

    @ApiModelProperty(value = "专区分类(1:点播;2:直播,4文库,5试卷,7博客;8:资讯,9:自定义,12:问答)")
    private Integer zoneCategory;

    @ApiModelProperty(value = "描述")
    private String zoneDesc;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;

    @ApiModelProperty(value = "展示方式(1横向，2纵向；3轮播; 4推荐)")
    private Integer showType;

    @ApiModelProperty(value = "产品ID", required = true)
    private Long productId;

    @ApiModelProperty(value = "外部链接", required = true)
    private String url;

    @ApiModelProperty(value = "图片", required = true)
    private String img;

    @ApiModelProperty(value = "课程")
    private CourseDTO CourseDTO;

    @ApiModelProperty(value = "试卷")
    private ExamInfoDTO ExamInfoDTO;

    @ApiModelProperty(value = "博客")
    private BlogViewDTO blogViewDTO;

    //补充信息
    @ApiModelProperty(value = "课程集合")
    private List<CourseDTO> courseList;

    @ApiModelProperty(value = "试卷集合")
    private List<ExamInfoDTO> examList;

    @ApiModelProperty(value = "博客集合")
    private List<BlogViewDTO> blogList;

    @ApiModelProperty(value = "自定义集合")
    private List<ZoneRefDefineDTO> defineList;

    @ApiModelProperty(value = "问答集合")
    private  List<QuestionsDTO> questionsList;

}
