package com.roncoo.education.system.service.api;

import com.roncoo.education.common.talk.callback.CallbackResult;
import com.roncoo.education.course.feign.qo.TalkFunCallbackQO;
import com.roncoo.education.system.service.api.biz.CallbackTalkFunBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 欢拓回调
 *
 * @author LYQ
 */
@RestController
@RequestMapping(value = "/callback/system/talk/fun")
public class CallbackTalkFunController {

    @Autowired
    private CallbackTalkFunBiz biz;

    /**
     * 回调事件通知
     */
    @ApiOperation(value = "欢拓回调通知", notes = "欢拓回调通知")
    @PostMapping(value = "/notify")
    public CallbackResult<String> callbackNotify(TalkFunCallbackQO callbackQO) {
        return biz.callbackNotify(callbackQO);
    }

}
