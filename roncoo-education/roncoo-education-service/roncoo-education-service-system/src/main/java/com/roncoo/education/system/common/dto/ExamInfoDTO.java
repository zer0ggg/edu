package com.roncoo.education.system.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 试卷信息
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamInfoDTO", description="试卷信息")
public class ExamInfoDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "头像")
    private String headImgUrl;

    @ApiModelProperty(value = "试卷排序")
    private Integer examSort;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "年级id")
    private Long gradeId;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "上一级科目ID")
    private Long parentSubjectId;

    @ApiModelProperty(value = "上一级科目名称")
    private String parentSubjectName;

    @ApiModelProperty(value = "上上一级科目ID")
    private Long grantSubjectId;

    @ApiModelProperty(value = "上上一级科目名称")
    private String grantSubjectName;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "答卷时间（分钟）")
    private Integer answerTime;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "试题数量")
    private Integer questionNum;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer studyCount;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionCount;

    @ApiModelProperty(value = "下载人数")
    private Integer downloadCount;

    @ApiModelProperty(value = "个人分类id")
    private Long personalId;
}
