package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignAdv;
import com.roncoo.education.system.feign.qo.AdvQO;
import com.roncoo.education.system.feign.vo.AdvVO;
import com.roncoo.education.system.service.feign.biz.FeignAdvBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广告信息
 *
 * @author wujing
 */
@RestController
public class FeignAdvController extends BaseController implements IFeignAdv {

	@Autowired
	private FeignAdvBiz biz;

	@Override
	public Page<AdvVO> listForPage(@RequestBody AdvQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody AdvQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody AdvQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public AdvVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public int updateStautsId(@RequestBody AdvQO qo) {
		return biz.updateStatusId(qo);
	}

}
