package com.roncoo.education.system.service.dao.impl;

import cn.hutool.core.collection.CollUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysConfigMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 参数配置 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysConfigDaoImpl implements SysConfigDao {

    @Autowired
    private SysConfigMapper mapper;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Override
    public int save(SysConfig record) {
        myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysConfig record) {
        myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysConfig getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysConfig> listForPage(int pageCurrent, int pageSize, SysConfigExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<SysConfig>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public SysConfig getByConfigKey(String configKey) {
        SysConfigExample example = new SysConfigExample();
        Criteria c = example.createCriteria();
        c.andConfigKeyEqualTo(configKey);
        List<SysConfig> list = this.mapper.selectByExample(example);
        if (CollUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public Map<String, String> getConfigMapByStatusId(Integer statusId) {
        @SuppressWarnings("unchecked")
        Map<String, String> configMap = myRedisTemplate.getForJson(RedisPreEnum.SYS_CONFIG.getCode(), HashMap.class);
        if (CollUtil.isEmpty(configMap)) {
            SysConfigExample example = new SysConfigExample();
            Criteria c = example.createCriteria();
            c.andStatusIdEqualTo(statusId);
            List<SysConfig> list = this.mapper.selectByExample(example);
            if (CollUtil.isEmpty(list)) {
                return null;
            }
            configMap = list.stream().collect(Collectors.toMap(SysConfig::getConfigKey, SysConfig::getConfigValue));
            myRedisTemplate.setByJson(RedisPreEnum.SYS_CONFIG.getCode(), configMap, 24 * 60, TimeUnit.MINUTES);// 存一天
        }
        return configMap;
    }

    @Override
    public SysConfig getByConfigKeyAndStatusId(String configKey, Integer statusId) {
        SysConfigExample example = new SysConfigExample();
        Criteria c = example.createCriteria();
        c.andConfigKeyEqualTo(configKey);
        c.andStatusIdEqualTo(statusId);
        List<SysConfig> list = this.mapper.selectByExample(example);
        if (CollUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

}
