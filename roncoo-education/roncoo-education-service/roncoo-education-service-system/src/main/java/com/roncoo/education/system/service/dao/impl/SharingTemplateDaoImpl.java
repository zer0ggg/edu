package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SharingTemplateDao;
import com.roncoo.education.system.service.dao.impl.mapper.SharingTemplateMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplate;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 分享图片模板 服务实现类
 *
 * @author wujing
 * @date 2020-07-16
 */
@Repository
public class SharingTemplateDaoImpl implements SharingTemplateDao {

    @Autowired
    private SharingTemplateMapper mapper;

    @Override
    public int save(SharingTemplate record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SharingTemplate record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SharingTemplate getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SharingTemplate> listForPage(int pageCurrent, int pageSize, SharingTemplateExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public SharingTemplate getIsUseTemplateAndTemplateType(Integer isUseTemplate, Integer templateType) {
        SharingTemplateExample example = new SharingTemplateExample();
        SharingTemplateExample.Criteria c = example.createCriteria();
        c.andIsUseTemplateEqualTo(isUseTemplate);
        c.andTemplateTypeEqualTo(templateType);
        List<SharingTemplate> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
