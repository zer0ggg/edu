package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample;

import java.util.List;

/**
 * 专区关联 服务类
 *
 * @author wujing
 * @date 2020-05-23
 */
public interface ZoneRefDao {

    /**
    * 保存专区关联
    *
    * @param record 专区关联
    * @return 影响记录数
    */
    int save(ZoneRef record);

    /**
    * 根据ID删除专区关联
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 专区关联
    * @return 影响记录数
    */
    int updateById(ZoneRef record);

    /**
    * 根据ID获取专区关联
    *
    * @param id 主键ID
    * @return 专区关联
    */
    ZoneRef getById(Long id);

    /**
    * 专区关联--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ZoneRef> listForPage(int pageCurrent, int pageSize, ZoneRefExample example);

    /**
     * 根据专区编号获取可用的专区信息集合
     *
     * @param zoneId
     * @param statusId
     * @return
     * @author wuyun
     */
    List<ZoneRef> listByZoneIdAndStatusId(Long zoneId, Integer statusId);

    /**
     * 根据专区编号,获取可以专区信息
     *
     * @param zoneId
     * @return
     */
    List<ZoneRef> listByZoneId(Long zoneId);

    /**
     * 根据专区id和关联id
     * @param zoneId
     * @param refId
     * @return
     */
    ZoneRef getZoneIdAndRefId(Long zoneId, Long refId);

    /**
     * 根据专区id和名称
     * @param zoneId
     * @param title
     * @return
     */
    ZoneRef getZoneIdAndTitle(Long zoneId, String title);

}
