package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignRegion;
import com.roncoo.education.system.feign.qo.RegionQO;
import com.roncoo.education.system.feign.vo.RegionVO;
import com.roncoo.education.system.service.feign.biz.FeignRegionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 行政区域表
 *
 * @author wujing
 */
@RestController
public class FeignRegionController extends BaseController implements IFeignRegion {

	@Autowired
	private FeignRegionBiz biz;

	@Override
	public Page<RegionVO> listForPage(@RequestBody RegionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody RegionQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody RegionQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public RegionVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

}
