package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Data
@Accessors(chain = true)
@ApiModel(value = "SysUserLoginREQ", description = "系统用户登录参数")
public class SysUserLoginREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "登录账号不能为空")
    @ApiModelProperty(value = "登录账号", required = true)
    private String loginName;

    @NotBlank(message = "登录密码不能为空")
    @ApiModelProperty(value = "登录密码", required = true)
    private String loginPassword;

//    @NotBlank(message = "签名字符串不能为空")
//    @ApiModelProperty(value = "签名字符串", required = true)
//    private String sign;

}
