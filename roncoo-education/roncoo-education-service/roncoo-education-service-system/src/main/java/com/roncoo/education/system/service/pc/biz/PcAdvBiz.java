package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.system.common.req.AdvEditREQ;
import com.roncoo.education.system.common.req.AdvListREQ;
import com.roncoo.education.system.common.req.AdvSaveREQ;
import com.roncoo.education.system.common.req.AdvUpdateStatusREQ;
import com.roncoo.education.system.common.resp.AdvListRESP;
import com.roncoo.education.system.common.resp.AdvViewRESP;
import com.roncoo.education.system.service.dao.AdvDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Adv;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AdvExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AdvExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 广告信息
 *
 * @author wujing
 */
@Component
public class PcAdvBiz extends BaseBiz {

    @Autowired
    private AdvDao dao;
    @Autowired
    private IFeignCourse feignCourse;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 广告信息列表
     *
     * @param req 广告信息分页查询参数
     * @return 广告信息分页查询结果
     */
    public Result<Page<AdvListRESP>> list(AdvListREQ req) {
        AdvExample example = new AdvExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (!StringUtils.isEmpty(req.getAdvTitle())) {
            c.andAdvTitleLike(PageUtil.like(req.getAdvTitle()));
        }
        if (req.getPlatShow() != null) {
            c.andPlatShowEqualTo(req.getPlatShow());
        }
        if (req.getAdvLocation() != null) {
            c.andAdvLocationEqualTo(req.getAdvLocation());
        }
        if (req.getMobileTerminalCategory() != null) {
            c.andMobileTerminalCategoryEqualTo(req.getMobileTerminalCategory());
        }
        Page<Adv> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<AdvListRESP> respPage = PageUtil.transform(page, AdvListRESP.class);
        for (AdvListRESP adv : respPage.getList()) {
            if (adv.getCourseId() != null) {
                CourseVO courseVO = feignCourse.getById(adv.getCourseId());
                if (courseVO != null) {
                    adv.setCourseName(courseVO.getCourseName());
                }
            }
        }
        return Result.success(respPage);
    }


    /**
     * 广告信息添加
     *
     * @param req 广告信息
     * @return 添加结果
     */
    public Result<String> save(AdvSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (req.getBeginTime().after(req.getEndTime())) {
            return Result.error("开始时间要小于结束时间");
        }
        Adv record = BeanUtil.copyProperties(req, Adv.class);
        record.setStatusId(StatusIdEnum.YES.getCode());
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 广告信息查看
     *
     * @param id 主键ID
     * @return 广告信息
     */
    public Result<AdvViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), AdvViewRESP.class));
    }


    /**
     * 广告信息修改
     *
     * @param req 广告信息修改对象
     * @return 修改结果
     */
    public Result<String> edit(AdvEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Adv oldAct = dao.getById(req.getId());
        if (oldAct == null) {
            return Result.error("广告信息不存在");
        }
        if (req.getBeginTime().after(req.getEndTime())) {
            return Result.error("开始时间要小于结束时间");
        }
        Adv record = BeanUtil.copyProperties(req, Adv.class);
        record.setCourseId(null);
        record.setCourseCategory(null);
        if (req.getCourseId() != null && oldAct.getCourseId() != null && req.getCourseId().longValue() != oldAct.getCourseId().longValue()) {
            CourseVO courseVO = feignCourse.getById(req.getCourseId());
            if (courseVO != null) {
                record.setCourseId(req.getCourseId());
                record.setCourseCategory(courseVO.getCourseCategory());
            }
        }
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 广告信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> updateStatus(AdvUpdateStatusREQ req) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(req.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(req.getId()))) {
            return Result.error("广告不存在");
        }
        Adv record = BeanUtil.copyProperties(req, Adv.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");

    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiAdvController::listAdvBO*");
        return Result.success("删除成功");
    }


}
