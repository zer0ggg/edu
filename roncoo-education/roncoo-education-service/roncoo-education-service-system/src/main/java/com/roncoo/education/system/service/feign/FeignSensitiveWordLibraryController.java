package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.system.feign.interfaces.IFeignSensitiveWordLibrary;
import com.roncoo.education.system.service.feign.biz.FeignSensitiveWordLibraryBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 敏感词信息
 *
 * @author wujing
 */
@RestController
public class FeignSensitiveWordLibraryController extends BaseController implements IFeignSensitiveWordLibrary {

	@Autowired
	private FeignSensitiveWordLibraryBiz biz;


	@Override
	public List<String> listAll() {
		return biz.listAll();
	}
}
