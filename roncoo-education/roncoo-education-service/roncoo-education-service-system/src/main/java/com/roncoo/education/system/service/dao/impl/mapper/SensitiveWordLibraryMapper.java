package com.roncoo.education.system.service.dao.impl.mapper;

import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibrary;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibraryExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SensitiveWordLibraryMapper {
    int countByExample(SensitiveWordLibraryExample example);

    int deleteByExample(SensitiveWordLibraryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SensitiveWordLibrary record);

    int insertSelective(SensitiveWordLibrary record);

    List<SensitiveWordLibrary> selectByExample(SensitiveWordLibraryExample example);

    SensitiveWordLibrary selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SensitiveWordLibrary record, @Param("example") SensitiveWordLibraryExample example);

    int updateByExample(@Param("record") SensitiveWordLibrary record, @Param("example") SensitiveWordLibraryExample example);

    int updateByPrimaryKeySelective(SensitiveWordLibrary record);

    int updateByPrimaryKey(SensitiveWordLibrary record);
}
