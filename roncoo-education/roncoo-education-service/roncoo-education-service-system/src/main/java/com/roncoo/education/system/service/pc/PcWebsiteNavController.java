package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.WebsiteNavEditREQ;
import com.roncoo.education.system.common.req.WebsiteNavListREQ;
import com.roncoo.education.system.common.req.WebsiteNavSaveREQ;
import com.roncoo.education.system.common.resp.WebsiteNavListRESP;
import com.roncoo.education.system.common.resp.WebsiteNavViewRESP;
import com.roncoo.education.system.service.pc.biz.PcWebsiteNavBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 站点导航 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/website/nav")
@Api(value = "system-站点导航", tags = {"system-站点导航"})
public class PcWebsiteNavController {

    @Autowired
    private PcWebsiteNavBiz biz;

    @ApiOperation(value = "站点导航列表", notes = "站点导航列表")
    @PostMapping(value = "/list")
    public Result<Page<WebsiteNavListRESP>> list(@RequestBody WebsiteNavListREQ websiteNavListREQ) {
        return biz.list(websiteNavListREQ);
    }


    @ApiOperation(value = "站点导航添加", notes = "站点导航添加")
    @SysLog(value = "站点导航添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody WebsiteNavSaveREQ websiteNavSaveREQ) {
        return biz.save(websiteNavSaveREQ);
    }

    @ApiOperation(value = "站点导航查看", notes = "站点导航查看")
    @GetMapping(value = "/view")
    public Result<WebsiteNavViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "站点导航修改", notes = "站点导航修改")
    @SysLog(value = "站点导航修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody WebsiteNavEditREQ websiteNavEditREQ) {
        return biz.edit(websiteNavEditREQ);
    }


    @ApiOperation(value = "站点导航删除", notes = "站点导航删除")
    @SysLog(value = "站点导航删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "清空缓存", notes = "清空缓存")
    @SysLog(value = "清空缓存")
    @PutMapping(value = "/delete/redis")
    public Result<String> deleteRedis() {
        return biz.deleteRedis();
    }
}
