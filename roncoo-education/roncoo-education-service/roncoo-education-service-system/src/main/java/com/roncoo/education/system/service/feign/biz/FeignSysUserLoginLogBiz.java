package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysUserLoginLogQO;
import com.roncoo.education.system.feign.vo.SysUserLoginLogVO;
import com.roncoo.education.system.service.dao.SysUserLoginLogDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLog;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统用户登录日志
 *
 * @author LYQ
 */
@Component
public class FeignSysUserLoginLogBiz extends BaseBiz {

    @Autowired
    private SysUserLoginLogDao dao;

	public Page<SysUserLoginLogVO> listForPage(SysUserLoginLogQO qo) {
	    SysUserLoginLogExample example = new SysUserLoginLogExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysUserLoginLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysUserLoginLogVO.class);
	}

	public int save(SysUserLoginLogQO qo) {
		SysUserLoginLog record = BeanUtil.copyProperties(qo, SysUserLoginLog.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysUserLoginLogVO getById(Long id) {
		SysUserLoginLog record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysUserLoginLogVO.class);
	}

	public int updateById(SysUserLoginLogQO qo) {
		SysUserLoginLog record = BeanUtil.copyProperties(qo, SysUserLoginLog.class);
		return dao.updateById(record);
	}

}
