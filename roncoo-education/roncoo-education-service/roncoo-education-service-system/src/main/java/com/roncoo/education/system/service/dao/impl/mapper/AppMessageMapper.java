package com.roncoo.education.system.service.dao.impl.mapper;

import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessage;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AppMessageMapper {
    int countByExample(AppMessageExample example);

    int deleteByExample(AppMessageExample example);

    int deleteByPrimaryKey(Long id);

    int insert(AppMessage record);

    int insertSelective(AppMessage record);

    List<AppMessage> selectByExample(AppMessageExample example);

    AppMessage selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") AppMessage record, @Param("example") AppMessageExample example);

    int updateByExample(@Param("record") AppMessage record, @Param("example") AppMessageExample example);

    int updateByPrimaryKeySelective(AppMessage record);

    int updateByPrimaryKey(AppMessage record);
}