package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysUserRoleEditREQ;
import com.roncoo.education.system.common.req.SysUserRoleListREQ;
import com.roncoo.education.system.common.req.SysUserRoleSaveREQ;
import com.roncoo.education.system.common.resp.SysUserRoleListRESP;
import com.roncoo.education.system.common.resp.SysUserRoleViewRESP;
import com.roncoo.education.system.service.dao.SysUserRoleDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统用户角色
 *
 * @author wujing
 */
@Component
public class PcSysUserRoleBiz extends BaseBiz {

    @Autowired
    private SysUserRoleDao dao;

    /**
    * 系统用户角色列表
    *
    * @param sysUserRoleListREQ 系统用户角色分页查询参数
    * @return 系统用户角色分页查询结果
    */
    public Result<Page<SysUserRoleListRESP>> list(SysUserRoleListREQ sysUserRoleListREQ) {
        SysUserRoleExample example = new SysUserRoleExample();
        Criteria c = example.createCriteria();
        Page<SysUserRole> page = dao.listForPage(sysUserRoleListREQ.getPageCurrent(), sysUserRoleListREQ.getPageSize(), example);
        Page<SysUserRoleListRESP> respPage = PageUtil.transform(page, SysUserRoleListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 系统用户角色添加
    *
    * @param sysUserRoleSaveREQ 系统用户角色
    * @return 添加结果
    */
    public Result<String> save(SysUserRoleSaveREQ sysUserRoleSaveREQ) {
        SysUserRole record = BeanUtil.copyProperties(sysUserRoleSaveREQ, SysUserRole.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 系统用户角色查看
    *
    * @param id 主键ID
    * @return 系统用户角色
    */
    public Result<SysUserRoleViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysUserRoleViewRESP.class));
    }


    /**
    * 系统用户角色修改
    *
    * @param sysUserRoleEditREQ 系统用户角色修改对象
    * @return 修改结果
    */
    public Result<String> edit(SysUserRoleEditREQ sysUserRoleEditREQ) {
        SysUserRole record = BeanUtil.copyProperties(sysUserRoleEditREQ, SysUserRole.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 系统用户角色删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
