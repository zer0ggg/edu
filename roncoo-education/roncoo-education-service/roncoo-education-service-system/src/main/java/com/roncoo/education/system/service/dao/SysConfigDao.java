package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample;

import java.util.Map;

/**
 * 参数配置 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysConfigDao {

	int save(SysConfig record);

	int deleteById(Long id);

	int updateById(SysConfig record);

	SysConfig getById(Long id);

	Page<SysConfig> listForPage(int pageCurrent, int pageSize, SysConfigExample example);

	SysConfig getByConfigKey(String configKey);

	/**
	 * 根据状态信息查找配置
	 *
	 * @param statusId 状态ID
	 * @return configMap
	 */
	Map<String, String> getConfigMapByStatusId(Integer statusId);

	/**
	 * 根据key状态查找配置信息
	 * @param configKey
	 * @param statusId
	 * @return
	 */
	SysConfig getByConfigKeyAndStatusId(String configKey, Integer statusId);
}
