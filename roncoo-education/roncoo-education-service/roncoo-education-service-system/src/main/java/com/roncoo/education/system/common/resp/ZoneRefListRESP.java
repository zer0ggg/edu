package com.roncoo.education.system.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.roncoo.education.system.common.dto.BlogViewDTO;
import com.roncoo.education.system.common.dto.CourseDTO;
import com.roncoo.education.system.common.dto.ExamInfoDTO;
import com.roncoo.education.system.common.dto.QuestionsDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefListRESP", description="专区关联列表")
public class ZoneRefListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "专区ID")
    private Long zoneId;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;

    @ApiModelProperty(value = "关联ID")
    private Long courseId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "外部链接")
    private String url;

    @ApiModelProperty(value = "图片")
    private String img;

    //补充信息
    @ApiModelProperty(value = "点播/直播/文库")
    private CourseDTO course;

    @ApiModelProperty(value = "试卷")
    private ExamInfoDTO exam;

    @ApiModelProperty(value = "博客/资讯")
    private BlogViewDTO blog;

    @ApiModelProperty(value = "问答")
    private QuestionsDTO questions;
}
