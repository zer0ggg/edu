package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.ZoneRefListRESP;
import com.roncoo.education.system.common.resp.ZoneRefViewRESP;
import com.roncoo.education.system.service.pc.biz.PcZoneRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 专区关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/zone/ref")
@Api(value = "system-专区关联", tags = {"system-专区关联"})
public class PcZoneRefController {

    @Autowired
    private PcZoneRefBiz biz;

    @ApiOperation(value = "分页", notes = "分页")
    @PostMapping(value = "/list")
    public Result<Page<ZoneRefListRESP>> list(@RequestBody ZoneRefListREQ req) {
        return biz.list(req);
    }


    @ApiOperation(value = "添加", notes = "添加")
    @SysLog(value = "专区关联修改")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ZoneRefSaveREQ req) {
        return biz.save(req);
    }


    @ApiOperation(value = "批量添加", notes = "批量添加")
    @SysLog(value = "专区关联批量添加")
    @PostMapping(value = "/save/batch")
    public Result<String> saveForBatch(@RequestBody ZoneRefSaveForBatchREQ req) {
        return biz.saveForBatch(req);
    }

    @ApiOperation(value = "查看", notes = "查看")
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public Result<ZoneRefViewRESP> view(@RequestParam Long id)  {
        return biz.view(id);
    }


    @ApiOperation(value = "修改", notes = "修改")
    @SysLog(value = "专区关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ZoneRefEditREQ req) {
        return biz.edit(req);
    }


    @ApiOperation(value = "修改状态", notes = "修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ZoneRefUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }


    @ApiOperation(value = "删除", notes = "删除")
    @SysLog(value = "专区关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


}
