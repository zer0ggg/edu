package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 分享图片模板
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SharingTemplateSaveREQ", description="分享图片模板添加")
public class SharingTemplateSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "模板URL")
    private String templateUrl;

    @ApiModelProperty(value = "模板类型（1用户推荐；2：课程分享）")
    private Integer templateType;

    @ApiModelProperty(value = "是否使用模板（1:是；0:否）")
    private Integer isUseTemplate;

    @ApiModelProperty(value = "x位置")
    private Integer picx;

    @ApiModelProperty(value = "y位置")
    private Integer picy;
}
