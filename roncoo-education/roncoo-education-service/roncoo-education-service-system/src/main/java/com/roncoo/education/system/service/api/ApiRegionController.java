package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.bo.UserRegionCityIdBO;
import com.roncoo.education.system.common.bo.UserRegionLevelBO;
import com.roncoo.education.system.common.bo.UserRegionProvinceBO;
import com.roncoo.education.system.common.dto.RegionListDTO;
import com.roncoo.education.system.service.api.biz.ApiRegionBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 行政区域表
 *
 * @author wujing
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping(value = "/system/api/region/")
public class ApiRegionController extends BaseController {

	@Autowired
	private ApiRegionBiz biz;

	/**
	 * 区域列出接口
	 */
	@Cacheable
	@ApiOperation(value = "区域列出接口", notes = "根据级别获取区域列出信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<RegionListDTO> listForLevel(@RequestBody UserRegionLevelBO userRegionLevelBO) {
		return biz.listForLevel(userRegionLevelBO);
	}

	/**
	 * 区域列出接口
	 */
	@Cacheable
	@ApiOperation(value = "区域列出接口", notes = "根据provinceId获取区域列表信息")
	@RequestMapping(value = "/list/province", method = RequestMethod.POST)
	public Result<RegionListDTO> listForProvince(@RequestBody UserRegionProvinceBO userRegionProvinceBO) {
		return biz.listForProvince(userRegionProvinceBO);
	}

	/**
	 * 区域列出接口
	 */
	@Cacheable
	@ApiOperation(value = "区域列出接口", notes = "根据cityId获取区域列表信息")
	@RequestMapping(value = "/list/city", method = RequestMethod.POST)
	public Result<RegionListDTO> listForCity(@RequestBody UserRegionCityIdBO userRegionCityIdBO) {
		return biz.listForCity(userRegionCityIdBO);
	}

}
