package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.qo.SysConfigQO;
import com.roncoo.education.system.feign.vo.*;
import com.roncoo.education.system.service.feign.biz.FeignSysConfigBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 参数配置
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysConfigController extends BaseController implements IFeignSysConfig {

    @Autowired
    private FeignSysConfigBiz biz;

    @Override
    public Page<SysConfigVO> listForPage(@RequestBody SysConfigQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody SysConfigQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody SysConfigQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public SysConfigVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

    @Override
    public ConfigWebsiteVO getWebsite() {
        return biz.getWebsite();
    }

    @Override
    public ConfigSysVO getSys() {
        return biz.getSys();
    }

    @Override
    public ConfigLivePlatformVO getLivePlatform() {
        return biz.getLivePlatform();
    }

    @Override
    public ConfigTalkFunVO getTalkFun() {
        return biz.getTalkFun();
    }

    @Override
    public ConfigPolyvVO getPolyv() {
        return biz.getPolyv();
    }

    @Override
    public ConfigAliyunVO getAliyun() {
        return biz.getAliyun();
    }

    @Override
    public ConfigVodVO getVod() {
        return biz.getVod();
    }

    @Override
    public ConfigAliYunVodVO getAliYunVod() {
        return biz.getAliYunVod();
    }

    @Override
    public ConfigWeixinVO getWeixin() {
        return biz.getWeixin();
    }

    @Override
    public SysConfigVO getByConfigKey(String configKey) {
        return biz.getByConfigKey(configKey);
    }

    @Override
    public ConfigTencentVO getTencent() {
        return biz.getTencent();
    }
}
