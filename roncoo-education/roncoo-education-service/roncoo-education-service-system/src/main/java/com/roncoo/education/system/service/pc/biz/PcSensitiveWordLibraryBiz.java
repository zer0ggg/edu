package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.system.common.req.SensitiveWordLibraryREQ;
import com.roncoo.education.system.common.req.SensitiveWordLibrarySaveREQ;
import com.roncoo.education.system.common.req.SensitiveWordLibraryUpdateREQ;
import com.roncoo.education.system.common.resp.SensitiveWordLibaryListRESP;
import com.roncoo.education.system.service.dao.SensitiveWordLibraryDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibrary;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibraryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class PcSensitiveWordLibraryBiz extends BaseBiz {

    @Autowired
    private SensitiveWordLibraryDao dao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Result<Page<SensitiveWordLibaryListRESP>> list(SensitiveWordLibraryREQ req) {
        SensitiveWordLibraryExample example = new SensitiveWordLibraryExample();
        SensitiveWordLibraryExample.Criteria criteria = example.createCriteria();
        if (StringUtils.hasText(req.getBadword())) {
            criteria.andBadwordLike(PageUtil.rightLike(req.getBadword()));
        }
        example.setOrderByClause(" id desc");
        return Result.success(PageUtil.transform(dao.listForPage(req.getPageCurrent(), req.getPageSize(), example), SensitiveWordLibaryListRESP.class));
    }

    public Result<String> save(SensitiveWordLibrarySaveREQ req) {
        if (StringUtils.isEmpty(req.getBadword())) {
            return Result.error("请输入敏感词");
        }
        SensitiveWordLibrary sensitiveWordLibrary = dao.getByBadword(req.getBadword());
        if (ObjectUtil.isNotNull(sensitiveWordLibrary)) {
            return Result.error("敏感词已存在，无需重复添加");
        }
        sensitiveWordLibrary = new SensitiveWordLibrary();
        sensitiveWordLibrary.setBadword(req.getBadword());
        if (dao.save(sensitiveWordLibrary) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode());
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    public Result<String> delete(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        if (dao.deleteById(id) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode());
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> update(SensitiveWordLibraryUpdateREQ req) {
        if (req.getId() == null) {
            return Result.error("id不能为空");
        }
        if (StringUtils.isEmpty(req.getBadword())) {
            return Result.error("请输入敏感词");
        }
        SensitiveWordLibrary record = dao.getById(req.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("找不到敏感词信息");
        }
        record.setBadword(req.getBadword());
        if (dao.updateById(record) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode());
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> txtImport(MultipartFile multipartFile) {
        if (multipartFile == null || multipartFile.isEmpty()) {
            return Result.error("请选择txt文本");
        }
        // 获取上传文件的原名
        String fileName = multipartFile.getOriginalFilename();
        // 上传文本的原名+小写+后缀
        if (!fileName.toLowerCase().endsWith("txt")) {
            return Result.error("上传的文本类型不正确");
        }
        // 当作存储到本地的文件名，方便定时任务的处理
        long videoId = IdWorker.getId();

        // 1、上传到本地
        File targetFile = new File(SystemUtil.PIC_PATH + videoId + "." + StrUtil.getSuffix(fileName));
        // targetFile.setLastModified(System.currentTimeMillis());
        // 判断文件目录是否存在，不存在就创建文件目录
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        try {
            multipartFile.transferTo(targetFile);
        } catch (Exception e) {
            logger.error("上传到本地失败", e);
            return Result.error("上传文件出错，请重新上传");
        }
        List<String> badwordList = null;
        try {
            badwordList = readTxtFile(targetFile);
        } catch (IOException e) {
            logger.error("异常", e);
        }
        if (badwordList == null || badwordList.size() <= 0) {
            return Result.error("txt文本无内容，请编辑敏感词");
        }
        for (String badword : badwordList) {
            SensitiveWordLibrary sensitiveWordLibrary = dao.getByBadword(badword);
            if (ObjectUtil.isNull(sensitiveWordLibrary)) {
                sensitiveWordLibrary = new SensitiveWordLibrary();
                sensitiveWordLibrary.setBadword(badword);
                dao.save(sensitiveWordLibrary);
            }
        }
        myRedisTemplate.delete(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode());
        // 4、成功删除本地文件
        if (targetFile.isFile() && targetFile.exists()) {
            if (!targetFile.delete()) {
                logger.error("删除本地文件失败");
            }
        }
        return Result.success("操作成功");

    }

    public List<String> readTxtFile(File file) throws IOException {
        BufferedReader reader = null;
        try {
            String encoding = "utf-8";
            List<String> list = new ArrayList<>();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
            String lineTxt;
            while ((lineTxt = reader.readLine()) != null) {
                list.add(lineTxt);
            }
            return list;
        } catch (Exception e) {
            logger.error("读取文件内容出错");
            return null;
        } finally {
            if (null != reader) {
                reader.close();
            }
        }
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete(RedisPreEnum.SENSITIVE_WORD_LIBRARY.getCode());
        return Result.success("删除成功");
    }

}
