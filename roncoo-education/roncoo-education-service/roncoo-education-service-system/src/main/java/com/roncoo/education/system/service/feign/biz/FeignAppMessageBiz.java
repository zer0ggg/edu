package com.roncoo.education.system.service.feign.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.AppMessageQO;
import com.roncoo.education.system.feign.vo.AppMessageVO;
import com.roncoo.education.system.service.dao.AppMessageDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessage;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample.Criteria;

/**
 * app推送
 *
 * @author wujing
 */
@Component
public class FeignAppMessageBiz extends BaseBiz {

    @Autowired
    private AppMessageDao dao;

	public Page<AppMessageVO> listForPage(AppMessageQO qo) {
	    AppMessageExample example = new AppMessageExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<AppMessage> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, AppMessageVO.class);
	}

	public int save(AppMessageQO qo) {
		AppMessage record = BeanUtil.copyProperties(qo, AppMessage.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public AppMessageVO getById(Long id) {
		AppMessage record = dao.getById(id);
		return BeanUtil.copyProperties(record, AppMessageVO.class);
	}

	public int updateById(AppMessageQO qo) {
		AppMessage record = BeanUtil.copyProperties(qo, AppMessage.class);
		return dao.updateById(record);
	}

}
