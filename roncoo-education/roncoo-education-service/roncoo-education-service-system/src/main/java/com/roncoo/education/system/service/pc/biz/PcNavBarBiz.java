package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.NavEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.system.common.req.NavBarEditREQ;
import com.roncoo.education.system.common.req.NavBarListREQ;
import com.roncoo.education.system.common.req.NavBarSaveREQ;
import com.roncoo.education.system.common.resp.NavBarListRESP;
import com.roncoo.education.system.common.resp.NavBarViewRESP;
import com.roncoo.education.system.service.dao.NavBarDao;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.NavBar;
import com.roncoo.education.system.service.dao.impl.mapper.entity.NavBarExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.NavBarExample.Criteria;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 头部导航
 *
 * @author wujing
 */
@Component
public class PcNavBarBiz extends BaseBiz {

    @Autowired
    private NavBarDao dao;
    @Autowired
    private SysConfigDao sysConfigDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 头部导航列表
     *
     * @param req 头部导航分页查询参数
     * @return 头部导航分页查询结果
     */
    public Result<Page<NavBarListRESP>> list(NavBarListREQ req) {
        NavBarExample example = new NavBarExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (req.getPlatShow() != null) {
            c.andPlatShowEqualTo(req.getPlatShow());
        }
        if (!StringUtils.isEmpty(req.getNavTitle())) {
            c.andNavTitleLike(PageUtil.like(req.getNavTitle()));
        }
        example.setOrderByClause(" sort asc, id desc ");
        Page<NavBar> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<NavBarListRESP> respPage = PageUtil.transform(page, NavBarListRESP.class);
        // 拼接跳转地址
        for (NavBarListRESP resp : respPage.getList()) {
            String navUrl = resp.getNavUrl();
            if (NavEnum.LINK.getCode().equals(navUrl) && !StringUtils.isEmpty(resp.getRemark())) {
                // 外部链接且已有地址,则跳过
            } else {
                // 拼接平台url
                SysConfig sysConfig = sysConfigDao.getByConfigKey("domain");
                if (!ObjectUtils.isEmpty(sysConfig)) {
                    resp.setRemark(sysConfig.getConfigValue() + resp.getNavUrl());
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 头部导航添加
     *
     * @param navBarSaveREQ 头部导航
     * @return 添加结果
     */
    public Result<String> save(NavBarSaveREQ navBarSaveREQ) {
        NavBar record = BeanUtil.copyProperties(navBarSaveREQ, NavBar.class);
        record.setStatusId(StatusIdEnum.YES.getCode());
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 头部导航查看
     *
     * @param id 主键ID
     * @return 头部导航
     */
    public Result<NavBarViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), NavBarViewRESP.class));
    }

    /**
     * 头部导航修改
     *
     * @param req 头部导航修改对象
     * @return 修改结果
     */
    public Result<String> edit(NavBarEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        NavBar record = BeanUtil.copyProperties(req, NavBar.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 头部导航删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public String emptyRedis() {
        // 不能直接清空
        return "清空失败!";
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiNavBarController::listNavBarListBO*");
        return Result.success("删除成功");
    }
}
