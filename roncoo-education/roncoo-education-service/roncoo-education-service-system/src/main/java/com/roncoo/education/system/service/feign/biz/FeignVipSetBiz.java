package com.roncoo.education.system.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.VipSetQO;
import com.roncoo.education.system.feign.vo.VipSetVO;
import com.roncoo.education.system.service.dao.VipSetDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 会员设置
 *
 * @author wujing
 */
@Component
public class FeignVipSetBiz {

    @Autowired
    private VipSetDao dao;

    public Page<VipSetVO> listForPage(VipSetQO qo) {
        VipSetExample example = new VipSetExample();
        Criteria c = example.createCriteria();
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }
        example.setOrderByClause(" id desc ");
        Page<VipSet> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, VipSetVO.class);
    }

    public int save(VipSetQO qo) {
        // 校验
        if (qo.getSetType() == null) {
            throw new BaseException("请选择会员类型");
        }
        check(qo);
        VipSet vipSet = dao.getBySetType(qo.getSetType());
        if (ObjectUtil.isNotNull(vipSet)) {
            throw new BaseException("已添加该会员");
        }
        VipSet record = BeanUtil.copyProperties(qo, VipSet.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public VipSetVO getById(Long id) {
        VipSet record = dao.getById(id);
        return BeanUtil.copyProperties(record, VipSetVO.class);
    }

    public int updateById(VipSetQO qo) {
        // 校验
        check(qo);
        VipSet record = BeanUtil.copyProperties(qo, VipSet.class);
        return dao.updateById(record);
    }

    public int updateStatusId(VipSetQO qo) {
        VipSet vipSet = new VipSet();
        vipSet.setId(qo.getId());
        if (qo.getStatusId() != null) {
            vipSet.setStatusId(qo.getStatusId());
        }
        return dao.updateById(vipSet);
    }

    private void check(VipSetQO qo) {
        if (qo.getOrgPrice() == null) {
            throw new BaseException("请输入原价");
        }
        if (qo.getFabPrice() == null) {
            throw new BaseException("请输入优惠价格");
        }
        if (qo.getRenewalPrice() == null) {
            throw new BaseException("请输入续费价格");
        }
        if (qo.getOrgPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("原价不能为负数");
        }
        if (qo.getFabPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("优惠不能为负数");
        }
        if (qo.getRenewalPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BaseException("续费不能为负数");
        }
        if (qo.getFabPrice().compareTo(qo.getOrgPrice()) > 0) {
            throw new BaseException("优惠价不能大于原价");
        }
        if (qo.getRenewalPrice().compareTo(qo.getFabPrice()) > 0) {
            throw new BaseException("续费价不能大于优惠价");
        }
    }

    public VipSetVO getByVipTypeAndStatusId(Integer vipType, Integer code) {
        VipSet record = dao.getByVipTypeAndStatusId(vipType, code);
        return BeanUtil.copyProperties(record, VipSetVO.class);
    }
}
