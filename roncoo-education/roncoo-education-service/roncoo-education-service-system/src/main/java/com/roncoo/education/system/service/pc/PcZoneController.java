package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.ZoneEditREQ;
import com.roncoo.education.system.common.req.ZoneListREQ;
import com.roncoo.education.system.common.req.ZoneSaveREQ;
import com.roncoo.education.system.common.req.ZoneUpdateStatusREQ;
import com.roncoo.education.system.common.resp.ZoneListRESP;
import com.roncoo.education.system.common.resp.ZoneViewRESP;
import com.roncoo.education.system.service.pc.biz.PcZoneBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 专区 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/zone")
@Api(value = "system-专区", tags = {"system-专区"})
public class PcZoneController {

    @Autowired
    private PcZoneBiz biz;

    @ApiOperation(value = "分页", notes = "分页")
    @PostMapping(value = "/list")
    public Result<Page<ZoneListRESP>> list(@RequestBody ZoneListREQ req) {
        return biz.list(req);
    }


    @ApiOperation(value = "添加", notes = "添加")
    @SysLog(value = "专区添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ZoneSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "查看", notes = "查看")
    @GetMapping(value = "/view")
    public Result<ZoneViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "修改", notes = "修改")
    @SysLog(value = "专区修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ZoneEditREQ req) {
        return biz.edit(req);
    }


    @ApiOperation(value = "修改状态", notes = "修改状态")
    @SysLog(value = "专区修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ZoneUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }


    @ApiOperation(value = "删除", notes = "删除")
    @SysLog(value = "专区删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "清空缓存", notes = "清空缓存")
    @SysLog(value = "清空缓存")
    @PutMapping(value = "/delete/redis")
    public Result<String> deleteRedis() {
        return biz.deleteRedis();
    }


}
