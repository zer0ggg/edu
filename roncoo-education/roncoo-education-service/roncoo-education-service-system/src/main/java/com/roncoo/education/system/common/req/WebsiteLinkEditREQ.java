package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 站点友情链接
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="WebsiteLinkEditREQ", description="站点友情链接修改")
public class WebsiteLinkEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "名称")
    private String linkName;

    @ApiModelProperty(value = "链接")
    private String linkUrl;

    @ApiModelProperty(value = "跳转方式(_blank，_self)")
    private String linkTarget;
}
