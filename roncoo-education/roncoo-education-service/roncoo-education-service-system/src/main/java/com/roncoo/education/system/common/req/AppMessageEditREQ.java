package com.roncoo.education.system.common.req;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * app推送
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AppMessageEditREQ", description="app推送修改")
public class AppMessageEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "跳转分类(1:点播;2:直播,3文库,4试卷,;5:资讯)")
    private Integer jumpCategory;

    @NotNull(message = "请选择是否立即已发送")
    @ApiModelProperty(value = "是否立即已发送(1立即发送;0不发送)")
    private Integer isPromptlySend;

}
