package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SysConfigEditREQ;
import com.roncoo.education.system.common.req.SysConfigListREQ;
import com.roncoo.education.system.common.req.SysConfigSaveREQ;
import com.roncoo.education.system.common.req.SysConfigUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysConfigListRESP;
import com.roncoo.education.system.common.resp.SysConfigViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysConfigBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 参数配置 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/config")
@Api(value = "system-参数配置", tags = {"system-参数配置"})
public class PcSysConfigController {

    @Autowired
    private PcSysConfigBiz biz;

    @ApiOperation(value = "参数配置列表", notes = "参数配置列表")
    @PostMapping(value = "/list")
    public Result<Page<SysConfigListRESP>> list(@RequestBody SysConfigListREQ sysConfigListREQ) {
        return biz.list(sysConfigListREQ);
    }


    @ApiOperation(value = "参数配置添加", notes = "参数配置添加")
    @SysLog(value = "参数配置添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody SysConfigSaveREQ sysConfigSaveREQ) {
        return biz.save(sysConfigSaveREQ);
    }

    @ApiOperation(value = "参数配置查看", notes = "参数配置查看")
    @GetMapping(value = "/view")
    public Result<SysConfigViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "参数配置修改", notes = "参数配置修改")
    @SysLog(value = "参数配置修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody SysConfigEditREQ sysConfigEditREQ) {
        return biz.edit(sysConfigEditREQ);
    }


    @ApiOperation(value = "参数配置删除", notes = "参数配置删除")
    @SysLog(value = "参数配置删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "参数配置状态修改", notes = "参数配置状态修改")
    @SysLog(value = "参数配置状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid SysConfigUpdateStatusREQ sysConfigUpdateStatusREQ) {
        return biz.updateStatus(sysConfigUpdateStatusREQ);
    }

    @ApiOperation(value = "清空缓存", notes = "清空缓存")
    @SysLog(value = "清空缓存")
    @PutMapping(value = "/delete/redis")
    public Result<String> deleteRedis() {
        return biz.deleteRedis();
    }
}
