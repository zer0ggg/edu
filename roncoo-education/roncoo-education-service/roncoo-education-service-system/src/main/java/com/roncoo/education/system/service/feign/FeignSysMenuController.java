package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysMenu;
import com.roncoo.education.system.feign.qo.SysMenuQO;
import com.roncoo.education.system.feign.vo.SysMenuVO;
import com.roncoo.education.system.service.feign.biz.FeignSysMenuBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统菜单
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysMenuController extends BaseController implements IFeignSysMenu{

    @Autowired
    private FeignSysMenuBiz biz;

	@Override
	public Page<SysMenuVO> listForPage(@RequestBody SysMenuQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysMenuQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysMenuQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysMenuVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
