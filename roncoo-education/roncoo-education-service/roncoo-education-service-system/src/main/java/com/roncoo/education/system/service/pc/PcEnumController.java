package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.EnumUtil;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * 获取枚举
 *
 * @author Quanf
 */
@RestController
@RequestMapping(value = "/system/pc/api/enum")
@Slf4j
@Api(value = "对外-系统枚举获取接口", tags = {"对外-系统枚举获取接口"})
public class PcEnumController extends BaseController {

    @GetMapping(value = "/list/{enumName}")
    public Result<ArrayList> getEnumInfo(@PathVariable(name = "enumName") String enumName) {
        if (enumName == null) {
            return Result.error("枚举[" + enumName + "]不存在");
        }
        log.info("获取枚举：{}", enumName);
        String className = "com.roncoo.education.common.core.enums." + enumName;
        try {
            Class clazz = Class.forName(className);
            if (clazz == null) {
                return Result.error("找不到该枚举信息");
            }
            return Result.success(new ArrayList<>(EnumUtil.toMapList(clazz)));
        } catch (ClassNotFoundException e) {
            log.info("{}", e);
            return Result.error("获取枚举失败");
        }
    }
}
