/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.polyv.PolyvAuth;
import com.roncoo.education.common.polyv.live.result.PLChannelgetLiveStatus;
import com.roncoo.education.course.feign.qo.PolyvVideoQO;
import com.roncoo.education.system.service.api.biz.CallbackPolyvBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * 保利威视-回调
 *
 * @author wujing123
 */
@RestController
@RequestMapping(value = "/callback/system/polyv")
public class CallbackPolyvController extends BaseController {

	@Autowired
	private CallbackPolyvBiz biz;

	/**
	 * 保利威视，视频上传回调接口
	 */
	@ApiOperation(value = "保利威视，视频上传回调接口", notes = "保利威视，视频上传回调接口")
	@RequestMapping(value = "/video", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackPolyvVideo(PolyvVideoQO polyvVideo) {
		return biz.video(polyvVideo);
	}

	/**
	 * 保利威视，视频授权播放回调接口
	 */
	@ApiOperation(value = "保利威视，视频授权播放回调接口", notes = "保利威视，视频授权播放回调接口")
	@RequestMapping(value = "/auth", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackPolyvAuth(PolyvAuth polyvAuth, HttpServletRequest request) {
		return biz.auth(polyvAuth, request);
	}

	/**
	 * 保利威视，直播状态回调接口
	 */
	@ApiOperation(value = "保利威视，直播状态回调接口", notes = "保利威视，直播状态回调接口")
	@RequestMapping(value = "/live/status", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackPolyvLiveStatus(PLChannelgetLiveStatus plChannelgetLiveStatus) {
		return biz.callbackPolyvLiveStatus(plChannelgetLiveStatus);
	}

	/**
	 * 保利威视，视频授权播放接口
	 */
	@ApiOperation(value = "保利威视，视频授权播放接口", notes = "保利威视，视频授权播放接口")
	@RequestMapping(value = "/live/auth", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackPolyvLiveAuth(String userid, Long ts, String token) {
		return biz.callbackPolyvLiveAuth(userid, ts, token);
	}

	/**
	 *
	 * 保利威视，回放生成回调通知接口
	 */
	@ApiOperation(value = "保利威视，回放生成回调通知接口", notes = "保利威视，回放生成回调通知接口")
	@RequestMapping(value = "/live/url", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackLiveFileUrl(String channelId, String fileUrl, String fileId) {
		return biz.callbackLiveFileUrl(channelId, fileUrl, fileId);
	}

	/**
	 *
	 * 保利威视，转存成功回调通知接口
	 */
	@ApiOperation(value = "保利威视，转存成功回调通知接口", notes = "保利威视，转存成功回调通知接口")
	@RequestMapping(value = "/live/save", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackLiveSave(String channelId, String vid, String title, String duration, String sessionId, String videoId) {
		return biz.callbackLiveSave(channelId, vid, title, duration, sessionId, videoId);
	}

	/**
	 * 保利威视，重制课件完成回调通知接口
	 * @param channelId 频道号
	 * @param title 重制对应的回放名称
	 * @param remainDay 重制剩余的有效期
	 * @param url 重制视频下载地址，会有防盗链，有效期为24小时
	 * @return
	 */
	@ApiOperation(value = "保利威视，重制课件完成回调通知接口", notes = "保利威视，重制课件完成回调通知接口")
	@RequestMapping(value = "/live/remakes", method = { RequestMethod.POST, RequestMethod.GET })
	public String callbackLiveRemakes(String channelId, String title, String remainDay, String url, String sessionId) {
		return biz.callbackLiveRemakes(channelId, title, remainDay, url, sessionId);
	}

}
