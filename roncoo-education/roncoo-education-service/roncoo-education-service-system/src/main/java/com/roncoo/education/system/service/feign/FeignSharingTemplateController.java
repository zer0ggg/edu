package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSharingTemplate;
import com.roncoo.education.system.feign.qo.SharingTemplateQO;
import com.roncoo.education.system.feign.vo.SharingTemplateVO;
import com.roncoo.education.system.service.feign.biz.FeignSharingTemplateBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分享图片模板
 *
 * @author wujing
 * @date 2020-07-16
 */
@RestController
public class FeignSharingTemplateController extends BaseController implements IFeignSharingTemplate{

    @Autowired
    private FeignSharingTemplateBiz biz;

	@Override
	public Page<SharingTemplateVO> listForPage(@RequestBody SharingTemplateQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SharingTemplateQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SharingTemplateQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SharingTemplateVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public SharingTemplateVO getIsUseTemplateAndTemplateType(@RequestBody SharingTemplateQO qo) {
		return biz.getIsUseTemplateAndTemplateType(qo);
	}
}
