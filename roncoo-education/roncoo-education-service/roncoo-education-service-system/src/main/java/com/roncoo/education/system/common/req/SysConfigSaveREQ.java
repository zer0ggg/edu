package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 参数配置
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysConfigSaveREQ", description="参数配置添加")
public class SysConfigSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "参数名称")
    private String configName;

    @ApiModelProperty(value = "参数键名")
    private String configKey;

    @ApiModelProperty(value = "参数键值")
    private String configValue;

    @ApiModelProperty(value = "允许删除(1:允许;0:不允许)")
    private Integer allowDelete;

    @ApiModelProperty(value = "配置类型(1:文本;2:富文本,3图片，4：开关)")
    private Integer configType;

    @ApiModelProperty(value = "配置信息分类（1站点信息，2系统信息，3阿里云信息，4视频信息，5微信信息，6小程序）")
    private Integer infoCategory;

    @ApiModelProperty(value = "是否隐藏(1:隐藏;0:不隐藏)")
    private Integer isHidden;

}
