package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysUser;
import com.roncoo.education.system.feign.qo.SysUserQO;
import com.roncoo.education.system.feign.vo.SysUserVO;
import com.roncoo.education.system.service.feign.biz.FeignSysUserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysUserController extends BaseController implements IFeignSysUser{

    @Autowired
    private FeignSysUserBiz biz;

	@Override
	public Page<SysUserVO> listForPage(@RequestBody SysUserQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysUserQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysUserQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysUserVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
