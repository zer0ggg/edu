package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.NavBarEditREQ;
import com.roncoo.education.system.common.req.NavBarListREQ;
import com.roncoo.education.system.common.req.NavBarSaveREQ;
import com.roncoo.education.system.common.resp.NavBarListRESP;
import com.roncoo.education.system.common.resp.NavBarViewRESP;
import com.roncoo.education.system.service.pc.biz.PcNavBarBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 头部导航 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/nav/bar")
@Api(value = "system-头部导航", tags = { "system-头部导航" })
public class PcNavBarController {

	@Autowired
	private PcNavBarBiz biz;

	@ApiOperation(value = "头部导航列表", notes = "头部导航列表")
	@PostMapping(value = "/list")
	public Result<Page<NavBarListRESP>> list(@RequestBody NavBarListREQ navBarListREQ) {
		return biz.list(navBarListREQ);
	}


	@ApiOperation(value = "头部导航添加", notes = "头部导航添加")
	@SysLog(value = "头部导航添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody NavBarSaveREQ navBarSaveREQ) {
		return biz.save(navBarSaveREQ);
	}

	@ApiOperation(value = "头部导航查看", notes = "头部导航查看")
	@GetMapping(value = "/view")
	public Result<NavBarViewRESP> view(@RequestParam Long id) {
		return biz.view(id);
	}


	@ApiOperation(value = "头部导航修改", notes = "头部导航修改")
	@SysLog(value = "头部导航修改")
	@PutMapping(value = "/edit")
	public Result<String> edit(@RequestBody NavBarEditREQ navBarEditREQ) {
		return biz.edit(navBarEditREQ);
	}


	@ApiOperation(value = "头部导航删除", notes = "头部导航删除")
	@SysLog(value = "头部导航删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}


	@ApiOperation(value = "清空缓存", notes = "清空缓存")
	@SysLog(value = "头部导航清空缓存")
	@RequestMapping(value = "/empty/redis")
	public Result<String> emptyRedis() {
		return Result.success(biz.emptyRedis());
	}


	@ApiOperation(value = "清空缓存", notes = "清空缓存")
	@SysLog(value = "清空缓存")
	@PutMapping(value = "/delete/redis")
	public Result<String> deleteRedis() {
		return biz.deleteRedis();
	}

}
