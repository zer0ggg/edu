package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.ZoneCategoryEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.LiveCourseVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.system.common.req.ZoneEditREQ;
import com.roncoo.education.system.common.req.ZoneListREQ;
import com.roncoo.education.system.common.req.ZoneSaveREQ;
import com.roncoo.education.system.common.req.ZoneUpdateStatusREQ;
import com.roncoo.education.system.common.resp.ZoneListRESP;
import com.roncoo.education.system.common.resp.ZoneViewRESP;
import com.roncoo.education.system.service.dao.ZoneDao;
import com.roncoo.education.system.service.dao.ZoneRefDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample.Criteria;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 专区
 *
 * @author wujing
 */
@Component
public class PcZoneBiz extends BaseBiz {

    @Autowired
    private ZoneDao dao;
    @Autowired
    private ZoneRefDao zoneRefDao;

    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignBlog feignBlog;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 专区列表
     *
     * @param req 专区分页查询参数
     * @return 专区分页查询结果
     */
    public Result<Page<ZoneListRESP>> list(ZoneListREQ req) {
        ZoneExample example = new ZoneExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (!StringUtils.isEmpty(req.getZoneName())) {
            c.andZoneNameLike(PageUtil.like(req.getZoneName()));
        }
        if (req.getZoneCategory() != null) {
            c.andZoneCategoryEqualTo(req.getZoneCategory());
        }
        if (req.getZoneLocation() != null) {
            c.andZoneLocationEqualTo(req.getZoneLocation());
        }
        example.setOrderByClause("sort asc, status_id desc, id desc");
        Page<Zone> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ZoneListRESP> respPage = PageUtil.transform(page, ZoneListRESP.class);
        for (ZoneListRESP resp : respPage.getList()) {
            if (resp.getProductId() != null && ZoneCategoryEnum.LIVE.getCode().equals(resp.getZoneCategory())) {
                LiveCourseVO liveCourse = feignCourse.getLiveCourseWithTimeByIdAndStatusId(resp.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(liveCourse)) {
                    resp.setProductName(liveCourse.getCourseName());
                }
            }
            if (resp.getProductId() != null && ZoneCategoryEnum.ORDINARY.getCode().equals(resp.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(resp.getZoneCategory())) {
                CourseVO course = feignCourse.getByIdAndStatusId(resp.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(course)) {
                    resp.setProductName(course.getCourseName());
                }
            } else if (resp.getProductId() != null && ZoneCategoryEnum.EXAM.getCode().equals(resp.getZoneCategory())) {
                ExamInfoVO exam = feignExamInfo.getByIdAndStatusId(resp.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(exam)) {
                    resp.setProductName(exam.getExamName());
                }
            } else if (resp.getProductId() != null && ZoneCategoryEnum.INFOMATION.getCode().equals(resp.getZoneCategory()) || ZoneCategoryEnum.Blog.getCode().equals(resp.getZoneCategory())) {
                BlogVO blog = feignBlog.getByIdAndStatusId(resp.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(blog)) {
                    resp.setProductName(blog.getTitle());
                }
            }
        }
        return Result.success(respPage);
    }


    /**
     * 专区添加
     *
     * @param req 专区
     * @return 添加结果
     */
    public Result<String> save(ZoneSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //防止名称重复
        Zone oldRecord = dao.getByZoneLocationAndzoneCategoryAndZoneName(req.getZoneLocation(), req.getZoneCategory(), req.getZoneName());
        if (!ObjectUtils.isEmpty(oldRecord)) {
            return Result.error("专区名称不能重复");
        }
        Zone record = BeanUtil.copyProperties(req, Zone.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 专区查看
     *
     * @param id 主键ID
     * @return 专区
     */
    public Result<ZoneViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ZoneViewRESP.class));
    }


    /**
     * 专区修改
     *
     * @param req 专区修改对象
     * @return 修改结果
     */
    public Result<String> edit(ZoneEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Zone record = dao.getById(req.getId());
        if (ObjectUtils.isEmpty(record)) {
            return Result.error("主键id不正确");
        }
        //防止名称重复
        Zone oldRecord = dao.getByZoneLocationAndzoneCategoryAndZoneName(record.getZoneLocation(), record.getZoneCategory(), req.getZoneName());
        if (!ObjectUtils.isEmpty(oldRecord) && oldRecord.getId().longValue() != req.getId().longValue()) {
            return Result.error("名称重复，请检查!");
        }
        record = BeanUtil.copyProperties(req, Zone.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 专区删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        List<ZoneRef> refList = zoneRefDao.listByZoneId(id);
        if (!CollectionUtils.isEmpty(refList)) {
            return Result.error("专区下关联记录，不可删除该专区");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> updateStatus(ZoneUpdateStatusREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
            return Result.error("枚举值不正确");
        }
        Zone record = dao.getById(req.getId());
        if (ObjectUtil.isNull(record)) {
            return Result.error("找不到专区信息");
        }
        Zone bean = BeanUtil.copyProperties(req, Zone.class);
        if (dao.updateById(bean) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiZoneController::listForPageZoneListBO*");
        return Result.success("删除成功");
    }

}
