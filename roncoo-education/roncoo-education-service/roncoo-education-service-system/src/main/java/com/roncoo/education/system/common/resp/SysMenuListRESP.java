package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统菜单
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysMenuListRESP", description="系统菜单列表")
public class SysMenuListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "菜单类型(0:目录;1菜单)")
    private Integer menuType;

    @ApiModelProperty(value = "路由地址")
    private String routerUrl;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "是否外链(1:是 ;0:否)")
    private Integer isFrame;

    @ApiModelProperty(value = "允许删除(1:允许;0:不允许)")
    private Integer allowDelete;
}
