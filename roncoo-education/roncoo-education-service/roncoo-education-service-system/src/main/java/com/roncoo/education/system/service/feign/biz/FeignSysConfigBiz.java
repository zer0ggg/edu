package com.roncoo.education.system.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysConfigQO;
import com.roncoo.education.system.feign.vo.*;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 参数配置
 *
 * @author wujing
 */
@Component
public class FeignSysConfigBiz extends BaseBiz {

    @Autowired
    private SysConfigDao dao;

    public Page<SysConfigVO> listForPage(SysConfigQO qo) {
        SysConfigExample example = new SysConfigExample();
        // Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<SysConfig> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, SysConfigVO.class);
    }

    public int save(SysConfigQO qo) {
        SysConfig record = BeanUtil.copyProperties(qo, SysConfig.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public SysConfigVO getById(Long id) {
        SysConfig record = dao.getById(id);
        return BeanUtil.copyProperties(record, SysConfigVO.class);
    }

    public int updateById(SysConfigQO qo) {
        SysConfig record = BeanUtil.copyProperties(qo, SysConfig.class);
        return dao.updateById(record);
    }

    /**
     * 站点信息
     *
     * @return 站点信息
     */
    public ConfigWebsiteVO getWebsite() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigWebsiteVO.class, true);
    }

    /**
     * 系统配置
     *
     * @return 系统配置
     */
    public ConfigSysVO getSys() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigSysVO.class, true);
    }

    /**
     * 获取直播类型
     *
     * @return 直播类型配置
     */
    public ConfigLivePlatformVO getLivePlatform() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigLivePlatformVO.class, true);
    }

    /**
     * 获取欢拓直播配置
     *
     * @return 欢拓直播配置
     */
    public ConfigTalkFunVO getTalkFun() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigTalkFunVO.class, true);
    }

    public ConfigPolyvVO getPolyv() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        ConfigPolyvVO configPolyvVO = cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigPolyvVO.class, true);

        // 获取网关域名设置
        SysConfig gatewayDomainConfig = dao.getByConfigKey("gatewayDomain");
        if (ObjectUtil.isNotNull(gatewayDomainConfig) && StrUtil.isNotBlank(gatewayDomainConfig.getConfigValue())) {
            configPolyvVO.setPolyvLiveNotify(gatewayDomainConfig.getConfigValue() + "/callback/system/polyv/live/auth");
        }
        return configPolyvVO;
    }

    public ConfigAliyunVO getAliyun() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigAliyunVO.class, true);
    }

    /**
     * 获取平台点播配置
     *
     * @return 点播配置
     */
    public ConfigVodVO getVod() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigVodVO.class, true);
    }

    /**
     * 获取阿里云点播配置
     *
     * @return 阿里云点播配置
     */
    public ConfigAliYunVodVO getAliYunVod() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        ConfigAliYunVodVO configAliYunVodVO = cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigAliYunVodVO.class, true);
        // 获取网关域名设置
        SysConfig gatewayDomainConfig = dao.getByConfigKey("gatewayDomain");
        if (ObjectUtil.isNotNull(gatewayDomainConfig) && StrUtil.isNotBlank(gatewayDomainConfig.getConfigValue())) {
            configAliYunVodVO.setAliYunVodCallbackUrl(gatewayDomainConfig.getConfigValue() + "/callback/system/aliyun/upload");
        }
        return configAliYunVodVO;
    }

    public ConfigWeixinVO getWeixin() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigWeixinVO.class, true);
    }

    public SysConfigVO getByConfigKey(String configKey) {
        SysConfig sysConfig = dao.getByConfigKeyAndStatusId(configKey, StatusIdEnum.YES.getCode());
        return BeanUtil.copyProperties(sysConfig, SysConfigVO.class);
    }

    public ConfigTencentVO getTencent() {
        Map<String, String> configMap = dao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        return cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigTencentVO.class, true);
    }
}
