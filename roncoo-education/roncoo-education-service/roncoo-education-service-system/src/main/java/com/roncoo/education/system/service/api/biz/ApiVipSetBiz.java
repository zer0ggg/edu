package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.system.common.dto.VipSetForListDTO;
import com.roncoo.education.system.common.dto.VipSetListDTO;
import com.roncoo.education.system.service.dao.VipSetDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 会员设置
 *
 * @author wujing
 */
@Component
public class ApiVipSetBiz {

	@Autowired
	private VipSetDao vipSetDao;

	public Result<VipSetListDTO> list() {
		VipSetListDTO dto = new VipSetListDTO();
		List<VipSet> list = vipSetDao.listByStatusId(StatusIdEnum.YES.getCode());
		if (CollectionUtil.isNotEmpty(list)) {
			dto.setList(PageUtil.copyList(list, VipSetForListDTO.class));
		}
		return Result.success(dto);
	}

}
