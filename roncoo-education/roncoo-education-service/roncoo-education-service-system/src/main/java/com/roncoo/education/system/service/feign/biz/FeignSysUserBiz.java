package com.roncoo.education.system.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SystemSignUtil;
import com.roncoo.education.system.feign.qo.SysUserQO;
import com.roncoo.education.system.feign.vo.SysUserVO;
import com.roncoo.education.system.service.dao.SysUserDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUser;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统用户
 *
 * @author wujing
 */
@Component
public class FeignSysUserBiz extends BaseBiz {

    @Autowired
    private SysUserDao dao;

    public Page<SysUserVO> listForPage(SysUserQO qo) {
        SysUserExample example = new SysUserExample();
        Criteria c = example.createCriteria();
        example.setOrderByClause(" id desc ");
        Page<SysUser> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, SysUserVO.class);
    }

    public int save(SysUserQO qo) {
        if (ObjectUtil.isNotNull(dao.getByLoginName(qo.getLoginName()))) {
            return 1;
        }
        SysUser record = BeanUtil.copyProperties(qo, SysUser.class);
        record.setLoginPassword(SystemSignUtil.sign(qo.getLoginPassword(), qo.getLoginName()));
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public SysUserVO getById(Long id) {
        SysUser record = dao.getById(id);
        return BeanUtil.copyProperties(record, SysUserVO.class);
    }

    public int updateById(SysUserQO qo) {
        SysUser record = BeanUtil.copyProperties(qo, SysUser.class);
        return dao.updateById(record);
    }

}
