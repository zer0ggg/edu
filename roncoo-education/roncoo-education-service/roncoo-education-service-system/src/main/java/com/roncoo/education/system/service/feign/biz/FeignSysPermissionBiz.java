package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysPermissionQO;
import com.roncoo.education.system.feign.vo.SysPermissionVO;
import com.roncoo.education.system.service.dao.SysPermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统权限
 *
 * @author wujing
 */
@Component
public class FeignSysPermissionBiz extends BaseBiz {

    @Autowired
    private SysPermissionDao dao;

	public Page<SysPermissionVO> listForPage(SysPermissionQO qo) {
	    SysPermissionExample example = new SysPermissionExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysPermission> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysPermissionVO.class);
	}

	public int save(SysPermissionQO qo) {
		SysPermission record = BeanUtil.copyProperties(qo, SysPermission.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysPermissionVO getById(Long id) {
		SysPermission record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysPermissionVO.class);
	}

	public int updateById(SysPermissionQO qo) {
		SysPermission record = BeanUtil.copyProperties(qo, SysPermission.class);
		return dao.updateById(record);
	}

}
