package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysPermission;
import com.roncoo.education.system.feign.qo.SysPermissionQO;
import com.roncoo.education.system.feign.vo.SysPermissionVO;
import com.roncoo.education.system.service.feign.biz.FeignSysPermissionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统权限
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysPermissionController extends BaseController implements IFeignSysPermission{

    @Autowired
    private FeignSysPermissionBiz biz;

	@Override
	public Page<SysPermissionVO> listForPage(@RequestBody SysPermissionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysPermissionQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysPermissionQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysPermissionVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
