package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefUpdateStatusREQ", description="专区关联表修改状态")
public class ZoneRefUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @NotNull(message = "状态(1有效, 0无效)不能为空")
    @ApiModelProperty(value = "状态(1有效, 0无效)",required = true)
    private Integer statusId;
}
