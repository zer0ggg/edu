package com.roncoo.education.system.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AdvSaveREQ", description="广告信息添加")
public class AdvSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序",required = true)
    private Integer sort;

    @NotEmpty(message = "广告标题不能为空")
    @ApiModelProperty(value = "广告标题",required = true)
    private String advTitle;

    @ApiModelProperty(value = "广告链接")
    private String advUrl;

    @ApiModelProperty(value = "广告图片")
    private String advImg;

    @ApiModelProperty(value = "广告跳转方式")
    private String advTarget;

    @NotNull(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @NotNull(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @NotNull(message = "位置(1:电脑端，2:APP移动端)不能为空")
    @ApiModelProperty(value = "位置(1:电脑端，2:APP移动端)",required = true)
    private Integer platShow;

    @ApiModelProperty(value = "移动端类型(1:APP移动端,2:H5移动端)")
    private Integer mobileTerminalCategory;

    @ApiModelProperty(value = "广告位置(1首页轮播图，2顶部活动;3:首页资讯轮播图)")
    private Integer advLocation;

    @ApiModelProperty(value = "课程分类(1:点播;2:直播,4:文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

}
