package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 站点导航
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="WebsiteNavEditREQ", description="站点导航修改")
public class WebsiteNavEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "导航名称")
    private String navName;

    @ApiModelProperty(value = "状态(1文章, 2外部链接)")
    private Integer type;

    @ApiModelProperty(value = "外部链接")
    private String url;
}
