package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AllowDeleteEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.system.common.req.SysConfigEditREQ;
import com.roncoo.education.system.common.req.SysConfigListREQ;
import com.roncoo.education.system.common.req.SysConfigSaveREQ;
import com.roncoo.education.system.common.req.SysConfigUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysConfigListRESP;
import com.roncoo.education.system.common.resp.SysConfigViewRESP;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfigExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 参数配置
 *
 * @author wujing
 */
@Component
public class PcSysConfigBiz extends BaseBiz {

    @Autowired
    private SysConfigDao dao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 参数配置列表
     *
     * @param sysConfigListREQ 参数配置分页查询参数
     * @return 参数配置分页查询结果
     */
    public Result<Page<SysConfigListRESP>> list(SysConfigListREQ sysConfigListREQ) {
        SysConfigExample example = new SysConfigExample();
        Criteria c = example.createCriteria();
        // 参数名称
        if (ObjectUtil.isNotEmpty(sysConfigListREQ.getConfigName())) {
            c.andConfigNameLike(SqlUtil.like(sysConfigListREQ.getConfigName()));
        }
        // 参数键名
        if (ObjectUtil.isNotEmpty(sysConfigListREQ.getConfigKey())) {
            c.andConfigKeyLike(SqlUtil.like(sysConfigListREQ.getConfigKey()));
        }
        // 参数键值
        if (ObjectUtil.isNotEmpty(sysConfigListREQ.getConfigValue())) {
            c.andConfigValueLike(SqlUtil.like(sysConfigListREQ.getConfigValue()));
        }
        // 备注
        if (ObjectUtil.isNotEmpty(sysConfigListREQ.getRemark())) {
            c.andRemarkLike(SqlUtil.like(sysConfigListREQ.getRemark()));
        }
        // 信息分类
        if (ObjectUtil.isNotEmpty(sysConfigListREQ.getInfoCategory())) {
            c.andInfoCategoryEqualTo(sysConfigListREQ.getInfoCategory());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<SysConfig> page = dao.listForPage(sysConfigListREQ.getPageCurrent(), sysConfigListREQ.getPageSize(), example);
        Page<SysConfigListRESP> respPage = PageUtil.transform(page, SysConfigListRESP.class);

        return Result.success(respPage);
    }

    /**
     * 参数配置添加
     *
     * @param sysConfigSaveREQ 参数配置
     * @return 添加结果
     */
    public Result<String> save(SysConfigSaveREQ sysConfigSaveREQ) {
        SysConfig record = BeanUtil.copyProperties(sysConfigSaveREQ, SysConfig.class);
        if (dao.save(record) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 参数配置查看
     *
     * @param id 主键ID
     * @return 参数配置
     */
    public Result<SysConfigViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysConfigViewRESP.class));
    }


    /**
     * 参数配置修改
     *
     * @param sysConfigEditREQ 参数配置修改对象
     * @return 修改结果
     */
    public Result<String> edit(SysConfigEditREQ sysConfigEditREQ) {
        SysConfig record = BeanUtil.copyProperties(sysConfigEditREQ, SysConfig.class);
        if (dao.updateById(record) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 参数配置删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        SysConfig config = dao.getById(id);
        if (AllowDeleteEnum.NOT_ALLOW.getCode().equals(config.getAllowDelete())) {
            return Result.error("不允许删除");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 修改状态
     *
     * @param sysConfigUpdateStatusREQ 参数配置修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(SysConfigUpdateStatusREQ sysConfigUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(sysConfigUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        SysConfig sysConfig = dao.getById(sysConfigUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(sysConfig)) {
            return Result.error("配置不存在");
        }
        if (AllowDeleteEnum.NOT_ALLOW.getCode().equals(sysConfig.getAllowDelete())) {
            return Result.error("配置不允许删除，无法修改状态");
        }

        SysConfig record = BeanUtil.copyProperties(sysConfigUpdateStatusREQ, SysConfig.class);
        if (dao.updateById(record) > 0) {
            myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete(RedisPreEnum.SYS_CONFIG.getCode());
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiWebsiteController::getWebsiteBO*");
        return Result.success("删除成功");
    }
}
