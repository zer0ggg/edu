package com.roncoo.education.system.service.api.biz;

import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.system.common.bo.AdvBO;
import com.roncoo.education.system.common.dto.AdvDTO;
import com.roncoo.education.system.common.dto.AdvListDTO;
import com.roncoo.education.system.service.dao.AdvDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Adv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 广告信息
 *
 * @author wujing
 */
@Component
public class ApiAdvBiz {

	@Autowired
	private AdvDao advDao;

	public Result<AdvListDTO> list(AdvBO advBO) {
		if (advBO.getAdvLocation() == null) {
			return Result.error("广告位置不能为空");
		}
		if (advBO.getPlatShow() == null) {
			return Result.error("位置不能为空");
		}
		AdvListDTO dto = new AdvListDTO();
		// 开始时间和结束时间
		List<Adv> advList = advDao.listByPlatShowAndAdvLocationAndvMobileTerminalCategoryAndStatusId(advBO.getPlatShow(), advBO.getAdvLocation(), advBO.getMobileTerminalCategory(),StatusIdEnum.YES.getCode());
		dto.setAdvList(PageUtil.copyList(advList, AdvDTO.class));
		return Result.success(dto);
	}

}
