package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysRolePermissionEditREQ;
import com.roncoo.education.system.common.req.SysRolePermissionListREQ;
import com.roncoo.education.system.common.req.SysRolePermissionSaveREQ;
import com.roncoo.education.system.common.resp.SysRolePermissionListRESP;
import com.roncoo.education.system.common.resp.SysRolePermissionViewRESP;
import com.roncoo.education.system.service.dao.SysRolePermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统角色权限
 *
 * @author wujing
 */
@Component
public class PcSysRolePermissionBiz extends BaseBiz {

    @Autowired
    private SysRolePermissionDao dao;

    /**
    * 系统角色权限列表
    *
    * @param sysRolePermissionListREQ 系统角色权限分页查询参数
    * @return 系统角色权限分页查询结果
    */
    public Result<Page<SysRolePermissionListRESP>> list(SysRolePermissionListREQ sysRolePermissionListREQ) {
        SysRolePermissionExample example = new SysRolePermissionExample();
        Criteria c = example.createCriteria();
        Page<SysRolePermission> page = dao.listForPage(sysRolePermissionListREQ.getPageCurrent(), sysRolePermissionListREQ.getPageSize(), example);
        Page<SysRolePermissionListRESP> respPage = PageUtil.transform(page, SysRolePermissionListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 系统角色权限添加
    *
    * @param sysRolePermissionSaveREQ 系统角色权限
    * @return 添加结果
    */
    public Result<String> save(SysRolePermissionSaveREQ sysRolePermissionSaveREQ) {
        SysRolePermission record = BeanUtil.copyProperties(sysRolePermissionSaveREQ, SysRolePermission.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 系统角色权限查看
    *
    * @param id 主键ID
    * @return 系统角色权限
    */
    public Result<SysRolePermissionViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysRolePermissionViewRESP.class));
    }


    /**
    * 系统角色权限修改
    *
    * @param sysRolePermissionEditREQ 系统角色权限修改对象
    * @return 修改结果
    */
    public Result<String> edit(SysRolePermissionEditREQ sysRolePermissionEditREQ) {
        SysRolePermission record = BeanUtil.copyProperties(sysRolePermissionEditREQ, SysRolePermission.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 系统角色权限删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
