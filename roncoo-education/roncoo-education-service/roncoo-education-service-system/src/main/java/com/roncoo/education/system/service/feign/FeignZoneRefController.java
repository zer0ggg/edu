package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignZoneRef;
import com.roncoo.education.system.feign.qo.ZoneRefQO;
import com.roncoo.education.system.feign.vo.ZoneRefVO;
import com.roncoo.education.system.service.feign.biz.FeignZoneRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 专区关联
 *
 * @author wujing
 * @date 2020-05-23
 */
@RestController
public class FeignZoneRefController extends BaseController implements IFeignZoneRef{

    @Autowired
    private FeignZoneRefBiz biz;

	@Override
	public Page<ZoneRefVO> listForPage(@RequestBody ZoneRefQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ZoneRefQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ZoneRefQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ZoneRefVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
