package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SysRoleAllocationMenuAndPermissionREQ", description = "系统角色分配菜单和权限")
public class SysRoleAllocationMenuAndPermissionREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "角色ID不能为空")
    @ApiModelProperty(value = "角色ID", required = true)
    private Long id;

    @ApiModelProperty(value = "菜单ID集合，多个以英文逗号分隔")
    private List<Long> menuIdList;

    @ApiModelProperty(value = "权限ID集合，多个以英文逗号分隔")
    private List<Long> permissionIdList;
}
