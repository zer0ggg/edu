package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SysPermissionEditREQ;
import com.roncoo.education.system.common.req.SysPermissionListREQ;
import com.roncoo.education.system.common.req.SysPermissionSaveREQ;
import com.roncoo.education.system.common.req.SysPermissionUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysPermissionListRESP;
import com.roncoo.education.system.common.resp.SysPermissionViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysPermissionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 系统权限 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/permission")
@Api(value = "system-系统权限", tags = {"system-系统权限"})
public class PcSysPermissionController {

    @Autowired
    private PcSysPermissionBiz biz;

    @ApiOperation(value = "系统权限列表", notes = "系统权限列表")
    @PostMapping(value = "/list")
    public Result<Page<SysPermissionListRESP>> list(@RequestBody SysPermissionListREQ sysPermissionListREQ) {
        return biz.list(sysPermissionListREQ);
    }


    @ApiOperation(value = "系统权限添加", notes = "系统权限添加")
    @SysLog(value = "系统权限添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid SysPermissionSaveREQ sysPermissionSaveREQ) {
        return biz.save(sysPermissionSaveREQ);
    }

    @ApiOperation(value = "系统权限查看", notes = "系统权限查看")
    @GetMapping(value = "/view")
    public Result<SysPermissionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "系统权限修改", notes = "系统权限修改")
    @SysLog(value = "系统权限修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid SysPermissionEditREQ sysPermissionEditREQ) {
        return biz.edit(sysPermissionEditREQ);
    }


    @ApiOperation(value = "系统权限状态修改", notes = "系统权限状态修改")
    @SysLog(value = "系统权限状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid SysPermissionUpdateStatusREQ sysPermissionUpdateStatusREQ) {
        return biz.updateStatus(sysPermissionUpdateStatusREQ);
    }


    @ApiOperation(value = "系统权限删除", notes = "系统权限删除")
    @SysLog(value = "系统权限删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
