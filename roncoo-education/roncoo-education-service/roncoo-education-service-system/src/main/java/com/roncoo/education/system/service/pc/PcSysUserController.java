package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.SysUserListRESP;
import com.roncoo.education.system.common.resp.SysUserViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysUserBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 系统用户 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/user")
@Api(value = "system-系统用户", tags = {"system-系统用户"})
public class PcSysUserController {

    @Autowired
    private PcSysUserBiz biz;

    @ApiOperation(value = "系统用户列表", notes = "系统用户列表")
    @PostMapping(value = "/list")
    public Result<Page<SysUserListRESP>> list(@RequestBody SysUserListREQ sysUserListREQ) {
        return biz.list(sysUserListREQ);
    }


    @SysLog(value = "系统用户添加")
    @ApiOperation(value = "系统用户添加", notes = "系统用户添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid SysUserSaveREQ sysUserSaveREQ) {
        return biz.save(sysUserSaveREQ);
    }

    @ApiOperation(value = "系统用户查看", notes = "系统用户查看")
    @GetMapping(value = "/view")
    public Result<SysUserViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "系统用户修改", notes = "系统用户修改")
    @SysLog(value = "系统用户修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid SysUserEditREQ sysUserEditREQ) {
        return biz.edit(sysUserEditREQ);
    }


    @ApiOperation(value = "系统用户状态修改", notes = "系统用户状态修改")
    @SysLog(value = "系统用户状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid SysUserUpdateStatusREQ sysUserUpdateStatusREQ) {
        return biz.updateStatus(sysUserUpdateStatusREQ);
    }


    @ApiOperation(value = "系统用户删除", notes = "系统用户删除")
    @SysLog(value = "系统用户删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "系统用户角色分配", notes = "系统用户角色分配")
    @SysLog(value = "系统用户角色分配")
    @PostMapping(value = "/allocation")
    public Result<String> allocationRole(@RequestBody @Valid SysUserAllocationRoleREQ sysUserAllocationRoleREQ) {
        return biz.allocationRole(sysUserAllocationRoleREQ);
    }


    @ApiOperation(value = "系统用户密码修改", notes = "系统用户密码修改")
    @SysLog(value = "系统用户密码修改")
    @PutMapping(value = "/update/password")
    public Result<String> updatePassword(@RequestBody @Valid SysUserUpdatePasswordREQ sysUserUpdatePasswordREQ) {
        return biz.updatePassword(sysUserUpdatePasswordREQ);
    }


    @ApiOperation(value = "当前用户修改密码", notes = "当前用户修改密码")
    @SysLog(value = "系统用户修改")
    @PutMapping(value = "/update/password/current")
    public Result<String> updatePasswordCurrent(@RequestBody @Valid SysUserUpdatePasswordCurrentREQ sysUserUpdatePasswordCurrentREQ) {
        return biz.updatePasswordCurrent(sysUserUpdatePasswordCurrentREQ);
    }

    @ApiOperation(value = "获取已分配角色ID", notes = "获取已分配角色ID")
    @ApiImplicitParams(@ApiImplicitParam(value = "用户ID", name = "id", required = true, dataType = "Long"))
    @GetMapping(value = "/select/role/list")
    public Result<List<Long>> listSelectRoleById(@RequestParam Long id) {
        return biz.listSelectRoleById(id);
    }

    @ApiOperation(value = "获取已分配菜单", notes = "获取已分配菜单")
    @PostMapping(value = "/menu")
    public Result<List<Map<String, Object>>> listUserMenu() {
        return biz.listUserMenu();
    }

    @ApiOperation(value = "获取已分配权限", notes = "获取已分配权限")
    @PostMapping(value = "/permission")
    public Result<List<String>> listUserPermission() {
        return biz.listUserPermission();
    }
}
