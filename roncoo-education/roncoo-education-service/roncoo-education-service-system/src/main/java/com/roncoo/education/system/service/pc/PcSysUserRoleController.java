package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SysUserRoleEditREQ;
import com.roncoo.education.system.common.req.SysUserRoleListREQ;
import com.roncoo.education.system.common.req.SysUserRoleSaveREQ;
import com.roncoo.education.system.common.resp.SysUserRoleListRESP;
import com.roncoo.education.system.common.resp.SysUserRoleViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysUserRoleBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统用户角色 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/user/role")
@Api(value = "system-系统用户角色", tags = { "system-系统用户角色" })
public class PcSysUserRoleController {

	@Autowired
	private PcSysUserRoleBiz biz;

	@ApiOperation(value = "系统用户角色列表", notes = "系统用户角色列表")
	@PostMapping(value = "/list")
	public Result<Page<SysUserRoleListRESP>> list(@RequestBody SysUserRoleListREQ sysUserRoleListREQ) {
		return biz.list(sysUserRoleListREQ);
	}


	@ApiOperation(value = "系统用户角色添加", notes = "系统用户角色添加")
	@SysLog(value = "系统用户角色添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody SysUserRoleSaveREQ sysUserRoleSaveREQ) {
		return biz.save(sysUserRoleSaveREQ);
	}

	@ApiOperation(value = "系统用户角色查看", notes = "系统用户角色查看")
	@GetMapping(value = "/view")
	public Result<SysUserRoleViewRESP> view(@RequestParam Long id) {
		return biz.view(id);
	}


	@ApiOperation(value = "系统用户角色修改", notes = "系统用户角色修改")
	@SysLog(value = "系统用户角色修改")
	@PutMapping(value = "/edit")
	public Result<String> edit(@RequestBody SysUserRoleEditREQ sysUserRoleEditREQ) {
		return biz.edit(sysUserRoleEditREQ);
	}



	@ApiOperation(value = "系统用户角色删除", notes = "系统用户角色删除")
	@SysLog(value = "系统用户角色删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}
}
