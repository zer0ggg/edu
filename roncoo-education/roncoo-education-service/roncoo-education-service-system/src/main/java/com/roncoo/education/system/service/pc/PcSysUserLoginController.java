package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.req.SysUserAuthenticateREQ;
import com.roncoo.education.system.common.req.SysUserLoginREQ;
import com.roncoo.education.system.common.resp.SysUserLoginRESP;
import com.roncoo.education.system.service.pc.biz.PcSysUserLoginBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author LYQ
 */
@RestController
@RequestMapping("/system/pc/api/sys/user")
@Api(value = "system-系统用户登录", tags = {"system-系统用户登录"})
public class PcSysUserLoginController {

    @Autowired
    private PcSysUserLoginBiz biz;

    @ApiOperation(value = "用户登录", notes = "用户登录")
    @PostMapping(value = "/login")
    public Result<SysUserLoginRESP> login(@RequestBody @Valid SysUserLoginREQ loginREQ, HttpServletRequest request) {
        return biz.login(loginREQ, request);
    }

    @ApiOperation(value = "人机验证", notes = "人机验证")
    @PostMapping(value = "/authenticate")
    public Result<String> manMachineVerification(@RequestBody @Valid SysUserAuthenticateREQ authenticateREQ) {
        return biz.manMachineVerification(authenticateREQ);
    }
}
