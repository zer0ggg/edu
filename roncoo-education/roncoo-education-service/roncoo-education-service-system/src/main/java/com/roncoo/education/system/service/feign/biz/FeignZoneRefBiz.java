package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.ZoneRefQO;
import com.roncoo.education.system.feign.vo.ZoneRefVO;
import com.roncoo.education.system.service.dao.ZoneRefDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 专区关联
 *
 * @author wujing
 */
@Component
public class FeignZoneRefBiz extends BaseBiz {

    @Autowired
    private ZoneRefDao dao;

	public Page<ZoneRefVO> listForPage(ZoneRefQO qo) {
	    ZoneRefExample example = new ZoneRefExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ZoneRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ZoneRefVO.class);
	}

	public int save(ZoneRefQO qo) {
		ZoneRef record = BeanUtil.copyProperties(qo, ZoneRef.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ZoneRefVO getById(Long id) {
		ZoneRef record = dao.getById(id);
		return BeanUtil.copyProperties(record, ZoneRefVO.class);
	}

	public int updateById(ZoneRefQO qo) {
		ZoneRef record = BeanUtil.copyProperties(qo, ZoneRef.class);
		return dao.updateById(record);
	}

}
