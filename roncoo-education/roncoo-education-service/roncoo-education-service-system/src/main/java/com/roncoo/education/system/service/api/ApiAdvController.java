package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.bo.AdvBO;
import com.roncoo.education.system.common.dto.AdvListDTO;
import com.roncoo.education.system.service.api.biz.ApiAdvBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广告信息
 *
 * @author wujing
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping(value = "/system/api/adv")
public class ApiAdvController extends BaseController {

	@Autowired
	private ApiAdvBiz biz;

	@Cacheable
	@ApiOperation(value = "广告列表接口", notes = "广告列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<AdvListDTO> list(@RequestBody AdvBO advBO) {
		return biz.list(advBO);
	}
}
