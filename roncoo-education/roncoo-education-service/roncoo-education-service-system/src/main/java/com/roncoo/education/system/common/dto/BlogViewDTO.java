package com.roncoo.education.system.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 博客信息表
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class BlogViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 博客ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博客ID")
	private Long id;
	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	@ApiModelProperty(value = "更新时间")
	private Date gmtModified;
	/**
	 * 博主用户编号
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "博主用户编号")
	private Long userNo;
	/**
	 * 博主用户头像
	 */
	@ApiModelProperty(value = "博主用户头像")
	private String bloggerUserImg;
	/**
	 * 博主用户昵称
	 */
	@ApiModelProperty(value = "博主用户昵称")
	private String bloggerNickname;
	/**
	 * 博客标题
	 */
	@ApiModelProperty(value = "博客标题")
	private String title;
	/**
	 * 博客标签
	 */
	@ApiModelProperty(value = "博客标签")
	private String tagsName;
	/**
	 * 博客内容
	 */
	@ApiModelProperty(value = "博客内容")
	private String content;
	/**
	 * 博客类型(1原创，2转载)
	 */
	@ApiModelProperty(value = "博客类型(1原创，2转载)")
	private Integer typeId;
	/**
	 * 是否置顶(1置顶，0否)
	 */
	@ApiModelProperty(value = "是否置顶(1置顶，0否)")
	private Integer isTop;
	/**
	 * 收藏人数
	 */
	@ApiModelProperty(value = "收藏人数")
	private Integer collectionAcount;
	/**
	 * 点赞人数
	 */
	@ApiModelProperty(value = "点赞人数")
	private Integer admireAcount;
	/**
	 * 评论人数
	 */
	@ApiModelProperty(value = "评论人数")
	private Integer commentAcount;
	/**
	 * 阅读人数
	 */
	@ApiModelProperty(value = "阅读人数")
	private Integer readAcount;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	@ApiModelProperty(value = "创建时间")
	private Date gmtCreate;

	/**
	 * 状态(1:正常;0:禁用)
	 */
	@ApiModelProperty(value = "状态(1:正常;0:禁用)")
	private Integer statusId;

	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;

	/**
	 * 是否存在封面(1,存在；0,不存在)
	 */
	@ApiModelProperty(value = "是否存在封面(1,存在；0,不存在)")
	private Integer isImg;

	/**
	 * 博客封面
	 */
	@ApiModelProperty(value = "博客封面")
	private String blogImg;

	/**
	 * 文章类型(1:博客;2资讯)
	 */
	@ApiModelProperty(value = "文章类型(1:博客;2资讯)")
	private Integer articleType;
	/**
	 * 博客摘要
	 */
	@ApiModelProperty(value = "博客摘要")
	private String summary;
	/**
	 * 质量度
	 */
	private Integer qualityScore;
	/**
	 * 博客关键词
	 */
	private String keywords;
	/**
	 * 编辑模式（1可视编辑，2markdown）
	 */
	private Integer editMode;
	/**
	 * 是否公开博客(1公开，0不公开)
	 */
	private Integer isOpen;
	/**
	 * 是否已经发布(1发布，0没发布)
	 */
	private Integer isIssue;


}
