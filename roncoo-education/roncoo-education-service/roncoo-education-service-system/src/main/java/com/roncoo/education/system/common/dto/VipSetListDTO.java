package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * vip配置
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class VipSetListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 会员信息集合
     */
    @ApiModelProperty(value = "会员信息集合")
    private List<VipSetForListDTO> list = new ArrayList<>();
}
