package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.ZoneCategoryEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.interfaces.IFeignQuestions;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.system.common.dto.BlogViewDTO;
import com.roncoo.education.system.common.dto.CourseDTO;
import com.roncoo.education.system.common.dto.ExamInfoDTO;
import com.roncoo.education.system.common.dto.QuestionsDTO;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.ZoneRefListRESP;
import com.roncoo.education.system.common.resp.ZoneRefViewRESP;
import com.roncoo.education.system.service.dao.ZoneDao;
import com.roncoo.education.system.service.dao.ZoneRefDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 专区关联
 *
 * @author wujing
 */
@Component
public class PcZoneRefBiz extends BaseBiz {

    @Autowired
    private ZoneRefDao dao;
    @Autowired
    private ZoneDao zoneDao;
    @Autowired
    private IFeignCourse courseDao;
    @Autowired
    private IFeignBlog blogDao;
    @Autowired
    private IFeignExamInfo examDao;
    @Autowired
    private IFeignQuestions feignQuestions;


    /**
     * 专区关联列表
     *
     * @param req 专区关联分页查询参数
     * @return 专区关联分页查询结果
     */
    public Result<Page<ZoneRefListRESP>> list(ZoneRefListREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Zone zone = zoneDao.getById(req.getZoneId());
        if (ObjectUtils.isEmpty(zone)) {
            return Result.error("专区id不正确");
        }
        ZoneRefExample example = new ZoneRefExample();
        Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(req.getZoneId());
        c.andZoneLocationEqualTo(req.getZoneLocation());
        if (req.getZoneId() != null) {
            c.andZoneIdEqualTo(req.getZoneId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (!StringUtils.isEmpty(req.getTitle())) {
            c.andTitleLike(PageUtil.like(req.getTitle()));
        }
        if (req.getZoneLocation() != null) {
            c.andZoneLocationEqualTo(req.getZoneLocation());
        }
        example.setOrderByClause("sort asc, status_id desc, id desc");
        Page<ZoneRef> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ZoneRefListRESP> respPage = PageUtil.transform(page, ZoneRefListRESP.class);
        //自定义类型不需要查询关联信息
        if (ZoneCategoryEnum.DEFINITION.getCode().equals(zone.getZoneCategory())) {
            return Result.success(respPage);
        }
        for (ZoneRefListRESP zoneRef : respPage.getList()) {
            if (ZoneCategoryEnum.ORDINARY.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.LIVE.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(zone.getZoneCategory())) {
                CourseVO vo = courseDao.getById(zoneRef.getCourseId());
                if (!ObjectUtils.isEmpty(vo)) {
                    zoneRef.setCourse(BeanUtil.copyProperties(vo, CourseDTO.class));
                }
            } else if (ZoneCategoryEnum.EXAM.getCode().equals(zone.getZoneCategory())) {
                ExamInfoVO vo = examDao.getById(zoneRef.getCourseId());
                if (!ObjectUtils.isEmpty(vo)) {
                    zoneRef.setExam(BeanUtil.copyProperties(vo, ExamInfoDTO.class));
                }
            } else if (ZoneCategoryEnum.Blog.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.INFOMATION.getCode().equals(zone.getZoneCategory())) {
                BlogVO vo = blogDao.getById(zoneRef.getCourseId());
                if (!ObjectUtils.isEmpty(vo)) {
                    zoneRef.setBlog(BeanUtil.copyProperties(vo, BlogViewDTO.class));
                }
            } else if (ZoneCategoryEnum.QUESTIONS.getCode().equals(zone.getZoneCategory())) {
                QuestionsVO questionsVO = feignQuestions.getById(zoneRef.getCourseId());
                if (!ObjectUtils.isEmpty(questionsVO)) {
                    zoneRef.setQuestions(BeanUtil.copyProperties(questionsVO, QuestionsDTO.class));
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 专区关联添加
     *
     * @param req 专区关联
     * @return 添加结果
     */
    public Result<String> save(ZoneRefSaveREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        //检验专区
        Zone zone = zoneDao.getById(req.getZoneId());
        if (ObjectUtils.isEmpty(zone)) {
            return Result.error("专区id不正确");
        }
        if (!ZoneCategoryEnum.DEFINITION.getCode().equals(zone.getZoneCategory()) && req.getCourseId() == null) {
            return Result.error("关联id不能为空");
        }
        //检验关联id真实性
        if (ZoneCategoryEnum.ORDINARY.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.LIVE.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(zone.getZoneCategory())) {
            CourseVO vo = courseDao.getById(req.getCourseId());
            if (ObjectUtils.isEmpty(vo)) {
                return Result.error("关联id不正确");
            }
        } else if (ZoneCategoryEnum.EXAM.getCode().equals(zone.getZoneCategory())) {
            ExamInfoVO vo = examDao.getById(req.getCourseId());
            if (ObjectUtils.isEmpty(vo)) {
                return Result.error("关联id不正确");
            }
        } else if (ZoneCategoryEnum.QUESTIONS.getCode().equals(zone.getZoneCategory())) {
            QuestionsVO questionsVO = feignQuestions.getById(req.getCourseId());
            if (ObjectUtils.isEmpty(questionsVO)) {
                return Result.error("找不到问答信息");
            }

        } else if (ZoneCategoryEnum.Blog.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.INFOMATION.getCode().equals(zone.getZoneCategory())) {
            BlogVO vo = blogDao.getById(req.getCourseId());
            if (ObjectUtils.isEmpty(vo)) {
                return Result.error("关联id不正确");
            }
        } else {
            // 自定义类型
            if (StringUtils.isEmpty(req.getTitle())) {
                return Result.error("标题不能为空");
            }
            req.setCourseId(null);
        }
        // 防止重复提交
        ZoneRef record = null;
        if (req.getCourseId() != null) {
            record = dao.getZoneIdAndRefId(req.getZoneId(), req.getCourseId());

        } else {
            record = dao.getZoneIdAndTitle(req.getZoneId(), req.getTitle());
        }
        if (!ObjectUtils.isEmpty(record)) {
            return Result.success("操作成功");
        }
        ZoneRef bean = BeanUtil.copyProperties(req, ZoneRef.class);
        if (bean.getSort() == null) {
            bean.setSort(Integer.valueOf(1));
        }
        bean.setZoneLocation(zone.getZoneLocation());
        if (dao.save(bean) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }


    /**
     * 专区关联查看
     *
     * @param id 主键ID
     * @return 专区关联
     */
    public Result<ZoneRefViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ZoneRefViewRESP.class));
    }


    /**
     * 专区关联修改
     *
     * @param req 专区关联修改对象
     * @return 修改结果
     */
    public Result<String> edit(ZoneRefEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ZoneRef record = BeanUtil.copyProperties(req, ZoneRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 专区关联删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> updateStatus(ZoneRefUpdateStatusREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
            return Result.error("枚举值不正确");
        }
        ZoneRef ref = dao.getById(req.getId());
        if (ObjectUtil.isNull(ref)) {
            return Result.error("找不到专区关联信息");
        }
        ZoneRef bean = BeanUtil.copyProperties(req, ZoneRef.class);
        if (dao.updateById(bean) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");

    }

    public Result<String> saveForBatch(ZoneRefSaveForBatchREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        Zone zone = zoneDao.getById(req.getZoneId());
        if (ObjectUtils.isEmpty(zone)) {
            return Result.error("专区id错误");
        }
        if (ZoneCategoryEnum.DEFINITION.getCode().equals(zone.getZoneCategory())) {
            return Result.error("自定义专区不允许批量添加");
        }
        String[] ids = req.getCourseIds().split(",");
        try {
            for (String idStr : ids) {
                Long id = Long.valueOf(idStr);
                ZoneRef record = dao.getZoneIdAndRefId(zone.getId(), id);
                // 已经关联过，也跳过
                if (!ObjectUtils.isEmpty(record)) {
                    continue;
                }
                //检验关联id可用，则保存关联id
                // 根据类型不同，检验id的真实性
                if (ZoneCategoryEnum.ORDINARY.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.LIVE.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(zone.getZoneCategory())) {
                    CourseVO vo = courseDao.getById(id);
                    //不存在,则跳过
                    if (ObjectUtils.isEmpty(vo)) {
                        continue;
                    }
                } else if (ZoneCategoryEnum.EXAM.getCode().equals(zone.getZoneCategory())) {
                    ExamInfoVO vo = examDao.getById(id);
                    //不存在,则跳过
                    if (ObjectUtils.isEmpty(vo)) {
                        continue;
                    }
                } else if (ZoneCategoryEnum.Blog.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.INFOMATION.getCode().equals(zone.getZoneCategory())) {
                    BlogVO vo = blogDao.getById(id);
                    //不存在,则跳过
                    if (ObjectUtils.isEmpty(vo)) {
                        continue;
                    }
                } else if (ZoneCategoryEnum.QUESTIONS.getCode().equals(zone.getZoneCategory())) {
                    QuestionsVO questionsVO = feignQuestions.getById(id);
                    if (ObjectUtils.isEmpty(questionsVO)) {
                        continue;
                    }
                }
                record = new ZoneRef();
                record.setId(IdWorker.getId());
                record.setZoneId(zone.getId());
                record.setCourseId(id);
                record.setZoneLocation(zone.getZoneLocation());
                record.setStatusId(StatusIdEnum.YES.getCode());
                record.setSort(Integer.valueOf(1));
                dao.save(record);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return Result.success("操作成功");
    }

}
