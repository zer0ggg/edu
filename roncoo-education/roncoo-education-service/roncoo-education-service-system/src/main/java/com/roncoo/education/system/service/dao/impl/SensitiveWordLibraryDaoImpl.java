package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SensitiveWordLibraryDao;
import com.roncoo.education.system.service.dao.impl.mapper.SensitiveWordLibraryMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibrary;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibraryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SensitiveWordLibraryDaoImpl implements SensitiveWordLibraryDao {
    @Autowired
    private SensitiveWordLibraryMapper sensitiveWordLibraryMapper;

    @Override
    public int save(SensitiveWordLibrary record) {
        record.setId(IdWorker.getId());
        return this.sensitiveWordLibraryMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.sensitiveWordLibraryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SensitiveWordLibrary record) {
        return this.sensitiveWordLibraryMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SensitiveWordLibrary getById(Long id) {
        return this.sensitiveWordLibraryMapper.selectByPrimaryKey(id);
    }


    @Override
    public Page<SensitiveWordLibrary> listForPage(int pageCurrent, int pageSize, SensitiveWordLibraryExample example) {
        int count = this.sensitiveWordLibraryMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<SensitiveWordLibrary>(count, totalPage, pageCurrent, pageSize, this.sensitiveWordLibraryMapper.selectByExample(example));
    }


    @Override
    public List<String> listAll() {
        List<SensitiveWordLibrary> list = this.sensitiveWordLibraryMapper.selectByExample(new SensitiveWordLibraryExample());
        List<String> listStr = new ArrayList<>();
        for (SensitiveWordLibrary sensitiveWordLibrary : list) {
            listStr.add(sensitiveWordLibrary.getBadword());
        }
        return listStr;
    }

    @Override
    public SensitiveWordLibrary getByBadword(String badword) {
        SensitiveWordLibraryExample example = new SensitiveWordLibraryExample();
        SensitiveWordLibraryExample.Criteria criteria = example.createCriteria();
        criteria.andBadwordEqualTo(badword);
        List<SensitiveWordLibrary> list = this.sensitiveWordLibraryMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
