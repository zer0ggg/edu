package com.roncoo.education.system.service.dao.impl.mapper;

import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VipSetMapper {
    int countByExample(VipSetExample example);

    int deleteByExample(VipSetExample example);

    int deleteByPrimaryKey(Long id);

    int insert(VipSet record);

    int insertSelective(VipSet record);

    List<VipSet> selectByExample(VipSetExample example);

    VipSet selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") VipSet record, @Param("example") VipSetExample example);

    int updateByExample(@Param("record") VipSet record, @Param("example") VipSetExample example);

    int updateByPrimaryKeySelective(VipSet record);

    int updateByPrimaryKey(VipSet record);
}
