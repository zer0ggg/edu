package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.req.AppMessageEditREQ;
import com.roncoo.education.system.common.req.AppMessageListREQ;
import com.roncoo.education.system.common.req.AppMessageSaveREQ;
import com.roncoo.education.system.common.req.AppMessageSendREQ;
import com.roncoo.education.system.common.resp.AppMessageListRESP;
import com.roncoo.education.system.common.resp.AppMessageViewRESP;
import com.roncoo.education.system.service.pc.biz.PcAppMessageBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * app推送 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-app推送")
@RestController
@RequestMapping("/system/pc/app/message")
public class PcAppMessageController {

    @Autowired
    private PcAppMessageBiz biz;

    @ApiOperation(value = "app推送列表", notes = "app推送列表")
    @PostMapping(value = "/list")
    public Result<Page<AppMessageListRESP>> list(@RequestBody AppMessageListREQ appMessageListREQ) {
        return biz.list(appMessageListREQ);
    }

    @ApiOperation(value = "app推送添加", notes = "app推送添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AppMessageSaveREQ appMessageSaveREQ) {
        return biz.save(appMessageSaveREQ);
    }

    @ApiOperation(value = "app推送查看", notes = "app推送查看")
    @GetMapping(value = "/view")
    public Result<AppMessageViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "app推送修改", notes = "app推送修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody AppMessageEditREQ appMessageEditREQ) {
        return biz.edit(appMessageEditREQ);
    }

    @ApiOperation(value = "app推送删除", notes = "app推送删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "app推送发送", notes = "app推送发送")
    @PostMapping(value = "/send")
    public Result<String> send(@RequestBody AppMessageSendREQ appMessageSendREQ) {
        return biz.send(appMessageSendREQ);
    }


}
