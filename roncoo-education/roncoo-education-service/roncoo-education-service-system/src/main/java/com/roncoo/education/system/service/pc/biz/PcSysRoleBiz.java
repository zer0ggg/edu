package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.AllowDeleteEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.SysRoleListRESP;
import com.roncoo.education.system.common.resp.SysRoleSelectMenuPermissionRESP;
import com.roncoo.education.system.common.resp.SysRoleViewRESP;
import com.roncoo.education.system.service.dao.SysRoleDao;
import com.roncoo.education.system.service.dao.SysRoleMenuDao;
import com.roncoo.education.system.service.dao.SysRolePermissionDao;
import com.roncoo.education.system.service.dao.SysUserRoleDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleExample.Criteria;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统角色
 *
 * @author wujing
 */
@Component
public class PcSysRoleBiz extends BaseBiz {

    @Autowired
    private SysRoleDao dao;
    @Autowired
    private SysUserRoleDao userRoleDao;
    @Autowired
    private SysRoleMenuDao roleMenuDao;
    @Autowired
    private SysRolePermissionDao rolePermissionDao;

    /**
     * 系统角色列表
     *
     * @param sysRoleListREQ 系统角色分页查询参数
     * @return 系统角色分页查询结果
     */
    public Result<Page<SysRoleListRESP>> list(SysRoleListREQ sysRoleListREQ) {
        SysRoleExample example = new SysRoleExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(sysRoleListREQ.getStatusId())) {
            c.andStatusIdEqualTo(sysRoleListREQ.getStatusId());
        }
        if (StrUtil.isNotBlank(sysRoleListREQ.getRoleName())) {
            c.andRoleNameLike(PageUtil.like(sysRoleListREQ.getRoleName()));
        }
        if (StrUtil.isNotBlank(sysRoleListREQ.getRoleValue())) {
            c.andRoleValueLike(PageUtil.like(sysRoleListREQ.getRoleValue()));
        }
        Page<SysRole> page = dao.listForPage(sysRoleListREQ.getPageCurrent(), sysRoleListREQ.getPageSize(), example);
        Page<SysRoleListRESP> respPage = PageUtil.transform(page, SysRoleListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 系统角色添加
     *
     * @param sysRoleSaveREQ 系统角色
     * @return 添加结果
     */
    public Result<String> save(SysRoleSaveREQ sysRoleSaveREQ) {
        if (ObjectUtil.isNotNull(dao.getByRoleValue(sysRoleSaveREQ.getRoleValue()))) {
            return Result.error("该角色【" + sysRoleSaveREQ.getRoleValue() + "】已存在");
        }
        SysRole record = BeanUtil.copyProperties(sysRoleSaveREQ, SysRole.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 系统角色查看
     *
     * @param id 主键ID
     * @return 系统角色
     */
    public Result<SysRoleViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysRoleViewRESP.class));
    }

    /**
     * 获取选中的角色菜单和权限
     *
     * @param id 主键ID
     * @return 选中的菜单和权限
     */
    public Result<SysRoleSelectMenuPermissionRESP> listSelectMenuAndPermission(Long id) {
        SysRoleSelectMenuPermissionRESP resp = new SysRoleSelectMenuPermissionRESP();

        // 获取关联菜单
        List<SysRoleMenu> roleMenuList = roleMenuDao.listByRoleId(id);
        if (CollectionUtil.isEmpty(roleMenuList)) {
            resp.setMenuList(new ArrayList<>());
        } else {
            resp.setMenuList(roleMenuList.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList()));
        }

        // 获取关联权限
        List<SysRolePermission> rolePermissionList = rolePermissionDao.listByRoleId(id);
        if (CollectionUtil.isEmpty(rolePermissionList)) {
            resp.setPermissionList(new ArrayList<>());
        } else {
            resp.setPermissionList(rolePermissionList.stream().map(SysRolePermission::getPermissionId).collect(Collectors.toList()));
        }
        return Result.success(resp);
    }

    /**
     * 分配菜单和权限
     *
     * @param sysRoleAllocationMenuAndPermissionREQ 分配参数
     * @return 分配结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> allocationMenuAndPermission(SysRoleAllocationMenuAndPermissionREQ sysRoleAllocationMenuAndPermissionREQ) {
        SysRole sysRole = dao.getById(sysRoleAllocationMenuAndPermissionREQ.getId());
        if (ObjectUtil.isNull(sysRole)) {
            return Result.error("角色不存在");
        }

        // 删除角色菜单关联
        roleMenuDao.deleteByRoleId(sysRoleAllocationMenuAndPermissionREQ.getId());

        // 删除角色权限关联
        rolePermissionDao.deleteByRoleId(sysRoleAllocationMenuAndPermissionREQ.getId());

        // 保存角色菜单关联
        if (CollectionUtil.isNotEmpty(sysRoleAllocationMenuAndPermissionREQ.getMenuIdList())) {
            for (Long menuId : sysRoleAllocationMenuAndPermissionREQ.getMenuIdList()) {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setRoleId(sysRoleAllocationMenuAndPermissionREQ.getId());
                sysRoleMenu.setMenuId(menuId);
                roleMenuDao.save(sysRoleMenu);
            }
        }

        // 保存角色权限关联
        if (CollectionUtil.isNotEmpty(sysRoleAllocationMenuAndPermissionREQ.getPermissionIdList())) {
            for (Long permissionId : sysRoleAllocationMenuAndPermissionREQ.getPermissionIdList()) {
                SysRolePermission sysRolePermission = new SysRolePermission();
                sysRolePermission.setRoleId(sysRoleAllocationMenuAndPermissionREQ.getId());
                sysRolePermission.setPermissionId(permissionId);
                rolePermissionDao.save(sysRolePermission);
            }
        }
        return Result.success("保存成功");
    }

    /**
     * 系统角色修改
     *
     * @param sysRoleEditREQ 系统角色修改对象
     * @return 修改结果
     */
    public Result<String> edit(SysRoleEditREQ sysRoleEditREQ) {
        SysRole record = BeanUtil.copyProperties(sysRoleEditREQ, SysRole.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统角色状态修改
     *
     * @param sysRoleUpdateStatusREQ 系统角色状态修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(SysRoleUpdateStatusREQ sysRoleUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(sysRoleUpdateStatusREQ.getStatusId()))) {
            return Result.error("传入状态异常");
        }
        if (ObjectUtil.isNull(dao.getById(sysRoleUpdateStatusREQ.getId()))) {
            return Result.error("角色不存在");
        }
        SysRole record = BeanUtil.copyProperties(sysRoleUpdateStatusREQ, SysRole.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统角色删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        SysRole sysRole = dao.getById(id);
        if (ObjectUtil.isNull(sysRole)) {
            return Result.error("角色不存在");
        }
        if (!AllowDeleteEnum.ALLOW.getCode().equals(sysRole.getAllowDelete())) {
            return Result.error("当前角色不允许删除");
        }
        // 判断是否存在用户角色关联
        int count = userRoleDao.countByRoleId(id);
        if (count > 0) {
            return Result.error("该角色被【" + count + "】用户使用，无法删除");
        }

        // 删除权限关联
        int rolePermissionCount = rolePermissionDao.countByRoleId(sysRole.getId());
        if (rolePermissionDao.deleteByRoleId(sysRole.getId()) != rolePermissionCount) {
            throw new BaseException("删除角色权限关联关系失败");
        }
        // 删除菜单关联
        int roleMenuCount = roleMenuDao.countByRoleId(sysRole.getId());
        if (roleMenuDao.deleteByRoleId(sysRole.getId()) != roleMenuCount) {
            throw new BaseException("删除角色菜单关联关系失败");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        throw new BaseException("删除角色失败");
    }
}
