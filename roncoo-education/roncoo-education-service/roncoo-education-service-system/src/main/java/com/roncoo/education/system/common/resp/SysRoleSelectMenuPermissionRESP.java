package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SysRoleSelectMenuPermissionRESP", description = "系统角色选中菜单权限")
public class SysRoleSelectMenuPermissionRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "选中菜单ID")
    private List<Long> menuList;

    @ApiModelProperty(value = "选中权限ID")
    private List<Long> permissionList;
}
