package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 站点导航文章
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="WebsiteNavArticleUpdateOrSaveByNavIdREQ", description="站点导航文章 编辑")
public class WebsiteNavArticleUpdateOrSaveByNavIdREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotNull(message = "导航ID不能为空")
    @ApiModelProperty(value = "导航ID",required = true)
    private Long navId;

    @ApiModelProperty(value = "文章标题")
    private String artTitle;

    @ApiModelProperty(value = "文章图片")
    private String artPic;

    @ApiModelProperty(value = "文章描述")
    private String artDesc;

    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型(1文章, 2外部链接)",required = true)
    private Integer type;

    @ApiModelProperty(value = "外部链接")
    private String url;
}
