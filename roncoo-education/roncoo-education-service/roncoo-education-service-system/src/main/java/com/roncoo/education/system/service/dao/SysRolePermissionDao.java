package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample;

import java.util.List;

/**
 * 系统角色权限 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysRolePermissionDao {

    int save(SysRolePermission record);

    int deleteById(Long id);

    int updateById(SysRolePermission record);

    SysRolePermission getById(Long id);

    Page<SysRolePermission> listForPage(int pageCurrent, int pageSize, SysRolePermissionExample example);

    /**
     * 根据角色ID获取权限集合
     *
     * @param roleId 角色ID
     * @return 角色权限关联集合
     */
    List<SysRolePermission> listByRoleId(Long roleId);

    /**
     * 根据权限ID获取权限集合
     *
     * @param permissionId 权限ID
     * @return 角色权限关联集合
     */
    List<SysRolePermission> listByPermissionId(Long permissionId);

    /**
     * 根据角色ID统计权限集合
     *
     * @param roleId 角色ID
     * @return 记录数
     */
    int countByRoleId(Long roleId);

    /**
     * 根据角色ID删除
     *
     * @param roleId 角色ID
     * @return 删除数量
     */
    int deleteByRoleId(Long roleId);


    /**
     * 根据角色ID集合获取角色菜单关联
     *
     * @param roleIdList 角色ID集合
     * @return 角色权限关联集合
     */
    List<SysRolePermission> listInRoleIdList(List<Long> roleIdList);

}
