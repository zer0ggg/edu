package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenuExample;

import java.util.List;

/**
 * 系统菜单 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysMenuDao {

    int save(SysMenu record);

    int deleteById(Long id);

    int updateById(SysMenu record);

    SysMenu getById(Long id);

    Page<SysMenu> listForPage(int pageCurrent, int pageSize, SysMenuExample example);

    /**
     * 列出所有菜单
     *
     * @return 系统菜单集合
     */
    List<SysMenu> listAll();

    /**
     * 列出所有可用菜单
     *
     * @return 系统菜单集合
     */
    List<SysMenu> listAllAvailable();

    /**
     * 根据父ID获取菜单
     *
     * @param parentId 父类ID
     * @return 系统菜单集合
     */
    List<SysMenu> listByParentId(Long parentId);

    /**
     * 根据ID集合和状态ID获取系统菜单
     *
     * @param idList   ID集合
     * @param statusId 状态ID
     * @return 系统菜单集合
     */
    List<SysMenu> listInIdListAndStatusId(List<Long> idList, Integer statusId);

}
