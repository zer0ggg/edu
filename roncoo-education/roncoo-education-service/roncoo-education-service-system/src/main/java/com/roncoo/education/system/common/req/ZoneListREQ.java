package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneListREQ", description="专区列表")
public class ZoneListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "名称")
    private String zoneName;

    @ApiModelProperty(value = "专区分类(1:点播;2:直播,4文库,5试卷,7博客;8:资讯,9:自定义，12：问答)")
    private Integer zoneCategory;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
