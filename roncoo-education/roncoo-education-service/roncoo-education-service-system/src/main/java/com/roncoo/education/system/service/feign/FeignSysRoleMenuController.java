package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysRoleMenu;
import com.roncoo.education.system.feign.qo.SysRoleMenuQO;
import com.roncoo.education.system.feign.vo.SysRoleMenuVO;
import com.roncoo.education.system.service.feign.biz.FeignSysRoleMenuBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统角色菜单
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysRoleMenuController extends BaseController implements IFeignSysRoleMenu{

    @Autowired
    private FeignSysRoleMenuBiz biz;

	@Override
	public Page<SysRoleMenuVO> listForPage(@RequestBody SysRoleMenuQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysRoleMenuQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysRoleMenuQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysRoleMenuVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
