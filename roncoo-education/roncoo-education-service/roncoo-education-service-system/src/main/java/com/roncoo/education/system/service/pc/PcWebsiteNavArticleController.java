package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.WebsiteNavArticleEditREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleListREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleSaveREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleUpdateOrSaveByNavIdREQ;
import com.roncoo.education.system.common.resp.WebsiteNavArticleListRESP;
import com.roncoo.education.system.common.resp.WebsiteNavArticleViewRESP;
import com.roncoo.education.system.service.pc.biz.PcWebsiteNavArticleBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 站点导航文章 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/website/nav/article")
@Api(value = "system-站点导航文章", tags = {"system-站点导航文章"})
public class PcWebsiteNavArticleController {

    @Autowired
    private PcWebsiteNavArticleBiz biz;

    @ApiOperation(value = "站点导航文章列表", notes = "站点导航文章列表")
    @PostMapping(value = "/list")
    public Result<Page<WebsiteNavArticleListRESP>> list(@RequestBody WebsiteNavArticleListREQ websiteNavArticleListREQ) {
        return biz.list(websiteNavArticleListREQ);
    }


    @ApiOperation(value = "站点导航文章添加", notes = "站点导航文章添加")
    @SysLog(value = "站点导航文章添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody WebsiteNavArticleSaveREQ websiteNavArticleSaveREQ) {
        return biz.save(websiteNavArticleSaveREQ);
    }

    @ApiOperation(value = "站点导航文章查看", notes = "站点导航文章查看")
    @GetMapping(value = "/view")
    public Result<WebsiteNavArticleViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "通过navid更新", notes = "通过navid更新")
    @SysLog(value = "通过navid更新")
    @PostMapping(value = "/update/navId")
    public Result<String> updateOrSaveByNavId(@RequestBody WebsiteNavArticleUpdateOrSaveByNavIdREQ req) {
        return biz.updateOrSaveByNavId(req);
    }

    @ApiOperation(value = "站点导航文章查看", notes = "站点导航文章查看")
    @GetMapping(value = "/view/navId")
    public Result<WebsiteNavArticleViewRESP> viewByNavId(@RequestParam Long id) {
        return biz.viewByNavId(id);
    }


    @ApiOperation(value = "站点导航文章修改", notes = "站点导航文章修改")
    @SysLog(value = "站点导航文章修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody WebsiteNavArticleEditREQ websiteNavArticleEditREQ) {
        return biz.edit(websiteNavArticleEditREQ);
    }


    @ApiOperation(value = "站点导航文章删除", notes = "站点导航文章删除")
    @SysLog(value = "站点导航文章删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
