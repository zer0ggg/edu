package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysUserLoginLog;
import com.roncoo.education.system.feign.qo.SysUserLoginLogQO;
import com.roncoo.education.system.feign.vo.SysUserLoginLogVO;
import com.roncoo.education.system.service.feign.biz.FeignSysUserLoginLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户登录日志
 *
 * @author LYQ
 * @date 2020-05-14
 */
@RestController
public class FeignSysUserLoginLogController extends BaseController implements IFeignSysUserLoginLog{

    @Autowired
    private FeignSysUserLoginLogBiz biz;

	@Override
	public Page<SysUserLoginLogVO> listForPage(@RequestBody SysUserLoginLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysUserLoginLogQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysUserLoginLogQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysUserLoginLogVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
