package com.roncoo.education.system.service.api;

import com.roncoo.education.system.common.bo.AliYunVodPlayAuthBO;
import com.roncoo.education.system.service.api.biz.CallbackAliYunBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 阿里云回调通知处理
 *
 * @author LYQ
 */
@RestController
@RequestMapping(value = "/callback/system/aliyun")
public class CallbackAliYunController {

    @Autowired
    private CallbackAliYunBiz biz;

    @ApiOperation(value = "视频上传回调处理")
    @PostMapping(value = "/upload")
    public void callbackUploadHandle(@RequestBody String paramStr, HttpServletRequest request, HttpServletResponse response) {
        biz.callbackHandle(paramStr, request, response);
    }

    @ApiOperation(value = "播放授权")
    @PostMapping(value = "/play/auth")
    public Integer playAuth(AliYunVodPlayAuthBO bo) {
        return biz.playAuth(bo);
    }
}
