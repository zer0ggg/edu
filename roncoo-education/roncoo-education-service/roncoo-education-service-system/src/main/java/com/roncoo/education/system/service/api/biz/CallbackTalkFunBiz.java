package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.TalkFunLiveCallbackEventEnum;
import com.roncoo.education.common.talk.TalkFunUtil;
import com.roncoo.education.common.talk.callback.CallbackResult;
import com.roncoo.education.course.feign.interfaces.IFeignCallbackTalkFunLive;
import com.roncoo.education.course.feign.qo.*;
import com.roncoo.education.system.feign.vo.ConfigTalkFunVO;
import com.roncoo.education.system.service.dao.SysConfigDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 回调时间通知
 *
 * @author LYQ
 */
@Slf4j
@Component
public class CallbackTalkFunBiz {

    @Autowired
    private SysConfigDao sysConfigDao;
    @Autowired
    private IFeignCallbackTalkFunLive feignCallbackTalkFunLive;

    /**
     * 通知处理
     *
     * @param callbackQO 通知参数
     * @return 处理结果
     */
    public CallbackResult<String> callbackNotify(TalkFunCallbackQO callbackQO) {
        log.info("欢拓通知事件参数：{}", JSON.toJSONString(callbackQO));
        // 获取欢拓配置
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        ConfigTalkFunVO sysVo = BeanUtil.mapToBean(configMap, ConfigTalkFunVO.class, true);
        if (ObjectUtil.isNull(sysVo) || StrUtil.isBlank(sysVo.getTalkFunOpenId()) || StrUtil.isBlank(sysVo.getTalkFunOpenToken())) {
            log.error("欢拓配置不存在，请配置");
            return CallbackResult.error();
        }

        Map<String, Object> paramMap = JSON.parseObject(JSON.toJSONString(callbackQO));
        String sign = TalkFunUtil.sign(paramMap, sysVo.getTalkFunOpenToken());
        if (!callbackQO.getSign().equals(sign)) {
            log.error("通知参数验签失败");
            return CallbackResult.error("验签失败");
        }

        boolean result = false;
        if (TalkFunLiveCallbackEventEnum.LIVE_START.getCode().equals(callbackQO.getCmd())) {
            //直播开启通知
            result = feignCallbackTalkFunLive.liveStart(JSON.parseObject(callbackQO.getParams(), TalkFunCallbackLiveStartQO.class));
        } else if (TalkFunLiveCallbackEventEnum.LIVE_STOP.getCode().equals(callbackQO.getCmd())) {
            // 直播结束通知
            result = feignCallbackTalkFunLive.liveStop(JSON.parseObject(callbackQO.getParams(), TalkFunCallbackLiveStopQO.class));
        } else if (TalkFunLiveCallbackEventEnum.LIVE_PLAYBACK.getCode().equals(callbackQO.getCmd())) {
            // 直播回放生成
            result = feignCallbackTalkFunLive.livePlayback(JSON.parseObject(callbackQO.getParams(), TalkFunCallbackLivePlaybackQO.class));
        } else if (TalkFunLiveCallbackEventEnum.LIVE_DOCUMENT_ADD.getCode().equals(callbackQO.getCmd())) {
            // 上传文档
            result = feignCallbackTalkFunLive.liveDocumentAdd(JSON.parseObject(callbackQO.getParams(), TalkFunCallbackLiveDocumentAddQO.class));
        } else {
            log.error("通知事件：[{}]暂未实现处理流程", callbackQO.getCmd());
        }

        // 返回
        return result ? CallbackResult.success() : CallbackResult.error();
    }
}
