package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 更新保利威授权验证信息
 */
@Data
@Accessors(chain = true)
public class AuthUpdatePolyvSignCode implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "userid")
    private String userid;

    @ApiModelProperty(value = "ptime")
    private Long ptime;

    @ApiModelProperty(value = "sign")
    private String sign;

    @ApiModelProperty(value = "hash")
    private String hash;
}
