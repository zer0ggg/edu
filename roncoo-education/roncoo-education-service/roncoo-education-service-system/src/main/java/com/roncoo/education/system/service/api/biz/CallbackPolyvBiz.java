package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.IsPayEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.common.polyv.PolyvAuth;
import com.roncoo.education.common.polyv.PolyvAuthResult;
import com.roncoo.education.common.polyv.PolyvCode;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.live.result.PLChannelgetLiveStatus;
import com.roncoo.education.course.feign.interfaces.*;
import com.roncoo.education.course.feign.qo.*;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblem;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.feign.vo.ExamProblemVO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Component
public class CallbackPolyvBiz extends BaseBiz {

    @Autowired
    private SysConfigDao sysConfigDao;

    @Autowired
    private IFeignCallbackPolyv feignCallbackPolyv;
    @Autowired
    private IFeignCallbackPolyvLive feignCallbackPolyvLive;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignCourseChapterPeriod feignCourseChapterPeriod;
    @Autowired
    private IFeignCourseChapter feignCourseChapter;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignExamProblem feignExamProblem;
    @Autowired
    private IFeignUserOrderExamRef feignUserOrderExamRef;

    /**
     * 保利威视，视频上传回调接口
     *
     * @param polyvVideoQO
     * @return
     */
    public String video(PolyvVideoQO polyvVideoQO) {
        return feignCallbackPolyv.callbackPolyvVideo(polyvVideoQO);
    }

    /**
     * 保利威视，视频授权播放回调接口 解密保利威视返回code值，获取到用户编号和课时编号
     *
     * @return
     * @author wuyun
     */
    public String auth(PolyvAuth polyvAuth, HttpServletRequest request) {
        logger.warn("视频授权播放回调={}", polyvAuth.toString());
		String code = replace(polyvAuth.getCode());

        if (StringUtils.isEmpty(polyvAuth.getCallback())) {
            String sourceParam = request.getQueryString();
            sourceParam = sourceParam.replaceAll("vid=" + polyvAuth.getVid(), "");
            sourceParam = sourceParam.replaceAll("&t=" + polyvAuth.getT(), "");
            sourceParam = sourceParam.replaceAll("&code=", "").replace("+", "%2B");
            polyvAuth.setCode(sourceParam);
        }
        PolyvAuthResult result = new PolyvAuthResult();
        PolyvCode polyvCode = PolyvUtil.decode(code);

        if (null == polyvCode) {
            logger.error("保利威授权播放失败，解密出错，参数={}", polyvAuth);
            result.setStatus(2);
            result.setMsg("系统异常，请重试");
            return resp(result, polyvAuth);
        }
        // 用户校验
        UserExtVO userExtVO = feignUserExt.getByUserNo(polyvCode.getUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            logger.error("保利威授权播放失败，用户账号不存在，参数={}", polyvCode);
            result.setStatus(2);
            result.setMsg("用户账号不存在");
            return resp(result, polyvAuth);
        }

        // 练习试题播放,试题都是免费，直接允许观看
        if (polyvCode.getExamId().equals(0L) && !polyvCode.getProblemId().equals(0L)) {
            // 支付成功
            result.setStatus(1);
            result.setMsg("允许播放");
            result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
            return resp(result, polyvAuth);
        }

        // 试卷试题播放
        if (!polyvCode.getExamId().equals(0L) && !polyvCode.getProblemId().equals(0L)) {
            return examAuth(result, polyvAuth, userExtVO, polyvCode.getExamId(), polyvCode.getProblemId());
        }
        // 课程播放
        return courseAuth(result, polyvAuth, userExtVO, polyvCode.getPeriodId());
    }

    /**
     * 试题播放
     * @param result
     * @param polyvAuth
     * @param userExtVO
     * @param examId
     * @param questionId
     * @return
     */
    private String examAuth(PolyvAuthResult result, PolyvAuth polyvAuth, UserExtVO userExtVO, Long examId, Long questionId) {
        ExamProblemVO examProblemVO = feignExamProblem.getById(questionId);
        if (ObjectUtil.isNull(examProblemVO)) {
            result.setStatus(2);
            result.setMsg("试题不存在");
            return resp(result, polyvAuth);
        }
        ExamInfoVO examInfoVO = feignExamInfo.getById(examId);
        if (ObjectUtil.isNull(examInfoVO)) {
            result.setStatus(2);
            result.setMsg("试卷不存在");
            return resp(result, polyvAuth);
        }

        if (IsFreeEnum.FREE.getCode().equals(examInfoVO.getIsFree()) || IsFreeEnum.FREE.getCode().equals(examProblemVO.getIsFree())) {
            // 支付成功
            result.setStatus(1);
            result.setMsg("允许播放");
            result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
            return resp(result, polyvAuth);
        }

        // 会员免费、用戶是会员
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), examInfoVO.getFabPrice())) {
            // 支付成功
            result.setStatus(1);
            result.setMsg("允许播放");
            result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
            return resp(result, polyvAuth);
        }

        UserOrderExamRefQO userOrderExamRefQO = new UserOrderExamRefQO();
        userOrderExamRefQO.setUserNo(userExtVO.getUserNo());
        userOrderExamRefQO.setExamId(examId);
        UserOrderExamRefVO userOrderExamRef = feignUserOrderExamRef.getByUserNoAndExamId(userOrderExamRefQO);
        if (ObjectUtil.isNull(userOrderExamRef)) {
            result.setStatus(2);
            result.setMsg("收费试卷，请先购买");
            return resp(result, polyvAuth);
        }
        // 支付成功
        result.setStatus(1);
        result.setMsg("允许播放");
        result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
        return resp(result, polyvAuth);
    }

    private String courseAuth(PolyvAuthResult result, PolyvAuth polyvAuth, UserExtVO userExtVO, Long periodId) {
        // 课时校验
        CourseChapterPeriodVO periodInfo = feignCourseChapterPeriod.getById(periodId);

        // 章节校验
        CourseChapterVO chapterInfo = feignCourseChapter.getById(periodInfo.getChapterId());

        // 课程校验
        CourseVO courseInfo = feignCourse.getById(periodInfo.getCourseId());

        // 课程、章节、课时任一个为免费该课时都可以免费观看
        if (IsFreeEnum.FREE.getCode().equals(periodInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(chapterInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(courseInfo.getIsFree())) {
            // 免费课时，可以播放
            result.setStatus(1);
            result.setMsg("该课时可以免费观看");
            result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
            return resp(result, polyvAuth);
        }

        // 收费课程
        // 是否购买了该课程
        Integer isPay;
        UserOrderCourseRefQO userOrderCourseRefQO = new UserOrderCourseRefQO();
        userOrderCourseRefQO.setUserNo(userExtVO.getUserNo());
        userOrderCourseRefQO.setRefId(periodInfo.getCourseId());
        UserOrderCourseRefVO userOrderCourseRef = feignUserOrderCourseRef.getByUserNoAndRefId(userOrderCourseRefQO);
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            // 未购买或者没支付情况
            isPay = IsPayEnum.NO.getCode();
        } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
            // 已过期情况
            isPay = IsPayEnum.NO.getCode();
        } else {
            // 订单状态为已支付
            isPay = IsPayEnum.YES.getCode();
        }
        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), courseInfo.getCourseDiscount())) {
            isPay = IsPayEnum.YES.getCode();
        }
        if (isPay.equals(IsPayEnum.NO.getCode())) {
            result.setStatus(2);
            result.setMsg("该课时为收费课程，请先购买");
            return resp(result, polyvAuth);
        }

        // 支付成功
        result.setStatus(1);
        result.setMsg("允许播放");
        result.setUsername(getUsername(userExtVO.getMobile(), userExtVO.getUserNo()));
        return resp(result, polyvAuth);
    }

    private String getUsername(String mobile, Long userNo) {
        return mobile;
    }

    /**
     * sign值校验
     *
     * @param sign
     * @param type
     * @param vid
     * @param secretkey
     * @return
     */
    public static boolean checkSign(String sign, String type, String vid, String secretkey) {
        StringBuilder sb = new StringBuilder();
        String argSign = MD5Util.MD5(sb.append("manage").append(type).append(vid).append(secretkey).toString());
        return argSign.equals(sign);
    }

    private String resp(PolyvAuthResult result, PolyvAuth polyvAuth) {
        // 判断是否开启跑马灯功能
        SysConfig polyvShowVO = sysConfigDao.getByConfigKeyAndStatusId("polyvShow", StatusIdEnum.YES.getCode());
        if (ObjectUtil.isNotEmpty(polyvShowVO)) {
            result.setShow(polyvShowVO.getConfigValue());
        }

        SysConfig sysVO = sysConfigDao.getByConfigKeyAndStatusId("polyvSecretkey", StatusIdEnum.YES.getCode());
        String sign = replace("vid=" + polyvAuth.getVid() + "&secretkey=" + sysVO.getConfigValue() + "&username=" + result.getUsername() + "&code=" + polyvAuth.getCode() + "&status=" + result.getStatus() + "&t=" + polyvAuth.getT());
        result.setSign(MD5Util.MD5(sign));
        StringBuffer sb = new StringBuffer();
        sb.append(polyvAuth.getCallback()).append("(").append(JSUtil.toJSONString(result)).append(")");
        logger.warn("保利威视，播放授权回调接口，返回报文={}", sb);
        return sb.toString();
    }

    /**
     * 保利威视，直播状态回调接口
     */
    public String callbackPolyvLiveStatus(PLChannelgetLiveStatus plChannelgetLiveStatus) {
        PolyvLiveStatusQO polyvLiveStatusQO = new PolyvLiveStatusQO();
        polyvLiveStatusQO.setChannelId(plChannelgetLiveStatus.getChannelId());
        polyvLiveStatusQO.setStatus(plChannelgetLiveStatus.getStatus());
        polyvLiveStatusQO.setSessionId(plChannelgetLiveStatus.getSessionId());
        return feignCallbackPolyvLive.callbackPolyvLiveStatus(polyvLiveStatusQO);
    }

    /**
     * 保利威视，视频授权播放接口
     */
    public String callbackPolyvLiveAuth(String userid, Long ts, String token) {
        PolyvLiveAuthQO polyvLiveAuthQO = new PolyvLiveAuthQO();
        polyvLiveAuthQO.setUserid(userid);
        polyvLiveAuthQO.setTs(ts);
        polyvLiveAuthQO.setToken(token);
        return feignCallbackPolyvLive.callbackPolyvLiveAuth(polyvLiveAuthQO);
    }

    /**
     * 保利威视，回放生成回调通知接口
     */
    public String callbackLiveFileUrl(String channelId, String fileUrl, String fileId) {
        PolyvLiveFileUrlQO polyvLiveFileUrlQO = new PolyvLiveFileUrlQO();
        polyvLiveFileUrlQO.setChannelId(channelId);
        polyvLiveFileUrlQO.setFileUrl(fileUrl);
        polyvLiveFileUrlQO.setFileId(fileId);
        return feignCallbackPolyvLive.callbackLiveFileUrl(polyvLiveFileUrlQO);

    }

    /**
     * 保利威视，转存成功回调通知接口
     */
    public String callbackLiveSave(String channelId, String vid, String title, String duration, String sessionId, String videoId) {
        PolyvLiveSaveQO polyvLiveSaveQO = new PolyvLiveSaveQO();
        polyvLiveSaveQO.setChannelId(channelId);
        polyvLiveSaveQO.setVid(vid);
        polyvLiveSaveQO.setTitle(title);
        polyvLiveSaveQO.setDuration(duration);
        polyvLiveSaveQO.setSessionId(sessionId);
        polyvLiveSaveQO.setVideoId(videoId);
        return feignCallbackPolyvLive.callbackLiveSave(polyvLiveSaveQO);
    }

    /**
     * 保利威视，重制课件完成回调通知接口
     *
     * @param channelId 频道号
     * @param title     重制对应的回放名称
     * @param remainDay 重制剩余的有效期
     * @param url       重制视频下载地址，会有防盗链，有效期为24小时
     * @return
     */
    public String callbackLiveRemakes(String channelId, String title, String remainDay, String url, String sessionId) {
        PolyvLiveRemakesQO polyvLiveRemakesQO = new PolyvLiveRemakesQO();
        polyvLiveRemakesQO.setChannelId(channelId);
        polyvLiveRemakesQO.setTitle(title);
        polyvLiveRemakesQO.setRemainDay(remainDay);
        polyvLiveRemakesQO.setUrl(url);
        polyvLiveRemakesQO.setSessionId(sessionId);
        return feignCallbackPolyvLive.callbackLiveRemakes(polyvLiveRemakesQO);
    }

    /**
     * 保利威播放回调参数问题，需要替换/
     *
     * @param input
     * @return
     */
    public static String replace(String input) {
        input = input.replaceAll("\\+", "%2B");
        return input.replaceAll("/", "%2F");
    }
}
