package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * app更新
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AppUpdateBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "版本号")
    private BigDecimal version;
}
