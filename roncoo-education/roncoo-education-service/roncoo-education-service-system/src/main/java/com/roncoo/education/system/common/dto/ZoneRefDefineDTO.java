package com.roncoo.education.system.common.dto;

import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 专区关联
 * </p>
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefDefineDTO", description="专区关联  自定义信息")
public class ZoneRefDefineDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "专区ID")
    private Long zoneId;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "外部链接")
    private String url;

    @ApiModelProperty(value = "图片")
    private String img;

    @ApiModelProperty(value = "课程(点播/直播/文库)")
    private CourseVO course;

    @ApiModelProperty(value = "试卷")
    private ExamInfoVO exam;

    @ApiModelProperty(value = "博客/资讯")
    private BlogVO blog;
}
