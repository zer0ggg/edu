package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AllowDeleteEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysPermissionEditREQ;
import com.roncoo.education.system.common.req.SysPermissionListREQ;
import com.roncoo.education.system.common.req.SysPermissionSaveREQ;
import com.roncoo.education.system.common.req.SysPermissionUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysPermissionListRESP;
import com.roncoo.education.system.common.resp.SysPermissionViewRESP;
import com.roncoo.education.system.service.dao.SysMenuDao;
import com.roncoo.education.system.service.dao.SysPermissionDao;
import com.roncoo.education.system.service.dao.SysRolePermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample.Criteria;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统权限
 *
 * @author wujing
 */
@Component
public class PcSysPermissionBiz extends BaseBiz {

    @Autowired
    private SysPermissionDao dao;
    @Autowired
    private SysMenuDao menuDao;
    @Autowired
    private SysRolePermissionDao sysRolePermissionDao;

    /**
     * 系统权限列表
     *
     * @param sysPermissionListREQ 系统权限分页查询参数
     * @return 系统权限分页查询结果
     */
    public Result<Page<SysPermissionListRESP>> list(SysPermissionListREQ sysPermissionListREQ) {
        SysPermissionExample example = new SysPermissionExample();
        Criteria c = example.createCriteria();
        Page<SysPermission> page = dao.listForPage(sysPermissionListREQ.getPageCurrent(), sysPermissionListREQ.getPageSize(), example);
        Page<SysPermissionListRESP> respPage = PageUtil.transform(page, SysPermissionListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 系统权限添加
     *
     * @param sysPermissionSaveREQ 系统权限
     * @return 添加结果
     */
    public Result<String> save(SysPermissionSaveREQ sysPermissionSaveREQ) {
        if (ObjectUtil.isNull(menuDao.getById(sysPermissionSaveREQ.getMenuId()))) {
            return Result.error("菜单不存在");
        }

        SysPermission record = BeanUtil.copyProperties(sysPermissionSaveREQ, SysPermission.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 系统权限查看
     *
     * @param id 主键ID
     * @return 系统权限
     */
    public Result<SysPermissionViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysPermissionViewRESP.class));
    }


    /**
     * 系统权限修改
     *
     * @param sysPermissionEditREQ 系统权限修改对象
     * @return 修改结果
     */
    public Result<String> edit(SysPermissionEditREQ sysPermissionEditREQ) {
        SysPermission record = BeanUtil.copyProperties(sysPermissionEditREQ, SysPermission.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统权限状态修改
     *
     * @param sysPermissionUpdateStatusREQ 系统权限状态修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(SysPermissionUpdateStatusREQ sysPermissionUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(sysPermissionUpdateStatusREQ.getStatusId()))) {
            return Result.error("传入权限状态异常");
        }
        if (ObjectUtil.isNull(dao.getById(sysPermissionUpdateStatusREQ.getId()))) {
            return Result.error("权限点不存在");
        }
        SysPermission record = BeanUtil.copyProperties(sysPermissionUpdateStatusREQ, SysPermission.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统权限删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        SysPermission sysPermission = dao.getById(id);
        if (ObjectUtil.isNull(sysPermission)) {
            return Result.error("权限不存在");
        }
        if (!AllowDeleteEnum.ALLOW.getCode().equals(sysPermission.getAllowDelete())) {
            return Result.error("当前权限不允许删除");
        }
        List<SysRolePermission> rolePermissionList = sysRolePermissionDao.listByPermissionId(id);
        if (CollUtil.isNotEmpty(rolePermissionList)) {
            return Result.error("权限已分配到角色，请先解绑");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
