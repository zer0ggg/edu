package com.roncoo.education.system.service.dao.impl.mapper;

import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ZoneRefMapper {
    int countByExample(ZoneRefExample example);

    int deleteByExample(ZoneRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ZoneRef record);

    int insertSelective(ZoneRef record);

    List<ZoneRef> selectByExample(ZoneRefExample example);

    ZoneRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ZoneRef record, @Param("example") ZoneRefExample example);

    int updateByExample(@Param("record") ZoneRef record, @Param("example") ZoneRefExample example);

    int updateByPrimaryKeySelective(ZoneRef record);

    int updateByPrimaryKey(ZoneRef record);
}
