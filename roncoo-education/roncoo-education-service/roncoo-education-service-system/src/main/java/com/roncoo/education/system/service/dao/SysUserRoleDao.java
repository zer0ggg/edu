package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample;

import java.util.List;

/**
 * 系统用户角色 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysUserRoleDao {

    int save(SysUserRole record);

    int deleteById(Long id);

    int updateById(SysUserRole record);

    SysUserRole getById(Long id);

    Page<SysUserRole> listForPage(int pageCurrent, int pageSize, SysUserRoleExample example);

    /**
     * 根据用户ID获取用户角色关联
     *
     * @param userId 用户ID
     * @return 用户角色关联
     */
    List<SysUserRole> listByUserId(Long userId);

    /**
     * 根据用户ID进行统计
     *
     * @param userId 用户ID
     * @return 统计结果
     */
    int countByUserId(Long userId);

    /**
     * 根据角色ID进行统计
     *
     * @param roleId 角色ID
     * @return 统计结果
     */
    int countByRoleId(Long roleId);

    /**
     * 根据用户ID删除用户角色关联
     *
     * @param userId 用户ID
     * @return 删除记录数
     */
    int deleteByUserId(Long userId);

}
