package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleExample;

import java.util.List;

/**
 * 系统角色 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysRoleDao {

    int save(SysRole record);

    int deleteById(Long id);

    int updateById(SysRole record);

    SysRole getById(Long id);

    Page<SysRole> listForPage(int pageCurrent, int pageSize, SysRoleExample example);

    /**
     * 根据角色值获取角色
     *
     * @param roleValue 角色值
     * @return 角色
     */
    SysRole getByRoleValue(String roleValue);

    /**
     * 根据ID集合和状态ID获取角色集合
     *
     * @param idList      ID集合
     * @param statusId 状态ID
     * @return 角色集合
     */
    List<SysRole> listInIdListAndStatusId(List<Long> idList, Integer statusId);

}
