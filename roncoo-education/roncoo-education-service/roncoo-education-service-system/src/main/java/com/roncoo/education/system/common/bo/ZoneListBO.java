package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ZoneBO", description = "专区")
public class ZoneListBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "专区分类(1:点播;2:直播,4文库,5试卷,7博客;8:资讯,9:自定义)")
	private Integer zoneCategory;

	@NotNull(message = "位置(1电脑端，2微信端)不能为空")
	@ApiModelProperty(value = "位置(1电脑端，2微信端)", required = true)
	private Integer zoneLocation;

	@ApiModelProperty(value = "当前页")
	private int pageCurrent = 1;

	@ApiModelProperty(value = "每页条数")
	private int pageSize = 20;
}
