package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.system.service.dao.SensitiveWordLibraryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 广告信息
 *
 * @author wujing
 */
@Component
public class FeignSensitiveWordLibraryBiz {

	@Autowired
	private SensitiveWordLibraryDao dao;

	public List<String> listAll() {
		return dao.listAll();
	}
}
