package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 获取上传参数
 */
@Data
@Accessors(chain = true)
public class AuthWebUploadVideoGetCodeBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户编号", required = true)
    private Long userNo;

}
