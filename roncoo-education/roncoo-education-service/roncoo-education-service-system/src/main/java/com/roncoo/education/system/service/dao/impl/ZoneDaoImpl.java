package com.roncoo.education.system.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.ZoneDao;
import com.roncoo.education.system.service.dao.impl.mapper.ZoneMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 专区 服务实现类
 *
 * @author wujing
 * @date 2020-05-23
 */
@Repository
public class ZoneDaoImpl implements ZoneDao {

    @Autowired
    private ZoneMapper mapper;

    @Override
    public int save(Zone record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Zone record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Zone getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Zone> listForPage(int pageCurrent, int pageSize, ZoneExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public Zone getByZoneLocationAndzoneCategoryAndZoneName(Integer zoneLocation, Integer zoneCategory, String zoneName) {
        ZoneExample example = new ZoneExample();
        ZoneExample.Criteria c = example.createCriteria();
        c.andZoneLocationEqualTo(zoneLocation);
        c.andZoneCategoryEqualTo(zoneCategory);
        c.andZoneNameEqualTo(zoneName);
        List<Zone> list = this.mapper.selectByExample(example);
        if(CollectionUtil.isEmpty(list)){
            return null;
        }
        return list.get(0);
    }


}
