package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.SysRoleListRESP;
import com.roncoo.education.system.common.resp.SysRoleSelectMenuPermissionRESP;
import com.roncoo.education.system.common.resp.SysRoleViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysRoleBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 系统角色 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/role")
@Api(value = "system-系统角色", tags = { "system-系统角色" })
public class PcSysRoleController {

	@Autowired
	private PcSysRoleBiz biz;

	@ApiOperation(value = "系统角色列表", notes = "系统角色列表")
	@PostMapping(value = "/list")
	public Result<Page<SysRoleListRESP>> list(@RequestBody SysRoleListREQ sysRoleListREQ) {
		return biz.list(sysRoleListREQ);
	}


	@ApiOperation(value = "系统角色添加", notes = "系统角色添加")
	@SysLog(value = "系统角色添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody @Valid SysRoleSaveREQ sysRoleSaveREQ) {
		return biz.save(sysRoleSaveREQ);
	}

	@ApiOperation(value = "系统角色查看", notes = "系统角色查看")
	@GetMapping(value = "/view")
	public Result<SysRoleViewRESP> view(@RequestParam Long id) {
		return biz.view(id);
	}


	@ApiOperation(value = "系统角色修改", notes = "系统角色修改")
	@SysLog(value = "系统角色修改")
	@PutMapping(value = "/edit")
	public Result<String> edit(@RequestBody @Valid SysRoleEditREQ sysRoleEditREQ) {
		return biz.edit(sysRoleEditREQ);
	}


	@ApiOperation(value = "系统角色状态修改", notes = "系统角色状态修改")
	@SysLog(value = "系统角色状态修改")
	@PutMapping(value = "/update/status")
	public Result<String> updateStatus(@RequestBody @Valid SysRoleUpdateStatusREQ sysRoleUpdateStatusREQ) {
		return biz.updateStatus(sysRoleUpdateStatusREQ);
	}


	@ApiOperation(value = "系统角色删除", notes = "系统角色删除")
	@SysLog(value = "系统角色删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}


	@ApiOperation(value = "获取已分配的菜单和权限", notes = "获取已分配的菜单和权限")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "角色ID", dataType = "Long", required = true) })
	@GetMapping(value = "/select")
	public Result<SysRoleSelectMenuPermissionRESP> listSelectMenuAndPermission(@RequestParam Long id) {
		return biz.listSelectMenuAndPermission(id);
	}


	@ApiOperation(value = "系统角色菜单和权限分配", notes = "系统角色菜单和权限分配")
	@SysLog(value = "系统角色菜单和权限分配")
	@PostMapping(value = "/allocation")
	public Result<String> allocationMenuAndPermission(@RequestBody SysRoleAllocationMenuAndPermissionREQ sysRoleAllocationMenuAndPermissionREQ) {
		return biz.allocationMenuAndPermission(sysRoleAllocationMenuAndPermissionREQ);
	}
}
