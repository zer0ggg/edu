package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysUserLoginLogDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysUserLoginLogMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLog;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 系统用户登录日志 服务实现类
 *
 * @author LYQ
 * @date 2020-05-14
 */
@Repository
public class SysUserLoginLogDaoImpl implements SysUserLoginLogDao {

    @Autowired
    private SysUserLoginLogMapper mapper;

    @Override
    public int save(SysUserLoginLog record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysUserLoginLog record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysUserLoginLog getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }
    
    @Override
    public Page<SysUserLoginLog> listForPage(int pageCurrent, int pageSize, SysUserLoginLogExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent((int)count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage((int)count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<SysUserLoginLog>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }
    

}
