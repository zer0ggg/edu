package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.PlatEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.system.common.WxMaAppUtil;
import com.roncoo.education.system.common.bo.WebsiteBO;
import com.roncoo.education.system.common.bo.WebsiteReferralCodeBO;
import com.roncoo.education.system.common.dto.WebsiteDTO;
import com.roncoo.education.system.feign.vo.ConfigWeixinVO;
import com.roncoo.education.system.service.dao.SysConfigDao;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 站点信息
 *
 * @author Quanf
 */
@Component
public class ApiWebsiteBiz extends BaseBiz {

    @Autowired
    private SysConfigDao sysConfigDao;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

//	@Autowired
//	private CacheRedis cacheRedis;

    public Result<WebsiteDTO> get(WebsiteBO bo) {
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        WebsiteDTO dto = BeanUtil.mapToBean(configMap, WebsiteDTO.class, true);
        if (PlatEnum.PC.getCode().equals(bo.getPlatShow())) {
            dto.setPolyvAppID("");
            dto.setPolyvAppSecret("");
        }
        if (StringUtils.hasText(dto.getPrn())) { // 公安网备案号处理
            String regEx = "[^0-9]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(dto.getPrn());
            dto.setPrnNo(m.replaceAll("").trim());
        }
        return Result.success(dto);
    }


    @Transactional(rollbackFor = Exception.class)
    public Result<String> referralCode(WebsiteReferralCodeBO bo) {
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        ConfigWeixinVO configWeixinVO = cn.hutool.core.bean.BeanUtil.mapToBean(configMap, ConfigWeixinVO.class, true);

        if (ObjectUtil.isNull(configWeixinVO)) {
            return Result.error("找不到系统配置信息");
        }
        if (StringUtils.isEmpty(configWeixinVO.getMinappAppId()) || StringUtils.isEmpty(configWeixinVO.getMinappAppSecret())) {
            return Result.error("appid或者appSecret没配置");
        }
        try {
            File file;
            if (StringUtils.isEmpty(bo.getScene())) {
                file = WxMaAppUtil.getService(configWeixinVO.getMinappAppId(), configWeixinVO.getMinappAppSecret(), WxMaAppUtil.AESKEY, stringRedisTemplate).getQrcodeService().createWxaCode(bo.getPage());

            } else {
                file = WxMaAppUtil.getService(configWeixinVO.getMinappAppId(), configWeixinVO.getMinappAppSecret(), WxMaAppUtil.AESKEY, stringRedisTemplate).getQrcodeService().createWxaCodeUnlimit(bo.getScene(), bo.getPage());
            }
            InputStream in = null;
            byte[] data;
            // 读取图片字节数组
            try {
                in = new FileInputStream(file);
                data = new byte[in.available()];
                int count = 0;
                while ((count = in.read(data)) > 0) {
                    // 对字节数组Base64编码
                    return Result.success("data:image/" + file.getName().toLowerCase() + ";base64, "+ Base64Utils.encodeToString(data));
                }
            } catch (IOException e) {
                logger.error("系统错误", e);
            } finally {
                in.close();
            }
        } catch (IOException | WxErrorException e) {
            logger.warn("获取二维码失败", e);
        }
        return Result.error("获取二维码失败");
    }

}
