package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.enums.ZoneCategoryEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.interfaces.IFeignQuestions;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.community.feign.vo.QuestionsVO;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.LiveCourseVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamCategory;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.vo.ExamCategoryVO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.system.common.bo.ZoneListBO;
import com.roncoo.education.system.common.dto.*;
import com.roncoo.education.system.service.dao.ZoneDao;
import com.roncoo.education.system.service.dao.ZoneRefDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 专区
 *
 * @author wujing
 */
@Component
public class ApiZoneBiz extends BaseBiz {

    @Autowired
    private ZoneDao dao;
    @Autowired
    private ZoneRefDao refDao;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignExamCategory feignExamCategory;
    @Autowired
    private IFeignBlog feignBlog;
    @Autowired
    private IFeignQuestions feignQuestions;

    public Result<Page<ZoneListDTO>> listForPage(ZoneListBO bo) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(bo);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ZoneExample example = new ZoneExample();
        ZoneExample.Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        if (!StringUtils.isEmpty(bo.getId())) {
            c.andIdEqualTo(bo.getId());
        }
        if (bo.getZoneLocation() != null) {
            c.andZoneLocationEqualTo(bo.getZoneLocation());
        }
        if (bo.getZoneCategory() != null) {
            c.andZoneCategoryEqualTo(bo.getZoneCategory());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<Zone> zonePage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        if (StringUtils.isEmpty(zonePage)) {
            return Result.error("找不到信息");
        }
        Page<ZoneListDTO> page = PageUtil.transform(zonePage, ZoneListDTO.class);
        // 专区--专区关联--“课程”
        //遍历专区
        for (ZoneListDTO zone : page.getList()) {
            if (zone.getProductId() != null && ZoneCategoryEnum.LIVE.getCode().equals(zone.getZoneCategory())) {
                LiveCourseVO course = feignCourse.getLiveCourseWithTimeByIdAndStatusId(zone.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(course)) {
                    zone.setCourseDTO(BeanUtil.copyProperties(course, CourseDTO.class));
                }
            }
            if (zone.getProductId() != null && ZoneCategoryEnum.ORDINARY.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(zone.getZoneCategory())) {
                CourseVO course = feignCourse.getByIdAndStatusId(zone.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(course)) {
                    zone.setCourseDTO(BeanUtil.copyProperties(course, CourseDTO.class));
                }
            } else if (zone.getProductId() != null && ZoneCategoryEnum.EXAM.getCode().equals(zone.getZoneCategory())) {
                ExamInfoVO exam = feignExamInfo.getByIdAndStatusId(zone.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(exam)) {
                    ExamInfoDTO dto = BeanUtil.copyProperties(exam, ExamInfoDTO.class);
                    zone.setExamInfoDTO(dto);
                }
            } else if (zone.getProductId() != null && ZoneCategoryEnum.INFOMATION.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.Blog.getCode().equals(zone.getZoneCategory())) {
                BlogVO blog = feignBlog.getByIdAndStatusId(zone.getProductId(), StatusIdEnum.YES.getCode());
                if (!ObjectUtils.isEmpty(blog)) {
                    zone.setBlogViewDTO(BeanUtil.copyProperties(blog, BlogViewDTO.class));
                }
            }

            List<ZoneRef> zoneRefList = refDao.listByZoneIdAndStatusId(zone.getId(), StatusIdEnum.YES.getCode());
            //遍历专区关联
            if (CollectionUtils.isEmpty(zoneRefList)) {
                continue;
            }
            List<ZoneRefDTO> zoneRefListDTO = BeanUtil.copyProperties(zoneRefList, ZoneRefDTO.class);
            List<CourseDTO> courseList = new ArrayList<>();
            List<QuestionsDTO> questionsList = new ArrayList<>();
            List<ExamInfoDTO> examList = new ArrayList<>();
            List<BlogViewDTO> blogList = new ArrayList<>();
            List<ZoneRefDefineDTO> defineList = new ArrayList<>();
            Map<Long, ExamCategoryVO> examCategorys = new HashMap<>();
            for (ZoneRefDTO refDto : zoneRefListDTO) {
                //填充课程信息
                if (ZoneCategoryEnum.LIVE.getCode().equals(zone.getZoneCategory())) {
                    LiveCourseVO course = feignCourse.getLiveCourseWithTimeByIdAndStatusId(refDto.getCourseId(), StatusIdEnum.YES.getCode());
                    if (!ObjectUtils.isEmpty(course)) {
                        courseList.add(BeanUtil.copyProperties(course, CourseDTO.class));
                    }
                }
                if (ZoneCategoryEnum.ORDINARY.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.RESOURCE.getCode().equals(zone.getZoneCategory())) {
                    CourseVO course = feignCourse.getByIdAndStatusId(refDto.getCourseId(), StatusIdEnum.YES.getCode());
                    if (!ObjectUtils.isEmpty(course)) {
                        courseList.add(BeanUtil.copyProperties(course, CourseDTO.class));
                    }
                } else if (ZoneCategoryEnum.EXAM.getCode().equals(zone.getZoneCategory())) {
                    ExamInfoVO exam = feignExamInfo.getByIdAndStatusId(refDto.getCourseId(), StatusIdEnum.YES.getCode());
                    if (!ObjectUtils.isEmpty(exam)) {
                        ExamInfoDTO dto = BeanUtil.copyProperties(exam, ExamInfoDTO.class);
                        setExamCategory(dto, examCategorys);
                        examList.add(dto);
                    }
                } else if (ZoneCategoryEnum.INFOMATION.getCode().equals(zone.getZoneCategory()) || ZoneCategoryEnum.Blog.getCode().equals(zone.getZoneCategory())) {
                    BlogVO blog = feignBlog.getByIdAndStatusId(refDto.getCourseId(), StatusIdEnum.YES.getCode());
                    if (!ObjectUtils.isEmpty(blog)) {
                        blogList.add(BeanUtil.copyProperties(blog, BlogViewDTO.class));
                    }
                } else if (ZoneCategoryEnum.DEFINITION.getCode().equals(zone.getZoneCategory())) {
                    defineList.add(BeanUtil.copyProperties(refDto, ZoneRefDefineDTO.class));
                } else if (ZoneCategoryEnum.QUESTIONS.getCode().equals(zone.getZoneCategory())) {
                    QuestionsVO questionsVO = feignQuestions.getById(refDto.getCourseId());
                    if (ObjectUtil.isNotNull(questionsVO)) {
                        QuestionsDTO questionsDTO = BeanUtil.copyProperties(questionsVO, QuestionsDTO.class);
                        questionsList.add(questionsDTO);
                    }
                }
            }
            zone.setCourseList(courseList);
            zone.setExamList(examList);
            zone.setBlogList(blogList);
            zone.setDefineList(defineList);
            zone.setQuestionsList(questionsList);
        }
        return Result.success(page);
    }

    /**
     * 填装分类
     *
     * @param exam
     * @param examCategorys
     */
    private void setExamCategory(ExamInfoDTO exam, Map<Long, ExamCategoryVO> examCategorys) {
        if (exam.getSubjectId() == null || exam.getSubjectId() == 0) {
            return;
        }
        // 第一级
        ExamCategoryVO examCategory = null;
        examCategory = examCategorys.get(exam.getSubjectId());
        if (examCategory == null) {
            examCategory = feignExamCategory.getById(exam.getSubjectId());
        }
        if (ObjectUtil.isNotNull(examCategory)) {
            examCategorys.put(examCategory.getId(), examCategory);
            exam.setSubjectName(examCategory.getCategoryName());
        }

        // 第二级
        if (examCategory == null || examCategory.getParentId() == 0) {
            return;
        }
        ExamCategoryVO examCategoryParent = examCategorys.get(examCategory.getParentId());
        if (examCategoryParent == null) {
            examCategoryParent = feignExamCategory.getById(examCategory.getParentId());
        }
        if (ObjectUtil.isNotNull(examCategoryParent)) {
            examCategorys.put(examCategoryParent.getId(), examCategoryParent);
            exam.setParentSubjectId(examCategoryParent.getId());
            exam.setParentSubjectName(examCategoryParent.getCategoryName());
        }
        // 第三级
        if (examCategoryParent == null || examCategoryParent.getParentId() == 0) {
            return;
        }
        ExamCategoryVO examCategoryGrant = examCategorys.get(examCategory.getParentId());
        if (examCategoryGrant == null) {
            examCategoryGrant = feignExamCategory.getById(examCategory.getParentId());
        }
        if (ObjectUtil.isNotNull(examCategoryGrant)) {
            examCategorys.put(examCategoryGrant.getId(), examCategoryGrant);
            exam.setGrantSubjectId(examCategoryGrant.getId());
            exam.setGrantSubjectName(examCategoryGrant.getCategoryName());
        }
    }
}
