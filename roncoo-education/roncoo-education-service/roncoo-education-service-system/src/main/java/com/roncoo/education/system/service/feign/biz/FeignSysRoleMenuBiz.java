package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysRoleMenuQO;
import com.roncoo.education.system.feign.vo.SysRoleMenuVO;
import com.roncoo.education.system.service.dao.SysRoleMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统角色菜单
 *
 * @author wujing
 */
@Component
public class FeignSysRoleMenuBiz extends BaseBiz {

    @Autowired
    private SysRoleMenuDao dao;

	public Page<SysRoleMenuVO> listForPage(SysRoleMenuQO qo) {
	    SysRoleMenuExample example = new SysRoleMenuExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysRoleMenu> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysRoleMenuVO.class);
	}

	public int save(SysRoleMenuQO qo) {
		SysRoleMenu record = BeanUtil.copyProperties(qo, SysRoleMenu.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysRoleMenuVO getById(Long id) {
		SysRoleMenu record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysRoleMenuVO.class);
	}

	public int updateById(SysRoleMenuQO qo) {
		SysRoleMenu record = BeanUtil.copyProperties(qo, SysRoleMenu.class);
		return dao.updateById(record);
	}

}
