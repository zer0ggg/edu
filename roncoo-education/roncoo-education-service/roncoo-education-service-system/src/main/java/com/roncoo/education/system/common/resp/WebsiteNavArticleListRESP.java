package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 站点导航文章
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="WebsiteNavArticleListRESP", description="站点导航文章列表")
public class WebsiteNavArticleListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "导航ID")
    private Long navId;

    @ApiModelProperty(value = "文章标题")
    private String artTitle;

    @ApiModelProperty(value = "文章图片")
    private String artPic;

    @ApiModelProperty(value = "文章描述")
    private String artDesc;

    @ApiModelProperty(value = "类型(1文章, 2外部链接)")
    private Integer type;

    @ApiModelProperty(value = "外部链接")
    private String url;
}
