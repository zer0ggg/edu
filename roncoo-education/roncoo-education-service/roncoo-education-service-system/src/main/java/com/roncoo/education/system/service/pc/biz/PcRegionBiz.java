package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.RegionEditREQ;
import com.roncoo.education.system.common.req.RegionListREQ;
import com.roncoo.education.system.common.req.RegionSaveREQ;
import com.roncoo.education.system.common.resp.RegionListRESP;
import com.roncoo.education.system.common.resp.RegionViewRESP;
import com.roncoo.education.system.service.dao.RegionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Region;
import com.roncoo.education.system.service.dao.impl.mapper.entity.RegionExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.RegionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 行政区域
 *
 * @author wujing
 */
@Component
public class PcRegionBiz extends BaseBiz {

    @Autowired
    private RegionDao dao;

    /**
    * 行政区域列表
    *
    * @param regionListREQ 行政区域分页查询参数
    * @return 行政区域分页查询结果
    */
    public Result<Page<RegionListRESP>> list(RegionListREQ regionListREQ) {
        RegionExample example = new RegionExample();
        Criteria c = example.createCriteria();
        Page<Region> page = dao.listForPage(regionListREQ.getPageCurrent(), regionListREQ.getPageSize(), example);
        Page<RegionListRESP> respPage = PageUtil.transform(page, RegionListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 行政区域添加
    *
    * @param regionSaveREQ 行政区域
    * @return 添加结果
    */
    public Result<String> save(RegionSaveREQ regionSaveREQ) {
        Region record = BeanUtil.copyProperties(regionSaveREQ, Region.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 行政区域查看
    *
    * @param id 主键ID
    * @return 行政区域
    */
    public Result<RegionViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), RegionViewRESP.class));
    }


    /**
    * 行政区域修改
    *
    * @param regionEditREQ 行政区域修改对象
    * @return 修改结果
    */
    public Result<String> edit(RegionEditREQ regionEditREQ) {
        Region record = BeanUtil.copyProperties(regionEditREQ, Region.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 行政区域删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
