package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 广告信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AdvBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 位置(0电脑端，1微信端)
	 */
	@ApiModelProperty(value = "位置(1电脑端，2微信端)")
	private Integer platShow;

	/**
	 * 广告位置(1首页轮播图；2顶部活动；3:首页资讯轮播图)
	 */
	@ApiModelProperty(value = "广告位置(1首页轮播图；2顶部活动；3:首页资讯轮播图)")
	private Integer advLocation;

	@ApiModelProperty(value = "移动端类型(1:小程序端,2:H5) 电脑端不需要传")
	private Integer mobileTerminalCategory;

}
