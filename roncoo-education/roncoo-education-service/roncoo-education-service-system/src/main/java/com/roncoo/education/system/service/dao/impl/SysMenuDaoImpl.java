package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysMenuMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenuExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统菜单 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysMenuDaoImpl implements SysMenuDao {

    @Autowired
    private SysMenuMapper mapper;

    @Override
    public int save(SysMenu record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysMenu record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysMenu getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysMenu> listForPage(int pageCurrent, int pageSize, SysMenuExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        example.setOrderByClause("sort asc, id asc");
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<SysMenu> listAll() {
        SysMenuExample example = new SysMenuExample();
        example.setOrderByClause("sort asc, id asc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysMenu> listAllAvailable() {
        SysMenuExample example = new SysMenuExample();
        SysMenuExample.Criteria criteria = example.createCriteria();
        criteria.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause("sort asc, id asc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysMenu> listByParentId(Long parentId) {
        SysMenuExample example = new SysMenuExample();
        SysMenuExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        example.setOrderByClause("sort asc, id asc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysMenu> listInIdListAndStatusId(List<Long> idList, Integer statusId) {
        SysMenuExample example = new SysMenuExample();
        SysMenuExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(idList);
        criteria.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id asc");
        return this.mapper.selectByExample(example);
    }


}
