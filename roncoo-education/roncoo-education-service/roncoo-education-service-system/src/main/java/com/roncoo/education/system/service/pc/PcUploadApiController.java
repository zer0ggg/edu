/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.dto.AuthUpdatePolyvSignCode;
import com.roncoo.education.system.common.dto.AuthWebUploadVideoGetCode;
import com.roncoo.education.system.service.api.auth.biz.UploadApiBiz;
import com.roncoo.education.system.service.pc.biz.PcUploadApiBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传接口
 *
 * @author wuyun
 */
@Api(tags = "system-上传接口")
@RestController
@RequestMapping(value = "/system/pc/upload")
public class PcUploadApiController extends BaseController {

    @Autowired
    private PcUploadApiBiz biz;

    /**
     * 上传图片接口
     *
     * @param picFile
     * @author wuyun
     */
    @ApiOperation(value = "上传图片接口", notes = "上传图片")
    @RequestMapping(value = "/pic", method = RequestMethod.POST)
    public Result<String> uploadPic(@RequestParam(value = "picFile", required = false) MultipartFile picFile) {
        return biz.uploadPic(picFile);
    }

    /**
     * 上传文档接口
     *
     * @param docFile
     * @author wuyun
     */
    @ApiOperation(value = "上传文档接口", notes = "上传文档")
    @RequestMapping(value = "/doc", method = RequestMethod.POST)
    public Result<String> uploadDoc(@RequestParam(name = "docFile", required = false) MultipartFile docFile) {
        return biz.uploadDoc(docFile);
    }

    /**
     * 获取上传参数
     */
    @ApiOperation(value = "获取上传参数", notes = "获取上传参数")
    @RequestMapping(value = "/aliyun", method = RequestMethod.POST)
    public Result<AuthWebUploadVideoGetCode> getCode() {
        return biz.getCode();
    }

    /**
     * 更新保利威授权验证信息
     */
    @ApiOperation(value = "更新保利威授权验证信息", notes = "更新保利威授权验证信息")
    @RequestMapping(value = "/polyv", method = RequestMethod.POST)
    public Result<AuthUpdatePolyvSignCode> updateSign() {
        return biz.updateSign();
    }

}
