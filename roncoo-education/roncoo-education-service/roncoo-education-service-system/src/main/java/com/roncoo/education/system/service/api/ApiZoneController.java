package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.bo.ZoneListBO;
import com.roncoo.education.system.common.dto.ZoneListDTO;
import com.roncoo.education.system.service.api.biz.ApiZoneBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * 专区 Api接口
 *
 * @author wujing
 * @date 2020-05-23
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping("/system/api/zone")
public class ApiZoneController {

    @Autowired
    private ApiZoneBiz biz;

    @Cacheable
    @ApiOperation(value = "专区列表接口", notes = "专区列表接口")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<Page<ZoneListDTO>> listForPage(@RequestBody ZoneListBO bo) {
        return biz.listForPage(bo);
    }

}
