package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SharingTemplateEditREQ;
import com.roncoo.education.system.common.req.SharingTemplateListREQ;
import com.roncoo.education.system.common.req.SharingTemplateSaveREQ;
import com.roncoo.education.system.common.req.SharingTemplateUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SharingTemplateListRESP;
import com.roncoo.education.system.common.resp.SharingTemplateViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSharingTemplateBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 分享图片模板 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-分享图片模板")
@RestController
@RequestMapping("/system/pc/sharing/template")
public class PcSharingTemplateController {

    @Autowired
    private PcSharingTemplateBiz biz;

    @ApiOperation(value = "分享图片模板列表", notes = "分享图片模板列表")
    @PostMapping(value = "/list")
    public Result<Page<SharingTemplateListRESP>> list(@RequestBody SharingTemplateListREQ sharingTemplateListREQ) {
        return biz.list(sharingTemplateListREQ);
    }

    @SysLog(value = "分享图片模板添加")
    @ApiOperation(value = "分享图片模板添加", notes = "分享图片模板添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody SharingTemplateSaveREQ sharingTemplateSaveREQ) {
        return biz.save(sharingTemplateSaveREQ);
    }

    @ApiOperation(value = "分享图片模板查看", notes = "分享图片模板查看")
    @GetMapping(value = "/view")
    public Result<SharingTemplateViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @SysLog(value = "分享图片模板修改")
    @ApiOperation(value = "分享图片模板修改", notes = "分享图片模板修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody SharingTemplateEditREQ sharingTemplateEditREQ) {
        return biz.edit(sharingTemplateEditREQ);
    }

    @SysLog(value = "分享图片模板删除")
    @ApiOperation(value = "分享图片模板删除", notes = "分享图片模板删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @SysLog(value = "分享图片模板状态修改")
    @ApiOperation(value = "分享图片模板状态修改", notes = "参数配置状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody @Valid SharingTemplateUpdateStatusREQ sharingTemplateUpdateStatusREQ) {
        return biz.updateStatus(sharingTemplateUpdateStatusREQ);
    }

}
