package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value="AuthWebsiteReferralCodeBO", description="生成推荐码")
public class WebsiteReferralCodeBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "跳转页面")
    private String page;

    @ApiModelProperty(value = "跳转页面携带参数")
    private String scene;



}
