package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.bo.NavBarListBO;
import com.roncoo.education.system.common.dto.NavBarListDTO;
import com.roncoo.education.system.service.api.biz.ApiNavBarBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 头部导航
 *
 * @author wuyun
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping(value = "/system/api/nav/bar")
public class ApiNavBarController extends BaseController {

	@Autowired
	private ApiNavBarBiz biz;

	/**
	 * 获取头部导航信息接口
	 *
	 * @return 站点信息
	 * @author wuyun
	 */
	@Cacheable
	@ApiOperation(value = "获取头部导航信息接口", notes = "获取头部导航信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<NavBarListDTO> list(@RequestBody NavBarListBO bo) {
		return biz.list(bo);
	}

}
