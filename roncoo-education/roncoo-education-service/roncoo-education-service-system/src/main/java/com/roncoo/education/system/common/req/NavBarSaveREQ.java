package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 头部导航
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="NavBarSaveREQ", description="头部导航添加")
public class NavBarSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "位置(1电脑端，2微信端)不能为空")
    @ApiModelProperty(value = "位置(1电脑端，2微信端)" ,required = true)
    private Integer platShow;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "导航标题")
    private String navTitle;

    @ApiModelProperty(value = "导航url")
    private String navUrl;

    @ApiModelProperty(value = "跳转方式")
    private String target;

    @ApiModelProperty(value = "备注")
    private String remark;
}
