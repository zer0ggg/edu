package com.roncoo.education.system.service.api.biz;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.aliyun.AliYunVodCode;
import com.roncoo.education.common.aliyun.AliYunVodUtil;
import com.roncoo.education.common.aliyun.enums.CallbackEventTypeEnum;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.IsPayEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.course.feign.interfaces.*;
import com.roncoo.education.course.feign.qo.AliYunVideoAnalysisCompleteQO;
import com.roncoo.education.course.feign.qo.AliYunVideoUploadComplateQO;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblem;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.feign.vo.ExamProblemVO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.system.common.bo.AliYunVodPlayAuthBO;
import com.roncoo.education.system.feign.vo.ConfigAliYunVodVO;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * 阿里云回调处理
 *
 * @author LYQ
 */
@Slf4j
@Component
public class CallbackAliYunBiz {

    @Autowired
    private SysConfigDao sysConfigDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Autowired
    private IFeignCallbackAliYun feignCallbackAliYun;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignCourseChapter feignCourseChapter;
    @Autowired
    private IFeignCourseChapterPeriod feignCourseChapterPeriod;
    @Autowired
    private IFeignUserOrderCourseRef feignUserOrderCourseRef;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignExamProblem feignExamProblem;
    @Autowired
    private IFeignUserOrderExamRef feignUserOrderExamRef;

    /**
     * 回调处理
     *
     * @param paramStr 回调参数
     * @param request  回调请求
     * @param response 回调响应
     */
    public void callbackHandle(String paramStr, HttpServletRequest request, HttpServletResponse response) {
        log.info("回调参数：{}", paramStr);
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        ConfigAliYunVodVO sysVo = BeanUtil.mapToBean(configMap, ConfigAliYunVodVO.class, true);
        if (ObjectUtil.isNull(sysVo)) {
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return;
        }
        Boolean resultStatus = AliYunVodUtil.verifyCallbackSign(request, sysVo.getAliYunVodCallbackUrl(), sysVo.getAliYunVodAuthKey());
        if (!resultStatus) {
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return;
        }

        boolean result;

        // 获取视频
        JSONObject paramJson = JSON.parseObject(paramStr);
        String eventType = paramJson.getString("EventType");
        if (CallbackEventTypeEnum.FILE_UPLOAD_COMPLETE.getCode().equals(eventType)) {
            // 上传完成处理
            result = feignCallbackAliYun.uploadComplete(JSON.parseObject(paramStr, AliYunVideoUploadComplateQO.class));
        } else if (CallbackEventTypeEnum.VIDEO_ANALYSIS_COMPLETE.getCode().equals(eventType)) {
            // 视频分析完成
            result = feignCallbackAliYun.videoAnalysisComplete(JSON.parseObject(paramStr, AliYunVideoAnalysisCompleteQO.class));
        } else {
            // 对于其他类型，默认当处理成功
            result = true;
        }

        log.info("阿里云通知处理结果：{}", result ? "处理成功" : "处理失败");
        if (result) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    /**
     * 播放授权
     *
     * @param bo 播放授权请求参数
     * @return 校验结果：1:为允许播放 0：为不允许播放
     */
    public Integer playAuth(AliYunVodPlayAuthBO bo) {
        if (StrUtil.isBlank(bo.getToken())) {
            return 0;
        }

        String redisValue = myRedisTemplate.get(RedisPreEnum.ALI_YUN_VOD_PLAY.getCode() + bo.getToken());
        if (StrUtil.isBlank(redisValue)) {
            return 0;
        }
        AliYunVodCode aliYunVodCode = JSON.parseObject(redisValue, AliYunVodCode.class);

        // 用户校验
        UserExtVO userExtVO = feignUserExt.getByUserNo(aliYunVodCode.getUserNo());
        if (null == userExtVO) {
            log.error("阿里云授权播放失败，用户账号不存在，参数={}", aliYunVodCode);
            return 0;
        }

        boolean result;
        if (!aliYunVodCode.getExamId().equals(0L) && !aliYunVodCode.getProblemId().equals(0L)) {
            result = examAuth(userExtVO, aliYunVodCode.getExamId(), aliYunVodCode.getProblemId());
        } else {
            result = courseAuth(userExtVO, aliYunVodCode.getPeriodId());
        }

        // 删除缓存--凡是使用过的token都应该删除
        myRedisTemplate.delete(RedisPreEnum.ALI_YUN_VOD_PLAY.getCode() + bo.getToken());
        return result ? 1 : 0;
    }

    /**
     * 试题视频播放校验
     *
     * @param userExtVO  用户教育信息
     * @param examId     试卷ID
     * @param questionId 试题ID
     * @return 校验结果
     */
    private boolean examAuth(UserExtVO userExtVO, Long examId, Long questionId) {
        ExamProblemVO examProblemVO = feignExamProblem.getById(questionId);
        if (ObjectUtil.isNull(examProblemVO)) {
            return false;
        }
        ExamInfoVO examInfoVO = feignExamInfo.getById(examId);
        if (ObjectUtil.isNull(examInfoVO)) {
            return false;
        }

        if (IsFreeEnum.FREE.getCode().equals(examInfoVO.getIsFree()) || IsFreeEnum.FREE.getCode().equals(examProblemVO.getIsFree())) {
            // 支付成功
            return true;
        }

        // 会员免费、用戶是会员
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), examInfoVO.getFabPrice())) {
            // 支付成功
            return true;
        }

        UserOrderExamRefQO qo = new UserOrderExamRefQO();
        qo.setUserNo(userExtVO.getUserNo());
        qo.setExamId(examId);
        UserOrderExamRefVO userOrderExamRef = feignUserOrderExamRef.getByUserNoAndExamId(qo);
        // 表示支付成功
        return ObjectUtil.isNotNull(userOrderExamRef);
    }

    /**
     * 课程视频播放校验
     *
     * @param userExtVO 用户教育信息
     * @param periodId  课时ID
     * @return 校验结果
     */
    private boolean courseAuth(UserExtVO userExtVO, Long periodId) {
        // 课时校验
        CourseChapterPeriodVO periodInfo = feignCourseChapterPeriod.getById(periodId);

        // 章节校验
        CourseChapterVO chapterInfo = feignCourseChapter.getById(periodInfo.getChapterId());

        // 课程校验
        CourseVO courseInfo = feignCourse.getById(periodInfo.getCourseId());

        // 课程、章节、课时任一个为免费该课时都可以免费观看
        if (IsFreeEnum.FREE.getCode().equals(periodInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(chapterInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(courseInfo.getIsFree())) {
            // 免费课时，可以播放
            return true;
        }

        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), courseInfo.getCourseDiscount())) {
            return true;
        }

        // 收费课程
        // 是否购买了该课程
        UserOrderCourseRefQO qo = new UserOrderCourseRefQO();
        qo.setUserNo(userExtVO.getUserNo());
        qo.setRefId(periodInfo.getCourseId());
        UserOrderCourseRefVO userOrderCourseRef = feignUserOrderCourseRef.getByUserNoAndRefId(qo);
        if (ObjectUtil.isNull(userOrderCourseRef) || !IsPayEnum.YES.getCode().equals(userOrderCourseRef.getIsPay())) {
            // 未购买或者没支付情况
            return false;
        } else {
            // 未过期返回true，过期返回false
            return new Date().before(userOrderCourseRef.getExpireTime());
        }

    }
}
