package com.roncoo.education.system.common.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AdvEditREQ", description="广告信息修改")
public class AdvEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "广告标题")
    private String advTitle;

    @ApiModelProperty(value = "广告链接")
    private String advUrl;

    @ApiModelProperty(value = "广告图片")
    private String advImg;

    @ApiModelProperty(value = "广告跳转方式")
    private String advTarget;

    @ApiModelProperty(value = "课程分类(1:点播;2:直播,4:文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @NotNull(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @NotNull(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间",required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

}
