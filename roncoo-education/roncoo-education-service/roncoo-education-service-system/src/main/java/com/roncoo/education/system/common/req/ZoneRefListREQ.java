package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefListREQ", description="专区关联列表")
public class ZoneRefListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @NotNull(message = "专区ID不能为空")
    @ApiModelProperty(value = "专区ID",required = true)
    private Long zoneId;

    @NotNull(message = "位置(1电脑端，2微信端)不能为空")
    @ApiModelProperty(value = "位置(1电脑端，2微信端)",required = true)
    private Integer zoneLocation;

    @ApiModelProperty(value = "标题")
    private String title;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
