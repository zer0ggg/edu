package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibrary;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SensitiveWordLibraryExample;

import java.util.List;

public interface SensitiveWordLibraryDao {
    int save(SensitiveWordLibrary record);

    int deleteById(Long id);

    int updateById(SensitiveWordLibrary record);

    SensitiveWordLibrary getById(Long id);

    Page<SensitiveWordLibrary> listForPage(int pageCurrent, int pageSize, SensitiveWordLibraryExample example);

    List<String> listAll();

    /**
     * 根据敏感词获取敏感词信息
     *
     * @param badword
     * @return
     */
    SensitiveWordLibrary getByBadword(String badword);
}
