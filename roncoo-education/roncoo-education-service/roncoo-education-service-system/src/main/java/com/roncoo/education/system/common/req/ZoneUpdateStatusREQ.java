package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneUpdateStatusREQ", description="专区修改状态")
public class ZoneUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @NotNull(message = "状态(1有效, 0无效)不能为空")
    @ApiModelProperty(value = "状态(1有效, 0无效)",required = true)
    private Integer statusId;
}
