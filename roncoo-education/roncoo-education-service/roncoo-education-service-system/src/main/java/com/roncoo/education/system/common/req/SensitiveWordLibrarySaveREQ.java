package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 敏感词列出
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SensitiveWordLibraryREQ", description="敏感词列出")
public class SensitiveWordLibrarySaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "敏感词")
    private String badword;
}
