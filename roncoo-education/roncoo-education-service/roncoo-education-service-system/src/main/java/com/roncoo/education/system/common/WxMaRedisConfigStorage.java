package com.roncoo.education.system.common;

import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public class WxMaRedisConfigStorage extends WxMaDefaultConfigImpl {

	private StringRedisTemplate stringRedisTemplate;

	private String accessTokenKey;

	private String jsapiTicketKey;

	private String cardapiTicketKey;

	public WxMaRedisConfigStorage(StringRedisTemplate stringRedisTemplate, String appId) {
		super.setAppid(appId);
		this.stringRedisTemplate = stringRedisTemplate;
		this.accessTokenKey = RedisPreEnum.MINIAPP_ACCESS_TOKEN_KEY.getCode().concat(appId);
		this.jsapiTicketKey = RedisPreEnum.MINIAPP_JSAPI_TICKET_KEY.getCode().concat(appId);
		this.cardapiTicketKey = RedisPreEnum.MINIAPP_CARDAPI_TICKET_KEY.getCode().concat(appId);
	}

	@Override
	public String getAccessToken() {
		return stringRedisTemplate.opsForValue().get(this.accessTokenKey);
	}

	@Override
	public boolean isAccessTokenExpired() {
		return stringRedisTemplate.getExpire(this.accessTokenKey, TimeUnit.SECONDS) < 2;
	}

	@Override
	public synchronized void updateAccessToken(String accessToken, int expiresInSeconds) {
		stringRedisTemplate.opsForValue().set(this.accessTokenKey, accessToken, expiresInSeconds - 200L, TimeUnit.SECONDS);
	}

	@Override
	public void expireAccessToken() {
		stringRedisTemplate.delete(this.accessTokenKey);
	}

	@Override
	public String getJsapiTicket() {
		return stringRedisTemplate.opsForValue().get(this.jsapiTicketKey);
	}

	@Override
	public boolean isJsapiTicketExpired() {
		return stringRedisTemplate.getExpire(this.jsapiTicketKey, TimeUnit.SECONDS) < 2;
	}

	@Override
	public synchronized void updateJsapiTicket(String jsapiTicket, int expiresInSeconds) {
		stringRedisTemplate.opsForValue().set(this.jsapiTicketKey, jsapiTicket, expiresInSeconds - 200L, TimeUnit.SECONDS);
	}

	@Override
	public void expireJsapiTicket() {
		stringRedisTemplate.delete(this.jsapiTicketKey);
	}

	@Override
	public String getCardApiTicket() {
		return stringRedisTemplate.opsForValue().get(this.cardapiTicketKey);
	}

	@Override
	public boolean isCardApiTicketExpired() {
		return stringRedisTemplate.getExpire(this.cardapiTicketKey, TimeUnit.SECONDS) < 2;
	}

	@Override
	public synchronized void updateCardApiTicket(String cardApiTicket, int expiresInSeconds) {
		stringRedisTemplate.opsForValue().set(this.cardapiTicketKey, cardapiTicketKey, expiresInSeconds - 200L, TimeUnit.SECONDS);
	}

	@Override
	public void expireCardApiTicket() {
		stringRedisTemplate.delete(this.cardapiTicketKey);
	}
}
