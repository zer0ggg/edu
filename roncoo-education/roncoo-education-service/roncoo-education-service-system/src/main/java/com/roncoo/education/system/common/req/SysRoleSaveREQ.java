package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SysRoleSaveREQ", description = "系统角色添加")
public class SysRoleSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty(value = "角色名称", required = true)
    private String roleName;

    @NotBlank(message = "角色标识不能为空")
    @ApiModelProperty(value = "角色标识", required = true)
    private String roleValue;

    @ApiModelProperty(value = "备注")
    private String remark;
}
