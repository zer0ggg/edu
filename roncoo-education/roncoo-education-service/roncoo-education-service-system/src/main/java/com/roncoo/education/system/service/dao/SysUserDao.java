package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUser;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserExample;

/**
 * 系统用户 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysUserDao {

    int save(SysUser record);

    int deleteById(Long id);

    int updateById(SysUser record);

    SysUser getById(Long id);

    Page<SysUser> listForPage(int pageCurrent, int pageSize, SysUserExample example);

    /**
     * 根据登录名称查找用户信息
     *
     * @param loginName 登录账号
     * @return 系统用户信息
     */
    SysUser getByLoginName(String loginName);
}
