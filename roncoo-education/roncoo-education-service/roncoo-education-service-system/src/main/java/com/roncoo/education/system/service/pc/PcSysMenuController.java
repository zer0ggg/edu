package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.SysMenuEditREQ;
import com.roncoo.education.system.common.req.SysMenuSaveREQ;
import com.roncoo.education.system.common.req.SysMenuUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysMenuPermissionListRESP;
import com.roncoo.education.system.common.resp.SysMenuViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysMenuBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 系统菜单 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/sys/menu")
@Api(value = "system-系统菜单", tags = { "system-系统菜单" })
public class PcSysMenuController {

	@Autowired
	private PcSysMenuBiz biz;

	@ApiOperation(value = "系统菜单列表", notes = "系统菜单列表")
	@GetMapping(value = "/list")
	public Result<List<SysMenuPermissionListRESP>> list(@RequestParam Long parentId) {
		return biz.list(parentId);
	}

	@ApiOperation(value = "系统可用菜单列表", notes = "系统可用菜单列表")
	@GetMapping(value = "/available/list")
	public Result<List<SysMenuPermissionListRESP>> availableList() {
		return biz.availableList();
	}

	@ApiOperation(value = "系统菜单添加", notes = "系统菜单添加")
	@SysLog(value = "系统菜单添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody @Valid SysMenuSaveREQ sysMenuSaveREQ) {
		return biz.save(sysMenuSaveREQ);
	}

	@ApiOperation(value = "系统菜单查看", notes = "系统菜单查看")
	@GetMapping(value = "/view")
	public Result<SysMenuViewRESP> view(@RequestParam Long id) {
		return biz.view(id);
	}


	@ApiOperation(value = "系统菜单修改", notes = "系统菜单修改")
	@SysLog(value = "系统菜单修改")
	@PutMapping(value = "/edit")
	public Result<String> edit(@RequestBody @Valid SysMenuEditREQ sysMenuEditREQ) {
		return biz.edit(sysMenuEditREQ);
	}


	@ApiOperation(value = "系统菜单状态修改", notes = "系统菜单状态修改")
	@SysLog(value = "系统菜单状态修改")
	@PutMapping(value = "/update/status")
	public Result<String> updateStatus(@RequestBody @Valid SysMenuUpdateStatusREQ sysMenuUpdateStatusREQ) {
		return biz.updateStatus(sysMenuUpdateStatusREQ);
	}


	@ApiOperation(value = "系统菜单删除", notes = "系统菜单删除")
	@SysLog(value = "系统菜单删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}
}
