package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.AdvEditREQ;
import com.roncoo.education.system.common.req.AdvListREQ;
import com.roncoo.education.system.common.req.AdvSaveREQ;
import com.roncoo.education.system.common.req.AdvUpdateStatusREQ;
import com.roncoo.education.system.common.resp.AdvListRESP;
import com.roncoo.education.system.common.resp.AdvViewRESP;
import com.roncoo.education.system.service.pc.biz.PcAdvBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 广告信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/adv")
@Api(value = "system-广告信息", tags = { "system-广告信息" })
public class PcAdvController {

	@Autowired
	private PcAdvBiz biz;

	@ApiOperation(value = "广告信息列表", notes = "广告信息列表")
	@PostMapping(value = "/list")
	public Result<Page<AdvListRESP>> list(@RequestBody AdvListREQ advListREQ) {
		return biz.list(advListREQ);
	}


	@ApiOperation(value = "广告信息添加", notes = "广告信息添加")
	@SysLog(value = "广告信息添加")
	@PostMapping(value = "/save")
	public Result<String> save(@RequestBody AdvSaveREQ advSaveREQ) {
		return biz.save(advSaveREQ);
	}

	@ApiOperation(value = "广告信息查看", notes = "广告信息查看")
	@GetMapping(value = "/view")
	public Result<AdvViewRESP> view(@RequestParam Long id) {
		return biz.view(id);
	}


	@ApiOperation(value = "广告信息修改", notes = "广告信息修改")
	@SysLog(value = "广告信息修改")
	@PutMapping(value = "/edit")
	public Result<String> edit(@RequestBody AdvEditREQ advEditREQ) {
		return biz.edit(advEditREQ);
	}


	@ApiOperation(value = "广告信息删除", notes = "广告信息删除")
	@SysLog(value = "广告信息删除")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam Long id) {
		return biz.delete(id);
	}


	@ApiOperation(value = "状态修改", notes = "状态修改")
	@SysLog(value = "状态修改")
	@PutMapping(value = "/update/status")
	public Result<String> updateStatus(@RequestBody @Valid AdvUpdateStatusREQ req) {
		return biz.updateStatus(req);
	}


	@ApiOperation(value = "清空缓存", notes = "清空缓存")
	@SysLog(value = "清空缓存")
	@PutMapping(value = "/delete/redis")
	public Result<String> deleteRedis() {
		return biz.deleteRedis();
	}

}
