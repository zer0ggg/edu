package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.WebsiteLinkEditREQ;
import com.roncoo.education.system.common.req.WebsiteLinkListREQ;
import com.roncoo.education.system.common.req.WebsiteLinkSaveREQ;
import com.roncoo.education.system.common.resp.WebsiteLinkListRESP;
import com.roncoo.education.system.common.resp.WebsiteLinkViewRESP;
import com.roncoo.education.system.service.pc.biz.PcWebsiteLinkBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 站点友情链接 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/website/link")
@Api(value = "system-站点友情链接", tags = {"system-站点友情链接"})
public class PcWebsiteLinkController {

    @Autowired
    private PcWebsiteLinkBiz biz;

    @ApiOperation(value = "站点友情链接列表", notes = "站点友情链接列表")
    @PostMapping(value = "/list")
    public Result<Page<WebsiteLinkListRESP>> list(@RequestBody WebsiteLinkListREQ websiteLinkListREQ) {
        return biz.list(websiteLinkListREQ);
    }


    @ApiOperation(value = "站点友情链接添加", notes = "站点友情链接添加")
    @SysLog(value = "站点友情链接添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody WebsiteLinkSaveREQ websiteLinkSaveREQ) {
        return biz.save(websiteLinkSaveREQ);
    }

    @ApiOperation(value = "站点友情链接查看", notes = "站点友情链接查看")
    @GetMapping(value = "/view")
    public Result<WebsiteLinkViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "站点友情链接修改", notes = "站点友情链接修改")
    @SysLog(value = "站点友情链接修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody WebsiteLinkEditREQ websiteLinkEditREQ) {
        return biz.edit(websiteLinkEditREQ);
    }


    @ApiOperation(value = "站点友情链接删除", notes = "站点友情链接删除")
    @SysLog(value = "站点友情链接删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "清空缓存", notes = "清空缓存")
    @SysLog(value = "清空缓存")
    @PutMapping(value = "/delete/redis")
    public Result<String> deleteRedis() {
        return biz.deleteRedis();
    }

}
