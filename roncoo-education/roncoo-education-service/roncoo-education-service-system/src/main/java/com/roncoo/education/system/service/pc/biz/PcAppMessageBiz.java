package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsSendEnum;
import com.roncoo.education.common.core.enums.JumpCategoryEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.AppPushUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.community.feign.interfaces.IFeignBlog;
import com.roncoo.education.community.feign.vo.BlogVO;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.system.common.req.AppMessageEditREQ;
import com.roncoo.education.system.common.req.AppMessageListREQ;
import com.roncoo.education.system.common.req.AppMessageSaveREQ;
import com.roncoo.education.system.common.req.AppMessageSendREQ;
import com.roncoo.education.system.common.resp.AppMessageListRESP;
import com.roncoo.education.system.common.resp.AppMessageViewRESP;
import com.roncoo.education.system.service.dao.AppMessageDao;
import com.roncoo.education.system.service.dao.SysConfigDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessage;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample.Criteria;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * app推送
 *
 * @author wujing
 */
@Component
public class PcAppMessageBiz extends BaseBiz {

    @Autowired
    private AppMessageDao dao;
    @Autowired
    private IFeignCourse feignCourse;
    @Autowired
    private IFeignExamInfo feignExamInfo;
    @Autowired
    private IFeignBlog feignBlog;
    @Autowired
    private SysConfigDao sysConfigDao;

    /**
     * app推送列表
     *
     * @param req app推送分页查询参数
     * @return app推送分页查询结果
     */
    public Result<Page<AppMessageListRESP>> list(AppMessageListREQ req) {
        AppMessageExample example = new AppMessageExample();
        Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getTitle())) {
            c.andTitleLike(PageUtil.like(req.getTitle()));
        }
        if (req.getJumpCategory() != null) {
            c.andJumpCategoryEqualTo(req.getJumpCategory());
        }
        if (req.getIsSend() != null) {
            c.andIsSendEqualTo(req.getIsSend());
        }
        Page<AppMessage> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<AppMessageListRESP> respPage = PageUtil.transform(page, AppMessageListRESP.class);
        return Result.success(respPage);
    }


    /**
     * app推送添加
     *
     * @param req app推送
     * @return 添加结果
     */
    public Result<String> save(AppMessageSaveREQ req) {
        AppMessage record = BeanUtil.copyProperties(req, AppMessage.class);
        //点播、直播、文库
        if (JumpCategoryEnum.ORDINARY.getCode().equals(req.getJumpCategory()) || JumpCategoryEnum.LIVE.getCode().equals(req.getJumpCategory()) || JumpCategoryEnum.RESOURCE.getCode().equals(req.getJumpCategory())) {
            CourseVO courseVO = feignCourse.getById(req.getProductId());
            if (ObjectUtil.isNull(courseVO)) {
                return Result.error("找不到课程信息");
            }
            record.setProductName(courseVO.getCourseName());
        } else if (JumpCategoryEnum.EXAM.getCode().equals(req.getJumpCategory())) {
            ExamInfoVO examInfoVO = feignExamInfo.getById(req.getProductId());
            if (ObjectUtil.isNull(examInfoVO)) {
                return Result.error("找不到试卷信息");
            }
            record.setProductName(examInfoVO.getExamName());
        } else if (JumpCategoryEnum.INFOMATION.getCode().equals(req.getJumpCategory())) {
            BlogVO BlogVO = feignBlog.getById(req.getProductId());
            if (ObjectUtil.isNull(BlogVO)) {
                return Result.error("找不到资讯信息");
            }
            record.setProductName(BlogVO.getTitle());
        }

        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        if (StringUtils.isEmpty(configMap.get("uniPushAppId")) || StringUtils.isEmpty(configMap.get("uniPushAppKey")) || StringUtils.isEmpty(configMap.get("masterSecret"))) {
            return Result.error("未配置APPUniPush参数");
        }
        if (req.getIsPromptlySend().equals(1)) {
            Map<String, Object> t = new TreeMap<>();
            t.put("productId", String.valueOf(record.getProductId()));
            t.put("jumpCategory", record.getJumpCategory());
            AppPushUtil.AppPush(configMap.get("uniPushAppId"), configMap.get("uniPushAppKey"), configMap.get("masterSecret"), record.getTitle(), record.getContent(), JSUtil.toJSONString(t));
            record.setIsSend(IsSendEnum.YES.getCode());
            record.setSendTime(new Date());
        } else {
            record.setIsSend(IsSendEnum.NO.getCode());
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * app推送查看
     *
     * @param id 主键ID
     * @return app推送
     */
    public Result<AppMessageViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), AppMessageViewRESP.class));
    }


    /**
     * app推送修改
     *
     * @param req app推送修改对象
     * @return 修改结果
     */
    public Result<String> edit(AppMessageEditREQ req) {
        AppMessage appMessage = dao.getById(req.getId());
        if (ObjectUtil.isNull(appMessage)) {
            return Result.error("找不到推送信息");
        }
        AppMessage record = BeanUtil.copyProperties(req, AppMessage.class);
        if (req.getIsPromptlySend() != null && req.getIsPromptlySend().equals(1)) {
            Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
            if (StringUtils.isEmpty(configMap.get("uniPushAppId")) || StringUtils.isEmpty(configMap.get("uniPushAppKey")) || StringUtils.isEmpty(configMap.get("masterSecret"))) {
                return Result.error("未配置APPUniPush参数");
            }
            Map<String, Object> t = new TreeMap<>();
            t.put("productId", String.valueOf(appMessage.getProductId()));
            t.put("jumpCategory", appMessage.getJumpCategory());
            AppPushUtil.AppPush(configMap.get("uniPushAppId"), configMap.get("uniPushAppKey"), configMap.get("masterSecret"), record.getTitle(), record.getContent(), JSUtil.toJSONString(t));
            record.setIsSend(IsSendEnum.YES.getCode());
            record.setSendTime(new Date());
        } else {
            record.setIsSend(IsSendEnum.NO.getCode());
        }
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * app推送删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> send(AppMessageSendREQ req) {
        AppMessage appMessage = dao.getById(req.getId());
        if (ObjectUtil.isNull(appMessage)) {
            return Result.error("找不到推送信息");
        }
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        if (StringUtils.isEmpty(configMap.get("uniPushAppId")) || StringUtils.isEmpty(configMap.get("uniPushAppKey")) || StringUtils.isEmpty(configMap.get("masterSecret"))) {
            return Result.error("未配置APPUniPush参数");
        }
        AppMessage record = new AppMessage();
        record.setId(appMessage.getId());
        Map<String, Object> t = new TreeMap<>();
        t.put("productId", String.valueOf(appMessage.getProductId()));
        t.put("jumpCategory", appMessage.getJumpCategory());
        AppPushUtil.AppPush(configMap.get("uniPushAppId"), configMap.get("uniPushAppKey"), configMap.get("masterSecret"), appMessage.getTitle(), appMessage.getContent(), JSUtil.toJSONString(t));
        record.setIsSend(IsSendEnum.YES.getCode());
        record.setSendTime(new Date());
        if (dao.updateById(record) > 0) {
            return Result.success("发送成功");
        }
        return Result.error("发送失败");
    }
}
