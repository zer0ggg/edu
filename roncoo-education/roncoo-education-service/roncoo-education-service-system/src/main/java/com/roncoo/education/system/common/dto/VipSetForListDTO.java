package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * vip配置
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class VipSetForListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键ID")
    private Long id;
    /**
     * 设置类型（1年费，2季度，3月度）
     */
    @ApiModelProperty(value = "设置类型（1年费，2季度，3月度）")
    private Integer setType;
    /**
     * 会员原价
     */
    @ApiModelProperty(value = "会员原价")
    private BigDecimal orgPrice;
    /**
     * 会员优惠价
     */
    @ApiModelProperty(value = "会员优惠价")
    private BigDecimal fabPrice;
    /**
     * 续费价格
     */
    @ApiModelProperty(value = "续费价格")
    private BigDecimal renewalPrice;
}
