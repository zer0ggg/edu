package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysRole;
import com.roncoo.education.system.feign.qo.SysRoleQO;
import com.roncoo.education.system.feign.vo.SysRoleVO;
import com.roncoo.education.system.service.feign.biz.FeignSysRoleBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统角色
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysRoleController extends BaseController implements IFeignSysRole{

    @Autowired
    private FeignSysRoleBiz biz;

	@Override
	public Page<SysRoleVO> listForPage(@RequestBody SysRoleQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysRoleQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysRoleQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysRoleVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
