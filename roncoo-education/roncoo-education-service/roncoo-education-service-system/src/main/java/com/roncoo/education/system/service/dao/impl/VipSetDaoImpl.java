package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.VipSetDao;
import com.roncoo.education.system.service.dao.impl.mapper.VipSetMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VipSetDaoImpl implements VipSetDao {
	@Autowired
	private VipSetMapper vipSetMapper;

	@Override
    public int save(VipSet record) {
		record.setId(IdWorker.getId());
		return this.vipSetMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.vipSetMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(VipSet record) {
		return this.vipSetMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(VipSet record, VipSetExample example) {
		return this.vipSetMapper.updateByExampleSelective(record, example);
	}

	@Override
    public VipSet getById(Long id) {
		return this.vipSetMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<VipSet> listForPage(int pageCurrent, int pageSize, VipSetExample example) {
		int count = this.vipSetMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<VipSet>(count, totalPage, pageCurrent, pageSize, this.vipSetMapper.selectByExample(example));
	}

	@Override
	public VipSet getByVipTypeAndStatusId(Integer vipType, Integer ststusId) {
		VipSetExample example = new VipSetExample();
		Criteria c = example.createCriteria();
		c.andSetTypeEqualTo(vipType);
		c.andStatusIdEqualTo(ststusId);
		List<VipSet> list = this.vipSetMapper.selectByExample(example);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public VipSet getBySetType(Integer setType) {
		VipSetExample example = new VipSetExample();
		Criteria c = example.createCriteria();
		c.andSetTypeEqualTo(setType);
		List<VipSet> list = this.vipSetMapper.selectByExample(example);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<VipSet> listAll() {
		VipSetExample example = new VipSetExample();
		return this.vipSetMapper.selectByExample(example);
	}

	@Override
	public List<VipSet> listByStatusId(Integer value) {
		VipSetExample example = new VipSetExample();
		Criteria c = example.createCriteria();
		c.andStatusIdEqualTo(value);
		return this.vipSetMapper.selectByExample(example);
	}
}
