package com.roncoo.education.system.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.ZoneRefDao;
import com.roncoo.education.system.service.dao.impl.mapper.ZoneRefMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRef;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 专区关联 服务实现类
 *
 * @author wujing
 * @date 2020-05-23
 */
@Repository
public class ZoneRefDaoImpl implements ZoneRefDao {

    @Autowired
    private ZoneRefMapper mapper;

    @Override
    public int save(ZoneRef record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ZoneRef record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ZoneRef getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ZoneRef> listForPage(int pageCurrent, int pageSize, ZoneRefExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ZoneRef> listByZoneIdAndStatusId(Long zoneId, Integer statusId) {
        ZoneRefExample example = new ZoneRefExample();
        ZoneRefExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ZoneRef> listByZoneId(Long zoneId) {
        ZoneRefExample example = new ZoneRefExample();
        ZoneRefExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        example.setOrderByClause("sort asc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public ZoneRef getZoneIdAndRefId(Long zoneId, Long refId) {
        ZoneRefExample example = new ZoneRefExample();
        ZoneRefExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andCourseIdEqualTo(refId);
        List<ZoneRef> list =  this.mapper.selectByExample(example);
        if(!CollectionUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    @Override
    public ZoneRef getZoneIdAndTitle(Long zoneId, String title) {
        ZoneRefExample example = new ZoneRefExample();
        ZoneRefExample.Criteria c = example.createCriteria();
        c.andZoneIdEqualTo(zoneId);
        c.andTitleEqualTo(title);
        List<ZoneRef> list =  this.mapper.selectByExample(example);
        if(!CollectionUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

}
