package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysRolePermission;
import com.roncoo.education.system.feign.qo.SysRolePermissionQO;
import com.roncoo.education.system.feign.vo.SysRolePermissionVO;
import com.roncoo.education.system.service.feign.biz.FeignSysRolePermissionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统角色权限
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysRolePermissionController extends BaseController implements IFeignSysRolePermission{

    @Autowired
    private FeignSysRolePermissionBiz biz;

	@Override
	public Page<SysRolePermissionVO> listForPage(@RequestBody SysRolePermissionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysRolePermissionQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysRolePermissionQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysRolePermissionVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
