package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysUserLoginLogEditREQ;
import com.roncoo.education.system.common.req.SysUserLoginLogListREQ;
import com.roncoo.education.system.common.req.SysUserLoginLogSaveREQ;
import com.roncoo.education.system.common.resp.SysUserLoginLogListRESP;
import com.roncoo.education.system.common.resp.SysUserLoginLogViewRESP;
import com.roncoo.education.system.service.dao.SysUserLoginLogDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLog;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统用户登录日志
 *
 * @author LYQ
 */
@Component
public class PcSysUserLoginLogBiz extends BaseBiz {

    @Autowired
    private SysUserLoginLogDao dao;

    /**
    * 系统用户登录日志列表
    *
    * @param sysUserLoginLogListREQ 系统用户登录日志分页查询参数
    * @return 系统用户登录日志分页查询结果
    */
    public Result<Page<SysUserLoginLogListRESP>> list(SysUserLoginLogListREQ sysUserLoginLogListREQ) {
        SysUserLoginLogExample example = new SysUserLoginLogExample();
        Criteria c = example.createCriteria();
        Page<SysUserLoginLog> page = dao.listForPage(sysUserLoginLogListREQ.getPageCurrent(), sysUserLoginLogListREQ.getPageSize(), example);
        Page<SysUserLoginLogListRESP> respPage = PageUtil.transform(page, SysUserLoginLogListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 系统用户登录日志添加
    *
    * @param sysUserLoginLogSaveREQ 系统用户登录日志
    * @return 添加结果
    */
    public Result<String> save(SysUserLoginLogSaveREQ sysUserLoginLogSaveREQ) {
        SysUserLoginLog record = BeanUtil.copyProperties(sysUserLoginLogSaveREQ, SysUserLoginLog.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 系统用户登录日志查看
    *
    * @param id 主键ID
    * @return 系统用户登录日志
    */
    public Result<SysUserLoginLogViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysUserLoginLogViewRESP.class));
    }


    /**
    * 系统用户登录日志修改
    *
    * @param sysUserLoginLogEditREQ 系统用户登录日志修改对象
    * @return 修改结果
    */
    public Result<String> edit(SysUserLoginLogEditREQ sysUserLoginLogEditREQ) {
        SysUserLoginLog record = BeanUtil.copyProperties(sysUserLoginLogEditREQ, SysUserLoginLog.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 系统用户登录日志删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
