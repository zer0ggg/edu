package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysUserRoleDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysUserRoleMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统用户角色 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysUserRoleDaoImpl implements SysUserRoleDao {

    @Autowired
    private SysUserRoleMapper mapper;

    @Override
    public int save(SysUserRole record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysUserRole record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysUserRole getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysUserRole> listForPage(int pageCurrent, int pageSize, SysUserRoleExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<SysUserRole> listByUserId(Long userId) {
        SysUserRoleExample example = new SysUserRoleExample();
        SysUserRoleExample.Criteria c = example.createCriteria();
        c.andUserIdEqualTo(userId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int countByUserId(Long userId) {
        SysUserRoleExample example = new SysUserRoleExample();
        SysUserRoleExample.Criteria c = example.createCriteria();
        c.andUserIdEqualTo(userId);
        return this.mapper.countByExample(example);
    }

    @Override
    public int countByRoleId(Long roleId) {
        SysUserRoleExample example = new SysUserRoleExample();
        SysUserRoleExample.Criteria c = example.createCriteria();
        c.andRoleIdEqualTo(roleId);
        return this.mapper.countByExample(example);
    }

    @Override
    public int deleteByUserId(Long userId) {
        SysUserRoleExample example = new SysUserRoleExample();
        SysUserRoleExample.Criteria c = example.createCriteria();
        c.andUserIdEqualTo(userId);
        return this.mapper.deleteByExample(example);
    }


}
