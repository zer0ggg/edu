package com.roncoo.education.system.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysRoleMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysRoleMenuMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统角色菜单 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysRoleMenuDaoImpl implements SysRoleMenuDao {

    @Autowired
    private SysRoleMenuMapper mapper;

    @Override
    public int save(SysRoleMenu record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysRoleMenu record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysRoleMenu getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysRoleMenu> listForPage(int pageCurrent, int pageSize, SysRoleMenuExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<SysRoleMenu> listByRoleId(Long roleId) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        SysRoleMenuExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysRoleMenu> listInRoleIdList(List<Long> roleIdList) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        SysRoleMenuExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdIn(roleIdList);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int countByRoleId(Long roleId) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        SysRoleMenuExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.countByExample(example);
    }

    @Override
    public int deleteByRoleId(Long roleId) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        SysRoleMenuExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public List<SysRoleMenu> listByMenuId(Long menuId) {
        SysRoleMenuExample example = new SysRoleMenuExample();
        SysRoleMenuExample.Criteria criteria = example.createCriteria();
        criteria.andMenuIdEqualTo(menuId);
        return this.mapper.selectByExample(example);
    }


}
