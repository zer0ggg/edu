package com.roncoo.education.system.service.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.AppMessageDao;
import com.roncoo.education.system.service.dao.impl.mapper.AppMessageMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessage;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample;

/**
 * app推送 服务实现类
 *
 * @author wujing
 * @date 2020-11-17
 */
@Repository
public class AppMessageDaoImpl implements AppMessageDao {

    @Autowired
    private AppMessageMapper mapper;

    @Override
    public int save(AppMessage record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(AppMessage record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public AppMessage getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<AppMessage> listForPage(int pageCurrent, int pageSize, AppMessageExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


}
