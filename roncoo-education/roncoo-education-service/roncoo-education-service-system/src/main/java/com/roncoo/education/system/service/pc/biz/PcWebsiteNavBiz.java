package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.system.common.req.WebsiteNavEditREQ;
import com.roncoo.education.system.common.req.WebsiteNavListREQ;
import com.roncoo.education.system.common.req.WebsiteNavSaveREQ;
import com.roncoo.education.system.common.resp.WebsiteNavListRESP;
import com.roncoo.education.system.common.resp.WebsiteNavViewRESP;
import com.roncoo.education.system.service.dao.WebsiteNavArticleDao;
import com.roncoo.education.system.service.dao.WebsiteNavDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNav;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavArticle;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 站点导航
 *
 * @author wujing
 */
@Component
public class PcWebsiteNavBiz extends BaseBiz {

    @Autowired
    private WebsiteNavDao dao;
    @Autowired
    private WebsiteNavArticleDao articleDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 站点导航列表
     *
     * @param req 站点导航分页查询参数
     * @return 站点导航分页查询结果
     */
    public Result<Page<WebsiteNavListRESP>> list(WebsiteNavListREQ req) {
        WebsiteNavExample example = new WebsiteNavExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (!StringUtils.isEmpty(req.getNavName())) {
            c.andNavNameLike(PageUtil.like(req.getNavName()));
        } else {
            c.andParentIdEqualTo(0L);
        }
        Page<WebsiteNav> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<WebsiteNavListRESP> respPage = PageUtil.transform(page, WebsiteNavListRESP.class);
        if (!CollectionUtils.isEmpty(respPage.getList())) {
            for (WebsiteNavListRESP nav : respPage.getList()) {
                websiteNav(nav);
            }
        }
        return Result.success(respPage);
    }

    public WebsiteNavListRESP websiteNav(WebsiteNavListRESP navResp) {
        List<WebsiteNav> websiteNav = dao.listByParentId(navResp.getId());
        if (ObjectUtil.isNotNull(websiteNav)) {
            List<WebsiteNavListRESP> websiteNavList = PageUtil.copyList(websiteNav, WebsiteNavListRESP.class);
            navResp.setChildren(websiteNavList);
            for (WebsiteNavListRESP websiteNavV2 : websiteNavList) {
                WebsiteNavArticle websiteNavArticle = articleDao.getByNavId(websiteNavV2.getId());
                if (ObjectUtil.isNotNull(websiteNavArticle)) {
                    websiteNavV2.setIsArticle(1);
                } else {
                    websiteNavV2.setIsArticle(0);
                }
            }
        }
        return navResp;
    }


    /**
     * 站点导航添加
     *
     * @param websiteNavSaveREQ 站点导航
     * @return 添加结果
     */
    public Result<String> save(WebsiteNavSaveREQ websiteNavSaveREQ) {
        WebsiteNav record = BeanUtil.copyProperties(websiteNavSaveREQ, WebsiteNav.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 站点导航查看
     *
     * @param id 主键ID
     * @return 站点导航
     */
    public Result<WebsiteNavViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), WebsiteNavViewRESP.class));
    }


    /**
     * 站点导航修改
     *
     * @param req 站点导航修改对象
     * @return 修改结果
     */
    public Result<String> edit(WebsiteNavEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        WebsiteNav record = BeanUtil.copyProperties(req, WebsiteNav.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 站点导航删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        if (id == null) {
            return Result.error("主键id不能为空");
        }
        List<WebsiteNav> sonList = dao.listByParentId(id);
        if (!CollectionUtils.isEmpty(sonList)) {
            throw new BaseException("存在子一级，请先删除子一级站点导航");
        }
        int record = dao.deleteById(id);
        if (record < 1) {
            throw new BaseException("站点导航表删除失败");
        }
        WebsiteNavArticle article = articleDao.getByNavId(id);
        if (ObjectUtil.isNotNull(article)) {
            int result = articleDao.deleteById(article.getId());
            if (result < 1) {
                throw new BaseException("站点导航文章表删除失败");
            }
        }
        return Result.success("删除成功");
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiWebsiteNavController::list");
        return Result.success("删除成功");
    }
}
