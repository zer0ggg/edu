package com.roncoo.education.system.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.system.service.dao.SysPermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.SysPermissionMapper;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统权限 服务实现类
 *
 * @author wujing
 * @date 2020-04-29
 */
@Repository
public class SysPermissionDaoImpl implements SysPermissionDao {

    @Autowired
    private SysPermissionMapper mapper;

    @Override
    public int save(SysPermission record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(SysPermission record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public SysPermission getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<SysPermission> listForPage(int pageCurrent, int pageSize, SysPermissionExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public SysPermission getByPermissionValue(String permissionValue) {
        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        criteria.andPermissionValueEqualTo(permissionValue);
        List<SysPermission> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<SysPermission> listAll() {
        SysPermissionExample example = new SysPermissionExample();
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysPermission> listByMenuId(Long menuId) {
        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        criteria.andMenuIdEqualTo(menuId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysPermission> listByMenuIdAndStatusId(Long menuId, Integer statusId) {
        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        criteria.andMenuIdEqualTo(menuId);
        criteria.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<SysPermission> listInIdListAndStatusId(List<Long> idList, Integer statusId) {
        SysPermissionExample example = new SysPermissionExample();
        SysPermissionExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(idList);
        criteria.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }


}
