package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSet;
import com.roncoo.education.system.service.dao.impl.mapper.entity.VipSetExample;

import java.util.List;

public interface VipSetDao {
	int save(VipSet record);

	int deleteById(Long id);

	int updateById(VipSet record);

	int updateByExampleSelective(VipSet record, VipSetExample example);

	VipSet getById(Long id);

	Page<VipSet> listForPage(int pageCurrent, int pageSize, VipSetExample example);

	VipSet getByVipTypeAndStatusId(Integer vipType, Integer code);

	/**
	 * 根据会员类型查看会员信息
	 *
	 * @param setType
	 * @return
	 */
	VipSet getBySetType(Integer setType);

	List<VipSet> listAll();

	List<VipSet> listByStatusId(Integer value);
}
