package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SharingTemplateQO;
import com.roncoo.education.system.feign.vo.SharingTemplateVO;
import com.roncoo.education.system.service.dao.SharingTemplateDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplate;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分享图片模板
 *
 * @author wujing
 */
@Component
public class FeignSharingTemplateBiz extends BaseBiz {

    @Autowired
    private SharingTemplateDao dao;

	public Page<SharingTemplateVO> listForPage(SharingTemplateQO qo) {
	    SharingTemplateExample example = new SharingTemplateExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SharingTemplate> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SharingTemplateVO.class);
	}

	public int save(SharingTemplateQO qo) {
		SharingTemplate record = BeanUtil.copyProperties(qo, SharingTemplate.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SharingTemplateVO getById(Long id) {
		SharingTemplate record = dao.getById(id);
		return BeanUtil.copyProperties(record, SharingTemplateVO.class);
	}

	public int updateById(SharingTemplateQO qo) {
		SharingTemplate record = BeanUtil.copyProperties(qo, SharingTemplate.class);
		return dao.updateById(record);
	}

	/**
	 * 根据是否使用模板、模板类型获取模板
	 *
	 * @return
	 */
	public SharingTemplateVO getIsUseTemplateAndTemplateType(SharingTemplateQO qo) {
		SharingTemplate sharingTemplate = dao.getIsUseTemplateAndTemplateType(qo.getIsUseTemplate(), qo.getTemplateType());
		return BeanUtil.copyProperties(sharingTemplate, SharingTemplateVO.class);
	}



}
