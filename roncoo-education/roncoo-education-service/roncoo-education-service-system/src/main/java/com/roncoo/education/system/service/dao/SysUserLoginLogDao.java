package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLog;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserLoginLogExample;

/**
 * 系统用户登录日志 服务类
 *
 * @author LYQ
 * @date 2020-05-14
 */
public interface SysUserLoginLogDao {

    int save(SysUserLoginLog record);

    int deleteById(Long id);

    int updateById(SysUserLoginLog record);

    SysUserLoginLog getById(Long id);

    Page<SysUserLoginLog> listForPage(int pageCurrent, int pageSize, SysUserLoginLogExample example);

}
