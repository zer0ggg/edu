package com.roncoo.education.system.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.bo.WebsiteBO;
import com.roncoo.education.system.common.bo.WebsiteReferralCodeBO;
import com.roncoo.education.system.common.dto.WebsiteDTO;
import com.roncoo.education.system.service.api.biz.ApiWebsiteBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站点信息
 *
 * @author Quanf
 */
@RestController
@CacheConfig(cacheNames = { "system" })
@RequestMapping(value = "/system/api/website")
public class ApiWebsiteController extends BaseController {

	@Autowired
	private ApiWebsiteBiz biz;

	/**
	 * 获取站点信息接口
	 *
	 * @return 站点信息
	 * @author Quanf
	 */
	@Cacheable
	@ApiOperation(value = "获取站点信息接口", notes = "获取站点信息")
	@RequestMapping(value = "/get", method = RequestMethod.POST)
	public Result<WebsiteDTO> get(@RequestBody WebsiteBO websiteBO) {
		return biz.get(websiteBO);
	}

	/**
	 * 获取小程序推荐码
	 */
	@ApiOperation(value = "获取小程序推荐码", notes = "获取小程序推荐码")
	@RequestMapping(value = "/referral/code", method = RequestMethod.POST)
	public Result<String> referralCode(@RequestBody WebsiteReferralCodeBO bo) {
		return biz.referralCode(bo);
	}

}
