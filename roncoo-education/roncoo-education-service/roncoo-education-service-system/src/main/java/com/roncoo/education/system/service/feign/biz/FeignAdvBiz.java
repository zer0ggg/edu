package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.qo.AdvQO;
import com.roncoo.education.system.feign.vo.AdvVO;
import com.roncoo.education.system.service.dao.AdvDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Adv;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AdvExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AdvExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 广告信息
 *
 * @author wujing
 */
@Component
public class FeignAdvBiz {
	@Autowired
	private IFeignSysConfig feignSysConfig;

	@Autowired
	private AdvDao dao;

	public Page<AdvVO> listForPage(AdvQO qo) {
		AdvExample example = new AdvExample();
		Criteria c = example.createCriteria();
		c.andAdvLocationEqualTo(qo.getAdvLocation());
		if (qo.getPlatShow() != null) {
			c.andPlatShowEqualTo(qo.getPlatShow());
		}
		if (StringUtils.hasText(qo.getAdvTitle())) {
			c.andAdvTitleLike(PageUtil.like(qo.getAdvTitle()));
		}
		if (qo.getStatusId() != null) {
			c.andStatusIdEqualTo(qo.getStatusId());
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<Adv> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, AdvVO.class);
	}

	public int save(AdvQO qo) {
		Adv record = BeanUtil.copyProperties(qo, Adv.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		Adv adv = dao.getById(id);
		if (adv != null) {
			AliyunUtil.delete(adv.getAdvImg(), BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
		}
		return dao.deleteById(id);
	}

	public AdvVO getById(Long id) {
		Adv record = dao.getById(id);
		return BeanUtil.copyProperties(record, AdvVO.class);
	}

	public int updateById(AdvQO qo) {
		Adv adv = dao.getById(qo.getId());
		if (StringUtils.hasText(qo.getAdvImg()) && !adv.getAdvImg().equals(qo.getAdvImg())) {
			AliyunUtil.delete(adv.getAdvImg(), BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
		}
		Adv record = BeanUtil.copyProperties(qo, Adv.class);
		return dao.updateById(record);
	}

	public int updateStatusId(AdvQO qo) {
		Adv adv = BeanUtil.copyProperties(qo, Adv.class);
		return dao.updateById(adv);
	}

}
