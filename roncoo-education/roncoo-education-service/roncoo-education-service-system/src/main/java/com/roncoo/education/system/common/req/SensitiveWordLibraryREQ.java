package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 敏感词列出
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SensitiveWordLibraryREQ", description="敏感词列出")
public class SensitiveWordLibraryREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "滤不良字符")
    private String badword;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
