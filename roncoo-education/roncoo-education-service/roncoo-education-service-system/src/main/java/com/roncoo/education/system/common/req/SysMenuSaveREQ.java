package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 系统菜单
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "SysMenuSaveREQ", description = "系统菜单添加")
public class SysMenuSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "父ID不能为空")
    @ApiModelProperty(value = "父ID", required = true)
    private Long parentId;

    @NotBlank(message = "菜单名称不能为空")
    @ApiModelProperty(value = "菜单名称", required = true)
    private String menuName;

    @NotNull(message = "菜单类型不能为空")
    @ApiModelProperty(value = "菜单类型(0:目录;1菜单)", required = true)
    private Integer menuType;

    @NotBlank(message = "路由地址不能为空")
    @ApiModelProperty(value = "路由地址", required = true)
    private String routerUrl;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "是否外链(1:是 ;0:否)")
    private Integer isFrame;
}
