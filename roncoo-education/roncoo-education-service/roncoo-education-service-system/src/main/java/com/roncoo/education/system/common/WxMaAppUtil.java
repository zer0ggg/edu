package com.roncoo.education.system.common;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * wechat mp configuration
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
public final class WxMaAppUtil {

	private static final String PRE = "ma";
	private static Map<String, WxMaService> services = new HashMap<>();

	public static final String AESKEY = "";

	private WxMaAppUtil() {

	}

	public static WxMaService getService(String appId, String appSecret, String aesKey, StringRedisTemplate stringRedisTemplate) {
		WxMaService service = services.get(PRE.concat(appSecret));
		if (service == null) {
			WxMaRedisConfigStorage configStorage = new WxMaRedisConfigStorage(stringRedisTemplate, appId);
			configStorage.setAppid(appId);
			configStorage.setSecret(appSecret);
			configStorage.setToken(appId);
			configStorage.setAesKey(aesKey);
			service = new WxMaServiceImpl();
			service.setWxMaConfig(configStorage);
			services.put(PRE.concat(appSecret), service);
		}
		return service;
	}

}
