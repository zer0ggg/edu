package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysUserRoleQO;
import com.roncoo.education.system.feign.vo.SysUserRoleVO;
import com.roncoo.education.system.service.dao.SysUserRoleDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRole;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserRoleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统用户角色
 *
 * @author wujing
 */
@Component
public class FeignSysUserRoleBiz extends BaseBiz {

    @Autowired
    private SysUserRoleDao dao;

	public Page<SysUserRoleVO> listForPage(SysUserRoleQO qo) {
	    SysUserRoleExample example = new SysUserRoleExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysUserRole> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysUserRoleVO.class);
	}

	public int save(SysUserRoleQO qo) {
		SysUserRole record = BeanUtil.copyProperties(qo, SysUserRole.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysUserRoleVO getById(Long id) {
		SysUserRole record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysUserRoleVO.class);
	}

	public int updateById(SysUserRoleQO qo) {
		SysUserRole record = BeanUtil.copyProperties(qo, SysUserRole.class);
		return dao.updateById(record);
	}

}
