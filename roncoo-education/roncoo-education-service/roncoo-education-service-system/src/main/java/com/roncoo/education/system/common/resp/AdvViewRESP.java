package com.roncoo.education.system.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AdvViewRESP", description="广告信息查看")
public class AdvViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "广告标题")
    private String advTitle;

    @ApiModelProperty(value = "广告链接")
    private String advUrl;

    @ApiModelProperty(value = "广告图片")
    private String advImg;

    @ApiModelProperty(value = "广告跳转方式")
    private String advTarget;

    @ApiModelProperty(value = "广告位置(1首页轮播图，2顶部活动;3:首页资讯轮播图)")
    private Integer advLocation;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer platShow;

    @ApiModelProperty(value = "移动端类型(1:APP移动端,2:H5移动端)")
    private Integer mobileTerminalCategory;

    @ApiModelProperty(value = "课程分类(1:点播;2:直播,4:文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

}
