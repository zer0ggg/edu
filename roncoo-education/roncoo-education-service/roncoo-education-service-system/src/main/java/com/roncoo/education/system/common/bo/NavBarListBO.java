package com.roncoo.education.system.common.bo;

import com.roncoo.education.common.core.enums.PlatEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 头部导航
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class NavBarListBO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "位置(1电脑端，2微信端)不能为空")
	@ApiModelProperty(value = "位置(1电脑端，2微信端)", required = true)
	private Integer platShow = PlatEnum.PC.getCode();

}
