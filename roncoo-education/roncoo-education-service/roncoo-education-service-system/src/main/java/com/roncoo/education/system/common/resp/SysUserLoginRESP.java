package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value = "SysUserLoginRESP", description = "系统用户登录结果")
public class SysUserLoginRESP implements Serializable {

    private static final long serialVersionUID = -5227677558610916215L;

    @ApiModelProperty(value = "昵称", required = true)
    private String nickname;

    @ApiModelProperty(value = "登录账号", required = true)
    private String loginName;

    @ApiModelProperty(value = "token，有效期为1天", required = true)
    private String token;
}
