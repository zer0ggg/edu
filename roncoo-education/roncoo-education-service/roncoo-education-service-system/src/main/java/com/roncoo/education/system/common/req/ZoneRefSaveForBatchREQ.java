package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefSaveForBatchREQ", description="专区关联  批量添加")
public class ZoneRefSaveForBatchREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "专区ID不能为空")
    @ApiModelProperty(value = "专区ID",required = true)
    private Long zoneId;

    @ApiModelProperty(value = "关联IDs", required = true)
    private String courseIds;

}
