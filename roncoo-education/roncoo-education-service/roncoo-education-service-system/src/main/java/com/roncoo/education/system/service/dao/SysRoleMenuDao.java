package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenuExample;

import java.util.List;

/**
 * 系统角色菜单 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysRoleMenuDao {

    int save(SysRoleMenu record);

    int deleteById(Long id);

    int updateById(SysRoleMenu record);

    SysRoleMenu getById(Long id);

    Page<SysRoleMenu> listForPage(int pageCurrent, int pageSize, SysRoleMenuExample example);

    /**
     * 根据角色ID获取角色菜单集合
     *
     * @param roleId 角色ID
     * @return 角色权限关联集合
     */
    List<SysRoleMenu> listByRoleId(Long roleId);

    /**
     * 根据角色ID获取角色菜单集合
     *
     * @param roleIdList 角色ID集合
     * @return 角色权限关联集合
     */
    List<SysRoleMenu> listInRoleIdList(List<Long> roleIdList);

    /**
     * 根据角色ID统计角色菜单集合
     *
     * @param roleId 角色ID
     * @return 记录数
     */
    int countByRoleId(Long roleId);

    /**
     * 根据角色ID删除
     *
     * @param roleId 角色ID
     * @return 删除数量
     */
    int deleteByRoleId(Long roleId);

    /**
     * 根据菜单ID查找
     *
     * @param menuId 菜单ID
     * @return 菜单角色
     */
    List<SysRoleMenu> listByMenuId(Long menuId);
}
