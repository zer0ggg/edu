package com.roncoo.education.system.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 专区关联
 * </p>
 *
 * @author wujing
 * @date 2020-05-23
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRefBO", description="专区关联")
public class ZoneRefBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "专区ID")
    private Long zoneId;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;

    @ApiModelProperty(value = "关联ID")
    private Long courseId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "外部链接")
    private String url;

    @ApiModelProperty(value = "图片")
    private String img;
}
