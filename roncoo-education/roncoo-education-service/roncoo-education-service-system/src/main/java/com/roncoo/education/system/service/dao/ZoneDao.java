package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample;

/**
 * 专区 服务类
 *
 * @author wujing
 * @date 2020-05-23
 */
public interface ZoneDao {

    /**
    * 保存专区
    *
    * @param record 专区
    * @return 影响记录数
    */
    int save(Zone record);

    /**
    * 根据ID删除专区
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 专区
    * @return 影响记录数
    */
    int updateById(Zone record);

    /**
    * 根据ID获取专区
    *
    * @param id 主键ID
    * @return 专区
    */
    Zone getById(Long id);

    /**
    * 专区--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<Zone> listForPage(int pageCurrent, int pageSize, ZoneExample example);

    /**
     * 根据位置、类型和名称获得专区信息
     * @param zoneLocation
     * @param zoneCategory
     * @param zoneName
     * @return
     */
    Zone getByZoneLocationAndzoneCategoryAndZoneName(Integer zoneLocation, Integer zoneCategory, String zoneName);
}
