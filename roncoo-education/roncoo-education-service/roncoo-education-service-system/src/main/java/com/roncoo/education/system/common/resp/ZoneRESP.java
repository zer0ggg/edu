package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneRESP", description="专区")
public class ZoneRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "名称")
    private String zoneName;

    @ApiModelProperty(value = "专区分类(1:普通课程;2:直播课程,3文库，4:试卷，5:博客;6:资讯，7:自定义)")
    private Integer zoneCategory;

    @ApiModelProperty(value = "描述")
    private String zoneDesc;

    @ApiModelProperty(value = "位置(1电脑端，2微信端)")
    private Integer zoneLocation;
}
