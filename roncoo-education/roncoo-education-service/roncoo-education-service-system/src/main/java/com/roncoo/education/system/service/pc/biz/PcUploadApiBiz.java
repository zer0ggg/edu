/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.FileTypeEnum;
import com.roncoo.education.common.core.enums.PlatformEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.MD5Util;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.course.feign.interfaces.IFeignFileInfo;
import com.roncoo.education.course.feign.qo.FileInfoQO;
import com.roncoo.education.system.common.dto.AuthUpdatePolyvSignCode;
import com.roncoo.education.system.common.dto.AuthWebUploadVideoGetCode;
import com.roncoo.education.system.service.dao.SysConfigDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 上传接口
 *
 * @author wuyun
 */
@Component
public class PcUploadApiBiz extends BaseBiz {

    @Autowired
    private IFeignFileInfo feignFileInfo;

    @Autowired
    private SysConfigDao sysConfigDao;

    /**
     * 上传图片接口
     *
     * @author wuyun
     */
    public Result<String> uploadPic(MultipartFile picFile) {
        if (ObjectUtil.isNotNull(picFile) && !picFile.isEmpty()) {
            // 上传到阿里云
            Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
            Aliyun aliyun = new Aliyun();
            aliyun.setAliyunOssEndpoint(configMap.get("aliyunOssEndpoint"));
            aliyun.setAliyunAccessKeyId(configMap.get("aliyunAccessKeyId"));
            aliyun.setAliyunAccessKeySecret(configMap.get("aliyunAccessKeySecret"));
            aliyun.setAliyunOssBucket(configMap.get("aliyunOssBucket"));
            aliyun.setAliyunOssUrl(configMap.get("aliyunOssUrl"));

            String fileUrl = AliyunUtil.uploadPic(PlatformEnum.COURSE, picFile, aliyun);
            // 记录文件信息
            FileInfoQO fileInfoQO = new FileInfoQO();
            fileInfoQO.setFileName(picFile.getName());
            fileInfoQO.setFileUrl(fileUrl);
            fileInfoQO.setFileType(FileTypeEnum.PICTURE.getCode());
            fileInfoQO.setFileSize(picFile.getSize());
            feignFileInfo.save(fileInfoQO);
            return Result.success(fileUrl);
        }
        return Result.error("请选择上传的图片");
    }

    /**
     * 上传文档接口
     *
     * @author wuyun
     */
    public Result<String> uploadDoc(MultipartFile docFile) {
        if (ObjectUtil.isNotNull(docFile) && !docFile.isEmpty()) {
            // 上传到阿里云
            Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
            Aliyun aliyun = new Aliyun();
            aliyun.setAliyunOssEndpoint(configMap.get("aliyunOssEndpoint"));
            aliyun.setAliyunAccessKeyId(configMap.get("aliyunAccessKeyId"));
            aliyun.setAliyunAccessKeySecret(configMap.get("aliyunAccessKeySecret"));
            aliyun.setAliyunOssBucket(configMap.get("aliyunOssBucket"));
            aliyun.setAliyunOssUrl(configMap.get("aliyunOssUrl"));

            String fileUrl = AliyunUtil.uploadDoc(PlatformEnum.COURSE, docFile, aliyun);
            // 记录文件信息
            FileInfoQO fileInfoQO = new FileInfoQO();
            fileInfoQO.setFileName(docFile.getName());
            fileInfoQO.setFileUrl(fileUrl);
            fileInfoQO.setFileType(FileTypeEnum.ACCESSORY.getCode());
            fileInfoQO.setFileSize(docFile.getSize());
            feignFileInfo.save(fileInfoQO);
            return Result.success(fileUrl);
        }
        return Result.error("请选择上传的文件");
    }

    public Result<AuthWebUploadVideoGetCode> getCode() {
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        if (configMap == null) {
            return Result.error("上传参数未配置!");
        }
        if (StringUtils.isEmpty(configMap.get("aliyunOSSBucketVideo")) ||
                StringUtils.isEmpty(configMap.get("aliyunAccessKeyId")) ||
                StringUtils.isEmpty(configMap.get("aliyunAccessKeySecret")) ||
                StringUtils.isEmpty(configMap.get("aliyunOssBucket")) ||
                StringUtils.isEmpty(configMap.get("aliyunOssUrl")) ||
                StringUtils.isEmpty(configMap.get("aliyunOssEndpoint")) ||
                StringUtils.isEmpty(configMap.get(SysConfigConstants.ISBACKUPALI))) {
            return Result.error("上传参数未配置!");
        }

        AuthWebUploadVideoGetCode webUploadVideoGetCode = new AuthWebUploadVideoGetCode();
        webUploadVideoGetCode.setOssBucketVideo(configMap.get("aliyunOSSBucketVideo"));
        webUploadVideoGetCode.setAliyunOssBucket(configMap.get("aliyunOssBucket"));
        webUploadVideoGetCode.setAliyunOssUrl(configMap.get("aliyunOssUrl"));
        webUploadVideoGetCode.setAliyunAccessKeyId(configMap.get("aliyunAccessKeyId"));
        webUploadVideoGetCode.setAliyunccessKeySecret(configMap.get("aliyunAccessKeySecret"));

        webUploadVideoGetCode.setEndPoint(configMap.get("aliyunOssEndpoint").replace(".aliyuncs.com/", "").replace(".aliyuncs.com", "").replace("http://", "").replace("https://", ""));
        webUploadVideoGetCode.setDirectory(PlatformEnum.COURSE.name().toLowerCase());
        webUploadVideoGetCode.setIsBackupAli(Integer.valueOf(configMap.get(SysConfigConstants.ISBACKUPALI)));
        return Result.success(webUploadVideoGetCode);
    }

    public Result<AuthUpdatePolyvSignCode> updateSign() {
        Map<String, String> configMap = sysConfigDao.getConfigMapByStatusId(StatusIdEnum.YES.getCode());
        if (configMap == null) {
            return Result.error("上传参数未配置!");
        }
        if (StringUtils.isEmpty(configMap.get("polyvUseid")) ||
                StringUtils.isEmpty(configMap.get("polyvSecretkey")) ||
                StringUtils.isEmpty(configMap.get("polyvWritetoken"))) {
            return Result.error("上传参数配置错误!");
        }
        AuthUpdatePolyvSignCode authUpdatePolyvSignCode = new AuthUpdatePolyvSignCode();
        authUpdatePolyvSignCode.setUserid(configMap.get("polyvUseid"));
        authUpdatePolyvSignCode.setPtime(System.currentTimeMillis());
        authUpdatePolyvSignCode.setSign(MD5Util.MD5(configMap.get("polyvSecretkey") + authUpdatePolyvSignCode.getPtime()));
        authUpdatePolyvSignCode.setHash(MD5Util.MD5(authUpdatePolyvSignCode.getPtime() + configMap.get("polyvWritetoken")));
        return Result.success(authUpdatePolyvSignCode);
    }
}
