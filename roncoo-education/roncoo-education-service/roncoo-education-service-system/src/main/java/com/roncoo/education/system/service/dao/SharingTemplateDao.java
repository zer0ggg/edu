package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplate;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample;

/**
 * 分享图片模板 服务类
 *
 * @author wujing
 * @date 2020-07-16
 */
public interface SharingTemplateDao {

    /**
    * 保存分享图片模板
    *
    * @param record 分享图片模板
    * @return 影响记录数
    */
    int save(SharingTemplate record);

    /**
    * 根据ID删除分享图片模板
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 分享图片模板
    * @return 影响记录数
    */
    int updateById(SharingTemplate record);

    /**
    * 根据ID获取分享图片模板
    *
    * @param id 主键ID
    * @return 分享图片模板
    */
    SharingTemplate getById(Long id);

    /**
    * 分享图片模板--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<SharingTemplate> listForPage(int pageCurrent, int pageSize, SharingTemplateExample example);


    /**
     * 根据是否使用模板、模板类型获取模板
     *
     * @param isUseTemplate
     * @return
     */
    SharingTemplate getIsUseTemplateAndTemplateType(Integer isUseTemplate, Integer templateType);


}
