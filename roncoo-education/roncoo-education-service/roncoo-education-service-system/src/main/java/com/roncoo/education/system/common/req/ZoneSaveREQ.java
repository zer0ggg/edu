package com.roncoo.education.system.common.req;

import com.roncoo.education.common.core.enums.ZoneShowTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneSaveREQ", description="专区添加")
public class ZoneSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序",required = true)
    private Integer sort;

    @NotEmpty(message = "名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String zoneName;

    @NotNull(message = "专区分类不能为空")
    @ApiModelProperty(value = "专区分类(1:点播;2:直播,4文库,5试卷,7博客;8:资讯,9:自定义，12：问答)")
    private Integer zoneCategory;

    @ApiModelProperty(value = "描述")
    private String zoneDesc;

    @NotNull(message = "专区分类不能为空")
    @ApiModelProperty(value = "位置(1电脑端，2微信端)",required = true)
    private Integer zoneLocation;

    @ApiModelProperty(value = "展示方式(1横向，2纵向；3轮播; 4推荐)")
    private Integer showType = ZoneShowTypeEnum.HORIZONTAL.getCode();

    @ApiModelProperty(value = "产品ID",required = true)
    private Long productId;

    @ApiModelProperty(value = "外部链接",required = true)
    private String url;

    @ApiModelProperty(value = "图片",required = true)
    private String img;
}
