package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统用户登录日志
 * </p>
 *
 * @author LYQ
 * @date 2020-05-14
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysUserLoginLogDTO", description="系统用户登录日志")
public class SysUserLoginLogDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "登录账号")
    private String loginName;

    @ApiModelProperty(value = "登录IP")
    private String loginIp;

    @ApiModelProperty(value = "登录状态(1:成功、0:失败)")
    private Integer loginStatus;

    @ApiModelProperty(value = "登录描述")
    private String loginDescription;
}
