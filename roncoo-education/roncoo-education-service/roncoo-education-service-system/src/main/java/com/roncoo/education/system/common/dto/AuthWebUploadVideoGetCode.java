package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 获取上传参数
 */
@Data
@Accessors(chain = true)
public class AuthWebUploadVideoGetCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "备份视频的阿里云bucket")
    private String ossBucketVideo;

    @ApiModelProperty(value = "阿里云bucket(图片，文件)")
    private String aliyunOssBucket;

    @ApiModelProperty(value = "阿里云accessKeyId")
    private String aliyunAccessKeyId;

    @ApiModelProperty(value = "阿里云accessKeySecret")
    private String aliyunccessKeySecret;

    @ApiModelProperty(value = "地区")
    private String endPoint;

    @ApiModelProperty(value = "上传目录")
    private String directory;

    @ApiModelProperty(value = "是否备份阿里云")
    private Integer isBackupAli;

    @ApiModelProperty(value = "OSS的自定义域名")
    private String aliyunOssUrl;
}
