package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统角色
 * </p>
 *
 * @author wujing
 * @date 2020-04-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value="SysRoleDTO", description="系统角色")
public class SysRoleDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色标识")
    private String roleValue;

    @ApiModelProperty(value = "允许删除(1:允许;0:不允许)")
    private Integer allowDelete;
}
