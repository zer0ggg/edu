package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermissionExample;

import java.util.List;

/**
 * 系统权限 服务类
 *
 * @author wujing
 * @date 2020-04-29
 */
public interface SysPermissionDao {

    int save(SysPermission record);

    int deleteById(Long id);

    int updateById(SysPermission record);

    SysPermission getById(Long id);

    Page<SysPermission> listForPage(int pageCurrent, int pageSize, SysPermissionExample example);

    /**
     * 根据权限标识获取权限点
     *
     * @param permissionValue 权限标识
     * @return 权限点
     */
    SysPermission getByPermissionValue(String permissionValue);

    /**
     * 列出全部权限点
     *
     * @return 权限点集合
     */
    List<SysPermission> listAll();

    /**
     * 根据菜单ID获取权限点
     *
     * @param menuId 菜单ID
     * @return 权限点集合
     */
    List<SysPermission> listByMenuId(Long menuId);

    /**
     * 根据菜单ID和状态ID获取权限点
     *
     * @param menuId   菜单ID
     * @param statusId 状态ID
     * @return 权限点集合
     */
    List<SysPermission> listByMenuIdAndStatusId(Long menuId, Integer statusId);

    /**
     * 根据菜单ID集合和状态获取权限点
     *
     * @param idList   权限ID集合
     * @param statusId 状态ID
     * @return 权限点集合
     */
    List<SysPermission> listInIdListAndStatusId(List<Long> idList, Integer statusId);

}
