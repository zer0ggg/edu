package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.system.common.req.WebsiteLinkEditREQ;
import com.roncoo.education.system.common.req.WebsiteLinkListREQ;
import com.roncoo.education.system.common.req.WebsiteLinkSaveREQ;
import com.roncoo.education.system.common.resp.WebsiteLinkListRESP;
import com.roncoo.education.system.common.resp.WebsiteLinkViewRESP;
import com.roncoo.education.system.service.dao.WebsiteLinkDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteLink;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteLinkExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteLinkExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 站点友情链接
 *
 * @author wujing
 */
@Component
public class PcWebsiteLinkBiz extends BaseBiz {

    @Autowired
    private WebsiteLinkDao dao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;


    /**
     * 站点友情链接列表
     *
     * @param req 站点友情链接分页查询参数
     * @return 站点友情链接分页查询结果
     */
    public Result<Page<WebsiteLinkListRESP>> list(WebsiteLinkListREQ req) {
        WebsiteLinkExample example = new WebsiteLinkExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (!StringUtils.isEmpty(req.getLinkName())) {
            c.andLinkNameLike(PageUtil.like(req.getLinkName()));
        }
        Page<WebsiteLink> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<WebsiteLinkListRESP> respPage = PageUtil.transform(page, WebsiteLinkListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 站点友情链接添加
     *
     * @param websiteLinkSaveREQ 站点友情链接
     * @return 添加结果
     */
    public Result<String> save(WebsiteLinkSaveREQ websiteLinkSaveREQ) {
        WebsiteLink record = BeanUtil.copyProperties(websiteLinkSaveREQ, WebsiteLink.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 站点友情链接查看
     *
     * @param id 主键ID
     * @return 站点友情链接
     */
    public Result<WebsiteLinkViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), WebsiteLinkViewRESP.class));
    }


    /**
     * 站点友情链接修改
     *
     * @param req 站点友情链接修改对象
     * @return 修改结果
     */
    public Result<String> edit(WebsiteLinkEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        WebsiteLink record = BeanUtil.copyProperties(req, WebsiteLink.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 站点友情链接删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> deleteRedis() {
        myRedisTemplate.delete("system::com.roncoo.education.system.service.api.ApiWebsiteLinkController::list");
        return Result.success("删除成功");
    }
}
