package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.system.common.req.RegionEditREQ;
import com.roncoo.education.system.common.req.RegionListREQ;
import com.roncoo.education.system.common.req.RegionSaveREQ;
import com.roncoo.education.system.common.resp.RegionListRESP;
import com.roncoo.education.system.common.resp.RegionViewRESP;
import com.roncoo.education.system.service.pc.biz.PcRegionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 行政区域 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/system/pc/region")
@Api(value = "system-行政区域", tags = {"system-行政区域"})
public class PcRegionController {

    @Autowired
    private PcRegionBiz biz;

    @ApiOperation(value = "行政区域列表", notes = "行政区域列表")
    @PostMapping(value = "/list")
    public Result<Page<RegionListRESP>> list(@RequestBody RegionListREQ regionListREQ) {
        return biz.list(regionListREQ);
    }


    @ApiOperation(value = "行政区域添加", notes = "行政区域添加")
    @SysLog(value = "行政区域添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody RegionSaveREQ regionSaveREQ) {
        return biz.save(regionSaveREQ);
    }

    @ApiOperation(value = "行政区域查看", notes = "行政区域查看")
    @GetMapping(value = "/view")
    public Result<RegionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "行政区域修改", notes = "行政区域修改")
    @SysLog(value = "行政区域修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody RegionEditREQ regionEditREQ) {
        return biz.edit(regionEditREQ);
    }


    @ApiOperation(value = "行政区域删除", notes = "行政区域删除")
    @SysLog(value = "行政区域删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
