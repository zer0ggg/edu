package com.roncoo.education.system.service.dao.impl.mapper;

import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplate;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SharingTemplateExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SharingTemplateMapper {
    int countByExample(SharingTemplateExample example);

    int deleteByExample(SharingTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SharingTemplate record);

    int insertSelective(SharingTemplate record);

    List<SharingTemplate> selectByExample(SharingTemplateExample example);

    SharingTemplate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SharingTemplate record, @Param("example") SharingTemplateExample example);

    int updateByExample(@Param("record") SharingTemplate record, @Param("example") SharingTemplateExample example);

    int updateByPrimaryKeySelective(SharingTemplate record);

    int updateByPrimaryKey(SharingTemplate record);
}
