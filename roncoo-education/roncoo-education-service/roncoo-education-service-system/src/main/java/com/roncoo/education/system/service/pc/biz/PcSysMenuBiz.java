package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AllowDeleteEnum;
import com.roncoo.education.common.core.enums.MenuTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.common.req.SysMenuEditREQ;
import com.roncoo.education.system.common.req.SysMenuSaveREQ;
import com.roncoo.education.system.common.req.SysMenuUpdateStatusREQ;
import com.roncoo.education.system.common.resp.SysMenuPermissionListRESP;
import com.roncoo.education.system.common.resp.SysMenuViewRESP;
import com.roncoo.education.system.service.dao.SysMenuDao;
import com.roncoo.education.system.service.dao.SysPermissionDao;
import com.roncoo.education.system.service.dao.SysRoleMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysPermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统菜单
 *
 * @author wujing
 */
@Component
public class PcSysMenuBiz extends BaseBiz {

    @Autowired
    private SysMenuDao dao;
    @Autowired
    private SysPermissionDao permissionDao;
    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    /**
     * 系统菜单权限列表
     *
     * @return 系统菜单权限查询结果
     */
    public Result<List<SysMenuPermissionListRESP>> list(Long parentId) {
        List<SysMenuPermissionListRESP> resultList = new ArrayList<>();

        // 返回权限
        SysMenu currentSysMenu = dao.getById(parentId);
        if (ObjectUtil.isNotNull(currentSysMenu) && MenuTypeEnum.MENU.getCode().equals(currentSysMenu.getMenuType())) {
            List<SysPermission> permissionList = permissionDao.listByMenuId(parentId);
            for (SysPermission sysPermission : permissionList) {
                SysMenuPermissionListRESP permissionResp = BeanUtil.copyProperties(sysPermission, SysMenuPermissionListRESP.class);
                permissionResp.setParentId(sysPermission.getMenuId());
                permissionResp.setType(MenuTypeEnum.PERMISSION.getCode());
                permissionResp.setName(sysPermission.getPermissionName());
                permissionResp.setValue(sysPermission.getPermissionValue());
                resultList.add(permissionResp);
            }
            return Result.success(resultList);
        }

        // 返回菜单
        List<SysMenu> sysMenuList = dao.listByParentId(parentId);
        if (CollectionUtil.isEmpty(sysMenuList)) {
            return Result.success(new ArrayList<>());
        }
        for (SysMenu sysMenu : sysMenuList) {
            SysMenuPermissionListRESP resp = BeanUtil.copyProperties(sysMenu, SysMenuPermissionListRESP.class);
            resp.setName(sysMenu.getMenuName());
            resp.setType(sysMenu.getMenuType());
            resultList.add(resp);
        }
        return Result.success(resultList);
    }

    /**
     * 系统可用菜单权限列表
     *
     * @return 系统可用菜单权限查询结果
     */
    public Result<List<SysMenuPermissionListRESP>> availableList() {
        List<SysMenu> sysMenuList = dao.listAllAvailable();
        if (CollectionUtil.isEmpty(sysMenuList)) {
            return Result.success(new ArrayList<>());
        }

        // 获取一级菜单
        List<SysMenu> parentSysMenuList = sysMenuList.stream().filter(sysMenu -> sysMenu.getParentId() == null || sysMenu.getParentId().equals(0L)).collect(Collectors.toList());

        // 递归处理返回权限菜单
        List<SysMenuPermissionListRESP> resultList = new ArrayList<>();
        for (SysMenu sysMenu : parentSysMenuList) {
            SysMenuPermissionListRESP resp = BeanUtil.copyProperties(sysMenu, SysMenuPermissionListRESP.class);
            resp.setName(sysMenu.getMenuName());
            resp.setType(sysMenu.getMenuType());
            resp.setChildrenList(recursiveAvailableMenuPermission(sysMenu.getId(), sysMenuList));
            resultList.add(resp);
        }
        return Result.success(resultList);
    }


    /**
     * 系统菜单添加
     *
     * @param sysMenuSaveREQ 系统菜单
     * @return 添加结果
     */
    public Result<String> save(SysMenuSaveREQ sysMenuSaveREQ) {
        SysMenu record = BeanUtil.copyProperties(sysMenuSaveREQ, SysMenu.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 系统菜单查看
     *
     * @param id 主键ID
     * @return 系统菜单
     */
    public Result<SysMenuViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysMenuViewRESP.class));
    }


    /**
     * 系统菜单修改
     *
     * @param sysMenuEditREQ 系统菜单修改对象
     * @return 修改结果
     */
    public Result<String> edit(SysMenuEditREQ sysMenuEditREQ) {
        SysMenu record = BeanUtil.copyProperties(sysMenuEditREQ, SysMenu.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统菜单修改
     *
     * @param sysMenuUpdateStatusREQ 系统菜单修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(SysMenuUpdateStatusREQ sysMenuUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(sysMenuUpdateStatusREQ.getStatusId()))) {
            return Result.error("传入菜单状态异常");
        }
        if (ObjectUtil.isNull(dao.getById(sysMenuUpdateStatusREQ.getId()))) {
            return Result.error("菜单不存在");
        }
        SysMenu record = BeanUtil.copyProperties(sysMenuUpdateStatusREQ, SysMenu.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统菜单删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        SysMenu sysMenu = dao.getById(id);
        if (ObjectUtil.isNull(sysMenu)) {
            return Result.error("菜单不存在");
        }
        if (!AllowDeleteEnum.ALLOW.getCode().equals(sysMenu.getAllowDelete())) {
            return Result.error("当前菜单不允许删除");
        }

        List<SysRoleMenu> roleMenuList = sysRoleMenuDao.listByMenuId(id);
        if (CollUtil.isNotEmpty(roleMenuList)) {
            return Result.error("菜单已分配到角色，请先解绑");
        }


        if (MenuTypeEnum.DIRECTORY.getCode().equals(sysMenu.getMenuType())) {
            if (CollectionUtil.isNotEmpty(dao.listByParentId(sysMenu.getId()))) {
                return Result.error("目录下存在子目录或者菜单，无法删除");
            }
        } else {
            if (CollectionUtil.isNotEmpty(permissionDao.listByMenuId(sysMenu.getId()))) {
                return Result.error("菜单下存在权限点，无法删除");
            }
        }

        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 循环迭代菜单权限
     *
     * @param parentId    父类ID
     * @param sysMenuList 系统菜单集合
     * @return 菜单权限集合列表
     */
    private List<SysMenuPermissionListRESP> recursiveMenuPermission(Long parentId, List<SysMenu> sysMenuList) {
        List<SysMenuPermissionListRESP> resultList = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            // 判断是否为该父类菜单下的菜单
            if (!parentId.equals(sysMenu.getParentId())) {
                continue;
            }

            SysMenuPermissionListRESP resp = BeanUtil.copyProperties(sysMenu, SysMenuPermissionListRESP.class);
            resp.setName(sysMenu.getMenuName());
            resp.setType(sysMenu.getMenuType());
            // 获取菜单下的权限
            if (MenuTypeEnum.MENU.getCode().equals(sysMenu.getMenuType())) {
                List<SysPermission> permissionList = permissionDao.listByMenuId(sysMenu.getId());
                if (CollectionUtil.isNotEmpty(permissionList)) {
                    List<SysMenuPermissionListRESP> permissionRespList = new ArrayList<>();
                    for (SysPermission sysPermission : permissionList) {
                        SysMenuPermissionListRESP permissionResp = BeanUtil.copyProperties(sysPermission, SysMenuPermissionListRESP.class);
                        permissionResp.setParentId(sysPermission.getMenuId());
                        permissionResp.setType(MenuTypeEnum.PERMISSION.getCode());
                        permissionResp.setName(sysPermission.getPermissionName());
                        permissionResp.setValue(sysPermission.getPermissionValue());
                        permissionRespList.add(permissionResp);
                    }
                    resp.setChildrenList(permissionRespList);
                }
            } else {
                // 目录则获取目录下的节点信息
                resp.setChildrenList(recursiveMenuPermission(sysMenu.getId(), sysMenuList));
            }
            resultList.add(resp);
        }
        return resultList;
    }

    /**
     * 循环迭代菜单权限
     *
     * @param parentId    父类ID
     * @param sysMenuList 系统菜单集合
     * @return 菜单权限集合列表
     */
    private List<SysMenuPermissionListRESP> recursiveAvailableMenuPermission(Long parentId, List<SysMenu> sysMenuList) {
        List<SysMenuPermissionListRESP> resultList = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            // 判断是否为该父类菜单下的菜单
            if (!parentId.equals(sysMenu.getParentId())) {
                continue;
            }

            SysMenuPermissionListRESP resp = BeanUtil.copyProperties(sysMenu, SysMenuPermissionListRESP.class);
            resp.setName(sysMenu.getMenuName());
            resp.setType(sysMenu.getMenuType());
            // 获取菜单下的权限
            if (MenuTypeEnum.MENU.getCode().equals(sysMenu.getMenuType())) {
                List<SysPermission> permissionList = permissionDao.listByMenuIdAndStatusId(sysMenu.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(permissionList)) {
                    List<SysMenuPermissionListRESP> permissionRespList = new ArrayList<>();
                    for (SysPermission sysPermission : permissionList) {
                        SysMenuPermissionListRESP permissionResp = BeanUtil.copyProperties(sysPermission, SysMenuPermissionListRESP.class);
                        permissionResp.setParentId(sysPermission.getMenuId());
                        permissionResp.setType(MenuTypeEnum.PERMISSION.getCode());
                        permissionResp.setName(sysPermission.getPermissionName());
                        permissionResp.setValue(sysPermission.getPermissionValue());
                        permissionRespList.add(permissionResp);
                    }
                    resp.setChildrenList(permissionRespList);
                }
            } else {
                // 目录则获取目录下的节点信息
                resp.setChildrenList(recursiveAvailableMenuPermission(sysMenu.getId(), sysMenuList));
            }
            resultList.add(resp);
        }
        return resultList;
    }
}
