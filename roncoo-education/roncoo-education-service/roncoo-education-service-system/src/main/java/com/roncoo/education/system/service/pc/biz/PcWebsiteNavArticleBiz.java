package com.roncoo.education.system.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.WebsiteNavArticleEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.system.common.req.WebsiteNavArticleEditREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleListREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleSaveREQ;
import com.roncoo.education.system.common.req.WebsiteNavArticleUpdateOrSaveByNavIdREQ;
import com.roncoo.education.system.common.resp.WebsiteNavArticleListRESP;
import com.roncoo.education.system.common.resp.WebsiteNavArticleViewRESP;
import com.roncoo.education.system.service.dao.WebsiteNavArticleDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavArticle;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavArticleExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.WebsiteNavArticleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 站点导航文章
 *
 * @author wujing
 */
@Component
public class PcWebsiteNavArticleBiz extends BaseBiz {

    @Autowired
    private WebsiteNavArticleDao dao;

    /**
    * 站点导航文章列表
    *
    * @param websiteNavArticleListREQ 站点导航文章分页查询参数
    * @return 站点导航文章分页查询结果
    */
    public Result<Page<WebsiteNavArticleListRESP>> list(WebsiteNavArticleListREQ websiteNavArticleListREQ) {
        WebsiteNavArticleExample example = new WebsiteNavArticleExample();
        Criteria c = example.createCriteria();
        Page<WebsiteNavArticle> page = dao.listForPage(websiteNavArticleListREQ.getPageCurrent(), websiteNavArticleListREQ.getPageSize(), example);
        Page<WebsiteNavArticleListRESP> respPage = PageUtil.transform(page, WebsiteNavArticleListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 站点导航文章添加
    *
    * @param req 站点导航文章
    * @return 添加结果
    */
    public Result<String> save(WebsiteNavArticleSaveREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        //防止重复
        WebsiteNavArticle record = dao.getByNavId(req.getNavId());
        if(!ObjectUtils.isEmpty(record)){
            return Result.error("不能重复添加");
        }
        if(WebsiteNavArticleEnum.ARTICLE.getCode().equals(req.getType())){
            if(StringUtils.isEmpty(req.getArtTitle())){
                return Result.error("文章标题不能为空");
            }
            req.setUrl(null);
        }else if(WebsiteNavArticleEnum.LINKURL.getCode().equals(req.getType())){
            if(StringUtils.isEmpty(req.getUrl())){
                return Result.error("链接url不能为空");
            }
            req.setArtTitle(null);
        }else {
            return Result.error("类型错误");
        }
        WebsiteNavArticle bean = BeanUtil.copyProperties(req, WebsiteNavArticle.class);

        if (dao.save(bean) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 站点导航文章查看
    *
    * @param id 主键ID
    * @return 站点导航文章
    */
    public Result<WebsiteNavArticleViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), WebsiteNavArticleViewRESP.class));
    }

    public Result<WebsiteNavArticleViewRESP> viewByNavId(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getByNavId(id), WebsiteNavArticleViewRESP.class));
    }


    /**
    * 站点导航文章修改
    *
    * @param req 站点导航文章修改对象
    * @return 修改结果
    */
    public Result<String> edit(WebsiteNavArticleEditREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        WebsiteNavArticle record = BeanUtil.copyProperties(req, WebsiteNavArticle.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 站点导航文章删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<String> updateOrSaveByNavId(WebsiteNavArticleUpdateOrSaveByNavIdREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        WebsiteNavArticle record = dao.getByNavId(req.getNavId());
        //新增记录
        if(ObjectUtils.isEmpty(record)){
            if(WebsiteNavArticleEnum.ARTICLE.getCode().equals(req.getType())){
                if(StringUtils.isEmpty(req.getArtTitle())){
                    return Result.error("文章标题不能为空");
                }
                req.setUrl(null);
            }else if(WebsiteNavArticleEnum.LINKURL.getCode().equals(req.getType())){
                if(StringUtils.isEmpty(req.getUrl())){
                    return Result.error("链接url不能为空");
                }
                req.setArtTitle(null);
                req.setArtDesc(null);
            }else {
                return Result.error("类型错误");
            }
            WebsiteNavArticle bean = BeanUtil.copyProperties(req, WebsiteNavArticle.class);
            if (dao.save(bean) > 0) {
                return Result.success("添加成功");
            }
        }else {//修改记录
            WebsiteNavArticle newRecord = BeanUtil.copyProperties(req, WebsiteNavArticle.class);
            newRecord.setId(record.getId());
            //切换类型，则重置部分数据
            if(WebsiteNavArticleEnum.ARTICLE.getCode().equals(req.getType())){
                req.setUrl(null);
            }else if(WebsiteNavArticleEnum.LINKURL.getCode().equals(req.getType())){
                req.setArtTitle(null);
                req.setArtDesc(null);
            }else {
                return Result.error("类型错误");
            }
            if (dao.updateById(newRecord) > 0) {
                return Result.success("编辑成功");
            }
        }
        return Result.success("操作成功");

    }
}
