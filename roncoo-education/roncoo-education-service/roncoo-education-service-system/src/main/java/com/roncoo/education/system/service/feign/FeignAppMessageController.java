package com.roncoo.education.system.service.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignAppMessage;
import com.roncoo.education.system.feign.qo.AppMessageQO;
import com.roncoo.education.system.feign.vo.AppMessageVO;
import com.roncoo.education.system.service.feign.biz.FeignAppMessageBiz;

/**
 * app推送
 *
 * @author wujing
 * @date 2020-11-17
 */
@RestController
public class FeignAppMessageController extends BaseController implements IFeignAppMessage{

    @Autowired
    private FeignAppMessageBiz biz;

	@Override
	public Page<AppMessageVO> listForPage(@RequestBody AppMessageQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody AppMessageQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody AppMessageQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public AppMessageVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
