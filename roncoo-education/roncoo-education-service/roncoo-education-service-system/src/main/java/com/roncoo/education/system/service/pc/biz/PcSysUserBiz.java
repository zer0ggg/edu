package com.roncoo.education.system.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.AllowDeleteEnum;
import com.roncoo.education.common.core.enums.MenuTypeEnum;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.JSUtil;
import com.roncoo.education.common.core.tools.SystemSignUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.system.common.req.*;
import com.roncoo.education.system.common.resp.SysUserListRESP;
import com.roncoo.education.system.common.resp.SysUserViewRESP;
import com.roncoo.education.system.service.dao.*;
import com.roncoo.education.system.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysUserExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 系统用户
 *
 * @author wujing
 */
@Component
public class PcSysUserBiz extends BaseBiz {

    @Autowired
    private SysUserDao dao;
    @Autowired
    private SysUserRoleDao userRoleDao;
    @Autowired
    private SysRoleDao roleDao;
    @Autowired
    private SysRoleMenuDao roleMenuDao;
    @Autowired
    private SysMenuDao menuDao;

    @Autowired
    private SysRolePermissionDao rolePermissionDao;
    @Autowired
    private SysPermissionDao permissionDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 系统用户列表
     *
     * @param sysUserListREQ 系统用户分页查询参数
     * @return 系统用户分页查询结果
     */
    public Result<Page<SysUserListRESP>> list(SysUserListREQ sysUserListREQ) {
        SysUserExample example = new SysUserExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(sysUserListREQ.getStatusId())) {
            c.andStatusIdEqualTo(sysUserListREQ.getStatusId());
        }
        if (StrUtil.isNotBlank(sysUserListREQ.getNickname())) {
            c.andNicknameLike(PageUtil.like(sysUserListREQ.getNickname()));
        }
        if (StrUtil.isNotBlank(sysUserListREQ.getLoginName())) {
            c.andLoginNameLike(PageUtil.like(sysUserListREQ.getLoginName()));
        }
        example.setOrderByClause("sort asc, id desc");
        Page<SysUser> page = dao.listForPage(sysUserListREQ.getPageCurrent(), sysUserListREQ.getPageSize(), example);
        Page<SysUserListRESP> respPage = PageUtil.transform(page, SysUserListRESP.class);
        return Result.success(respPage);
    }

    /**
     * 系统用户添加
     *
     * @param sysUserSaveREQ 系统用户
     * @return 添加结果
     */
    public Result<String> save(SysUserSaveREQ sysUserSaveREQ) {
        if (!sysUserSaveREQ.getLoginPassword().equals(sysUserSaveREQ.getConfirmPassword())) {
            return Result.error("登录密码和确认密码不一致");
        }
        if (ObjectUtil.isNotNull(dao.getByLoginName(sysUserSaveREQ.getLoginName()))) {
            return Result.error("【" + sysUserSaveREQ.getLoginName() + "】该登录账号已存在");
        }
        SysUser record = BeanUtil.copyProperties(sysUserSaveREQ, SysUser.class);
        record.setLoginPassword(SystemSignUtil.sign(sysUserSaveREQ.getLoginPassword(), sysUserSaveREQ.getLoginName()));
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 系统用户查看
     *
     * @param id 主键ID
     * @return 系统用户
     */
    public Result<SysUserViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), SysUserViewRESP.class));
    }

    /**
     * 系统用户修改
     *
     * @param sysUserEditREQ 系统用户修改对象
     * @return 修改结果
     */
    public Result<String> edit(SysUserEditREQ sysUserEditREQ) {
        SysUser record = BeanUtil.copyProperties(sysUserEditREQ, SysUser.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 修改状态
     *
     * @param sysUserUpdateStatusREQ 系统用户修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(SysUserUpdateStatusREQ sysUserUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(sysUserUpdateStatusREQ.getStatusId()))) {
            return Result.error("输入状态异常，无法修改");
        }
        if (ObjectUtil.isNull(dao.getById(sysUserUpdateStatusREQ.getId()))) {
            return Result.error("用户不存在");
        }

        SysUser record = BeanUtil.copyProperties(sysUserUpdateStatusREQ, SysUser.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 系统用户删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        SysUser sysUser = dao.getById(id);
        if (ObjectUtil.isNull(sysUser)) {
            return Result.error("用户不存在");
        }
        if (!AllowDeleteEnum.ALLOW.getCode().equals(sysUser.getAllowDelete())) {
            return Result.error("当前用户不允许删除");
        }
        int count = userRoleDao.countByUserId(id);
        if (count > 0) {
            if (count != userRoleDao.deleteByUserId(id)) {
                throw new BaseException("删除系统用户失败");
            }
        }

        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 分配菜单
     *
     * @param sysUserAllocationRoleREQ 分配角色参数
     * @return 分配结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> allocationRole(SysUserAllocationRoleREQ sysUserAllocationRoleREQ) {
        if (ObjectUtil.isNull(dao.getById(sysUserAllocationRoleREQ.getId()))) {
            return Result.error("用户不存在");
        }

        int count = userRoleDao.countByUserId(sysUserAllocationRoleREQ.getId());
        if (count > 0) {
            if (count != userRoleDao.deleteByUserId(sysUserAllocationRoleREQ.getId())) {
                throw new BaseException("删除系统用户失败");
            }
        }

        if (CollectionUtil.isNotEmpty(sysUserAllocationRoleREQ.getRoleIdList())) {
            for (Long roleId : sysUserAllocationRoleREQ.getRoleIdList()) {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setUserId(sysUserAllocationRoleREQ.getId());
                sysUserRole.setRoleId(roleId);
                userRoleDao.save(sysUserRole);
            }
        }
        return Result.success("分配成功");
    }

    /**
     * 修改免密
     *
     * @param sysUserUpdatePasswordREQ 修改密码参数
     * @return 修改结果
     */
    public Result<String> updatePassword(SysUserUpdatePasswordREQ sysUserUpdatePasswordREQ) {
        SysUser queryUser = dao.getById(sysUserUpdatePasswordREQ.getId());
        if (ObjectUtil.isNull(queryUser)) {
            return Result.error("用户不存在");
        }
        if (!sysUserUpdatePasswordREQ.getLoginPassword().equals(sysUserUpdatePasswordREQ.getConfirmPassword())) {
            return Result.error("登录密码和确认密码不一致");
        }
        return updatePassword(queryUser, sysUserUpdatePasswordREQ.getLoginPassword());
    }

    /**
     * 修改当前用户密码
     *
     * @param sysUserUpdatePasswordREQ 修改密码参数
     * @return 修改结果
     */
    public Result<String> updatePasswordCurrent(SysUserUpdatePasswordCurrentREQ sysUserUpdatePasswordREQ) {
        SysUser queryUser = dao.getByLoginName(ThreadContext.loginName());
        if (ObjectUtil.isNull(queryUser)) {
            return Result.error("用户不存在");
        }
        if (!SystemSignUtil.verifySign(sysUserUpdatePasswordREQ.getOldPassword(), queryUser.getLoginName(), queryUser.getLoginPassword())) {
            return Result.error("原密码有误");
        }
        if (!sysUserUpdatePasswordREQ.getLoginPassword().equals(sysUserUpdatePasswordREQ.getConfirmPassword())) {
            return Result.error("登录密码和确认密码不一致");
        }
        return updatePassword(queryUser, sysUserUpdatePasswordREQ.getLoginPassword());
    }

    /**
     * 修改用户密码
     *
     * @param queryUser 用户信息
     * @param password  新密码
     * @return 处理结果
     */
    private Result<String> updatePassword(SysUser queryUser, String password) {
        SysUser sysUser = new SysUser();
        sysUser.setId(queryUser.getId());
        sysUser.setLoginPassword(SystemSignUtil.sign(password, queryUser.getLoginName()));
        if (dao.updateById(sysUser) > 0) {
            return Result.success("修改密码成功");
        }
        return Result.error("修改密码失败");
    }

    /**
     * 根据用户ID获取关联角色
     *
     * @param id 用户ID
     * @return 关联角色ID集合
     */
    public Result<List<Long>> listSelectRoleById(Long id) {
        List<SysUserRole> userRoleList = userRoleDao.listByUserId(id);
        if (CollectionUtil.isEmpty(userRoleList)) {
            return Result.success(new ArrayList<>());
        }
        return Result.success(userRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList()));
    }

    /**
     * 列出用户权限点
     *
     * @return 用户权限点
     */
    public Result<List<String>> listUserPermission() {
        // 用户校验
        SysUser sysUser = dao.getByLoginName(ThreadContext.loginName());
        if (null == sysUser) {
            return Result.error("当前用户不存在");
        }
        // 获取权限
        return Result.success(getUserPermissionValueList(sysUser));
    }

    public List<String> getUserPermissionValueList(SysUser sysUser) {
        List<String> permissionValueList = new ArrayList<>();
        List<SysUserRole> userRoleList = userRoleDao.listByUserId(sysUser.getId());
        if (CollectionUtil.isNotEmpty(userRoleList)) {
            List<Long> roleIdList = userRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
            List<SysRole> roleList = roleDao.listInIdListAndStatusId(roleIdList, StatusIdEnum.YES.getCode());

            if (CollectionUtil.isNotEmpty(roleList)) {
                List<Long> availableRoleIdList = roleList.stream().map(SysRole::getId).collect(Collectors.toList());
                List<SysRolePermission> rolePermissionList = rolePermissionDao.listInRoleIdList(availableRoleIdList);

                if (CollectionUtil.isNotEmpty(rolePermissionList)) {
                    List<Long> permissionIdList = rolePermissionList.stream().map(SysRolePermission::getPermissionId).collect(Collectors.toList());
                    List<SysPermission> permissionList = permissionDao.listInIdListAndStatusId(permissionIdList, StatusIdEnum.YES.getCode());

                    if (CollectionUtil.isNotEmpty(permissionList)) {
                        permissionValueList = permissionList.stream().map(SysPermission::getPermissionValue).collect(Collectors.toList());
                    }

                }
            }
        }

        // 缓存当前用户登录权限 有效时间1小时
        myRedisTemplate.set(RedisPreEnum.SYS_USER_PERMISSION.getCode().concat(sysUser.getLoginName()), JSUtil.toJSONString(permissionValueList), 1, TimeUnit.DAYS);
        return permissionValueList;
    }

    /**
     * 列出用户菜单
     *
     * @return 用户菜单
     */
    public Result<List<Map<String, Object>>> listUserMenu() {
        SysUser sysUser = dao.getByLoginName(ThreadContext.loginName());
        if (ObjectUtil.isNull(sysUser)) {
            return Result.error("当前用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(sysUser.getStatusId())) {
            return Result.error("当前用户不可用");
        }

        // 获取用户角色关联
        List<SysUserRole> userRoleList = userRoleDao.listByUserId(sysUser.getId());
        if (CollectionUtil.isEmpty(userRoleList)) {
            return Result.error("当前用户没有可用菜单");
        }

        // 获取用户可用角色
        List<Long> roleIdList = userRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        List<SysRole> roleList = roleDao.listInIdListAndStatusId(roleIdList, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(roleList)) {
            return Result.error("当前用户没有可用菜单");
        }

        // 获取角色菜单关联
        List<Long> availableRoleIdList = roleList.stream().map(SysRole::getId).collect(Collectors.toList());
        List<SysRoleMenu> roleMenuList = roleMenuDao.listInRoleIdList(availableRoleIdList);
        if (CollectionUtil.isEmpty(roleMenuList)) {
            return Result.error("当前用户没有可用菜单");
        }

        // 获取用户可用菜单
        List<Long> menuIdList = roleMenuList.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        List<SysMenu> menuList = menuDao.listInIdListAndStatusId(menuIdList, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(menuList)) {
            return Result.error("当前用户没有可用菜单");
        }

        // 获取一级菜单
        List<SysMenu> parentSysMenuList = menuList.stream().filter(sysMenu -> sysMenu.getParentId() == null || sysMenu.getParentId().equals(0L)).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(parentSysMenuList)) {
            return Result.error("当前用户没有可用菜单");
        }

        // 封装菜单
        List<Map<String, Object>> menuResultList = new ArrayList<>();

        // 固定菜单
        Map<String, Object> homeMap = new HashMap<>();
        homeMap.put("id", 1);
        homeMap.put("name", "首页");
        homeMap.put("path", "/dashboard");
        homeMap.put("targetName", null);
        homeMap.put("hidden", true);
        menuResultList.add(homeMap);

        for (SysMenu parentMenu : parentSysMenuList) {
            Map<String, Object> menuMap = new HashMap<>();
            menuMap.put("id", parentMenu.getId());
            menuMap.put("name", parentMenu.getMenuName());
            menuMap.put("path", parentMenu.getRouterUrl());
            menuMap.put("targetName", parentMenu.getIcon());

            // 判断当前菜单是否为目录，是则循环获取子菜单
            if (MenuTypeEnum.DIRECTORY.getCode().equals(parentMenu.getMenuType())) {
                List<Map<String, Object>> children = recursiveMenu(parentMenu.getId(), menuList);
                if (children.size() > 0) {
                    menuMap.put("children", children);
                }
            }
            menuResultList.add(menuMap);
        }

        return Result.success(menuResultList);
    }

    /**
     * 循环迭代菜单
     *
     * @param parentId 父级菜单ID
     * @param menuList 菜单列表
     * @return 菜单树集合
     */
    private List<Map<String, Object>> recursiveMenu(Long parentId, List<SysMenu> menuList) {
        List<Map<String, Object>> childrenList = new ArrayList<>();

        for (SysMenu sysMenu : menuList) {
            // 判断是否属于当前父级菜单下的菜单
            if (parentId.equals(sysMenu.getParentId())) {
                Map<String, Object> menuMap = new JSONObject();
                menuMap.put("id", sysMenu.getId());
                menuMap.put("name", sysMenu.getMenuName());
                menuMap.put("path", sysMenu.getRouterUrl());
                menuMap.put("targetName", sysMenu.getIcon());

                // 判断当前菜单是否为目录，是则循环获取子菜单
                if (MenuTypeEnum.DIRECTORY.getCode().equals(sysMenu.getMenuType())) {
                    List<Map<String, Object>> children = recursiveMenu(sysMenu.getId(), menuList);
                    if (children.size() > 0) {
                        menuMap.put("children", children);
                    }
                }

                // 加入菜单集合
                childrenList.add(menuMap);
            }
        }
        return childrenList;
    }
}
