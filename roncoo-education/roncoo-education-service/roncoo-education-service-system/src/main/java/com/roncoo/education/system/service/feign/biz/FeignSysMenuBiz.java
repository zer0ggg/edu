package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysMenuQO;
import com.roncoo.education.system.feign.vo.SysMenuVO;
import com.roncoo.education.system.service.dao.SysMenuDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenu;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenuExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysMenuExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统菜单
 *
 * @author wujing
 */
@Component
public class FeignSysMenuBiz extends BaseBiz {

    @Autowired
    private SysMenuDao dao;

	public Page<SysMenuVO> listForPage(SysMenuQO qo) {
	    SysMenuExample example = new SysMenuExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysMenu> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysMenuVO.class);
	}

	public int save(SysMenuQO qo) {
		SysMenu record = BeanUtil.copyProperties(qo, SysMenu.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysMenuVO getById(Long id) {
		SysMenu record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysMenuVO.class);
	}

	public int updateById(SysMenuQO qo) {
		SysMenu record = BeanUtil.copyProperties(qo, SysMenu.class);
		return dao.updateById(record);
	}

}
