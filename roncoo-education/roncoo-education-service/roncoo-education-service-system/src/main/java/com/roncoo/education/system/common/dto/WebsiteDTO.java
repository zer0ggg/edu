package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 站点信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class WebsiteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "站点域名")
	private String domain;

	@ApiModelProperty(value = "H5站点域名")
	private String h5Domain;

	@ApiModelProperty(value = "站点ICO")
	private String ico;

	@ApiModelProperty(value = "站点logo")
	private String logo;

	@ApiModelProperty(value = "站点logo,无文字")
	private String logoNoWord ;

	@ApiModelProperty(value = "移动端标题")
	private String midTitle;

	@ApiModelProperty(value = "站点标题")
	private String title;

	@ApiModelProperty(value = "站点关键词")
	private String keyword;

	@ApiModelProperty(value = "站点描述")
	private String desc;

	@ApiModelProperty(value = "站点版权")
	private String copyright;

	@ApiModelProperty(value = "备案号")
	private String icp;

	@ApiModelProperty(value = "公安备案号")
	private String prnNo;

	@ApiModelProperty(value = "公安备案")
	private String prn;

	@ApiModelProperty(value = "微信二维码")
	private String wxCode;

	@ApiModelProperty(value = "微信小程序二维码")
	private String minappCode;

	@ApiModelProperty(value = "微博二维码")
	private String wbCode;

	@ApiModelProperty(value = "是否开启统计")
	private Integer isEnableStatistics;

	@ApiModelProperty(value = "统计代码")
	private String statisticsCode;

	@ApiModelProperty(value = "是否显示客服信息")
	private Integer isShowService;

	@ApiModelProperty(value = "客服信息1")
	private String service1;

	@ApiModelProperty(value = "客服信息2")
	private String service2;

	@ApiModelProperty(value = "用户协议")
	private String userAgreement;

	@ApiModelProperty(value = "会员协议")
	private String vipAgreement;

	@ApiModelProperty(value = "隐私协议")
	private String privacyAgreement;

	@ApiModelProperty(value = "入驻协议")
	private String recruitAgreement;

	@ApiModelProperty(value = "招募标题")
	private String recruitTitle;

	@ApiModelProperty(value = "招募信息")
	private String recruitInfo;

	@ApiModelProperty(value = "直播AppID")
	private String polyvAppID;

	@ApiModelProperty(value = "直播AppSecret")
	private String polyvAppSecret;

	@ApiModelProperty(value = "是否显示视频（1：显示； 0：不显示）")
	private Integer isShowVideo;

	@ApiModelProperty(value = "是否显示价格（1：显示； 0：不显示）")
	private Integer isShowPrice;

	@ApiModelProperty(value = "小程序审核模式（1: 审核后设置； 0：审核中设置）")
	private Integer isMinappAudit;

	@ApiModelProperty(value = "是否需要人脸对比(0:否，1:是)")
	private Integer isFaceContras;
	@ApiModelProperty(value = "直播平台 (1保利威，2欢拓云)")
	private Integer livePlatform;


}
