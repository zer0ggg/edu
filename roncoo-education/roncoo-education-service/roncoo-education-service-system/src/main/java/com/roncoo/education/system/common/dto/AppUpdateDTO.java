package com.roncoo.education.system.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 站点信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AppUpdateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "当前版本号")
	private BigDecimal version;

	@ApiModelProperty(value = "是否有更新")
	private Boolean update;

	@ApiModelProperty(value = "wgt 包的下载地址，用于 wgt 方式更新")
	private String wgtUrl;

	@ApiModelProperty(value = "apk/ipa 包的下载地址或 AppStore 地址，用于整包升级的方式。")
	private String pkgUrl;
}
