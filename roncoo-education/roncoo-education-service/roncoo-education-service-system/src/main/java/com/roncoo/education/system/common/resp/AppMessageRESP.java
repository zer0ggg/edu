package com.roncoo.education.system.common.resp;

import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * app推送
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AppMessageRESP", description="app推送")
public class AppMessageRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "产品ID")
    private Long productId;

    @ApiModelProperty(value = "跳转分类(1:点播;2:直播,3文库,4试卷,;5:资讯)")
    private Integer jumpCategory;

    @ApiModelProperty(value = "是否已发送(1是;0否)")
    private Integer isSend;

    @ApiModelProperty(value = "发送时间")
    private LocalDateTime sendTime;
}
