package com.roncoo.education.system.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessage;
import com.roncoo.education.system.service.dao.impl.mapper.entity.AppMessageExample;

/**
 * app推送 服务类
 *
 * @author wujing
 * @date 2020-11-17
 */
public interface AppMessageDao {

    /**
    * 保存app推送
    *
    * @param record app推送
    * @return 影响记录数
    */
    int save(AppMessage record);

    /**
    * 根据ID删除app推送
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record app推送
    * @return 影响记录数
    */
    int updateById(AppMessage record);

    /**
    * 根据ID获取app推送
    *
    * @param id 主键ID
    * @return app推送
    */
    AppMessage getById(Long id);

    /**
    * app推送--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<AppMessage> listForPage(int pageCurrent, int pageSize, AppMessageExample example);

}
