package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 会员设置
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="VipSetListREQ", description="会员设置列表")
public class VipSetListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "状态(1:有效;0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "设置类型(1.年费，2.季度，3.月度)")
    private Integer setType;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
