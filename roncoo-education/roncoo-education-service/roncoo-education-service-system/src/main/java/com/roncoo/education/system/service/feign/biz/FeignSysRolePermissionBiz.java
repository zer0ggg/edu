package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.SysRolePermissionQO;
import com.roncoo.education.system.feign.vo.SysRolePermissionVO;
import com.roncoo.education.system.service.dao.SysRolePermissionDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermission;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.SysRolePermissionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 系统角色权限
 *
 * @author wujing
 */
@Component
public class FeignSysRolePermissionBiz extends BaseBiz {

    @Autowired
    private SysRolePermissionDao dao;

	public Page<SysRolePermissionVO> listForPage(SysRolePermissionQO qo) {
	    SysRolePermissionExample example = new SysRolePermissionExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<SysRolePermission> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, SysRolePermissionVO.class);
	}

	public int save(SysRolePermissionQO qo) {
		SysRolePermission record = BeanUtil.copyProperties(qo, SysRolePermission.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public SysRolePermissionVO getById(Long id) {
		SysRolePermission record = dao.getById(id);
		return BeanUtil.copyProperties(record, SysRolePermissionVO.class);
	}

	public int updateById(SysRolePermissionQO qo) {
		SysRolePermission record = BeanUtil.copyProperties(qo, SysRolePermission.class);
		return dao.updateById(record);
	}

}
