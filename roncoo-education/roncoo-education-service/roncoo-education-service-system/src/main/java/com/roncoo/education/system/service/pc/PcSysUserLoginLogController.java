package com.roncoo.education.system.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.system.common.req.SysUserLoginLogEditREQ;
import com.roncoo.education.system.common.req.SysUserLoginLogListREQ;
import com.roncoo.education.system.common.req.SysUserLoginLogSaveREQ;
import com.roncoo.education.system.common.resp.SysUserLoginLogListRESP;
import com.roncoo.education.system.common.resp.SysUserLoginLogViewRESP;
import com.roncoo.education.system.service.pc.biz.PcSysUserLoginLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统用户登录日志 Pc接口
 *
 * @author LYQ
 */
@RestController
@RequestMapping("/system/pc/sys/user/login/log")
@Api(value = "system-系统用户登录日志", tags = {"system-系统用户登录日志"})
public class PcSysUserLoginLogController {

    @Autowired
    private PcSysUserLoginLogBiz biz;

    @ApiOperation(value = "系统用户登录日志列表", notes = "系统用户登录日志列表")
    @PostMapping(value = "/list")
    public Result<Page<SysUserLoginLogListRESP>> list(@RequestBody SysUserLoginLogListREQ sysUserLoginLogListREQ) {
        return biz.list(sysUserLoginLogListREQ);
    }

    @ApiOperation(value = "系统用户登录日志添加", notes = "系统用户登录日志添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody SysUserLoginLogSaveREQ sysUserLoginLogSaveREQ) {
        return biz.save(sysUserLoginLogSaveREQ);
    }

    @ApiOperation(value = "系统用户登录日志查看", notes = "系统用户登录日志查看")
    @GetMapping(value = "/view")
    public Result<SysUserLoginLogViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "系统用户登录日志修改", notes = "系统用户登录日志修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody SysUserLoginLogEditREQ sysUserLoginLogEditREQ) {
        return biz.edit(sysUserLoginLogEditREQ);
    }

    @ApiOperation(value = "系统用户登录日志删除", notes = "系统用户登录日志删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
