package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignSysUserRole;
import com.roncoo.education.system.feign.qo.SysUserRoleQO;
import com.roncoo.education.system.feign.vo.SysUserRoleVO;
import com.roncoo.education.system.service.feign.biz.FeignSysUserRoleBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统用户角色
 *
 * @author wujing
 * @date 2020-04-29
 */
@RestController
public class FeignSysUserRoleController extends BaseController implements IFeignSysUserRole{

    @Autowired
    private FeignSysUserRoleBiz biz;

	@Override
	public Page<SysUserRoleVO> listForPage(@RequestBody SysUserRoleQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody SysUserRoleQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody SysUserRoleQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public SysUserRoleVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
