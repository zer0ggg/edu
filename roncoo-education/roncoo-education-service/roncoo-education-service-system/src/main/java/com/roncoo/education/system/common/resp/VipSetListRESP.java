package com.roncoo.education.system.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 会员设置
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="VipSetListRESP", description="会员设置列表")
public class VipSetListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:有效;0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "设置类型(1.年费，2.季度，3.月度)")
    private Integer setType;

    @ApiModelProperty(value = "会员原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "会员优惠价")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "续费价格")
    private BigDecimal renewalPrice;
}
