package com.roncoo.education.system.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 站点导航
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="WebsiteNavListREQ", description="站点导航列表")
public class WebsiteNavListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1有效, 0无效)")
    private Integer statusId;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "导航名称")
    private String navName;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
