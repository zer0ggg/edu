package com.roncoo.education.system.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.system.feign.interfaces.IFeignZone;
import com.roncoo.education.system.feign.qo.ZoneQO;
import com.roncoo.education.system.feign.vo.ZoneVO;
import com.roncoo.education.system.service.feign.biz.FeignZoneBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 专区
 *
 * @author wujing
 * @date 2020-05-23
 */
@RestController
public class FeignZoneController extends BaseController implements IFeignZone{

    @Autowired
    private FeignZoneBiz biz;

	@Override
	public Page<ZoneVO> listForPage(@RequestBody ZoneQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ZoneQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ZoneQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ZoneVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
