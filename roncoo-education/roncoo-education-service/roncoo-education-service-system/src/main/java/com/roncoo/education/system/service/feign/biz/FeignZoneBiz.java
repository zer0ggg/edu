package com.roncoo.education.system.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.system.feign.qo.ZoneQO;
import com.roncoo.education.system.feign.vo.ZoneVO;
import com.roncoo.education.system.service.dao.ZoneDao;
import com.roncoo.education.system.service.dao.impl.mapper.entity.Zone;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample;
import com.roncoo.education.system.service.dao.impl.mapper.entity.ZoneExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 专区
 *
 * @author wujing
 */
@Component
public class FeignZoneBiz extends BaseBiz {

    @Autowired
    private ZoneDao dao;

	public Page<ZoneVO> listForPage(ZoneQO qo) {
	    ZoneExample example = new ZoneExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<Zone> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ZoneVO.class);
	}

	public int save(ZoneQO qo) {
		Zone record = BeanUtil.copyProperties(qo, Zone.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ZoneVO getById(Long id) {
		Zone record = dao.getById(id);
		return BeanUtil.copyProperties(record, ZoneVO.class);
	}

	public int updateById(ZoneQO qo) {
		Zone record = BeanUtil.copyProperties(qo, Zone.class);
		return dao.updateById(record);
	}

}
