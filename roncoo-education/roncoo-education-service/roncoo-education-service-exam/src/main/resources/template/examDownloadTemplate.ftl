<html lang="en" data-n-head="%7B%22lang%22:%7B%22ssr%22:%22en%22%7D%7D">
	<head>
    	<title>开发环境-领课教育系统</title>
	</head>
	<body>

		<div class="exam-content container" style="margin: 0 auto 20px; display: flex; width: 1200px;">
			<div class="exam-list container" style="flex: 1; min-height: 400px; width: 1200px; margin: 0 auto;">
				<!-- 试卷名称 -->
				<div class="issus-item" style="padding: 10px 20px; background: #fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;">
					<div class="issus-item-title" style="font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;">${exam.examName}</div>
					<div class="issus-item-tip" style="font-weight: 400; font-style: normal; font-size: 12px; color: #aaa; line-height: 20px;">
						<p>${exam.examName}</p>
					</div>
				</div>

				<!-- 试卷标题 -->
				<#if exam.titleList?? && (exam.titleList?size > 0) >
				<#list exam.titleList as title>
				<div class="issus-item" style="padding: 10px 20px; background: #fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;">
					<div class="issus-item-title" style="font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;">${title.no}、${title.titleName}</div>
				</div>
					<#--试卷题目-->
					<#if title.problemList?? && (title.problemList?size > 0) >
					<#list title.problemList as problem>
				<div class="issus-item" style="padding: 10px 20px; background: #fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;">
					<div class="issus-item-title" style="font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;">
						${problem.no}、${problem.problemContent}
					</div>
				</div>
				<div class="issus-item-answer" style="font-weight: 400; font-style: normal; font-size: 12px; color: #333;line-height: 28px; ">
						<!--试题选项-->
						<#if problem.optionList?? && (problem.optionList?size > 0) >
						<#list problem.optionList as option>
							<div><span> ${option.no}．</span> ${option.optionContent}</div>
						</#list>
						</#if>
						<!--题目小题-->
						<#if problem.childrenList?? && (problem.childrenList?size > 0) >
						<#list problem.childrenList as subProblem>
						<div class="issus-item" style="padding: 10px 20px; background: #fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;">
							<div class="issus-item-title" style="font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;">
								${subProblem.no}、<div class="issus-item-title-text" style="flex: 1;">${subProblem.problemContent}</div>
							</div>
							<div class="issus-item-answer" style="font-weight: 400; font-style: normal; font-size: 12px; color: #333;line-height: 28px; ">
							<!--题目小题选项-->
							<#if subProblem.optionList?? && (subProblem.optionList?size > 0) >
							<#list subProblem.optionList as subOption>
								<div><span> ${subOption.no}．</span> ${subOption.optionContent}</div>
							</#list>
							</#if>
							</div>
						</div>
						</#list>
						</#if>
				</div>
					</#list>
					</#if>
				</#list>
				</#if>
			</div>
		</div>
	</body>
</html>
