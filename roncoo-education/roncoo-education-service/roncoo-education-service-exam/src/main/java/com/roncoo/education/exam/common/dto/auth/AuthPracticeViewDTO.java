package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 获取练习信息
 * </p>
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeViewDTO ", description="获取练习信息")
public class AuthPracticeViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "难度id")
    private Long difficultyId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "试题数量")
    private Integer problemQuantity;

    @ApiModelProperty(value = "题类id")
    private Long topicId;

    @ApiModelProperty(value = "年级id")
    private Long graId;

    @ApiModelProperty(value = "考点id")
    private Long emphasisId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "练习试题信息集合")
    private List<AuthPracticeProblemViewDTO> problemList;
}
