package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@ApiModel(value = "用户试卷答案查看", description = "用户试卷答案查看")
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "标题集合")
    private List<AuthUserExamAnswerTitleDTO> titleList;
}
