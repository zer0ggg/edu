package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.GradeApplyAuditStatusEnum;
import com.roncoo.education.common.core.enums.GradeViewStatusEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.exam.service.dao.GradeApplyRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeApplyRecordMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeApplyRecordDaoImpl extends AbstractBaseJdbc implements GradeApplyRecordDao {

    @Autowired
    private GradeApplyRecordMapper mapper;

    @Override
    public int save(GradeApplyRecord record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeApplyRecord record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeApplyRecord getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeApplyRecord> listForPage(int pageCurrent, int pageSize, GradeApplyRecordExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public GradeApplyRecord getByIdForUpdate(Long id) {
        return this.queryForObject("select * from grade_apply_record where id = ? for update ", GradeApplyRecord.class, id);
    }

    @Override
    public int countByGradeIdAndAudit(Long gradeId, Integer auditStatus) {
        GradeApplyRecordExample example = new GradeApplyRecordExample();
        GradeApplyRecordExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        if (null != auditStatus) {
            c.andAuditStatusEqualTo(auditStatus);
        }
        return this.mapper.countByExample(example);
    }

    @Override
    public List<GradeApplyRecord> limitTipsListInLecturerUserNo(List<Long> lecturerUserNoList, Integer num) {
        GradeApplyRecordExample example = new GradeApplyRecordExample();
        GradeApplyRecordExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoIn(lecturerUserNoList);
        c.andViewStatusEqualTo(GradeViewStatusEnum.NO.getCode());
        c.andAuditStatusEqualTo(GradeApplyAuditStatusEnum.WAIT.getCode());
        example.setOrderByClause("id desc");
        example.setLimitStart(0);
        example.setPageSize(num);
        return this.mapper.selectByExample(example);
    }

}
