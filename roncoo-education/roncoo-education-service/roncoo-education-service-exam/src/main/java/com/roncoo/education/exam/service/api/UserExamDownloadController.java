package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.UserExamDownloadBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户试卷下载管理
 *
 * @author LYQ
 */
@Api(tags = "API-用户试卷下载管理")
@RestController
@RequestMapping("/exam/api/user/exam/download")
public class UserExamDownloadController {

    @Autowired
    private UserExamDownloadBiz biz;

    @GetMapping(value = "/test/{examId}")
    public void test(@PathVariable(name = "examId") Long examId, HttpServletRequest request, HttpServletResponse response) {
        biz.export(examId, request, response);
    }
}
