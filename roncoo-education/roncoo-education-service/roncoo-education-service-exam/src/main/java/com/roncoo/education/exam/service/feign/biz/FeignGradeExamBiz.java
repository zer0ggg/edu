package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.feign.qo.GradeExamQO;
import com.roncoo.education.exam.feign.vo.GradeExamVO;
import com.roncoo.education.exam.service.dao.GradeExamDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeExamBiz extends BaseBiz {

    @Autowired
    private GradeExamDao dao;

	public Page<GradeExamVO> listForPage(GradeExamQO qo) {
	    GradeExamExample example = new GradeExamExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeExam> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeExamVO.class);
	}

	public int save(GradeExamQO qo) {
		GradeExam record = BeanUtil.copyProperties(qo, GradeExam.class);
		record.setId(IdWorker.getId());
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeExamVO getById(Long id) {
		GradeExam record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeExamVO.class);
	}

	public int updateById(GradeExamQO qo) {
		GradeExam record = BeanUtil.copyProperties(qo, GradeExam.class);
		return dao.updateById(record);
	}

}
