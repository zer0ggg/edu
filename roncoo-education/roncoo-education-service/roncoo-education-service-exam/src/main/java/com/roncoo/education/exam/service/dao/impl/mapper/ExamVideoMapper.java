package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamVideoMapper {
    int countByExample(ExamVideoExample example);

    int deleteByExample(ExamVideoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamVideo record);

    int insertSelective(ExamVideo record);

    List<ExamVideo> selectByExample(ExamVideoExample example);

    ExamVideo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamVideo record, @Param("example") ExamVideoExample example);

    int updateByExample(@Param("record") ExamVideo record, @Param("example") ExamVideoExample example);

    int updateByPrimaryKeySelective(ExamVideo record);

    int updateByPrimaryKey(ExamVideo record);
}