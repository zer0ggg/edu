package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.FileInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.FileInfoMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 文件信息 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class FileInfoDaoImpl implements FileInfoDao {

    @Autowired
    private FileInfoMapper mapper;

    @Override
    public int save(FileInfo record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(FileInfo record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public FileInfo getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<FileInfo> listForPage(int pageCurrent, int pageSize, FileInfoExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


}
