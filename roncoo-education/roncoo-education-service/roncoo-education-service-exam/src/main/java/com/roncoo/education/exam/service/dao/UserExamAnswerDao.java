package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample;

import java.util.List;

/**
 * 用户考试答案 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamAnswerDao {

    /**
     * 保存用户考试答案
     *
     * @param record 用户考试答案
     * @return 影响记录数
     */
    int save(UserExamAnswer record);

    /**
     * 根据ID删除用户考试答案
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 用户考试答案
     * @return 影响记录数
     */
    int updateById(UserExamAnswer record);

    /**
     * 根据ID获取用户考试答案
     *
     * @param id 主键ID
     * @return 用户考试答案
     */
    UserExamAnswer getById(Long id);

    /**
     * 用户考试答案--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<UserExamAnswer> listForPage(int pageCurrent, int pageSize, UserExamAnswerExample example);

    /**
     * 根据记录ID、标题ID和问题ID获取用户考试记录
     *
     * @param recordId  记录ID
     * @param titleId   标题ID
     * @param problemId 问题ID
     * @return 考试答案
     */
    UserExamAnswer getByRecordIdAndTitleIdAndProblemId(Long recordId, Long titleId, Long problemId);

    /**
     * 根据记录ID获取用户考试答案
     *
     * @param recordId 记录ID
     * @return 考试答案
     */
    List<UserExamAnswer> listByRecordId(Long recordId);

    /**
     * 根据记录ID、标题ID和问题ID列出用户考试记录
     *
     * @param recordId 记录ID
     * @param titleId  标题ID
     * @return 考试答案
     */
    List<UserExamAnswer> listByRecordIdAndTitleId(Long recordId, Long titleId);

    /**
     * 根据试题ID、试卷ID和用户编号列出用户考试记录
     *
     * @param problemId
     * @param examId
     * @param userNo
     * @return
     */
    List<UserExamAnswer> getProblemIdAndExamIdAndUserNo(Long problemId, Long examId, Long userNo);

    /**
     * 根据试题ID、试卷ID列出用户考试记录
     *
     * @param problemId
     * @param examId
     * @return
     */
    List<UserExamAnswer> getProblemIdAndExamId(Long problemId, Long examId);
}
