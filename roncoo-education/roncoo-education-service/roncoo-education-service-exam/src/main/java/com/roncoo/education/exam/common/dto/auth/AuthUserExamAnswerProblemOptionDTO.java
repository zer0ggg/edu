package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户试卷答案小题选项
 *
 * @author LYQ
 */
@ApiModel(value = "用户试卷答案小题选项", description = "用户试卷答案小题选项")
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerProblemOptionDTO implements Serializable {

    private static final long serialVersionUID = -1335834757706736983L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;

    @ApiModelProperty(value = "选项内容")
    private String optionContent;
}
