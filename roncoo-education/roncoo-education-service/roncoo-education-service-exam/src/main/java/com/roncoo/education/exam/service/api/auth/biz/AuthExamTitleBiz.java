package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamTitleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题
 *
 * @author wujing
 */
@Component
public class AuthExamTitleBiz extends BaseBiz {

    @Autowired
    private ExamTitleDao dao;

}
