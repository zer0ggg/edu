package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourseAudit;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterAudit;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriodAudit;
import com.roncoo.education.course.feign.vo.CourseAuditVO;
import com.roncoo.education.course.feign.vo.CourseChapterAuditVO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodAuditVO;
import com.roncoo.education.exam.common.req.CourseExamRefEditREQ;
import com.roncoo.education.exam.common.req.CourseExamRefListREQ;
import com.roncoo.education.exam.common.req.CourseExamRefSaveREQ;
import com.roncoo.education.exam.common.resp.CourseExamRefListRESP;
import com.roncoo.education.exam.common.resp.CourseExamRefViewRESP;
import com.roncoo.education.exam.service.dao.CourseExamRefDao;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample.Criteria;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程试卷关联
 *
 * @author wujing
 */
@Component
public class PcCourseExamRefBiz extends BaseBiz {

    @Autowired
    private CourseExamRefDao dao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;

    @Autowired
    private IFeignCourseAudit feignCourseAudit;
    @Autowired
    private IFeignCourseChapterAudit feignCourseChapterAudit;
    @Autowired
    private IFeignCourseChapterPeriodAudit feignCourseChapterPeriodAudit;

    /**
     * 课程试卷关联列表
     *
     * @param req 课程试卷关联分页查询参数
     * @return 课程试卷关联分页查询结果
     */
    public Result<Page<CourseExamRefListRESP>> list(CourseExamRefListREQ req) {
        CourseExamRefExample example = new CourseExamRefExample();
        Criteria c = example.createCriteria();
        if (req.getRefType() != null) {
            c.andRefTypeEqualTo(req.getRefType());
        }
        if (req.getCourseCategory() != null) {
            c.andCourseCategoryEqualTo(req.getCourseCategory());
        }
        Page<CourseExamRef> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseExamRefListRESP> respPage = PageUtil.transform(page, CourseExamRefListRESP.class);
        for (CourseExamRefListRESP resp : respPage.getList()) {
            ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(resp.getExamId());
            if (ObjectUtil.isNotNull(examInfoAudit)) {
                resp.setExamName(examInfoAudit.getExamName());
            }
            if (RefTypeEnum.COURSE.getCode().equals(resp.getRefType())) {
                CourseAuditVO courseAuditVO = feignCourseAudit.getById(resp.getRefId());
                if (ObjectUtil.isNotNull(courseAuditVO)) {
                    resp.setRefName(courseAuditVO.getCourseName());
                }
            }
            if (RefTypeEnum.CHAPTER.getCode().equals(resp.getRefType())) {
                CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(resp.getRefId());
                if (ObjectUtil.isNotNull(courseChapterAuditVO)) {
                    resp.setRefName(courseChapterAuditVO.getChapterName());
                }
                CourseAuditVO courseAuditVO = feignCourseAudit.getById(courseChapterAuditVO.getCourseId());
                if (ObjectUtil.isNotNull(courseAuditVO)) {
                    resp.setCourseName(courseAuditVO.getCourseName());
                }
            }
            if (RefTypeEnum.PERIOD.getCode().equals(resp.getRefType())) {
                CourseChapterPeriodAuditVO courseChapterPeriodAuditVO = feignCourseChapterPeriodAudit.getById(resp.getRefId());
                if (ObjectUtil.isNotNull(courseChapterPeriodAuditVO)) {
                    resp.setRefName(courseChapterPeriodAuditVO.getPeriodName());
                }
                CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(courseChapterPeriodAuditVO.getChapterId());
                if (ObjectUtil.isNotNull(courseChapterAuditVO)) {
                    resp.setChapterName(courseChapterAuditVO.getChapterName());
                }
                CourseAuditVO courseAuditVO = feignCourseAudit.getById(courseChapterPeriodAuditVO.getCourseId());
                if (ObjectUtil.isNotNull(courseAuditVO)) {
                    resp.setCourseName(courseAuditVO.getCourseName());
                }
            }
        }
        return Result.success(respPage);
    }


    /**
     * 课程试卷关联添加
     *
     * @param req 课程试卷关联
     * @return 添加结果
     */
    public Result<String> save(CourseExamRefSaveREQ req) {
        CourseExamRef courseExamRef = dao.getByRefIdAndExamId(req.getRefId(), req.getExamId());
        if (ObjectUtil.isNotNull(courseExamRef)) {
            return Result.error("已绑定试卷");
        }
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(req.getExamId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("找不到试卷信息");
        }
        if (RefTypeEnum.COURSE.getCode().equals(req.getRefType())) {
            CourseAuditVO courseAuditVO = feignCourseAudit.getById(req.getRefId());
            if (ObjectUtil.isNull(courseAuditVO)) {
                return Result.error("找不到课程信息");
            }
        }
        if (RefTypeEnum.CHAPTER.getCode().equals(req.getRefType())) {
            CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(req.getRefId());
            if (ObjectUtil.isNull(courseChapterAuditVO)) {
                return Result.error("找不到章节信息");
            }
        }
        if (RefTypeEnum.PERIOD.getCode().equals(req.getRefType())) {
            CourseChapterPeriodAuditVO courseChapterPeriodAuditVO = feignCourseChapterPeriodAudit.getById(req.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriodAuditVO)) {
                return Result.error("找不到课时信息");
            }
        }
        CourseExamRef record = BeanUtil.copyProperties(req, CourseExamRef.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 课程试卷关联查看
     *
     * @param id 主键ID
     * @return 课程试卷关联
     */
    public Result<CourseExamRefViewRESP> view(Long id) {
        CourseExamRef courseExamRef = dao.getById(id);
        if (ObjectUtil.isNull(courseExamRef)) {
            return Result.error("找不到课程试卷关联信息");
        }
        CourseExamRefViewRESP resp = BeanUtil.copyProperties(courseExamRef, CourseExamRefViewRESP.class);
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(resp.getExamId());
        if (ObjectUtil.isNotNull(examInfoAudit)) {
            resp.setExamName(examInfoAudit.getExamName());
        }
        if (RefTypeEnum.COURSE.getCode().equals(resp.getRefType())) {
            CourseAuditVO courseAuditVO = feignCourseAudit.getById(resp.getRefId());
            if (ObjectUtil.isNotNull(courseAuditVO)) {
                resp.setRefName(courseAuditVO.getCourseName());
            }
        }
        if (RefTypeEnum.CHAPTER.getCode().equals(resp.getRefType())) {
            CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(resp.getRefId());
            if (ObjectUtil.isNotNull(courseChapterAuditVO)) {
                resp.setRefName(courseChapterAuditVO.getChapterName());
            }
        }
        if (RefTypeEnum.PERIOD.getCode().equals(resp.getRefType())) {
            CourseChapterPeriodAuditVO courseChapterPeriodAuditVO = feignCourseChapterPeriodAudit.getById(resp.getRefId());
            if (ObjectUtil.isNotNull(courseChapterPeriodAuditVO)) {
                resp.setRefName(courseChapterPeriodAuditVO.getPeriodName());
            }
        }
        return Result.success(resp);
    }


    /**
     * 课程试卷关联修改
     *
     * @param courseExamRefEditREQ 课程试卷关联修改对象
     * @return 修改结果
     */
    public Result<String> edit(CourseExamRefEditREQ courseExamRefEditREQ) {
        CourseExamRef record = BeanUtil.copyProperties(courseExamRefEditREQ, CourseExamRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 课程试卷关联删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
