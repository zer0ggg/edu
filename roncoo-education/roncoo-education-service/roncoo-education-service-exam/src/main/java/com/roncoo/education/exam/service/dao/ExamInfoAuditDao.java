package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAuditExample;

/**
 * 试卷信息审核 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamInfoAuditDao {

    /**
     * 保存试卷信息审核
     *
     * @param record 试卷信息审核
     * @return 影响记录数
     */
    int save(ExamInfoAudit record);

    /**
     * 根据ID删除试卷信息审核
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 试卷信息审核
     * @return 影响记录数
     */
    int updateById(ExamInfoAudit record);

    /**
     * 根据ID获取试卷信息审核
     *
     * @param id 主键ID
     * @return 试卷信息审核
     */
    ExamInfoAudit getById(Long id);

    /**
     * 试卷信息审核--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ExamInfoAudit> listForPage(int pageCurrent, int pageSize, ExamInfoAuditExample example);

    /**
     * 根据ID和讲师用户比那好获取试卷信息审核
     *
     * @param id             试卷ID
     * @param lecturerUserNo 讲师用户编号
     * @return 试卷信息审核
     */
    ExamInfoAudit getByIdAndLecturerUserNo(Long id, Long lecturerUserNo);
}
