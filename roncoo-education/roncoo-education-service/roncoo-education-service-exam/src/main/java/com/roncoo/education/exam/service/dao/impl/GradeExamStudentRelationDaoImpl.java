package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ExamStatusEnum;
import com.roncoo.education.common.core.enums.GradeViewStatusEnum;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.exam.service.dao.GradeExamStudentRelationDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeExamStudentRelationMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelation;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeExamStudentRelationDaoImpl extends AbstractBaseJdbc implements GradeExamStudentRelationDao {

    @Autowired
    private GradeExamStudentRelationMapper mapper;

    @Override
    public int save(GradeExamStudentRelation record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeExamStudentRelation record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeExamStudentRelation getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeExamStudentRelation> listForPage(int pageCurrent, int pageSize, GradeExamStudentRelationExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int deleteByGradeId(Long gradeId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByGradeExamId(Long gradeExamId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByGradeIdAndStudentId(Long gradeId, Long studentId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        c.andStudentIdEqualTo(studentId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public GradeExamStudentRelation getByIdForUpdate(Long id) {
        return queryForObject("select * from grade_exam_student_relation where id = ? for update ", GradeExamStudentRelation.class, id);
    }

    @Override
    public int countByGradeExamId(Long gradeExamId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        return this.mapper.countByExample(example);
    }

    @Override
    public Integer countByGradeIdAndUserNoAndExamStatus(Long id, Long userNo, Integer examStatus) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(id);
        c.andUserNoEqualTo(userNo);
        c.andExamStatusEqualTo(examStatus);
        return this.mapper.countByExample(example);
    }

    @Override
    public List<GradeExamStudentRelation> limitTipsListByUserNo(Long userNo, Integer num) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();

        // 查询条件
        c.andUserNoEqualTo(userNo);
        c.andViewStatusEqualTo(GradeViewStatusEnum.NO.getCode());
        c.andEndExamTimeLessThan(new Date());

        // 排序
        example.setOrderByClause("id desc");

        // 获取记录数
        example.setLimitStart(0);
        example.setPageSize(num);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeExamStudentRelation> listCompleteByGradeExamId(Long gradeExamId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        c.andExamStatusEqualTo(ExamStatusEnum.COMPLETE.getCode());
        example.setOrderByClause("exam_status desc, audit_status asc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeExamStudentRelation> listByGradeExamId(Long gradeExamId) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        example.setOrderByClause("exam_status desc, audit_status asc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int countByGradeExamIdAndAuditStatus(Long gradeExamId, Integer auditStatus) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        c.andAuditStatusEqualTo(auditStatus);
        return this.mapper.countByExample(example);
    }

    @Override
    public GradeExamStudentRelation getByUserNoAndGradeIdAndExamIdAndBeginExamTimeAndExamTime(Long userNo, Long gradeId, Long examId, Date beginTime, Date endTime) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andGradeIdEqualTo(gradeId);
        c.andExamIdEqualTo(examId);
        c.andBeginExamTimeGreaterThanOrEqualTo(beginTime);
        c.andEndExamTimeLessThanOrEqualTo(DateUtil.addDate(endTime, 1));
        List<GradeExamStudentRelation> resultList = mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public int countByGradeExamIdAndExamStatus(Long gradeExamId, Integer examStatus) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andGradeExamIdEqualTo(gradeExamId);
        if (examStatus != null) {
            c.andExamStatusEqualTo(examStatus);
        }
        return this.mapper.countByExample(example);
    }


}
