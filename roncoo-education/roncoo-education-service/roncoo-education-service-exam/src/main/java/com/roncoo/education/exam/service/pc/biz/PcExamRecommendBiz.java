package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.ExamRecommendListRESP;
import com.roncoo.education.exam.common.resp.ExamRecommendViewRESP;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.ExamRecommendDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

/**
 * 试卷推荐
 *
 * @author wujing
 */
@Component
public class PcExamRecommendBiz extends BaseBiz {

	@Autowired
	private ExamRecommendDao dao;
	@Autowired
	private ExamInfoDao examInfoDao;

	/**
	 * 试卷推荐列表
	 *
	 * @param req 试卷推荐分页查询参数
	 * @return 试卷推荐分页查询结果
	 */
	public Result<Page<ExamRecommendListRESP>> list(ExamRecommendListREQ req) {
		ExamRecommendExample example = new ExamRecommendExample();
		Criteria c = example.createCriteria();
		if (req.getStatusId() != null) {
			c.andStatusIdEqualTo(req.getStatusId());
		}
		if (!StringUtils.isEmpty(req.getExamName())) {
			c.andExamNameLike(PageUtil.like(req.getExamName()));
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc  ");
		Page<ExamRecommend> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
		Page<ExamRecommendListRESP> respPage = PageUtil.transform(page, ExamRecommendListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷推荐查看
	 *
	 * @param id 主键ID
	 * @return 试卷推荐
	 */
	public Result<ExamRecommendViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamRecommendViewRESP.class));
	}

	/**
	 * 试卷推荐修改
	 *
	 * @param req 试卷推荐修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamRecommendEditREQ req) {
		ExamRecommend record = BeanUtil.copyProperties(req, ExamRecommend.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷推荐删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}

	public Result<String> updateStatus(ExamRecommendUpdateStatusREQ req) {
		// 检验必传参数
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
			return Result.error("状态不正确");
		}
		ExamRecommend record = BeanUtil.copyProperties(req, ExamRecommend.class);
		if (dao.updateById(record) > 0) {
			return Result.success("操作成功");
		}
		return Result.error("操作失败");

	}

	/**
	 * 试卷推荐添加
	 *
	 * @param req 试卷推荐
	 * @return 添加结果
	 */
	public Result<String> save(ExamRecommendSaveREQ req) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		ExamInfo examInfo = examInfoDao.getById(req.getExamId());
		if (ObjectUtils.isEmpty(examInfo)) {
			return Result.error("试卷id不正确");
		}
		ExamRecommend record = dao.getByExamId(req.getExamId());
		if (!ObjectUtils.isEmpty(record)) {// 已添加
			return Result.success("添加成功");
		}

		record = BeanUtil.copyProperties(req, ExamRecommend.class);
		record.setExamName(examInfo.getExamName());
		record.setStatusId(StatusIdEnum.YES.getCode());
		record.setSort(Integer.valueOf(1));
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	public Result<String> saveBatch(ExamRecommendSaveBatchREQ req) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		String[] examIds = req.getExamIds().split(",");
		try {
			for (String examIdStr : examIds) {
				Long examId = Long.valueOf(examIdStr);
				ExamInfo exam = examInfoDao.getById(examId);
				if (ObjectUtils.isEmpty(exam)) {// 试卷不存在，跳过
					continue;
				}
				ExamRecommend record = dao.getByExamId(examId);
				if (!ObjectUtils.isEmpty(record)) {// 已添加，跳过
					continue;
				}
				ExamRecommend bean = new ExamRecommend();
				bean.setExamName(exam.getExamName());
				bean.setExamId(examId);
				bean.setStatusId(StatusIdEnum.YES.getCode());
				bean.setSort(Integer.valueOf(1));
				dao.save(bean);
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}

		return Result.success("添加成功");
	}
}
