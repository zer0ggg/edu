package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级考试布置对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeExamEditBeforeBO", description = "班级考试布置对象  修改")
public class AuthGradeExamEditBeforeBO implements Serializable {

    private static final long serialVersionUID = -7753019257932890611L;

    @NotNull(message = "班级试卷id不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;
}
