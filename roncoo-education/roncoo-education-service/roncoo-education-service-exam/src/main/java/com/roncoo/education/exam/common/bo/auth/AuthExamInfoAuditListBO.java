package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷信息审核列表
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditListBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)")
    private Integer auditStatus;

    @ApiModelProperty(value = "是否公开(1:公开，0:不公开-绑定课程)")
    private Integer isPublic;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数", required = true)
    private Integer pageSize = 20;

}
