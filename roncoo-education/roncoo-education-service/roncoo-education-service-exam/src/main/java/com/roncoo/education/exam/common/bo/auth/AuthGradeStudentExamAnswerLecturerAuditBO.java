package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 学生考试  评阅
 *
 * @author zk
 */
@Data  // grade_student_exam_answer
@ApiModel(value = "AuthGradeStudentExamAnswerLecturerAuditBO", description = "学生答案  讲师评阅")
public class AuthGradeStudentExamAnswerLecturerAuditBO implements Serializable {

    private static final long serialVersionUID = -2130352843151324312L;

    @NotNull(message = "学员答案ID不能为空")
    @ApiModelProperty(value = "学员答案ID", required = true)
    private Long id;

    @NotNull(message = "得分不能为空")
    @ApiModelProperty(value = "得分", required = true)
    private Integer score;

    @ApiModelProperty(value = "备注")
    private String remark;
}
