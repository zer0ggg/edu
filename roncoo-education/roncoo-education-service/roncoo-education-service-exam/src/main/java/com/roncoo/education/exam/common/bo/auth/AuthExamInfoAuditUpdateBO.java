package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 试卷信息审核列表
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditUpdateBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id", required = true)
    private Long id;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;

    @NotNull(message = "是否免费不能为空")
    @ApiModelProperty(value = "是否免费：1免费，0收费'", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "优惠价格")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "答卷时长(单位：分钟)")
    private Integer answerTime;

    @ApiModelProperty(value = "讲师试卷排序")
    private Integer examSort;

    @ApiModelProperty(value = "是否公开(1:公开，0:不公开-绑定课程)", required = true)
    private Integer isPublic;

}
