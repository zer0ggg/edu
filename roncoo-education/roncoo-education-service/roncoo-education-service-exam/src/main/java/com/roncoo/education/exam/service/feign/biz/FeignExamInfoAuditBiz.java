package com.roncoo.education.exam.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.ProblemTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditLecturerViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditProblemViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditQO;
import com.roncoo.education.exam.feign.vo.*;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 试卷信息审核
 *
 * @author wujing
 */
@Component
public class FeignExamInfoAuditBiz extends BaseBiz {

    @Autowired
    private ExamInfoAuditDao dao;
    @Autowired
    private ExamTitleAuditDao examTitleAuditDao;
    @Autowired
    private ExamTitleProblemRefAuditDao examTitleProblemRefAuditDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    public Page<ExamInfoAuditVO> listForPage(ExamInfoAuditQO qo) {
        ExamInfoAuditExample example = new ExamInfoAuditExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(qo.getLecturerUserNo())) {
            c.andLecturerUserNoEqualTo(qo.getLecturerUserNo());
        }
        if (StrUtil.isNotBlank(qo.getExamName())) {
            c.andExamNameLike(PageUtil.like(qo.getExamName()));
        }
        if (ObjectUtil.isNotNull(qo.getGraId())) {
            c.andGraIdEqualTo(qo.getGraId());
        }
        if (ObjectUtil.isNotNull(qo.getSubjectId())) {
            c.andSubjectIdEqualTo(qo.getSubjectId());
        }
        if (ObjectUtil.isNotNull(qo.getYearId())) {
            c.andYearIdEqualTo(qo.getYearId());
        }
        if (ObjectUtil.isNotNull(qo.getSourceId())) {
            c.andSourceIdEqualTo(qo.getSourceId());
        }
        if (ObjectUtil.isNotNull(qo.getIsFree())) {
            c.andIsFreeEqualTo(qo.getIsFree());
        }
        example.setOrderByClause("sort asc, id desc ");
        Page<ExamInfoAudit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, ExamInfoAuditVO.class);
    }

    public int save(ExamInfoAuditQO qo) {
        ExamInfoAudit record = BeanUtil.copyProperties(qo, ExamInfoAudit.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public ExamInfoAuditVO getById(Long id) {
        ExamInfoAudit record = dao.getById(id);
        return BeanUtil.copyProperties(record, ExamInfoAuditVO.class);
    }

    public int updateById(ExamInfoAuditQO qo) {
        ExamInfoAudit record = BeanUtil.copyProperties(qo, ExamInfoAudit.class);
        return dao.updateById(record);
    }

    public ExamInfoAuditVO getByIdAndLecturerUserNo(ExamInfoAuditLecturerViewQO qo) {
        return BeanUtil.copyProperties(dao.getByIdAndLecturerUserNo(qo.getId(), qo.getLecturerUserNo()), ExamInfoAuditVO.class);
    }

    /**
     * 获取试卷试题
     *
     * @param qo 获取试卷试题参数
     * @return 试卷试题
     */
    public ExamInfoAuditProblemViewVO getExamProblem(ExamInfoAuditProblemViewQO qo) {
        ExamInfoAudit examInfoAudit = dao.getById(qo.getExamId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            throw new BaseException("试卷不存在");
        }
        ExamTitleAudit examTitleAudit = examTitleAuditDao.getById(qo.getTitleId());
        if (ObjectUtil.isNull(examTitleAudit)) {
            throw new BaseException("试卷标题不存在");
        }
        if (examTitleAudit.getExamId().equals(examInfoAudit.getId())) {
            throw new BaseException("试卷不存在该标题");
        }

        // 获取标题题目
        ExamTitleProblemRefAudit examTitleProblemRefAudit = examTitleProblemRefAuditDao.getByTitleIdAndProblemId(examTitleAudit.getId(), qo.getProblemId());
        if (ObjectUtil.isNull(examTitleProblemRefAudit)) {
            throw new BaseException("试卷题目不存在");
        }
        ExamProblem examProblem = examProblemDao.getById(qo.getProblemId());
        if (ObjectUtil.isNull(examProblem)) {
            throw new BaseException("试题不存在");
        }

        ExamInfoAuditProblemViewVO viewVO = BeanUtil.copyProperties(examProblem, ExamInfoAuditProblemViewVO.class);
        viewVO.setScore(examTitleProblemRefAudit.getScore());
        return viewVO;
    }

    /**
     * 根据ID获取试卷详细信息
     *
     * @param id 试卷ID
     * @return 试卷审核信息
     */
    public ExamInfoAuditViewVO getExamInfoById(Long id) {
        // 获取试卷
        ExamInfoAudit examInfoAudit = dao.getById(id);
        if (ObjectUtil.isNull(examInfoAudit)) {
            return null;
        }
        ExamInfoAuditViewVO examInfoAuditViewVO = BeanUtil.copyProperties(examInfoAudit, ExamInfoAuditViewVO.class);

        // 获取标题
        List<ExamTitleAudit> examTitleAuditList = examTitleAuditDao.listByExamId(examInfoAudit.getId());
        if (CollectionUtil.isEmpty(examTitleAuditList)) {
            return examInfoAuditViewVO;
        }
        List<ExamInfoAuditTitleViewVO> titleList = BeanUtil.copyProperties(examTitleAuditList, ExamInfoAuditTitleViewVO.class);

        //获取标题题目
        for (ExamInfoAuditTitleViewVO titleViewVO : titleList) {
            List<ExamTitleProblemRefAudit> refList = examTitleProblemRefAuditDao.listByTitleId(titleViewVO.getId());
            if (CollectionUtil.isEmpty(refList)) {
                continue;
            }
            List<Long> problemIdList = refList.stream().map(ExamTitleProblemRefAudit::getProblemId).collect(Collectors.toList());
            List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
            if (CollectionUtil.isEmpty(problemList)) {
                continue;
            }
            Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, examProblem -> examProblem));

            List<ExamInfoAuditTitleProblemViewVO> titleProblemList = new ArrayList<>();
            for (ExamTitleProblemRefAudit ref : refList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }

                ExamInfoAuditTitleProblemViewVO problemViewVO = BeanUtil.copyProperties(problem, ExamInfoAuditTitleProblemViewVO.class);
                problemViewVO.setScore(ref.getScore());

                if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
                    //获取选项
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemId(problem.getId());
                    problemViewVO.setOptionList(BeanUtil.copyProperties(optionList, ExamInfoAuditTitleProblemOptionViewVO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(problem.getProblemType())) {
                    // 获取填空
                    problemViewVO.setOptionCount(StrUtil.isBlank(problem.getProblemAnswer()) ? 0 : problem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                }
                titleProblemList.add(problemViewVO);
            }
            titleViewVO.setProblemList(titleProblemList);
        }

        examInfoAuditViewVO.setTitleList(titleList);
        return examInfoAuditViewVO;
    }
}
