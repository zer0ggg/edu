package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamQO;
import com.roncoo.education.exam.feign.vo.UserExamVO;
import com.roncoo.education.exam.service.dao.UserExamDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷
 *
 * @author wujing
 */
@Component
public class FeignUserExamBiz extends BaseBiz {

    @Autowired
    private UserExamDao dao;

	public Page<UserExamVO> listForPage(UserExamQO qo) {
	    UserExamExample example = new UserExamExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExam> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamVO.class);
	}

	public int save(UserExamQO qo) {
		UserExam record = BeanUtil.copyProperties(qo, UserExam.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamVO getById(Long id) {
		UserExam record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamVO.class);
	}

	public int updateById(UserExamQO qo) {
		UserExam record = BeanUtil.copyProperties(qo, UserExam.class);
		return dao.updateById(record);
	}

}
