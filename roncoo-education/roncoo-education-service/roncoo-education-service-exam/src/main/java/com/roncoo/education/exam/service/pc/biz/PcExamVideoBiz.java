package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.common.req.ExamVideoEditREQ;
import com.roncoo.education.exam.common.req.ExamVideoListREQ;
import com.roncoo.education.exam.common.req.ExamVideoSaveREQ;
import com.roncoo.education.exam.common.resp.ExamVideoListRESP;
import com.roncoo.education.exam.common.resp.ExamVideoViewRESP;
import com.roncoo.education.exam.service.dao.ExamVideoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷视频
 *
 * @author wujing
 */
@Component
public class PcExamVideoBiz extends BaseBiz {

	@Autowired
	private ExamVideoDao dao;

	/**
	 * 试卷视频列表
	 *
	 * @param examVideoListREQ 试卷视频分页查询参数
	 * @return 试卷视频分页查询结果
	 */
	public Result<Page<ExamVideoListRESP>> list(ExamVideoListREQ examVideoListREQ) {
		ExamVideoExample example = new ExamVideoExample();
		Criteria c = example.createCriteria();
		Page<ExamVideo> page = dao.listForPage(examVideoListREQ.getPageCurrent(), examVideoListREQ.getPageSize(), example);
		Page<ExamVideoListRESP> respPage = PageUtil.transform(page, ExamVideoListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷视频添加
	 *
	 * @param examVideoSaveREQ 试卷视频
	 * @return 添加结果
	 */
	public Result<String> save(ExamVideoSaveREQ examVideoSaveREQ) {
		ExamVideo record = BeanUtil.copyProperties(examVideoSaveREQ, ExamVideo.class);
		record.setId(IdWorker.getId());
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷视频查看
	 *
	 * @param id 主键ID
	 * @return 试卷视频
	 */
	public Result<ExamVideoViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamVideoViewRESP.class));
	}

	/**
	 * 试卷视频修改
	 *
	 * @param examVideoEditREQ 试卷视频修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamVideoEditREQ examVideoEditREQ) {
		ExamVideo record = BeanUtil.copyProperties(examVideoEditREQ, ExamVideo.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷视频删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
