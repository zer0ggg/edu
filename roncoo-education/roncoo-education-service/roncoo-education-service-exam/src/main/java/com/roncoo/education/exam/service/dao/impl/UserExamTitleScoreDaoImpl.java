package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamTitleScoreDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamTitleScoreMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户考试标题得分 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamTitleScoreDaoImpl implements UserExamTitleScoreDao {

    @Autowired
    private UserExamTitleScoreMapper mapper;

    @Override
    public int save(UserExamTitleScore record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExamTitleScore record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExamTitleScore getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserExamTitleScore> listForPage(int pageCurrent, int pageSize, UserExamTitleScoreExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<UserExamTitleScore> listByRecordId(Long recordId) {
        UserExamTitleScoreExample example = new UserExamTitleScoreExample();
        UserExamTitleScoreExample.Criteria c = example.createCriteria();
        c.andRecordIdEqualTo(recordId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public UserExamTitleScore getByRecordIdAndExamId(Long recordId, Long examId) {
        UserExamTitleScoreExample example = new UserExamTitleScoreExample();
        UserExamTitleScoreExample.Criteria c = example.createCriteria();
        c.andRecordIdEqualTo(recordId);
        List<UserExamTitleScore> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }


}
