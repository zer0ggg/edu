package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample;

import java.util.List;

/**
 * 试卷题目 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamProblemDao {

    /**
     * 保存试卷题目
     *
     * @param record 试卷题目
     * @return 影响记录数
     */
    int save(ExamProblem record);

    /**
     * 根据ID删除试卷题目
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 试卷题目
     * @return 影响记录数
     */
    int updateById(ExamProblem record);

    /**
     * 根据ID获取试卷题目
     *
     * @param id 主键ID
     * @return 试卷题目
     */
    ExamProblem getById(Long id);

    /**
     * 试卷题目--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ExamProblem> listForPage(int pageCurrent, int pageSize, ExamProblemExample example);

    /**
     * 根据ID集合获取题目(包含大字段)
     *
     * @param idList 题目ID集合
     * @return 题目
     */
    List<ExamProblem> listInIdList(List<Long> idList);

    /**
     * 根据父ID删除所有小题
     *
     * @param parentId 父类ID
     * @return 影响记录数
     */
    int deleteByParentId(Long parentId);

    /**
     * 根据父ID获取小题
     *
     * @param parentId 父类ID
     * @return 试卷题目
     */
    List<ExamProblem> listByParentId(Long parentId);

    /**
     * 根据父ID获取小题
     *
     * @param parentId 父类ID
     * @return 试卷题目
     */
    List<ExamProblem> listByParentIdWithBLOBs(Long parentId);

    /**
     * 根据视频id获取题目列表
     *
     * @param videoId 视频ID
     * @return 试卷题目
     */
    List<ExamProblem> listByVideoId(Long videoId);

    /**
     * 根据试题主键集合，批量修改用户分类id
     *
     * @param idList     ID集合
     * @param personalId 个人分类ID
     * @return 试卷题目
     */
    int updatePersonalIdByIdBatch(List<Long> idList, Long userNo, Long personalId);

    /**
     * 根据试题id和视频Vid获取信息
     *
     * @param id       试题ID
     * @param videoVid 视频VID
     * @return 试卷题目
     */
    ExamProblem getByIdAndVideoVid(Long id, String videoVid);

    ExamProblem viewWithBLOBs(Long id);

    /**
     * 根据vid更新
     * @param record
     * @return
     */
    int updateByVideoId(ExamProblem record);

    /**
     * 根据条件筛选符合的试题信息--练习随机使用
     * @param record
     * @return
     */
    List<ExamProblem> listByCondition(ExamProblem record);

    /**
     * 根据是否免费列出所有可用的试题
     * @param isFree
     * @return
     */
    List<ExamProblem> listByIsFree(Integer isFree);

    /**
     * 根据科目id、是否免费列出所有的单选题、多选题、判断题
     * @param subjectId
     * @param isFree
     * @return
     */
    List<ExamProblem> listBySubjectIdAndIsFreeAndProblemType(Long subjectId, Integer isFree);


    List<ExamProblem> ListByGraIdAndProblemCategory(Long problemCategoryId, Integer problemCategory);
}
