package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeInfoPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeInfoViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeStudentGradeInfoAdminPageDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级信息管理")
@RestController
@RequestMapping("/exam/auth/grade/info")
public class AuthGradeInfoController {

    @Autowired
    private AuthGradeInfoBiz biz;

    @ApiOperation(value = "分页列出--讲师创建的班级", notes = "分页列出--讲师创建的班级")
    @PostMapping(value = "/page")
    public Result<Page<AuthGradeInfoPageDTO>> listForPage(@RequestBody @Valid AuthGradeInfoPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "根据邀请码获得班级信息", notes = "根据邀请码获得班级信息")
    @PostMapping(value = "/view/invitation/code")
    public Result<AuthGradeInfoViewDTO> viewByInvitationCode(@RequestBody @Valid AuthGradeInfoViewByInvitationCodeBO bo) {
        return biz.viewByInvitationCode(bo);
    }

    @ApiOperation(value = "分页列出--根据学员类型展示所有班级", notes = "分页列出--根据学员类型展示所有班级")
    @PostMapping(value = "/grade/list/all")
    public Result<Page<AuthGradeInfoPageDTO>> listAllGradeForPage(@RequestBody @Valid AuthGradeStudentGradeInfoByRolePageBO bo) {
        return biz.listAllGradeForPage(bo);
    }


    @ApiOperation(value = "分页列出--查询班级成员管理的班级", notes = "分页列出--查询班级成员管理的班级")
    @PostMapping(value = "/grade/list/admin")
    public Result<Page<AuthGradeStudentGradeInfoAdminPageDTO>> listGradeInfoForPage(@RequestBody @Valid AuthGradeStudentGradeInfoPageBO bo) {
        return biz.listGradeInfoForPage(bo);
    }

    @ApiOperation(value = "查看详情", notes = "查看详情")
    @PostMapping(value = "/view")
    public Result<AuthGradeInfoViewDTO> view(@RequestBody @Valid AuthGradeInfoViewBO bo) {
        return biz.view(bo);
    }

    @ApiOperation(value = "创建班级", notes = "创建班级")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AuthGradeInfoSaveBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "解散班级", notes = "解散班级")
    @PostMapping(value = "/disband")
    public Result<String> disband(@RequestBody @Valid AuthGradeInfoDisbandBO bo) {
        return biz.disband(bo);
    }

    @ApiOperation(value = "班级信息修改", notes = "班级信息修改")
    @PutMapping(value = "/update")
    public Result<String> update(@RequestBody @Valid AuthGradeInfoUpdateBO bo) {
        return biz.update(bo);
    }

}
