package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;

import java.util.List;

/**
 * 用户试卷分类 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamCategoryDao {

    /**
    * 保存用户试卷分类
    *
    * @param record 用户试卷分类
    * @return 影响记录数
    */
    int save(UserExamCategory record);

    /**
    * 根据ID删除用户试卷分类
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户试卷分类
    * @return 影响记录数
    */
    int updateById(UserExamCategory record);

    /**
    * 根据ID获取用户试卷分类
    *
    * @param id 主键ID
    * @return 用户试卷分类
    */
    UserExamCategory getById(Long id);

    /**
    * 用户试卷分类--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<UserExamCategory> listForPage(int pageCurrent, int pageSize, UserExamCategoryExample example);

    /**
     * 根据父分类编号查找分类信息
     * @param parentId 父类ID
     * @return 用户试卷分类
     */
    List<UserExamCategory> listByParentId(Long parentId);
    /**
     * 根据id集合获取信息
     *
     * @param ids
     * @return
     */
    List<UserExamCategory> listByIds(List<Long> ids);
}
