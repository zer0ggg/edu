package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.exam.service.dao.GradeExamDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeExamMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeExamDaoImpl implements GradeExamDao {

    @Autowired
    private GradeExamMapper mapper;

    @Override
    public int save(GradeExam record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeExam record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeExam getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeExam> listForPage(int pageCurrent, int pageSize, GradeExamExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int deleteByGradeId(Long gradeId) {
        GradeExamExample example = new GradeExamExample();
        GradeExamExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int countByGradeId(Long gradeId) {
        GradeExamExample example = new GradeExamExample();
        GradeExamExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.countByExample(example);
    }

}
