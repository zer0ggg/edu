package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 试卷推荐分类
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ExamRecommendListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 试卷专区集合
	 */
	@ApiModelProperty(value = "试卷专区集合")
	private List<ExamRecommendDTO> recommendList = new ArrayList<>();

}
