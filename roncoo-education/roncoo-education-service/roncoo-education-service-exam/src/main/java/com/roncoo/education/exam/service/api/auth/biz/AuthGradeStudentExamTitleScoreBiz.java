package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.GradeStudentExamTitleScoreDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class AuthGradeStudentExamTitleScoreBiz extends BaseBiz {

    @Autowired
    private GradeStudentExamTitleScoreDao dao;

}
