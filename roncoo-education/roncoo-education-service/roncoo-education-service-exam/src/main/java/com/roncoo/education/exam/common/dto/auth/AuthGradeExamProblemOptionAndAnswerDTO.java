package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 班级考试题目选项带答案响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamProblemOptionAndAnswerDTO", description = "班级考试题目选项带答案响应对象")
public class AuthGradeExamProblemOptionAndAnswerDTO implements Serializable {

    private static final long serialVersionUID = -8893473152465551112L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "选项内容")
    private String optionContent;

}
