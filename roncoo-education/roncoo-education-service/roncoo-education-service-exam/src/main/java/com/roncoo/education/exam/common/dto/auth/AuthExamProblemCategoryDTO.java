package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户试卷分析
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */

@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamProblemCategoryDTO", description = "用户试卷分析")
public class AuthExamProblemCategoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类名称")
    private String name;

    @ApiModelProperty(value = "题数（错题数）")
    private Integer value = 0;


}
