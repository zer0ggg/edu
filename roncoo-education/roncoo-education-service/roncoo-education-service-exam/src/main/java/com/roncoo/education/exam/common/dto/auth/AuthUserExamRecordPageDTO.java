package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户考试记录分页结果
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthUserExamRecordPageDTO", description = "用户考试记录分页结果")
public class AuthUserExamRecordPageDTO implements Serializable {

    private static final long serialVersionUID = 7875245331832182360L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "开始时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "答卷时长")
    private Integer answerTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开考时间")
    private Date beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "交卷时间")
    private Date endTime;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "待评分")
    private Integer unCountScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "试卷状态(0:考试中、1:考试完成)")
    private Integer examStatus;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer studyCount;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionCount;

    @ApiModelProperty(value = "下载人数")
    private Integer downloadCount;

    @ApiModelProperty(value = "年级id")
    private Long graId;

    @ApiModelProperty(value = "年级名称")
    private String graName;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "年份名称")
    private String yearName;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "来源名称")
    private String sourceName;
}
