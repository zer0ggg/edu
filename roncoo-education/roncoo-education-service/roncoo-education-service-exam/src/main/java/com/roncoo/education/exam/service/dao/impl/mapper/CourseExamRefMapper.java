package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseExamRefMapper {
    int countByExample(CourseExamRefExample example);

    int deleteByExample(CourseExamRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseExamRef record);

    int insertSelective(CourseExamRef record);

    List<CourseExamRef> selectByExample(CourseExamRefExample example);

    CourseExamRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseExamRef record, @Param("example") CourseExamRefExample example);

    int updateByExample(@Param("record") CourseExamRef record, @Param("example") CourseExamRefExample example);

    int updateByPrimaryKeySelective(CourseExamRef record);

    int updateByPrimaryKey(CourseExamRef record);
}