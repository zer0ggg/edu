package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷信息审核列表：上下架
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditIsSubmitAuditBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @NotNull(message = "是否提交审核(1:提交，0:未提交)不能为空")
    @ApiModelProperty(value = "是否提交审核(1:提交，0:未提交)", required = true)
    private Integer submitAudit;

}
