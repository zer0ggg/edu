package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamProblemMapper {
    int countByExample(ExamProblemExample example);

    int deleteByExample(ExamProblemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamProblem record);

    int insertSelective(ExamProblem record);

    List<ExamProblem> selectByExample(ExamProblemExample example);

    ExamProblem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamProblem record, @Param("example") ExamProblemExample example);

    int updateByExample(@Param("record") ExamProblem record, @Param("example") ExamProblemExample example);

    int updateByPrimaryKeySelective(ExamProblem record);

    int updateByPrimaryKey(ExamProblem record);
}