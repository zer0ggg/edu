package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamMapper {
    int countByExample(UserExamExample example);

    int deleteByExample(UserExamExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExam record);

    int insertSelective(UserExam record);

    List<UserExam> selectByExample(UserExamExample example);

    UserExam selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExam record, @Param("example") UserExamExample example);

    int updateByExample(@Param("record") UserExam record, @Param("example") UserExamExample example);

    int updateByPrimaryKeySelective(UserExam record);

    int updateByPrimaryKey(UserExam record);
}