package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiExamProblemBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 试卷题目 Api接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
@RequestMapping("/exam/api/examProblem")
public class ApiExamProblemController {

    @Autowired
    private ApiExamProblemBiz biz;

}
