package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiExamProblemCollectionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 试卷题目收藏 Api接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
@RequestMapping("/exam/api/examProblemCollection")
public class ApiExamProblemCollectionController {

    @Autowired
    private ApiExamProblemCollectionBiz biz;

}
