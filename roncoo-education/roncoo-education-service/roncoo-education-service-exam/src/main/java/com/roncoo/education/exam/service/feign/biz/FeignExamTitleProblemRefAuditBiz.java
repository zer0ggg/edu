package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefAuditQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefAuditVO;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联审核
 *
 * @author wujing
 */
@Component
public class FeignExamTitleProblemRefAuditBiz extends BaseBiz {

    @Autowired
    private ExamTitleProblemRefAuditDao dao;

	public Page<ExamTitleProblemRefAuditVO> listForPage(ExamTitleProblemRefAuditQO qo) {
	    ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamTitleProblemRefAudit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamTitleProblemRefAuditVO.class);
	}

	public int save(ExamTitleProblemRefAuditQO qo) {
		ExamTitleProblemRefAudit record = BeanUtil.copyProperties(qo, ExamTitleProblemRefAudit.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamTitleProblemRefAuditVO getById(Long id) {
		ExamTitleProblemRefAudit record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamTitleProblemRefAuditVO.class);
	}

	public int updateById(ExamTitleProblemRefAuditQO qo) {
		ExamTitleProblemRefAudit record = BeanUtil.copyProperties(qo, ExamTitleProblemRefAudit.class);
		return dao.updateById(record);
	}

}
