package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 试卷信息审核列表
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditAddBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年级id")
    private Long graId;

    @NotNull(message = "科目id不能为空")
    @ApiModelProperty(value = "科目id", required = true)
    private Long subjectId;

    @NotNull(message = "年份id不能为空")
    @ApiModelProperty(value = "年份id", required = true)
    private Long yearId;

    @NotNull(message = "来源id不能为空")
    @ApiModelProperty(value = "来源id", required = true)
    private Long sourceId;

    @NotEmpty(message = "试卷名称不能为空")
    @ApiModelProperty(value = "试卷名称", required = true)
    private String examName;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "答题时间(分钟)")
    private Integer answerTime;

    @NotNull(message = "是否免费不能为空")
    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "优惠价格")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "试卷排序")
    private Integer examSort;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "是否公开(1:公开，0:不公开-绑定课程)", required = true)
    private Integer isPublic;
}
