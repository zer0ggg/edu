package com.roncoo.education.exam.common.dto.auth;

import com.roncoo.education.exam.common.dto.UserExamCategoryDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 * @date 2020-04-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExamCategoryListDTO", description="用户试卷分类")
public class UserExamCategoryListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分类列表", required = true)
    private List<UserExamCategoryDTO> examCategoryList = new ArrayList<>();

}
