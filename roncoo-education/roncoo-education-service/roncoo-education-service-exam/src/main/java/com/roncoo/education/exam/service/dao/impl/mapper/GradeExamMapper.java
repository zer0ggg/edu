package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeExamMapper {
    int countByExample(GradeExamExample example);

    int deleteByExample(GradeExamExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeExam record);

    int insertSelective(GradeExam record);

    List<GradeExam> selectByExample(GradeExamExample example);

    GradeExam selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeExam record, @Param("example") GradeExamExample example);

    int updateByExample(@Param("record") GradeExam record, @Param("example") GradeExamExample example);

    int updateByPrimaryKeySelective(GradeExam record);

    int updateByPrimaryKey(GradeExam record);
}
