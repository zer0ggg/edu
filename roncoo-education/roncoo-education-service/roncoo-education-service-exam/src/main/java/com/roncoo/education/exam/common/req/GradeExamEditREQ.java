package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeExamEditREQ", description = "修改")
public class GradeExamEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @NotNull(message = "状态ID不能为空")
    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)", required = true)
    private Integer statusId;

    @NotNull(message = "班级考试名称不能为空")
    @ApiModelProperty(value = "班级考试名称", required = true)
    private String gradeExamName;

    @NotNull(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间", required = true)
    private Date beginTime;

    @NotNull(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间", required = true)
    private Date endTime;

    @NotNull(message = "答案展示不能为空")
    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)", required = true)
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间", required = true)
    private Date answerShowTime;

    @NotNull(message = "评阅类型不能为空")
    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)", required = true)
    private Integer auditType;

    @NotNull(message = "补交状态不能为空")
    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)", required = true)
    private Integer compensateStatus;
}
