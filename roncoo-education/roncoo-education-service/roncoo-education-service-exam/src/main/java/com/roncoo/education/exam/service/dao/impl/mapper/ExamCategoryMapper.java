package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamCategoryMapper {
    int countByExample(ExamCategoryExample example);

    int deleteByExample(ExamCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamCategory record);

    int insertSelective(ExamCategory record);

    List<ExamCategory> selectByExample(ExamCategoryExample example);

    ExamCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamCategory record, @Param("example") ExamCategoryExample example);

    int updateByExample(@Param("record") ExamCategory record, @Param("example") ExamCategoryExample example);

    int updateByPrimaryKeySelective(ExamCategory record);

    int updateByPrimaryKey(ExamCategory record);
}