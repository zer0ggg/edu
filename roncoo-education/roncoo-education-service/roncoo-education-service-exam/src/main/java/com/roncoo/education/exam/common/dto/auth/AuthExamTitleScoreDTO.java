package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级考试试卷大题分数
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamTitleScoreDTO", description = "班级考试试卷大题分数")
public class AuthExamTitleScoreDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题ID")
    private Long id ;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "标题总分")
    private Integer score;

    @ApiModelProperty(value = "标题类型（1：单选题；2多选题；3判断题；4填空题；5解答题；6组合题）")
    private Integer titleType;

}
