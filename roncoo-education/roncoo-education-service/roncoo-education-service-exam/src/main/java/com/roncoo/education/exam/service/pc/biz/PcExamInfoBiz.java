package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.IsPutawayEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.exam.common.es.EsExam;
import com.roncoo.education.exam.common.req.ExamInfoListREQ;
import com.roncoo.education.exam.common.req.ExamInfoPutWayREQ;
import com.roncoo.education.exam.common.req.ExamInfoUpdateREQ;
import com.roncoo.education.exam.common.req.ExamInfoUpdateStatusREQ;
import com.roncoo.education.exam.common.resp.ExamInfoListRESP;
import com.roncoo.education.exam.common.resp.ExamInfoViewRESP;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 试卷信息
 *
 * @author wujing
 */
@Component
public class PcExamInfoBiz extends BaseBiz {

    @Autowired
    private ExamInfoDao dao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private UserExamCategoryDao userExamCategoryDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    public Result<Page<ExamInfoListRESP>> list(ExamInfoListREQ req) {
        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getExamName())) {
            c.andExamNameLike(PageUtil.like(req.getExamName()));
        }
        if (req.getSubjectId() != null) {
            c.andSubjectIdEqualTo(req.getSubjectId());
        }
        if (req.getYearId() != null) {
            c.andYearIdEqualTo(req.getYearId());
        }
        if (req.getSourceId() != null) {
            c.andSourceIdEqualTo(req.getSourceId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (req.getIsPutaway() != null) {
            c.andIsPutawayEqualTo(req.getIsPutaway());
        }
        if (req.getIsFree() != null) {
            c.andIsFreeEqualTo(req.getIsFree());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<ExamInfo> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ExamInfoListRESP> respPage = PageUtil.transform(page, ExamInfoListRESP.class);
        // 补充分类名称
        for (ExamInfoListRESP dto : respPage.getList()) {
            if (dto.getLecturerUserNo() != null && dto.getLecturerUserNo() != 0) {
                LecturerVO lecturer = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
                if (!ObjectUtil.isNull(lecturer) && StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                    dto.setLecturerName(lecturer.getLecturerName());
                }
            }
            if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setSubjectName(examCategory.getCategoryName());
                }
            }
            if (dto.getYearId() != null && dto.getYearId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getYearId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setYear(examCategory.getCategoryName());
                }
            }
            if (dto.getSourceId() != null && dto.getSourceId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getSourceId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setSourceView(examCategory.getCategoryName());
                }
            }
            if (dto.getPersonalId() != null && dto.getPersonalId() != 0) {
                UserExamCategory examCategory = userExamCategoryDao.getById(dto.getPersonalId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setPersonalName(examCategory.getCategoryName());
                }
            }
        }
        return Result.success(respPage);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> isPutaway(ExamInfoPutWayREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!IsPutawayEnum.YES.getCode().equals(req.getIsPutaway()) && !IsPutawayEnum.NO.getCode().equals(req.getIsPutaway())) {
            return Result.error("请输入正确得枚举");
        }
        ExamInfo examInfo = dao.getById(req.getId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("找不到试卷信息");
        }
        ExamInfo exam = BeanUtil.copyProperties(req, ExamInfo.class);
        dao.updateById(exam);

        // 修改审核表
        ExamInfoAudit examAudit = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        examAudit.setIsPutaway(req.getIsPutaway());
        examInfoAuditDao.updateById(examAudit);

        return Result.success("修改成功");
    }

    public Result<ExamInfoViewRESP> view(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        ExamInfo record = dao.getById(id);
        if (ObjectUtils.isEmpty(record)) {
            return Result.error("id不正确");
        }
        ExamInfoViewRESP resp = BeanUtil.copyProperties(record, ExamInfoViewRESP.class);
        return Result.success(resp);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> update(ExamInfoUpdateREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        // 价格处理
        // 原价为0，试卷也是免费
        if (IsFreeEnum.FREE.getCode().equals(req.getIsFree())) {
            // 免费就设置价格为0(原价、优惠价)
            req.setOrgPrice(BigDecimal.valueOf(0));
            req.setFabPrice(BigDecimal.valueOf(0));
        } else {
            // 收费但价格为空
            if (req.getOrgPrice() == null) {
                return Result.error("请输入价格");
            }
            if (req.getFabPrice() == null) {
                req.setFabPrice(BigDecimal.valueOf(0));
            }
            // 原价小于0
            if (req.getOrgPrice().compareTo(BigDecimal.valueOf(0)) < 0 || req.getFabPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("价格不能为负数");
            }
            if (req.getFabPrice().compareTo(req.getOrgPrice()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }
        ExamInfo examInfo = BeanUtil.copyProperties(req, ExamInfo.class);
        dao.updateById(examInfo);
        // 同步审核表
        ExamInfoAudit examAudit = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        if (examInfoAuditDao.updateById(examAudit) > 0) {
            // 插入es或者更新es
            try {
                EsExam esExam = BeanUtil.copyProperties(examAudit, EsExam.class);
                IndexQuery query = new IndexQueryBuilder().withObject(esExam).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsExam.EXAM));
            } catch (Exception e) {
                logger.warn("elasticsearch更新数据失败", e);
                throw new BaseException("操作失败");
            }
            return Result.success("操作成功");
        }
        return Result.error("编辑失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> updateStatus(ExamInfoUpdateStatusREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
            return Result.error("枚举值不正确");
        }
        ExamInfo examInfo = dao.getById(req.getId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("找不到试卷信息");
        }
        ExamInfo exam = BeanUtil.copyProperties(req, ExamInfo.class);
        dao.updateById(exam);

        // 修改审核表
        ExamInfoAudit examAudit = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        examAudit.setStatusId(req.getStatusId());
        examInfoAuditDao.updateById(examAudit);
        return Result.success("修改成功");
    }

    public Result<String> addEs() {
        // 分批查出来
        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        example.setOrderByClause("sort asc, id desc ");
        int count = dao.countByExample(example);
        int pageCurrent = 1;
        int pageSize = 1000;
        elasticsearchRestTemplate.delete(EsExam.EXAM);
        for (pageCurrent = 1; pageCurrent <= count / pageSize + 1; pageCurrent++) {
            Page<ExamInfo> examInfoPage = dao.listForPage(pageCurrent, pageSize, example);
            List<IndexQuery> queries = new ArrayList<>();
            try {
                // 插入es
                for (ExamInfo exam : examInfoPage.getList()) {
                    EsExam esExam = BeanUtil.copyProperties(exam, EsExam.class);
                    IndexQuery query = new IndexQueryBuilder().withObject(esExam).build();
                    queries.add(query);
                }
                elasticsearchRestTemplate.indexOps(EsExam.class).delete();
                elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsExam.EXAM));
            } catch (Exception e) {
                logger.warn("elasticsearch更新数据失败", e);
                throw new BaseException("导入成功");
            }
        }
        return Result.success("导入成功");
    }
}
