package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample;

import java.util.List;

/**
 * 题目练习关联 服务类
 *
 * @author LHR
 * @date 2020-10-10
 */
public interface PracticeProblemRefDao {

    /**
    * 保存题目练习关联
    *
    * @param record 题目练习关联
    * @return 影响记录数
    */
    int save(PracticeProblemRef record);

    /**
    * 根据ID删除题目练习关联
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 题目练习关联
    * @return 影响记录数
    */
    int updateById(PracticeProblemRef record);

    /**
    * 根据ID获取题目练习关联
    *
    * @param id 主键ID
    * @return 题目练习关联
    */
    PracticeProblemRef getById(Long id);

    /**
    * 题目练习关联--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<PracticeProblemRef> listForPage(int pageCurrent, int pageSize, PracticeProblemRefExample example);

    /**
     * 根据练习id获取关联信息
     * @param practiceId
     * @return
     */
    List<PracticeProblemRef> listByPracticeId(Long practiceId);

    /**
     * 根据用户编号、练习id删除关联关系
     * @param userNo
     * @param practiceId
     * @return
     */
    Integer deleteByUserNoAndPracticeId(Long userNo, Long practiceId);

    /**
     * 根据用户编号、练习id、试题id获取关联信息
     * @param userNo
     * @param practiceId
     * @param problemId
     * @return
     */
    PracticeProblemRef getByUserNoAndPracticeIdAndProblemId(Long userNo, Long practiceId, Long problemId);
}
