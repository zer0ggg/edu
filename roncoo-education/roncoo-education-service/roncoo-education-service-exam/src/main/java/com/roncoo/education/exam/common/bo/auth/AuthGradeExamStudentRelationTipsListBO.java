package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级学生考试提示列表请求对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationTipsListBO", description = "班级学生考试提示列表请求对象")
public class AuthGradeExamStudentRelationTipsListBO implements Serializable {

    private static final long serialVersionUID = 1087298828115084717L;

    @Min(value = 1, message = "显示记录数必须大于0")
    @Max(value = 20, message = "最多只能显示20条")
    @ApiModelProperty(value = "显示记录数")
    private Integer num = 5;
}
