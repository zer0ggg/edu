package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeInfo;
import com.roncoo.education.exam.feign.qo.GradeInfoQO;
import com.roncoo.education.exam.feign.vo.GradeInfoVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeInfoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeInfoController extends BaseController implements IFeignGradeInfo{

    @Autowired
    private FeignGradeInfoBiz biz;

	@Override
	public Page<GradeInfoVO> listForPage(@RequestBody GradeInfoQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeInfoQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeInfoQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeInfoVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
