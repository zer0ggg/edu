package com.roncoo.education.exam.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 试卷题目收藏
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamProblemCollectionViewRESP", description="试卷题目收藏查看")
public class ExamProblemCollectionViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效，0：无效)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编码")
    private Long userNo;

    @ApiModelProperty(value = "收藏类型(4:试卷,5:题目)")
    private Integer collectionType;

    @ApiModelProperty(value = "题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:材料题)")
    private Integer problemType;

    @ApiModelProperty(value = "收藏ID")
    private Long collectionId;
}
