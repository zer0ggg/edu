package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 考试分类
 *
 * @author forest
 *
 */
@Data
@Accessors(chain = true)
public class ExamCategoryListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "分类列表", required = true)
	private List<ExamCategoryDTO> examCategoryList = new ArrayList<>();
}
