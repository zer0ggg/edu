package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.IsPubilcEnum;
import com.roncoo.education.common.core.enums.ProblemCategoryEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamProblemMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷题目 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamProblemDaoImpl extends AbstractBaseJdbc implements ExamProblemDao {

    @Autowired
    private ExamProblemMapper mapper;

    @Override
    public int save(ExamProblem record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamProblem record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamProblem getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamProblem> listForPage(int pageCurrent, int pageSize, ExamProblemExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    // 查出大字段
    @Override
    public List<ExamProblem> listInIdList(List<Long> idList) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andIdIn(idList);
        example.setOrderByClause("sort asc, status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByParentId(Long parentId) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public List<ExamProblem> listByParentId(Long parentId) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblem> listByParentIdWithBLOBs(Long parentId) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblem> listByVideoId(Long videoId) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andVideoIdEqualTo(videoId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int updatePersonalIdByIdBatch(List<Long> idList, Long userNo, Long personalId) {
        ExamProblem problem = new ExamProblem();
        problem.setPersonalId(personalId);
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andIdIn(idList);
        c.andLecturerUserNoEqualTo(userNo);
        return this.mapper.updateByExampleSelective(problem, example);
    }

    @Override
    public ExamProblem getByIdAndVideoVid(Long id, String videoVid) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(id);
        c.andVideoVidEqualTo(videoVid);
        List<ExamProblem> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public ExamProblem viewWithBLOBs(Long id) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(id);
        List<ExamProblem> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public int updateByVideoId(ExamProblem record) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andVideoIdEqualTo(record.getVideoId());
        return this.mapper.updateByExampleSelective(record, example);
    }

    @Override
    public List<ExamProblem> listByCondition(ExamProblem record) {
        StringBuilder sql = new StringBuilder();
        sql.append("select id from exam_problem from where ");
        sql.append("is_public = ").append(IsPubilcEnum.YES.getCode()).append(" and ");
        if (record.getSubjectId() != null) {
            sql.append("subject_id=").append(record.getSubjectId()).append(" and ");
        }
        if (record.getProblemType() != null) {
            sql.append("subject_id=").append(record.getProblemType()).append(" and ");
        }
        if (record.getDifficultyId() != null) {
            sql.append("difficulty_id=").append(record.getDifficultyId()).append(" and ");
        }
        if (record.getSourceId() != null) {
            sql.append("source_id=").append(record.getSourceId()).append(" and ");
        }
        if (record.getYearId() != null) {
            sql.append("year_id=").append(record.getYearId()).append(" and ");
        }
        if (record.getTopicId() != null) {
            sql.append("topic_id=").append(record.getTopicId()).append(" and ");
        }
        if (record.getGraId() != null) {
            sql.append("gra_id=").append(record.getGraId()).append(" and ");
        }
        if (record.getEmphasisId() != null) {
            sql.append("emphasis_id=").append(record.getEmphasisId()).append(" and ");
        }
        sql.append(" parent_id = 0 ORDER BY id DESC");
        List<ExamProblem> resultList = queryForObjectList(sql.toString(), ExamProblem.class);
        return resultList;
    }

    @Override
    public List<ExamProblem> listByIsFree(Integer isFree) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andIsFreeEqualTo(isFree);
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblem> listBySubjectIdAndIsFreeAndProblemType(Long subjectId, Integer isFree) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        c.andSubjectIdEqualTo(subjectId);
        c.andIsFreeEqualTo(isFree);
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andProblemTypeLessThan(4);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblem> ListByGraIdAndProblemCategory(Long problemCategoryId, Integer problemCategory) {
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        if (ProblemCategoryEnum.GRA.getCode().equals(problemCategory)) {
            // 年级id
            c.andGraIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.SUBJECT.getCode().equals(problemCategory)) {
            // 科目id
            c.andSubjectIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.YEAR.getCode().equals(problemCategory)) {
            // 年份id
            c.andYearIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.SOURCE.getCode().equals(problemCategory)) {
            // 来源id
            c.andSourceIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.DIFFICULTY.getCode().equals(problemCategory)) {
            // 难度id
            c.andDifficultyIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.TOPIC.getCode().equals(problemCategory)) {
            // 题类id
            c.andTopicIdEqualTo(problemCategoryId);
        }
        if (ProblemCategoryEnum.EMPHASIS.getCode().equals(problemCategory)) {
            // 考点id
            c.andTopicIdEqualTo(problemCategoryId);
        }
        return this.mapper.selectByExample(example);
    }
}
