package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExamCategory;
import com.roncoo.education.exam.feign.qo.UserExamCategoryQO;
import com.roncoo.education.exam.feign.vo.UserExamCategoryVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamCategoryBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户试卷分类
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamCategoryController extends BaseController implements IFeignUserExamCategory{

    @Autowired
    private FeignUserExamCategoryBiz biz;

	@Override
	public Page<UserExamCategoryVO> listForPage(@RequestBody UserExamCategoryQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamCategoryQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamCategoryQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamCategoryVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
