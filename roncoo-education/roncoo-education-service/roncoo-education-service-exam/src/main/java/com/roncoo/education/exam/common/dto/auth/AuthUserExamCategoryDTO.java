package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 * @date 2020-04-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthUserExamCategoryDTO", description="用户试卷分类")
public class AuthUserExamCategoryDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "分类类型(1:用户；2：讲师）")
    private Integer userType;

    @ApiModelProperty(value = "父分类ID")
    private Long parentId;

    @ApiModelProperty(value = "分类类型(1:试题；2：试卷）")
    private Integer categoryType;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    @ApiModelProperty(value = "层级")
    private Integer floor;

    @ApiModelProperty(value = "备注")
    private String remark;

    private List<AuthUserExamCategoryDTO> list;
}
