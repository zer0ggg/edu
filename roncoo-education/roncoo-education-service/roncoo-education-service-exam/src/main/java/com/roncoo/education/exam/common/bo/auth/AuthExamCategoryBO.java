package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 考试分类
 * @author Administrator
 *
 */
@Data
@Accessors(chain = true)
public class AuthExamCategoryBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 试卷分类类型
     */
    @ApiModelProperty(value = "分类类型(1:试卷科目,2:试卷年份,3：试卷来源,4：试题难度,5：试题类型)")
    private Integer examCategoryType;

}
