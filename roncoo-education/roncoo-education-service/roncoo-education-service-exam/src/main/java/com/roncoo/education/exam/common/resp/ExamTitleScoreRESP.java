package com.roncoo.education.exam.common.resp;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *  逻辑业务类：PcGradeExamStudentRelationBiz
 *   @author hsq
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamTitleScoreRESP", description="试卷大标题分数")
public class ExamTitleScoreRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题ID")
    private Long id ;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "标题总分")
    private Integer score;

    @ApiModelProperty(value = "标题类型（1：单选题；2多选题；3判断题；4填空题；5解答题；6组合题）")
    private Integer titleType;

}
