package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 试卷题目选项
 * </p>
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthExamTitleProblemOptionDTO", description="试卷题目选项")
public class AuthExamTitleProblemOptionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;

    @ApiModelProperty(value = "选项内容")
    private String optionContent;
}
