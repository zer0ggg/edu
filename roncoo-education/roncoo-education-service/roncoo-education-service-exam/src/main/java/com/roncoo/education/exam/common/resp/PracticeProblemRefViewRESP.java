package com.roncoo.education.exam.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 题目练习关联
 * </p>
 *
 * @author LHR
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PracticeProblemRefViewRESP", description="题目练习关联查看")
public class PracticeProblemRefViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "练习ID")
    private Long practiceId;

    @ApiModelProperty(value = "题目父类ID")
    private Long problemParentId;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;
}
