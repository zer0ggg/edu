package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample;

import java.util.List;

/**
 * 试卷分类 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamCategoryDao {

    /**
    * 保存试卷分类
    *
    * @param record 试卷分类
    * @return 影响记录数
    */
    int save(ExamCategory record);

    /**
    * 根据ID删除试卷分类
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷分类
    * @return 影响记录数
    */
    int updateById(ExamCategory record);

    /**
    * 根据ID获取试卷分类
    *
    * @param id 主键ID
    * @return 试卷分类
    */
    ExamCategory getById(Long id);

    /**
    * 试卷分类--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamCategory> listForPage(int pageCurrent, int pageSize, ExamCategoryExample example);

    /**
     * 根据ID和类型查找考试分类
     *
     * @param id               试卷分类ID
     * @param examCategoryType 试卷分类类型
     * @return
     */
    List<ExamCategory> listByIdAndCategoryType(Long id, Integer examCategoryType);

    /**
     * 根据父分类编号查找考试分类信息
     *
     * @param parentId
     * @return
     */
    List<ExamCategory> listByParentId(Long parentId);

    /**
     * 根据层级列表分类信息
     *
     * @param floor
     * @return
     */
    List<ExamCategory> listByFloor(Integer floor);

    /**
     * 根据层级、父类ID列出分类信息
     *
     * @param floor
     * @param parentId
     * @return
     */
    List<ExamCategory> listByFloorAndCategoryId(Integer floor, Long parentId);

    /**
     * 根据分类类型、层级查询可用状态的课程分类集合
     *
     * @param floor
     * @param statusId
     * @return
     */
    List<ExamCategory> listByFloorAndStatusId(Integer floor, Integer statusId);

    /**
     * 根据父分类ID和状态查找考试分类信息列表
     *
     * @param parentId
     * @param statusId
     * @return
     */
    List<ExamCategory> listByParentIdAndStatusId(Long parentId, Integer statusId);

    /**
     * 根据类型、层级、父分类ID获取分类信息
     *
     * @param examCategoryType
     * @param floor
     * @param parentId
     * @return
     */
    List<ExamCategory> listByExamCategoryTypeAndFloorAndCategoryId(Integer examCategoryType, Integer floor, Long parentId);

    /**
     * 根据层级、状态、试卷分类获取考试分类信息列表
     *
     * @param i
     * @param code
     * @param examCategoryType
     * @return
     */
    List<ExamCategory> listByFloorAndStatusIdAndCategoryTypeAndExamCategoryType(int i, Integer code, Integer categoryType,Integer examCategoryType);

    /**
     * 根据类型,父级id，层级，名称获取考试分类信息
     *
     * @param i
     * @param code
     * @param examCategoryType
     * @return
     */
    ExamCategory getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(Integer categoryType,Integer examCategoryType,Long parentId,String categoryName);

    /**
     * 根据类型,层级，状态获取考试分类信息
     * @param examCategoryType
     * @param floor
     * @param statusId
     * @return
     */
    List<ExamCategory> listByExamCategoryTypeAndFloorAndStatusId(Integer examCategoryType, int floor, Integer statusId);

    /**
     * 根据类型获取所有分类信息
     *
     * @return
     */
    List<ExamCategory> listCategoryTypeAll(Integer categoryType);
}
