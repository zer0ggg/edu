package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamProblemOptionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamProblemOptionMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷题目选项 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamProblemOptionDaoImpl implements ExamProblemOptionDao {

    @Autowired
    private ExamProblemOptionMapper mapper;

    @Override
    public int save(ExamProblemOption record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamProblemOption record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamProblemOption getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamProblemOption> listForPage(int pageCurrent, int pageSize, ExamProblemOptionExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ExamProblemOption> listByProblemIdAndStatusId(Long problemId, Integer statusId) {
        ExamProblemOptionExample example = new ExamProblemOptionExample();
        ExamProblemOptionExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblemOption> listByProblemId(Long problemId) {
        ExamProblemOptionExample example = new ExamProblemOptionExample();
        ExamProblemOptionExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamProblemOption> listByProblemIdWithBLOBs(Long problemId) {
        ExamProblemOptionExample example = new ExamProblemOptionExample();
        ExamProblemOptionExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public int deleteByProblemId(Long problemId) {
        ExamProblemOptionExample example = new ExamProblemOptionExample();
        ExamProblemOptionExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        return this.mapper.deleteByExample(example);
    }


}
