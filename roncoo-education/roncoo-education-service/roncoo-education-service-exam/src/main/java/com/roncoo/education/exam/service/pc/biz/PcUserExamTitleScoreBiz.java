package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamTitleScoreEditREQ;
import com.roncoo.education.exam.common.req.UserExamTitleScoreListREQ;
import com.roncoo.education.exam.common.req.UserExamTitleScoreSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamTitleScoreListRESP;
import com.roncoo.education.exam.common.resp.UserExamTitleScoreViewRESP;
import com.roncoo.education.exam.service.dao.UserExamTitleScoreDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试标题得分
 *
 * @author wujing
 */
@Component
public class PcUserExamTitleScoreBiz extends BaseBiz {

	@Autowired
	private UserExamTitleScoreDao dao;

	/**
	 * 用户考试标题得分列表
	 *
	 * @param userExamTitleScoreListREQ 用户考试标题得分分页查询参数
	 * @return 用户考试标题得分分页查询结果
	 */
	public Result<Page<UserExamTitleScoreListRESP>> list(UserExamTitleScoreListREQ userExamTitleScoreListREQ) {
		UserExamTitleScoreExample example = new UserExamTitleScoreExample();
		Criteria c = example.createCriteria();
		Page<UserExamTitleScore> page = dao.listForPage(userExamTitleScoreListREQ.getPageCurrent(), userExamTitleScoreListREQ.getPageSize(), example);
		Page<UserExamTitleScoreListRESP> respPage = PageUtil.transform(page, UserExamTitleScoreListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户考试标题得分添加
	 *
	 * @param userExamTitleScoreSaveREQ 用户考试标题得分
	 * @return 添加结果
	 */
	public Result<String> save(UserExamTitleScoreSaveREQ userExamTitleScoreSaveREQ) {
		UserExamTitleScore record = BeanUtil.copyProperties(userExamTitleScoreSaveREQ, UserExamTitleScore.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户考试标题得分查看
	 *
	 * @param id 主键ID
	 * @return 用户考试标题得分
	 */
	public Result<UserExamTitleScoreViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamTitleScoreViewRESP.class));
	}

	/**
	 * 用户考试标题得分修改
	 *
	 * @param userExamTitleScoreEditREQ 用户考试标题得分修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamTitleScoreEditREQ userExamTitleScoreEditREQ) {
		UserExamTitleScore record = BeanUtil.copyProperties(userExamTitleScoreEditREQ, UserExamTitleScore.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户考试标题得分删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
