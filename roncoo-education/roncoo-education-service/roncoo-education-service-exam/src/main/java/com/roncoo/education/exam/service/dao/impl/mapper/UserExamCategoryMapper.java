package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamCategoryMapper {
    int countByExample(UserExamCategoryExample example);

    int deleteByExample(UserExamCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExamCategory record);

    int insertSelective(UserExamCategory record);

    List<UserExamCategory> selectByExample(UserExamCategoryExample example);

    UserExamCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExamCategory record, @Param("example") UserExamCategoryExample example);

    int updateByExample(@Param("record") UserExamCategory record, @Param("example") UserExamCategoryExample example);

    int updateByPrimaryKeySelective(UserExamCategory record);

    int updateByPrimaryKey(UserExamCategory record);
}