package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 学生添加班级对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeStudentAddGradeBO", description = "学生添加班级对象")
public class AuthGradeStudentAddGradeBO implements Serializable {

    private static final long serialVersionUID = 648575838850680372L;

    @NotNull(message = "昵称不能为空")
    @ApiModelProperty(value = "昵称", required = true)
    private String nickname;

    @NotBlank(message = "班级邀请码不能为空")
    @ApiModelProperty(value = "班级邀请码", required = true)
    private String invitationCode;

    @NotBlank(message = "申请信息")
    @ApiModelProperty(value = "申请信息", required = true)
    private String applyDescription;
}
