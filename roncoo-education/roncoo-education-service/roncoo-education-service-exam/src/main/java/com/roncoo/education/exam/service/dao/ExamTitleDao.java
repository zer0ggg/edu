package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitle;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample;

import java.util.List;

/**
 * 试卷标题 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamTitleDao {

    /**
    * 保存试卷标题
    *
    * @param record 试卷标题
    * @return 影响记录数
    */
    int save(ExamTitle record);

    /**
    * 根据ID删除试卷标题
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷标题
    * @return 影响记录数
    */
    int updateById(ExamTitle record);

    /**
    * 根据ID获取试卷标题
    *
    * @param id 主键ID
    * @return 试卷标题
    */
    ExamTitle getById(Long id);

    /**
    * 试卷标题--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamTitle> listForPage(int pageCurrent, int pageSize, ExamTitleExample example);

    /**
     * 根据试卷ID和状态ID获取试卷标题集合
     *
     * @param examId   试卷ID
     * @param statusId 状态ID
     * @return 试卷标题集合
     */
    List<ExamTitle> listByExamIdAndStatusId(Long examId, Integer statusId);

    /**
     * 根据试卷ID获取试卷标题集合
     *
     * @param examId 试卷ID
     * @return 试卷标题集合
     */
    List<ExamTitle> listByExamId(Long examId);

    /**
     * 根据试卷ID删除试卷标题
     *
     * @param examId 试卷ID
     * @return 删除记录数
     */
    int deleteByExamId(Long examId);

}
