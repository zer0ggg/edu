package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamRecommendQO;
import com.roncoo.education.exam.feign.vo.ExamRecommendVO;
import com.roncoo.education.exam.service.dao.ExamRecommendDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷推荐
 *
 * @author wujing
 */
@Component
public class FeignExamRecommendBiz extends BaseBiz {

    @Autowired
    private ExamRecommendDao dao;

	public Page<ExamRecommendVO> listForPage(ExamRecommendQO qo) {
	    ExamRecommendExample example = new ExamRecommendExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamRecommend> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamRecommendVO.class);
	}

	public int save(ExamRecommendQO qo) {
		ExamRecommend record = BeanUtil.copyProperties(qo, ExamRecommend.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamRecommendVO getById(Long id) {
		ExamRecommend record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamRecommendVO.class);
	}

	public int updateById(ExamRecommendQO qo) {
		ExamRecommend record = BeanUtil.copyProperties(qo, ExamRecommend.class);
		return dao.updateById(record);
	}

}
