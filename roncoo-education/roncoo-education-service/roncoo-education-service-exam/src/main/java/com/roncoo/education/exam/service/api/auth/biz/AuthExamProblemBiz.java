package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.ExcelToolUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadUrlFile;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemOptionViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemViewDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 试卷题目
 *
 * @author wujing
 */
@Component
public class AuthExamProblemBiz extends BaseBiz {

    @Autowired
    private ExamProblemDao dao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private UserExamCategoryDao userExamCategoryDao;
    @Autowired
    private ExamVideoDao examVideoDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    /**
     * 讲师分页列出试题
     *
     * @param bo 分页参数
     * @return 分页结果
     */
    public Result<Page<AuthExamProblemPageDTO>> listForPage(AuthExamProblemPageBO bo) {
        logger.info(bo.toString());
        if (ThreadContext.userNo() == null) {
            return Result.error("用户编号不能为空");
        }
        ExamProblemExample example = new ExamProblemExample();
        ExamProblemExample.Criteria c = example.createCriteria();
        if (StringUtils.hasText(bo.getProblemContent())) {
            c.andProblemContentLike(PageUtil.like(bo.getProblemContent()));
        }
        if (bo.getProblemType() != null) {
            c.andProblemTypeEqualTo(bo.getProblemType());
        }
        if (bo.getSubjectId() != null) {
            c.andSubjectIdEqualTo(bo.getSubjectId());
        }
        if (bo.getDifficultyId() != null) {
            c.andDifficultyIdEqualTo(bo.getDifficultyId());
        }
        //拿第一级
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andParentIdEqualTo(0L);
        example.setOrderByClause("gmt_modified desc, sort asc ,id asc ");
        Page<ExamProblem> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);

        Page<AuthExamProblemPageDTO> listForPage = PageUtil.transform(page, AuthExamProblemPageDTO.class);



        if (page.getList() == null || page.getList().size() <= 0) {
            return Result.success(listForPage);
        }
        Set<Long> lecturerUserNos = listForPage.getList().stream().map(AuthExamProblemPageDTO::getLecturerUserNo).collect(Collectors.toSet());
        LecturerQO lecturerQO = new LecturerQO();
        lecturerQO.setLecturerUserNos(new ArrayList<>(lecturerUserNos));
        List<LecturerVO> lecturerList = feignLecturer.listByLecturerUserNos(lecturerQO);
        if (CollectionUtil.isNotEmpty(lecturerList)) {
            Map<Long, LecturerVO> lecturerMap = lecturerList.stream().collect(Collectors.toMap(LecturerVO::getLecturerUserNo, item -> item));
            listForPage.getList().stream().forEach(item -> {
                LecturerVO lecturerVO = lecturerMap.get(item.getLecturerUserNo());
                item.setLecturerName(lecturerVO.getLecturerName());
            });
        }
        // 获取分类
        List<ExamCategory> examCategoryList = examCategoryDao.listCategoryTypeAll(ExamMainCategoryTypeEnum.SUBJECT.getCode());
        if (CollectionUtil.isNotEmpty(examCategoryList)) {
            Map<Long, ExamCategory> examCategoryMap = examCategoryList.stream().collect(Collectors.toMap(ExamCategory::getId, item -> item));
            listForPage.getList().stream().forEach(item -> {
                // 科目名称
                ExamCategory examCategory = examCategoryMap.get(item.getSubjectId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setSubjectName(examCategory.getCategoryName());
                    //第二级则查出第一级
                    ExamCategory parent = examCategoryDao.getById(examCategory.getParentId());
                    if (ObjectUtil.isNotNull(parent)) {
                        item.setSubjectParentId(parent.getId());
                        item.setSubjectParentName(parent.getCategoryName());
                    }
                }
                // 考点
                examCategory = examCategoryMap.get(item.getEmphasisId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setAnalysis(examCategory.getCategoryName());
                }
                // 年份
                examCategory = examCategoryMap.get(item.getYearId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setYear(examCategory.getCategoryName());
                }
                // 来源
                examCategory = examCategoryMap.get(item.getSourceId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setSourceView(examCategory.getCategoryName());
                }
                // 难度
                examCategory = examCategoryMap.get(item.getDifficultyId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setDifficulty(examCategory.getCategoryName());
                }
                // 题类
                examCategory = examCategoryMap.get(item.getTopicId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setTopicName(examCategory.getCategoryName());
                }
                // 讲师分类
                examCategory = examCategoryMap.get(item.getPersonalId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setPersonalName(examCategory.getCategoryName());
                }
            });
        }


        Map<Long, ExamCategory> subjectMap = new HashMap<>();
        for (AuthExamProblemPageDTO dto : listForPage.getList()) {
            //标题过长，截取
            if (StrUtil.length(dto.getProblemContent()) > 30) {
                dto.setProblemContent(dto.getProblemContent().substring(0, 30));
            }
            //对材料题进行特殊处理，查出小题
            if (ProblemTypeEnum.MATERIAL.getCode().equals(dto.getProblemType())) {
                List<ExamProblem> childrenList = dao.listByParentId(dto.getId());
                if (CollectionUtil.isNotEmpty(childrenList)) {
                    dto.setChildrenList(BeanUtil.copyProperties(childrenList, AuthExamProblemPageDTO.class));
                }
            }

            //科目名称 支持三层结构的科目
            setPageCategory(dto, subjectMap);
            // 设置附属信息
           // setAuthExamProblemPageAttach(dto);
        }
        return Result.success(listForPage);
    }

    /**
     * 讲师试题审核保存
     *
     * @param bo 试题审核保存参数
     * @return 保存结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthExamProblemSaveBO bo) {
        logger.debug("======={}", JSONUtil.toJsonStr(bo));
        //检验讲师
        LecturerVO lecturer = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
            return Result.error("讲师异常");
        }
        if (!ProblemTypeEnum.MATERIAL.getCode().equals(bo.getProblemType()) && StringUtils.isEmpty(bo.getProblemAnswer())) {
            throw new BaseException("试题答案不能为空");
        }
        ExamProblem problem = BeanUtil.copyProperties(bo, ExamProblem.class);
        problem.setLecturerUserNo(ThreadContext.userNo());
        //保存试题
        problem.setLecturerUserNo(ThreadContext.userNo());
        problem.setIsFree(IsFreeEnum.FREE.getCode());
        problem.setParentId(0L);
        problem.setId(IdWorker.getId());
        int results = dao.save(problem);

        //添加视频关联
        if (bo.getVideoId() != null) {
            ExamVideo examVideo = examVideoDao.getById(bo.getVideoId());
            if (ObjectUtil.isNull(examVideo)) {
                logger.debug("获取不到该视频信息,参数={}", bo.getVideoId());
                throw new BaseException("获取不到该视频信息");
            }
            problem.setVideoId(examVideo.getId());
            problem.setVideoName(problem.getVideoName());
            problem.setVideoLength(examVideo.getVideoLength());
            problem.setVideoVid(examVideo.getVideoVid());
            //更新视频关联
            examVideo.setProblemId(problem.getId());
            examVideoDao.updateById(examVideo);
        }


        // 材料题（小题套小题）
        if (ProblemTypeEnum.MATERIAL.getCode().equals(bo.getProblemType())) {
            for (AuthExamProblemSaveBO problemSaveBO : bo.getChildrenList()) {
                if (StringUtils.isEmpty(problemSaveBO.getProblemAnswer())) {
                    throw new BaseException("试题答案不能为空");
                }
                // 添加小题
                ExamProblem subProblem = BeanUtil.copyProperties(problemSaveBO, ExamProblem.class);
                subProblem.setLecturerUserNo(ThreadContext.userNo());
                subProblem.setParentId(problem.getId());
                subProblem.setId(IdWorker.getId());
                dao.save(subProblem);

                // 子试题 单选题、多选题、判断题 选项必填
                validOptionList(bo.getProblemType(), problemSaveBO.getOptionList());
                // 选项添加
                if (!CollectionUtils.isEmpty(problemSaveBO.getOptionList())) {
                    for (AuthExamProblemOptionSaveBO option : problemSaveBO.getOptionList()) {
                        saveOption(option, subProblem.getId());
                    }
                }
            }
        } else {
            // 单选题、多选题、判断题、不定项选择题、填空題、解答题
            // 单选题、多选题、判断题 选项必填
            validOptionList(bo.getProblemType(), bo.getOptionList());
            if (!CollectionUtils.isEmpty(bo.getOptionList())) {
                for (AuthExamProblemOptionSaveBO option : bo.getOptionList()) {
                    saveOption(option, problem.getId());
                }
            }

        }
        //

        return Result.success(results);
    }

    /**
     * 校验  选项
     *
     * @param problemType 题目类型
     * @param optionList  选项集合
     */
    private void validOptionList(Integer problemType, List<AuthExamProblemOptionSaveBO> optionList) {
        if (ProblemTypeEnum.THE_RADIO.getCode().equals(problemType) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problemType) || ProblemTypeEnum.ESTIMATE.getCode().equals(problemType)) {
            if (CollectionUtils.isEmpty(optionList)) {
                throw new BaseException("试题选项不能为空");
            }
        }
    }


    /**
     * 试题删除(如果已经组卷,不允许删除)
     *
     * @param bo 试题删除参数
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(AuthExamProblemDeleteBO bo) {
        if (CollectionUtils.isEmpty(bo.getIdList())) {
            return Result.error("请选择需要删除的试题");
        }

        StringBuilder tips = new StringBuilder();
        int index = 0;
        for (String idStr : bo.getIdList()) {
            index++;
            Long id = Long.valueOf(idStr);
            ExamProblem problem = dao.getById(id);
            if (ObjectUtil.isNull(problem)) {
                tips.append("【第:").append(index).append("条------试题异常，不可删除】");
                logger.warn(tips.toString());
                continue;
            }
            //查是否已经组卷
            List<ExamTitleProblemRef> problemRefList = examTitleProblemRefDao.listByTitleIdOrProblemId(null, id);
            if (!CollectionUtils.isEmpty(problemRefList)) {
                tips.append("【第:").append(index).append("条-----已经组卷，不可删除】");
                logger.warn(tips.toString());
                //已经组卷，不可删除
            } else {
                if (ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
                    List<ExamProblem> subProblemList = dao.listByParentId(id);
                    //删除子一级的试题和选项
                    for (ExamProblem subProblem : subProblemList) {
                        //删除选项
                        examProblemOptionDao.deleteByProblemId(subProblem.getId());
                        //删除试题
                        dao.deleteById(subProblem.getId());
                    }
                }
                //删除本级的试题和选项
                examProblemOptionDao.deleteByProblemId(problem.getId());
                dao.deleteById(problem.getId());
            }
        }
        return Result.success(tips.toString());
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> update(AuthExamProblemUpdateBO bo) {
        logger.debug(bo.toString());
        ExamProblem problem = dao.getById(bo.getId());
        if (ObjectUtil.isNull(problem)) {
            return Result.error("获取不到该题目信息!");
        }
        if (!problem.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有操作权限");
        }

        ExamProblem updateProblem = BeanUtil.copyProperties(bo, ExamProblem.class);
        updateProblem.setLecturerUserNo(ThreadContext.userNo());
        //如果视频id为空
        if (bo.getVideoId() == null) {
            updateProblem.setVideoName(null);
            updateProblem.setVideoLength(null);
            updateProblem.setVideoType(null);
        }
        // 材料题（小题套小题）
        if (ProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
            //原ids
            List<ExamProblem> oldProblemList = dao.listByParentId(bo.getId());
            //删除所有试题的选项
            for (ExamProblem subProblem : oldProblemList) {
                examProblemOptionDao.deleteByProblemId(subProblem.getId());
            }

            //删除失效的子试题ids = 原ids与传入ids的差集
            List<Long> delIds = oldProblemList.stream().map(ExamProblem::getId).collect(Collectors.toList());
            delIds.removeAll(bo.getChildrenList().stream().filter(x -> x.getId() != null).map(AuthExamProblemSaveBO::getId).collect(Collectors.toList()));
            for (Long delId : delIds) {
                dao.deleteById(delId);
            }
            //更新/保存试题与选项
            try {
                for (AuthExamProblemSaveBO problemSaveBO : bo.getChildrenList()) {
                    if (StringUtils.isEmpty(problemSaveBO.getProblemAnswer())) {
                        throw new BaseException("试题答案不能为空");
                    }
                    // 添加/更新小题
                    ExamProblem subProblem = null;
                    if (problemSaveBO.getId() != null) {
                        subProblem = BeanUtil.copyProperties(problemSaveBO, ExamProblem.class);
                        subProblem.setLecturerUserNo(ThreadContext.userNo());
                        dao.updateById(subProblem);

                    } else {
                        subProblem = BeanUtil.copyProperties(problemSaveBO, ExamProblem.class);
                        subProblem.setParentId(problem.getId());
                        subProblem.setLecturerUserNo(ThreadContext.userNo());
                        subProblem.setId(IdWorker.getId());
                        dao.save(subProblem);
                    }

                    // 选项添加
                    if (!CollectionUtils.isEmpty(problemSaveBO.getOptionList())) {
                        for (AuthExamProblemOptionSaveBO option : problemSaveBO.getOptionList()) {
                            saveOption(option, subProblem.getId());
                        }
                    }
                }
            } catch (Exception e) {
                return Result.error(e.getMessage());
            }
        } else {
            // 单选题、多选题、判断题、不定项选择题、填空題、解答题
            //删除选项再重建选项
            examProblemOptionDao.deleteByProblemId(problem.getId());
            // 选项添加
            if (!CollectionUtils.isEmpty(bo.getOptionList())) {
                for (AuthExamProblemOptionSaveBO option : bo.getOptionList()) {
                    saveOption(option, problem.getId());
                }
            }
        }

        if (dao.updateById(updateProblem) > 0) {
            return Result.success(1);
        }
        return Result.error("修改失败");
    }

    public Result<AuthExamProblemViewDTO> view(Long id) {
        ExamProblem problem = dao.getById(id);
        if (ObjectUtil.isNull(problem)) {
            return Result.error("获取不到该试题信息!");
        }
        AuthExamProblemViewDTO problemViewDTO = BeanUtil.copyProperties(problem, AuthExamProblemViewDTO.class);
        //选项集合
        List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(id);
        if (CollectionUtil.isNotEmpty(optionList)) {
            problemViewDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamProblemOptionViewDTO.class));
        }

        //组合题，需要拿出子试题结合
        if (ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
            List<ExamProblem> subProblemList = dao.listByParentId(problem.getId());
            List<AuthExamProblemViewDTO> subProblemViewList = BeanUtil.copyProperties(subProblemList, AuthExamProblemViewDTO.class);

            for (AuthExamProblemViewDTO subProblemViewDto : subProblemViewList) {
                if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(subProblemViewDto.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblemViewDto.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(subProblemViewDto.getProblemType())) {
                    //选项集合
                    List<ExamProblemOption> options = examProblemOptionDao.listByProblemIdWithBLOBs(subProblemViewDto.getId());
                    if (CollectionUtil.isNotEmpty(options)) {
                        subProblemViewDto.setOptionList(BeanUtil.copyProperties(options, AuthExamProblemOptionViewDTO.class));
                    }
                }
            }
            problemViewDTO.setItemList(subProblemViewList);
        }

        //  设置附属信息
        setAuthExamProblemViewAttach(problemViewDTO);

        return Result.success(problemViewDTO);
    }

    /**
     * 添加选项
     *
     * @param bo        选项保存对象
     * @param problemId 题目ID
     */
    private void saveOption(AuthExamProblemOptionSaveBO bo, Long problemId) {
        ExamProblemOption record = BeanUtil.copyProperties(bo, ExamProblemOption.class);
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setProblemId(problemId);
        examProblemOptionDao.save(record);
    }

    /**
     * 设置试题分页对象附加属性
     *
     * @param dto 试题
     */
    private void setAuthExamProblemPageAttach(AuthExamProblemPageDTO dto) {
        //填充一级 的分类信息
        if (dto.getLecturerUserNo() != null && dto.getLecturerUserNo() != 0) {
            // 查看讲师
            LecturerVO lecturer = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            if (!ObjectUtil.isNull(lecturer) && StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                dto.setLecturerName(lecturer.getLecturerName());
            }
        }

        // 获取科目
        if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSubjectName(examCategory.getCategoryName());
            }
            // 科目名称(父一级)
            if (ObjectUtil.isNotNull(examCategory) && examCategory.getFloor().equals(2)) {
                //第二级则查出第一级
                ExamCategory parent = examCategoryDao.getById(examCategory.getParentId());
                dto.setSubjectParentId(parent.getId());
                dto.setSubjectParentName(parent.getCategoryName());
            }
        }
        // 考点
        if (dto.getEmphasisId() != null && dto.getEmphasisId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getEmphasisId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setAnalysis(examCategory.getCategoryName());
            }
        }
        // 年份
        if (dto.getYearId() != null && dto.getYearId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getYearId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setYear(examCategory.getCategoryName());
            }
        }

        // 来源
        if (dto.getSourceId() != null && dto.getSourceId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSourceId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSourceView(examCategory.getCategoryName());
            }
        }
        // 难度
        if (dto.getDifficultyId() != null && dto.getDifficultyId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getDifficultyId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setDifficulty(examCategory.getCategoryName());
            }
        }
        // 题类
        if (dto.getTopicId() != null && dto.getTopicId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getTopicId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setTopicName(examCategory.getCategoryName());
            }
        }
        // 讲师分类
        if (dto.getPersonalId() != null && dto.getPersonalId() != 0) {
            UserExamCategory examCategory = userExamCategoryDao.getById(dto.getPersonalId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setPersonalName(examCategory.getCategoryName());
            }
        }
    }

    /**
     * 设置试题详情附属信息
     *
     * @param dto 试题
     */
    private void setAuthExamProblemViewAttach(AuthExamProblemViewDTO dto) {
        if (dto.getGradeId() != null && dto.getGradeId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getGradeId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setGradeName(examCategory.getCategoryName());
            }
        }
        if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSubjectName(examCategory.getCategoryName());
            }
        }
        if (dto.getEmphasisId() != null && dto.getEmphasisId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getEmphasisId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setEmphasis(examCategory.getCategoryName());
            }
        }
        if (dto.getYearId() != null && dto.getYearId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getYearId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setYear(examCategory.getCategoryName());
            }
        }
        if (dto.getSourceId() != null && dto.getSourceId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSourceId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSourceView(examCategory.getCategoryName());
            }
        }
        if (dto.getDifficultyId() != null && dto.getDifficultyId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getDifficultyId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setDifficulty(examCategory.getCategoryName());
            }
        }
        if (dto.getTopicId() != null && dto.getTopicId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getTopicId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setTopicName(examCategory.getCategoryName());
            }
        }
    }

    /**
     * 设置分页对象的分类属性
     *
     * @param dto        分页返回对象
     * @param subjectMap 科目缓存对象
     */
    private void setPageCategory(AuthExamProblemPageDTO dto, Map<Long, ExamCategory> subjectMap) {
        //当前层
        ExamCategory subjectCategory = subjectMap.get(dto.getSubjectId());
        if (ObjectUtil.isNull(subjectCategory)) {
            subjectCategory = examCategoryDao.getById(dto.getSubjectId());
        }
        if (ObjectUtil.isNull(subjectCategory)) {
            return;
        }
        dto.setSubjectName(subjectCategory.getCategoryName());
        subjectMap.put(subjectCategory.getId(), subjectCategory);

        // 父级
        if (ObjectUtil.isNull(subjectCategory.getParentId()) || subjectCategory.getParentId() == 0) {
            return;
        }
        ExamCategory parentCategory = subjectMap.get(subjectCategory.getParentId());
        if (ObjectUtil.isNull(parentCategory)) {
            parentCategory = examCategoryDao.getById(subjectCategory.getParentId());
        }
        if (ObjectUtil.isNull(parentCategory)) {
            return;
        }
        dto.setSubjectParentName(parentCategory.getCategoryName());
        dto.setSubjectParentId(parentCategory.getId());
        subjectMap.put(parentCategory.getId(), parentCategory);

        // 祖父级
        if (ObjectUtil.isNull(parentCategory.getParentId()) || parentCategory.getParentId() == 0) {
            return;
        }
        ExamCategory grandParentCategory = subjectMap.get(parentCategory.getParentId());
        if (ObjectUtil.isNull(grandParentCategory)) {
            grandParentCategory = examCategoryDao.getById(parentCategory.getParentId());
        }
        if (ObjectUtil.isNotNull(grandParentCategory)) {
            dto.setGrantSubjectName(grandParentCategory.getCategoryName());
            dto.setGrantSubjectId(grandParentCategory.getId());
            subjectMap.put(grandParentCategory.getId(), grandParentCategory);
        }
    }


    /**
     * 批量导入试题
     *
     * @param
     * @param
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> uploadExcel(MultipartFile file) {
        XSSFWorkbook wookbook = null;
        try {
            wookbook = new XSSFWorkbook(file.getInputStream());
            for (int sheetI = 0; sheetI < 5; sheetI++) {
                XSSFSheet sheet = wookbook.getSheetAt(sheetI);
                // 		指的行数，一共有多少行+
                int rows = sheet.getLastRowNum();
                Long lecturerUserNo = null;
                for (int i = 0; i <= rows; i++) {
                    if (i == 1) {
                        // 读取左上端单元格
                        XSSFRow row = sheet.getRow(i);
                        // 行不为空
                        String mobile = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 手机号
                        if (StringUtils.isEmpty(mobile)) {
                            continue;
                        }
                        LecturerVO lecturer = feignLecturer.getByLecturerMobile(mobile);
                        logger.debug("讲师信息, {}", lecturer);
                        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                            continue;
                        }
                        lecturerUserNo = lecturer.getLecturerUserNo();
                    }
                    logger.debug("讲师编号, {}", lecturerUserNo);
                    if (i >= 3) {
                        if (lecturerUserNo == null) {
                            continue;
                        }

                        // 读取左上端单元格
                        XSSFRow row = sheet.getRow(i);
                        // 行不为空
                        if (row != null) {
                            // 获取到Excel文件中的所有的列
                            // **读取cell**
                            // 试题内容

                            ExamProblem problem = problemSave(row, sheetI);
                            if (problem == null) {
                                continue;
                            }
                            //保存试题
                            problem.setLecturerUserNo(lecturerUserNo);
                            problem.setIsFree(IsFreeEnum.FREE.getCode());
                            problem.setParentId(0L);
                            problem.setId(IdWorker.getId());

                            problem = optionSave(problem, row);
                            dao.save(problem);
                        }
                    }
                }
            }
            return Result.success("导入成功");
        } catch (IOException e) {
            logger.error("导入失败, {}", e);
            return Result.error("导入失败");
        }
    }

    // 获取试题信息
    private ExamProblem problemSave(XSSFRow row, int sheetI) {
        ExamProblem problem = new ExamProblem();
        String problemContent = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 试题内容
        if (StringUtils.isEmpty(problemContent)) {
            return null;
        }
        problem.setProblemContent(problemContent);

        // 题类
        problem.setProblemType(sheetI + 1);

        //分数
        String score = ExcelToolUtil.getXValue(row.getCell(1)).trim();// 分数
        if (StringUtils.isEmpty(score)) {
            return null;
        }
        problem.setScore(Integer.valueOf(score));

        // 试题年份
        String year = ExcelToolUtil.getXValue(row.getCell(2)).trim();
        if (StringUtils.hasText(year)) {
            ExamCategory yearCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.YEAR.getCode(), null, year);
            if (ObjectUtil.isNull(yearCategory)) {
                yearCategory = new ExamCategory();
                yearCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                yearCategory.setExamCategoryType(ExamCategoryTypeEnum.YEAR.getCode());
                yearCategory.setCategoryName(year);
                yearCategory.setId(IdWorker.getId());
                examCategoryDao.save(yearCategory);
            }
            problem.setYearId(yearCategory.getId());
        }


        // 试题地区
        String region = ExcelToolUtil.getXValue(row.getCell(3)).trim();
        ;
        if (StringUtils.hasText(region)) {
            problem.setRegion(region);
        }


        // 试题年级
        String gra = ExcelToolUtil.getXValue(row.getCell(4)).trim();// 试题年级
        if (StringUtils.hasText(gra)) {
            ExamCategory graCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.SUBJECT.getCode(), null, gra);
            if (ObjectUtil.isNull(graCategory)) {
                graCategory = new ExamCategory();
                graCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                graCategory.setExamCategoryType(ExamCategoryTypeEnum.SUBJECT.getCode());
                graCategory.setCategoryName(gra);
                graCategory.setId(IdWorker.getId());
                examCategoryDao.save(graCategory);
            }
            problem.setGraId(graCategory.getId());
            // 试题科目
            String subject = ExcelToolUtil.getXValue(row.getCell(5)).trim();// 试题科目
            if (StringUtils.hasText(subject)) {
                ExamCategory examCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(null, null, graCategory.getId(), subject);
                if (StringUtils.isEmpty(examCategory)) {
                    examCategory = new ExamCategory();
                    examCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                    examCategory.setExamCategoryType(ExamCategoryTypeEnum.SUBJECT.getCode());
                    examCategory.setCategoryName(subject);
                    examCategory.setParentId(graCategory.getId());
                    examCategory.setId(IdWorker.getId());
                    examCategoryDao.save(examCategory);
                }
                problem.setSubjectId(examCategory.getId());
            }
        }

        // 试题难度
        String level = ExcelToolUtil.getXValue(row.getCell(6)).trim();// 试题难度
        if (StringUtils.hasText(level)) {
            ExamCategory levelCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.DIFFICULTY.getCode(), null, level);
            if (ObjectUtil.isNull(levelCategory)) {
                levelCategory = new ExamCategory();
                levelCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                levelCategory.setExamCategoryType(ExamCategoryTypeEnum.DIFFICULTY.getCode());
                levelCategory.setCategoryName(level);
                levelCategory.setId(IdWorker.getId());
                examCategoryDao.save(levelCategory);
            }
            problem.setDifficultyId(levelCategory.getId());
        }

        // 试题解答
        String analysis = ExcelToolUtil.getXValue(row.getCell(7)).trim();// 试题解答
        if (StringUtils.hasText(analysis)) {
            problem.setAnalysis(analysis);
        }

        //试题考点
        String emphasis = ExcelToolUtil.getXValue(row.getCell(8)).trim();// 试题考点
        if (StringUtils.hasText(emphasis)) {
            ExamCategory emphasisCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.EMPHASIS.getCode(), null, emphasis);
            if (ObjectUtil.isNull(emphasisCategory)) {
                emphasisCategory = new ExamCategory();
                emphasisCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                emphasisCategory.setExamCategoryType(ExamCategoryTypeEnum.EMPHASIS.getCode());
                emphasisCategory.setCategoryName(emphasis);
                emphasisCategory.setId(IdWorker.getId());
                examCategoryDao.save(emphasisCategory);
            }
            problem.setEmphasisId(emphasisCategory.getId());
            problem.setEmphasis(emphasis);
        }


        // 试题类
        String topic = ExcelToolUtil.getXValue(row.getCell(9)).trim();// 试题类
        if (StringUtils.hasText(topic)) {
            ExamCategory classificationCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.CLASSIFICATION.getCode(), null, topic);
            if (ObjectUtil.isNull(classificationCategory)) {
                classificationCategory = new ExamCategory();
                classificationCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                classificationCategory.setExamCategoryType(ExamCategoryTypeEnum.CLASSIFICATION.getCode());
                classificationCategory.setCategoryName(topic);
                classificationCategory.setId(IdWorker.getId());
                examCategoryDao.save(classificationCategory);
            }
            problem.setTopicId(classificationCategory.getId());
        }

        // 试题来源
        String source = ExcelToolUtil.getXValue(row.getCell(10)).trim();// 试题来源
        if (StringUtils.hasText(source)) {
            ExamCategory sourceCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.SOURCE.getCode(), null, source);
            if (ObjectUtil.isNull(sourceCategory)) {
                sourceCategory = new ExamCategory();
                sourceCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                sourceCategory.setExamCategoryType(ExamCategoryTypeEnum.SOURCE.getCode());
                sourceCategory.setCategoryName(source);
                sourceCategory.setId(IdWorker.getId());
                examCategoryDao.save(sourceCategory);
            }
            problem.setSourceId(sourceCategory.getId());
        }

        return problem;
    }


    // 选项添加
    private ExamProblem optionSave(ExamProblem problem, XSSFRow row) {
        // 单选题、多选题、判断题
        if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.GAP_FILLING.getCode().equals(problem.getProblemType())) {
            // 选项A
            String optionA = ExcelToolUtil.getXValue(row.getCell(11)).trim(); // A
            if (StringUtils.hasText(optionA)) {
                ExamProblemOption recordOptionA = new ExamProblemOption();
                recordOptionA.setProblemId(problem.getId());
                recordOptionA.setSort(1);
                recordOptionA.setOptionContent(optionA);
                examProblemOptionDao.save(recordOptionA);
            }
            // 选项B
            String optionB = ExcelToolUtil.getXValue(row.getCell(12)).trim(); // B
            if (StringUtils.hasText(optionB)) {
                ExamProblemOption recordOptionB = new ExamProblemOption();
                recordOptionB.setProblemId(problem.getId());
                recordOptionB.setSort(2);
                recordOptionB.setOptionContent(optionB);
                examProblemOptionDao.save(recordOptionB);
            }
            // 选项C
            String optionC = ExcelToolUtil.getXValue(row.getCell(13)).trim(); // C
            if (StringUtils.hasText(optionC)) {
                ExamProblemOption recordOptionC = new ExamProblemOption();
                recordOptionC.setProblemId(problem.getId());
                recordOptionC.setSort(3);
                recordOptionC.setOptionContent(optionC);
                examProblemOptionDao.save(recordOptionC);
            }
            // 选项D
            String optionD = ExcelToolUtil.getXValue(row.getCell(14)).trim(); // D
            if (StringUtils.hasText(optionD)) {
                ExamProblemOption recordOptionD = new ExamProblemOption();
                recordOptionD.setProblemId(problem.getId());
                recordOptionD.setSort(4);
                recordOptionD.setOptionContent(optionD);
                examProblemOptionDao.save(recordOptionD);
            }
            // 选项E
            String optionE = ExcelToolUtil.getXValue(row.getCell(15)).trim(); // E
            if (StringUtils.hasText(optionE)) {
                ExamProblemOption recordOptionE = new ExamProblemOption();
                recordOptionE.setProblemId(problem.getId());
                recordOptionE.setSort(5);
                recordOptionE.setOptionContent(optionE);
                examProblemOptionDao.save(recordOptionE);
            }
            // 选项F
            String optionF = ExcelToolUtil.getXValue(row.getCell(16)).trim(); // F
            if (StringUtils.hasText(optionF)) {
                ExamProblemOption recordOptionF = new ExamProblemOption();
                recordOptionF.setProblemId(problem.getId());
                recordOptionF.setSort(6);
                recordOptionF.setOptionContent(optionF);
                examProblemOptionDao.save(recordOptionF);
            }
            // 选项G
            String optionG = ExcelToolUtil.getXValue(row.getCell(17)).trim(); // G
            if (StringUtils.hasText(optionG)) {
                ExamProblemOption recordOptionG = new ExamProblemOption();
                recordOptionG.setProblemId(problem.getId());
                recordOptionG.setSort(7);
                recordOptionG.setOptionContent(optionG);
                examProblemOptionDao.save(recordOptionG);
            }
            // 选项H
            String optionH = ExcelToolUtil.getXValue(row.getCell(18)).trim(); // H
            if (StringUtils.hasText(optionH)) {
                ExamProblemOption recordOptionH = new ExamProblemOption();
                recordOptionH.setProblemId(problem.getId());
                recordOptionH.setSort(8);
                recordOptionH.setOptionContent(optionH);
                examProblemOptionDao.save(recordOptionH);
            }
            // 选项H
            String isAsr = ExcelToolUtil.getXValue(row.getCell(19)).trim();// 正确答案
            String[] ids = isAsr.split(",");
            String problemAnswer = "";
            for (String answer : ids) {
                if (answer.equals("A") && StringUtils.hasText(optionA)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionA;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionA;
                    }
                }
                if (answer.equals("B") && StringUtils.hasText(optionB)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionB;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionB;
                    }
                }
                if (answer.equals("C") && StringUtils.hasText(optionC)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionC;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionC;
                    }
                }
                if (answer.equals("D") && StringUtils.hasText(optionD)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionD;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionD;
                    }
                }
                if (answer.equals("E") && StringUtils.hasText(optionE)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionE;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionE;
                    }
                }
                if (answer.equals("F") && StringUtils.hasText(optionF)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionF;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionF;
                    }
                }
                if (answer.equals("G") && StringUtils.hasText(optionG)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionG;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionG;
                    }
                }
                if (answer.equals("H") && StringUtils.hasText(optionH)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionH;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionH;
                    }
                }
            }
            problem.setProblemAnswer(problemAnswer);
        }
        // 判断题
        if (ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
            // 选项A
            String optionA = ExcelToolUtil.getXValue(row.getCell(11)).trim();
            ExamProblemOption recordOptionH = new ExamProblemOption();
            recordOptionH.setProblemId(problem.getId());
            recordOptionH.setSort(1);
            recordOptionH.setOptionContent(optionA);
            examProblemOptionDao.save(recordOptionH);

            // 选项B
            String optionB = ExcelToolUtil.getXValue(row.getCell(12)).trim();
            ExamProblemOption recordOptionB = new ExamProblemOption();
            recordOptionB.setProblemId(problem.getId());
            recordOptionB.setSort(2);
            recordOptionB.setOptionContent(optionB);
            examProblemOptionDao.save(recordOptionB);

            // 正确答案
            String isAsr = ExcelToolUtil.getXValue(row.getCell(13)).trim();// 正确答案
            if (StringUtils.isEmpty(isAsr)) {
                return problem;
            }
            if (isAsr.equals("A")) {
                problem.setProblemAnswer(optionA);
            }
            if (isAsr.equals("B")) {
                problem.setProblemAnswer(optionB);
            }
            return problem;
        }
        // 解答题
        if (ProblemTypeEnum.SHORT_ANSWER.getCode().equals(problem.getProblemType())) {
            String answer = ExcelToolUtil.getXValue(row.getCell(11)).trim();  // 标准答案
            if (StringUtils.isEmpty(answer)) {
                return problem;
            }
            problem.setProblemAnswer(answer);
        }
        return problem;
    }

}
