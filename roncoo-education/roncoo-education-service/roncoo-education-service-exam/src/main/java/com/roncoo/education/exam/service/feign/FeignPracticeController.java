package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignPractice;
import com.roncoo.education.exam.feign.qo.PracticeQO;
import com.roncoo.education.exam.feign.vo.PracticeVO;
import com.roncoo.education.exam.service.feign.biz.FeignPracticeBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 练习信息
 *
 * @author LHR
 * @date 2020-10-10
 */
@RestController
public class FeignPracticeController extends BaseController implements IFeignPractice{

    @Autowired
    private FeignPracticeBiz biz;

	@Override
	public Page<PracticeVO> listForPage(@RequestBody PracticeQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody PracticeQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody PracticeQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public PracticeVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
