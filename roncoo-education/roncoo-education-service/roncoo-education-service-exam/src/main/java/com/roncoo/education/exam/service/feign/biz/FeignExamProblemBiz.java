package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.feign.qo.ExamProblemQO;
import com.roncoo.education.exam.feign.vo.ExamProblemVO;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目
 *
 * @author wujing
 */
@Component
public class FeignExamProblemBiz extends BaseBiz {

    @Autowired
    private ExamProblemDao dao;

	public Page<ExamProblemVO> listForPage(ExamProblemQO qo) {
	    ExamProblemExample example = new ExamProblemExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamProblem> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamProblemVO.class);
	}

	public int save(ExamProblemQO qo) {
		ExamProblem record = BeanUtil.copyProperties(qo, ExamProblem.class);
		record.setId(IdWorker.getId());
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamProblemVO getById(Long id) {
		ExamProblem record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamProblemVO.class);
	}

	public int updateById(ExamProblemQO qo) {
		ExamProblem record = BeanUtil.copyProperties(qo, ExamProblem.class);
		return dao.updateById(record);
	}

	public int updateByVideoId(ExamProblemQO examProblemQO) {
		ExamProblemExample example = new ExamProblemExample();
		Criteria c = example.createCriteria();
		c.andVideoIdEqualTo(examProblemQO.getVideoId());
		return dao.updateByVideoId(BeanUtil.copyProperties(examProblemQO, ExamProblem.class));
	}

	public int updateByVid(ExamProblemQO examProblemQO) {
		ExamProblemExample example = new ExamProblemExample();
		Criteria c = example.createCriteria();
		c.andVideoVidEqualTo(examProblemQO.getVideoVid());
		return dao.updateByVideoId(BeanUtil.copyProperties(examProblemQO, ExamProblem.class));
	}
}
