package com.roncoo.education.exam.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.ExamCategoryPageBO;
import com.roncoo.education.exam.common.dto.ExamCategoryListDTO;
import com.roncoo.education.exam.service.api.biz.ApiExamCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷分类 Api接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@Api(tags = "API-试卷分类管理")
@RestController
@RequestMapping("/exam/api/exam/category")
public class ApiExamCategoryController {

    @Autowired
    private ApiExamCategoryBiz biz;

    @ApiOperation(value = "考试分类列表接口", notes = "考试分类列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<ExamCategoryListDTO> list(@RequestBody ExamCategoryPageBO bo) {
        return biz.list(bo);
    }

}
