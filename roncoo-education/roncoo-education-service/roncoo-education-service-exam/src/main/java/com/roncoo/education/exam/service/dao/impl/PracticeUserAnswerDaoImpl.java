package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.PracticeUserAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.PracticeUserAnswerMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户考试答案 服务实现类
 *
 * @author LHR
 * @date 2020-10-10
 */
@Repository
public class PracticeUserAnswerDaoImpl implements PracticeUserAnswerDao {

    @Autowired
    private PracticeUserAnswerMapper mapper;

    @Override
    public int save(PracticeUserAnswer record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(PracticeUserAnswer record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public PracticeUserAnswer getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<PracticeUserAnswer> listForPage(int pageCurrent, int pageSize, PracticeUserAnswerExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<PracticeUserAnswer> listByUserNoAndPracticeId(Long userNo, Long practiceId) {
        PracticeUserAnswerExample example = new PracticeUserAnswerExample();
        PracticeUserAnswerExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andPracticeIdEqualTo(practiceId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public Integer deleteByUserNoAndPracticeId(Long userNo, Long practiceId) {
        PracticeUserAnswerExample example = new PracticeUserAnswerExample();
        PracticeUserAnswerExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andPracticeIdEqualTo(practiceId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public PracticeUserAnswer getByUserNoAndPracticeIdAndProblemId(Long userNo, Long practiceId, Long problemId) {
        PracticeUserAnswerExample example = new PracticeUserAnswerExample();
        PracticeUserAnswerExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andPracticeIdEqualTo(practiceId);
        c.andProblemIdEqualTo(problemId);
        List<PracticeUserAnswer> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }


}
