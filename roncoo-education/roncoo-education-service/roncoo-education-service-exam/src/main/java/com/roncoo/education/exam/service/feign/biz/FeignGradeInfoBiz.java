package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeInfoQO;
import com.roncoo.education.exam.feign.vo.GradeInfoVO;
import com.roncoo.education.exam.service.dao.GradeInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeInfoBiz extends BaseBiz {

    @Autowired
    private GradeInfoDao dao;

	public Page<GradeInfoVO> listForPage(GradeInfoQO qo) {
	    GradeInfoExample example = new GradeInfoExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeInfo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeInfoVO.class);
	}

	public int save(GradeInfoQO qo) {
		GradeInfo record = BeanUtil.copyProperties(qo, GradeInfo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeInfoVO getById(Long id) {
		GradeInfo record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeInfoVO.class);
	}

	public int updateById(GradeInfoQO qo) {
		GradeInfo record = BeanUtil.copyProperties(qo, GradeInfo.class);
		return dao.updateById(record);
	}

}
