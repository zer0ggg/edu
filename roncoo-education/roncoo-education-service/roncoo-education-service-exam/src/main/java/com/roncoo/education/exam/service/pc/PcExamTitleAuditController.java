package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamTitleAuditEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleAuditListREQ;
import com.roncoo.education.exam.common.req.ExamTitleAuditSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleAuditViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamTitleAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷标题审核 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷标题审核管理")
@RestController
@RequestMapping("/exam/pc/exam/title/audit")
public class PcExamTitleAuditController {

    @Autowired
    private PcExamTitleAuditBiz biz;

    @ApiOperation(value = "试卷标题审核列表", notes = "试卷标题审核列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamTitleAuditListRESP>> list(@RequestBody ExamTitleAuditListREQ examTitleAuditListREQ) {
        return biz.list(examTitleAuditListREQ);
    }

    @ApiOperation(value = "试卷标题审核添加", notes = "试卷标题审核添加")
    @SysLog(value = "试卷标题审核添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamTitleAuditSaveREQ examTitleAuditSaveREQ) {
        return biz.save(examTitleAuditSaveREQ);
    }

    @ApiOperation(value = "试卷标题审核查看", notes = "试卷标题审核查看")
    @GetMapping(value = "/view")
    public Result<ExamTitleAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷标题审核修改", notes = "试卷标题审核修改")
    @SysLog(value = "试卷标题审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamTitleAuditEditREQ examTitleAuditEditREQ) {
        return biz.edit(examTitleAuditEditREQ);
    }

    @ApiOperation(value = "试卷标题审核删除", notes = "试卷标题审核删除")
    @SysLog(value = "试卷标题审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
