package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 申请加入班级审核分页对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeApplyRecordPageBO", description = "申请加入班级审核分页对象")
public class AuthGradeApplyRecordPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = -1450921553475079492L;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "审核状态(1:待审核，2:同意，3:拒绝)")
    private Integer auditStatus;
}
