package com.roncoo.education.exam.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 练习信息
 * </p>
 *
 * @author LHR
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PracticeRESP", description="练习信息")
public class PracticeRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "难度id")
    private Long difficultyId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "试题数量")
    private Integer problemQuantity;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "题类id")
    private Long topicId;

    @ApiModelProperty(value = "年级id")
    private Long graId;

    @ApiModelProperty(value = "考点id")
    private Long emphasisId;

    @ApiModelProperty(value = "名称")
    private String name;
}
