package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.GradeInfoEditREQ;
import com.roncoo.education.exam.common.req.GradeInfoPageREQ;
import com.roncoo.education.exam.common.req.GradeInfoSaveREQ;
import com.roncoo.education.exam.common.resp.GradeInfoListRESP;
import com.roncoo.education.exam.common.resp.GradeInfoViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-班级信息管理")
@RestController
@RequestMapping("/exam/pc/grade/info")
public class PcGradeInfoController {

    @Autowired
    private PcGradeInfoBiz biz;

    @ApiOperation(value = "班级信息列表", notes = "班级信息列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeInfoListRESP>> list(@RequestBody GradeInfoPageREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级信息添加", notes = "班级信息添加")
    @SysLog(value = "班级信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid GradeInfoSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "班级信息查看", notes = "班级信息查看")
    @GetMapping(value = "/view")
    public Result<GradeInfoViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "班级信息修改", notes = "班级信息修改")
    @SysLog(value = "班级信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid GradeInfoEditREQ req) {
        return biz.edit(req);
    }

    @ApiOperation(value = "修改状态", notes = "修改状态")
    @SysLog(value = "班级信息修改状态")
    @PutMapping(value = "update/status")
    public Result<String> updateStatus(@RequestParam Long id) {
        return biz.updateStatus(id);
    }

    @ApiOperation(value = "班级信息删除", notes = "班级信息删除")
    @SysLog(value = "班级信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
