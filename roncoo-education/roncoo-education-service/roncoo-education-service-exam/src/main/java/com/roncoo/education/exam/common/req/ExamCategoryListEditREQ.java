package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 试卷分类
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamCategoryListEditREQ", description="试卷分类")
public class ExamCategoryListEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类类型(1:试题；2：试卷）不能为空")
    @ApiModelProperty(value = "分类类型(1:试题；2：试卷）",required = true)
    private Integer categoryType;


    @NotNull(message = "分类类型(1:试卷科目,2:试卷年份,3：试卷来源,4：试题难度,5：试题类型)不能为空")
    @ApiModelProperty(value = "分类类型(1:试卷科目,2:试卷年份,3：试卷来源,4：试题难度,5：试题类型)",required = true)
    private Integer examCategoryType;

}
