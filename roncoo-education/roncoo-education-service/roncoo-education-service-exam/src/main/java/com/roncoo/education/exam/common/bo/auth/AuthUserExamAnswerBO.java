package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 试卷ID
     */
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
    /**
     * 标题id
     */
    @ApiModelProperty(value = "标题ID")
    private Long titleId;
    /**
     * 题目ID
     */
    @ApiModelProperty(value = "题目ID")
    private Long subjectId;
    /**
     * 题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:材料题)
     */
    @ApiModelProperty(value = "题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:材料题)")
    private Integer subjectType;
    /**
     * 是否正确(0:错误,1:正确)
     */
    @ApiModelProperty(value = "是否正确(0:错误,1:正确)")
    private Integer isRight;
    /**
     * 回答内容
     */
    @ApiModelProperty(value = "回答内容")
    private String answerContent;

}
