package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerLecturerAuditBO;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamAndAnswerDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeStudentExamAnswerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级学生考试答案管理")
@RestController
@RequestMapping("/exam/auth/grade/student/exam/answer")
public class AuthGradeStudentExamAnswerController {

    @Autowired
    private AuthGradeStudentExamAnswerBiz biz;

    @ApiOperation(value = "讲师阅卷（主观题）", notes = "讲师阅卷（主观题）")
    @PostMapping(value = "/sys/audit")
    public Result<String> lecturerAudit(@RequestBody @Valid AuthGradeStudentExamAnswerLecturerAuditBO bo) {
        return biz.lecturerAudit(bo);
    }

    @ApiOperation(value = "保存答案", notes = "保存答案")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AuthGradeStudentExamAnswerSaveBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "查看试卷答案", notes = "查看试卷答案")
    @PostMapping(value = "/view")
    public Result<AuthGradeExamAndAnswerDTO> view(@RequestBody @Valid AuthGradeStudentExamAnswerViewBO bo) {
        return biz.view(bo);
    }
}
