package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级申请记录提示列出请求对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeApplyRecordTipsListBO", description = "班级申请记录提示列出请求对象")
public class AuthGradeApplyRecordTipsListBO implements Serializable {

    private static final long serialVersionUID = -1450921553475079492L;

    @NotNull(message = "是否讲师不能为空")
    @ApiModelProperty(value = "是否讲师（true：只获取讲师，false：只获取管理员）", required = true)
    private Boolean isLecturer;

    @Min(value = 1, message = "显示记录数必须大于0")
    @Max(value = 20, message = "最多显示20条记录")
    @NotNull(message = "显示记录数")
    @ApiModelProperty(value = "显示记录数", required = true)
    private Integer num = 5;


}
