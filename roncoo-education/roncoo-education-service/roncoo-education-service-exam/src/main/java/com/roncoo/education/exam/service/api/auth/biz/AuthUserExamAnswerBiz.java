package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamAnswerViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnswerProblemDTO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnswerProblemOptionDTO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnswerTitleDTO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnswerViewDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@Component
public class AuthUserExamAnswerBiz extends BaseBiz {

    @Autowired
    private UserExamAnswerDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private UserExamRecordDao userExamRecordDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    /**
     * 获取用户考试答案
     *
     * @param viewBO 查看参数
     * @return 考试答案
     */
    public Result<AuthUserExamAnswerViewDTO> view(AuthUserExamAnswerViewBO viewBO) {
        // 判断是否考试完成
        UserExamRecord userExamRecord = userExamRecordDao.getById(viewBO.getRecordId());
        if (!ThreadContext.userNo().equals(userExamRecord.getUserNo())) {
            return Result.error("考试记录不存在");
        }
        if (!(ExamStatusEnum.FINISH.getCode().equals(userExamRecord.getExamStatus()) || ExamStatusEnum.COMPLETE.getCode().equals(userExamRecord.getExamStatus()))) {
            return Result.error("用户尚未考试完成");
        }

        // 判断试卷是否存在
        ExamInfo examInfo = examInfoDao.getById(userExamRecord.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("试卷不存在");
        }

        AuthUserExamAnswerViewDTO viewDTO = new AuthUserExamAnswerViewDTO();
        viewDTO.setExamId(examInfo.getId());
        viewDTO.setExamName(examInfo.getExamName());

        // 获取试卷标题集合
        List<ExamTitle> examTitleList = examTitleDao.listByExamIdAndStatusId(examInfo.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(examTitleList)) {
            return Result.success(viewDTO);
        }

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = dao.listByRecordId(viewBO.getRecordId());
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }

        //获取试卷标题
        List<AuthUserExamAnswerTitleDTO> examAnswerTitleList = new ArrayList<>();
        for (ExamTitle examTitle : examTitleList) {
            AuthUserExamAnswerTitleDTO titleDTO = BeanUtil.copyProperties(examTitle, AuthUserExamAnswerTitleDTO.class);
            titleDTO.setProblemList(listProblemByTitle(examTitle, userExamAnswerList));
            examAnswerTitleList.add(titleDTO);
        }
        viewDTO.setTitleList(examAnswerTitleList);

        return Result.success(viewDTO);
    }

    /**
     * 根据标题获取小题集合
     *
     * @param examTitle 试卷标题
     * @return 小题集合
     */
    private List<AuthUserExamAnswerProblemDTO> listProblemByTitle(ExamTitle examTitle, List<UserExamAnswer> userExamAnswerList) {
        List<ExamTitleProblemRef> problemRefList = examTitleProblemRefDao.listByTitleId(examTitle.getId());
        if (CollectionUtil.isEmpty(problemRefList)) {
            return null;
        }

        // 获取标题下小题
        List<Long> problemIdList = problemRefList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return null;
        }
        // 获取用户答案
        List<UserExamAnswer> userExamTitleAnswerList = userExamAnswerList.stream().filter(userExamAnswer -> examTitle.getId().equals(userExamAnswer.getTitleId())).collect(Collectors.toList());

        // 转Map
        Map<Long, Integer> problemScoreMap = problemRefList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, ExamTitleProblemRef::getScore));
        // 获取题目答案集合
        Map<Long, UserExamAnswer> problemUserAnswerMap = userExamTitleAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));


        if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
            // 获取组合题
            return getMaterialProblemAndAnswer(problemList, problemScoreMap, problemUserAnswerMap);
        } else {
            // 其他类型题目 获取用户答案
            List<AuthUserExamAnswerProblemDTO> answerProblemDTOList = new ArrayList<>();
            for (ExamProblem problem : problemList) {
                AuthUserExamAnswerProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthUserExamAnswerProblemDTO.class);

                //设置分值
                problemDTO.setScore(problemScoreMap.get(problem.getId()));

                // 获取题目用户答案
                UserExamAnswer userExamAnswer = problemUserAnswerMap.get(problem.getId());
                if (ObjectUtil.isNotNull(userExamAnswer)) {
                    problemDTO.setUserAnswer(userExamAnswer.getUserAnswer());
                    problemDTO.setUserScore(userExamAnswer.getScore());

                    if (ExamAnswerStatusEnum.WAIT.getCode().equals(userExamAnswer.getAnswerStatus())) {
                        problemDTO.setProblemStatus(ExamProblemStatusEnum.WAIT.getCode());
                    } else {
                        problemDTO.setProblemStatus(userExamAnswer.getScore() > 0 ? ExamProblemStatusEnum.YES.getCode() : ExamProblemStatusEnum.NO.getCode());
                    }
                } else {
                    // 用户答案不存在，设置为错误
                    problemDTO.setUserScore(0);
                    problemDTO.setProblemStatus(ExamProblemStatusEnum.NO.getCode());
                }

                // 判断是否需要获取选项
                if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitle.getTitleType())) {
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId());
                    problemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthUserExamAnswerProblemOptionDTO.class));
                }
                answerProblemDTOList.add(problemDTO);
            }
            return answerProblemDTOList;
        }
    }

    private List<AuthUserExamAnswerProblemDTO> getMaterialProblemAndAnswer(List<ExamProblem> problemList, Map<Long, Integer> problemScoreMap, Map<Long, UserExamAnswer> problemUserAnswerMap) {
        List<AuthUserExamAnswerProblemDTO> answerProblemDTOList = new ArrayList<>();

        List<ExamProblem> parentProblemList = problemList.stream().filter(problem -> ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())).collect(Collectors.toList());


        for (ExamProblem problem : parentProblemList) {
            AuthUserExamAnswerProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthUserExamAnswerProblemDTO.class);

            //设置分值
            problemDTO.setScore(problemScoreMap.get(problem.getId()));
            // 统计组合题得分
            int problemUserScore = 0;
            int problemStatus = ExamProblemStatusEnum.WAIT.getCode();


            // 获取组合题下小题
            List<AuthUserExamAnswerProblemDTO> childrenList = new ArrayList<>();
            List<ExamProblem> subProblemList = problemList.stream().filter(subProblem -> problem.getId().equals(subProblem.getParentId())).collect(Collectors.toList());
            for (ExamProblem subProblem : subProblemList) {
                AuthUserExamAnswerProblemDTO subProblemDTO = BeanUtil.copyProperties(subProblem, AuthUserExamAnswerProblemDTO.class);

                // 设置分值
                subProblemDTO.setScore(problemScoreMap.get(subProblem.getId()));

                //获取用户答案
                UserExamAnswer userExamAnswer = problemUserAnswerMap.get(subProblem.getId());
                if (ObjectUtil.isNotNull(userExamAnswer)) {
                    subProblemDTO.setUserAnswer(userExamAnswer.getUserAnswer());
                    subProblemDTO.setUserScore(userExamAnswer.getScore());
                    problemUserScore += userExamAnswer.getScore();

                    if (ExamAnswerStatusEnum.WAIT.getCode().equals(userExamAnswer.getAnswerStatus())) {
                        subProblemDTO.setProblemStatus(ExamProblemStatusEnum.WAIT.getCode());
                    } else {
                        subProblemDTO.setProblemStatus(userExamAnswer.getScore() > 0 ? ExamProblemStatusEnum.YES.getCode() : ExamProblemStatusEnum.NO.getCode());
                    }
                } else {
                    // 用户答案不存在，设置为错误
                    subProblemDTO.setProblemStatus(ExamProblemStatusEnum.NO.getCode());
                    subProblemDTO.setUserScore(0);
                }

                if (!ExamProblemStatusEnum.NO.getCode().equals(problemStatus) && !subProblemDTO.getProblemStatus().equals(problemStatus)) {
                    problemStatus = subProblemDTO.getProblemStatus();
                }

                // 判断是否需要获取选项
                if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblem.getProblemType())) {
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(subProblem.getId());
                    subProblemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthUserExamAnswerProblemOptionDTO.class));
                }
                childrenList.add(subProblemDTO);
            }

            problemDTO.setProblemStatus(problemStatus);
            problemDTO.setChildrenList(childrenList);
            problemDTO.setUserScore(problemUserScore);

            answerProblemDTOList.add(problemDTO);
        }

        return answerProblemDTOList;
    }
}
