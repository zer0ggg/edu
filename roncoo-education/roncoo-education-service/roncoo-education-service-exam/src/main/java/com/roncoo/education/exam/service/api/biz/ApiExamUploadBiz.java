package com.roncoo.education.exam.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.exam.feign.interfaces.IFeignExamFileInfo;
import com.roncoo.education.exam.feign.qo.FileInfoQO;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.ExamVideoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@Component
public class ApiExamUploadBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignExamFileInfo feignFileInfo;

    @Autowired
    private ExamVideoDao examVideoDao;
    @Autowired
    private ExamProblemDao examProblemDao;

    /**
     * 试卷上传视频接口
     *
     * @param videoFile 视频文件
     * @return 上传结果
     */
    public Result<String> uploadVideo(MultipartFile videoFile) {
        // 视频上传
        if (videoFile == null || videoFile.isEmpty()) {
            return Result.error("请选择视频进行上传");
        }

        // 获取上传文件的原名
        String fileName = videoFile.getOriginalFilename();
        boolean fileStatus = true;
        List<String> fileTypes = Arrays.asList("avi", "mp4", "flv", "mpg", "mov", "asf", "3gp", "f4v", "wmv", "x-ms-wmv\n");
        for (String filetype : fileTypes) {
            // 上传文件的原名+小写+后缀
            if (fileName.toLowerCase().endsWith(filetype)) {
                fileStatus = false;
                break;
            }
        }
        if (fileStatus) {
            return Result.error("上传的视频类型不正确");
        }

        // 当作存储到本地的文件名，方便定时任务的处理
        Long videoId = IdWorker.getId();

        // 1、上传到本地
        File targetFile = new File(SystemUtil.VIDEO_PATH + videoId.toString() + "." + StrUtil.getSuffix(fileName));
        // targetFile.setLastModified(System.currentTimeMillis());
        // 判断文件目录是否存在，不存在就创建文件目录
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        try {
            videoFile.transferTo(targetFile);
        } catch (Exception e) {
            logger.error("上传到本地失败", e);
            return Result.error("上传文件出错，请重新上传");
        }

        // 新增试卷视频表信息
        ExamVideo examVideo = new ExamVideo();
        examVideo.setId(videoId);
        examVideo.setGmtCreate(null);
        examVideo.setGmtModified(null);
        examVideo.setVideoName(fileName);
        examVideo.setVideoStatus(VideoStatusEnum.WAIT.getCode());
        int result = examVideoDao.save(examVideo);

        if (result > 0) {
            CALLBACK_EXECUTOR.execute(() -> {
                ConfigPolyvVO sys = feignSysConfig.getPolyv();
                // 2、异步上传到保利威视
                // 获取系统配置信息
                String videoVId = PolyvUtil.sdkUploadFile(targetFile.getName(), fileName, SystemUtil.VIDEO_PATH, VideoTagEnum.EXAM.getCode(), fileName, sys.getPolyvUseid(), sys.getPolyvSecretkey());
                if (videoVId == null) {
                    // 上传异常，不再进行处理，定时任务会继续进行处理
                    return;
                }
                examVideo.setVideoVid(videoVId);
                examVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
                examVideoDao.updateById(examVideo);

                // 记录文件信息
                FileInfoQO fileInfoQO = new FileInfoQO();

                if (IsBackupEnum.YES.getCode().equals(feignSysConfig.getByConfigKey(SysConfigConstants.IS_BACKUP_ALIYUN).getConfigValue())) {
                    // 3、异步上传到阿里云
                    String videoOasId = AliyunUtil.uploadDoc(PlatformEnum.COURSE, targetFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
                    examVideo.setVideoOssId(videoOasId);
                    examVideoDao.updateById(examVideo);
                    fileInfoQO.setFileUrl(videoOasId);
                }

                List<ExamProblem> problemList = examProblemDao.listByVideoId(videoId);
                if (CollectionUtil.isNotEmpty(problemList)) {
                    for (ExamProblem problem : problemList) {
                        problem.setVideoVid(examVideo.getVideoVid());
                        examProblemDao.updateById(problem);
                    }
                }

                fileInfoQO.setFileName(examVideo.getVideoName());
                fileInfoQO.setFileType(FileTypeEnum.VIDEO.getCode());
                fileInfoQO.setFileSize(videoFile.getSize());
                feignFileInfo.save(fileInfoQO);

                // 4、成功删除本地文件
                if (targetFile.isFile() && targetFile.exists()) {
                    if (!targetFile.delete()) {
                        logger.error("删除本地文件失败");
                    }
                }
            });
        } else {
            return Result.error("系统异常，请重试");
        }
        return Result.success(String.valueOf(videoId));
    }
}
