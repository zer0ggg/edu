package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamAnalysisBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamPageBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoDTO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnalysisDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthUserExamBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户试卷 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-用户试卷管理")
@RestController
@RequestMapping("/exam/auth/user/exam")
public class AuthUserExamController {

    @Autowired
    private AuthUserExamBiz biz;

    @ApiOperation(value = "分页列出", notes = "分页列出")
    @PostMapping(value = "/list")
    public Result<Page<AuthExamInfoDTO>> list(@RequestBody AuthUserExamPageBO pageBO) {
        return biz.list(pageBO);
    }

    @ApiOperation(value = "用户试卷分析", notes = "用户试卷分析")
    @PostMapping(value = "/analysis")
    public Result<AuthUserExamAnalysisDTO> analysis(@RequestBody AuthUserExamAnalysisBO authUserExamAnalysisBO) {
        return biz.analysis(authUserExamAnalysisBO);
    }

}
