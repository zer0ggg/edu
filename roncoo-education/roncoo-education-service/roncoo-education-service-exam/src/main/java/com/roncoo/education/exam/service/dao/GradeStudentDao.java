package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample;

import java.util.List;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeStudentDao {

    /**
     * 保存
     *
     * @param record
     * @return 影响记录数
     */
    int save(GradeStudent record);

    /**
     * 根据ID删除
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record
     * @return 影响记录数
     */
    int updateById(GradeStudent record);

    /**
     * 根据ID获取
     *
     * @param id 主键ID
     * @return
     */
    GradeStudent getById(Long id);

    /**
     * --分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeStudent> listForPage(int pageCurrent, int pageSize, GradeStudentExample example);

    /**
     * 根据班级ID列出班级学生
     *
     * @param gradeId 班级ID
     * @return 班级学生列表
     */
    List<GradeStudent> listByGradeId(Long gradeId);

    /**
     * 根据班级ID删除班级学生
     *
     * @param gradeId 班级ID
     * @return 影响记录
     */
    int deleteByGradeId(Long gradeId);

    /**
     * 根据班级ID和用户编号获取班级学生信息
     *
     * @param gradeId 班级ID
     * @param userNo  用户编号
     * @return 班级学生信息
     */
    GradeStudent getByGradeIdAndUserNo(Long gradeId, Long userNo);

    /**
     * 根据用户编号和用户角色获取班级学生信息
     *
     * @param userNo   用户编号
     * @param userRole 用户角色
     * @return 班级学生信息
     */
    List<GradeStudent> listByUserNoAndUserRole(Long userNo, Integer userRole);

    /**
     * 根据用户编号、用户角色和状态获取班级学生信息
     * @param userNo
     * @param userRole
     * @param statusId
     * @return
     */
    List<GradeStudent> listByUserNoAndUserRoleAndStatusId(Long userNo, Integer userRole, Integer statusId);

    /**
     * 根据用户编号和状态获取班级学生信息
     * @param userNo
     * @param statusId
     * @return
     */
    List<GradeStudent> listByUserNoAndStatusId(Long userNo, Integer statusId);

    /**
     * 根据班级ID统计学生人数
     *
     * @param gradeId 班级ID
     * @return 学生人数
     */
    int countByGradeId(Long gradeId);

    /**
     * 根据班级ID和用户角色统计学生人数
     *
     * @param gradeId  班级ID
     * @param userRole 用户角色
     * @return 学生人数
     */
    int countByGradeIdAndUserRole(Long gradeId, Integer userRole);

    /**
     * 根据班级ID和用户角色 列出
     * @param gradeId
     * @param userRole
     * @return
     */
    List<GradeStudent> listByGradeIdAndUserRole(Long gradeId, Integer userRole);
    /**
     *  根据ID集合获取班级学生信息
     *
     * @param ids
     * @return
     */
    List<GradeStudent> listByIds(List<Long> ids);
}
