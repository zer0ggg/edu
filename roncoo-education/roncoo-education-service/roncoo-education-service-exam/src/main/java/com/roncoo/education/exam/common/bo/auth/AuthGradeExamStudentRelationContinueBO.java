package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级考试--继续考试
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationContinueBO", description = "班级考试--继续考试")
public class AuthGradeExamStudentRelationContinueBO implements Serializable {

    private static final long serialVersionUID = -1696613478798741640L;

    @NotNull(message = "考试ID不能为空")
    @ApiModelProperty(value = "考试ID", required = true)
    private Long id;
}
