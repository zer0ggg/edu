package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户试卷
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthUserExamPageBO", description = "用户试卷")
public class AuthUserExamPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;
}
