package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamRecordEditREQ;
import com.roncoo.education.exam.common.req.UserExamRecordListREQ;
import com.roncoo.education.exam.common.req.UserExamRecordSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamRecordListRESP;
import com.roncoo.education.exam.common.resp.UserExamRecordViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户考试记录 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户考试记录管理")
@RestController
@RequestMapping("/exam/pc/user/exam/record")
public class PcUserExamRecordController {

    @Autowired
    private PcUserExamRecordBiz biz;

    @ApiOperation(value = "用户考试记录列表", notes = "用户考试记录列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamRecordListRESP>> list(@RequestBody UserExamRecordListREQ userExamRecordListREQ) {
        return biz.list(userExamRecordListREQ);
    }

    @ApiOperation(value = "用户考试记录添加", notes = "用户考试记录添加")
    @SysLog(value = "用户考试记录添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamRecordSaveREQ userExamRecordSaveREQ) {
        return biz.save(userExamRecordSaveREQ);
    }

    @ApiOperation(value = "用户考试记录查看", notes = "用户考试记录查看")
    @GetMapping(value = "/view")
    public Result<UserExamRecordViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户考试记录修改", notes = "用户考试记录修改")
    @SysLog(value = "用户考试记录修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamRecordEditREQ userExamRecordEditREQ) {
        return biz.edit(userExamRecordEditREQ);
    }

    @ApiOperation(value = "用户考试记录删除", notes = "用户考试记录删除")
    @SysLog(value = "用户考试记录删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
