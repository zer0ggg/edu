package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 考试保存试题答案
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthExamSaveProblemAnswerBO", description = "考试保存试题答案")
public class AuthExamSaveProblemAnswerBO implements Serializable {

    private static final long serialVersionUID = -6975890277771858300L;

    @ApiModelProperty(value = "记录ID", required = true)
    private Long recordId;

    @ApiModelProperty(value = "标题ID", required = true)
    private Long titleId;

    @ApiModelProperty(value = "题目ID", required = true)
    private Long problemId;

    @ApiModelProperty(value = "回答内容")
    private String answerContent;
}
