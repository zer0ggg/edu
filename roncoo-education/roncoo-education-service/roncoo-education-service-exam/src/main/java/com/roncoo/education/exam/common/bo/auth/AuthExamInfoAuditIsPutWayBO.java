package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷信息审核上下架请求对象
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamInfoAuditIsPutWayBO", description = "试卷信息审核上下架请求对象")
public class AuthExamInfoAuditIsPutWayBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @NotNull(message = "是否上架(1:上架，0:下架)不能为空")
    @ApiModelProperty(value = "是否上架(1:上架，0:下架)", required = true)
    private Integer isPutaway;

}
