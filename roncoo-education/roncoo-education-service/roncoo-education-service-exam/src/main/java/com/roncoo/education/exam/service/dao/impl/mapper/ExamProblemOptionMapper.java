package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamProblemOptionMapper {
    int countByExample(ExamProblemOptionExample example);

    int deleteByExample(ExamProblemOptionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamProblemOption record);

    int insertSelective(ExamProblemOption record);

    List<ExamProblemOption> selectByExampleWithBLOBs(ExamProblemOptionExample example);

    List<ExamProblemOption> selectByExample(ExamProblemOptionExample example);

    ExamProblemOption selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamProblemOption record, @Param("example") ExamProblemOptionExample example);

    int updateByExampleWithBLOBs(@Param("record") ExamProblemOption record, @Param("example") ExamProblemOptionExample example);

    int updateByExample(@Param("record") ExamProblemOption record, @Param("example") ExamProblemOptionExample example);

    int updateByPrimaryKeySelective(ExamProblemOption record);

    int updateByPrimaryKeyWithBLOBs(ExamProblemOption record);

    int updateByPrimaryKey(ExamProblemOption record);
}