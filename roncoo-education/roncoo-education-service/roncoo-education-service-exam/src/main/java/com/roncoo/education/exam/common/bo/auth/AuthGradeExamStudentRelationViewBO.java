package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 学生考试详情查看
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationViewBO", description = "学生考试详情查看")
public class AuthGradeExamStudentRelationViewBO implements Serializable {

    private static final long serialVersionUID = -2130352843151324312L;

    @ApiModelProperty(value = "记录ID", required = true)
    private Long id;
}
