package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.PracticeUserAnswerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author LHR
 */
@Component
public class ApiPracticeUserAnswerBiz extends BaseBiz {

    @Autowired
    private PracticeUserAnswerDao dao;

}
