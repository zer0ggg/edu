package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷标题审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamTitleAuditAddBO implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "标题ID")
    private Long id;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 试卷ID
     */
    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID",required = true)
    private Long examId;
    /**
     * 标题排序
     */
    @ApiModelProperty(value = "标题排序")
    private Integer titleSort;
    /**
     * 标题名称
     */
    @NotEmpty(message = "标题名称不能为空")
    @ApiModelProperty(value = "标题名称",required = true)
    private String titleName;
    /**
     * 标题类型
     */
    @NotEmpty(message = "标题类型不能为空")
    @ApiModelProperty(value = "标题类型",required = true)
    private String titleType;
}
