package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.ExamRecommendListRESP;
import com.roncoo.education.exam.common.resp.ExamRecommendViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamRecommendBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷推荐 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷推荐管理")
@RestController
@RequestMapping("/exam/pc/exam/recommend")
public class PcExamRecommendController {

    @Autowired
    private PcExamRecommendBiz biz;

    @ApiOperation(value = "试卷推荐列表", notes = "试卷推荐列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamRecommendListRESP>> list(@RequestBody ExamRecommendListREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "试卷推荐添加", notes = "试卷推荐添加")
    @SysLog(value = "试卷推荐添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamRecommendSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "批量添加", notes = "批量添加")
    @SysLog(value = "试卷推荐添加")
    @PostMapping(value = "/save/batch")
    public Result<String> saveBatch(@RequestBody ExamRecommendSaveBatchREQ req) {
        return biz.saveBatch(req);
    }

    @ApiOperation(value = "试卷推荐查看", notes = "试卷推荐查看")
    @GetMapping(value = "/view")
    public Result<ExamRecommendViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷推荐修改", notes = "试卷推荐修改")
    @SysLog(value = "试卷推荐修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamRecommendEditREQ req) {
        return biz.edit(req);
    }

    @ApiOperation(value = "试卷推荐状态修改", notes = "试卷推荐修改")
    @SysLog(value = "试卷推荐修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ExamRecommendUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }

    @ApiOperation(value = "试卷推荐删除", notes = "试卷推荐删除")
    @SysLog(value = "试卷推荐删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
