package com.roncoo.education.exam.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 试卷中心
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class ExamInfoPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷ID")
    private Long id;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date gmtModified;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "学习人数")
    private Integer studyCount;

    @ApiModelProperty(value = "下载人数")
    private Integer downloadCount;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

}
