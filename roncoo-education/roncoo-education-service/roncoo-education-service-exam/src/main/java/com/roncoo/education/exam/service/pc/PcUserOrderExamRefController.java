package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserOrderExamRefEditREQ;
import com.roncoo.education.exam.common.req.UserOrderExamRefListREQ;
import com.roncoo.education.exam.common.req.UserOrderExamRefSaveREQ;
import com.roncoo.education.exam.common.resp.UserOrderExamRefListRESP;
import com.roncoo.education.exam.common.resp.UserOrderExamRefViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserOrderExamRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户订单试卷关联 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户订单试卷关联管理")
@RestController
@RequestMapping("/exam/pc/user/order/exam/ref")
public class PcUserOrderExamRefController {

    @Autowired
    private PcUserOrderExamRefBiz biz;

    @ApiOperation(value = "用户订单试卷关联列表", notes = "用户订单试卷关联列表")
    @PostMapping(value = "/list")
    public Result<Page<UserOrderExamRefListRESP>> list(@RequestBody UserOrderExamRefListREQ userOrderExamRefListREQ) {
        return biz.list(userOrderExamRefListREQ);
    }

    @ApiOperation(value = "用户订单试卷关联添加", notes = "用户订单试卷关联添加")
    @SysLog(value = "用户订单试卷关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserOrderExamRefSaveREQ userOrderExamRefSaveREQ) {
        return biz.save(userOrderExamRefSaveREQ);
    }

    @ApiOperation(value = "用户订单试卷关联查看", notes = "用户订单试卷关联查看")
    @GetMapping(value = "/view")
    public Result<UserOrderExamRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户订单试卷关联修改", notes = "用户订单试卷关联修改")
    @SysLog(value = "用户订单试卷关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserOrderExamRefEditREQ userOrderExamRefEditREQ) {
        return biz.edit(userOrderExamRefEditREQ);
    }

    @ApiOperation(value = "用户订单试卷关联删除", notes = "用户订单试卷关联删除")
    @SysLog(value = "用户订单试卷关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
