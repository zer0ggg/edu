package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class UserExamAnswer implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private Long recordId;

    private Long examId;

    private Long titleId;

    private Long problemParentId;

    private Long problemId;

    private Integer problemType;

    private Integer problemScore;

    private Integer score;

    private Integer answerStatus;

    private Integer sysAudit;

    private Long auditUserNo;

    private String userAnswer;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public Long getProblemParentId() {
        return problemParentId;
    }

    public void setProblemParentId(Long problemParentId) {
        this.problemParentId = problemParentId;
    }

    public Long getProblemId() {
        return problemId;
    }

    public void setProblemId(Long problemId) {
        this.problemId = problemId;
    }

    public Integer getProblemType() {
        return problemType;
    }

    public void setProblemType(Integer problemType) {
        this.problemType = problemType;
    }

    public Integer getProblemScore() {
        return problemScore;
    }

    public void setProblemScore(Integer problemScore) {
        this.problemScore = problemScore;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getAnswerStatus() {
        return answerStatus;
    }

    public void setAnswerStatus(Integer answerStatus) {
        this.answerStatus = answerStatus;
    }

    public Integer getSysAudit() {
        return sysAudit;
    }

    public void setSysAudit(Integer sysAudit) {
        this.sysAudit = sysAudit;
    }

    public Long getAuditUserNo() {
        return auditUserNo;
    }

    public void setAuditUserNo(Long auditUserNo) {
        this.auditUserNo = auditUserNo;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer == null ? null : userAnswer.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", recordId=").append(recordId);
        sb.append(", examId=").append(examId);
        sb.append(", titleId=").append(titleId);
        sb.append(", problemParentId=").append(problemParentId);
        sb.append(", problemId=").append(problemId);
        sb.append(", problemType=").append(problemType);
        sb.append(", problemScore=").append(problemScore);
        sb.append(", score=").append(score);
        sb.append(", answerStatus=").append(answerStatus);
        sb.append(", sysAudit=").append(sysAudit);
        sb.append(", auditUserNo=").append(auditUserNo);
        sb.append(", userAnswer=").append(userAnswer);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}