package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 考试分类
 * @author zkpc
 *
 */
@Data
@Accessors(chain = true)
public class AuthExamCategorySortBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id",required = true)
    private Long id;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序",required = true)
    private Integer sort;

}
