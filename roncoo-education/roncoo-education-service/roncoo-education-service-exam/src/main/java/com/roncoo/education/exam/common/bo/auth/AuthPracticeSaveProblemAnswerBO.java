package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 随机练习保存试题答案
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthPracticeSaveProblemAnswerBO", description = "随机练习保存试题答案")
public class AuthPracticeSaveProblemAnswerBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习ID", required = true)
    private Long practiceId;

    @ApiModelProperty(value = "题目ID", required = true)
    private Long problemId;

    @ApiModelProperty(value = "回答内容")
    private String answerContent;
}
