package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefSaveQO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.exam.service.dao.UserOrderExamRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户订单试卷关联
 *
 * @author wujing
 */
@Component
public class FeignUserOrderExamRefBiz extends BaseBiz {

    @Autowired
    private UserOrderExamRefDao dao;

    public UserOrderExamRefVO getByUserNoAndExamId(UserOrderExamRefQO qo) {
        UserOrderExamRef userOrderExamRef = dao.getByUserNoAndExamId(qo.getUserNo(), qo.getExamId());
        return BeanUtil.copyProperties(userOrderExamRef, UserOrderExamRefVO.class);
    }

    public int save(UserOrderExamRefSaveQO qo) {
        UserOrderExamRef userOrderExamRef = BeanUtil.copyProperties(qo, UserOrderExamRef.class);
        return dao.save(userOrderExamRef);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

}
