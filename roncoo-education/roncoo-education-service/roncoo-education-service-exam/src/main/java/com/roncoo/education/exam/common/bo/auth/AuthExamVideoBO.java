package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 试卷视频信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamVideoBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;
    /**
     * 状态(1:正常，0:禁用)
     */
    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 试卷ID
     */
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
    /**
     * 题目ID
     */
    @ApiModelProperty(value = "题目ID")
    private Long subjectId;
    /**
     * 视频名称
     */
    @ApiModelProperty(value = "视频名称")
    private String videoName;
    /**
     * 视频状态(1待上传，2上传成功，3上传失败)
     */
    @ApiModelProperty(value = "视频状态(1待上传，2上传成功，3上传失败)")
    private Integer videoStatus;
    /**
     * 时长
     */
    @ApiModelProperty(value = "时长")
    private String videoLength;
    /**
     * 视频ID
     */
    @ApiModelProperty(value = "视频ID")
    private String videoVid;
    /**
     * 阿里云oss
     */
    @ApiModelProperty(value = "阿里云oss")
    private String videoOssId;

}
