package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationViewDTO", description = "学生班级考试详情")
public class AuthGradeExamStudentRelationViewDTO implements Serializable {

    private static final long serialVersionUID = 1636536344793291512L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师用户名称")
    private String lecturerUserName;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "上一级科目ID")
    private Long parentSubjectId;

    @ApiModelProperty(value = "上一级科目名称")
    private String parentSubjectName;

    @ApiModelProperty(value = "上上一级科目ID")
    private Long grantSubjectId;

    @ApiModelProperty(value = "上上一级科目名称")
    private String grantSubjectName;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3:考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date answerShowTime;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)")
    private Integer compensateStatus;

    @ApiModelProperty(value = "考试状态(1:未考试，2:考试中，3:考试完成，4:批阅中，5:批阅完成)")
    private Integer examStatus;

    @ApiModelProperty(value = "年份名称")
    private String yearName;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

}
