package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamPageDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.interfaces.IFeignUserMsg;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wujing
 */
@Component
public class AuthGradeExamBiz extends BaseBiz {

    @Autowired
    private GradeExamDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;

    @Autowired
    private IFeignUser feignUser;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignUserMsg feignUserMsg;

    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(AuthGradeExamSaveBO bo) {
        if (CollectionUtil.isEmpty(bo.getStudentIdList())) {
            return Result.error("请选择需要考试的学生");
        }
        if (bo.getBeginTime().before(new Date())) {
            return Result.error("开始考试时间必须大于当前时间");
        }
        if (bo.getBeginTime().after(bo.getEndTime())) {
            return Result.error("开始考试时间必须小于结束时间");
        }


        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("用户状态异常");
        }
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("不是讲师，无法布置考试");
        }

        // 判断班级
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("当前班级不可用");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不是当前班级讲师，无法操作");
        }

        // 判断试卷是否存在
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getByIdAndLecturerUserNo(bo.getExamId(), ThreadContext.userNo());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("试卷不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(examInfoAudit.getStatusId())) {
            return Result.error("试卷不可用");
        }

        // 保存班级考试
        GradeExam gradeExam = BeanUtil.copyProperties(bo, GradeExam.class);
        gradeExam.setLecturerUserNo(ThreadContext.userNo());
        gradeExam.setGradeId(gradeInfo.getId());
        gradeExam.setGradeExamName(bo.getGradeExamName());
        gradeExam.setSubjectId(examInfoAudit.getSubjectId());
        gradeExam.setExamId(examInfoAudit.getId());
        gradeExam.setId(IdWorker.getId());

        int i = 0;
        for (Long studentId : bo.getStudentIdList()) {
            GradeStudent gradeStudent = gradeStudentDao.getById(studentId);
            if (ObjectUtil.isNull(gradeStudent)) {
                logger.error("学生【{}】不存在", studentId);
                throw new BaseException("学生【" + studentId + "】不存在");
            }
            // 判断是否为当前当前班级学生
            if (!gradeStudent.getGradeId().equals(gradeInfo.getId())) {
                logger.error("学生【{}】不是【{}】该班级学生", studentId, gradeInfo.getId());
                throw new BaseException("学生【" + studentId + "】不是该班级学生");
            }
            // 根据用户编号、班级ID、考试ID、开始时间、结束时间获取学生试卷
            GradeExamStudentRelation relation = gradeExamStudentRelationDao.getByUserNoAndGradeIdAndExamIdAndBeginExamTimeAndExamTime(gradeStudent.getUserNo(), bo.getGradeId(), bo.getExamId(), gradeExam.getBeginTime(), gradeExam.getEndTime());
            if (ObjectUtil.isNotNull(relation)) {
                continue;
            }
            i = i + 1;
            relation = new GradeExamStudentRelation();
            relation.setGradeId(gradeInfo.getId());
            relation.setGradeExamName(bo.getGradeExamName());
            relation.setGradeExamId(gradeExam.getId());
            relation.setExamId(bo.getExamId());
            relation.setStudentId(studentId);
            relation.setUserNo(gradeStudent.getUserNo());
            relation.setCutoffEndTime(gradeExam.getEndTime());
            relation.setBeginExamTime(gradeExam.getBeginTime());
            relation.setEndExamTime(gradeExam.getEndTime());
            gradeExamStudentRelationDao.save(relation);
            try {
                pushMsg(gradeInfo.getGradeName(), gradeExam.getGradeExamName(), gradeStudent.getUserNo());
            } catch (Exception e) {
                logger.warn("推送站内信失败", e);
            }
        }
        if (i > 0) {
            dao.save(gradeExam);
            return Result.success("布置成功");
        }
        return Result.error("选择的学习已布置了考试");
    }

    /**
     * 发送站内信
     *
     * @param gradeName 班级考试名称
     * @param userNo    用户名称
     */
    private void pushMsg(String gradeName, String gradeExamName, Long userNo) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();

        UserVO userVO = feignUser.getByUserNo(userNo);
        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        msg.setMsgTitle("【" + gradeName + "】班级考试通知");
        msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYEXAMINATION + "\">【" + gradeName + "】发布了新考试【" + gradeExamName + "】,快去看看吧。</a>");
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("站内信通知");
        msg.setSendTime(null);
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(userNo);
        userMsg.setMobile(userVO.getMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);
        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }

    public Result<Page<AuthGradeExamPageDTO>> listForAdminPage(AuthGradeExamPageBO bo) {
        // 判断班级是否存在
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }

        // 判断用户
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(bo.getGradeId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，不能获取班级考试信息");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                return Result.error("不是班级管理员，不能获取班级考试信息");
            }
        }

        GradeExamExample example = new GradeExamExample();
        GradeExamExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(bo.getGradeId());
        if (StrUtil.isNotBlank(bo.getGradeExamName())) {
            c.andGradeExamNameLike(PageUtil.like(bo.getGradeExamName()));
        }
        if (ObjectUtil.isNotNull(bo.getBeginExamTime())) {
            c.andBeginTimeGreaterThanOrEqualTo(bo.getBeginExamTime());
        }
        if (ObjectUtil.isNotNull(bo.getEndExamTime())) {
            c.andEndTimeLessThanOrEqualTo(bo.getEndExamTime());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeExam> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthGradeExamPageDTO> resultPage = PageUtil.transform(page, AuthGradeExamPageDTO.class);
        if (CollectionUtil.isEmpty(resultPage.getList())) {
            return Result.success(resultPage);
        }

        //补充信息
        for (AuthGradeExamPageDTO dto : resultPage.getList()) {
            //填充 原来试卷名称
            ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(dto.getExamId());
            if (!ObjectUtil.isEmpty(examInfoAudit)) {
                dto.setExamName(examInfoAudit.getExamName());

            }
            //统计 试卷完成度 未完成、考试中、已完成,总数量
            dto.setExamWaitCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(dto.getId(), GradeExamStatusEnum.WAIT.getCode()));
            dto.setExamNotOverCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(dto.getId(), GradeExamStatusEnum.NOT_OVER.getCode()));
            dto.setExamFinishCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(dto.getId(), GradeExamStatusEnum.FINISH.getCode()));
            dto.setExamCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(dto.getId(), null));

            //完成评阅 的数量 和状态
            dto.setExamFinishAuditCount(gradeExamStudentRelationDao.countByGradeExamIdAndAuditStatus(dto.getId(), ExamAuditStatusEnum.COMPLETE_AUDIT.getCode()));
            if (ExamAuditTypeEnum.NO_AUDIT.getCode().equals(dto.getAuditType()) || dto.getExamCount().equals(dto.getExamFinishAuditCount())) {
                dto.setExamIsFinishAudit(true);
            }
        }

        return Result.success(resultPage);
    }

    public Result<String> edit(AuthGradeExamEditBO bo) {
        GradeExam gradeExam = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("试卷不存在");
        }
        if (new Date().after(gradeExam.getBeginTime())) {
            return Result.error("考试已开始，无法操作");
        }
        if (!gradeExam.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不是当前班级讲师，无法操作");
        }
        if (CollectionUtil.isEmpty(bo.getStudentIdList())) {
            return Result.error("请选择需要考试的学生");
        }
        if (!bo.getBeginTime().before(bo.getEndTime())) {
            return Result.error("开始考试时间必须小于结束时间");
        }

        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("用户状态异常");
        }

        // 修改班级考试
        gradeExam.setGradeExamName(bo.getGradeExamName());
        gradeExam.setBeginTime(bo.getBeginTime());
        gradeExam.setEndTime(bo.getEndTime());
        gradeExam.setAnswerShow(bo.getAnswerShow());
        gradeExam.setAnswerShowTime(bo.getAnswerShowTime());
        gradeExam.setAuditType(bo.getAuditType());
        gradeExam.setCompensateStatus(bo.getCompensateStatus());
        dao.updateById(gradeExam);


        GradeInfo gradeInfo = gradeInfoDao.getById(gradeExam.getGradeId());
        for (Long studentId : bo.getStudentIdList()) {
            // 删除  关联学生
            gradeExamStudentRelationDao.deleteByGradeExamId(gradeExam.getId());
            //重新 关联学生
            GradeStudent gradeStudent = gradeStudentDao.getById(studentId);
            if (ObjectUtil.isNull(gradeStudent)) {
                logger.error("学生【{}】不存在", studentId);
                throw new BaseException("学生【" + studentId + "】不存在");
            }
            // 判断是否为当前当前班级学生
            if (!gradeStudent.getGradeId().equals(gradeInfo.getId())) {
                logger.error("学生【{}】不是【{}】该班级学生", studentId, gradeInfo.getId());
                throw new BaseException("学生【" + studentId + "】不是该班级学生");
            }

            GradeExamStudentRelation relation = new GradeExamStudentRelation();
            relation.setGradeId(gradeInfo.getId());
            relation.setGradeExamName(bo.getGradeExamName());
            relation.setGradeExamId(gradeExam.getId());
            relation.setExamId(gradeExam.getExamId());
            relation.setStudentId(studentId);
            relation.setUserNo(gradeStudent.getUserNo());
            relation.setCutoffEndTime(gradeExam.getEndTime());
            relation.setBeginExamTime(gradeExam.getBeginTime());
            relation.setEndExamTime(gradeExam.getEndTime());
            gradeExamStudentRelationDao.save(relation);
        }
        return Result.success("操作成功");

    }

    public Result<String> editCompensateStatus(AuthGradeExamEditCompensateStatusBO bo) {
        GradeExam gradeExam = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("考试不存在");
        }
        GradeExamCompensateStatusEnum statusEnum = GradeExamCompensateStatusEnum.byCode(bo.getCompensateStatus());
        if (ObjectUtil.isNull(statusEnum)) {
            return Result.error("补交状态不正确");
        }
        GradeExam bean = BeanUtil.copyProperties(bo, GradeExam.class);
        if (dao.updateById(bean) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> del(AuthGradeExamDelBO bo) {
        GradeExam gradeExam = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("试卷不存在");
        }
        if (!gradeExam.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不是当前班级讲师，无法操作");
        }

        int count = gradeExamStudentRelationDao.countByGradeExamId(gradeExam.getId());
        if (count > 0 && new Date().after(gradeExam.getBeginTime())) {
            // 如果有人且开考则不能关闭
            return Result.error("考试已开始，无法操作");
        }

        //删除试卷
        dao.deleteById(bo.getId());
        // 删除  关联学生
        gradeExamStudentRelationDao.deleteByGradeExamId(gradeExam.getId());
        return Result.success("操作成功");
    }

    public Result<List<Long>> getSelectedStudentIdsByGradeExamId(AuthGradeExamEditBeforeBO bo) {
        GradeExam gradeExam = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("试卷不存在!");
        }
        List<GradeExamStudentRelation> studentRelations = gradeExamStudentRelationDao.listByGradeExamId(bo.getId());
        if (CollectionUtil.isEmpty(studentRelations)) {
            return Result.success(null);
        }
        return Result.success(studentRelations.stream().map(GradeExamStudentRelation::getStudentId).collect(Collectors.toList()));

    }
}
