package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 学生考试  评阅
 *
 * @author zk
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationSysAuditBO", description = "学生考试  预览分数")
public class AuthGradeExamStudentRelationViewScoreBO implements Serializable {

    private static final long serialVersionUID = -2130352843151324312L;

    @ApiModelProperty(value = "记录ID", required = true)
    private Long id;
}
