package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamTitleProblemRef;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamTitleProblemRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题题目关联
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamTitleProblemRefController extends BaseController implements IFeignExamTitleProblemRef{

    @Autowired
    private FeignExamTitleProblemRefBiz biz;

	@Override
	public Page<ExamTitleProblemRefVO> listForPage(@RequestBody ExamTitleProblemRefQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamTitleProblemRefQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamTitleProblemRefQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamTitleProblemRefVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
