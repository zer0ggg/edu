package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitle;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamTitleMapper {
    int countByExample(ExamTitleExample example);

    int deleteByExample(ExamTitleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamTitle record);

    int insertSelective(ExamTitle record);

    List<ExamTitle> selectByExample(ExamTitleExample example);

    ExamTitle selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamTitle record, @Param("example") ExamTitleExample example);

    int updateByExample(@Param("record") ExamTitle record, @Param("example") ExamTitleExample example);

    int updateByPrimaryKeySelective(ExamTitle record);

    int updateByPrimaryKey(ExamTitle record);
}