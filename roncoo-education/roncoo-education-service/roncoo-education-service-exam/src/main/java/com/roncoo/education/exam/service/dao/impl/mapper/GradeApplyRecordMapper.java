package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeApplyRecordMapper {
    int countByExample(GradeApplyRecordExample example);

    int deleteByExample(GradeApplyRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeApplyRecord record);

    int insertSelective(GradeApplyRecord record);

    List<GradeApplyRecord> selectByExample(GradeApplyRecordExample example);

    GradeApplyRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeApplyRecord record, @Param("example") GradeApplyRecordExample example);

    int updateByExample(@Param("record") GradeApplyRecord record, @Param("example") GradeApplyRecordExample example);

    int updateByPrimaryKeySelective(GradeApplyRecord record);

    int updateByPrimaryKey(GradeApplyRecord record);
}
