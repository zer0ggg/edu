package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamAnswerEditREQ;
import com.roncoo.education.exam.common.req.UserExamAnswerListREQ;
import com.roncoo.education.exam.common.req.UserExamAnswerSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamAnswerListRESP;
import com.roncoo.education.exam.common.resp.UserExamAnswerViewRESP;
import com.roncoo.education.exam.service.dao.UserExamAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@Component
public class PcUserExamAnswerBiz extends BaseBiz {

	@Autowired
	private UserExamAnswerDao dao;

	/**
	 * 用户考试答案列表
	 *
	 * @param userExamAnswerListREQ 用户考试答案分页查询参数
	 * @return 用户考试答案分页查询结果
	 */
	public Result<Page<UserExamAnswerListRESP>> list(UserExamAnswerListREQ userExamAnswerListREQ) {
		UserExamAnswerExample example = new UserExamAnswerExample();
		Criteria c = example.createCriteria();
		Page<UserExamAnswer> page = dao.listForPage(userExamAnswerListREQ.getPageCurrent(), userExamAnswerListREQ.getPageSize(), example);
		Page<UserExamAnswerListRESP> respPage = PageUtil.transform(page, UserExamAnswerListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户考试答案添加
	 *
	 * @param userExamAnswerSaveREQ 用户考试答案
	 * @return 添加结果
	 */
	public Result<String> save(UserExamAnswerSaveREQ userExamAnswerSaveREQ) {
		UserExamAnswer record = BeanUtil.copyProperties(userExamAnswerSaveREQ, UserExamAnswer.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户考试答案查看
	 *
	 * @param id 主键ID
	 * @return 用户考试答案
	 */
	public Result<UserExamAnswerViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamAnswerViewRESP.class));
	}

	/**
	 * 用户考试答案修改
	 *
	 * @param userExamAnswerEditREQ 用户考试答案修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamAnswerEditREQ userExamAnswerEditREQ) {
		UserExamAnswer record = BeanUtil.copyProperties(userExamAnswerEditREQ, UserExamAnswer.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户考试答案删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
