package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamVideo;
import com.roncoo.education.exam.feign.qo.ExamVideoQO;
import com.roncoo.education.exam.feign.vo.ExamVideoVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamVideoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷视频
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamVideoController extends BaseController implements IFeignExamVideo{

    @Autowired
    private FeignExamVideoBiz biz;

	@Override
	public Page<ExamVideoVO> listForPage(@RequestBody ExamVideoQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamVideoQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamVideoQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamVideoVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public ExamVideoVO getByVideoVid(@PathVariable(value = "vid") String vid) {
		return biz.getByVideoVid(vid);
	}

	@Override
	public int updateByVid(@RequestBody ExamVideoQO examVideoQO) {
		return biz.updateByVid(examVideoQO);
	}
}
