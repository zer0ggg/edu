package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "AuthGradeExamStudentRelationCompleteAuditBO", description = "班级考试完成对象")
public class AuthGradeExamStudentRelationCompleteAuditBO implements Serializable {

    @NotNull(message = "关联ID不能为空")
    @ApiModelProperty(value = "考试关联ID", required = true)
    private Long relationId;
}
