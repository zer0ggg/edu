package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamRecordMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户考试记录 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamRecordDaoImpl implements UserExamRecordDao {

    @Autowired
    private UserExamRecordMapper mapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(UserExamRecord record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExamRecord record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExamRecord getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public UserExamRecord getByIdForUpdate(Long id) {
        return jdbcTemplate.queryForObject("select * from user_exam_record where id = ? for update", new Object[]{id}, (resultSet, i) -> {
            UserExamRecord record = new UserExamRecord();
            record.setId(resultSet.getLong("id"));
            record.setGmtCreate(resultSet.getTimestamp("gmt_create"));
            record.setGmtModified(resultSet.getTimestamp("gmt_modified"));
            record.setStatusId(resultSet.getInt("status_id"));
            record.setSort(resultSet.getInt("sort"));
            record.setUserNo(resultSet.getLong("user_no"));
            record.setExamId(resultSet.getLong("exam_id"));
            record.setAnswerTime(resultSet.getInt("answer_time"));
            record.setEndTime(resultSet.getTimestamp("end_time"));
            record.setTotalScore(resultSet.getInt("total_score"));
            record.setScore(resultSet.getInt("score"));
            record.setSysAuditTotalScore(resultSet.getInt("sys_audit_total_score"));
            record.setSysAuditScore(resultSet.getInt("sys_audit_score"));
            record.setHandworkAuditTotalScore(resultSet.getInt("handwork_audit_total_score"));
            record.setHandworkAuditScore(resultSet.getInt("handwork_audit_score"));
            record.setExamStatus(resultSet.getInt("exam_status"));
            return record;
        });
    }

    @Override
    public Page<UserExamRecord> listForPage(int pageCurrent, int pageSize, UserExamRecordExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<UserExamRecord> listByUserNoAndExamStatus(Long userNo, Integer examStatus) {
        UserExamRecordExample example = new UserExamRecordExample();
        UserExamRecordExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamStatusEqualTo(examStatus);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<UserExamRecord> listByUserNoAndExamIdAndExamStatus(Long userNo, Long examId, Integer examStatus) {
        UserExamRecordExample example = new UserExamRecordExample();
        UserExamRecordExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamIdEqualTo(examId);
        c.andExamStatusEqualTo(examStatus);
        return this.mapper.selectByExample(example);
    }

    @Override
    public UserExamRecord getByUserNoAndExamId(Long userNo, Long examId) {
        UserExamRecordExample example = new UserExamRecordExample();
        UserExamRecordExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamIdEqualTo(examId);
        List<UserExamRecord> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public UserExamRecord getByUserNoAndExamIdAndExamStatusNotOver(Long userNo, Long examId, Integer examStatus) {
        UserExamRecordExample example = new UserExamRecordExample();
        UserExamRecordExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamIdEqualTo(examId);
        c.andExamStatusEqualTo(examStatus);
        List<UserExamRecord> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
