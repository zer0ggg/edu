package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiPracticeUserAnswerBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 用户考试答案 Api接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-用户考试答案")
@RestController
@RequestMapping("/exam/api/practiceUserAnswer")
public class ApiPracticeUserAnswerController {

    @Autowired
    private ApiPracticeUserAnswerBiz biz;

}
