package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamAnswerQO;
import com.roncoo.education.exam.feign.vo.UserExamAnswerVO;
import com.roncoo.education.exam.service.dao.UserExamAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@Component
public class FeignUserExamAnswerBiz extends BaseBiz {

    @Autowired
    private UserExamAnswerDao dao;

	public Page<UserExamAnswerVO> listForPage(UserExamAnswerQO qo) {
	    UserExamAnswerExample example = new UserExamAnswerExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExamAnswer> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamAnswerVO.class);
	}

	public int save(UserExamAnswerQO qo) {
		UserExamAnswer record = BeanUtil.copyProperties(qo, UserExamAnswer.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamAnswerVO getById(Long id) {
		UserExamAnswer record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamAnswerVO.class);
	}

	public int updateById(UserExamAnswerQO qo) {
		UserExamAnswer record = BeanUtil.copyProperties(qo, UserExamAnswer.class);
		return dao.updateById(record);
	}

}
