package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeApplyRecordQO;
import com.roncoo.education.exam.feign.vo.GradeApplyRecordVO;
import com.roncoo.education.exam.service.dao.GradeApplyRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeApplyRecordBiz extends BaseBiz {

    @Autowired
    private GradeApplyRecordDao dao;

	public Page<GradeApplyRecordVO> listForPage(GradeApplyRecordQO qo) {
	    GradeApplyRecordExample example = new GradeApplyRecordExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeApplyRecord> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeApplyRecordVO.class);
	}

	public int save(GradeApplyRecordQO qo) {
		GradeApplyRecord record = BeanUtil.copyProperties(qo, GradeApplyRecord.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeApplyRecordVO getById(Long id) {
		GradeApplyRecord record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeApplyRecordVO.class);
	}

	public int updateById(GradeApplyRecordQO qo) {
		GradeApplyRecord record = BeanUtil.copyProperties(qo, GradeApplyRecord.class);
		return dao.updateById(record);
	}

}
