package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.GradeStudentEditREQ;
import com.roncoo.education.exam.common.req.GradeStudentPageREQ;
import com.roncoo.education.exam.common.req.GradeStudentSaveREQ;
import com.roncoo.education.exam.common.resp.GradeStudentListRESP;
import com.roncoo.education.exam.common.resp.GradeStudentViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeStudentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 班级学生管理 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-班级学生管理")
@RestController
@RequestMapping("/exam/pc/grade/student")
public class PcGradeStudentController {

    @Autowired
    private PcGradeStudentBiz biz;

    @ApiOperation(value = "班级学生列表", notes = "班级学生列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeStudentListRESP>> list(@RequestBody @Valid GradeStudentPageREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级学生添加", notes = "班级学生添加")
    @SysLog(value = "班级学生添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid GradeStudentSaveREQ req) {
        return biz.save(req);
    }

    @ApiOperation(value = "班级学生修改", notes = "班级学生修改")
    @SysLog(value = "班级学生修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid GradeStudentEditREQ req) {
        return biz.edit(req);
    }

    @ApiOperation(value = "班级学生删除", notes = "班级学生删除")
    @SysLog(value = "班级学生删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "班级学生查看", notes = "班级学生查看")
    @GetMapping(value = "/view")
    public Result<GradeStudentViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }
}
