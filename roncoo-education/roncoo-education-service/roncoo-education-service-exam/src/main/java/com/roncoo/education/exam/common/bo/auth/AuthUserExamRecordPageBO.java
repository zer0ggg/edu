package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户考试记录分页参数
 *
 * @author LYQ
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthUserExamRecordPageBO", description = "用户考试记录分页参数")
public class AuthUserExamRecordPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 83333821677860190L;
}
