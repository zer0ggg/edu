package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamTitleQO;
import com.roncoo.education.exam.feign.vo.ExamTitleVO;
import com.roncoo.education.exam.service.dao.ExamTitleDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitle;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题
 *
 * @author wujing
 */
@Component
public class FeignExamTitleBiz extends BaseBiz {

    @Autowired
    private ExamTitleDao dao;

	public Page<ExamTitleVO> listForPage(ExamTitleQO qo) {
	    ExamTitleExample example = new ExamTitleExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamTitle> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamTitleVO.class);
	}

	public int save(ExamTitleQO qo) {
		ExamTitle record = BeanUtil.copyProperties(qo, ExamTitle.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamTitleVO getById(Long id) {
		ExamTitle record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamTitleVO.class);
	}

	public int updateById(ExamTitleQO qo) {
		ExamTitle record = BeanUtil.copyProperties(qo, ExamTitle.class);
		return dao.updateById(record);
	}

}
