package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoAuditDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoAuditPreviewDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthExamInfoAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 试卷信息审核 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-试卷信息审核管理")
@RestController
@RequestMapping("/exam/auth/exam/audit")
public class AuthExamInfoAuditController {

    @Autowired
    private AuthExamInfoAuditBiz biz;

    @ApiOperation(value = "试卷分页接口", notes = "试卷分页接口")
    @PostMapping(value = "/list")
    public Result<Page<AuthExamInfoAuditDTO>> listForPage(@RequestBody AuthExamInfoAuditListBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "查看接口", notes = "查看接口")
    @PostMapping(value = "/view")
    public Result<AuthExamInfoAuditDTO> view(@RequestBody @Valid AuthExamInfoAuditViewBO viewBO) {
        return biz.view(viewBO);
    }

    @ApiOperation(value = "上下架接口", notes = "上下架接口")
    @PostMapping(value = "/is/put/way")
    public Result<String> isPutaway(@RequestBody @Valid AuthExamInfoAuditIsPutWayBO bo) {
        return biz.isPutaway(bo);
    }

    @ApiOperation(value = "提交审核接口", notes = "提交审核接口")
    @PostMapping(value = "/is/submit/audit")
    public Result<String> submitAudit(@RequestBody @Valid AuthExamInfoAuditIsSubmitAuditBO bo) {
        return biz.submitAudit(bo);
    }

    @ApiOperation(value = "修改接口", notes = "修改接口")
    @PostMapping(value = "/update")
    public Result<String> update(@RequestBody @Valid AuthExamInfoAuditUpdateBO bo) {
        return biz.update(bo);
    }

    @ApiOperation(value = "保存接口", notes = "保存接口")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AuthExamInfoAuditAddBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "预览试卷", notes = "预览试卷")
    @PostMapping(value = "/preview")
    public Result<AuthExamInfoAuditPreviewDTO> preview(@RequestBody @Valid AuthExamInfoAuditPreviewBO previewBO) {
        return biz.preview(previewBO);
    }

    @ApiOperation(value = "组卷", notes = "组卷")
    @PostMapping(value = "/save/exam/ref")
    public Result<String> saveExamRef(@RequestBody @Valid AuthExamInfoAuditSaveRefBO bo) {
        return biz.saveExamRef(bo);
    }


}
