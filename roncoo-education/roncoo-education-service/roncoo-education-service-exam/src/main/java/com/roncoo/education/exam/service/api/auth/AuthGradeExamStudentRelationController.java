package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeExamStudentRelationBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级学生考试管理")
@RestController
@RequestMapping("/exam/auth/grade/exam/student/relation")
public class AuthGradeExamStudentRelationController {

    @Autowired
    private AuthGradeExamStudentRelationBiz biz;

    @ApiOperation(value = "提交试卷，系统阅卷（客观题）", notes = "提交试卷，系统阅卷（客观题）")
    @PostMapping(value = "/submit/audit")
    public Result<AuthGradeExamStudentRelationScoreDTO> submitAndAudit(@RequestBody @Valid AuthGradeExamStudentRelationSysAuditBO bo) {
        return biz.submitAndAudit(bo);
    }

    @ApiOperation(value = "预览分数", notes = "系统阅卷（客观题）")
    @PostMapping(value = "/view/score")
    public Result<AuthGradeExamStudentRelationScoreDTO> viewScore(@RequestBody @Valid AuthGradeExamStudentRelationSysAuditBO bo) {
        return biz.viewScore(bo);
    }

    @ApiOperation(value = "分页列出", notes = "分页列出")
    @PostMapping(value = "/page")
    public Result<Page<AuthGradeExamStudentRelationPageDTO>> listForPage(@RequestBody @Valid AuthGradeExamStudentRelationPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "查看详情", notes = "查看详情")
    @PostMapping(value = "/view")
    public Result<AuthGradeExamStudentRelationViewDTO> view(@RequestBody @Valid AuthGradeExamStudentRelationViewBO bo) {
        return biz.view(bo);
    }

    @ApiOperation(value = "开始考试", notes = "开始考试")
    @PostMapping(value = "/begin")
    public Result<AuthGradeExamAndAnswerDTO> examBegin(@RequestBody @Valid AuthGradeExamStudentRelationContinueBO bo) {
        return biz.examBegin(bo);
    }

    @ApiOperation(value = "继续考试", notes = "继续考试")
    @PostMapping(value = "/continue")
    public Result<AuthGradeExamAndAnswerDTO> examContinue(@RequestBody @Valid AuthGradeExamStudentRelationContinueBO bo) {
        return biz.examContinue(bo);
    }

    @ApiOperation(value = "学生考试提示列表", notes = "学生考试提示列表")
    @PostMapping(value = "/tips")
    public Result<List<AuthGradeExamStudentRelationTipsListDTO>> listTips(@RequestBody @Valid AuthGradeExamStudentRelationTipsListBO bo) {
        return biz.listTips(bo);
    }

    @ApiOperation(value = "学生考试忽略查看", notes = "学生考试忽略查看")
    @PutMapping(value = "/ignore")
    public Result<String> examViewIgnore(@RequestBody @Valid AuthGradeExamStudentRelationTipsIgnoreBO bo) {
        return biz.examViewIgnore(bo);
    }

    @ApiOperation(value = "班级考试完成--学生试卷", notes = "班级考试完成--学生试卷")
    @PostMapping(value = "/list")
    public Result<List<AuthGradeExamStudentRelationCompleteGradeExamIdListDTO>> listByGradeExamId(@RequestBody @Valid AuthGradeExamStudentRelationCompleteGradeExamIdListBO bo) {
        return biz.listByGradeExamId(bo);
    }

    @ApiOperation(value = "评阅完成", notes = "评阅完成")
    @PutMapping(value = "/audit/complete")
    public Result<String> auditComplete(@RequestBody @Valid AuthGradeExamStudentRelationContinueBO bo) {
        return biz.auditComplete(bo);
    }

    @ApiOperation(value = "班级显示信息列", notes = "班级显示信息列")
    @PostMapping(value = "/page/grade")
    public Result<Page<AuthGradeExamStudentRelationGradeForPageDTO>> listGradeForPage(@RequestBody @Valid AuthGradeExamStudentRelationGradeForPageBO bo) {
        return biz.listGradeForPage(bo);
    }

    @ApiOperation(value = "导出该考试所有学员信息", notes = "导出该考试所有学员信息")
    @PostMapping(value = "/export")
    public void export(HttpServletResponse response, @Valid AuthGradeExamStudentRelationGradeForPageBO bo) {
        biz.export(response, bo);
    }
}
