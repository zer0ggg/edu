package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 课程试卷关联
 * </p>
 *
 * @author wujing
 * @date 2020-10-21
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthCourseExamRefDeleteBO", description="课程试卷关联")
public class AuthCourseExamRefDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键")
    private Long id;

}
