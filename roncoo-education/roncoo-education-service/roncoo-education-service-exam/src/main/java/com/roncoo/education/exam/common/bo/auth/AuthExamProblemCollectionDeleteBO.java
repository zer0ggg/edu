package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷试题收藏
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemCollectionDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "收藏类型不能为空")
    @ApiModelProperty(value = "收藏类型", required = true)
    private Integer collectionType;

    @NotNull(message = "收藏ID不能为空")
    @ApiModelProperty(value = "收藏ID", required = true)
    private Long collectionId;

}
