package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblemOption;
import com.roncoo.education.exam.feign.qo.ExamProblemOptionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemOptionVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamProblemOptionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷题目选项
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamProblemOptionController extends BaseController implements IFeignExamProblemOption{

    @Autowired
    private FeignExamProblemOptionBiz biz;

	@Override
	public Page<ExamProblemOptionVO> listForPage(@RequestBody ExamProblemOptionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamProblemOptionQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamProblemOptionQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamProblemOptionVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
