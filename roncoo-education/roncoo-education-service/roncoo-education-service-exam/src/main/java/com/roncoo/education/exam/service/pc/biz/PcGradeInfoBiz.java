package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.GradeVerificationSetEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.ReferralCodeUtil;
import com.roncoo.education.exam.common.req.GradeInfoEditREQ;
import com.roncoo.education.exam.common.req.GradeInfoPageREQ;
import com.roncoo.education.exam.common.req.GradeInfoSaveREQ;
import com.roncoo.education.exam.common.resp.GradeInfoListRESP;
import com.roncoo.education.exam.common.resp.GradeInfoViewRESP;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wujing
 */
@Component
public class PcGradeInfoBiz extends BaseBiz {

    @Autowired
    private GradeInfoDao dao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeExamDao gradeExamDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeStudentExamAnswerDao;

    @Autowired
    private IFeignLecturer feignLecturer;

    /**
     * 班级学生分页查询
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeInfoListRESP>> list(GradeInfoPageREQ req) {
        GradeInfoExample example = new GradeInfoExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(req.getStatusId())) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (ObjectUtil.isNotNull(req.getLecturerUserNo())) {
            c.andLecturerUserNoEqualTo(req.getLecturerUserNo());
        }
        if (StrUtil.isNotBlank(req.getGradeName())) {
            c.andGradeNameLike(PageUtil.like(req.getGradeName()));
        }
        if (ObjectUtil.isNotNull(req.getVerificationSet())) {
            c.andVerificationSetEqualTo(req.getVerificationSet());
        }
        if (StrUtil.isNotBlank(req.getInvitationCode())) {
            c.andInvitationCodeLike(PageUtil.like(req.getInvitationCode()));
        }
        example.setOrderByClause("backstage_sort asc, id desc");
        Page<GradeInfo> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeInfoListRESP> respPage = PageUtil.transform(page, GradeInfoListRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }

        // 获取讲师名称
        Map<Long, LecturerVO> lecturerVOCacheMap = new HashMap<>();
        for (GradeInfoListRESP resp : respPage.getList()) {
            LecturerVO lecturerVO = lecturerVOCacheMap.get(resp.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturerVO)) {
                lecturerVO = feignLecturer.getByLecturerUserNo(resp.getLecturerUserNo());
            }

            if (ObjectUtil.isNotNull(lecturerVO)) {
                resp.setLecturerName(lecturerVO.getLecturerName());
                lecturerVOCacheMap.put(lecturerVO.getLecturerUserNo(), lecturerVO);
            }
        }

        return Result.success(respPage);
    }

    /**
     * 添加班级
     *
     * @param req 添加班级参数
     * @return 添加结果
     */
    public Result<String> save(GradeInfoSaveREQ req) {
        // 校验讲师信息
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(req.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("讲师不存在");
        }

        // 判断验证类型
        GradeVerificationSetEnum verificationSetEnum = GradeVerificationSetEnum.byCode(req.getVerificationSet());
        if (ObjectUtil.isNull(verificationSetEnum)) {
            return Result.error("班级权限类型错误，请传入正确的权限类型");
        }

        // 保存
        GradeInfo gradeInfo = BeanUtil.copyProperties(req, GradeInfo.class);
        gradeInfo.setInvitationCode(ReferralCodeUtil.getStringRandom());
        if (dao.save(gradeInfo) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 班级信息查看
     *
     * @param id 主键ID
     * @return 班级信息
     */
    public Result<GradeInfoViewRESP> view(Long id) {
        GradeInfoViewRESP resp = BeanUtil.copyProperties(dao.getById(id), GradeInfoViewRESP.class);
        if (ObjectUtil.isNull(resp)) {
            return Result.error("讲师不存在");
        }

        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(resp.getLecturerUserNo());
        if (ObjectUtil.isNotNull(lecturerVO)) {
            resp.setLecturerName(lecturerVO.getLecturerName());
        }

        // 获取班级学生人数
        resp.setStudentCount(gradeStudentDao.countByGradeId(resp.getId()));
        // 考试次数
        resp.setGradeExamCount(gradeExamDao.countByGradeId(resp.getId()));
        return Result.success(resp);
    }

    /**
     * 班级信息修改
     *
     * @param req 修改对象
     * @return 修改结果
     */
    public Result<String> edit(GradeInfoEditREQ req) {
        GradeInfo gradeInfo = BeanUtil.copyProperties(req, GradeInfo.class);

        // 判断验证类型
        GradeVerificationSetEnum verificationSetEnum = GradeVerificationSetEnum.byCode(req.getVerificationSet());
        if (ObjectUtil.isNull(verificationSetEnum)) {
            return Result.error("班级权限类型错误，请传入正确的权限类型");
        }

        if (dao.updateById(gradeInfo) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 修改班级状态
     *
     * @param id 班级ID
     * @return 修改结果
     */
    public Result<String> updateStatus(Long id) {
        GradeInfo gradeInfo = dao.getById(id);
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }

        GradeInfo updateGradeInfo = new GradeInfo();
        updateGradeInfo.setId(id);
        updateGradeInfo.setStatusId(StatusIdEnum.NO.getCode().equals(gradeInfo.getStatusId()) ? StatusIdEnum.YES.getCode() : StatusIdEnum.NO.getCode());

        if (dao.updateById(updateGradeInfo) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 班级信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        // 删除班级学生考试答案
        gradeStudentExamAnswerDao.deleteByGradeId(id);

        // 删除班级学生考试试卷标题成绩
        gradeStudentExamTitleScoreDao.deleteByGradeId(id);

        // 删除班级考试
        gradeExamStudentRelationDao.deleteByGradeId(id);
        gradeExamDao.deleteByGradeId(id);

        // 删除班级学生
        gradeStudentDao.deleteByGradeId(id);

        // 删除班级
        if (dao.deleteById(id) < 0) {
            throw new BaseException("删除班级失败");
        }
        return Result.success("删除成功");
    }
}
