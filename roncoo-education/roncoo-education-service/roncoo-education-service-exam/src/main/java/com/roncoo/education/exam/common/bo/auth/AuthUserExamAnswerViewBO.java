package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户考试答案
 *
 * @author wujing
 */
@ApiModel(value = "用户试卷答案--查询参数", description = "用户试卷答案--查询参数")
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "记录ID不能为空")
    @ApiModelProperty(value = "记录ID", required = true)
    private Long recordId;
}
