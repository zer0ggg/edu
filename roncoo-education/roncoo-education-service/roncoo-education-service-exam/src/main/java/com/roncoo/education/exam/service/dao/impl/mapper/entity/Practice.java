package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class Practice implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private Long subjectId;

    private Long difficultyId;

    private Long sourceId;

    private Long yearId;

    private String region;

    private String emphasis;

    private Long topicId;

    private Long graId;

    private Long emphasisId;

    private String name;

    private Integer theRadioNum;

    private Integer multipleChoiceNum;

    private Integer estimateNum;

    private Integer gapFillingNum;

    private Integer shortAnswerNum;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getDifficultyId() {
        return difficultyId;
    }

    public void setDifficultyId(Long difficultyId) {
        this.difficultyId = difficultyId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public String getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(String emphasis) {
        this.emphasis = emphasis == null ? null : emphasis.trim();
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public Long getGraId() {
        return graId;
    }

    public void setGraId(Long graId) {
        this.graId = graId;
    }

    public Long getEmphasisId() {
        return emphasisId;
    }

    public void setEmphasisId(Long emphasisId) {
        this.emphasisId = emphasisId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getTheRadioNum() {
        return theRadioNum;
    }

    public void setTheRadioNum(Integer theRadioNum) {
        this.theRadioNum = theRadioNum;
    }

    public Integer getMultipleChoiceNum() {
        return multipleChoiceNum;
    }

    public void setMultipleChoiceNum(Integer multipleChoiceNum) {
        this.multipleChoiceNum = multipleChoiceNum;
    }

    public Integer getEstimateNum() {
        return estimateNum;
    }

    public void setEstimateNum(Integer estimateNum) {
        this.estimateNum = estimateNum;
    }

    public Integer getGapFillingNum() {
        return gapFillingNum;
    }

    public void setGapFillingNum(Integer gapFillingNum) {
        this.gapFillingNum = gapFillingNum;
    }

    public Integer getShortAnswerNum() {
        return shortAnswerNum;
    }

    public void setShortAnswerNum(Integer shortAnswerNum) {
        this.shortAnswerNum = shortAnswerNum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", subjectId=").append(subjectId);
        sb.append(", difficultyId=").append(difficultyId);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", yearId=").append(yearId);
        sb.append(", region=").append(region);
        sb.append(", emphasis=").append(emphasis);
        sb.append(", topicId=").append(topicId);
        sb.append(", graId=").append(graId);
        sb.append(", emphasisId=").append(emphasisId);
        sb.append(", name=").append(name);
        sb.append(", theRadioNum=").append(theRadioNum);
        sb.append(", multipleChoiceNum=").append(multipleChoiceNum);
        sb.append(", estimateNum=").append(estimateNum);
        sb.append(", gapFillingNum=").append(gapFillingNum);
        sb.append(", shortAnswerNum=").append(shortAnswerNum);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}