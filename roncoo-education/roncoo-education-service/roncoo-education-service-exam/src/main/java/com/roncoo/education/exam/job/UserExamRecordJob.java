package com.roncoo.education.exam.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.enums.ExamStatusEnum;
import com.roncoo.education.exam.job.biz.UserExamRecordJobBiz;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.UserExamRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 考试到期关闭
 *
 * @author LYQ
 */
@Slf4j
@Component
public class UserExamRecordJob extends IJobHandler {

	@Autowired
	private UserExamRecordJobBiz biz;

	@Autowired
	private UserExamRecordDao dao;
	@Autowired
	private ExamInfoDao examInfoDao;

	@Override
	@XxlJob(value = "userExamRecordJob")
	public ReturnT<String> execute(String s) {
		XxlJobLogger.log("开始执行用户考试关闭任务");
		log.debug("开始执行用户考试关闭任务");
		int pageCurrent = 1;
		int pageSize = 1000;
		UserExamRecordExample example = new UserExamRecordExample();
		UserExamRecordExample.Criteria c = example.createCriteria();
		c.andExamStatusEqualTo(ExamStatusEnum.NOT_OVER.getCode());
		example.setOrderByClause("id asc");

		boolean go;
		do {
			Page<UserExamRecord> resultPage = dao.listForPage(pageCurrent, pageSize, example);
			if (ObjectUtil.isNull(resultPage) || CollectionUtil.isEmpty(resultPage.getList())) {
				return SUCCESS;
			}

			for (UserExamRecord record : resultPage.getList()) {
				ExamInfo examInfo = examInfoDao.getById(record.getExamId());
				if (ObjectUtil.isNull(examInfo)) {
					continue;
				}

				// 判断是否已经结束
				int answerTime = examInfo.getAnswerTime() + 10; //答卷时长已经结束超过10分钟执行考试结束
				if (DateUtil.offsetMinute(record.getGmtCreate(), answerTime).after(new Date())) {
					continue;
				}

				// 执行考试结束
				log.debug("考试记录：[{}]达到考试关闭条件，进行关闭处理", record.getId());
				XxlJobLogger.log("考试记录：[{}]达到考试关闭条件，进行关闭处理", record.getId());
				try {
					biz.handleExamClosed(record.getId());
				} catch (Exception e) {
					log.error("考试记录：[{}]考试关闭异常！", record.getId(), e);
					XxlJobLogger.log("考试记录：[{}]考试关闭异常！", record.getId(), e);
				}
			}

			// 判断是否继续
			go = resultPage.getPageCurrent() < resultPage.getTotalPage();
		} while (go);
		log.debug("结束执行用户考试关闭任务");
		XxlJobLogger.log("结束执行用户考试关闭任务");
		return SUCCESS;
	}
}
