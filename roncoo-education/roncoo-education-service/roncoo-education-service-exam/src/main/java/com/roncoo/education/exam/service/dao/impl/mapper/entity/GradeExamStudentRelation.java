package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class GradeExamStudentRelation implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer sort;

    private Integer statusId;

    private String remark;

    private Long gradeId;

    private Long gradeExamId;

    private Long examId;

    private Long studentId;

    private Long userNo;

    private Integer viewStatus;

    private Integer examStatus;

    private Date beginAnswerTime;

    private Date endAnswerTime;

    private Integer score;

    private Integer sysAuditTotalScore;

    private Integer sysAuditScore;

    private Integer lecturerAuditTotalScore;

    private Integer lecturerAuditScore;

    private Long auditUserNo;

    private String gradeExamName;

    private Date cutoffEndTime;

    private Date beginExamTime;

    private Date endExamTime;

    private Integer auditStatus;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getGradeExamId() {
        return gradeExamId;
    }

    public void setGradeExamId(Long gradeExamId) {
        this.gradeExamId = gradeExamId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Integer getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(Integer viewStatus) {
        this.viewStatus = viewStatus;
    }

    public Integer getExamStatus() {
        return examStatus;
    }

    public void setExamStatus(Integer examStatus) {
        this.examStatus = examStatus;
    }

    public Date getBeginAnswerTime() {
        return beginAnswerTime;
    }

    public void setBeginAnswerTime(Date beginAnswerTime) {
        this.beginAnswerTime = beginAnswerTime;
    }

    public Date getEndAnswerTime() {
        return endAnswerTime;
    }

    public void setEndAnswerTime(Date endAnswerTime) {
        this.endAnswerTime = endAnswerTime;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getSysAuditTotalScore() {
        return sysAuditTotalScore;
    }

    public void setSysAuditTotalScore(Integer sysAuditTotalScore) {
        this.sysAuditTotalScore = sysAuditTotalScore;
    }

    public Integer getSysAuditScore() {
        return sysAuditScore;
    }

    public void setSysAuditScore(Integer sysAuditScore) {
        this.sysAuditScore = sysAuditScore;
    }

    public Integer getLecturerAuditTotalScore() {
        return lecturerAuditTotalScore;
    }

    public void setLecturerAuditTotalScore(Integer lecturerAuditTotalScore) {
        this.lecturerAuditTotalScore = lecturerAuditTotalScore;
    }

    public Integer getLecturerAuditScore() {
        return lecturerAuditScore;
    }

    public void setLecturerAuditScore(Integer lecturerAuditScore) {
        this.lecturerAuditScore = lecturerAuditScore;
    }

    public Long getAuditUserNo() {
        return auditUserNo;
    }

    public void setAuditUserNo(Long auditUserNo) {
        this.auditUserNo = auditUserNo;
    }

    public String getGradeExamName() {
        return gradeExamName;
    }

    public void setGradeExamName(String gradeExamName) {
        this.gradeExamName = gradeExamName == null ? null : gradeExamName.trim();
    }

    public Date getCutoffEndTime() {
        return cutoffEndTime;
    }

    public void setCutoffEndTime(Date cutoffEndTime) {
        this.cutoffEndTime = cutoffEndTime;
    }

    public Date getBeginExamTime() {
        return beginExamTime;
    }

    public void setBeginExamTime(Date beginExamTime) {
        this.beginExamTime = beginExamTime;
    }

    public Date getEndExamTime() {
        return endExamTime;
    }

    public void setEndExamTime(Date endExamTime) {
        this.endExamTime = endExamTime;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", sort=").append(sort);
        sb.append(", statusId=").append(statusId);
        sb.append(", remark=").append(remark);
        sb.append(", gradeId=").append(gradeId);
        sb.append(", gradeExamId=").append(gradeExamId);
        sb.append(", examId=").append(examId);
        sb.append(", studentId=").append(studentId);
        sb.append(", userNo=").append(userNo);
        sb.append(", viewStatus=").append(viewStatus);
        sb.append(", examStatus=").append(examStatus);
        sb.append(", beginAnswerTime=").append(beginAnswerTime);
        sb.append(", endAnswerTime=").append(endAnswerTime);
        sb.append(", score=").append(score);
        sb.append(", sysAuditTotalScore=").append(sysAuditTotalScore);
        sb.append(", sysAuditScore=").append(sysAuditScore);
        sb.append(", lecturerAuditTotalScore=").append(lecturerAuditTotalScore);
        sb.append(", lecturerAuditScore=").append(lecturerAuditScore);
        sb.append(", auditUserNo=").append(auditUserNo);
        sb.append(", gradeExamName=").append(gradeExamName);
        sb.append(", cutoffEndTime=").append(cutoffEndTime);
        sb.append(", beginExamTime=").append(beginExamTime);
        sb.append(", endExamTime=").append(endExamTime);
        sb.append(", auditStatus=").append(auditStatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
