package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级考试列表请求对象
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "GradeExamListREQ", description = "班级考试列表请求对象")
public class GradeExamPageREQ extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)")
    private Integer compensateStatus;
}
