package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.PracticeProblemRefEditREQ;
import com.roncoo.education.exam.common.req.PracticeProblemRefListREQ;
import com.roncoo.education.exam.common.req.PracticeProblemRefSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeProblemRefListRESP;
import com.roncoo.education.exam.common.resp.PracticeProblemRefViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcPracticeProblemRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 题目练习关联 Pc接口
 *
 * @author LHR
 */
@Api(tags = "PC-题目练习关联")
@RestController
@RequestMapping("/exam/pc/practice/problem/ref")
public class PcPracticeProblemRefController {

    @Autowired
    private PcPracticeProblemRefBiz biz;

    @ApiOperation(value = "题目练习关联列表", notes = "题目练习关联列表")
    @PostMapping(value = "/list")
    public Result<Page<PracticeProblemRefListRESP>> list(@RequestBody PracticeProblemRefListREQ practiceProblemRefListREQ) {
        return biz.list(practiceProblemRefListREQ);
    }

    @ApiOperation(value = "题目练习关联添加", notes = "题目练习关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody PracticeProblemRefSaveREQ practiceProblemRefSaveREQ) {
        return biz.save(practiceProblemRefSaveREQ);
    }

    @ApiOperation(value = "题目练习关联查看", notes = "题目练习关联查看")
    @GetMapping(value = "/view")
    public Result<PracticeProblemRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "题目练习关联修改", notes = "题目练习关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody PracticeProblemRefEditREQ practiceProblemRefEditREQ) {
        return biz.edit(practiceProblemRefEditREQ);
    }

    @ApiOperation(value = "题目练习关联删除", notes = "题目练习关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
