package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerLecturerAuditBO;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthGradeStudentExamAnswerViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamAndAnswerDTO;
import com.roncoo.education.exam.service.common.GradeExamCommon;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author wujing
 */
@Component
public class AuthGradeStudentExamAnswerBiz extends BaseBiz {

    @Autowired
    private GradeStudentExamAnswerDao dao;
    @Autowired
    private ExamProblemDao problemDao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeExamStudentRelationDao gradeRelationDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeTitleScoreDao;

    @Autowired
    private GradeExamDao gradeExamDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private ExamTitleProblemRefAuditDao examTitleProblemRefDao;
    @Autowired
    private GradeExamCommon gradeExamCommon;

    @Autowired
    private IFeignUser feignUser;

    /**
     * 讲师 评阅主观题（简答题、填空题）
     *
     * @param bo 讲师评卷参数
     * @return 评卷结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> lecturerAudit(AuthGradeStudentExamAnswerLecturerAuditBO bo) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(bo);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        GradeStudentExamAnswer answer = dao.getById(bo.getId());
        if (ObjectUtil.isEmpty(answer)) {
            return Result.error("答案id不正确");
        }
        if (ExamAnswerStatusEnum.COMPLETE.getCode().equals(answer.getAnswerStatus())) {
            return Result.error("答案已评阅");
        }
        ExamProblem problem = problemDao.getById(answer.getProblemId());
        if (ObjectUtil.isEmpty(problem)) {
            return Result.error("试题id不正确");
        }
        if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
            return Result.error("主观题不需要讲师评阅");
        }
        // 校验阅卷权限:讲师与学员中得管理员可阅卷
        //阅卷权限
        boolean canAudit = false;
        if (problem.getLecturerUserNo().longValue() == ThreadContext.userNo().longValue()) {
            canAudit = true;
        } else {
            GradeStudent student = gradeStudentDao.getByGradeIdAndUserNo(answer.getGradeId(), ThreadContext.userNo());
            if (ObjectUtil.isNotEmpty(student)) {
                logger.info("student--------" + student.toString());
            }
            logger.info(student.toString());
            if (!ObjectUtil.isEmpty(student) && GradeStudentUserRoleEnum.ADMIN.getCode().equals(student.getUserRole())) {
                canAudit = true;
            }
        }
        if (!canAudit) {
            return Result.error("无阅卷权限");
        }

        answer.setScore(bo.getScore());
        answer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
        answer.setSysAudit(ExamSysAuditEnum.HANDWORK.getCode());
        answer.setAuditUserNo(ThreadContext.userNo());
        dao.updateById(answer);

        // 累加分数
        GradeStudentExamTitleScore title = gradeTitleScoreDao.getByRelationIdAndTitleId(answer.getRelationId(), answer.getTitleId());
        title.setScore(title.getScore() + bo.getScore());
        gradeTitleScoreDao.updateById(title);

        GradeExamStudentRelation relation = gradeRelationDao.getById(answer.getRelationId());
        relation.setScore(relation.getScore() + bo.getScore());
        gradeRelationDao.updateById(relation);
        return Result.success("操作成功");
    }

    /**
     * 保存题目答案
     *
     * @param bo 保存答案参数
     * @return 保存结果
     */
    public Result<String> save(AuthGradeStudentExamAnswerSaveBO bo) {
        // 判断考试
        GradeExamStudentRelation relation = gradeExamStudentRelationDao.getById(bo.getRelationId());
        if (ObjectUtil.isNull(relation)) {
            return Result.error("考试不存在");
        }
        GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }

        // 获取用户考试答案
        GradeStudentExamAnswer gradeStudentExamAnswer = dao.getByRelationIdAndTitleIdProblemId(bo.getRelationId(), bo.getTitleId(), bo.getProblemId());
        if (ObjectUtil.isNotNull(gradeStudentExamAnswer)) {
            // 更新答案
            GradeStudentExamAnswer updateExamAnswer = new GradeStudentExamAnswer();
            updateExamAnswer.setId(gradeStudentExamAnswer.getId());
            updateExamAnswer.setUserAnswer(StrUtil.isBlank(bo.getUserAnswer()) ? "" : bo.getUserAnswer());
            dao.updateById(updateExamAnswer);
            return Result.success("保存成功");
        }

        // 保存新答案
        ExamTitleProblemRefAudit examTitleProblemRef = examTitleProblemRefDao.getByTitleIdAndProblemId(bo.getTitleId(), bo.getProblemId());
        if (ObjectUtil.isNull(examTitleProblemRef)) {
            return Result.error("标题题目不存在");
        }
        ExamProblem problem = problemDao.getById(bo.getProblemId());
        if (ObjectUtil.isNull(problem)) {
            return Result.error("题目不存在");
        }

        GradeStudentExamAnswer saveExamAnswer = new GradeStudentExamAnswer();
        saveExamAnswer.setRelationId(relation.getId());
        saveExamAnswer.setGradeId(gradeExam.getGradeId());
        saveExamAnswer.setGradeExamId(gradeExam.getId());
        saveExamAnswer.setStudentId(relation.getStudentId());
        saveExamAnswer.setExamId(gradeExam.getExamId());
        saveExamAnswer.setTitleId(bo.getTitleId());
        saveExamAnswer.setProblemParentId(problem.getParentId());
        saveExamAnswer.setProblemId(problem.getId());
        saveExamAnswer.setUserAnswer(bo.getUserAnswer());
        saveExamAnswer.setProblemScore(problem.getScore());
        saveExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.WAIT.getCode());
        saveExamAnswer.setSysAudit(ExamSysAuditEnum.UNKNOWN.getCode());
        dao.save(saveExamAnswer);
        return Result.success("保存成功");
    }

    /**
     * 获取答案（带题目）
     *
     * @param bo 获取考试答案参数
     * @return 试卷答案（带题目）
     */
    public Result<AuthGradeExamAndAnswerDTO> view(AuthGradeStudentExamAnswerViewBO bo) {
        // 校验用户
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("当前用户状态不可用");
        }

        // 获取考试记录
        GradeExamStudentRelation studentRelation = gradeExamStudentRelationDao.getById(bo.getRelationId());
        if (ObjectUtil.isNull(studentRelation)) {
            return Result.error("考试记录不存在");
        }
        GradeExam gradeExam = gradeExamDao.getById(studentRelation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        GradeStudent gradeStudent = null;
        if (!gradeExam.getLecturerUserNo().equals(ThreadContext.userNo())) {
            gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeExam.getGradeId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，无法查看试卷");
            }
        }
        boolean isShowProblemAnswer = false;
        if (!studentRelation.getUserNo().equals(ThreadContext.userNo())) {
            // 不是本人试卷，判断是否为讲师或者管理员
            if (!gradeExam.getLecturerUserNo().equals(ThreadContext.userNo())) {
                if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                    return Result.error("没有查看该试卷权限");
                }
                if (!checkShowExamAnswer(gradeExam, studentRelation)) {
                    return Result.error("考试未结束，不能展示答案");
                }
            }
            isShowProblemAnswer = true;
        } else if (GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
            // 本人试卷 且是管理员，允许查看
            isShowProblemAnswer = true;
        } else {
            // 不是本人试卷，不是管理员
            if (!checkShowExamAnswer(gradeExam, studentRelation)) {
                return Result.error("考试未结束，不能展示答案");
            }
            // 判断是否展示参考答案
            if (GradeExamAnswerShowEnum.EXAM_SUBMIT.getCode().equals(gradeExam.getAnswerShow())) {
                isShowProblemAnswer = true;
            } else if (GradeExamAnswerShowEnum.EXAM_COMPLETE.getCode().equals(gradeExam.getAnswerShow())) {
                if (new Date().after(gradeExam.getEndTime())) {
                    isShowProblemAnswer = true;
                }
            } else if (GradeExamAnswerShowEnum.CUSTOMIZE.getCode().equals(gradeExam.getAnswerShow())) {
                if (ObjectUtil.isNotNull(gradeExam.getAnswerShowTime()) && new Date().after(gradeExam.getAnswerShowTime())) {
                    isShowProblemAnswer = true;
                }
            }
        }

        AuthGradeExamAndAnswerDTO gradeExamInfoAudit = gradeExamCommon.getGradeExamInfoAudit(gradeExam.getExamId(), isShowProblemAnswer, true, studentRelation);
        return Result.success(gradeExamInfoAudit);
    }

    /**
     * 校验查看考试答案权限
     *
     * @param gradeExam       班级考试
     * @param studentRelation 学生考试
     * @return 校验结果
     */
    private boolean checkShowExamAnswer(GradeExam gradeExam, GradeExamStudentRelation studentRelation) {
        if (GradeExamStatusEnum.FINISH.getCode().equals(studentRelation.getExamStatus())) {
            // 考试完成
            return true;
        }

        if (!new Date().after(studentRelation.getCutoffEndTime())) {
            return false;
        }
        if (!GradeExamCompensateStatusEnum.CLOSE.getCode().equals(gradeExam.getCompensateStatus())) {
            return false;
        }
        return GradeExamStatusEnum.WAIT.getCode().equals(studentRelation.getExamStatus());
    }
}
