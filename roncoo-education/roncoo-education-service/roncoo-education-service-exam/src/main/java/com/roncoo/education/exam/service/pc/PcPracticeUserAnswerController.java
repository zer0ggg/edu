package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.PracticeUserAnswerEditREQ;
import com.roncoo.education.exam.common.req.PracticeUserAnswerListREQ;
import com.roncoo.education.exam.common.req.PracticeUserAnswerSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeUserAnswerListRESP;
import com.roncoo.education.exam.common.resp.PracticeUserAnswerViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcPracticeUserAnswerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户考试答案 Pc接口
 *
 * @author LHR
 */
@Api(tags = "PC-用户考试答案")
@RestController
@RequestMapping("/exam/pc/practice/user/answer")
public class PcPracticeUserAnswerController {

    @Autowired
    private PcPracticeUserAnswerBiz biz;

    @ApiOperation(value = "用户考试答案列表", notes = "用户考试答案列表")
    @PostMapping(value = "/list")
    public Result<Page<PracticeUserAnswerListRESP>> list(@RequestBody PracticeUserAnswerListREQ practiceUserAnswerListREQ) {
        return biz.list(practiceUserAnswerListREQ);
    }

    @ApiOperation(value = "用户考试答案添加", notes = "用户考试答案添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody PracticeUserAnswerSaveREQ practiceUserAnswerSaveREQ) {
        return biz.save(practiceUserAnswerSaveREQ);
    }

    @ApiOperation(value = "用户考试答案查看", notes = "用户考试答案查看")
    @GetMapping(value = "/view")
    public Result<PracticeUserAnswerViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户考试答案修改", notes = "用户考试答案修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody PracticeUserAnswerEditREQ practiceUserAnswerEditREQ) {
        return biz.edit(practiceUserAnswerEditREQ);
    }

    @ApiOperation(value = "用户考试答案删除", notes = "用户考试答案删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
