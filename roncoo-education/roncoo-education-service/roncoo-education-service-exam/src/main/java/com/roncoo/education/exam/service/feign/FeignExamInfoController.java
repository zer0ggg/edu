package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfo;
import com.roncoo.education.exam.feign.qo.ExamInfoQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamInfoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷信息
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamInfoController extends BaseController implements IFeignExamInfo{

    @Autowired
    private FeignExamInfoBiz biz;

	@Override
	public Page<ExamInfoVO> listForPage(@RequestBody ExamInfoQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamInfoQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamInfoQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamInfoVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public ExamInfoVO getByIdAndStatusId(@PathVariable(value = "id") Long id,@PathVariable(value = "statusId") Integer statusId) {
		return biz.getByIdAndStatusId(id,statusId);
	}
}
