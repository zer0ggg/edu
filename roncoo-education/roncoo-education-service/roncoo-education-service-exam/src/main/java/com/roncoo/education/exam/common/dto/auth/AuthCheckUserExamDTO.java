package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 校验用户考试记录
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthCheckUserExamDTO", description = "校验用户考试记录")
public class AuthCheckUserExamDTO implements Serializable {

    private static final long serialVersionUID = 1559081478560972928L;

    @ApiModelProperty(value = "是否考试中 true：有考试中、false：没有考试中", required = true)
    private Boolean isExamination;

    @ApiModelProperty(value = "考试记录ID")
    private Long recordId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;
}
