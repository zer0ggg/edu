package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamTitleProblemRefMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 试卷标题题目关联 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamTitleProblemRefDaoImpl extends AbstractBaseJdbc implements ExamTitleProblemRefDao {

    @Autowired
    private ExamTitleProblemRefMapper mapper;

    @Override
    public int save(ExamTitleProblemRef record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamTitleProblemRef record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamTitleProblemRef getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamTitleProblemRef> listForPage(int pageCurrent, int pageSize, ExamTitleProblemRefExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ExamTitleProblemRef getByTitleIdAndProblemId(Long titleId, Long problemId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemIdEqualTo(problemId);
        List<ExamTitleProblemRef> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public ExamTitleProblemRef getByTitleIdAndProblemParentIdAndProblemId(Long titleId, Long problemParentId, Long problemId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(problemParentId);
        c.andProblemIdEqualTo(problemId);
        List<ExamTitleProblemRef> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<ExamTitleProblemRef> listByTitleIdAndStatusId(Long titleId, Integer statusId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRef> listByUserNoAndStatusId(Long userNo, Integer statusId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(userNo);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, status_id desc,  id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRef> listByTitleIdOrProblemId(Long titleId, Long problemId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        if (titleId != null) {
            c.andTitleIdEqualTo(titleId);
        }
        if (problemId != null) {
            c.andProblemIdEqualTo(problemId);
        }
        example.setOrderByClause("sort asc, status_id desc,  id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByTitleId(Long titleId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByExamId(Long examId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByTitleIdAndParentProblemId(Long titleId, Long parentProblemId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemIdEqualTo(parentProblemId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public List<ExamTitleProblemRef> listByTitleId(Long titleId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        example.setOrderByClause("sort asc, status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRef> listByTitleIdAndProblemParentId(Long titleId, Long problemParentId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(problemParentId);
        example.setOrderByClause("sort asc, status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRef> listByTitleIdAndProblemParentIdAndStatusId(Long titleId, Long problemParentId, Integer statusId) {
        ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
        ExamTitleProblemRefExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(problemParentId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<Long> getByExamIdAndDuplicateProblemId(Long examId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct problem_id from exam_title_problem_ref where");
        sql.append(" exam_id = '").append(examId).append("'");
        List<Long> resultList = jdbcTemplate.query(sql.toString(), new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                return resultSet.getLong("problem_id");
            }
        });
        return resultList;
    }

}
