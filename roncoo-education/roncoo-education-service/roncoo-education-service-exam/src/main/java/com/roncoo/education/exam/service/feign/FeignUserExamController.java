package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExam;
import com.roncoo.education.exam.feign.qo.UserExamQO;
import com.roncoo.education.exam.feign.vo.UserExamVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户试卷
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamController extends BaseController implements IFeignUserExam{

    @Autowired
    private FeignUserExamBiz biz;

	@Override
	public Page<UserExamVO> listForPage(@RequestBody UserExamQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
