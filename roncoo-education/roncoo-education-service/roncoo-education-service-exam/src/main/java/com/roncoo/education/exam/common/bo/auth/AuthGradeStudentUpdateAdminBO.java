package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级设置或取消管理员
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeStudentUpdateAdminBO", description = "班级设置或取消管理员")
public class AuthGradeStudentUpdateAdminBO implements Serializable {

    private static final long serialVersionUID = 5530413255472902580L;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @NotNull(message = "学生用户编号不能为空")
    @ApiModelProperty(value = "学生用户编号", required = true)
    private Long studentUserNo;
}
