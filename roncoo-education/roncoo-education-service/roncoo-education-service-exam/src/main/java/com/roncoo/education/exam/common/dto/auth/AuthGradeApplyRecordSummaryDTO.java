package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级申请记录分页返回对象
 *
 * @author LYQ
 * @date 2020-06-01
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeApplyRecordSummaryDTO", description = "班级申请记录分页返回对象")
public class AuthGradeApplyRecordSummaryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数量")
    private int count = 0;

    @ApiModelProperty(value = "待审核 数量")
    private int waitCount = 0;

    @ApiModelProperty(value = "通过 数量")
    private int assentCount = 0;

    @ApiModelProperty(value = "拒绝 数量")
    private int refusalCount = 0;

}
