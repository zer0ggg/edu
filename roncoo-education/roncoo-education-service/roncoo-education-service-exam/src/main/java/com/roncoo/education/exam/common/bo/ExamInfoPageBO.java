package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷中心
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class ExamInfoPageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数")
    private Integer pageSize = 30;

    /**
     * 年级id
     */
    @ApiModelProperty(value = "年级id")
    private Long graId;
    /**
     * 科目id
     */
    @ApiModelProperty(value = "科目id")
    private Long subjectId;
    /**
     * 年份id
     */
    @ApiModelProperty(value = "年份id")
    private Long yearId;
    /**
     * 来源id
     */
    @ApiModelProperty(value = "来源id")
    private Long sourceId;
    /**
     * 排序类型
     */
    @ApiModelProperty(value = "排序类型(0, 综合排序),(1, 浏览次数),(2, 下载次数)")
    private Integer sortType;

    /**
     * 试卷名称
     */
    @ApiModelProperty(value = "试卷名称")
    private String examName;

}
