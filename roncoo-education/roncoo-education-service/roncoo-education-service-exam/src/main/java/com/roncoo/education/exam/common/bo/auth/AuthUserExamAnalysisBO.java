package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户试卷分析
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */

@Data
@Accessors(chain = true)
@ApiModel(value = "AuthUserExamAnalysisBO", description = "用户试卷分析")
public class AuthUserExamAnalysisBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷id")
    private Long examId;

    @ApiModelProperty(value = "讲师编号（讲师汇总时候）")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "题目类型（1：年级；2：科目； 3：年份；4：来源；5：难度；6：题类；7：考点）")
    private Integer problemCategory;

}
