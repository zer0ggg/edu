package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamTitleProblemRefAudit;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefAuditQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefAuditVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamTitleProblemRefAuditBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题题目关联审核
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamTitleProblemRefAuditController extends BaseController implements IFeignExamTitleProblemRefAudit{

    @Autowired
    private FeignExamTitleProblemRefAuditBiz biz;

	@Override
	public Page<ExamTitleProblemRefAuditVO> listForPage(@RequestBody ExamTitleProblemRefAuditQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamTitleProblemRefAuditQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamTitleProblemRefAuditQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamTitleProblemRefAuditVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
