package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamCategoryMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户试卷分类 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamCategoryDaoImpl implements UserExamCategoryDao {

    @Autowired
    private UserExamCategoryMapper mapper;

    @Override
    public int save(UserExamCategory record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExamCategory record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExamCategory getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserExamCategory> listForPage(int pageCurrent, int pageSize, UserExamCategoryExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<UserExamCategory> listByParentId(Long parentId) {
        UserExamCategoryExample example = new UserExamCategoryExample();
        UserExamCategoryExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        example.setOrderByClause("sort asc,status_id desc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<UserExamCategory> listByIds(List<Long> ids) {
        UserExamCategoryExample example = new UserExamCategoryExample();
        UserExamCategoryExample.Criteria c = example.createCriteria();
        c.andIdIn(ids);
        return this.mapper.selectByExample(example);
    }
}
