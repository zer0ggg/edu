package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.GradeOperationLogListREQ;
import com.roncoo.education.exam.common.resp.GradeOperationLogPageRESP;
import com.roncoo.education.exam.common.resp.GradeOperationLogViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeOperationLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 班级操作日志管理 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-班级操作日志管理")
@RestController
@RequestMapping("/exam/pc/grade/operation/log")
public class PcGradeOperationLogController {

    @Autowired
    private PcGradeOperationLogBiz biz;

    @ApiOperation(value = "班级操作日志列表", notes = "班级操作日志列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeOperationLogPageRESP>> list(@RequestBody GradeOperationLogListREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级操作日志查看", notes = "班级操作日志查看")
    @GetMapping(value = "/view")
    public Result<GradeOperationLogViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

}
