package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户试卷
 * </p>
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExamBO", description="用户试卷")
public class UserExamBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编码")
    private Long userNo;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;
}
