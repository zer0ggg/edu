package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class GradeStudentExamAnswer implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer sort;

    private Integer statusId;

    private String remark;

    private Long relationId;

    private Long gradeId;

    private Long gradeExamId;

    private Long studentId;

    private Long examId;

    private Long titleId;

    private Long problemParentId;

    private Long problemId;

    private Integer problemScore;

    private Integer score;

    private Integer answerStatus;

    private Integer sysAudit;

    private Long auditUserNo;

    private String userAnswer;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public Long getGradeExamId() {
        return gradeExamId;
    }

    public void setGradeExamId(Long gradeExamId) {
        this.gradeExamId = gradeExamId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public Long getProblemParentId() {
        return problemParentId;
    }

    public void setProblemParentId(Long problemParentId) {
        this.problemParentId = problemParentId;
    }

    public Long getProblemId() {
        return problemId;
    }

    public void setProblemId(Long problemId) {
        this.problemId = problemId;
    }

    public Integer getProblemScore() {
        return problemScore;
    }

    public void setProblemScore(Integer problemScore) {
        this.problemScore = problemScore;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getAnswerStatus() {
        return answerStatus;
    }

    public void setAnswerStatus(Integer answerStatus) {
        this.answerStatus = answerStatus;
    }

    public Integer getSysAudit() {
        return sysAudit;
    }

    public void setSysAudit(Integer sysAudit) {
        this.sysAudit = sysAudit;
    }

    public Long getAuditUserNo() {
        return auditUserNo;
    }

    public void setAuditUserNo(Long auditUserNo) {
        this.auditUserNo = auditUserNo;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer == null ? null : userAnswer.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", sort=").append(sort);
        sb.append(", statusId=").append(statusId);
        sb.append(", remark=").append(remark);
        sb.append(", relationId=").append(relationId);
        sb.append(", gradeId=").append(gradeId);
        sb.append(", gradeExamId=").append(gradeExamId);
        sb.append(", studentId=").append(studentId);
        sb.append(", examId=").append(examId);
        sb.append(", titleId=").append(titleId);
        sb.append(", problemParentId=").append(problemParentId);
        sb.append(", problemId=").append(problemId);
        sb.append(", problemScore=").append(problemScore);
        sb.append(", score=").append(score);
        sb.append(", answerStatus=").append(answerStatus);
        sb.append(", sysAudit=").append(sysAudit);
        sb.append(", auditUserNo=").append(auditUserNo);
        sb.append(", userAnswer=").append(userAnswer);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
