package com.roncoo.education.exam.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.bo.ExamCategoryPageBO;
import com.roncoo.education.exam.common.dto.ExamCategoryDTO;
import com.roncoo.education.exam.common.dto.ExamCategoryListDTO;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 试卷分类
 *
 * @author wujing
 */
@Component
public class ApiExamCategoryBiz extends BaseBiz {

    @Autowired
    private ExamCategoryDao dao;

    /**
     * 获取考试分类信息列表
     *
     * @return
     */
    public Result<ExamCategoryListDTO> list(ExamCategoryPageBO bo) {
        ExamCategoryListDTO dto = new ExamCategoryListDTO();
        // 根据分类类型、层级查询、分类类型、可用状态的考试分类集合
        List<ExamCategory> oneCategoryList = dao.listByFloorAndStatusIdAndCategoryTypeAndExamCategoryType(1, StatusIdEnum.YES.getCode(), bo.getCategoryType(),bo.getExamCategoryType());
        if (CollectionUtils.isEmpty(oneCategoryList)) {
            return Result.success(dto);
        }

        List<ExamCategoryDTO> examCategoryList = new ArrayList<>();

        for (ExamCategory examCategory : oneCategoryList) {
            ExamCategoryDTO examCategoryDTO = BeanUtil.copyProperties(examCategory, ExamCategoryDTO.class);
            // 递归获取考试分类
            examCategoryDTO.setChildren(recursionList(examCategory.getId()));
            examCategoryList.add(examCategoryDTO);
        }
        dto.setExamCategoryList(examCategoryList);
        return Result.success(dto);
    }

    /**
     * 递归展示分类
     *
     * @param parentId
     * @return
     */
    private List<ExamCategoryDTO> recursionList(Long parentId) {
        List<ExamCategoryDTO> list =null;
        // 根据父ID、和状态查找分类信息结合
        List<ExamCategory> examList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(examList)) {
            list = new ArrayList<>();
            for (ExamCategory examCategory : examList) {
                ExamCategoryDTO dto = BeanUtil.copyProperties(examCategory, ExamCategoryDTO.class);
                // 递归列出考试分类
                dto.setChildren(recursionList(examCategory.getId()));
                list.add(dto);
            }
        }
        return list;
    }

}
