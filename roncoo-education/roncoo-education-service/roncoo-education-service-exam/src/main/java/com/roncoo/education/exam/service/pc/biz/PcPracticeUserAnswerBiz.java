package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.PracticeUserAnswerEditREQ;
import com.roncoo.education.exam.common.req.PracticeUserAnswerListREQ;
import com.roncoo.education.exam.common.req.PracticeUserAnswerSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeUserAnswerListRESP;
import com.roncoo.education.exam.common.resp.PracticeUserAnswerViewRESP;
import com.roncoo.education.exam.service.dao.PracticeUserAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author LHR
 */
@Component
public class PcPracticeUserAnswerBiz extends BaseBiz {

    @Autowired
    private PracticeUserAnswerDao dao;

    /**
    * 用户考试答案列表
    *
    * @param practiceUserAnswerListREQ 用户考试答案分页查询参数
    * @return 用户考试答案分页查询结果
    */
    public Result<Page<PracticeUserAnswerListRESP>> list(PracticeUserAnswerListREQ practiceUserAnswerListREQ) {
        PracticeUserAnswerExample example = new PracticeUserAnswerExample();
        Criteria c = example.createCriteria();
        Page<PracticeUserAnswer> page = dao.listForPage(practiceUserAnswerListREQ.getPageCurrent(), practiceUserAnswerListREQ.getPageSize(), example);
        Page<PracticeUserAnswerListRESP> respPage = PageUtil.transform(page, PracticeUserAnswerListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 用户考试答案添加
    *
    * @param practiceUserAnswerSaveREQ 用户考试答案
    * @return 添加结果
    */
    public Result<String> save(PracticeUserAnswerSaveREQ practiceUserAnswerSaveREQ) {
        PracticeUserAnswer record = BeanUtil.copyProperties(practiceUserAnswerSaveREQ, PracticeUserAnswer.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 用户考试答案查看
    *
    * @param id 主键ID
    * @return 用户考试答案
    */
    public Result<PracticeUserAnswerViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), PracticeUserAnswerViewRESP.class));
    }


    /**
    * 用户考试答案修改
    *
    * @param practiceUserAnswerEditREQ 用户考试答案修改对象
    * @return 修改结果
    */
    public Result<String> edit(PracticeUserAnswerEditREQ practiceUserAnswerEditREQ) {
        PracticeUserAnswer record = BeanUtil.copyProperties(practiceUserAnswerEditREQ, PracticeUserAnswer.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 用户考试答案删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
