package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.api.auth.biz.AuthUserExamRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户考试记录 UserApi接口
 *
 * @author LYQ
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-考试记录管理")
@RestController
@RequestMapping("/exam/auth/user/exam/record")
public class AuthUserExamRecordController {

    @Autowired
    private AuthUserExamRecordBiz biz;

    @ApiOperation(value = "分页列出", notes = "分页列出")
    @PostMapping(value = "/list")
    public Result<Page<AuthUserExamRecordPageDTO>> listForPage(@RequestBody AuthUserExamRecordPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "在线考试", notes = "在线考试")
    @PostMapping(value = "/examOnline")
    public Result<AuthExamOnlineDTO> examOnline(@RequestBody @Valid AuthExamOnlineBO onlineBO) {
        return biz.examOnline(onlineBO);
    }

    @ApiOperation(value = "继续考试", notes = "继续考试")
    @PostMapping(value = "/continue")
    public Result<AuthExamContinueDTO> examContinue(@RequestBody @Valid AuthExamContinueBO bo) {
        return biz.examContinue(bo);
    }

    @ApiOperation(value = "保存试题答案", notes = "保存试题答案")
    @PostMapping(value = "/answer/save")
    public Result<String> saveProblemAnswer(@RequestBody @Valid AuthExamSaveProblemAnswerBO bo) {
        return biz.saveProblemAnswer(bo);
    }

    @ApiOperation(value = "考试完成", notes = "试卷完成提交")
    @RequestMapping(value = "/examFinish", method = RequestMethod.POST)
    public Result<AuthExamFinishDTO> examFinish(@RequestBody @Valid AuthExamFinishBO authExamFinishBO) {
        return biz.examFinish(authExamFinishBO);
    }

    @ApiOperation(value = "校验用户是否处于考试中", notes = "校验用户是否处于考试中")
    @PostMapping(value = "/check")
    public Result<AuthCheckUserExamDTO> checkUserExam(@RequestBody AuthCheckUserExamBO bo) {
        return biz.checkUserExam(ThreadContext.userNo());
    }

    @ApiOperation(value = "试卷分析", notes = "试卷分析")
    @RequestMapping(value = "/examAnalysis", method = RequestMethod.POST)
    public Result<AuthExamAnalysisDTO> examAnalysis(@RequestBody @Valid AuthExamAnalysisBO authExamAnalysisBO) {
        return biz.examAnalysis(authExamAnalysisBO);
    }
}
