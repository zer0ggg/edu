package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户试卷下载导出请求对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthUserExamDownloadExportBO", description = "用户试卷下载导出请求对象")
public class AuthUserExamDownloadExportBO implements Serializable {

    private static final long serialVersionUID = -701640461356876528L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;

    @NotNull(message = "是否展示答案不能为空")
    @ApiModelProperty(value = "展示答案", required = true)
    private Boolean showAnswer;
}
