package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 试卷标题审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamTitleAuditListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;
    /**
     * 状态(1:有效;0:无效)
     */
    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 试卷ID
     */
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
    /**
     * 标题排序
     */
    @ApiModelProperty(value = "标题排序")
    private Integer titleSort;
    /**
     * 标题名称
     */
    @ApiModelProperty(value = "标题名称")
    private String titleName;
    /**
     * 标题类型
     */
    @ApiModelProperty(value = "标题类型")
    private String titleType;
    /**
     * 审核状态(0:待审核,1:审核通过,2:审核不通过)
     */
    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)")
    private Integer auditStatus;
    /**
     * 审核意见
     */
    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

}
