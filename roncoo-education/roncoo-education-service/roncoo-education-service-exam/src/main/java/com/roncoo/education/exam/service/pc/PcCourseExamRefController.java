package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.CourseExamRefEditREQ;
import com.roncoo.education.exam.common.req.CourseExamRefListREQ;
import com.roncoo.education.exam.common.req.CourseExamRefSaveREQ;
import com.roncoo.education.exam.common.resp.CourseExamRefListRESP;
import com.roncoo.education.exam.common.resp.CourseExamRefViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcCourseExamRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 课程试卷关联 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-课程试卷关联")
@RestController
@RequestMapping("/exam/pc/course/exam/ref")
public class PcCourseExamRefController {

    @Autowired
    private PcCourseExamRefBiz biz;

    @ApiOperation(value = "课程试卷关联列表", notes = "课程试卷关联列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseExamRefListRESP>> list(@RequestBody CourseExamRefListREQ courseExamRefListREQ) {
        return biz.list(courseExamRefListREQ);
    }

    @ApiOperation(value = "课程试卷关联添加", notes = "课程试卷关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid CourseExamRefSaveREQ courseExamRefSaveREQ) {
        return biz.save(courseExamRefSaveREQ);
    }

    @ApiOperation(value = "课程试卷关联查看", notes = "课程试卷关联查看")
    @GetMapping(value = "/view")
    public Result<CourseExamRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程试卷关联修改", notes = "课程试卷关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseExamRefEditREQ courseExamRefEditREQ) {
        return biz.edit(courseExamRefEditREQ);
    }

    @ApiOperation(value = "课程试卷关联删除", notes = "课程试卷关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
