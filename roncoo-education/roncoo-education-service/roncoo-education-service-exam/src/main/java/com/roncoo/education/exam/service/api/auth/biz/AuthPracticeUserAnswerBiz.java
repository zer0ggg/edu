package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeSaveProblemAnswerBO;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.PracticeDao;
import com.roncoo.education.exam.service.dao.PracticeProblemRefDao;
import com.roncoo.education.exam.service.dao.PracticeUserAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author LHR
 */
@Component
public class AuthPracticeUserAnswerBiz extends BaseBiz {

    @Autowired
    private PracticeUserAnswerDao dao;
    @Autowired
    private PracticeProblemRefDao problemRefDao;
    @Autowired
    private PracticeDao practiceDao;
    @Autowired
    private ExamProblemDao examProblemDao;

    public Result<String> savePracticeAnswer(AuthPracticeSaveProblemAnswerBO bo) {
        Practice practice = practiceDao.getById(bo.getPracticeId());
        if (ObjectUtil.isNull(practice)) {
            return Result.error("获取不到该练习信息!");
        }
        ExamProblem examProblem = examProblemDao.getById(bo.getProblemId());
        if (ObjectUtil.isNull(examProblem)) {
            return Result.error("获取不到该习题信息!");
        }
        PracticeProblemRef practiceProblemRef = problemRefDao.getByUserNoAndPracticeIdAndProblemId(ThreadContext.userNo(), bo.getPracticeId(), bo.getProblemId());
        if (ObjectUtil.isNull(practiceProblemRef)) {
            return Result.error("该试题未绑定练习!");
        }

        //更新或者保存答案信息
        PracticeUserAnswer practiceUserAnswer = dao.getByUserNoAndPracticeIdAndProblemId(ThreadContext.userNo(), bo.getPracticeId(), bo.getProblemId());
        if (ObjectUtil.isNull(practiceUserAnswer)) {
            practiceUserAnswer.setUserAnswer(StrUtil.isBlank(bo.getAnswerContent()) ? "" : bo.getAnswerContent());
            dao.updateById(practiceUserAnswer);
            return Result.success("保存成功");
        }

        practiceUserAnswer = new PracticeUserAnswer();
        practiceUserAnswer.setUserNo(ThreadContext.userNo());
        practiceUserAnswer.setPracticeId(bo.getPracticeId());
        practiceUserAnswer.setProblemParentId(examProblem.getParentId());
        practiceUserAnswer.setProblemId(examProblem.getId());
        practiceUserAnswer.setProblemType(examProblem.getProblemType());
        practiceUserAnswer.setUserAnswer(bo.getAnswerContent());
        practiceUserAnswer.setProblemScore(examProblem.getScore());
        practiceUserAnswer.setScore(0);
        dao.save(practiceUserAnswer);
        return Result.success("保存成功");
    }
}
