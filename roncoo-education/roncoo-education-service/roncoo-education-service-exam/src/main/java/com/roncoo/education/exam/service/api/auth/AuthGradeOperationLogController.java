package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.exam.service.api.auth.biz.AuthGradeOperationLogBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级操作记录管理")
@RestController
@RequestMapping("/exam/auth/grade/operation/log")
public class AuthGradeOperationLogController {

    @Autowired
    private AuthGradeOperationLogBiz biz;

}
