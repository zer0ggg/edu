package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 获取课时code值实体类
 *
 * @author forest
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamVideoSignBO", description = "试卷视频播放Sign获取请求对象")
public class AuthExamVideoSignBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;

    @NotNull(message = "题目ID不能为空")
    @ApiModelProperty(value = "题目ID", required = true)
    private Long problemId;

    @NotNull(message = "视频VID不能为空")
    @ApiModelProperty(value = "视频VID", required = true)
    private String videoVid;

    @ApiModelProperty(value = "播放IP地址")
    private String ip = "127.0.0.1";

}
