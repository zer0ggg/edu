package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeStudentPageDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeStudentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级学生管理")
@RestController
@RequestMapping("/exam/auth/grade/student")
public class AuthGradeStudentController {

    @Autowired
    private AuthGradeStudentBiz biz;

    @ApiOperation(value = "班级学生分页列出", notes = "班级学生分页列出")
    @PostMapping(value = "/page")
    public Result<Page<AuthGradeStudentPageDTO>> listForPage(@RequestBody @Valid AuthGradeStudentPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "加入班级", notes = "加入班级")
    @PostMapping(value = "/grade/add")
    public Result<String> gradeAdd(@RequestBody @Valid AuthGradeStudentAddGradeBO bo) {
        return biz.gradeAdd(bo);
    }

    @ApiOperation(value = "(讲师或管理员)修改学生信息", notes = "修改学生信息")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid AuthGradeStudentEditBO bo) {
        return biz.edit(bo);
    }

    @ApiOperation(value = "退出班级", notes = "退出班级")
    @PutMapping(value = "/quit")
    public Result<String> quit(@RequestBody @Valid AuthGradeStudentQuitBO bo) {
        return biz.quit(bo);
    }

    @ApiOperation(value = "设置或取消管理员", notes = "设置或取消管理员")
    @PutMapping(value = "/update/admin")
    public Result<String> updateAdmin(@RequestBody @Valid AuthGradeStudentUpdateAdminBO bo) {
        return biz.updateAdmin(bo);
    }

    @ApiOperation(value = "踢出班级", notes = "踢出班级")
    @PutMapping(value = "/kick/out")
    public Result<String> kickOut(@RequestBody @Valid AuthGradeStudentKickOutBO bo) {
        return biz.kickOut(bo);
    }

    @ApiOperation(value = "是否是管理员", notes = "是否是管理员")
    @PutMapping(value = "/isAdmin")
    public Result<Boolean> isAdmin(@RequestBody @Valid AuthGradeStudentIsAdminBO bo) {
        return biz.isAdmin(bo);
    }

    @ApiOperation(value = "导入用户信息", notes = "导入用户信息")
    @SysLog(value = "导入用户信息")
    @PostMapping(value = "/upload/excel")
    public Result<String> uploadExcel(@RequestParam("file") MultipartFile file) {
        return biz.uploadExcel(file);
    }

}
