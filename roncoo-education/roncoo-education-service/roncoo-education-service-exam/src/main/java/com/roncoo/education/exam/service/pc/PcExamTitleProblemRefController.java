package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefListREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamTitleProblemRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷标题题目关联 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷标题题目关联管理")
@RestController
@RequestMapping("/exam/pc/exam/title/problem/ref")
public class PcExamTitleProblemRefController {

    @Autowired
    private PcExamTitleProblemRefBiz biz;

    @ApiOperation(value = "试卷标题题目关联列表", notes = "试卷标题题目关联列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamTitleProblemRefListRESP>> list(@RequestBody ExamTitleProblemRefListREQ examTitleProblemRefListREQ) {
        return biz.list(examTitleProblemRefListREQ);
    }

    @ApiOperation(value = "试卷标题题目关联添加", notes = "试卷标题题目关联添加")
    @SysLog(value = "试卷标题题目关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamTitleProblemRefSaveREQ examTitleProblemRefSaveREQ) {
        return biz.save(examTitleProblemRefSaveREQ);
    }

    @ApiOperation(value = "试卷标题题目关联查看", notes = "试卷标题题目关联查看")
    @GetMapping(value = "/view")
    public Result<ExamTitleProblemRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷标题题目关联修改", notes = "试卷标题题目关联修改")
    @SysLog(value = "试卷标题题目关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamTitleProblemRefEditREQ examTitleProblemRefEditREQ) {
        return biz.edit(examTitleProblemRefEditREQ);
    }

    @ApiOperation(value = "试卷标题题目关联删除", notes = "试卷标题题目关联删除")
    @SysLog(value = "试卷标题题目关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
