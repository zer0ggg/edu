package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级用户考试成绩分页请求对象
 *
 * @author hsq
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeExamStudentRelationGradeForPageBO", description = "班级用户考试成绩分页请求对象")
public class AuthGradeExamStudentRelationGradeForPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 5861473380764088728L;

    @ApiModelProperty(value = "学员名称")
    private String nickname;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "班级考试ID")
    private Long gradeExamId;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;
}
