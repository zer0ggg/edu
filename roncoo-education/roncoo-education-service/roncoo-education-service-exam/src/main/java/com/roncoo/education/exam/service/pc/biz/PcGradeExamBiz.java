package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ExamAuditTypeEnum;
import com.roncoo.education.common.core.enums.GradeExamAnswerShowEnum;
import com.roncoo.education.common.core.enums.GradeExamCompensateStatusEnum;
import com.roncoo.education.common.core.enums.GradeExamStatusEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.GradeExamEditREQ;
import com.roncoo.education.exam.common.req.GradeExamPageREQ;
import com.roncoo.education.exam.common.resp.GradeExamPageRESP;
import com.roncoo.education.exam.common.resp.GradeExamViewRESP;
import com.roncoo.education.exam.service.dao.GradeExamDao;
import com.roncoo.education.exam.service.dao.GradeExamStudentRelationDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wujing
 */
@Component
public class PcGradeExamBiz extends BaseBiz {

    @Autowired
    private GradeExamDao dao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;

    /**
     * 列表
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeExamPageRESP>> list(GradeExamPageREQ req) {
        GradeExamExample example = new GradeExamExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(req.getStatusId())) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (ObjectUtil.isNotNull(req.getLecturerUserNo())) {
            c.andLecturerUserNoEqualTo(req.getLecturerUserNo());
        }
        if (ObjectUtil.isNotNull(req.getGradeId())) {
            c.andGradeIdEqualTo(req.getGradeId());
        }
        if (StrUtil.isNotBlank(req.getGradeExamName())) {
            c.andGradeExamNameLike(PageUtil.like(req.getGradeExamName()));
        }
        if (ObjectUtil.isNotNull(req.getAnswerShow())) {
            c.andAnswerShowEqualTo(req.getAnswerShow());
        }
        if (ObjectUtil.isNotNull(req.getAuditType())) {
            c.andAuditTypeEqualTo(req.getAuditType());
        }
        if (ObjectUtil.isNotNull(req.getCompensateStatus())) {
            c.andCompensateStatusEqualTo(req.getCompensateStatus());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeExam> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeExamPageRESP> respPage = PageUtil.transform(page, GradeExamPageRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }

        for (GradeExamPageRESP resp : respPage.getList()) {
            // 统计参考人员
            resp.setStudentCount(gradeExamStudentRelationDao.countByGradeExamId(resp.getId()));

            // 统计考试完成人员
            resp.setCompleteCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(resp.getId(), GradeExamStatusEnum.FINISH.getCode()));
        }
        return Result.success(respPage);
    }

    /**
     * 班级考试查看
     *
     * @param id 主键ID
     * @return 班级考试
     */
    public Result<GradeExamViewRESP> view(Long id) {
        GradeExamViewRESP resp = BeanUtil.copyProperties(dao.getById(id), GradeExamViewRESP.class);
        // 考试人数
        if (ObjectUtil.isNotNull(resp)) {
            resp.setStudentCount(gradeExamStudentRelationDao.countByGradeExamId(resp.getId()));

            // 统计考试完成人员
            resp.setCompleteCount(gradeExamStudentRelationDao.countByGradeExamIdAndExamStatus(resp.getId(), GradeExamStatusEnum.FINISH.getCode()));

        }
        return Result.success(resp);
    }

    /**
     * 班级考试修改
     *
     * @param req 修改对象
     * @return 修改结果
     */
    public Result<String> edit(GradeExamEditREQ req) {
        // 判断考试时间
        if (!req.getBeginTime().before(req.getEndTime())) {
            return Result.error("结束时间必须大于开始时间");
        }

        // 答案展示
        GradeExamAnswerShowEnum showEnum = GradeExamAnswerShowEnum.byCode(req.getAnswerShow());
        if (ObjectUtil.isNull(showEnum)) {
            return Result.error("答案展示类型不正确");
        }
        if (GradeExamAnswerShowEnum.CUSTOMIZE.getCode().equals(req.getAnswerShow())) {
            if (req.getAnswerShowTime() == null) {
                return Result.error("答案展示时间不能为空");
            }
        }

        // 评阅类型
        ExamAuditTypeEnum typeEnum = ExamAuditTypeEnum.byCode(req.getAuditType());
        if (ObjectUtil.isNull(typeEnum)) {
            return Result.error("评阅类型错误");
        }

        // 补交类型
        GradeExamCompensateStatusEnum statusEnum = GradeExamCompensateStatusEnum.byCode(req.getCompensateStatus());
        if (ObjectUtil.isNull(statusEnum)) {
            return Result.error("补交类型错误");
        }

        // 修改
        GradeExam gradeExam = BeanUtil.copyProperties(req, GradeExam.class);
        if (dao.updateById(gradeExam) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }
}
