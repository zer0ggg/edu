package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamCategory;
import com.roncoo.education.exam.feign.qo.ExamCategoryQO;
import com.roncoo.education.exam.feign.vo.ExamCategoryVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamCategoryBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷分类
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamCategoryController extends BaseController implements IFeignExamCategory{

    @Autowired
    private FeignExamCategoryBiz biz;

	@Override
	public Page<ExamCategoryVO> listForPage(@RequestBody ExamCategoryQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamCategoryQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamCategoryQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamCategoryVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
