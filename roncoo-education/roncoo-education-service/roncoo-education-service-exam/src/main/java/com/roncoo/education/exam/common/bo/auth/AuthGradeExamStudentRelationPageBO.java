package com.roncoo.education.exam.common.bo.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 班级考试学生关联分页请求对象
 *
 * @author LYQ
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeExamStudentRelationPageBO", description = "班级考试学生关联分页请求对象")
public class AuthGradeExamStudentRelationPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 5861473380764088728L;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;


    //发布试卷的开始时间和结束时间
    @ApiModelProperty(value = "开始考试时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginExamTime;

    @ApiModelProperty(value = "结束考试时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endExamTime;
}
