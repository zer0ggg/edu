package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 组卷 时 统计实体
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class SaveRefCountBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目数量")
    private int problemQuantity = 0;

}
