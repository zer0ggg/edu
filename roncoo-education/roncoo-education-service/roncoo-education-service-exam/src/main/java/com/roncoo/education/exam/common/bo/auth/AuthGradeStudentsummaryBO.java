package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeStudentPageBO", description = "班级学生分页列出")
public class AuthGradeStudentsummaryBO extends PageParam implements Serializable {

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @ApiModelProperty(value = "用户角色")
    private Integer userRole;

    @ApiModelProperty(value = "昵称")
    private String nickname;
}
