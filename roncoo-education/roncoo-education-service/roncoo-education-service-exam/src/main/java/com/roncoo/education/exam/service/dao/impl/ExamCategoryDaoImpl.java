package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamCategoryMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 试卷分类 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamCategoryDaoImpl implements ExamCategoryDao {

    @Autowired
    private ExamCategoryMapper mapper;

    @Override
    public int save(ExamCategory record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamCategory record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamCategory getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamCategory> listForPage(int pageCurrent, int pageSize, ExamCategoryExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    /**
     * 根据父分类ID查找考试分类列表
     */
    @Override
    public List<ExamCategory> listByParentId(Long parentId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    /**
     * 根据层级查找考试分类列表
     */
    @Override
    public List<ExamCategory> listByFloor(Integer floor) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andFloorEqualTo(floor);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    /**
     * 根据层级和分类ID查找考试分类列表
     */
    @Override
    public List<ExamCategory> listByFloorAndCategoryId(Integer floor, Long parentId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andFloorEqualTo(floor);
        c.andParentIdEqualTo(parentId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    /**
     * 根据类型、层级、状态查找考试分类
     */
    @Override
    public List<ExamCategory> listByFloorAndStatusId(Integer floor, Integer statusId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andFloorEqualTo(floor);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    /**
     * 根据父分类ID和状态查找考试分类信息列表
     */
    @Override
    public List<ExamCategory> listByParentIdAndStatusId(Long parentId, Integer statusId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamCategory> listByExamCategoryTypeAndFloorAndCategoryId(Integer examCategoryType, Integer floor,
                                                                          Long parentId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andExamCategoryTypeEqualTo(examCategoryType);
        c.andFloorEqualTo(floor);
        c.andParentIdEqualTo(parentId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamCategory> listByFloorAndStatusIdAndCategoryTypeAndExamCategoryType(int floor, Integer statusId,
                                                                                       Integer categoryType,Integer examCategoryType) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(categoryType)) {
            c.andCategoryTypeEqualTo(categoryType);
        }
        if (ObjectUtil.isNotNull(examCategoryType)) {
            c.andExamCategoryTypeEqualTo(examCategoryType);
        }
        c.andFloorEqualTo(floor);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public ExamCategory getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(Integer categoryType, Integer examCategoryType, Long parentId, String categoryName) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(categoryType)) {
            c.andCategoryTypeEqualTo(categoryType);
        }
        if (ObjectUtil.isNotNull(examCategoryType)) {
            c.andExamCategoryTypeEqualTo(examCategoryType);
        }
        if (ObjectUtil.isNotNull(parentId)) {
            c.andParentIdEqualTo(parentId);
        }
        if (StringUtils.hasText(categoryName)) {
            c.andCategoryNameEqualTo(categoryName);
        }
        example.setOrderByClause("status_id desc, sort asc, id desc");
        List<ExamCategory> list = this.mapper.selectByExample(example);
        if(CollectionUtil.isEmpty(list)){
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<ExamCategory> listByExamCategoryTypeAndFloorAndStatusId(Integer examCategoryType, int floor, Integer statusId) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andExamCategoryTypeEqualTo(examCategoryType);
        c.andFloorEqualTo(floor);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public  List<ExamCategory> listByIdAndCategoryType(Long id, Integer examCategoryType) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(id);
        c.andExamCategoryTypeEqualTo(examCategoryType);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamCategory> listCategoryTypeAll(Integer categoryType) {
        ExamCategoryExample example = new ExamCategoryExample();
        ExamCategoryExample.Criteria c = example.createCriteria();
        c.andCategoryTypeEqualTo(categoryType);
        return this.mapper.selectByExample(example);
    }

}
