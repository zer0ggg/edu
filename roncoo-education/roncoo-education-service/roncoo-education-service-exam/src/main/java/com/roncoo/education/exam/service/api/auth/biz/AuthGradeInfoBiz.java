package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.ReferralCodeUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeInfoPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeInfoViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeStudentGradeInfoAdminPageDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLog;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wujing
 */
@Component
public class AuthGradeInfoBiz extends BaseBiz {

    @Autowired
    private GradeInfoDao dao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeExamDao gradeExamDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeStudentExamAnswerDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private GradeOperationLogDao gradeOperationLogDao;
    @Autowired
    private GradeApplyRecordDao gradeApplyRecordDao;

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignUser feignUser;

    /**
     * 分页列出--查询讲师创建的班级
     *
     * @param bo 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthGradeInfoPageDTO>> listForPage(AuthGradeInfoPageBO bo) {
        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        if (StrUtil.isNotBlank(bo.getGradeName())) {
            c.andGradeNameLike(PageUtil.like(bo.getGradeName()));
        }
        if (ObjectUtil.isNotNull(bo.getVerificationSet())) {
            c.andVerificationSetEqualTo(bo.getVerificationSet());
        }
        if (StrUtil.isNotBlank(bo.getInvitationCode())) {
            c.andInvitationCodeLike(PageUtil.like(bo.getInvitationCode()));
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeInfo> gradeInfoPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthGradeInfoPageDTO> resultPage = PageUtil.transform(gradeInfoPage, AuthGradeInfoPageDTO.class);
        if (CollectionUtil.isEmpty(resultPage.getList())) {
            return Result.success(resultPage);
        }

        Map<Long, LecturerVO> lecturerCacheMap = new HashMap<>();
        for (AuthGradeInfoPageDTO dto : resultPage.getList()) {
            // 获取讲师名称
            LecturerVO lecturerVO = lecturerCacheMap.get(dto.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturerVO)) {
                lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            }
            if (ObjectUtil.isNotNull(lecturerVO)) {
                dto.setLecturerName(lecturerVO.getLecturerName());
                lecturerCacheMap.put(lecturerVO.getLecturerUserNo(), lecturerVO);
            }
            // 统计班级管理
            dto.setAdminCount(gradeStudentDao.countByGradeIdAndUserRole(dto.getId(), GradeStudentUserRoleEnum.ADMIN.getCode()));

            // 班级学生
            dto.setStudentCount(gradeStudentDao.countByGradeId(dto.getId()));

            //统计各个班级  待审核申请的数量
            dto.setToAuditCount(gradeApplyRecordDao.countByGradeIdAndAudit(dto.getId(), GradeApplyAuditStatusEnum.WAIT.getCode()));

        }
        return Result.success(resultPage);
    }

    /**
     * 分页列出--查询讲师创建的班级
     *
     * @param bo 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthGradeInfoPageDTO>> listAllGradeForPage(AuthGradeStudentGradeInfoByRolePageBO bo) {
        List<GradeStudent> gradeStudentList;
        if (bo.getUserRole() == null || bo.getUserRole() == 0) {
            //管理员和学员身份
            gradeStudentList = gradeStudentDao.listByUserNoAndUserRoleAndStatusId(ThreadContext.userNo(), null, StatusIdEnum.YES.getCode());
        } else if (GradeStudentUserRoleEnum.ADMIN.getCode().equals(bo.getUserRole())) {
            //管理员身份
            gradeStudentList = gradeStudentDao.listByUserNoAndUserRoleAndStatusId(ThreadContext.userNo(), GradeStudentUserRoleEnum.ADMIN.getCode(), StatusIdEnum.YES.getCode());
        } else {
            //学员身份
            gradeStudentList = gradeStudentDao.listByUserNoAndUserRoleAndStatusId(ThreadContext.userNo(), GradeStudentUserRoleEnum.USER.getCode(), StatusIdEnum.YES.getCode());
        }
        if (CollectionUtil.isEmpty(gradeStudentList)) {
            return Result.success(new Page<>());
        }
        List<Long> gradeIdList = gradeStudentList.stream().map(GradeStudent::getGradeId).collect(Collectors.toList());

        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andIdIn(gradeIdList);
        if (StrUtil.isNotBlank(bo.getGradeName())) {
            c.andGradeNameLike(PageUtil.like(bo.getGradeName()));
        }
        if (ObjectUtil.isNotNull(bo.getVerificationSet())) {
            c.andVerificationSetEqualTo(bo.getVerificationSet());
        }
        if (StrUtil.isNotBlank(bo.getInvitationCode())) {
            c.andInvitationCodeLike(PageUtil.like(bo.getInvitationCode()));
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeInfo> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthGradeInfoPageDTO> resultPage = PageUtil.transform(page, AuthGradeInfoPageDTO.class);
        if (CollectionUtil.isEmpty(resultPage.getList())) {
            return Result.success(resultPage);
        }

        Map<Long, LecturerVO> lecturerCacheMap = new HashMap<>();
        Map<Long, UserExtVO> userExtCacheMap = new HashMap<>();
        for (AuthGradeInfoPageDTO dto : resultPage.getList()) {
            // 获取讲师名称
            LecturerVO lecturerVO = lecturerCacheMap.get(dto.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturerVO)) {
                lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            }
            if (ObjectUtil.isNotNull(lecturerVO)) {
                dto.setLecturerName(lecturerVO.getLecturerName());
                lecturerCacheMap.put(lecturerVO.getLecturerUserNo(), lecturerVO);
            }

            // 统计班级管理
            dto.setAdminCount(gradeStudentDao.countByGradeIdAndUserRole(dto.getId(), GradeStudentUserRoleEnum.ADMIN.getCode()));

            // 班级学生
            dto.setStudentCount(gradeStudentDao.countByGradeId(dto.getId()));
            //统计各个班级  待审核申请的数量
            dto.setToAuditCount(gradeApplyRecordDao.countByGradeIdAndAudit(dto.getId(), GradeApplyAuditStatusEnum.WAIT.getCode()));

            //计算 班级下某学员  未考试的试卷数量
            dto.setUndoExamCount(gradeExamStudentRelationDao.countByGradeIdAndUserNoAndExamStatus(dto.getId(), ThreadContext.userNo(), GradeExamStatusEnum.WAIT.getCode()));

            //管理员信息
            List<UserExtVO> adminList = new ArrayList<>();
            List<GradeStudent> studentList = gradeStudentDao.listByGradeIdAndUserRole(dto.getId(), GradeStudentUserRoleEnum.ADMIN.getCode());
            for (GradeStudent student : studentList) {
                // 获取管理名称
                UserExtVO adminVO = userExtCacheMap.get(dto.getLecturerUserNo());
                if (null == adminVO) {
                    adminVO = feignUserExt.getByUserNo(student.getUserNo());
                }
                if (ObjectUtil.isNotNull(adminVO)) {
                    userExtCacheMap.put(adminVO.getUserNo(), adminVO);
                    adminList.add(adminVO);
                }
            }
            dto.setAdminList(adminList);
        }
        return Result.success(resultPage);
    }

    /**
     * 普通学员  参加的班级
     *
     * @param bo 分页请求参数
     * @return 分页结果
     */
    public Result<Page<AuthGradeStudentGradeInfoAdminPageDTO>> listGradeInfoForPage(AuthGradeStudentGradeInfoPageBO bo) {
        List<GradeStudent> gradeStudentList = gradeStudentDao.listByUserNoAndUserRole(ThreadContext.userNo(), GradeStudentUserRoleEnum.ADMIN.getCode());
        if (CollectionUtil.isEmpty(gradeStudentList)) {
            return Result.success(new Page<>());
        }
        List<Long> gradeIdList = gradeStudentList.stream().map(GradeStudent::getGradeId).collect(Collectors.toList());

        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andIdIn(gradeIdList);
        if (StrUtil.isNotBlank(bo.getGradeName())) {
            c.andGradeNameLike(PageUtil.like(bo.getGradeName()));
        }
        if (ObjectUtil.isNotNull(bo.getVerificationSet())) {
            c.andVerificationSetEqualTo(bo.getVerificationSet());
        }
        if (StrUtil.isNotBlank(bo.getInvitationCode())) {
            c.andInvitationCodeLike(PageUtil.like(bo.getInvitationCode()));
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeInfo> resultPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(resultPage, AuthGradeStudentGradeInfoAdminPageDTO.class));
    }

    /**
     * 获取班级信息详情
     *
     * @param bo 详情获取参数
     * @return 班级信息详情
     */
    public Result<AuthGradeInfoViewDTO> view(AuthGradeInfoViewBO bo) {
        GradeInfo gradeInfo = dao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }

        // 若是讲师获取，直接返回
        if (gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.success(BeanUtil.copyProperties(gradeInfo, AuthGradeInfoViewDTO.class));
        }

        // 判断是否为班级成员
        GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("不是班级成员，不能获取班级信息");
        }
        return Result.success(BeanUtil.copyProperties(gradeInfo, AuthGradeInfoViewDTO.class));
    }


    /**
     * 创建班级
     *
     * @param bo 创建信息
     * @return 创建结果
     */
    public Result<String> save(AuthGradeInfoSaveBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("班级添加-->用户：[{}]不存在，不能添加班级", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("班级添加-->用户：[{}]--状态：[{}]，用户状态异常，不能添加班级", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 判断是否为讲师
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            logger.warn("班级添加-->用户：[{}]不是讲师，不能添加", ThreadContext.userNo());
            return Result.error("用户不是讲师，不能添加班级");
        }

        // 判断验证类型
        GradeVerificationSetEnum verificationSetEnum = GradeVerificationSetEnum.byCode(bo.getVerificationSet());
        if (ObjectUtil.isNull(verificationSetEnum)) {
            return Result.error("班级权限类型错误，请传入正确的权限类型");
        }

        // 添加班级
        GradeInfo gradeInfo = new GradeInfo();
        gradeInfo.setLecturerUserNo(ThreadContext.userNo());
        gradeInfo.setGradeName(bo.getGradeName());
        gradeInfo.setGradeIntro(bo.getGradeIntro());
        gradeInfo.setVerificationSet(bo.getVerificationSet());
        gradeInfo.setInvitationCode(ReferralCodeUtil.getStringRandom());
        dao.save(gradeInfo);

        // 添加班级日志
        createSaveGradeInfoLog(gradeInfo, lecturerVO);

        return Result.success("添加班级成功");
    }

    /**
     * 解散班级
     *
     * @param bo 解散班级参数
     * @return 解散结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> disband(AuthGradeInfoDisbandBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("班级解散-->用户：[{}]不存在，不能解散班级", ThreadContext.userNo());
            return Result.error("用户不存在，不能解散班级");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("班级解散-->用户：[{}]--状态：[{}]，用户状态异常，不能解散班级", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常，不能解散班级");
        }
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            logger.warn("班级解散-->用户：[{}]--状态：[{}]，讲师信息不存在，不能解散班级", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("讲师信息异常，不能解散班级");
        }

        //判断班级
        GradeInfo gradeInfo = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在，删除失败！");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有删除班级权限");
        }

        // 删除班级学生、向学生发生解散站内信
        List<GradeStudent> gradeStudentList = gradeStudentDao.listByGradeId(gradeInfo.getId());
        if (CollectionUtil.isNotEmpty(gradeStudentList)) {
            for (GradeStudent student : gradeStudentList) {
                //TODO 需要发送站内信，（需要修改站内信的表结构、添加是否展示给后台的字段、添加系统消息类型）
            }
            gradeStudentDao.deleteByGradeId(gradeInfo.getId());
        }

        // 删除班级考试
        gradeExamDao.deleteByGradeId(gradeInfo.getId());
        // 删除学生考试记录
        gradeExamStudentRelationDao.deleteByGradeId(gradeInfo.getId());
        // 删除用户考试标题得分
        gradeStudentExamTitleScoreDao.deleteByGradeId(gradeInfo.getId());
        // 删除用户答案
        gradeStudentExamAnswerDao.deleteByGradeId(gradeInfo.getId());

        // 删除班级、添加删除日志
        dao.deleteById(gradeInfo.getId());
        createDisbandGradeInfoLog(gradeInfo, lecturerVO);
        return Result.success("解散成功");
    }

    /**
     * 修改班级信息
     *
     * @param bo 修改参数
     * @return 修改结果
     */
    public Result<String> update(AuthGradeInfoUpdateBO bo) {
        // 判断验证设置
        GradeVerificationSetEnum verificationSetEnum = GradeVerificationSetEnum.byCode(bo.getVerificationSet());
        if (ObjectUtil.isNull(verificationSetEnum)) {
            return Result.error("班级权限类型错误，请传入正确的班级权限");
        }

        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("修改班级信息-->用户：[{}]不存在，不能修改班级信息",ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("修改班级信息-->用户：[{}]--状态：[{}]，用户状态异常，不能修改班级信息", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 判断班级
        GradeInfo gradeInfo = dao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("当前班级状态不可用");
        }
        GradeStudent gradeAdminStudent = null;
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            // 不是用户，判断是否为管理员
            gradeAdminStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeAdminStudent)) {
                return Result.error("不是班级人员，没有操作权限");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeAdminStudent.getUserRole())) {
                return Result.error("不是班级管理人员，没有操作权限");
            }
        }

        // 修改班级信息
        GradeInfo updateGradeInfo = new GradeInfo();
        updateGradeInfo.setId(bo.getGradeId());
        updateGradeInfo.setGradeName(bo.getGradeName());
        updateGradeInfo.setGradeIntro(bo.getGradeIntro());
        updateGradeInfo.setVerificationSet(bo.getVerificationSet());
        dao.updateById(updateGradeInfo);

        // 记录日志
        createGradeInfoUpdateLog(gradeInfo, gradeAdminStudent, bo);

        return Result.success("修改成功");
    }

    /**
     * 保存创建班级日志
     *
     * @param gradeInfo  班级信息
     * @param lecturerVO 讲师信息
     */
    private void createSaveGradeInfoLog(GradeInfo gradeInfo, LecturerVO lecturerVO) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.CREATE_GRADE.getCode());

            // 操作内容
            String operationContent = "讲师：【" +
                    lecturerVO.getLecturerName() +
                    "(" +
                    lecturerVO.getLecturerUserNo() +
                    ")" +
                    "】创建班级【" +
                    gradeInfo.getGradeName() +
                    "(" +
                    gradeInfo.getId() +
                    ")" +
                    "】";
            operationLog.setOperationContent(operationContent);
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("创建班级，添加日志失败！", e);
        }
    }

    /**
     * 保存解散班级日志
     *
     * @param gradeInfo  班级信息
     * @param lecturerVO 讲师信息
     */
    private void createDisbandGradeInfoLog(GradeInfo gradeInfo, LecturerVO lecturerVO) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.DISBAND_GRADE.getCode());

            // 操作内容
            String operationContent = "讲师：【" +
                    lecturerVO.getLecturerName() +
                    "(" +
                    lecturerVO.getLecturerUserNo() +
                    ")" +
                    "】解散班级【" +
                    gradeInfo.getGradeName() +
                    "(" +
                    gradeInfo.getId() +
                    ")" +
                    "】";
            operationLog.setOperationContent(operationContent);
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("讲师【{}】解散班级【{}】，添加日志失败！", gradeInfo.getLecturerUserNo(), gradeInfo.getId(), e);
        }
    }

    /**
     * 创建班级信息更新日志
     *
     * @param gradeInfo         班级信息
     * @param gradeAdminStudent 班级管理员信息
     * @param bo                修改参数
     */
    private void createGradeInfoUpdateLog(GradeInfo gradeInfo, GradeStudent gradeAdminStudent, AuthGradeInfoUpdateBO bo) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(ThreadContext.userNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.UPDATE_GRADE.getCode());

            // 操作内容
            StringBuilder operationContent = new StringBuilder();
            operationContent.append("班级：【")
                    .append(gradeInfo.getGradeName())
                    .append("(")
                    .append(gradeInfo.getId())
                    .append(")】");
            if (gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
                operationContent.append("讲师");
            } else {
                operationContent.append("管理员【")
                        .append(gradeAdminStudent.getNickname())
                        .append("(")
                        .append(gradeAdminStudent.getUserNo())
                        .append(")】");
            }
            operationContent.append("修改信息-->班级名称：【")
                    .append(bo.getGradeName())
                    .append("】--班级简介：【")
                    .append(bo.getGradeIntro())
                    .append("】--验证方式：【")
                    .append(bo.getVerificationSet())
                    .append("】");
            operationLog.setOperationContent(operationContent.toString());
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("班级【{}（{}）】被（讲师/管理员）【{}】修改信息,修改为-->班级名称【{}】--班级简介【{}】--验证设置【{}】，添加日志失败！", gradeInfo.getGradeName(), gradeInfo.getId(), ThreadContext.loginName(), bo.getGradeName(), bo.getGradeIntro(), bo.getVerificationSet(), e);
        }
    }

    public Result<AuthGradeInfoViewDTO> viewByInvitationCode(AuthGradeInfoViewByInvitationCodeBO bo) {
        GradeInfo gradeInfo = dao.getByInvitationCode(bo.getInvitationCode());
        return Result.success(BeanUtil.copyProperties(gradeInfo, AuthGradeInfoViewDTO.class));
    }
}
