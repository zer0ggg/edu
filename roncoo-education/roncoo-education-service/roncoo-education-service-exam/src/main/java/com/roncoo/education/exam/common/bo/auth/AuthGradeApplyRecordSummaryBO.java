package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 申请加入班级审核对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeApplyRecordSummaryBO", description = "申请加入班级审核对象  数量统计")
public class AuthGradeApplyRecordSummaryBO implements Serializable {

    private static final long serialVersionUID = -1450921553475079492L;

    @NotNull(message = "班级id不能为空")
    @ApiModelProperty(value = "班级id", required = true)
    private Long gradeId;
}
