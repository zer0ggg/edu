package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExamTitleScore;
import com.roncoo.education.exam.feign.qo.UserExamTitleScoreQO;
import com.roncoo.education.exam.feign.vo.UserExamTitleScoreVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamTitleScoreBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户考试标题得分
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamTitleScoreController extends BaseController implements IFeignUserExamTitleScore{

    @Autowired
    private FeignUserExamTitleScoreBiz biz;

	@Override
	public Page<UserExamTitleScoreVO> listForPage(@RequestBody UserExamTitleScoreQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamTitleScoreQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamTitleScoreQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamTitleScoreVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
