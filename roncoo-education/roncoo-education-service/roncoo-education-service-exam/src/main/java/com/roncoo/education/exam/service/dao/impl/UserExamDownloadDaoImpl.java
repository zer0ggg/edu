package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamDownloadDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamDownloadMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownload;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 用户试卷下载 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamDownloadDaoImpl implements UserExamDownloadDao {

    @Autowired
    private UserExamDownloadMapper mapper;

    @Override
    public int save(UserExamDownload record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExamDownload record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExamDownload getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserExamDownload> listForPage(int pageCurrent, int pageSize, UserExamDownloadExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


}
