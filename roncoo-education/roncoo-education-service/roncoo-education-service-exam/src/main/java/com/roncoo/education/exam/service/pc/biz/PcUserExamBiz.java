package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamEditREQ;
import com.roncoo.education.exam.common.req.UserExamListREQ;
import com.roncoo.education.exam.common.req.UserExamSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamListRESP;
import com.roncoo.education.exam.common.resp.UserExamViewRESP;
import com.roncoo.education.exam.service.dao.UserExamDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷
 *
 * @author wujing
 */
@Component
public class PcUserExamBiz extends BaseBiz {

	@Autowired
	private UserExamDao dao;

	/**
	 * 用户试卷列表
	 *
	 * @param userExamListREQ 用户试卷分页查询参数
	 * @return 用户试卷分页查询结果
	 */
	public Result<Page<UserExamListRESP>> list(UserExamListREQ userExamListREQ) {
		UserExamExample example = new UserExamExample();
		Criteria c = example.createCriteria();
		Page<UserExam> page = dao.listForPage(userExamListREQ.getPageCurrent(), userExamListREQ.getPageSize(), example);
		Page<UserExamListRESP> respPage = PageUtil.transform(page, UserExamListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户试卷添加
	 *
	 * @param userExamSaveREQ 用户试卷
	 * @return 添加结果
	 */
	public Result<String> save(UserExamSaveREQ userExamSaveREQ) {
		UserExam record = BeanUtil.copyProperties(userExamSaveREQ, UserExam.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户试卷查看
	 *
	 * @param id 主键ID
	 * @return 用户试卷
	 */
	public Result<UserExamViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamViewRESP.class));
	}

	/**
	 * 用户试卷修改
	 *
	 * @param userExamEditREQ 用户试卷修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamEditREQ userExamEditREQ) {
		UserExam record = BeanUtil.copyProperties(userExamEditREQ, UserExam.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户试卷删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
