package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.GradeStudentDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeStudentMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeStudentDaoImpl implements GradeStudentDao {

    @Autowired
    private GradeStudentMapper mapper;

    @Override
    public int save(GradeStudent record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeStudent record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeStudent getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeStudent> listForPage(int pageCurrent, int pageSize, GradeStudentExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }


    @Override
    public List<GradeStudent> listByGradeId(Long gradeId) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByGradeId(Long gradeId) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public GradeStudent getByGradeIdAndUserNo(Long gradeId, Long userNo) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        c.andUserNoEqualTo(userNo);
        List<GradeStudent> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<GradeStudent> listByUserNoAndUserRole(Long userNo, Integer userRole) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andUserRoleEqualTo(userRole);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeStudent> listByUserNoAndUserRoleAndStatusId(Long userNo, Integer userRole, Integer statusId) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        if (userRole != null) {
            c.andUserRoleEqualTo(userRole);
        }
        if (statusId != null) {
            c.andStatusIdEqualTo(statusId);
        }
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeStudent> listByUserNoAndStatusId(Long userNo, Integer statusId) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public int countByGradeId(Long gradeId) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.countByExample(example);
    }

    @Override
    public int countByGradeIdAndUserRole(Long gradeId, Integer userRole) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        c.andUserRoleEqualTo(userRole);
        return this.mapper.countByExample(example);
    }

    @Override
    public List<GradeStudent> listByGradeIdAndUserRole(Long gradeId, Integer userRole) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        c.andUserRoleEqualTo(userRole);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeStudent> listByIds(List<Long> ids) {
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andIdIn(ids);
        return this.mapper.selectByExample(example);
    }

}
