package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamDownloadEditREQ;
import com.roncoo.education.exam.common.req.UserExamDownloadListREQ;
import com.roncoo.education.exam.common.req.UserExamDownloadSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamDownloadListRESP;
import com.roncoo.education.exam.common.resp.UserExamDownloadViewRESP;
import com.roncoo.education.exam.service.dao.UserExamDownloadDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownload;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷下载
 *
 * @author wujing
 */
@Component
public class PcUserExamDownloadBiz extends BaseBiz {

	@Autowired
	private UserExamDownloadDao dao;

	/**
	 * 用户试卷下载列表
	 *
	 * @param userExamDownloadListREQ 用户试卷下载分页查询参数
	 * @return 用户试卷下载分页查询结果
	 */
	public Result<Page<UserExamDownloadListRESP>> list(UserExamDownloadListREQ userExamDownloadListREQ) {
		UserExamDownloadExample example = new UserExamDownloadExample();
		Criteria c = example.createCriteria();
		Page<UserExamDownload> page = dao.listForPage(userExamDownloadListREQ.getPageCurrent(), userExamDownloadListREQ.getPageSize(), example);
		Page<UserExamDownloadListRESP> respPage = PageUtil.transform(page, UserExamDownloadListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户试卷下载添加
	 *
	 * @param userExamDownloadSaveREQ 用户试卷下载
	 * @return 添加结果
	 */
	public Result<String> save(UserExamDownloadSaveREQ userExamDownloadSaveREQ) {
		UserExamDownload record = BeanUtil.copyProperties(userExamDownloadSaveREQ, UserExamDownload.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户试卷下载查看
	 *
	 * @param id 主键ID
	 * @return 用户试卷下载
	 */
	public Result<UserExamDownloadViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamDownloadViewRESP.class));
	}

	/**
	 * 用户试卷下载修改
	 *
	 * @param userExamDownloadEditREQ 用户试卷下载修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamDownloadEditREQ userExamDownloadEditREQ) {
		UserExamDownload record = BeanUtil.copyProperties(userExamDownloadEditREQ, UserExamDownload.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户试卷下载删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
