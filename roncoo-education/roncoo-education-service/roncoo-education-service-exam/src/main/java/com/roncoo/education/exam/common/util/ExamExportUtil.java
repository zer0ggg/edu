package com.roncoo.education.exam.common.util;

import cn.hutool.core.collection.CollectionUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 试卷导出工具
 *
 * @author LYQ
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExamExportUtil {

    /**
     * 不用改
     */
    private static final String SHAPEID_PREX = "_x56fe__x7247__x0020";
    /**
     * 不用改
     */
    private static final String SPID_PREX = "_x0000_i";
    /**
     * 不用改
     */
    private static final String TYPEID = "#_x0000_t75";

    private static final Configuration CONFIGURATION;

    static {
        CONFIGURATION = new Configuration(Configuration.VERSION_2_3_23);
        CONFIGURATION.setDefaultEncoding("utf-8");
        CONFIGURATION.setClassicCompatible(true);
        CONFIGURATION.setClassForTemplateLoading(ExamExportUtil.class, "/");
    }

    public static void export(HttpServletRequest request, HttpServletResponse response, String templateName, String exportFileName, String docSrcLocationPrex, String docSrcParent, String nextPartId, String html) {

        Document doc = Jsoup.parse(wrapHtmlHtml(html));

        List<String> docBase64BlockResults = new ArrayList<>();
        //List<String> xmlImgRefs = new ArrayList<>();
        List<String> imagePaths = new ArrayList<>();

        Elements imagElements = doc.getElementsByTag("img");
        if (CollectionUtil.isNotEmpty(imagElements)) {
            // 返回编码后字符串
            // 转换成word mht 能识别图片标签内容，去替换html中的图片标签
            for (Element item : imagElements) {
                // 把文件取出来
                String srcRealPath = item.attr("src");

                //如果图片来源于网络,将图片下载到本地
                if (srcRealPath.startsWith("http") || srcRealPath.startsWith("https")) {
                    try {
                        srcRealPath = readRemoteImageToLocal(request, srcRealPath);
                        imagePaths.add(srcRealPath);
                        log.info("图片来源于网络，地址:{},写入磁盘后的地址：{}", srcRealPath, srcRealPath);
                    } catch (Exception e) {
                        log.error("网络图片写入磁盘失败，", e);
                    }
                }


                File imageFile = new File(srcRealPath);
                String imageFileShortName = imageFile.getName();
                String fileTypeName = getFileSuffix(srcRealPath);

                String docFileName = "image" + UUID.randomUUID().toString() + "." + fileTypeName;
                String srcLocationShortName = docSrcParent + "/" + docFileName;

                // 样式
                String styleAttr = item.attr("style");

                //高度
                String imagHeightStr = item.attr("height");
                if (StringUtils.isEmpty(imagHeightStr)) {
                    imagHeightStr = getStyleAttrValue(styleAttr, "height");
                }
                //宽度
                String imagWidthStr = item.attr("width");
                if (StringUtils.isEmpty(imagHeightStr)) {
                    imagHeightStr = getStyleAttrValue(styleAttr, "width");
                }

                imagHeightStr = imagHeightStr.replace("px", "");
                imagWidthStr = imagWidthStr.replace("px", "");
                if (StringUtils.isEmpty(imagHeightStr)) {
                    //去得到默认的文件高度
                    imagHeightStr = "0";
                }
                if (StringUtils.isEmpty(imagWidthStr)) {
                    imagWidthStr = "0";
                }
                int imageHeight = Integer.parseInt(imagHeightStr);
                int imageWidth = Integer.parseInt(imagWidthStr);

                // 得到文件的word mht的body块
                String handledDocBodyBlock = toDocBodyBlock(srcRealPath,
                        imageFileShortName, imageHeight, imageWidth, styleAttr,
                        srcLocationShortName, SHAPEID_PREX, SPID_PREX, TYPEID);

                //这里的顺序有点问题：应该是替换item，而不是整个后面追加
                item.after(handledDocBodyBlock);
                item.remove();

                // 去替换原生的html中的imag
                String base64Content = imageToBase64(srcRealPath);
                String contextLocation = docSrcLocationPrex + "/" + docSrcParent + "/" + docFileName;

                String docBase64BlockResult = generateImageBase64Block(nextPartId, contextLocation, fileTypeName, base64Content);
                docBase64BlockResults.add(docBase64BlockResult);

            }
        }

        // 处理导出
        handleExport(response, doc, templateName, exportFileName, docBase64BlockResults, imagePaths);
    }

    /**
     * 读取远程图片
     *
     * @param imgUrl 图片链接
     * @return 下载到本地的图片地址
     */
    private static String readRemoteImageToLocal(HttpServletRequest request, String imgUrl) {
        // web项目的获取方式
        String contextPath = request.getSession().getServletContext().getRealPath("/");
        String localImagePath = null;

        FileOutputStream outStream = null;
        try {
            //new一个URL对象
            URL url = new URL(imgUrl);
            //打开链接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置请求方式为"GET"
            conn.setRequestMethod("GET");
            //超时响应时间为5秒
            conn.setConnectTimeout(5 * 1000);
            //通过输入流获取图片数据
            InputStream inStream = conn.getInputStream();
            //得到图片的二进制数据，以二进制封装得到数据，具有通用性
            byte[] data = readInputStream(inStream);
            //new一个文件对象用来保存图片，默认保存当前工程根目录
            localImagePath = contextPath + UUID.randomUUID().toString().replace("-", "") + ".jpg";
            File imageFile = new File(localImagePath);
            //创建输出流
            outStream = new FileOutputStream(imageFile);
            //写入数据
            outStream.write(data);
        } catch (Exception e) {
            log.error("获取网络图片异常！", e);
        } finally {
            if (outStream != null) {
                try {
                    //关闭输出流
                    outStream.close();
                } catch (IOException e) {
                    log.error("关闭输出流失败！", e);
                }
            }
        }

        return localImagePath;
    }

    private static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //创建一个Buffer字符串
        byte[] buffer = new byte[1024];
        //每次读取的字符串长度，如果为-1，代表全部读取完毕
        int len;
        //使用一个输入流从buffer里把数据读取出来
        while ((len = inStream.read(buffer)) != -1) {
            //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
            outStream.write(buffer, 0, len);
        }
        //关闭输入流
        inStream.close();
        //把outStream里的数据写入内存
        return outStream.toByteArray();
    }

    private static String getStyleAttrValue(String style, String attributeKey) {
        if (StringUtils.isEmpty(style)) {
            return "";
        }

        // 以";"分割
        String[] styleAttrValues = style.split(";");
        for (String item : styleAttrValues) {
            // 在以 ":"分割
            String[] keyValuePairs = item.split(":");
            if (attributeKey.equals(keyValuePairs[0])) {
                return keyValuePairs[1];
            }
        }

        return "";
    }

    /**
     * 包装HTML
     *
     * @param htmlContent HTML内容
     * @return 完整的HTML页面
     */
    private static String wrapHtmlHtml(String htmlContent) {
        // 因为传递过来都是不完整的doc
        return "<html>" +
                "<body>" +
                htmlContent +
                "</body>" +
                "</html>";
    }

    private static void handleExport(HttpServletResponse response, Document doc, String templateName, String exportFileName, List<String> docBase64BlockResults, List<String> tempImagePaths) {
        HashMap<String, Object> dataMap = new HashMap<>();
        StringBuilder handledBase64Block = new StringBuilder();
        for (String item : docBase64BlockResults) {
            handledBase64Block.append(item).append("\n");
        }
        dataMap.put("imagesBase64String", handledBase64Block.toString());

        String encodeExportFileName;
        try {
            encodeExportFileName = URLEncoder.encode(exportFileName, "utf-8");
        } catch (UnsupportedEncodingException e) {
            log.error("文件名称编码失败");
            encodeExportFileName = exportFileName;
        }

        // 设置请求头
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String(encodeExportFileName.getBytes(), StandardCharsets.ISO_8859_1) + ".doc");

        //ftl文件中${}的替换内容
        String raw = string2Ascii(doc.getElementsByTag("body").html());
        String bodyBlock = raw.replace("=3D", "=").replace("=", "=3D");
        dataMap.put("content", bodyBlock);

        // 导出
        Template t;
        try {
            t = CONFIGURATION.getTemplate(templateName, "utf-8");
        } catch (IOException e) {
            log.error("获取导出模板失败");
            return;
        }

        //t.setEncoding("utf-8");
        handleAllObject(dataMap);

        try {
            t.process(dataMap, response.getWriter());
        } catch (Exception e) {
            log.error("获取导出模板失败");
        }

        //如果图片来源于网络，由于需要先下载到本地，因此，处理完之后需要进行删除
        if (!tempImagePaths.isEmpty()) {
            for (String path : tempImagePaths) {
                File file = new File(path);
                if (file.exists()) {
                    if (!file.delete()) {
                        log.error("删除本地文件失败");
                    }
                }
            }
        }
    }

    private static void handleAllObject(Map<String, Object> dataMap) {
        //去处理数据
        for (Map.Entry<String, Object> entry : dataMap.entrySet()) {
            Object item = entry.getValue();

            //判断object是否是primitive type
            if (isPrimitiveType(item.getClass())) {
                if (item.getClass().equals(String.class)) {
                    item = string2Ascii((String) item);
                    entry.setValue(item);
                }
            } else if (isCollection(item.getClass())) {
                for (Object itemObject : (Collection) item) {
                    handleObject2Ascii(itemObject);
                }
            } else {
                handleObject2Ascii(item);
            }
        }
    }

    private static <T> void handleObject2Ascii(final T toHandleObject) {
        class MyFieldsCallBack implements ReflectionUtils.FieldCallback {
            @Override
            public void doWith(Field f) throws IllegalArgumentException, IllegalAccessException {
                if (f.getType().equals(String.class)) {
                    //如果是字符串类型
                    f.setAccessible(true);
                    String oldValue = (String) f.get(toHandleObject);
                    if (!StringUtils.isEmpty(oldValue)) {
                        f.set(toHandleObject, string2Ascii(oldValue));
                    }
                }
            }
        }
        ReflectionUtils.doWithFields(toHandleObject.getClass(), new MyFieldsCallBack());
    }

    private static String string2Ascii(String source) {
        if (source == null || "".equals(source)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();

        char[] c = source.toCharArray();
        for (char item : c) {
            String itemAscii;
            if (item >= 19968 && item < 40623) {
                itemAscii = "&#" + (item & 0xffff) + ";";
            } else {
                itemAscii = item + "";
            }
            sb.append(itemAscii);
        }

        return sb.toString();

    }

    private static boolean isPrimitiveType(Class<?> clazz) {
        return clazz.isEnum() || CharSequence.class.isAssignableFrom(clazz) || Number.class.isAssignableFrom(clazz) || Date.class.isAssignableFrom(clazz);
    }

    private static boolean isCollection(Class<?> clazz) {
        return Collection.class.isAssignableFrom(clazz);
    }

    public static String getFileSuffix(String srcRealPath) {
        return srcRealPath.substring(srcRealPath.indexOf(".") + 1);
    }

    public static String toDocBodyBlock(String imageFilePath, String imageFielShortName, int imageHeight, int imageWidth, String imageStyle, String srcLocationShortName, String shapeidPrex, String spidPrex, String typeid) {
        //shapeid
        String shapeid = shapeidPrex;
        shapeid += UUID.randomUUID().toString();

        //spid ,同shapeid处理
        String spid = spidPrex;
        spid += UUID.randomUUID().toString();


        return " <!--[if gte vml 1]>" +
                "<v:shape id=3D\"" + shapeid + "\"" +
                "\n" +
                " o:spid=3D\"" + spid + "\"" +
                " type=3D\"" + typeid + "\" alt=3D\"" + imageFielShortName + "\"" +
                "\n" +
                " style=3D' " + generateImageBodyBlockStyleAttr(imageFilePath, imageHeight, imageWidth) + imageStyle + "'" +
                ">" +
                "\n" +
                " <v:imagedata src=3D\"" + srcLocationShortName + "\"" +
                "\n" +
                " o:title=3D\"" + imageFielShortName.split("\\.")[0] + "\"" +
                "/>" +
                "</v:shape>" +
                "<![endif]-->";
    }

    public static String imageToBase64(String imageSrc) {
        //判断文件是否存在
        FileInputStream input = null;
        try {
            File file = new File(imageSrc);
            if (!file.exists()) {
                throw new FileNotFoundException("文件不存在！");
            }
            StringBuilder pictureBuffer = new StringBuilder();
            input = new FileInputStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            //读取文件

            //BufferedInputStream bi=new BufferedInputStream(in);
            byte[] temp = new byte[1024];
            for (int len = input.read(temp); len != -1; len = input.read(temp)) {
                out.write(temp, 0, len);
            }
            pictureBuffer.append(new String(Base64.encodeBase64Chunked(out.toByteArray())));


            return pictureBuffer.toString();
        } catch (IOException e) {
            log.error("图片转Base64失败", e);
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error("关闭输入流失败", e);
                }
            }
        }
    }

    public static String generateImageBase64Block(String nextPartId, String contextLoacation, String fileTypeName, String base64Content) {

        return "\n" +
                "\n" +
                "------=_NextPart_" + nextPartId +
                "\n" +
                "Content-Location: " + contextLoacation +
                "\n" +
                "Content-Transfer-Encoding: base64" +
                "\n" +
                "Content-Type: " + getImageContentType(fileTypeName) +
                "\n" +
                "\n" +
                base64Content;
    }

    private static String generateImageBodyBlockStyleAttr(String imageFilePath, int height, int width) {
        StringBuilder sb = new StringBuilder();

        BufferedImage sourceImg;
        try {
            sourceImg = ImageIO.read(new FileInputStream(imageFilePath));
            if (height == 0) {
                height = sourceImg.getHeight();
            }
            if (width == 0) {
                width = sourceImg.getWidth();
            }

        } catch (IOException e) {
            log.error("获取导出模板失败");
        }


        //将像素转化成pt
        BigDecimal heightValue = new BigDecimal(height * 12 / 16);
        heightValue = heightValue.setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal widthValue = new BigDecimal(width * 12 / 16);
        widthValue = widthValue.setScale(2, BigDecimal.ROUND_HALF_UP);

        sb.append("height:").append(heightValue).append("pt;");
        sb.append("width:").append(widthValue).append("pt;");
        sb.append("visibility:visible;");
        sb.append("mso-wrap-style:square; ");


        return sb.toString();
    }

    /**
     * 获取图片Content_Type
     *
     * @param fileTypeName 文件类型名称
     * @return 图片Content_Type
     */
    private static String getImageContentType(String fileTypeName) {
        String result = "image/jpeg";
        //http://tools.jb51.net/table/http_content_type
        if ("tif".equals(fileTypeName) || "tiff".equals(fileTypeName)) {
            result = "image/tiff";
        } else if ("fax".equals(fileTypeName)) {
            result = "image/fax";
        } else if ("gif".equals(fileTypeName)) {
            result = "image/gif";
        } else if ("ico".equals(fileTypeName)) {
            result = "image/x-icon";
        } else if ("jfif".equals(fileTypeName) || "jpe".equals(fileTypeName) || "jpeg".equals(fileTypeName) || "jpg".equals(fileTypeName)) {
            result = "image/jpeg";
        } else if ("net".equals(fileTypeName)) {
            result = "image/pnetvue";
        } else if ("png".equals(fileTypeName) || "bmp".equals(fileTypeName)) {
            result = "image/png";
        } else if ("rp".equals(fileTypeName)) {
            result = "image/vnd.rn-realpix";
        }

        return result;
    }

}
