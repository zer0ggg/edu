package com.roncoo.education.exam.common.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课程信息表
 *
 * @author wuyun
 */
@Document(indexName = EsExam.EXAM)
public class EsExam implements Serializable {

	public static final String EXAM = "edu_exam";

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;

	/**
	 * 修改时间
	 */
	private Date gmtModified;

	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 试卷名称
	 */
	private String examName;

	/**
	 * 是否免费：1免费，0收费
	 */
	private Integer isFree;

	/**
	 * 优惠价
	 */
	private BigDecimal fabPrice;

	/**
	 * 原价
	 */
	private BigDecimal orgPrice;

	/**
	 * 关键字
	 */
	private String keyword;

	/**
	 * 购买人数
	 */
	private Integer countBuy;

	/**
	 * 学习人数
	 */
	private Integer studyCount;


	/**
	 * 收藏人数
	 */
	private Integer collectionCount;

	/**
	 * 下载人数
	 */
	private Integer downloadCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public Integer getIsFree() {
		return isFree;
	}

	public void setIsFree(Integer isFree) {
		this.isFree = isFree;
	}

	public BigDecimal getFabPrice() {
		return fabPrice;
	}

	public void setFabPrice(BigDecimal fabPrice) {
		this.fabPrice = fabPrice;
	}

	public BigDecimal getOrgPrice() {
		return orgPrice;
	}

	public void setOrgPrice(BigDecimal orgPrice) {
		this.orgPrice = orgPrice;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getCountBuy() {
		return countBuy;
	}

	public void setCountBuy(Integer countBuy) {
		this.countBuy = countBuy;
	}

	public Integer getStudyCount() {
		return studyCount;
	}

	public void setStudyCount(Integer studyCount) {
		this.studyCount = studyCount;
	}

	public Integer getCollectionCount() {
		return collectionCount;
	}

	public void setCollectionCount(Integer collectionCount) {
		this.collectionCount = collectionCount;
	}

	public Integer getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(Integer downloadCount) {
		this.downloadCount = downloadCount;
	}


}
