package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PracticeMapper {
    int countByExample(PracticeExample example);

    int deleteByExample(PracticeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Practice record);

    int insertSelective(Practice record);

    List<Practice> selectByExample(PracticeExample example);

    Practice selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Practice record, @Param("example") PracticeExample example);

    int updateByExample(@Param("record") Practice record, @Param("example") PracticeExample example);

    int updateByPrimaryKeySelective(Practice record);

    int updateByPrimaryKey(Practice record);
}