package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScoreExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeStudentExamTitleScoreMapper {
    int countByExample(GradeStudentExamTitleScoreExample example);

    int deleteByExample(GradeStudentExamTitleScoreExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeStudentExamTitleScore record);

    int insertSelective(GradeStudentExamTitleScore record);

    List<GradeStudentExamTitleScore> selectByExample(GradeStudentExamTitleScoreExample example);

    GradeStudentExamTitleScore selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeStudentExamTitleScore record, @Param("example") GradeStudentExamTitleScoreExample example);

    int updateByExample(@Param("record") GradeStudentExamTitleScore record, @Param("example") GradeStudentExamTitleScoreExample example);

    int updateByPrimaryKeySelective(GradeStudentExamTitleScore record);

    int updateByPrimaryKey(GradeStudentExamTitleScore record);
}
