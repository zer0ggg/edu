package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamInfoListREQ;
import com.roncoo.education.exam.common.req.ExamInfoPutWayREQ;
import com.roncoo.education.exam.common.req.ExamInfoUpdateREQ;
import com.roncoo.education.exam.common.req.ExamInfoUpdateStatusREQ;
import com.roncoo.education.exam.common.resp.ExamInfoListRESP;
import com.roncoo.education.exam.common.resp.ExamInfoViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷信息管理")
@RestController
@RequestMapping("/exam/pc/exam/info")
public class PcExamInfoController {
    @Autowired
    private PcExamInfoBiz biz;

    @ApiOperation(value = "分页", notes = "分页")
    @PostMapping(value = "/list")
    public Result<Page<ExamInfoListRESP>> list(@RequestBody ExamInfoListREQ examInfoListREQ) {
        return biz.list(examInfoListREQ);
    }


    @ApiOperation(value = "上下架", notes = "上下架")
    @SysLog(value = "试卷信息上下架")
    @PutMapping(value = "/put/away")
    public Result<String> isPutaway(@RequestBody ExamInfoPutWayREQ req) {
        return biz.isPutaway(req);
    }


    @ApiOperation(value = "修改状态", notes = "修改状态")
    @SysLog(value = "试卷信息修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ExamInfoUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }

    @ApiOperation(value = "查询", notes = "查询")
    @GetMapping(value = "/view")
    public Result<ExamInfoViewRESP> view(@RequestParam(value = "id") Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "修改", notes = "修改")
    @SysLog(value = "试卷信息修改")
    @PutMapping(value = "/update")
    public Result<String> update(@RequestBody ExamInfoUpdateREQ req) {
        return biz.update(req);
    }


    @ApiOperation(value = "一键导入ES", notes = "一键导入ES")
    @SysLog(value = "试卷信息一键导入ES")
    @PostMapping(value = "/es/add")
    public Result<String> addEs() {
        return biz.addEs();
    }
}
