package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamInfoUpdateStatusREQ", description="试卷 修改状态操作")
public class ExamInfoUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id",required = true)
    private Long id;

    @NotNull(message = "状态不能为空")
    @ApiModelProperty(value = "状态(1:正常，0:禁用)",required = true)
    private Integer statusId;

}
