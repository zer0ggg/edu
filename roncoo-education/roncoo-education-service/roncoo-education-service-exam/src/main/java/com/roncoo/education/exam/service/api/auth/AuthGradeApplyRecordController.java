package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordSummaryDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordTipsListDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeApplyRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级申请记录管理")
@RestController
@RequestMapping("/exam/auth/grade/apply/record")
public class AuthGradeApplyRecordController {

    @Autowired
    private AuthGradeApplyRecordBiz biz;

    @ApiOperation(value = "分页列出", notes = "分页列出")
    @PostMapping(value = "/page")
    public Result<Page<AuthGradeApplyRecordPageDTO>> listForPage(@RequestBody @Valid AuthGradeApplyRecordPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "申请记录审核", notes = "申请记录审核")
    @PutMapping(value = "/audit")
    public Result<String> audit(@RequestBody @Valid AuthGradeApplyRecordAuditBO bo) {
        return biz.audit(bo);
    }

    @ApiOperation(value = "申请记录 批量审核", notes = "申请记录批量审核")
    @PutMapping(value = "/audit/batch")
    public Result<String> batchAudit(@RequestBody @Valid AuthGradeApplyRecordBatchAuditBO bo) {
        return biz.batchAudit(bo);
    }

    @ApiOperation(value = "申请记录数量统计", notes = "申请记录数量")
    @PostMapping(value = "/summary")
    public Result<AuthGradeApplyRecordSummaryDTO> summary(@RequestBody @Valid AuthGradeApplyRecordSummaryBO bo) {
        return biz.summary(bo);
    }

    @ApiOperation(value = "申请记录提示列表", notes = "申请记录提示列表")
    @PostMapping(value = "/tips")
    public Result<List<AuthGradeApplyRecordTipsListDTO>> tipsList(@RequestBody @Valid AuthGradeApplyRecordTipsListBO bo) {
        return biz.tipsList(bo);
    }

    @ApiOperation(value = "申请记录忽略查看", notes = "申请记录忽略查看")
    @PutMapping(value = "/ignore")
    public Result<String> recordViewIgnore(@RequestBody @Valid AuthGradeApplyRecordIgnoreBO bo) {
        return biz.recordViewIgnore(bo);
    }

}
