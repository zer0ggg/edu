package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 题目练习关联
 * </p>
 *
 * @author LHR
 */
@Data
@Accessors(chain = true)
@ApiModel(value="PracticeProblemRefListREQ", description="题目练习关联列表")
public class PracticeProblemRefListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "练习ID")
    private Long practiceId;

    @ApiModelProperty(value = "题目父类ID")
    private Long problemParentId;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
