package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 在线考试
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class AuthExamOnlineBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;
}
