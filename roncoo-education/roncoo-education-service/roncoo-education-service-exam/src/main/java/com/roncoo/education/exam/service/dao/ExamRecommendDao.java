package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;

/**
 * 试卷推荐 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamRecommendDao {

    /**
    * 保存试卷推荐
    *
    * @param record 试卷推荐
    * @return 影响记录数
    */
    int save(ExamRecommend record);

    /**
    * 根据ID删除试卷推荐
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷推荐
    * @return 影响记录数
    */
    int updateById(ExamRecommend record);

    /**
    * 根据ID获取试卷推荐
    *
    * @param id 主键ID
    * @return 试卷推荐
    */
    ExamRecommend getById(Long id);

    /**
    * 试卷推荐--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamRecommend> listForPage(int pageCurrent, int pageSize, ExamRecommendExample example);

    /**
     * 根据试卷id获取
     * @param examId
     * @return
     */
    ExamRecommend getByExamId(Long examId);
}
