package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 试卷题目
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ExamProblemListREQ", description = "试卷题目列表")
public class ExamProblemListREQ extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;
}
