package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷分析
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamAnalysisBO", description = "试卷分析")
public class AuthExamAnalysisBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;

    @NotNull(message = "科目ID")
    @ApiModelProperty(value = "科目ID", required = true)
    private Long subjectId;
}
