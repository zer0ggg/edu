package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthExamWebUploadVideoRefBO;
import com.roncoo.education.exam.service.api.auth.biz.AuthExamVideoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷视频 UserApi接口
 */
@Api(tags = "API-AUTH--试卷视频关联接口")
@RestController
@RequestMapping("/exam/auth/video")
public class AuthExamVideoController {

    @Autowired
    private AuthExamVideoBiz biz;

    /**
     * 前端上传视频关联章节接口
     */
    @ApiOperation(value = "前端上传视频关联章节接口", notes = "前端上传视频关联章节接口")
    @RequestMapping(value = "/ref", method = RequestMethod.POST)
    public Result<Long> ref(@RequestBody AuthExamWebUploadVideoRefBO bo) {
        return biz.ref(bo);
    }
}
