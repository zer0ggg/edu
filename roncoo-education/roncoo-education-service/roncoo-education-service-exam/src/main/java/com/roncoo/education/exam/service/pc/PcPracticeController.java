package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.PracticeEditREQ;
import com.roncoo.education.exam.common.req.PracticeListREQ;
import com.roncoo.education.exam.common.req.PracticeSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeListRESP;
import com.roncoo.education.exam.common.resp.PracticeViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcPracticeBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 练习信息 Pc接口
 *
 * @author LHR
 */
@Api(tags = "PC-练习信息")
@RestController
@RequestMapping("/exam/pc/practice")
public class PcPracticeController {

    @Autowired
    private PcPracticeBiz biz;

    @ApiOperation(value = "练习信息列表", notes = "练习信息列表")
    @PostMapping(value = "/list")
    public Result<Page<PracticeListRESP>> list(@RequestBody PracticeListREQ practiceListREQ) {
        return biz.list(practiceListREQ);
    }

    @ApiOperation(value = "练习信息添加", notes = "练习信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody PracticeSaveREQ practiceSaveREQ) {
        return biz.save(practiceSaveREQ);
    }

    @ApiOperation(value = "练习信息查看", notes = "练习信息查看")
    @GetMapping(value = "/view")
    public Result<PracticeViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "练习信息修改", notes = "练习信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody PracticeEditREQ practiceEditREQ) {
        return biz.edit(practiceEditREQ);
    }

    @ApiOperation(value = "练习信息删除", notes = "练习信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
