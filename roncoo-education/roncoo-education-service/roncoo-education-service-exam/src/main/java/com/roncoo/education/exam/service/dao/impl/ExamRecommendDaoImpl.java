package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamRecommendDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamRecommendMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷推荐 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamRecommendDaoImpl implements ExamRecommendDao {

    @Autowired
    private ExamRecommendMapper mapper;

    @Override
    public int save(ExamRecommend record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamRecommend record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamRecommend getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamRecommend> listForPage(int pageCurrent, int pageSize, ExamRecommendExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ExamRecommend getByExamId(Long examId) {
        ExamRecommendExample example = new ExamRecommendExample();
        ExamRecommendExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        List<ExamRecommend> list = this.mapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return  null;
    }


}
