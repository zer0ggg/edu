package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;

public class ExamProblemWithBLOBs extends ExamProblem implements Serializable {
    private String problemContent;

    private String problemAnswer;

    private String analysis;

    private static final long serialVersionUID = 1L;

    public String getProblemContent() {
        return problemContent;
    }

    public void setProblemContent(String problemContent) {
        this.problemContent = problemContent == null ? null : problemContent.trim();
    }

    public String getProblemAnswer() {
        return problemAnswer;
    }

    public void setProblemAnswer(String problemAnswer) {
        this.problemAnswer = problemAnswer == null ? null : problemAnswer.trim();
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis == null ? null : analysis.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", problemContent=").append(problemContent);
        sb.append(", problemAnswer=").append(problemAnswer);
        sb.append(", analysis=").append(analysis);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}