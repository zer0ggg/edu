package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditListREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefAuditViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamTitleProblemRefAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷标题题目关联审核 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷标题题目关联审核管理")
@RestController
@RequestMapping("/exam/pc/exam/title/problem/ref/audit")
public class PcExamTitleProblemRefAuditController {

    @Autowired
    private PcExamTitleProblemRefAuditBiz biz;

    @ApiOperation(value = "试卷标题题目关联审核列表", notes = "试卷标题题目关联审核列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamTitleProblemRefAuditListRESP>> list(@RequestBody ExamTitleProblemRefAuditListREQ examTitleProblemRefAuditListREQ) {
        return biz.list(examTitleProblemRefAuditListREQ);
    }

    @ApiOperation(value = "试卷标题题目关联审核添加", notes = "试卷标题题目关联审核添加")
    @SysLog(value = "试卷标题题目关联审核添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamTitleProblemRefAuditSaveREQ examTitleProblemRefAuditSaveREQ) {
        return biz.save(examTitleProblemRefAuditSaveREQ);
    }

    @ApiOperation(value = "试卷标题题目关联审核查看", notes = "试卷标题题目关联审核查看")
    @GetMapping(value = "/view")
    public Result<ExamTitleProblemRefAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷标题题目关联审核修改", notes = "试卷标题题目关联审核修改")
    @SysLog(value = "试卷标题题目关联审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamTitleProblemRefAuditEditREQ examTitleProblemRefAuditEditREQ) {
        return biz.edit(examTitleProblemRefAuditEditREQ);
    }

    @ApiOperation(value = "试卷标题题目关联审核删除", notes = "试卷标题题目关联审核删除")
    @SysLog(value = "试卷标题题目关联审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
