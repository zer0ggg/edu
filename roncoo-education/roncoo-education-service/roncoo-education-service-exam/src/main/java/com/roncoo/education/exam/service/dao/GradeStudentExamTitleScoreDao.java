package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScoreExample;

import java.util.List;

/**
 *  服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeStudentExamTitleScoreDao {

    /**
    * 保存
    *
    * @param record
    * @return 影响记录数
    */
    int save(GradeStudentExamTitleScore record);

    /**
    * 根据ID删除
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record
    * @return 影响记录数
    */
    int updateById(GradeStudentExamTitleScore record);

    /**
    * 根据ID获取
    *
    * @param id 主键ID
    * @return
    */
    GradeStudentExamTitleScore getById(Long id);

    /**
    * --分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<GradeStudentExamTitleScore> listForPage(int pageCurrent, int pageSize, GradeStudentExamTitleScoreExample example);

    /**
     * 根据班级ID删除用户考试标题得分
     *
     * @param gradeId 班级ID
     * @return 影响记录数
     */
    int deleteByGradeId(Long gradeId);

    /**
     * 根据班级ID和学生ID删除考试标题得分
     *
     * @param gradeId   班级ID
     * @param studentId 学生ID
     * @return 影响记录数
     */
    int deleteByGradeIdAndStudentId(Long gradeId, Long studentId);

    /**
     * 根据关联id列出
     * @param relationId
     * @return
     */
    List<GradeStudentExamTitleScore> listByRelationId(Long relationId);

    /**
     * 根据ID获取用户考试标题得分
     * @param relationId
     * @param titleId
     * @return
     */
    GradeStudentExamTitleScore getByRelationIdAndTitleId(Long relationId, Long titleId);

    /***
     * 根据班级试卷ID、班级学生ID获取标题得分信息
     * @param gradeExamId
     * @param studentId
     * @return
     */
    List<GradeStudentExamTitleScore> getByGradeExamIdAndStudentId(Long gradeExamId, Long studentId);
}
