package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.PracticeUserAnswerQO;
import com.roncoo.education.exam.feign.vo.PracticeUserAnswerVO;
import com.roncoo.education.exam.service.dao.PracticeUserAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试答案
 *
 * @author LHR
 */
@Component
public class FeignPracticeUserAnswerBiz extends BaseBiz {

    @Autowired
    private PracticeUserAnswerDao dao;

	public Page<PracticeUserAnswerVO> listForPage(PracticeUserAnswerQO qo) {
	    PracticeUserAnswerExample example = new PracticeUserAnswerExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<PracticeUserAnswer> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, PracticeUserAnswerVO.class);
	}

	public int save(PracticeUserAnswerQO qo) {
		PracticeUserAnswer record = BeanUtil.copyProperties(qo, PracticeUserAnswer.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public PracticeUserAnswerVO getById(Long id) {
		PracticeUserAnswer record = dao.getById(id);
		return BeanUtil.copyProperties(record, PracticeUserAnswerVO.class);
	}

	public int updateById(PracticeUserAnswerQO qo) {
		PracticeUserAnswer record = BeanUtil.copyProperties(qo, PracticeUserAnswer.class);
		return dao.updateById(record);
	}

}
