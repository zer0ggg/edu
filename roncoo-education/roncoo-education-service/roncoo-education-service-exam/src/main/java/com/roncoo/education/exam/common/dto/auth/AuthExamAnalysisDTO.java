package com.roncoo.education.exam.common.dto.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷分析
 */
@Data
@Accessors(chain = true)
public class AuthExamAnalysisDTO implements Serializable {

    private static final long serialVersionUID = 1L;


}
