package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExamInfoAudit implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long lecturerUserNo;

    private Integer examSort;

    private String examName;

    private String region;

    private Long graId;

    private Long subjectId;

    private Long yearId;

    private Long sourceId;

    private Integer isPutaway;

    private Integer isFree;

    private BigDecimal fabPrice;

    private BigDecimal orgPrice;

    private Integer answerTime;

    private Integer totalScore;

    private Integer problemQuantity;

    private String description;

    private String keyword;

    private Integer auditStatus;

    private String auditOpinion;

    private Long personalId;

    private Integer submitAudit;

    private Integer isPublic;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getLecturerUserNo() {
        return lecturerUserNo;
    }

    public void setLecturerUserNo(Long lecturerUserNo) {
        this.lecturerUserNo = lecturerUserNo;
    }

    public Integer getExamSort() {
        return examSort;
    }

    public void setExamSort(Integer examSort) {
        this.examSort = examSort;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName == null ? null : examName.trim();
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public Long getGraId() {
        return graId;
    }

    public void setGraId(Long graId) {
        this.graId = graId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getYearId() {
        return yearId;
    }

    public void setYearId(Long yearId) {
        this.yearId = yearId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getIsPutaway() {
        return isPutaway;
    }

    public void setIsPutaway(Integer isPutaway) {
        this.isPutaway = isPutaway;
    }

    public Integer getIsFree() {
        return isFree;
    }

    public void setIsFree(Integer isFree) {
        this.isFree = isFree;
    }

    public BigDecimal getFabPrice() {
        return fabPrice;
    }

    public void setFabPrice(BigDecimal fabPrice) {
        this.fabPrice = fabPrice;
    }

    public BigDecimal getOrgPrice() {
        return orgPrice;
    }

    public void setOrgPrice(BigDecimal orgPrice) {
        this.orgPrice = orgPrice;
    }

    public Integer getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Integer answerTime) {
        this.answerTime = answerTime;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getProblemQuantity() {
        return problemQuantity;
    }

    public void setProblemQuantity(Integer problemQuantity) {
        this.problemQuantity = problemQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword == null ? null : keyword.trim();
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion == null ? null : auditOpinion.trim();
    }

    public Long getPersonalId() {
        return personalId;
    }

    public void setPersonalId(Long personalId) {
        this.personalId = personalId;
    }

    public Integer getSubmitAudit() {
        return submitAudit;
    }

    public void setSubmitAudit(Integer submitAudit) {
        this.submitAudit = submitAudit;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", lecturerUserNo=").append(lecturerUserNo);
        sb.append(", examSort=").append(examSort);
        sb.append(", examName=").append(examName);
        sb.append(", region=").append(region);
        sb.append(", graId=").append(graId);
        sb.append(", subjectId=").append(subjectId);
        sb.append(", yearId=").append(yearId);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", isPutaway=").append(isPutaway);
        sb.append(", isFree=").append(isFree);
        sb.append(", fabPrice=").append(fabPrice);
        sb.append(", orgPrice=").append(orgPrice);
        sb.append(", answerTime=").append(answerTime);
        sb.append(", totalScore=").append(totalScore);
        sb.append(", problemQuantity=").append(problemQuantity);
        sb.append(", description=").append(description);
        sb.append(", keyword=").append(keyword);
        sb.append(", auditStatus=").append(auditStatus);
        sb.append(", auditOpinion=").append(auditOpinion);
        sb.append(", personalId=").append(personalId);
        sb.append(", submitAudit=").append(submitAudit);
        sb.append(", isPublic=").append(isPublic);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}