package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="GradeExamBO", description="")
public class GradeExamBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime beginTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    private LocalDateTime answerShowTime;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)")
    private Integer compensateStatus;
}
