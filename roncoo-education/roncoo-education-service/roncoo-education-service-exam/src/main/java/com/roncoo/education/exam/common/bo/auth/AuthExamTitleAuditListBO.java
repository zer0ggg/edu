package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷标题审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamTitleAuditListBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 试卷ID
     */
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
    /**
     * 标题名称
     */
    @ApiModelProperty(value = "标题名称")
    private String titleName;
    /**
     * 标题类型
     */
    @ApiModelProperty(value = "标题类型")
    private String titleType;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数", required = true)
    private Integer pageSize = 20;

}
