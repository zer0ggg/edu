package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷搜索
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class ExamInfoSearchBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "是否高亮(1高亮;0不高亮)")
    private Integer isHfield = 1;

    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize = 20;
}
