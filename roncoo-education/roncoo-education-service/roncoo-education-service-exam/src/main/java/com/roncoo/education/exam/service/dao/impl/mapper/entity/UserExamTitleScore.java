package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class UserExamTitleScore implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private String remark;

    private Long examId;

    private Long recordId;

    private Long titleId;

    private Integer score;

    private Integer sysAuditTotalScore;

    private Integer sysAuditScore;

    private Integer handworkAuditTotalScore;

    private Integer handworkAuditScore;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getSysAuditTotalScore() {
        return sysAuditTotalScore;
    }

    public void setSysAuditTotalScore(Integer sysAuditTotalScore) {
        this.sysAuditTotalScore = sysAuditTotalScore;
    }

    public Integer getSysAuditScore() {
        return sysAuditScore;
    }

    public void setSysAuditScore(Integer sysAuditScore) {
        this.sysAuditScore = sysAuditScore;
    }

    public Integer getHandworkAuditTotalScore() {
        return handworkAuditTotalScore;
    }

    public void setHandworkAuditTotalScore(Integer handworkAuditTotalScore) {
        this.handworkAuditTotalScore = handworkAuditTotalScore;
    }

    public Integer getHandworkAuditScore() {
        return handworkAuditScore;
    }

    public void setHandworkAuditScore(Integer handworkAuditScore) {
        this.handworkAuditScore = handworkAuditScore;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", remark=").append(remark);
        sb.append(", examId=").append(examId);
        sb.append(", recordId=").append(recordId);
        sb.append(", titleId=").append(titleId);
        sb.append(", score=").append(score);
        sb.append(", sysAuditTotalScore=").append(sysAuditTotalScore);
        sb.append(", sysAuditScore=").append(sysAuditScore);
        sb.append(", handworkAuditTotalScore=").append(handworkAuditTotalScore);
        sb.append(", handworkAuditScore=").append(handworkAuditScore);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}