package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 试卷信息审核表 -- 删除
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "id集合不能为空")
    @ApiModelProperty(value = "id集合", required = true)
    private List<String> idList;

}
