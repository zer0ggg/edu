package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamProblemOptionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemOptionVO;
import com.roncoo.education.exam.service.dao.ExamProblemOptionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目选项
 *
 * @author wujing
 */
@Component
public class FeignExamProblemOptionBiz extends BaseBiz {

    @Autowired
    private ExamProblemOptionDao dao;

	public Page<ExamProblemOptionVO> listForPage(ExamProblemOptionQO qo) {
	    ExamProblemOptionExample example = new ExamProblemOptionExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamProblemOption> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamProblemOptionVO.class);
	}

	public int save(ExamProblemOptionQO qo) {
		ExamProblemOption record = BeanUtil.copyProperties(qo, ExamProblemOption.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamProblemOptionVO getById(Long id) {
		ExamProblemOption record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamProblemOptionVO.class);
	}

	public int updateById(ExamProblemOptionQO qo) {
		ExamProblemOption record = BeanUtil.copyProperties(qo, ExamProblemOption.class);
		return dao.updateById(record);
	}

}
