package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 班级考试分页响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamPageDTO", description = "班级考试分页响应对象")
public class AuthGradeExamPageDTO implements Serializable {

    private static final long serialVersionUID = 4625477773752647898L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "原考试名称")
    private String examName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3:考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date answerShowTime;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)")
    private Integer compensateStatus;

    @ApiModelProperty(value = "试卷完成度：总数量")
    private Integer examCount = 0;

    @ApiModelProperty(value = "试卷完成度：未完成数量")
    private Integer examWaitCount = 0;

    @ApiModelProperty(value = "试卷完成度：考试中数量")
    private Integer examNotOverCount = 0;

    @ApiModelProperty(value = "试卷完成度：考试完成数量")
    private Integer examFinishCount = 0;

    @ApiModelProperty(value = "试卷完成度：考试评阅已完成数量")
    private Integer examFinishAuditCount = 0;

    @ApiModelProperty(value = "完成评阅状态(0:未完成，1:完成)")
    private Boolean examIsFinishAudit = false;

}
