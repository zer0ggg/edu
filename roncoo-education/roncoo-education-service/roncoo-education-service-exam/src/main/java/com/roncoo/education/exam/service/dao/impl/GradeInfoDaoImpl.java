package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.GradeInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeInfoMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeInfoDaoImpl implements GradeInfoDao {

    @Autowired
    private GradeInfoMapper mapper;

    @Override
    public int save(GradeInfo record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeInfo record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeInfo getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeInfo> listForPage(int pageCurrent, int pageSize, GradeInfoExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<GradeInfo> listByLecturerUserNo(Long lecturerUserNo) {
        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(lecturerUserNo);
        return this.mapper.selectByExample(example);
    }

    @Override
    public GradeInfo getByInvitationCode(String invitationCode) {
        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andInvitationCodeEqualTo(invitationCode);
        List<GradeInfo> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<GradeInfo> lsitByIds(List<Long> gradeIds) {
        GradeInfoExample example = new GradeInfoExample();
        GradeInfoExample.Criteria c = example.createCriteria();
        c.andIdIn(gradeIds);
        return this.mapper.selectByExample(example);
    }
}
