package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.FileInfoEditREQ;
import com.roncoo.education.exam.common.req.FileInfoListREQ;
import com.roncoo.education.exam.common.req.FileInfoSaveREQ;
import com.roncoo.education.exam.common.resp.FileInfoListRESP;
import com.roncoo.education.exam.common.resp.FileInfoViewRESP;
import com.roncoo.education.exam.service.dao.FileInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件信息
 *
 * @author wujing
 */
@Component
public class PcFileInfoBiz extends BaseBiz {

	@Autowired
	private FileInfoDao dao;

	/**
	 * 文件信息列表
	 *
	 * @param fileInfoListREQ 文件信息分页查询参数
	 * @return 文件信息分页查询结果
	 */
	public Result<Page<FileInfoListRESP>> list(FileInfoListREQ fileInfoListREQ) {
		FileInfoExample example = new FileInfoExample();
		Criteria c = example.createCriteria();
		Page<FileInfo> page = dao.listForPage(fileInfoListREQ.getPageCurrent(), fileInfoListREQ.getPageSize(), example);
		Page<FileInfoListRESP> respPage = PageUtil.transform(page, FileInfoListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 文件信息添加
	 *
	 * @param fileInfoSaveREQ 文件信息
	 * @return 添加结果
	 */
	public Result<String> save(FileInfoSaveREQ fileInfoSaveREQ) {
		FileInfo record = BeanUtil.copyProperties(fileInfoSaveREQ, FileInfo.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 文件信息查看
	 *
	 * @param id 主键ID
	 * @return 文件信息
	 */
	public Result<FileInfoViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), FileInfoViewRESP.class));
	}

	/**
	 * 文件信息修改
	 *
	 * @param fileInfoEditREQ 文件信息修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(FileInfoEditREQ fileInfoEditREQ) {
		FileInfo record = BeanUtil.copyProperties(fileInfoEditREQ, FileInfo.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 文件信息删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
