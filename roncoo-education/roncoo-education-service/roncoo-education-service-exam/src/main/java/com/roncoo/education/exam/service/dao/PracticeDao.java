package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample;

/**
 * 练习信息 服务类
 *
 * @author LHR
 * @date 2020-10-10
 */
public interface PracticeDao {

    /**
    * 保存练习信息
    *
    * @param record 练习信息
    * @return 影响记录数
    */
    int save(Practice record);

    /**
    * 根据ID删除练习信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 练习信息
    * @return 影响记录数
    */
    int updateById(Practice record);

    /**
    * 根据ID获取练习信息
    *
    * @param id 主键ID
    * @return 练习信息
    */
    Practice getById(Long id);

    /**
    * 练习信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<Practice> listForPage(int pageCurrent, int pageSize, PracticeExample example);

    /**
     * 获取该用户最新一条记录
     * @param userNo
     * @return
     */
    Practice getLastByUserNo(Long userNo);
}
