package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamTitleAuditEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleAuditListREQ;
import com.roncoo.education.exam.common.req.ExamTitleAuditSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleAuditViewRESP;
import com.roncoo.education.exam.service.dao.ExamTitleAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题审核
 *
 * @author wujing
 */
@Component
public class PcExamTitleAuditBiz extends BaseBiz {

	@Autowired
	private ExamTitleAuditDao dao;

	/**
	 * 试卷标题审核列表
	 *
	 * @param examTitleAuditListREQ 试卷标题审核分页查询参数
	 * @return 试卷标题审核分页查询结果
	 */
	public Result<Page<ExamTitleAuditListRESP>> list(ExamTitleAuditListREQ examTitleAuditListREQ) {
		ExamTitleAuditExample example = new ExamTitleAuditExample();
		Criteria c = example.createCriteria();
		Page<ExamTitleAudit> page = dao.listForPage(examTitleAuditListREQ.getPageCurrent(), examTitleAuditListREQ.getPageSize(), example);
		Page<ExamTitleAuditListRESP> respPage = PageUtil.transform(page, ExamTitleAuditListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷标题审核添加
	 *
	 * @param examTitleAuditSaveREQ 试卷标题审核
	 * @return 添加结果
	 */
	public Result<String> save(ExamTitleAuditSaveREQ examTitleAuditSaveREQ) {
		ExamTitleAudit record = BeanUtil.copyProperties(examTitleAuditSaveREQ, ExamTitleAudit.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷标题审核查看
	 *
	 * @param id 主键ID
	 * @return 试卷标题审核
	 */
	public Result<ExamTitleAuditViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamTitleAuditViewRESP.class));
	}

	/**
	 * 试卷标题审核修改
	 *
	 * @param examTitleAuditEditREQ 试卷标题审核修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamTitleAuditEditREQ examTitleAuditEditREQ) {
		ExamTitleAudit record = BeanUtil.copyProperties(examTitleAuditEditREQ, ExamTitleAudit.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷标题审核删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
