package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamAnswerViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnswerViewDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthUserExamAnswerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户考试答案 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-用户试卷答案管理")
@RestController
@RequestMapping("/exam/auth/user/exam/answer")
public class AuthUserExamAnswerController {

    @Autowired
    private AuthUserExamAnswerBiz biz;

    @ApiOperation(value = "获取用户试卷答案和参考答案", notes = "获取用户试卷答案和参考答案")
    @PostMapping(value = "/view")
    public Result<AuthUserExamAnswerViewDTO> view(@RequestBody @Valid AuthUserExamAnswerViewBO viewBO) {
        return biz.view(viewBO);
    }



}
