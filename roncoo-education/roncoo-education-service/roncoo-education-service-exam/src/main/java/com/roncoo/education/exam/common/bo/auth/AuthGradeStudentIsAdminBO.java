package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 退出班级对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeStudentIsAdminBO", description = "班级对象 判断管理员")
public class AuthGradeStudentIsAdminBO implements Serializable {

    private static final long serialVersionUID = 5530413255472902580L;
}
