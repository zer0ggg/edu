package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试题选项审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemOptionViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
     * 题目ID
     */
    @ApiModelProperty(value = "题目ID")
    private Long problemId;
    /**
     * 选项内容
     */
    @ApiModelProperty(value = "选项内容")
    private String optionContent;

}
