package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷标题表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ExamTitleViewBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	private Long id;

}
