package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeExamStudentRelation;
import com.roncoo.education.exam.feign.qo.GradeExamStudentRelationQO;
import com.roncoo.education.exam.feign.vo.GradeExamStudentRelationVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeExamStudentRelationBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeExamStudentRelationController extends BaseController implements IFeignGradeExamStudentRelation{

    @Autowired
    private FeignGradeExamStudentRelationBiz biz;

	@Override
	public Page<GradeExamStudentRelationVO> listForPage(@RequestBody GradeExamStudentRelationQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeExamStudentRelationQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeExamStudentRelationQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeExamStudentRelationVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
