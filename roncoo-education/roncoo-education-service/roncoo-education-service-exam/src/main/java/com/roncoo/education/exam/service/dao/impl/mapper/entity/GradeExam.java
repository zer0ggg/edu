package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class GradeExam implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer sort;

    private Integer statusId;

    private String remark;

    private Long lecturerUserNo;

    private Long gradeId;

    private String gradeExamName;

    private Long subjectId;

    private Long examId;

    private Date beginTime;

    private Date endTime;

    private Integer answerShow;

    private Date answerShowTime;

    private Integer auditType;

    private Integer compensateStatus;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getLecturerUserNo() {
        return lecturerUserNo;
    }

    public void setLecturerUserNo(Long lecturerUserNo) {
        this.lecturerUserNo = lecturerUserNo;
    }

    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeExamName() {
        return gradeExamName;
    }

    public void setGradeExamName(String gradeExamName) {
        this.gradeExamName = gradeExamName == null ? null : gradeExamName.trim();
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getAnswerShow() {
        return answerShow;
    }

    public void setAnswerShow(Integer answerShow) {
        this.answerShow = answerShow;
    }

    public Date getAnswerShowTime() {
        return answerShowTime;
    }

    public void setAnswerShowTime(Date answerShowTime) {
        this.answerShowTime = answerShowTime;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public Integer getCompensateStatus() {
        return compensateStatus;
    }

    public void setCompensateStatus(Integer compensateStatus) {
        this.compensateStatus = compensateStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", sort=").append(sort);
        sb.append(", statusId=").append(statusId);
        sb.append(", remark=").append(remark);
        sb.append(", lecturerUserNo=").append(lecturerUserNo);
        sb.append(", gradeId=").append(gradeId);
        sb.append(", gradeExamName=").append(gradeExamName);
        sb.append(", subjectId=").append(subjectId);
        sb.append(", examId=").append(examId);
        sb.append(", beginTime=").append(beginTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", answerShow=").append(answerShow);
        sb.append(", answerShowTime=").append(answerShowTime);
        sb.append(", auditType=").append(auditType);
        sb.append(", compensateStatus=").append(compensateStatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
