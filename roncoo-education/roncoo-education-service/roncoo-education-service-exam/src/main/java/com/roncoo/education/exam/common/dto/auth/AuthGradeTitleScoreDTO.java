package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 班级考试学生关联分页响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeTitleScoreDTO", description = "班级考试学生 分数返回")
public class AuthGradeTitleScoreDTO implements Serializable {

    private static final long serialVersionUID = 6970667085344566196L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "标题类型")
    private Integer titleType;

    @ApiModelProperty(value = "班级id")
    private Long gradeId;

    @ApiModelProperty(value = "班级试卷id")
    private Long gradeExamId;

    @ApiModelProperty(value = "试卷id")
    private Long examId;

    @ApiModelProperty(value = "标题id")
    private Long titleId;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;

    //补充标题名称
    @ApiModelProperty(value = "标题名称")
    private String titleName;


}
