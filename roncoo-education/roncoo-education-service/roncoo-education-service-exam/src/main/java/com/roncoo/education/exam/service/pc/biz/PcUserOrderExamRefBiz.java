package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserOrderExamRefEditREQ;
import com.roncoo.education.exam.common.req.UserOrderExamRefListREQ;
import com.roncoo.education.exam.common.req.UserOrderExamRefSaveREQ;
import com.roncoo.education.exam.common.resp.UserOrderExamRefListRESP;
import com.roncoo.education.exam.common.resp.UserOrderExamRefViewRESP;
import com.roncoo.education.exam.service.dao.UserOrderExamRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户订单试卷关联
 *
 * @author wujing
 */
@Component
public class PcUserOrderExamRefBiz extends BaseBiz {

	@Autowired
	private UserOrderExamRefDao dao;

	/**
	 * 用户订单试卷关联列表
	 *
	 * @param userOrderExamRefListREQ 用户订单试卷关联分页查询参数
	 * @return 用户订单试卷关联分页查询结果
	 */
	public Result<Page<UserOrderExamRefListRESP>> list(UserOrderExamRefListREQ userOrderExamRefListREQ) {
		UserOrderExamRefExample example = new UserOrderExamRefExample();
		Criteria c = example.createCriteria();
		Page<UserOrderExamRef> page = dao.listForPage(userOrderExamRefListREQ.getPageCurrent(), userOrderExamRefListREQ.getPageSize(), example);
		Page<UserOrderExamRefListRESP> respPage = PageUtil.transform(page, UserOrderExamRefListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户订单试卷关联添加
	 *
	 * @param userOrderExamRefSaveREQ 用户订单试卷关联
	 * @return 添加结果
	 */
	public Result<String> save(UserOrderExamRefSaveREQ userOrderExamRefSaveREQ) {
		UserOrderExamRef record = BeanUtil.copyProperties(userOrderExamRefSaveREQ, UserOrderExamRef.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户订单试卷关联查看
	 *
	 * @param id 主键ID
	 * @return 用户订单试卷关联
	 */
	public Result<UserOrderExamRefViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserOrderExamRefViewRESP.class));
	}

	/**
	 * 用户订单试卷关联修改
	 *
	 * @param userOrderExamRefEditREQ 用户订单试卷关联修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserOrderExamRefEditREQ userOrderExamRefEditREQ) {
		UserOrderExamRef record = BeanUtil.copyProperties(userOrderExamRefEditREQ, UserOrderExamRef.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户订单试卷关联删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
