package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷标题表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ExamTitleViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	private Long id;

	/**
	 * 试卷ID
	 */
	@ApiModelProperty(value = "试卷ID")
	private Long examId;

	/**
	 * 标题名称
	 */
	@ApiModelProperty(value = "标题名称")
	private String titleName;
	/**
	 * 标题类型
	 */
	@ApiModelProperty(value = "标题类型")
	private String titleType;

}
