package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 试卷题目
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamProblemDTO", description = "试卷题目")
public class AuthExamProblemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题试题关联主键refId")
    private Long refId;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionCount;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "分值")
    private Integer score;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @ApiModelProperty(value = "考点Id")
    private Long emphasisId;

    @ApiModelProperty(value = "年级id")
    private Long gradeId;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "难度id")
    private Long difficultyId;

    @ApiModelProperty(value = "题类id")
    private Long topicId;

    @ApiModelProperty(value = "个人分类id")
    private Long personalId;

    @ApiModelProperty(value = "小题")
    private List<AuthExamProblemDTO> childrenList;

    @ApiModelProperty(value = "填空数")
    private Integer optionCount;

    @ApiModelProperty(value = "选项")
    private List<AuthExamProblemOptionDTO> optionList;
}
