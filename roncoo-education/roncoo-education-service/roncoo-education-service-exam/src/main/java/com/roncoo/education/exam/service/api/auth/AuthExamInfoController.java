package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthExamInfoBaseViewBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamInfoViewBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamVideoSignBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoBaseViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamVideoSignDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthExamInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 试卷信息 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-试卷信息管理")
@RestController
@RequestMapping("/exam/auth/exam")
public class AuthExamInfoController {

    @Autowired
    private AuthExamInfoBiz biz;

    @ApiOperation(value = "试卷详情--基础信息", notes = "试卷详情--基础信息")
    @PostMapping(value = "/info")
    public Result<AuthExamInfoBaseViewDTO> info(@RequestBody @Valid AuthExamInfoBaseViewBO bo) {
        return biz.info(bo);
    }

    @ApiOperation(value = "试卷详情", notes = "试卷详情（通过标题ID查 获取大题 接口）")
    @PostMapping(value = "/view")
    public Result<AuthExamInfoViewDTO> view(@RequestBody @Valid AuthExamInfoViewBO authExamInfoViewBO) {
        return biz.view(authExamInfoViewBO);
    }

    @ApiOperation(value = "播放获取sign值接口", notes = "播放获取sign值接口")
    @PostMapping(value = "/sign")
    public Result<AuthExamVideoSignDTO> sign(@RequestBody @Valid AuthExamVideoSignBO bo) {
        return biz.sign(bo);
    }

}
