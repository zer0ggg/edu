package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.exam.service.api.auth.biz.AuthExamTitleProblemRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题题目关联 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/exam/app/examTitleProblemRef")
public class AuthExamTitleProblemRefController {

    @Autowired
    private AuthExamTitleProblemRefBiz biz;

}
