package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加班级学生请求对象
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeStudentSaveREQ", description = "添加班级学生请求对象")
public class GradeStudentSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @NotNull(message = "用户编号不能为空")
    @ApiModelProperty(value = "用户编号", required = true)
    private Long userNo;

    @NotNull(message = "用户角色不能为空")
    @ApiModelProperty(value = "用户角色(1:普通成员，2:管理员)", required = true)
    private Integer userRole;

    @NotBlank(message = "昵称不能为空")
    @ApiModelProperty(value = "昵称", required = true)
    private String nickname;
}
