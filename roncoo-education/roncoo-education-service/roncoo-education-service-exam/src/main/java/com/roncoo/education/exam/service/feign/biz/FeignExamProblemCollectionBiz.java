package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamProblemCollectionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemCollectionVO;
import com.roncoo.education.exam.service.dao.ExamProblemCollectionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollection;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目收藏
 *
 * @author wujing
 */
@Component
public class FeignExamProblemCollectionBiz extends BaseBiz {

    @Autowired
    private ExamProblemCollectionDao dao;

	public Page<ExamProblemCollectionVO> listForPage(ExamProblemCollectionQO qo) {
	    ExamProblemCollectionExample example = new ExamProblemCollectionExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamProblemCollection> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamProblemCollectionVO.class);
	}

	public int save(ExamProblemCollectionQO qo) {
		ExamProblemCollection record = BeanUtil.copyProperties(qo, ExamProblemCollection.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamProblemCollectionVO getById(Long id) {
		ExamProblemCollection record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamProblemCollectionVO.class);
	}

	public int updateById(ExamProblemCollectionQO qo) {
		ExamProblemCollection record = BeanUtil.copyProperties(qo, ExamProblemCollection.class);
		return dao.updateById(record);
	}

}
