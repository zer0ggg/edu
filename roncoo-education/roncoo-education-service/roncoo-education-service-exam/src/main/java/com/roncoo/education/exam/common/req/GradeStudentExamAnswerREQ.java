package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="GradeStudentExamAnswerREQ", description="")
public class GradeStudentExamAnswerREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "关联ID")
    private Long relationId;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试ID")
    private Long gradeExamId;

    @ApiModelProperty(value = "学生ID")
    private Long studentId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "标题ID")
    private Long titleId;

    @ApiModelProperty(value = "题目父类ID")
    private Long problemParentId;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;

    @ApiModelProperty(value = "用户答案")
    private String userAnswer;

    @ApiModelProperty(value = "题目分值")
    private Integer problemScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "答案状态(1:待评阅，2:已评阅)")
    private Integer answerStatus;

    @ApiModelProperty(value = "系统审核(1:未确定，2:系统审核，3:人工审核)")
    private Integer sysAudit;

    @ApiModelProperty(value = "评阅用户编号")
    private Long auditUserNo;
}
