package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamInfoAuditEditREQ;
import com.roncoo.education.exam.common.req.ExamInfoAuditListREQ;
import com.roncoo.education.exam.common.req.ExamInfoAuditToAuditREQ;
import com.roncoo.education.exam.common.req.ExamInfoAuditUpdateStatusREQ;
import com.roncoo.education.exam.common.resp.ExamInfoAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamInfoAuditViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamInfoAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷信息审核 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷信息审核管理")
@RestController
@RequestMapping("/exam/pc/exam/info/audit")
public class PcExamInfoAuditController {

    @Autowired
    private PcExamInfoAuditBiz biz;

    @ApiOperation(value = "分页", notes = "分页")
    @PostMapping(value = "/list")
    public Result<Page<ExamInfoAuditListRESP>> list(@RequestBody ExamInfoAuditListREQ examInfoAuditListREQ) {
        return biz.list(examInfoAuditListREQ);
    }

    @ApiOperation(value = "查看", notes = "查看")
    @GetMapping(value = "/view")
    public Result<ExamInfoAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "修改", notes = "修改")
    @SysLog(value = "试卷信息审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamInfoAuditEditREQ req) {
        return biz.update(req);
    }


    @ApiOperation(value = "修改状态", notes = "修改状态")
    @SysLog(value = "试卷信息审核修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ExamInfoAuditUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }


    @ApiOperation(value = "审核", notes = "审核")
    @SysLog(value = "试卷信息审核")
    @PutMapping(value = "/audit")
    public Result<String> audit(@RequestBody ExamInfoAuditToAuditREQ req) {
        return biz.audit(req);
    }
}
