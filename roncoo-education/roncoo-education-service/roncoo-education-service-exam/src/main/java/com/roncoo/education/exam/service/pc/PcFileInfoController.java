package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.FileInfoEditREQ;
import com.roncoo.education.exam.common.req.FileInfoListREQ;
import com.roncoo.education.exam.common.req.FileInfoSaveREQ;
import com.roncoo.education.exam.common.resp.FileInfoListRESP;
import com.roncoo.education.exam.common.resp.FileInfoViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcFileInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文件信息 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-文件信息管理")
@RestController
@RequestMapping("/exam/pc/file/info")
public class PcFileInfoController {

    @Autowired
    private PcFileInfoBiz biz;

    @ApiOperation(value = "文件信息列表", notes = "文件信息列表")
    @PostMapping(value = "/list")
    public Result<Page<FileInfoListRESP>> list(@RequestBody FileInfoListREQ fileInfoListREQ) {
        return biz.list(fileInfoListREQ);
    }

    @ApiOperation(value = "文件信息添加", notes = "文件信息添加")
    @SysLog(value = "文件信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody FileInfoSaveREQ fileInfoSaveREQ) {
        return biz.save(fileInfoSaveREQ);
    }

    @ApiOperation(value = "文件信息查看", notes = "文件信息查看")
    @GetMapping(value = "/view")
    public Result<FileInfoViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "文件信息修改", notes = "文件信息修改")
    @SysLog(value = "文件信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody FileInfoEditREQ fileInfoEditREQ) {
        return biz.edit(fileInfoEditREQ);
    }

    @ApiOperation(value = "文件信息删除", notes = "文件信息删除")
    @SysLog(value = "文件信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
