package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamTitleAuditMapper {
    int countByExample(ExamTitleAuditExample example);

    int deleteByExample(ExamTitleAuditExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamTitleAudit record);

    int insertSelective(ExamTitleAudit record);

    List<ExamTitleAudit> selectByExample(ExamTitleAuditExample example);

    ExamTitleAudit selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamTitleAudit record, @Param("example") ExamTitleAuditExample example);

    int updateByExample(@Param("record") ExamTitleAudit record, @Param("example") ExamTitleAuditExample example);

    int updateByPrimaryKeySelective(ExamTitleAudit record);

    int updateByPrimaryKey(ExamTitleAudit record);
}