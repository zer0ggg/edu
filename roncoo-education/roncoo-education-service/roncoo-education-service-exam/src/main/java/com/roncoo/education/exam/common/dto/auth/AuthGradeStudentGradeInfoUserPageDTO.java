package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 班级信息--查询班级成员管理的班级
 * </p>
 *
 * @author wujing
 * @date 2020-06-01
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthGradeStudentGradeInfoUserPageDTO", description="班级信息--查询班级成员管理的班级")
public class AuthGradeStudentGradeInfoUserPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级名称")
    private String gradeName;

    @ApiModelProperty(value = "班级简介")
    private String gradeIntro;
}
