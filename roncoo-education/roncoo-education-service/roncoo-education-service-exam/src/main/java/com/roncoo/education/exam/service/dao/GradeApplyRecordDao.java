package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;

import java.util.List;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeApplyRecordDao {

    /**
     * 保存
     *
     * @param record
     * @return 影响记录数
     */
    int save(GradeApplyRecord record);

    /**
     * 根据ID删除
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record
     * @return 影响记录数
     */
    int updateById(GradeApplyRecord record);

    /**
     * 根据ID获取
     *
     * @param id 主键ID
     * @return
     */
    GradeApplyRecord getById(Long id);

    /**
     * --分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeApplyRecord> listForPage(int pageCurrent, int pageSize, GradeApplyRecordExample example);

    /**
     * 根据ID获取班级申请记录--加行锁
     *
     * @param id 主键ID
     * @return 班级申请记录
     */
    GradeApplyRecord getByIdForUpdate(Long id);

    /**
     * 根据班级id和审核状态计算数量
     *
     * @param gradeId
     * @param auditStatus
     * @return
     */
    int countByGradeIdAndAudit(Long gradeId, Integer auditStatus);

    /**
     * 根据讲师用户编号列出指定条数的提示申请记录
     *
     * @param lecturerUserNoList 讲师用户编号集合
     * @param num                记录数
     * @return 班级申请记录
     */
    List<GradeApplyRecord> limitTipsListInLecturerUserNo(List<Long> lecturerUserNoList, Integer num);
}
