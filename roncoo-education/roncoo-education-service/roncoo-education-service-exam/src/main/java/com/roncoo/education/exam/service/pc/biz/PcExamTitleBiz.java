package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamTitleEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleListREQ;
import com.roncoo.education.exam.common.req.ExamTitleSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleViewRESP;
import com.roncoo.education.exam.service.dao.ExamTitleDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitle;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题
 *
 * @author wujing
 */
@Component
public class PcExamTitleBiz extends BaseBiz {

	@Autowired
	private ExamTitleDao dao;

	/**
	 * 试卷标题列表
	 *
	 * @param examTitleListREQ 试卷标题分页查询参数
	 * @return 试卷标题分页查询结果
	 */
	public Result<Page<ExamTitleListRESP>> list(ExamTitleListREQ examTitleListREQ) {
		ExamTitleExample example = new ExamTitleExample();
		Criteria c = example.createCriteria();
		Page<ExamTitle> page = dao.listForPage(examTitleListREQ.getPageCurrent(), examTitleListREQ.getPageSize(), example);
		Page<ExamTitleListRESP> respPage = PageUtil.transform(page, ExamTitleListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷标题添加
	 *
	 * @param examTitleSaveREQ 试卷标题
	 * @return 添加结果
	 */
	public Result<String> save(ExamTitleSaveREQ examTitleSaveREQ) {
		ExamTitle record = BeanUtil.copyProperties(examTitleSaveREQ, ExamTitle.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷标题查看
	 *
	 * @param id 主键ID
	 * @return 试卷标题
	 */
	public Result<ExamTitleViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamTitleViewRESP.class));
	}

	/**
	 * 试卷标题修改
	 *
	 * @param examTitleEditREQ 试卷标题修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamTitleEditREQ examTitleEditREQ) {
		ExamTitle record = BeanUtil.copyProperties(examTitleEditREQ, ExamTitle.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷标题删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
