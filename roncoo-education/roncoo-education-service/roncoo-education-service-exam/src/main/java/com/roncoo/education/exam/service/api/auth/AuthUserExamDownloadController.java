package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamDownloadExportBO;
import com.roncoo.education.exam.service.api.auth.biz.AuthUserExamDownloadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * 用户试卷下载 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-用户试卷下载管理")
@RestController
@RequestMapping("/exam/auth/user/exam/download")
public class AuthUserExamDownloadController {

    @Autowired
    private AuthUserExamDownloadBiz biz;

    @ApiOperation(value = "试卷导出", notes = "试卷导出")
    @PostMapping(value = "/export")
    public void export(AuthUserExamDownloadExportBO bo, HttpServletRequest request, HttpServletResponse response) {
        biz.export(bo, request, response);
    }

}
