package com.roncoo.education.exam.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.enums.GradeExamStatusEnum;
import com.roncoo.education.exam.common.bo.auth.AuthGradeExamStudentRelationSysAuditBO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeExamStudentRelationBiz;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.GradeExamStudentRelationDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelation;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 班级考试到期：考试状态置为已完成，提交
 *
 * @author zkpc
 */
@Slf4j
@Component
public class GradeExamStudentRelationJob extends IJobHandler {

    @Autowired
    private AuthGradeExamStudentRelationBiz biz;

    @Autowired
    private GradeExamStudentRelationDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;

    @Override
    @XxlJob(value = "gradeExamStudentRelationJob")
    public ReturnT<String> execute(String s) {
        XxlJobLogger.log("开始执行班级考试关闭任务");
        log.debug("开始执行班级考试关闭任务");
        Date cutoffEndTime = DateUtil.offsetSecond(new Date(), -30);//截止时间已经过了30秒，系统自动提交 考试中的试卷
        int pageCurrent = 1;
        int pageSize = 1000;
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andExamStatusEqualTo(GradeExamStatusEnum.NOT_OVER.getCode());
        c.andCutoffEndTimeLessThan(cutoffEndTime);
        example.setOrderByClause("id asc");

        boolean go;
        do {
            Page<GradeExamStudentRelation> resultPage = dao.listForPage(pageCurrent, pageSize, example);
            if (ObjectUtil.isNull(resultPage) || CollectionUtil.isEmpty(resultPage.getList())) {
                return SUCCESS;
            }

            for (GradeExamStudentRelation record : resultPage.getList()) {
                ExamInfo examInfo = examInfoDao.getById(record.getExamId());
                if (ObjectUtil.isNull(examInfo)) {
                    continue;
                }

                // 执行考试结束
                log.debug("考试记录：[{}]达到考试关闭条件，进行关闭处理", record.getId());
                XxlJobLogger.log("考试记录：[{}]达到考试关闭条件，进行关闭处理", record.getId());
                try {
                    AuthGradeExamStudentRelationSysAuditBO bo = new AuthGradeExamStudentRelationSysAuditBO();
                    bo.setId(record.getId());
                    //bo.setUserNo(record.getUserNo());
                    biz.submitAndAudit(bo);
                } catch (Exception e) {
                    log.error("考试记录：[{}]班级考试关闭异常！", record.getId(), e);
                    XxlJobLogger.log("班级考试记录：[{}]考试关闭异常！", record.getId(), e);
                }
            }

            // 判断是否继续
            go = resultPage.getPageCurrent() < resultPage.getTotalPage();
        } while (go);
        log.debug("结束执行用户考试关闭任务");
        XxlJobLogger.log("结束执行用户考试关闭任务");
        return SUCCESS;
    }
}
