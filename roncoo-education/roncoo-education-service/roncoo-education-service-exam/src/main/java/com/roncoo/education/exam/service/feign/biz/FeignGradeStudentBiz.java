package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeStudentQO;
import com.roncoo.education.exam.feign.vo.GradeStudentVO;
import com.roncoo.education.exam.service.dao.GradeStudentDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeStudentBiz extends BaseBiz {

    @Autowired
    private GradeStudentDao dao;

	public Page<GradeStudentVO> listForPage(GradeStudentQO qo) {
	    GradeStudentExample example = new GradeStudentExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeStudent> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeStudentVO.class);
	}

	public int save(GradeStudentQO qo) {
		GradeStudent record = BeanUtil.copyProperties(qo, GradeStudent.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeStudentVO getById(Long id) {
		GradeStudent record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeStudentVO.class);
	}

	public int updateById(GradeStudentQO qo) {
		GradeStudent record = BeanUtil.copyProperties(qo, GradeStudent.class);
		return dao.updateById(record);
	}

}
