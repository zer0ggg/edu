package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblemCollection;
import com.roncoo.education.exam.feign.qo.ExamProblemCollectionQO;
import com.roncoo.education.exam.feign.vo.ExamProblemCollectionVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamProblemCollectionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷题目收藏
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamProblemCollectionController extends BaseController implements IFeignExamProblemCollection{

    @Autowired
    private FeignExamProblemCollectionBiz biz;

	@Override
	public Page<ExamProblemCollectionVO> listForPage(@RequestBody ExamProblemCollectionQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamProblemCollectionQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamProblemCollectionQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamProblemCollectionVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
