package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample;

/**
 * 文件信息 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface FileInfoDao {

    /**
    * 保存文件信息
    *
    * @param record 文件信息
    * @return 影响记录数
    */
    int save(FileInfo record);

    /**
    * 根据ID删除文件信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 文件信息
    * @return 影响记录数
    */
    int updateById(FileInfo record);

    /**
    * 根据ID获取文件信息
    *
    * @param id 主键ID
    * @return 文件信息
    */
    FileInfo getById(Long id);

    /**
    * 文件信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<FileInfo> listForPage(int pageCurrent, int pageSize, FileInfoExample example);

}
