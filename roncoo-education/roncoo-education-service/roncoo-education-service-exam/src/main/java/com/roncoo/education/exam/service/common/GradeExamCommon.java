package com.roncoo.education.exam.service.common;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.enums.ExamAnswerStatusEnum;
import com.roncoo.education.common.core.enums.ExamProblemStatusEnum;
import com.roncoo.education.common.core.enums.GradeExamStatusEnum;
import com.roncoo.education.common.core.enums.ProblemTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamAndAnswerDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamProblemAndAnswerDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamProblemOptionAndAnswerDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamTitleAndAnswerDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 试卷信息
 *
 * @author LYQ
 */
@Component
public class GradeExamCommon extends BaseBiz {

    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeExamDao gradeExamDao;

    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;
    @Autowired
    private ExamTitleAuditDao examTitleAuditDao;
    @Autowired
    private ExamTitleProblemRefAuditDao examTitleProblemRefAuditDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeStudentExamAnswerDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;

    @Autowired
    private IFeignLecturer feignLecturer;

    /**
     * 获取试卷审核信息详情
     *
     * @param examId              试卷ID
     * @param isShowProblemAnswer 是否展示题目答案
     * @param isShowStudentAnswer 是否展示用户答案
     * @param relation            班级考试记录
     * @return 试卷审核信息
     */
    public AuthGradeExamAndAnswerDTO getGradeExamInfoAudit(Long examId, boolean isShowProblemAnswer, boolean isShowStudentAnswer, GradeExamStudentRelation relation) {
        //获取班级考试
        GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            throw new BaseException("班级考试不存在");
        }
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(examId);
        if (ObjectUtil.isNull(examInfoAudit)) {
            throw new BaseException("获取试卷信息异常");
        }

        // 试卷
        AuthGradeExamAndAnswerDTO dto = BeanUtil.copyProperties(gradeExam, AuthGradeExamAndAnswerDTO.class);
        dto.setTotalScore(examInfoAudit.getTotalScore());

        //获取班级信息
        GradeInfo gradeInfo = gradeInfoDao.getById(gradeExam.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            throw new BaseException("获取班级信息异常");
        }
        dto.setGradeName(gradeInfo.getGradeName());

        // 评阅状态
        dto.setAuditStatus(relation.getAuditStatus());

        // 获取讲师信息
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(gradeExam.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            throw new BaseException("获取讲师信息异常");
        }
        dto.setLecturerUserName(lecturerVO.getLecturerName());

        // 获取标题
        dto.setTitleList(listGradeExamTitle(examId, relation.getExamStatus(), isShowProblemAnswer, isShowStudentAnswer, relation.getId()));

        // 考试完成获取得分
        if (GradeExamStatusEnum.FINISH.getCode().equals(relation.getExamStatus())) {
            dto.setScore(relation.getScore());
            dto.setSysAuditTotalScore(relation.getSysAuditTotalScore());
            dto.setSysAuditScore(relation.getSysAuditScore());
            dto.setLecturerAuditTotalScore(relation.getLecturerAuditTotalScore());
            dto.setLecturerAuditScore(relation.getLecturerAuditScore());
        }
        return dto;
    }

    /**
     * 试卷标题审核
     *
     * @param examId              试卷ID
     * @param gradeExamStatus     班级考试状态
     * @param isShowProblemAnswer 是否展示题目答案
     * @param isShowStudentAnswer 是否展示用户答案
     * @param relationId          班级考试关联ID
     * @return 试卷审核标题
     */
    private List<AuthGradeExamTitleAndAnswerDTO> listGradeExamTitle(Long examId, Integer gradeExamStatus, boolean isShowProblemAnswer, boolean isShowStudentAnswer, Long relationId) {
        List<ExamTitleAudit> examTitleAuditList = examTitleAuditDao.listByExamId(examId);
        if (CollectionUtil.isEmpty(examTitleAuditList)) {
            return null;
        }

        //获取答案
        List<GradeStudentExamAnswer> gradeStudentExamAnswerList = new ArrayList<>();
        if (isShowStudentAnswer) {
            gradeStudentExamAnswerList = gradeStudentExamAnswerDao.listByRelationIdWithBLOBs(relationId);
            if (gradeStudentExamAnswerList == null) {
                gradeStudentExamAnswerList = new ArrayList<>();
            }
        }

        // 获取标题评分
        Map<Long, GradeStudentExamTitleScore> titleScoreMap = new HashMap<>();
        if (GradeExamStatusEnum.FINISH.getCode().equals(gradeExamStatus)) {
            List<GradeStudentExamTitleScore> titleScoreList = gradeStudentExamTitleScoreDao.listByRelationId(relationId);
            if (CollectionUtil.isNotEmpty(titleScoreList)) {
                titleScoreMap = titleScoreList.stream().collect(Collectors.toMap(GradeStudentExamTitleScore::getTitleId, titleScore -> titleScore));
            }
        }

        // 封装标题
        List<AuthGradeExamTitleAndAnswerDTO> resultList = BeanUtil.copyProperties(examTitleAuditList, AuthGradeExamTitleAndAnswerDTO.class);
        for (AuthGradeExamTitleAndAnswerDTO dto : resultList) {
            Map<Long, GradeStudentExamAnswer> studentExamAnswerMap = null;
            // 获取标题用户答案
            if (isShowStudentAnswer) {
                List<GradeStudentExamAnswer> studentExamAnswerList = gradeStudentExamAnswerList.stream().filter(gradeStudentExamAnswer -> dto.getId().equals(gradeStudentExamAnswer.getTitleId())).collect(Collectors.toList());
                studentExamAnswerMap = studentExamAnswerList.stream().collect(Collectors.toMap(GradeStudentExamAnswer::getProblemId, studentExamAnswer -> studentExamAnswer));
            }

            // 获取题目
            List<AuthGradeExamProblemAndAnswerDTO> problemList = listGradeExamProblem(dto, gradeExamStatus, isShowProblemAnswer, isShowStudentAnswer, studentExamAnswerMap);
            dto.setProblemList(problemList);

            // 标题总分
            if (CollectionUtil.isEmpty(problemList)) {
                dto.setTotalScore(0);
            } else {
                dto.setTotalScore(problemList.stream().mapToInt(AuthGradeExamProblemAndAnswerDTO::getScore).sum());
            }

            // 考试完成获取评分结果
            if (GradeExamStatusEnum.FINISH.getCode().equals(gradeExamStatus)) {
                GradeStudentExamTitleScore titleScore = titleScoreMap.get(dto.getId());
                if (ObjectUtil.isNotNull(titleScore)) {
                    dto.setScore(titleScore.getScore());
                    dto.setSysAuditTotalScore(titleScore.getSysAuditTotalScore());
                    dto.setSysAuditScore(titleScore.getSysAuditScore());
                    dto.setLecturerAuditTotalScore(titleScore.getLecturerAuditTotalScore());
                    dto.setLecturerAuditScore(titleScore.getLecturerAuditScore());
                }
            }
        }
        return resultList;
    }


    /**
     * 获取标题题目
     *
     * @param title                标题
     * @param gradeExamStatus      班级考试状态
     * @param isShowUserAnswer     是否展示答案
     * @param studentExamAnswerMap 学生考试答案
     * @return 标题题目
     */
    private List<AuthGradeExamProblemAndAnswerDTO> listGradeExamProblem(AuthGradeExamTitleAndAnswerDTO title, Integer gradeExamStatus, boolean isShowProblemAnswer, boolean isShowUserAnswer, Map<Long, GradeStudentExamAnswer> studentExamAnswerMap) {
        // 获取标题题目
        List<ExamTitleProblemRefAudit> refList = examTitleProblemRefAuditDao.listByTitleId(title.getId());
        if (CollectionUtil.isEmpty(refList)) {
            return null;
        }
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRefAudit::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return null;
        }
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, examProblem -> examProblem));

        // 封装返回题目
        List<AuthGradeExamProblemAndAnswerDTO> resultList = new ArrayList<>();
        if (ProblemTypeEnum.MATERIAL.getCode().equals(title.getTitleType())) {
            // 组合题
            List<ExamTitleProblemRefAudit> parentProblemRefList = refList.stream().filter(refAudit -> ProblemTypeEnum.MATERIAL.getCode().equals(refAudit.getProblemType())).collect(Collectors.toList());
            for (ExamTitleProblemRefAudit ref : parentProblemRefList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }
                AuthGradeExamProblemAndAnswerDTO dto = BeanUtil.copyProperties(problem, AuthGradeExamProblemAndAnswerDTO.class);

                // 题目分值
                dto.setProblemScore(ref.getScore());
                dto.setScore(0);

                // 小题
                dto.setChildrenList(listProblemChildren(ref.getProblemId(), gradeExamStatus, refList, problemMap, isShowProblemAnswer, isShowUserAnswer, studentExamAnswerMap));
                resultList.add(dto);
            }
        } else {
            // 其他题型
            for (ExamTitleProblemRefAudit ref : refList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }
                AuthGradeExamProblemAndAnswerDTO dto = BeanUtil.copyProperties(problem, AuthGradeExamProblemAndAnswerDTO.class);
                // 题目分值
                dto.setProblemScore(ref.getScore());

                if (ProblemTypeEnum.THE_RADIO.getCode().equals(title.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(title.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(title.getTitleType())) {
                    // 获取选项
                    dto.setOptionList(BeanUtil.copyProperties(examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId()), AuthGradeExamProblemOptionAndAnswerDTO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(title.getTitleType())) {
                    // 获取填空数
                    dto.setOptionCount(StrUtil.isBlank(dto.getProblemAnswer()) ? 0 : dto.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                }

                // 用户答案
                setProblemAnswer(dto, gradeExamStatus, isShowProblemAnswer, isShowUserAnswer, studentExamAnswerMap);
                resultList.add(dto);
            }
        }
        return resultList;
    }

    /**
     * 获取小题
     *
     * @param problemParentId      题目父ID
     * @param gradeExamStatus      班级考试状态
     * @param refList              关联集合
     * @param problemMap           题目集合
     * @param isShowProblemAnswer  是否展示题目答案
     * @param isShowUserAnswer     是否展示用户答案
     * @param studentExamAnswerMap 学生考试答案
     * @return 小题
     */
    public List<AuthGradeExamProblemAndAnswerDTO> listProblemChildren(Long problemParentId, Integer gradeExamStatus, List<ExamTitleProblemRefAudit> refList, Map<Long, ExamProblem> problemMap, boolean isShowProblemAnswer, boolean isShowUserAnswer, Map<Long, GradeStudentExamAnswer> studentExamAnswerMap) {
        // 获取小题
        List<ExamTitleProblemRefAudit> problemRefList = refList.stream().filter(refAudit -> problemParentId.equals(refAudit.getProblemParentId())).collect(Collectors.toList());

        List<AuthGradeExamProblemAndAnswerDTO> childrenList = new ArrayList<>();
        for (ExamTitleProblemRefAudit ref : problemRefList) {
            ExamProblem problem = problemMap.get(ref.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }
            AuthGradeExamProblemAndAnswerDTO dto = BeanUtil.copyProperties(problem, AuthGradeExamProblemAndAnswerDTO.class);
            dto.setProblemScore(ref.getScore());

            if (ProblemTypeEnum.THE_RADIO.getCode().equals(dto.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(dto.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(dto.getProblemType())) {
                // 获取选项
                dto.setOptionList(BeanUtil.copyProperties(examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId()), AuthGradeExamProblemOptionAndAnswerDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(dto.getProblemType())) {
                // 获取填空
                dto.setOptionCount(StrUtil.isBlank(dto.getProblemAnswer()) ? 0 : dto.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
            }

            // 用户答案
            setProblemAnswer(dto, gradeExamStatus, isShowProblemAnswer, isShowUserAnswer, studentExamAnswerMap);
            childrenList.add(dto);
        }
        return childrenList;
    }

    /**
     * 设置题目答案
     *
     * @param dto                  题目
     * @param gradeExamStatus      班级考试状态
     * @param isShowProblemAnswer  是否展示题目答案
     * @param isShowUserAnswer     是否展示用户答案
     * @param studentExamAnswerMap 学生考试答案
     */
    private void setProblemAnswer(AuthGradeExamProblemAndAnswerDTO dto, Integer gradeExamStatus, boolean isShowProblemAnswer, boolean isShowUserAnswer, Map<Long, GradeStudentExamAnswer> studentExamAnswerMap) {
        dto.setScore(0);

        // 是否展示题目答案
        if (!isShowProblemAnswer) {
            dto.setProblemAnswer(null);
        }

        // 是否展示答案
        if (!isShowUserAnswer) {
            return;
        }

        // 若考试完成，整个标题都没有答案，视为错误
        if (CollectionUtil.isEmpty(studentExamAnswerMap)) {
            if (GradeExamStatusEnum.FINISH.getCode().equals(gradeExamStatus)) {
                dto.setProblemStatus(ExamProblemStatusEnum.NO.getCode());
            }
            return;
        }

        GradeStudentExamAnswer studentExamAnswer = studentExamAnswerMap.get(dto.getId());
        if (ObjectUtil.isNull(studentExamAnswer)) {
            // 若考试完成，没有考试答案视为错误
            if (GradeExamStatusEnum.FINISH.getCode().equals(gradeExamStatus)) {
                dto.setProblemStatus(ExamProblemStatusEnum.NO.getCode());
            }
            return;
        }
        dto.setUserAnswerId(studentExamAnswer.getId());
        dto.setUserAnswer(studentExamAnswer.getUserAnswer());
        dto.setScore(studentExamAnswer.getScore());
        dto.setAnswerStatus(studentExamAnswer.getAnswerStatus());
        dto.setSysAudit(studentExamAnswer.getSysAudit());
        dto.setAuditUserNo(studentExamAnswer.getAuditUserNo());

        // 如果是考试完成判断题目对错
        if (GradeExamStatusEnum.FINISH.getCode().equals(gradeExamStatus)) {
            if (ExamAnswerStatusEnum.WAIT.getCode().equals(studentExamAnswer.getAnswerStatus())) {
                dto.setProblemStatus(ExamProblemStatusEnum.WAIT.getCode());
            } else {
                dto.setProblemStatus(studentExamAnswer.getScore() > 0 ? ExamProblemStatusEnum.YES.getCode() : ExamProblemStatusEnum.NO.getCode());
            }
        }

    }
}
