package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 班级考试标题带答案响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamTitleAndAnswerDTO", description = "班级考试标题带答案响应对象")
public class AuthGradeExamTitleAndAnswerDTO implements Serializable {

    private static final long serialVersionUID = -7638190570842691451L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "标题类型")
    private Integer titleType;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;

    @ApiModelProperty(value = "题目集合")
    private List<AuthGradeExamProblemAndAnswerDTO> problemList;
}
