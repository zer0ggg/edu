package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级操作日志请求对象
 *
 * @author wujing
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "GradeOperationLogListREQ", description = "班级操作日志请求对象")
public class GradeOperationLogListREQ extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "操作员用户编号")
    private Long operatorUserNo;

    @ApiModelProperty(value = "操作类型")
    private Integer operationType;
}
