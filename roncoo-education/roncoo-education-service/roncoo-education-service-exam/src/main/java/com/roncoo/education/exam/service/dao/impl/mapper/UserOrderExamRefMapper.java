package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserOrderExamRefMapper {
    int countByExample(UserOrderExamRefExample example);

    int deleteByExample(UserOrderExamRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserOrderExamRef record);

    int insertSelective(UserOrderExamRef record);

    List<UserOrderExamRef> selectByExample(UserOrderExamRefExample example);

    UserOrderExamRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserOrderExamRef record, @Param("example") UserOrderExamRefExample example);

    int updateByExample(@Param("record") UserOrderExamRef record, @Param("example") UserOrderExamRefExample example);

    int updateByPrimaryKeySelective(UserOrderExamRef record);

    int updateByPrimaryKey(UserOrderExamRef record);
}