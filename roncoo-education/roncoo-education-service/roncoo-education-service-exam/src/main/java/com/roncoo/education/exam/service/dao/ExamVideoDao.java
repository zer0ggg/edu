package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample;

/**
 * 试卷视频 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamVideoDao {

    /**
    * 保存试卷视频
    *
    * @param record 试卷视频
    * @return 影响记录数
    */
    int save(ExamVideo record);

    /**
    * 根据ID删除试卷视频
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷视频
    * @return 影响记录数
    */
    int updateById(ExamVideo record);

    /**
    * 根据ID获取试卷视频
    *
    * @param id 主键ID
    * @return 试卷视频
    */
    ExamVideo getById(Long id);

    /**
    * 试卷视频--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamVideo> listForPage(int pageCurrent, int pageSize, ExamVideoExample example);

    /**
     * 根据视频vid查找
     * @param vid
     * @return
     */
    ExamVideo getByVideoVid(String vid);

    /**
     * 根据vid更新
     * @param record
     * @return
     */
    int updateByVid(ExamVideo record);
}
