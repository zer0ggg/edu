package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRefExample;

/**
 * 用户订单试卷关联 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserOrderExamRefDao {

    /**
    * 保存用户订单试卷关联
    *
    * @param record 用户订单试卷关联
    * @return 影响记录数
    */
    int save(UserOrderExamRef record);

    /**
    * 根据ID删除用户订单试卷关联
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户订单试卷关联
    * @return 影响记录数
    */
    int updateById(UserOrderExamRef record);

    /**
    * 根据ID获取用户订单试卷关联
    *
    * @param id 主键ID
    * @return 用户订单试卷关联
    */
    UserOrderExamRef getById(Long id);

    /**
    * 用户订单试卷关联--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<UserOrderExamRef> listForPage(int pageCurrent, int pageSize, UserOrderExamRefExample example);

    /**
     * 根据用户编号和试卷ID获取用户订单试卷
     *
     * @param userNo 用户编号
     * @param examId 试卷ID
     * @return 用户订单试卷
     */
    UserOrderExamRef getByUserNoAndExamId(Long userNo, Long examId);

}
