package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExamDownload;
import com.roncoo.education.exam.feign.qo.UserExamDownloadQO;
import com.roncoo.education.exam.feign.vo.UserExamDownloadVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamDownloadBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户试卷下载
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamDownloadController extends BaseController implements IFeignUserExamDownload{

    @Autowired
    private FeignUserExamDownloadBiz biz;

	@Override
	public Page<UserExamDownloadVO> listForPage(@RequestBody UserExamDownloadQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamDownloadQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamDownloadQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamDownloadVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
