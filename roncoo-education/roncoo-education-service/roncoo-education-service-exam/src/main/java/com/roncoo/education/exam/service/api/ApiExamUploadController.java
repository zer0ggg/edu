/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.exam.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.service.api.biz.ApiExamUploadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 试卷视频上传接口
 *
 * @author wuyun
 */
@Api(tags = "API-试卷视频上传管理")
@RestController
@RequestMapping(value = "/exam/api/upload")
public class ApiExamUploadController extends BaseController {

    @Autowired
    private ApiExamUploadBiz biz;

    /**
     * 试卷上传视频接口
     *
     * @param videoFile
     * @author wuyun
     */
    @ApiOperation(value = "试卷上传视频接口", notes = "试卷上传视频接口")
    @RequestMapping(value = "/video", method = RequestMethod.POST)
    public Result<String> uploadVideo(@RequestParam(value = "videoFile", required = false) MultipartFile videoFile) {
        return biz.uploadVideo(videoFile);
    }

}
