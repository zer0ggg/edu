package com.roncoo.education.exam.common.bo.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 班级考试分页请求对象
 *
 * @author LYQ
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeExamPageBO", description = "班级考试分页请求对象")
public class AuthGradeExamPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 8910200923458610947L;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始考试时间")
    private Date beginExamTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束考试时间")
    private Date endExamTime;

}
