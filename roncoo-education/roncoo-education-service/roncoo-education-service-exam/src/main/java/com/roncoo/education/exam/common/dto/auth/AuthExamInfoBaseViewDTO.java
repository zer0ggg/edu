package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 试卷详情
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoBaseViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷ID")
    private Long id;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "年级ID")
    private Long gradeId;

    @ApiModelProperty(value = "年级名称")
    private String gradeName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "年份ID")
    private Long yearId;

    @ApiModelProperty(value = "年份名称")
    private String yearName;

    @ApiModelProperty(value = "来源ID")
    private Long sourceId;

    @ApiModelProperty(value = "来源名称")
    private String sourceName;

    @ApiModelProperty(value = "是否被收藏")
    private Boolean isCollect;

    @ApiModelProperty(value = "是否已经购买")
    private Boolean isPay;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "答卷时间（分钟）")
    private Integer answerTime;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "浏览人数")
    private Integer studyCount;

    @ApiModelProperty(value = "下载人数")
    private Integer downloadCount;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "记录ID，如果用户的该试卷如果在考试中则返回")
    private Long recordId;
}
