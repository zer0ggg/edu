package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷信息审核表-保存
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemOptionSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "选项内容")
    private String optionContent;
}
