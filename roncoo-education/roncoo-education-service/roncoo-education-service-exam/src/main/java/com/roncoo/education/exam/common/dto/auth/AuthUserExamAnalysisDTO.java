package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户试卷分析
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */

@Data
@Accessors(chain = true)
@ApiModel(value = "AuthUserExamAnalysisDTO", description = "用户试卷分析")
public class AuthUserExamAnalysisDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<AuthExamProblemCategoryDTO> list = new ArrayList<>();

}
