package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.CourseExamRefDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程试卷关联
 *
 * @author wujing
 */
@Component
public class ApiCourseExamRefBiz extends BaseBiz {

    @Autowired
    private CourseExamRefDao dao;

}
