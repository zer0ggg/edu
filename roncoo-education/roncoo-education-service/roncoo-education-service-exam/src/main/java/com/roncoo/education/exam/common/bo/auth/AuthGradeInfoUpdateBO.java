package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级信息更新对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeInfoUpdateBO", description = "班级信息更新对象")
public class AuthGradeInfoUpdateBO implements Serializable {

    private static final long serialVersionUID = 1613959485681077854L;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @NotBlank(message = "班级名称不能为空")
    @ApiModelProperty(value = "班级名称", required = true)
    private String gradeName;

    @NotBlank(message = "班级简介不能为空")
    @ApiModelProperty(value = "班级简介", required = true)
    private String gradeIntro;

    @NotNull(message = "班级权限不能为空")
    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)", required = true)
    private Integer verificationSet;
}
