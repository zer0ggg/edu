package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷信息审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamInfoAuditViewBO", description = "试卷信息查看请求对象")
public class AuthExamInfoAuditViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long id;

}
