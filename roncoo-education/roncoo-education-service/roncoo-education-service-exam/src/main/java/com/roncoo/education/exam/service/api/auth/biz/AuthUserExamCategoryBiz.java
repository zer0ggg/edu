package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.UserExamCategorySaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamCategoryBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamCategoryUpdateBO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamCategoryDTO;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户试卷分类
 *
 * @author wujing
 */
@Component
public class AuthUserExamCategoryBiz extends BaseBiz {

    @Autowired
    private UserExamCategoryDao dao;
    @Autowired
    private IFeignUserExt iFeignUserExt;

    public Result<Page<AuthUserExamCategoryDTO>> list(AuthUserExamCategoryBO bo) {
        UserExamCategoryExample example = new UserExamCategoryExample();
        UserExamCategoryExample.Criteria c = example.createCriteria();
        if (ThreadContext.userNo() == null) {
            return Result.error("用户编号不能为空");
        } else {
            c.andUserNoEqualTo(ThreadContext.userNo());
        }
        if (bo.getUserType() != null) {
            c.andUserTypeEqualTo(bo.getCategoryType());
        }
        if (bo.getCategoryType() != null) {
            c.andCategoryTypeEqualTo(bo.getCategoryType());
        }
        if (!StringUtils.isEmpty(bo.getCategoryName())) {
            c.andCategoryNameEqualTo(PageUtil.rightLike(bo.getCategoryName()));
        } else {
            c.andFloorEqualTo(1);
        }
        example.setOrderByClause(" sort asc,status_id desc, id desc ");
        Page<UserExamCategory> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserExamCategoryDTO> pageDto = PageUtil.transform(page, AuthUserExamCategoryDTO.class);
        for (AuthUserExamCategoryDTO dto : pageDto.getList()) {
            dto.setList(recursionList(dto.getId()));
        }
        return Result.success(pageDto);
    }

    /**
     * 递归展示分类
     *
     * @param parentId 父类ID
     * @return 用户试卷子分类
     */
    private List<AuthUserExamCategoryDTO> recursionList(Long parentId) {
        List<AuthUserExamCategoryDTO> list = new ArrayList<>();
        List<UserExamCategory> userExamCategoryList = dao.listByParentId(parentId);
        if (!CollectionUtils.isEmpty(userExamCategoryList)) {
            for (UserExamCategory examCategory : userExamCategoryList) {
                AuthUserExamCategoryDTO dto = BeanUtil.copyProperties(examCategory, AuthUserExamCategoryDTO.class);
                dto.setList(recursionList(examCategory.getId()));
                list.add(dto);
            }
        }
        return list;
    }

    /**
     * 保存考试分类信息
     *
     * @param bo 用户试卷分类保存参数
     * @return 保存结果
     */
    public Result<String> save(UserExamCategorySaveBO bo) {
        UserExamCategory record = BeanUtil.copyProperties(bo, UserExamCategory.class);
        // 校验用户
        UserExtVO user = iFeignUserExt.getByUserNo(ThreadContext.userNo());
        if (user == null) {
            return Result.error("用户编号不正确");
        }
        record.setUserType(user.getUserType());
        if (Integer.valueOf(1).equals(bo.getCategoryType())) {
            record.setUserType(bo.getCategoryType());
        } else if (Integer.valueOf(2).equals(bo.getCategoryType())) {
            record.setUserType(bo.getCategoryType());
        } else {
            return Result.error("类型只能为(1:试题；2：试卷）");
        }

        // 查找父一级
        UserExamCategory parent = null;
        if (bo.getParentId() != null && bo.getParentId() != 0) {
            parent = dao.getById(bo.getParentId());
            if (parent == null) {
                return Result.error("父id错误");
            }
        }
        // 继承父一级属性
        if (parent != null) {
            record.setCategoryType(parent.getCategoryType());
            record.setParentId(parent.getId());
            record.setFloor(parent.getFloor() + 1);
        } else {
            record.setParentId(0L);
            record.setFloor(1);
        }
        // 补充默认信息
        if (bo.getSort() == null) {
            record.setSort(1);
        }
        if (dao.save(record) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> delete(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        UserExamCategory record = dao.getById(id);
        if (ObjectUtils.isEmpty(record)) {
            return Result.error("id不正确");
        }
        List<UserExamCategory> sonList = dao.listByParentId(id);
        if (!CollectionUtils.isEmpty(sonList)) {
            return Result.error("删除失败，请先删除子一级分类");
        }
        // todo 删除前检验该分类未被用户使用
        if (dao.deleteById(id) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> update(AuthUserExamCategoryUpdateBO bo) {
        UserExamCategory record = dao.getById(bo.getId());
        if (ObjectUtils.isEmpty(record)) {
            return Result.error("id不正确");
        }
        UserExamCategory category = BeanUtil.copyProperties(bo, UserExamCategory.class);

        if (dao.updateById(category) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }
}
