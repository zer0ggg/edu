package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ExamAuditStatusEnum;
import com.roncoo.education.common.core.enums.GradeExamStatusEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationEditREQ;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationGradeForPageREQ;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationListREQ;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleScoreRESP;
import com.roncoo.education.exam.common.resp.GradeExamStudentRelationGradeForPageRESP;
import com.roncoo.education.exam.common.resp.GradeExamStudentRelationListRESP;
import com.roncoo.education.exam.common.resp.GradeExamStudentRelationViewRESP;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wujing
 */
@Component
public class PcGradeExamStudentRelationBiz extends BaseBiz {

    @Autowired
    private GradeExamStudentRelationDao dao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private GradeExamDao gradeExamDao;

    @Autowired
    private IFeignUserExt iFeignUserExt;

    /**
     * 列表
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeExamStudentRelationListRESP>> list(GradeExamStudentRelationListREQ req) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(req.getGradeId())) {
            c.andGradeIdEqualTo(req.getGradeId());
        }
        if (ObjectUtil.isNotNull(req.getGradeExamId())) {
            c.andGradeExamIdEqualTo(req.getGradeExamId());
        }
        if (ObjectUtil.isNotNull(req.getStudentId())) {
            c.andStudentIdEqualTo(req.getStudentId());
        }
        if (ObjectUtil.isNotNull(req.getViewStatus())) {
            c.andViewStatusEqualTo(req.getViewStatus());
        }
        if (ObjectUtil.isNotNull(req.getExamStatus())) {
            c.andExamStatusEqualTo(req.getExamStatus());
        }
        Page<GradeExamStudentRelation> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeExamStudentRelationListRESP> respPage = PageUtil.transform(page, GradeExamStudentRelationListRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }

        for (GradeExamStudentRelationListRESP resp : respPage.getList()) {
            GradeStudent gradeStudent = gradeStudentDao.getById(resp.getStudentId());
            if (ObjectUtil.isNull(gradeStudent)) {
                continue;
            }
            resp.setNickname(gradeStudent.getNickname());
            resp.setUserRole(gradeStudent.getUserRole());
        }
        return Result.success(respPage);
    }

    /**
     * 添加
     *
     * @param gradeExamStudentRelationSaveREQ
     * @return 添加结果
     */
    public Result<String> save(GradeExamStudentRelationSaveREQ gradeExamStudentRelationSaveREQ) {
        GradeExamStudentRelation record = BeanUtil.copyProperties(gradeExamStudentRelationSaveREQ, GradeExamStudentRelation.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 查看
     *
     * @param id 主键ID
     * @return
     */
    public Result<GradeExamStudentRelationViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), GradeExamStudentRelationViewRESP.class));
    }

    /**
     * 修改
     *
     * @param gradeExamStudentRelationEditREQ 修改对象
     * @return 修改结果
     */
    public Result<String> edit(GradeExamStudentRelationEditREQ gradeExamStudentRelationEditREQ) {
        GradeExamStudentRelation record = BeanUtil.copyProperties(gradeExamStudentRelationEditREQ, GradeExamStudentRelation.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     *
     * @return  学员成绩列表响应
     */
    public Result<Page<GradeExamStudentRelationGradeForPageRESP>> listGradeForPage(GradeExamStudentRelationGradeForPageREQ req){

        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        Criteria c = example.createCriteria();   //条件查询
        if(req.getGradeId() != null){
            c.andGradeIdEqualTo(req.getGradeId());
        }
        //班级试卷名称
        if(StrUtil.isNotEmpty(req.getGradeExamName())){
            c.andGradeExamNameEqualTo(req.getGradeExamName());
        }
        //手机号码
        if(StrUtil.isNotBlank(req.getMobile())){
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setMobile(req.getMobile());
            UserExtVO userExtVO = iFeignUserExt.getByMobile(userExtQO);
            if(ObjectUtil.isNotNull(userExtVO)){
                c.andUserNoEqualTo(userExtVO.getUserNo());
            }
        }
        if(ObjectUtil.isNotNull(req.getBeginRegisterTime())){
                c.andGmtCreateGreaterThanOrEqualTo(DateUtil.parseDate(req.getBeginRegisterTime(), "yyyy-MM-dd"));
        }
        //结束时间
        if(ObjectUtil.isNotNull(req.getEndRegisterTime())){
            c.andGmtCreateLessThanOrEqualTo(DateUtil.addDate(DateUtil.parseDate(req.getEndRegisterTime(),"yyyy-MM-dd"),1));
        }

        c.andAuditStatusEqualTo(ExamAuditStatusEnum.COMPLETE_AUDIT.getCode());
        c.andExamStatusEqualTo(GradeExamStatusEnum.FINISH.getCode());
        example.setOrderByClause("sort asc, id desc");
        //判断页面
        Page<GradeExamStudentRelation> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        if (ObjectUtil.isNull(page.getList())) {
            return Result.success(new Page<>());
        }
        Page<GradeExamStudentRelationGradeForPageRESP> agesRelationPageRESP = PageUtil.transform(page, GradeExamStudentRelationGradeForPageRESP.class);
        for (GradeExamStudentRelationGradeForPageRESP resp : agesRelationPageRESP.getList()) {

            if(resp.getStatusId() == 0 || resp.getStatusId() == null){
                continue;
            }else {
                //获取学生信息-姓名（昵称）
                GradeStudent gradeStudent = gradeStudentDao.getById(resp.getStudentId());
                if (ObjectUtil.isNull(gradeStudent)) {
                    logger.error("学生信息不存在");
                    continue;
                }
                //获取用户信息-身份证号，手机号码
                UserExtVO userExtVO = iFeignUserExt.getByUserNo(gradeStudent.getUserNo());
                if (ObjectUtil.isNull(userExtVO)) {
                    logger.error("用户不存在");
                    continue;
                }
                //1、获取试卷
                ExamInfo examInfo = examInfoDao.getById(resp.getExamId());
                //获取试卷名称
                resp.setExamName(examInfo.getExamName());
                if (ObjectUtil.isNull(examInfo)) {
                    logger.error("考试试卷不存在");
                    continue;
                }
                //2、通过试卷获取科目
                ExamCategory examCategory = examCategoryDao.getById(examInfo.getSubjectId());
                if (ObjectUtil.isNull(examCategory)) {
                    logger.error("考试科目不存在");
                    continue;
                }
                //获取用户考试答案-成绩（选择题，判断题，总数）
                //1、根据试卷获取试卷标题
                List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
                if (CollectionUtil.isEmpty(examTitleList)) {
                    logger.error("考试试卷标题不存在");
                    continue;
                }

                //2、通过分类标题获取不同类型题目分数
                List<ExamTitleScoreRESP> ExamTitleScoreRESP = ArrayListUtil.copy(examTitleList, ExamTitleScoreRESP.class);
                // 初始化总得分
                int studentScore = 0;
                for (ExamTitleScoreRESP titleScoreRESP : ExamTitleScoreRESP) {
                    // 根据班级考试id、标题ID获取标题考试成绩
                    GradeStudentExamTitleScore gradeStudentExamTitleScore = gradeStudentExamTitleScoreDao.getByRelationIdAndTitleId(resp.getId(), titleScoreRESP.getId());
                    if (ObjectUtil.isNotNull(gradeStudentExamTitleScore)) {
                        titleScoreRESP.setScore(gradeStudentExamTitleScore.getScore());
                    }else {
                        titleScoreRESP.setScore(0);
                    }
                    // 总得分
                    studentScore = studentScore + titleScoreRESP.getScore();
                }


                resp.setNickname(gradeStudent.getNickname());   //学员姓名
                resp.setIdCardNo(userExtVO.getIdCardNo());      //学员身份证
                resp.setMobile(userExtVO.getMobile());          //学员手机号
                resp.setSubjectId(examInfo.getSubjectId());     //学员考试科目
                resp.getBeginAnswerTime();                      //答卷时间
                resp.getEndAnswerTime();                        //结束搭建试卷
                //答卷结束时间 答卷考试开始时间之差 算出完成考试花费度多少分钟
                Long  minute = ((resp.getEndAnswerTime().getTime() - resp.getBeginAnswerTime().getTime())/1000) /60;
                Integer minutes = minute.intValue();   //算出时间转换类型
                resp.setMinute(minutes);                        //答卷花费多少分钟
                resp.setSubjectName(examCategory.getCategoryName());    //试卷科目名称
                resp.setTitlelist(ExamTitleScoreRESP);
                resp.setScore(studentScore); //总分数
            }

        }
        return Result.success(agesRelationPageRESP);
    }
}
