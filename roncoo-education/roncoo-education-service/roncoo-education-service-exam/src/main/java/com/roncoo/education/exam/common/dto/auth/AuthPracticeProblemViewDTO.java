package com.roncoo.education.exam.common.dto.auth;

import com.roncoo.education.exam.common.dto.ExamProblemOptionDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 练习试题详情信息
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeProblemViewDTO ", description="练习试题详情信息")
public class AuthPracticeProblemViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "题目答案")
    private String problemAnswer;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "分值")
    private Integer score;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @ApiModelProperty(value = "选项集合")
    private List<ExamProblemOptionDTO> optionList;

    @ApiModelProperty(value = "填空数")
    private Integer optionCount;

    @ApiModelProperty(value = "用户答案集合")
    private List<AuthPracticeUserAnswerDTO> answerList;
}
