package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级学生分页请求参数
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "GradeStudentListREQ", description = "班级学生分页请求参数")
public class GradeStudentPageREQ extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户角色(1:普通成员，2:管理员)")
    private Integer userRole;

    @ApiModelProperty(value = "昵称")
    private String nickname;
}
