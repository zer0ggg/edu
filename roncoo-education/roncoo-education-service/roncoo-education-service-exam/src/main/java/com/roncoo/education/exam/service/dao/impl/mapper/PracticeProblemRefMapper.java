package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PracticeProblemRefMapper {
    int countByExample(PracticeProblemRefExample example);

    int deleteByExample(PracticeProblemRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PracticeProblemRef record);

    int insertSelective(PracticeProblemRef record);

    List<PracticeProblemRef> selectByExample(PracticeProblemRefExample example);

    PracticeProblemRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PracticeProblemRef record, @Param("example") PracticeProblemRefExample example);

    int updateByExample(@Param("record") PracticeProblemRef record, @Param("example") PracticeProblemRefExample example);

    int updateByPrimaryKeySelective(PracticeProblemRef record);

    int updateByPrimaryKey(PracticeProblemRef record);
}