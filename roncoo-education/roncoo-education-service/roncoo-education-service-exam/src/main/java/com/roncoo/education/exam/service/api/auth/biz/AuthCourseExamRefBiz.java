package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.interfaces.IFeignCourseAudit;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterAudit;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriodAudit;
import com.roncoo.education.course.feign.vo.CourseAuditVO;
import com.roncoo.education.course.feign.vo.CourseChapterAuditVO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodAuditVO;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefDeleteBO;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthCourseExamRefViewDTO;
import com.roncoo.education.exam.service.dao.CourseExamRefDao;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程试卷关联
 *
 * @author wujing
 */
@Component
public class AuthCourseExamRefBiz extends BaseBiz {

    @Autowired
    private CourseExamRefDao dao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;

    @Autowired
    private IFeignCourseAudit feignCourseAudit;
    @Autowired
    private IFeignCourseChapterAudit feignCourseChapterAudit;
    @Autowired
    private IFeignCourseChapterPeriodAudit feignCourseChapterPeriodAudit;

    public Result<String> save(AuthCourseExamRefSaveBO bo) {

        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(bo.getExamId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("找不到试卷信息");
        }

        CourseAuditVO courseAudit = feignCourseAudit.getById(bo.getCourseId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到课程信息");
        }

        if (RefTypeEnum.COURSE.getCode().equals(bo.getRefType())) {
            CourseAuditVO courseAuditVO = feignCourseAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseAuditVO)) {
                return Result.error("找不到课程信息");
            }
        }
        if (RefTypeEnum.CHAPTER.getCode().equals(bo.getRefType())) {
            CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterAuditVO)) {
                return Result.error("找不到章节信息");
            }
        }
        if (RefTypeEnum.PERIOD.getCode().equals(bo.getRefType())) {
            CourseChapterPeriodAuditVO courseChapterPeriodAuditVO = feignCourseChapterPeriodAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriodAuditVO)) {
                return Result.error("找不到课时信息");
            }
        }
        CourseExamRef record = BeanUtil.copyProperties(bo, CourseExamRef.class);
        record.setCourseCategory(courseAudit.getCourseCategory());
        int results = 0;
        if (bo.getId() == null) {
            CourseExamRef courseExamRef = dao.getByRefIdAndExamId(bo.getRefId(), bo.getExamId());
            if (ObjectUtil.isNotNull(courseExamRef)) {
                return Result.error("已绑定试卷");
            }
            results = dao.save(record);
        } else {
            results = dao.updateById(record);
        }
        if (results > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    public Result<AuthCourseExamRefViewDTO> view(AuthCourseExamRefViewBO bo) {
        CourseExamRef courseExamRef = dao.getByRefIdAndExamId(bo.getRefId(), bo.getExamId());
        if (ObjectUtil.isNull(courseExamRef)) {
            return Result.success(null);
        }
        AuthCourseExamRefViewDTO dto = BeanUtil.copyProperties(courseExamRef, AuthCourseExamRefViewDTO.class);
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(bo.getExamId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("找不到试卷信息");
        }
        dto.setExamName(examInfoAudit.getExamName());
        if (RefTypeEnum.COURSE.getCode().equals(bo.getCourseType())) {
            CourseAuditVO courseAuditVO = feignCourseAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseAuditVO)) {
                return Result.error("找不到课程信息");
            }
            dto.setRefName(courseAuditVO.getCourseName());
        }
        if (RefTypeEnum.CHAPTER.getCode().equals(bo.getCourseType())) {
            CourseChapterAuditVO courseChapterAuditVO = feignCourseChapterAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterAuditVO)) {
                return Result.error("找不到章节信息");
            }
            dto.setRefName(courseChapterAuditVO.getChapterName());
        }
        if (RefTypeEnum.PERIOD.getCode().equals(bo.getCourseType())) {
            CourseChapterPeriodAuditVO courseChapterPeriodAuditVO = feignCourseChapterPeriodAudit.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriodAuditVO)) {
                return Result.error("找不到课时信息");
            }
            dto.setRefName(courseChapterPeriodAuditVO.getPeriodName());
        }
        return Result.success(dto);
    }

    public Result<String> delete(AuthCourseExamRefDeleteBO bo) {
        if ( dao.deleteById(bo.getId()) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
