package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamVideoQO;
import com.roncoo.education.exam.feign.vo.ExamVideoVO;
import com.roncoo.education.exam.service.dao.ExamVideoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷视频
 *
 * @author wujing
 */
@Component
public class FeignExamVideoBiz extends BaseBiz {

    @Autowired
    private ExamVideoDao dao;

	public Page<ExamVideoVO> listForPage(ExamVideoQO qo) {
	    ExamVideoExample example = new ExamVideoExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamVideo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamVideoVO.class);
	}

	public int save(ExamVideoQO qo) {
		ExamVideo record = BeanUtil.copyProperties(qo, ExamVideo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamVideoVO getById(Long id) {
		ExamVideo record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamVideoVO.class);
	}

	public int updateById(ExamVideoQO qo) {
		ExamVideo record = BeanUtil.copyProperties(qo, ExamVideo.class);
		return dao.updateById(record);
	}

    public ExamVideoVO getByVideoVid(String vid) {
		return BeanUtil.copyProperties(dao.getByVideoVid(vid), ExamVideoVO.class);
    }

	public int updateByVid(ExamVideoQO examVideoQO) {
		ExamVideo record = BeanUtil.copyProperties(examVideoQO, ExamVideo.class);
		return dao.updateByVid(record);
	}
}
