package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample;

/**
 * 课程试卷关联 服务类
 *
 * @author wujing
 * @date 2020-10-21
 */
public interface CourseExamRefDao {

    /**
     * 保存课程试卷关联
     *
     * @param record 课程试卷关联
     * @return 影响记录数
     */
    int save(CourseExamRef record);

    /**
     * 根据ID删除课程试卷关联
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 课程试卷关联
     * @return 影响记录数
     */
    int updateById(CourseExamRef record);

    /**
     * 根据ID获取课程试卷关联
     *
     * @param id 主键ID
     * @return 课程试卷关联
     */
    CourseExamRef getById(Long id);

    /**
     * 课程试卷关联--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<CourseExamRef> listForPage(int pageCurrent, int pageSize, CourseExamRefExample example);

    /**
     * @param refId
     * @param examId
     * @return
     */
    CourseExamRef getByRefIdAndExamId(Long refId, Long examId);

    /**
     * 根据关联ID、关联类型获取课程试卷关联信息
     *
     * @param refId
     * @param refType
     * @return
     */
    CourseExamRef getByRefIdAndRefType(Long refId, Integer refType);
}
