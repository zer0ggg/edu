package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeStudentMapper {
    int countByExample(GradeStudentExample example);

    int deleteByExample(GradeStudentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeStudent record);

    int insertSelective(GradeStudent record);

    List<GradeStudent> selectByExample(GradeStudentExample example);

    GradeStudent selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeStudent record, @Param("example") GradeStudentExample example);

    int updateByExample(@Param("record") GradeStudent record, @Param("example") GradeStudentExample example);

    int updateByPrimaryKeySelective(GradeStudent record);

    int updateByPrimaryKey(GradeStudent record);
}
