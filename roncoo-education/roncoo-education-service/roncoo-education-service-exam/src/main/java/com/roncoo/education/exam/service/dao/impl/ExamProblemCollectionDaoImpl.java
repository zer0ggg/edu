package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamProblemCollectionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamProblemCollectionMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollection;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷题目收藏 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamProblemCollectionDaoImpl implements ExamProblemCollectionDao {

    @Autowired
    private ExamProblemCollectionMapper mapper;

    @Override
    public int save(ExamProblemCollection record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamProblemCollection record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamProblemCollection getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamProblemCollection> listForPage(int pageCurrent, int pageSize, ExamProblemCollectionExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ExamProblemCollection getByUserNoAndCollectionIdAndCollectionType(Long userNo, Long collectionId, Integer collectionType) {
        ExamProblemCollectionExample example = new ExamProblemCollectionExample();
        ExamProblemCollectionExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andCollectionIdEqualTo(collectionId);
        c.andCollectionTypeEqualTo(collectionType);
        List<ExamProblemCollection> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public Integer deleteByUserNoAndCollectionId(Long userNo, Long collectionId) {
        ExamProblemCollectionExample example = new ExamProblemCollectionExample();
        ExamProblemCollectionExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andCollectionIdEqualTo(collectionId);
        return this.mapper.deleteByExample(example);
    }

}
