package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.roncoo.education.user.feign.vo.UserExtVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 班级信息
 * </p>
 *
 * @author wujing
 * @date 2020-06-01
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeInfoPageDTO", description = "班级信息")
public class AuthGradeInfoPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名名称")
    private String lecturerName;

    @ApiModelProperty(value = "班级名称")
    private String gradeName;

    @ApiModelProperty(value = "班级简介")
    private String gradeIntro;

    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)")
    private Integer verificationSet;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "管理员人数")
    private Integer adminCount;

    @ApiModelProperty(value = "管理员信息")
    private List<UserExtVO> adminList;

    @ApiModelProperty(value = "学生人数")
    private Integer studentCount;

    @ApiModelProperty(value = "待审核的申请入班的人数")
    private Integer toAuditCount;

    @ApiModelProperty(value = "未考试的试卷数量")
    private Integer undoExamCount;


}
