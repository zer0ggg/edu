package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 考试完成
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamFinishBO", description = "考试完成参数")
public class AuthExamFinishBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "考试记录ID不能为空")
    @ApiModelProperty(value = "考试记录ID", required = true)
    private Long recordId;
}
