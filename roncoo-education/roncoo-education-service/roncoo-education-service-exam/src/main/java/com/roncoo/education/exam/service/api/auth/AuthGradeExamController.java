package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeExamPageDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthGradeExamBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * UserApi接口
 *
 * @author wujing
 * @date 2020-06-10
 */
@Api(tags = "API-AUTH-班级考试管理")
@RestController
@RequestMapping("/exam/auth/grade/exam")
public class AuthGradeExamController {

    @Autowired
    private AuthGradeExamBiz biz;

    @ApiModelProperty(value = "布置考试", notes = "布置考试")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AuthGradeExamSaveBO bo) {
        return biz.save(bo);
    }

    @ApiModelProperty(value = "分页列出--讲师或管理员使用", notes = "分页列出--讲师或管理员使用")
    @PostMapping(value = "/admin/page")
    public Result<Page<AuthGradeExamPageDTO>> listForAdminPage(@RequestBody @Valid AuthGradeExamPageBO bo) {
        return biz.listForAdminPage(bo);
    }

    @ApiModelProperty(value = "开考前:更新", notes = "开考前:更新更新")
    @PostMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid AuthGradeExamEditBO bo) {
        return biz.edit(bo);
    }

    @ApiModelProperty(value = "开考前:删除", notes = "开考前:删除")
    @PostMapping(value = "/del")
    public Result<String> del(@RequestBody @Valid AuthGradeExamDelBO bo) {
        return biz.del(bo);
    }

    @ApiModelProperty(value = "修改补交按钮", notes = "修改补交按钮")
    @PostMapping(value = "/edit/compensate/status")
    public Result<String> editCompensateStatus(@RequestBody @Valid AuthGradeExamEditCompensateStatusBO bo) {
        return biz.editCompensateStatus(bo);
    }

    @ApiModelProperty(value = "根据考场id获得学生id", notes = "开考前:删除")
    @PostMapping(value = "/get/student/ids")
    public Result<List<Long>> del(@RequestBody @Valid AuthGradeExamEditBeforeBO bo) {
        return biz.getSelectedStudentIdsByGradeExamId(bo);
    }

}
