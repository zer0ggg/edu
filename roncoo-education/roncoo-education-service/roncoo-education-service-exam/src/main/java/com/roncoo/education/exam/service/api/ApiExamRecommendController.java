package com.roncoo.education.exam.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.ExamRecommendPageBO;
import com.roncoo.education.exam.common.dto.ExamRecommendDTO;
import com.roncoo.education.exam.service.api.biz.ApiExamRecommendBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷推荐 Api接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@Api(tags = "API-试卷推荐管理")
@RestController
@RequestMapping("/exam/api/exam/recommend")
public class ApiExamRecommendController {

    @Autowired
    private ApiExamRecommendBiz biz;

    @ApiOperation(value = "试卷推荐列表", notes = "试卷推荐列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamRecommendDTO>> list(@RequestBody ExamRecommendPageBO bo) {
        return biz.list(bo);
    }

}
