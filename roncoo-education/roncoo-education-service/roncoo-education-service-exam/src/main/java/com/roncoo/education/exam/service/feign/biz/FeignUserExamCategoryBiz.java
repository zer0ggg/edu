package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamCategoryQO;
import com.roncoo.education.exam.feign.vo.UserExamCategoryVO;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷分类
 *
 * @author wujing
 */
@Component
public class FeignUserExamCategoryBiz extends BaseBiz {

    @Autowired
    private UserExamCategoryDao dao;

	public Page<UserExamCategoryVO> listForPage(UserExamCategoryQO qo) {
	    UserExamCategoryExample example = new UserExamCategoryExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExamCategory> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamCategoryVO.class);
	}

	public int save(UserExamCategoryQO qo) {
		UserExamCategory record = BeanUtil.copyProperties(qo, UserExamCategory.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamCategoryVO getById(Long id) {
		UserExamCategory record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamCategoryVO.class);
	}

	public int updateById(UserExamCategoryQO qo) {
		UserExamCategory record = BeanUtil.copyProperties(qo, UserExamCategory.class);
		return dao.updateById(record);
	}

}
