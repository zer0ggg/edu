package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamFileInfo;
import com.roncoo.education.exam.feign.qo.FileInfoQO;
import com.roncoo.education.exam.feign.vo.FileInfoVO;
import com.roncoo.education.exam.service.feign.biz.FeignFileInfoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件信息
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignFileInfoController extends BaseController implements IFeignExamFileInfo {

    @Autowired
    private FeignFileInfoBiz biz;

	@Override
	public Page<FileInfoVO> listForPage(@RequestBody FileInfoQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody FileInfoQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody FileInfoQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public FileInfoVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
