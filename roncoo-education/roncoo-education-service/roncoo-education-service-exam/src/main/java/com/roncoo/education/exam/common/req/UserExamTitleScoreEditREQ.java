package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户考试标题得分
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExamTitleScoreEditREQ", description="用户考试标题得分修改")
public class UserExamTitleScoreEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "记录ID")
    private Long recordId;

    @ApiModelProperty(value = "标题ID")
    private Long titleId;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅分值")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "人工评阅分值")
    private Integer handworkAuditTotalScore;

    @ApiModelProperty(value = "人工评阅得分")
    private Integer handworkAuditScore;
}
