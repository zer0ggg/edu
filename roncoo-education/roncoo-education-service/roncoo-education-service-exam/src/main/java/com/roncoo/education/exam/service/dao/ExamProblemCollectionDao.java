package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollection;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample;

/**
 * 试卷题目收藏 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamProblemCollectionDao {

    /**
    * 保存试卷题目收藏
    *
    * @param record 试卷题目收藏
    * @return 影响记录数
    */
    int save(ExamProblemCollection record);

    /**
    * 根据ID删除试卷题目收藏
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷题目收藏
    * @return 影响记录数
    */
    int updateById(ExamProblemCollection record);

    /**
    * 根据ID获取试卷题目收藏
    *
    * @param id 主键ID
    * @return 试卷题目收藏
    */
    ExamProblemCollection getById(Long id);

    /**
    * 试卷题目收藏--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamProblemCollection> listForPage(int pageCurrent, int pageSize, ExamProblemCollectionExample example);

    /**
     * 根据用户编号、收藏ID和收藏类型获取收藏记录
     *
     * @param userNo         用户编号
     * @param collectionId   收藏ID
     * @param collectionType 收藏类型
     * @return 收藏记录
     */
    ExamProblemCollection getByUserNoAndCollectionIdAndCollectionType(Long userNo, Long collectionId, Integer collectionType);

    /**
     * 根据用户No和收藏ID删除用户收藏信息
     *
     * @param userNo
     * @param collectionId
     * @return
     */
    Integer deleteByUserNoAndCollectionId(Long userNo, Long collectionId);

}
