package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目
 *
 * @author wujing
 */
@Component
public class ApiExamProblemBiz extends BaseBiz {

    @Autowired
    private ExamProblemDao dao;

}
