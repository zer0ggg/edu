package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 修改班级信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeInfoEditREQ", description = "修改班级信息")
public class GradeInfoEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "后台排序", required = true)
    private Integer backstageSort;

    @NotNull(message = "班级名称不能为空")
    @ApiModelProperty(value = "班级名称", required = true)
    private String gradeName;

    @NotNull(message = "班级简介不能为空")
    @ApiModelProperty(value = "班级简介", required = true)
    private String gradeIntro;

    @NotNull(message = "权限设置不能为空")
    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)", required = true)
    private Integer verificationSet;
}
