package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownload;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample;

/**
 * 用户试卷下载 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamDownloadDao {

    /**
    * 保存用户试卷下载
    *
    * @param record 用户试卷下载
    * @return 影响记录数
    */
    int save(UserExamDownload record);

    /**
    * 根据ID删除用户试卷下载
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户试卷下载
    * @return 影响记录数
    */
    int updateById(UserExamDownload record);

    /**
    * 根据ID获取用户试卷下载
    *
    * @param id 主键ID
    * @return 用户试卷下载
    */
    UserExamDownload getById(Long id);

    /**
    * 用户试卷下载--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<UserExamDownload> listForPage(int pageCurrent, int pageSize, UserExamDownloadExample example);

}
