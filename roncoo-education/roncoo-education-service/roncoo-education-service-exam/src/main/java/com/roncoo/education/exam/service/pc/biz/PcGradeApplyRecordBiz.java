package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.GradeApplyRecordPageREQ;
import com.roncoo.education.exam.common.resp.GradeApplyRecordPageRESP;
import com.roncoo.education.exam.common.resp.GradeApplyRecordViewRESP;
import com.roncoo.education.exam.service.dao.GradeApplyRecordDao;
import com.roncoo.education.exam.service.dao.GradeInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample.Criteria;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wujing
 */
@Component
public class PcGradeApplyRecordBiz extends BaseBiz {

    @Autowired
    private GradeApplyRecordDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;

    /**
     * 列表
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeApplyRecordPageRESP>> list(GradeApplyRecordPageREQ req) {
        GradeApplyRecordExample example = new GradeApplyRecordExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(req.getLecturerUserNo())) {
            c.andLecturerUserNoEqualTo(req.getLecturerUserNo());
        }
        if (ObjectUtil.isNotNull(req.getGradeId())) {
            c.andGradeIdEqualTo(req.getGradeId());
        }
        if (ObjectUtil.isNotNull(req.getUserNo())) {
            c.andUserNoEqualTo(req.getUserNo());
        }
        if (StrUtil.isNotBlank(req.getNickname())) {
            c.andNicknameLike(PageUtil.like(req.getNickname()));
        }
        if (ObjectUtil.isNotNull(req.getAuditStatus())) {
            c.andAuditStatusEqualTo(req.getAuditStatus());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeApplyRecord> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeApplyRecordPageRESP> respPage = PageUtil.transform(page, GradeApplyRecordPageRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }

        // 获取班级名称
        for (GradeApplyRecordPageRESP resp : respPage.getList()) {
            GradeInfo gradeInfo = gradeInfoDao.getById(req.getGradeId());
            if (ObjectUtil.isNotNull(gradeInfo)) {
                resp.setGradeName(gradeInfo.getGradeName());
            }
        }
        return Result.success(respPage);
    }

    /**
     * 班级考试查看
     *
     * @param id 主键ID
     * @return 班级考试
     */
    public Result<GradeApplyRecordViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), GradeApplyRecordViewRESP.class));
    }

    /**
     * 删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
