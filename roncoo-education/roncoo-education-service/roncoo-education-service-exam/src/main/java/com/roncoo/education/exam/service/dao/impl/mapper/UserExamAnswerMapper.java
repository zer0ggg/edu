package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamAnswerMapper {
    int countByExample(UserExamAnswerExample example);

    int deleteByExample(UserExamAnswerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExamAnswer record);

    int insertSelective(UserExamAnswer record);

    List<UserExamAnswer> selectByExampleWithBLOBs(UserExamAnswerExample example);

    List<UserExamAnswer> selectByExample(UserExamAnswerExample example);

    UserExamAnswer selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExamAnswer record, @Param("example") UserExamAnswerExample example);

    int updateByExampleWithBLOBs(@Param("record") UserExamAnswer record, @Param("example") UserExamAnswerExample example);

    int updateByExample(@Param("record") UserExamAnswer record, @Param("example") UserExamAnswerExample example);

    int updateByPrimaryKeySelective(UserExamAnswer record);

    int updateByPrimaryKeyWithBLOBs(UserExamAnswer record);

    int updateByPrimaryKey(UserExamAnswer record);
}