package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamInfoAuditMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAuditExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷信息审核 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamInfoAuditDaoImpl implements ExamInfoAuditDao {

    @Autowired
    private ExamInfoAuditMapper mapper;

    @Override
    public int save(ExamInfoAudit record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamInfoAudit record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamInfoAudit getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamInfoAudit> listForPage(int pageCurrent, int pageSize, ExamInfoAuditExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ExamInfoAudit getByIdAndLecturerUserNo(Long id, Long lecturerUserNo) {
        ExamInfoAuditExample example = new ExamInfoAuditExample();
        ExamInfoAuditExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(id);
        c.andLecturerUserNoEqualTo(lecturerUserNo);
        List<ExamInfoAudit> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }


}
