package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample;

import java.util.List;

/**
 * 用户考试答案 服务类
 *
 * @author LHR
 * @date 2020-10-10
 */
public interface PracticeUserAnswerDao {

    /**
    * 保存用户考试答案
    *
    * @param record 用户考试答案
    * @return 影响记录数
    */
    int save(PracticeUserAnswer record);

    /**
    * 根据ID删除用户考试答案
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 用户考试答案
    * @return 影响记录数
    */
    int updateById(PracticeUserAnswer record);

    /**
    * 根据ID获取用户考试答案
    *
    * @param id 主键ID
    * @return 用户考试答案
    */
    PracticeUserAnswer getById(Long id);

    /**
    * 用户考试答案--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<PracticeUserAnswer> listForPage(int pageCurrent, int pageSize, PracticeUserAnswerExample example);

    /**
     * 根据用户编号、练习id获取用户答案
     * @param userNo
     * @param practiceId
     * @return
     */
    List<PracticeUserAnswer> listByUserNoAndPracticeId(Long userNo, Long practiceId);

    /**
     * 根据用户编号、练习id删除用户答案
     * @param userNo
     * @param practiceId
     * @return
     */
    Integer deleteByUserNoAndPracticeId(Long userNo, Long practiceId);

    /**
     * 根据用户编号、练习id、试题id获取答案信息
     * @param userNo
     * @param practiceId
     * @param problemId
     * @return
     */
    PracticeUserAnswer getByUserNoAndPracticeIdAndProblemId(Long userNo, Long practiceId, Long problemId);
}
