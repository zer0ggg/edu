package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamProblemOptionEditREQ;
import com.roncoo.education.exam.common.req.ExamProblemOptionListREQ;
import com.roncoo.education.exam.common.req.ExamProblemOptionSaveREQ;
import com.roncoo.education.exam.common.resp.ExamProblemOptionListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemOptionViewRESP;
import com.roncoo.education.exam.service.dao.ExamProblemOptionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目选项
 *
 * @author wujing
 */
@Component
public class PcExamProblemOptionBiz extends BaseBiz {

	@Autowired
	private ExamProblemOptionDao dao;

	/**
	 * 试卷题目选项列表
	 *
	 * @param examProblemOptionListREQ 试卷题目选项分页查询参数
	 * @return 试卷题目选项分页查询结果
	 */
	public Result<Page<ExamProblemOptionListRESP>> list(ExamProblemOptionListREQ examProblemOptionListREQ) {
		ExamProblemOptionExample example = new ExamProblemOptionExample();
		Criteria c = example.createCriteria();
		Page<ExamProblemOption> page = dao.listForPage(examProblemOptionListREQ.getPageCurrent(), examProblemOptionListREQ.getPageSize(), example);
		Page<ExamProblemOptionListRESP> respPage = PageUtil.transform(page, ExamProblemOptionListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷题目选项添加
	 *
	 * @param examProblemOptionSaveREQ 试卷题目选项
	 * @return 添加结果
	 */
	public Result<String> save(ExamProblemOptionSaveREQ examProblemOptionSaveREQ) {
		ExamProblemOption record = BeanUtil.copyProperties(examProblemOptionSaveREQ, ExamProblemOption.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷题目选项查看
	 *
	 * @param id 主键ID
	 * @return 试卷题目选项
	 */
	public Result<ExamProblemOptionViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamProblemOptionViewRESP.class));
	}

	/**
	 * 试卷题目选项修改
	 *
	 * @param examProblemOptionEditREQ 试卷题目选项修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamProblemOptionEditREQ examProblemOptionEditREQ) {
		ExamProblemOption record = BeanUtil.copyProperties(examProblemOptionEditREQ, ExamProblemOption.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷题目选项删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
