package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamRecordEditREQ;
import com.roncoo.education.exam.common.req.UserExamRecordListREQ;
import com.roncoo.education.exam.common.req.UserExamRecordSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamRecordListRESP;
import com.roncoo.education.exam.common.resp.UserExamRecordViewRESP;
import com.roncoo.education.exam.service.dao.UserExamRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试记录
 *
 * @author wujing
 */
@Component
public class PcUserExamRecordBiz extends BaseBiz {

	@Autowired
	private UserExamRecordDao dao;

	/**
	 * 用户考试记录列表
	 *
	 * @param userExamRecordListREQ 用户考试记录分页查询参数
	 * @return 用户考试记录分页查询结果
	 */
	public Result<Page<UserExamRecordListRESP>> list(UserExamRecordListREQ userExamRecordListREQ) {
		UserExamRecordExample example = new UserExamRecordExample();
		Criteria c = example.createCriteria();
		Page<UserExamRecord> page = dao.listForPage(userExamRecordListREQ.getPageCurrent(), userExamRecordListREQ.getPageSize(), example);
		Page<UserExamRecordListRESP> respPage = PageUtil.transform(page, UserExamRecordListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户考试记录添加
	 *
	 * @param userExamRecordSaveREQ 用户考试记录
	 * @return 添加结果
	 */
	public Result<String> save(UserExamRecordSaveREQ userExamRecordSaveREQ) {
		UserExamRecord record = BeanUtil.copyProperties(userExamRecordSaveREQ, UserExamRecord.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户考试记录查看
	 *
	 * @param id 主键ID
	 * @return 用户考试记录
	 */
	public Result<UserExamRecordViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamRecordViewRESP.class));
	}

	/**
	 * 用户考试记录修改
	 *
	 * @param userExamRecordEditREQ 用户考试记录修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamRecordEditREQ userExamRecordEditREQ) {
		UserExamRecord record = BeanUtil.copyProperties(userExamRecordEditREQ, UserExamRecord.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户考试记录删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
