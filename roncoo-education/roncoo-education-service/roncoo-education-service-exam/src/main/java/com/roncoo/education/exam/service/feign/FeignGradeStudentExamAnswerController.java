package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeStudentExamAnswer;
import com.roncoo.education.exam.feign.qo.GradeStudentExamAnswerQO;
import com.roncoo.education.exam.feign.vo.GradeStudentExamAnswerVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeStudentExamAnswerBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeStudentExamAnswerController extends BaseController implements IFeignGradeStudentExamAnswer{

    @Autowired
    private FeignGradeStudentExamAnswerBiz biz;

	@Override
	public Page<GradeStudentExamAnswerVO> listForPage(@RequestBody GradeStudentExamAnswerQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeStudentExamAnswerQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeStudentExamAnswerQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeStudentExamAnswerVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
