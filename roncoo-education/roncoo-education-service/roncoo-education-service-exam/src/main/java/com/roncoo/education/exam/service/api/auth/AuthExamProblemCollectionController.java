package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionDeleteBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionPageBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemCollectionPageDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthExamProblemCollectionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 试卷题目收藏 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-试卷试题收藏管理")
@RestController
@RequestMapping("/exam/auth/exam/problem/collection")
public class AuthExamProblemCollectionController {

    @Autowired
    private AuthExamProblemCollectionBiz biz;

    @ApiOperation(value = "试卷收藏分页列表接口", notes = "试卷收藏分页列表接口")
    @PostMapping(value = "/list")
    public Result<Page<AuthExamProblemCollectionPageDTO>> page(@RequestBody @Valid AuthExamProblemCollectionPageBO bo) {
        return biz.list(bo);
    }

    @ApiOperation(value = "添加收藏", notes = "添加收藏")
    @PostMapping(value = "/collect")
    public Result<String> collect(@RequestBody @Valid AuthExamProblemCollectionBO bo) {
        return biz.collect(bo);
    }

    @ApiOperation(value = "取消收藏", notes = "取消收藏")
    @PostMapping(value = "/delete")
    public Result<String> delete(@RequestBody @Valid AuthExamProblemCollectionDeleteBO bo) {
        return biz.delete(bo);
    }

}
