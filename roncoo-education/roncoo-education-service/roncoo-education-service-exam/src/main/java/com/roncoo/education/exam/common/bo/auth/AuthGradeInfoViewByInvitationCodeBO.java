package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级信息分页查询对象--查询讲师创建的班级
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeInfoViewByInvitationCodeBO", description = "班级信息分页查询对象--查询班级")
public class AuthGradeInfoViewByInvitationCodeBO extends PageParam implements Serializable {

    private static final long serialVersionUID = -8620430812316792354L;

    @NotEmpty(message = "邀请码不能为空")
    @ApiModelProperty(value = "邀请码",required = true)
    private String invitationCode;
}
