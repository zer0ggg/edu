package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.PracticeProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.PracticeProblemRefMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 题目练习关联 服务实现类
 *
 * @author LHR
 * @date 2020-10-10
 */
@Repository
public class PracticeProblemRefDaoImpl implements PracticeProblemRefDao {

    @Autowired
    private PracticeProblemRefMapper mapper;

    @Override
    public int save(PracticeProblemRef record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(PracticeProblemRef record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public PracticeProblemRef getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<PracticeProblemRef> listForPage(int pageCurrent, int pageSize, PracticeProblemRefExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<PracticeProblemRef> listByPracticeId(Long practiceId) {
        PracticeProblemRefExample example= new PracticeProblemRefExample();
        PracticeProblemRefExample.Criteria c = example.createCriteria();
        c.andPracticeIdEqualTo(practiceId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public Integer deleteByUserNoAndPracticeId(Long userNo, Long practiceId) {
        PracticeProblemRefExample example= new PracticeProblemRefExample();
        PracticeProblemRefExample.Criteria c = example.createCriteria();
        c.andPracticeIdEqualTo(practiceId);
        c.andUserNoEqualTo(userNo);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public PracticeProblemRef getByUserNoAndPracticeIdAndProblemId(Long userNo, Long practiceId, Long problemId) {
        PracticeProblemRefExample example= new PracticeProblemRefExample();
        PracticeProblemRefExample.Criteria c = example.createCriteria();
        c.andPracticeIdEqualTo(practiceId);
        c.andUserNoEqualTo(userNo);
        c.andProblemIdEqualTo(problemId);
        List<PracticeProblemRef> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

}
