package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.PracticeProblemRefDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 题目练习关联
 *
 * @author LHR
 */
@Component
public class AuthPracticeProblemRefBiz extends BaseBiz {

    @Autowired
    private PracticeProblemRefDao dao;

}
