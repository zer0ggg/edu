package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamInfoAudit;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditLecturerViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditProblemViewQO;
import com.roncoo.education.exam.feign.qo.ExamInfoAuditQO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditProblemViewVO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditVO;
import com.roncoo.education.exam.feign.vo.ExamInfoAuditViewVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamInfoAuditBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷信息审核
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamInfoAuditController extends BaseController implements IFeignExamInfoAudit {

    @Autowired
    private FeignExamInfoAuditBiz biz;

    @Override
    public Page<ExamInfoAuditVO> listForPage(@RequestBody ExamInfoAuditQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody ExamInfoAuditQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody ExamInfoAuditQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public ExamInfoAuditVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

    @Override
    public ExamInfoAuditVO getByIdAndLecturerUserNo(@RequestBody ExamInfoAuditLecturerViewQO qo) {
        return biz.getByIdAndLecturerUserNo(qo);
    }

    @Override
    public ExamInfoAuditProblemViewVO getExamProblem(@RequestBody ExamInfoAuditProblemViewQO qo) {
        return biz.getExamProblem(qo);
    }

    @Override
    public ExamInfoAuditViewVO getExamInfoById(@PathVariable(value = "id") Long id) {
        return biz.getExamInfoById(id);
    }
}
