package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExamRecord;
import com.roncoo.education.exam.feign.qo.UserExamRecordQO;
import com.roncoo.education.exam.feign.vo.UserExamRecordVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamRecordBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户考试记录
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamRecordController extends BaseController implements IFeignUserExamRecord{

    @Autowired
    private FeignUserExamRecordBiz biz;

	@Override
	public Page<UserExamRecordVO> listForPage(@RequestBody UserExamRecordQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamRecordQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamRecordQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamRecordVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
