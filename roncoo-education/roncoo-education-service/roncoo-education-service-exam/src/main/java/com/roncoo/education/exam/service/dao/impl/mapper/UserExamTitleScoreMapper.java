package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamTitleScoreMapper {
    int countByExample(UserExamTitleScoreExample example);

    int deleteByExample(UserExamTitleScoreExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExamTitleScore record);

    int insertSelective(UserExamTitleScore record);

    List<UserExamTitleScore> selectByExample(UserExamTitleScoreExample example);

    UserExamTitleScore selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExamTitleScore record, @Param("example") UserExamTitleScoreExample example);

    int updateByExample(@Param("record") UserExamTitleScore record, @Param("example") UserExamTitleScoreExample example);

    int updateByPrimaryKeySelective(UserExamTitleScore record);

    int updateByPrimaryKey(UserExamTitleScore record);
}