package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.exam.common.bo.ExamRecommendPageBO;
import com.roncoo.education.exam.common.dto.ExamRecommendDTO;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.ExamRecommendDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷推荐
 *
 * @author wujing
 */
@Component
public class ApiExamRecommendBiz extends BaseBiz {

    @Autowired
    private ExamRecommendDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;

    public Result<Page<ExamRecommendDTO>> list(ExamRecommendPageBO bo) {
        ExamRecommendExample example = new ExamRecommendExample();
        ExamRecommendExample.Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause("sort asc, id desc");
        Page<ExamRecommend> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<ExamRecommendDTO> dtoPage = PageUtil.transform(page, ExamRecommendDTO.class);
        for(ExamRecommendDTO dto:dtoPage.getList()){
            ExamInfo examInfo = examInfoDao.getById(dto.getExamId());
            dto.setStudyCount(examInfo.getStudyCount());
            dto.setGmtModified(examInfo.getGmtModified());
        }
        return Result.success(dtoPage);
    }

}
