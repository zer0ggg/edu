package com.roncoo.education.exam.common;

import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.JSUtil;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * @author wujing
 */
@RestControllerAdvice(basePackages = {"com.roncoo.education.exam.service.api", "com.roncoo.education.exam.service.pc"})
public class HandlerException extends BaseController {

    @ResponseStatus(HttpStatus.OK)
    public Result<String> processBizException(FeignException e) {
        String msg = e.getCause().toString().split("\n|\r\n|\r")[1];
        logger.error(msg);
        @SuppressWarnings("unchecked")
        Map<String, Object> m = JSUtil.parseObject(msg, Map.class);
        if (null != m.get("message")) {
            return Result.error(m.get("message").toString());
        }
        return Result.error("系统错误");
    }

    @ExceptionHandler({HystrixRuntimeException.class})
    @ResponseStatus(HttpStatus.OK)
    public Result<String> processException(HystrixRuntimeException e) {
        String msg = e.getCause().toString().split("\n|\r\n|\r")[1];
        logger.error(msg);
        @SuppressWarnings("unchecked")
        Map<String, Object> m = JSUtil.parseObject(msg, Map.class);
        if (null != m.get("message")) {
            return Result.error(m.get("message").toString());
        }
        return Result.error("系统错误");
    }

    @ExceptionHandler({BaseException.class})
    @ResponseStatus(HttpStatus.OK)
    public Result<String> processException(BaseException e) {
        logger.error("BaseException", e);
        return Result.error(e.getMessage());
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.OK)
    public Result<String> processException(Exception e) {
        logger.error("Exception", e);
        return Result.error("系统错误");
    }

    /**
     * 校验参数错误
     *
     * @param ex 参数校验错误
     * @return 错误返回
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.OK)
    public Result<String> validException(MethodArgumentNotValidException ex) {
        StringBuilder sb = new StringBuilder();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            sb.append(fieldError.getDefaultMessage()).append(",");
            if (StringUtils.hasText(sb)) {
                break;
            }
        }
        sb.substring(0, sb.length() - 1);
        logger.error("校验参数异常:{}", ex.getMessage());
        return Result.error(sb.toString());
    }

}
