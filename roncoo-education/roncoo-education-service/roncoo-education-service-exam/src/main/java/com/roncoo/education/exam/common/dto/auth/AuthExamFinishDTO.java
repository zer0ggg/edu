package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 考试完成
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
public class AuthExamFinishDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "开始考试时间")
    private Date beginTime;

    @ApiModelProperty(value = "答题时间")
    private Integer answerTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "总分", required = true)
    private Integer totalScore;

    @ApiModelProperty(value = "得分", required = true)
    private Integer score;

    @ApiModelProperty(value = "已判总分", required = true)
    private Integer checkTotalScore;

    @ApiModelProperty(value = "未判总分", required = true)
    private Integer notCheckTotalScore;

    @ApiModelProperty(value = "标题得分集合")
    private List<AuthExamFinishTitleScoreDTO> titleScoreList;

}
