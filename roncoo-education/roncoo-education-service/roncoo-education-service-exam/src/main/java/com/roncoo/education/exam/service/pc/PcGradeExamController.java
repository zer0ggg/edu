package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.GradeExamEditREQ;
import com.roncoo.education.exam.common.req.GradeExamPageREQ;
import com.roncoo.education.exam.common.resp.GradeExamPageRESP;
import com.roncoo.education.exam.common.resp.GradeExamViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeExamBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 班级考试 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-班级考试管理")
@RestController
@RequestMapping("/exam/pc/grade/exam")
public class PcGradeExamController {

    @Autowired
    private PcGradeExamBiz biz;

    @ApiOperation(value = "班级考试列表", notes = "班级考试列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeExamPageRESP>> list(@RequestBody GradeExamPageREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级考试查看", notes = "班级考试查看")
    @GetMapping(value = "/view")
    public Result<GradeExamViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "班级考试修改", notes = "班级考试修改")
    @SysLog(value = "班级考试修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody @Valid GradeExamEditREQ req) {
        return biz.edit(req);
    }
}
