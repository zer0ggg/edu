package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.common.GradeExamCommon;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wujing
 */
@Component
public class AuthGradeExamStudentRelationBiz extends BaseBiz {


    //内存中缓存记录行数
    private static final Integer REPORT_CACHENUM = 100;
    private static final Integer NUM_PERPAGE = 1000;
    private static final Integer REPORT_MAX_TOTAL = 100000;
    private static final Integer SHEET_MAX_NUM = 60000;

    @Autowired
    private GradeExamStudentRelationDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeExamDao gradeExamDao;
    @Autowired
    private GradeStudentDao gradeStudentDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeTitleScoreDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeAnswerDao;

    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;
    @Autowired
    private ExamTitleAuditDao titleAuditDao;
    @Autowired
    private ExamTitleProblemRefAuditDao problemRefAuditDao;
    @Autowired
    private ExamProblemDao problemDao;
    @Autowired
    private GradeExamCommon gradeExamCommon;

    @Autowired
    private IFeignUser feignUser;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private ExamTitleDao examTitleDao;


    /**
     * 系统评卷，评阅客观题（单选、多选、判断和空的填空题）
     *
     * @param bo 分页请求参数
     * @return 分页结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthGradeExamStudentRelationScoreDTO> submitAndAudit(AuthGradeExamStudentRelationSysAuditBO bo) {
        //预留提交试卷时间
        Date endTime = new Date();

        //锁表
        GradeExamStudentRelation relation = dao.getByIdForUpdate(bo.getId());
        if (!GradeExamStatusEnum.NOT_OVER.getCode().equals(relation.getExamStatus())) {
            return Result.error("非考试中状态，不可交卷");
        }

        GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        List<AuthGradeTitleScoreDTO> titleScoreDTOList = new ArrayList<>();
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(gradeExam.getExamId());
        List<ExamTitleAudit> titleList = titleAuditDao.listByExamId(examInfoAudit.getId());
        //试卷客观题打分
        int examScore = 0;
        //试卷客观题最大分值
        int examSysMaxScore = 0;
        //试卷主观题最大分值
        int examLecturerMaxScore = 0;
        //标题排序
        int titleSort = 1;
        for (ExamTitleAudit titleAudit : titleList) {
            //标题客观题打分
            int titleScore = 0;
            //标题客观题最大分值
            int titleSysMaxScore = 0;
            //标题主观题最大分值
            int titleLecturerMaxScore = 0;
            //查回 指定标题下的试题关联记录集合，使用该集合中的分数作为试题的分数标准
            List<ExamTitleProblemRefAudit> problemRefAuditList = problemRefAuditDao.listByTitleId(titleAudit.getId());
            Map<Long, Integer> problemScoreMap = problemRefAuditList.stream().collect(Collectors.toMap(ExamTitleProblemRefAudit::getProblemId, ExamTitleProblemRefAudit::getScore));

            // 过滤掉 组合题中的父级试题，防止标题重复加分
            List<Long> problemIds = problemRefAuditList.stream().filter(x -> !ExamProblemTypeEnum.MATERIAL.getCode().equals(x.getProblemType())).map(ExamTitleProblemRefAudit::getProblemId).collect(Collectors.toList());
            //查回  指定标题下的试题集合，该集合的答案
            List<ExamProblem> problemList = problemDao.listInIdList(problemIds);

            List<GradeStudentExamAnswer> userAnswerList = gradeAnswerDao.listByRelationIdAndTitleIdWithBLOBs(relation.getId(), titleAudit.getId());
            for (ExamProblem problem : problemList) {
                Integer score = problemScoreMap.get(problem.getId());
                // 单选题、多选题、判断题
                if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
                    //累加 客观题分数或主观题分数
                    // 分数 使用组卷时的分数
                    titleSysMaxScore += score;
                    // 遍历 学员答案,匹配累加分数
                    if (CollectionUtils.isNotEmpty(userAnswerList)) {
                        for (GradeStudentExamAnswer userAnswer : userAnswerList) {
                            if (userAnswer.getProblemId().longValue() != problem.getId().longValue()) {
                                continue;
                            }
                            // 匹配完成，匹配答案,累加分数
                            if (problem.getProblemAnswer().equals(userAnswer.getUserAnswer())) {
                                userAnswer.setScore(userAnswer.getProblemScore());
                                titleScore += score;
                            }
                            userAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                            userAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());
                            gradeAnswerDao.updateById(userAnswer);

                        }
                    }
                } else {
                    //累加 客观题分数或主观题分数
                    // 分数 使用组卷时的分数
                    titleLecturerMaxScore += score;
                }

            }
            //汇总标题分数，并保存
            GradeStudentExamTitleScore gradeTitleScore = new GradeStudentExamTitleScore();
            gradeTitleScore.setSort(titleSort++);
            gradeTitleScore.setStatusId(StatusIdEnum.YES.getCode());
            gradeTitleScore.setGradeId(gradeExam.getGradeId());
            gradeTitleScore.setRelationId(relation.getId());
            gradeTitleScore.setGradeExamId(relation.getGradeExamId());
            gradeTitleScore.setStudentId(relation.getStudentId());
            gradeTitleScore.setExamId(relation.getExamId());
            gradeTitleScore.setTitleId(titleAudit.getId());
            gradeTitleScore.setScore(titleScore);
            gradeTitleScore.setSysAuditScore(titleScore);
            gradeTitleScore.setSysAuditTotalScore(titleSysMaxScore);
            gradeTitleScore.setLecturerAuditScore(0);
            gradeTitleScore.setLecturerAuditTotalScore(titleLecturerMaxScore);
            gradeTitleScoreDao.save(gradeTitleScore);


            AuthGradeTitleScoreDTO titleScoreDTO = BeanUtil.copyProperties(gradeTitleScore, AuthGradeTitleScoreDTO.class);
            //补充标题名称
            titleScoreDTO.setTitleName(titleAudit.getTitleName());
            titleScoreDTOList.add(titleScoreDTO);

            //累加分数至试卷
            examScore += titleScore;
            examSysMaxScore += titleSysMaxScore;
            examLecturerMaxScore += titleLecturerMaxScore;

        }
        // 判断  所有用户所有答案的评阅状态，找出未评阅的答案
        boolean allFinish = false;
        List<GradeStudentExamAnswer> waitAnswerList = gradeAnswerDao.listByRelationIdAndAnswerStatus(bo.getId(), ExamAnswerStatusEnum.WAIT.getCode());
        if (CollectionUtil.isEmpty(waitAnswerList)) {
            allFinish = true;
        }
        relation.setScore(examScore);
        relation.setSysAuditScore(examScore);
        relation.setSysAuditTotalScore(examSysMaxScore);
        relation.setLecturerAuditTotalScore(examLecturerMaxScore);
        relation.setEndAnswerTime(endTime);
        relation.setAuditUserNo(ThreadContext.userNo());
        relation.setExamStatus(GradeExamStatusEnum.FINISH.getCode());
        // 所有题目已经评阅完成，则考试评阅状态设为评阅完成，若存在未评阅完的试题，则设为系统评阅
        relation.setAuditStatus(allFinish ? ExamAuditStatusEnum.COMPLETE_AUDIT.getCode() : ExamAuditStatusEnum.SYS_AUDIT.getCode());
        dao.updateById(relation);

        //完善返回的分数
        AuthGradeExamStudentRelationScoreDTO scoreDTO = BeanUtil.copyProperties(relation, AuthGradeExamStudentRelationScoreDTO.class);
        scoreDTO.setAuditUserNo(ThreadContext.userNo());
        scoreDTO.setTitleScoreDTOS(titleScoreDTOList);
        scoreDTO.setExamName(examInfoAudit.getExamName());
        scoreDTO.setAnswerShow(gradeExam.getAnswerShow());
        scoreDTO.setAnswerShowTime(gradeExam.getAnswerShowTime());
        //计算答卷时长
        if (relation.getEndAnswerTime() != null && relation.getBeginAnswerTime() != null) {
            int duration = (int) DateUtil.between(relation.getBeginAnswerTime(), relation.getEndAnswerTime(), DateUnit.MINUTE, false);
            scoreDTO.setDuration(duration);
        }

        return Result.success(scoreDTO);
    }

    /**
     * 考完试，预览分数（试卷和标题分数）
     *
     * @param bo 分数预览请求参数
     * @return 分数预览结果
     */
    public Result<AuthGradeExamStudentRelationScoreDTO> viewScore(AuthGradeExamStudentRelationSysAuditBO bo) {
        //检验
        GradeExamStudentRelation relation = dao.getById(bo.getId());
        if (ObjectUtil.isNull(relation)) {
            return Result.error("考试不存在");
        }
        GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        // 拷贝信息
        AuthGradeExamStudentRelationScoreDTO scoreDTO = BeanUtil.copyProperties(relation, AuthGradeExamStudentRelationScoreDTO.class);
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(gradeExam.getExamId());
        //填充其余信息
        scoreDTO.setExamName(examInfoAudit.getExamName());
        scoreDTO.setAnswerShow(gradeExam.getAnswerShow());
        scoreDTO.setAnswerShowTime(gradeExam.getAnswerShowTime());

        List<GradeStudentExamTitleScore> titleScores = gradeTitleScoreDao.listByRelationId(bo.getId());
        if (CollectionUtils.isEmpty(titleScores)) {
            return Result.success(scoreDTO);
        }
        //填充 标题名称
        scoreDTO.setTitleScoreDTOS(BeanUtil.copyProperties(titleScores, AuthGradeTitleScoreDTO.class));
        Map<Long, String> titleMap = titleAuditDao.listByExamId(relation.getExamId()).stream().collect(Collectors.toMap(ExamTitleAudit::getId, ExamTitleAudit::getTitleName));
        for (AuthGradeTitleScoreDTO titleScore : scoreDTO.getTitleScoreDTOS()) {
            titleScore.setTitleName(titleMap.get(titleScore.getTitleId()));
        }

        //计算答卷时长
        if (relation.getEndAnswerTime() != null && relation.getBeginAnswerTime() != null) {
            int duration = (int) DateUtil.between(relation.getBeginAnswerTime(), relation.getEndAnswerTime(), DateUnit.MINUTE, false);
            scoreDTO.setDuration(duration);
        }
        return Result.success(scoreDTO);

    }

    /**
     * 分页查询
     *
     * @param bo 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthGradeExamStudentRelationPageDTO>> listForPage(AuthGradeExamStudentRelationPageBO bo) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        GradeExamStudentRelationExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        if (!ObjectUtil.isNull(bo.getGradeId())) {
            c.andGradeIdEqualTo(bo.getGradeId());
        }
        if (StrUtil.isNotBlank(bo.getGradeExamName())) {
            c.andGradeExamNameLike(PageUtil.like(bo.getGradeExamName()));
        }
        if (bo.getBeginExamTime() != null) {
            c.andBeginExamTimeGreaterThanOrEqualTo(bo.getBeginExamTime());
        }
        if (bo.getEndExamTime() != null) {
            c.andBeginExamTimeLessThanOrEqualTo(bo.getEndExamTime());
        }
        example.setOrderByClause("sort asc, id desc");

        Page<GradeExamStudentRelation> relationPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        if (ObjectUtil.isNull(relationPage)) {
            return Result.success(new Page<>());
        }

        Page<AuthGradeExamStudentRelationPageDTO> resultPage = PageUtil.transform(relationPage, AuthGradeExamStudentRelationPageDTO.class);
        if (CollectionUtil.isEmpty(relationPage.getList())) {
            return Result.success(resultPage);
        }

        List<AuthGradeExamStudentRelationPageDTO> resultList = new ArrayList<>();

        Map<Long, LecturerVO> lecturerMap = new HashMap<>();
        Map<Long, ExamCategory> subjectMap = new HashMap<>();
        Map<Long, ExamInfoAudit> examInfoAuditMap = new HashMap<>();
        Date currentTime = new Date();
        for (GradeExamStudentRelation relation : relationPage.getList()) {
            //使用试卷信息 覆盖 记录信息，但不覆盖 记录id
            GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
            AuthGradeExamStudentRelationPageDTO dto = BeanUtil.copyProperties(gradeExam, AuthGradeExamStudentRelationPageDTO.class);
            dto.setLecturerUserNo(ThreadContext.userNo());
            dto.setExamStatus(relation.getExamStatus());
            dto.setBeginExamTime(gradeExam.getBeginTime());
            dto.setEndExamTime(gradeExam.getEndTime());

            dto.setId(relation.getId());

            //保留考试完成时间
            dto.setBeginAnswerTime(relation.getBeginAnswerTime());
            dto.setEndAnswerTime(relation.getEndAnswerTime());
            dto.setCutoffEndTime(relation.getCutoffEndTime());

            // 判断学生考试状态
            if (currentTime.before(dto.getEndExamTime())) {
                dto.setStudentExamShowStatus(GradeStudentExamShowStatus.NORMAL.getCode());
            } else if (GradeExamCompensateStatusEnum.OPEN.getCode().equals(dto.getCompensateStatus())) {
                dto.setStudentExamShowStatus(GradeStudentExamShowStatus.TIMEOUT_COMPENSATE.getCode());
            } else {
                dto.setStudentExamShowStatus(GradeStudentExamShowStatus.TIMEOUT_NO_COMPENSATE.getCode());
            }

            //填充讲师
            LecturerVO lecturerVO = lecturerMap.get(gradeExam.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturerVO)) {
                lecturerVO = feignLecturer.getByLecturerUserNo(gradeExam.getLecturerUserNo());
            }
            if (ObjectUtil.isNotNull(lecturerVO)) {
                lecturerMap.put(lecturerVO.getLecturerUserNo(), lecturerVO);
                dto.setLecturerUserName(lecturerVO.getLecturerName());
            }

            //科目名称 支持三层结构的科目
            setStudentRelationPageCategory(dto, subjectMap);

            //试卷名称
            ExamInfoAudit examInfoAudit = examInfoAuditMap.get(gradeExam.getExamId());
            if (examInfoAudit == null) {
                examInfoAudit = examInfoAuditDao.getById(gradeExam.getExamId());
            }
            if (ObjectUtil.isNotNull(examInfoAudit)) {
                dto.setExamName(examInfoAudit.getExamName());
                examInfoAuditMap.put(examInfoAudit.getId(), examInfoAudit);
            }

            //年份
            ExamCategory yearCategory = examCategoryDao.getById(examInfoAudit.getYearId());
            if (ObjectUtil.isNotNull(yearCategory)) {
                dto.setYearName(yearCategory.getCategoryName());
            }
            // 获取总分数
            if (ExamStatusEnum.COMPLETE.getCode().equals(relation.getExamStatus())) {
                List<GradeStudentExamTitleScore> listGradeStudentExamTitleScore = gradeStudentExamTitleScoreDao.getByGradeExamIdAndStudentId(relation.getGradeExamId(), relation.getStudentId());
                // 初始化总得分
                int studentScore = 0;
                for (GradeStudentExamTitleScore gradeStudentExamTitleScore : listGradeStudentExamTitleScore) {
                    studentScore = studentScore + gradeStudentExamTitleScore.getScore();
                }
                //总分数
                dto.setScore(studentScore);
            }


            resultList.add(dto);
        }
        resultPage.setList(resultList);
        return Result.success(resultPage);

    }

    /**
     * 获取班级考试详情
     *
     * @param bo 获取参数
     * @return 班级考试详情
     */
    public Result<AuthGradeExamStudentRelationViewDTO> view(AuthGradeExamStudentRelationViewBO bo) {
        GradeExamStudentRelation gradeExamStudentRelation = dao.getById(bo.getId());
        if (gradeExamStudentRelation == null) {
            return Result.error("班级考试不存在");
        }
        if (!gradeExamStudentRelation.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有查看该班级考试权限");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeExamStudentRelation.getStatusId())) {
            return Result.error("当前班级考试不可用");
        }

        GradeExam gradeExam = gradeExamDao.getById(gradeExamStudentRelation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        AuthGradeExamStudentRelationViewDTO viewDTO = BeanUtil.copyProperties(gradeExam, AuthGradeExamStudentRelationViewDTO.class);
        viewDTO.setExamStatus(gradeExamStudentRelation.getExamStatus());
        viewDTO.setLecturerUserNo(ThreadContext.userNo());

        //保留 记录id
        viewDTO.setId(gradeExamStudentRelation.getId());

        // 讲师名称
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(gradeExam.getLecturerUserNo());
        if (ObjectUtil.isNotNull(lecturerVO)) {
            viewDTO.setLecturerUserName(lecturerVO.getLecturerName());
        }

        //科目名称 支持三层结构的科目
        //当前层
        ExamCategory subjectCategory = examCategoryDao.getById(gradeExam.getSubjectId());
        if (ObjectUtil.isNotNull(subjectCategory)) {
            viewDTO.setSubjectName(subjectCategory.getCategoryName());
            //上一层
            if (subjectCategory.getParentId() != 0) {
                ExamCategory parentCategory = examCategoryDao.getById(gradeExam.getSubjectId());
                if (ObjectUtil.isNotNull(parentCategory)) {
                    viewDTO.setParentSubjectId(parentCategory.getId());
                    viewDTO.setParentSubjectName(parentCategory.getCategoryName());

                    // 上上层
                    if (parentCategory.getParentId() != 0) {
                        ExamCategory grandParentCategory = examCategoryDao.getById(parentCategory.getParentId());
                        if (ObjectUtil.isNotNull(grandParentCategory)) {
                            viewDTO.setGrantSubjectId(grandParentCategory.getId());
                            viewDTO.setGrantSubjectName(grandParentCategory.getCategoryName());
                        }
                    }
                }

            }
        }

        //试卷名称
        ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(gradeExam.getExamId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("班级考试，试卷不存在");
        }
        viewDTO.setExamName(examInfoAudit.getExamName());

        //年份
        ExamCategory year = examCategoryDao.getById(examInfoAudit.getYearId());
        if (ObjectUtil.isNotNull(year)) {
            viewDTO.setYearName(year.getCategoryName());
        }

        //总分
        viewDTO.setTotalScore(examInfoAudit.getTotalScore());

        // 设置为已查看
        if (!GradeViewStatusEnum.YES.getCode().equals(gradeExamStudentRelation.getViewStatus())) {
            GradeExamStudentRelation updateRelation = new GradeExamStudentRelation();
            updateRelation.setId(gradeExamStudentRelation.getId());
            updateRelation.setViewStatus(GradeViewStatusEnum.YES.getCode());
            dao.updateById(updateRelation);
        }
        return Result.success(viewDTO);
    }

    /**
     * 继续考试
     *
     * @param bo 班级考试--继续考试参数
     * @return 考试内容
     */
    public Result<AuthGradeExamAndAnswerDTO> examContinue(AuthGradeExamStudentRelationContinueBO bo) {
        // 判断考试记录是否存在
        GradeExamStudentRelation relation = dao.getById(bo.getId());
        if (ObjectUtil.isNull(relation)) {
            return Result.error("考试不存在");
        }
        if (!relation.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("学员该考试不存在");
        }
        if (!new Date().before(relation.getCutoffEndTime())) {
            return Result.error("考试已结束，不能继续考试");
        }

        // 获取班级考试
        GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }

        // 班级考试
        return Result.success(gradeExamCommon.getGradeExamInfoAudit(relation.getExamId(), false, true, relation));
    }

    /**
     * 开始考试
     *
     * @param bo 班级考试
     * @return 考试内容
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthGradeExamAndAnswerDTO> examBegin(AuthGradeExamStudentRelationContinueBO bo) {
        // 判断考试记录是否存在
        GradeExamStudentRelation relation = dao.getById(bo.getId());
        if (ObjectUtil.isNull(relation)) {
            return Result.error("考试不存在");
        }
        if (!relation.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("学员该考试不存在");
        }
        if (!GradeExamStatusEnum.WAIT.getCode().equals(relation.getExamStatus())) {
            return Result.error("考试已开始或结束，不能开始考试");
        }

        Date beginAnswerTime = new Date();
        if (beginAnswerTime.before(relation.getBeginExamTime())) {
            return Result.error("考试未开始，不能考试");
        }

        GradeExamStudentRelation updateRelation = new GradeExamStudentRelation();
        updateRelation.setId(relation.getId());
        if (beginAnswerTime.before(relation.getEndExamTime())) {
            // 在考试结束之前
            updateRelation.setCutoffEndTime(relation.getEndExamTime());
        } else {
            // 已经在达到或超过考试截止时间
            GradeExam gradeExam = gradeExamDao.getById(relation.getGradeExamId());
            if (!GradeExamCompensateStatusEnum.OPEN.getCode().equals(gradeExam.getCompensateStatus())) {
                return Result.error("考试已结束，不能参与考试");
            }

            // 计算答卷时长
            long answerTime = DateUtil.between(relation.getBeginExamTime(), relation.getEndExamTime(), DateUnit.MS, false);
            Date cutoffEndTime = DateUtil.offsetMillisecond(beginAnswerTime, (int) answerTime);
            updateRelation.setCutoffEndTime(cutoffEndTime);
        }

        updateRelation.setBeginAnswerTime(beginAnswerTime);
        updateRelation.setExamStatus(GradeExamStatusEnum.NOT_OVER.getCode());
        dao.updateById(updateRelation);

        // 班级考试
        return Result.success(gradeExamCommon.getGradeExamInfoAudit(relation.getExamId(), false, false, relation));
    }

    /**
     * 列出提示
     *
     * @param bo 列出提示参数
     * @return 列出结果
     */
    public Result<List<AuthGradeExamStudentRelationTipsListDTO>> listTips(AuthGradeExamStudentRelationTipsListBO bo) {
        List<GradeExamStudentRelation> relationList = dao.limitTipsListByUserNo(ThreadContext.userNo(), bo.getNum());
        if (CollectionUtil.isEmpty(relationList)) {
            return Result.success(Collections.emptyList());
        }

        List<AuthGradeExamStudentRelationTipsListDTO> dtoList = BeanUtil.copyProperties(relationList, AuthGradeExamStudentRelationTipsListDTO.class);
        for (AuthGradeExamStudentRelationTipsListDTO dto : dtoList) {
            // 设置班级名称
            GradeInfo gradeInfo = gradeInfoDao.getById(dto.getGradeId());
            if (ObjectUtil.isNotNull(gradeInfo)) {
                dto.setGradeName(gradeInfo.getGradeName());
            }
            dto.setLecturerUserNo(gradeInfo.getLecturerUserNo());

            // 设置讲师名称
            LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturerVO)) {
                dto.setLecturerName(lecturerVO.getLecturerName());
            }

            // 设置班级考试名称
            GradeExam gradeExam = gradeExamDao.getById(dto.getGradeExamId());
            if (ObjectUtil.isNotNull(gradeExam)) {
                dto.setGradeExamName(gradeExam.getGradeExamName());
            }
        }

        return Result.success(dtoList);
    }

    /**
     * 申请记录忽略
     *
     * @param bo 忽略请求对象
     * @return 处理结果
     */
    public Result<String> examViewIgnore(AuthGradeExamStudentRelationTipsIgnoreBO bo) {
        GradeExamStudentRelation gradeExamStudentRelation = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeExamStudentRelation)) {
            return Result.error("考试不存在");
        }
        if (!gradeExamStudentRelation.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有操作权限");
        }

        //判断班级，忽略与班级状态无关
        // 判断用户身份
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("当前用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("状态异常，不能操作");
        }

        // 只有未查看才能忽略，否则直接跳过
        if (GradeViewStatusEnum.NO.getCode().equals(gradeExamStudentRelation.getViewStatus())) {
            GradeExamStudentRelation updateRelation = new GradeExamStudentRelation();
            updateRelation.setId(gradeExamStudentRelation.getId());
            updateRelation.setViewStatus(GradeViewStatusEnum.IGNORE.getCode());
            if (dao.updateById(updateRelation) > 0) {
                return Result.success("处理成功");
            }
            return Result.error("处理失败");
        }

        return Result.success("处理成功");
    }

    /**
     * 列出参加班级考试的所有学生试卷
     *
     * @param bo 请求参数
     * @return 学生试卷集合
     */
    public Result<List<AuthGradeExamStudentRelationCompleteGradeExamIdListDTO>> listByGradeExamId(AuthGradeExamStudentRelationCompleteGradeExamIdListBO bo) {
        // 判断用户
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("用户状态不可用");
        }

        // 判断班级考试
        GradeExam gradeExam = gradeExamDao.getById(bo.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        GradeInfo gradeInfo = gradeInfoDao.getById(gradeExam.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("班级状态不可用");
        }

        // 判断用户身份
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，无法查看");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                return Result.error("没有操作权限");
            }
        }

        List<GradeExamStudentRelation> studentRelationList = dao.listCompleteByGradeExamId(gradeExam.getId());
        if (CollectionUtil.isEmpty(studentRelationList)) {
            return Result.success(Collections.emptyList());
        }

        List<AuthGradeExamStudentRelationCompleteGradeExamIdListDTO> dtoList = BeanUtil.copyProperties(studentRelationList, AuthGradeExamStudentRelationCompleteGradeExamIdListDTO.class);
        for (AuthGradeExamStudentRelationCompleteGradeExamIdListDTO dto : dtoList) {
            // 班级名称
            dto.setGradeName(gradeInfo.getGradeName());

            // 获取用户头像
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getUserNo());
            if (ObjectUtil.isNotNull(userExtVO)) {
                dto.setHeadImgUrl(userExtVO.getHeadImgUrl());
            }

            // 学生名称
            GradeStudent gradeStudent = gradeStudentDao.getById(dto.getStudentId());
            if (ObjectUtil.isNotNull(gradeStudent)) {
                dto.setNickname(gradeStudent.getNickname());
            }
        }
        return Result.success(dtoList);
    }

    public Result<String> auditComplete(AuthGradeExamStudentRelationContinueBO bo) {
        // 判断考试是否存在
        GradeExamStudentRelation studentRelation = dao.getById(bo.getId());
        if (ObjectUtil.isNull(studentRelation)) {
            return Result.error("试卷不存在");
        }
        if (ExamAuditStatusEnum.COMPLETE_AUDIT.getCode().equals(studentRelation.getAuditStatus())) {
            return Result.error("试卷已评阅完成，无需重复处理");
        }

        // 判断用户权限
        GradeExam gradeExam = gradeExamDao.getById(studentRelation.getGradeExamId());
        if (ObjectUtil.isNull(gradeExam)) {
            return Result.error("班级考试不存在");
        }
        if (!gradeExam.getLecturerUserNo().equals(ThreadContext.userNo())) {
            GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeExam.getGradeId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，无法查看");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                return Result.error("没有操作权限");
            }
        }

        GradeExamStudentRelation updateRelation = new GradeExamStudentRelation();
        updateRelation.setId(studentRelation.getId());
        updateRelation.setAuditStatus(ExamAuditStatusEnum.COMPLETE_AUDIT.getCode());
        if (dao.updateById(updateRelation) > 0) {
            return Result.success("处理成功");
        }
        return Result.error("处理失败");
    }

    /**
     * 设置分页对象的分类属性
     *
     * @param dto        分页返回对象
     * @param subjectMap 科目缓存对象
     */
    private void setStudentRelationPageCategory(AuthGradeExamStudentRelationPageDTO dto, Map<Long, ExamCategory> subjectMap) {
        //当前层
        ExamCategory subjectCategory = subjectMap.get(dto.getSubjectId());
        if (ObjectUtil.isNull(subjectCategory)) {
            subjectCategory = examCategoryDao.getById(dto.getSubjectId());
        }
        if (ObjectUtil.isNull(subjectCategory)) {
            return;
        }
        dto.setSubjectName(subjectCategory.getCategoryName());
        subjectMap.put(subjectCategory.getId(), subjectCategory);

        // 父级
        if (ObjectUtil.isNull(subjectCategory.getParentId()) || subjectCategory.getParentId() == 0) {
            return;
        }
        ExamCategory parentCategory = subjectMap.get(subjectCategory.getParentId());
        if (ObjectUtil.isNull(parentCategory)) {
            parentCategory = examCategoryDao.getById(subjectCategory.getParentId());
        }
        if (ObjectUtil.isNull(parentCategory)) {
            return;
        }
        dto.setParentSubjectName(parentCategory.getCategoryName());
        dto.setParentSubjectId(parentCategory.getId());
        subjectMap.put(parentCategory.getId(), parentCategory);

        // 祖父级
        if (ObjectUtil.isNull(parentCategory.getParentId()) || parentCategory.getParentId() == 0) {
            return;
        }
        ExamCategory grandParentCategory = subjectMap.get(parentCategory.getParentId());
        if (ObjectUtil.isNull(grandParentCategory)) {
            grandParentCategory = examCategoryDao.getById(parentCategory.getParentId());
        }
        if (ObjectUtil.isNotNull(grandParentCategory)) {
            dto.setGrantSubjectName(grandParentCategory.getCategoryName());
            dto.setGrantSubjectId(grandParentCategory.getId());
            subjectMap.put(grandParentCategory.getId(), grandParentCategory);
        }
    }


    /**
     * @param bo
     * @return 学员成绩 信息
     */
    public Result<Page<AuthGradeExamStudentRelationGradeForPageDTO>> listGradeForPage(AuthGradeExamStudentRelationGradeForPageBO bo) {
        GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
        Criteria c = example.createCriteria();

        if (bo.getGradeId() != null) {
            c.andGradeIdEqualTo(bo.getGradeId());
        }
        if (bo.getGradeExamId() != null) {
            c.andGradeExamIdEqualTo(bo.getGradeExamId());
        }
        if (StrUtil.isNotBlank(bo.getMobile())) {
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setMobile(bo.getMobile());
            UserExtVO userExtVO = feignUserExt.getByMobile(userExtQO);
            if (ObjectUtil.isNotNull(userExtVO)) {
                c.andUserNoEqualTo(userExtVO.getUserNo());
            }
        }
        c.andAuditStatusEqualTo(ExamAuditStatusEnum.COMPLETE_AUDIT.getCode());
        c.andExamStatusEqualTo(GradeExamStatusEnum.FINISH.getCode());
        example.setOrderByClause("sort asc, id desc");
        //判断页面
        Page<GradeExamStudentRelation> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        if (page.getList().isEmpty()) {
            return Result.success(new Page<>());
        }

        List<Long> userNos = page.getList().stream().map(GradeExamStudentRelation::getUserNo).collect(Collectors.toList());
        UserExtQO userExtQO = new UserExtQO();
        userExtQO.setUserNos(userNos);
        List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
        if (CollectionUtils.isEmpty(userExtVOList)) {
            throw new BaseException("用户不存在");
        }
        Map<Long, UserExtVO> userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item));

        List<Long> studentIds = page.getList().stream().map(GradeExamStudentRelation::getStudentId).collect(Collectors.toList());
        //通过班级考试ID获取该学员  在班级的名称
        List<GradeStudent> gradeStudentList = gradeStudentDao.listByIds(studentIds);
        if (CollectionUtils.isEmpty(gradeStudentList)) {
            throw new BaseException("学员不存在!");
        }
        Map<Long, GradeStudent> gradeStudentMap = gradeStudentList.stream().collect(Collectors.toMap(GradeStudent::getId, item -> item));

        List<Long> examIds = page.getList().stream().map(GradeExamStudentRelation::getExamId).collect(Collectors.toList());

        List<ExamInfo> examInfoList = examInfoDao.listByIds(examIds);
        if (CollectionUtils.isEmpty(examInfoList)) {
            throw new BaseException("找不到试卷信息!");
        }
        Map<Long, ExamInfo> examInfoMap = examInfoList.stream().collect(Collectors.toMap(ExamInfo::getId, item -> item));

        Page<AuthGradeExamStudentRelationGradeForPageDTO> agesRelationPageDTO = PageUtil.transform(page, AuthGradeExamStudentRelationGradeForPageDTO.class);
        for (AuthGradeExamStudentRelationGradeForPageDTO dto : agesRelationPageDTO.getList()) {

            if (dto.getStatusId() == 0 || dto.getStatusId() == null) {
                continue;
            } else {
                //获取学生信息-姓名（昵称）
                GradeStudent gradeStudent = gradeStudentMap.get(dto.getStudentId());
                if (ObjectUtil.isNotNull(gradeStudent)) {
                    logger.error("学生信息不存在");
                    dto.setNickname(gradeStudent.getNickname());
                }

                //获取用户信息-身份证号，手机号码
                UserExtVO userExtVO = userExtVOMap.get(gradeStudent.getUserNo());
                if (ObjectUtil.isNotNull(userExtVO)) {
                    dto.setIdCardNo(userExtVO.getIdCardNo());
                    dto.setMobile(userExtVO.getMobile());
                }

                //获取学生用户考试时间
                dto.getBeginExamTime();

                ExamInfo examInfo = examInfoMap.get(dto.getExamId());

                //获取试卷名称
                if (ObjectUtil.isNotNull(examInfo)) {
                    dto.setExamName(examInfo.getExamName());
                    //2、通过试卷获取科目
                    dto.setSubjectId(examInfo.getSubjectId());
                    ExamCategory examCategory = examCategoryDao.getById(examInfo.getSubjectId());
                    if (ObjectUtil.isNotNull(examCategory)) {
                        dto.setSubjectName(examCategory.getCategoryName());
                    }
                }

                //获取用户考试答案-成绩（选择题，判断题，总数）
                //1、根据试卷获取试卷标题
                List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
                if (CollectionUtil.isEmpty(examTitleList)) {
                    logger.error("考试试卷标题不存在");
                    continue;
                }

                //2、通过分类标题获取不同类型题目分数
                List<AuthExamTitleScoreDTO> authExamTitleScoreDTO = ArrayListUtil.copy(examTitleList, AuthExamTitleScoreDTO.class);
                // 初始化总得分
                int studentScore = 0;
                for (AuthExamTitleScoreDTO titleScoreDTO : authExamTitleScoreDTO) {
                    // 根据班级考试id、标题ID获取标题考试成绩
                    GradeStudentExamTitleScore gradeStudentExamTitleScore = gradeStudentExamTitleScoreDao.getByRelationIdAndTitleId(dto.getId(), titleScoreDTO.getId());
                    if (ObjectUtil.isNotNull(gradeStudentExamTitleScore)) {
                        titleScoreDTO.setScore(gradeStudentExamTitleScore.getScore());
                    } else {
                        titleScoreDTO.setScore(0);
                    }
                    // 总得分
                    studentScore = studentScore + titleScoreDTO.getScore();
                }
                dto.setTitlelist(authExamTitleScoreDTO);

                //总分数
                dto.setScore(studentScore);
            }

        }

        return Result.success(agesRelationPageDTO);
    }

    /**
     * @param bo 导出参数
     */
    public void export(HttpServletResponse response, AuthGradeExamStudentRelationGradeForPageBO bo) {
        //条件查询 判断
        if (bo.getGradeExamId() == null) {
            logger.info("未获取班级考试信息");
            return;
        }

        try {
            String fileName = "学员考试信息-" + DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN);
            response.setCharacterEncoding("utf-8");
            String[] student = {"姓名", "身份证号码", "班级名", "科目", "成绩"};    //学员考试成绩
            Integer[] widths = {5, 10, 8, 5, 7};  //导出行数

            int rowIndex = 2; // 一种大题当前sheet行数
            int firstRow = 2; //多种大题合并 起始行
            int lastRow = 2;

            // 创建一个excel文件
            //final SXSSFWorkbook workbook = new SXSSFWorkbook(REPORT_CACHENUM);
            final SXSSFWorkbook workbook = new SXSSFWorkbook();
            //为该工作簿创建一张图纸sheet
            SXSSFSheet sheet = workbook.createSheet("考试成绩");
            //创建循环一行数据
            SXSSFRow row;
            SXSSFRow row4;
            SXSSFRow row5;

            //头部  合并列单元对象
            sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 5));     //加载合并单元格对象
            sheet.addMergedRegion(new CellRangeAddress(2, 2, 4, 5));     //加载合并单元格对象
            CellStyle cellStyle = workbook.createCellStyle();    //创建样式
            Font font = workbook.createFont();             //创建 FONT
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            cellStyle.setAlignment(HorizontalAlignment.CENTER);   //写入样式居中 CENTER:水平对齐方式以多个单元格为中心。
            font.setBold(true);
            cellStyle.setFont(font);   //文字加粗

            //头列 推荐信息
            SXSSFRow row1 = sheet.createRow(0);
            SXSSFCell cell1 = row1.createCell(0);
            cell1.setCellValue("班级考试成绩");
            cell1.setCellStyle(cellStyle);  //文本赋值样式

            //创建sheet第一列
            SXSSFRow row2 = sheet.createRow(2);   //起始行0
            int studentInfoLength = student.length;  //列表题标
            for (int j = 0; j < studentInfoLength; j++) {
                //进行firstCol判断==4是否等于,等于（进行创建合并单元）
                if (j == 4) {
                    SXSSFCell sxssfCell = row2.createCell(j);
                    sxssfCell.setCellValue("成绩");
                    sxssfCell.setCellStyle(cellStyle);
                    sheet.setColumnWidth(j, widths[j] * 512);
                } else {
                    row2.createCell(j).setCellValue(student[j]);  //第一行
                    sheet.setColumnWidth(j, widths[j] * 512);
                }
            }

            // 第一次获取数据
            boolean isGo = true;
            bo.setPageSize(NUM_PERPAGE); // 最多获取条数

            GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
            Criteria c = example.createCriteria();
            c.andGradeExamIdEqualTo(bo.getGradeExamId());
            // c.andAuditStatusEqualTo(ExamAuditStatusEnum.COMPLETE_AUDIT.getCode());
            c.andExamStatusEqualTo(GradeExamStatusEnum.FINISH.getCode());
            example.setOrderByClause("id desc");

            do {
                Page<GradeExamStudentRelation> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
                if (StringUtils.isEmpty(page) || page.getList() == null || page.getList().isEmpty()) {
                    logger.error("没有需要导出的学员考试信息!");
                    sheet.createRow(rowIndex++).createCell(0).setCellValue("没有需要导出的学员考试信息!");
                    writeExcel(response, workbook, fileName);
                    return;
                }
                if (REPORT_MAX_TOTAL.compareTo(page.getTotalCount()) < 0) {
                    logger.error("选中的学员考试信息据大于" + REPORT_MAX_TOTAL + "条");
                    sheet.createRow(rowIndex++).createCell(0).setCellValue("选中的推荐信息据大于" + REPORT_MAX_TOTAL + "条");
                    writeExcel(response, workbook, fileName);
                    return;
                }

                List<GradeExamStudentRelation> resultList = page.getList();
                List<Long> userNos = resultList.stream().map(GradeExamStudentRelation::getUserNo).collect(Collectors.toList());
                UserExtQO userExtQO = new UserExtQO();
                userExtQO.setUserNos(userNos);
                List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
                if (CollectionUtils.isEmpty(userExtVOList)) {
                    throw new BaseException("用户不存在");
                }
                Map<Long, UserExtVO> userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item));

                List<Long> studentIds = resultList.stream().map(GradeExamStudentRelation::getStudentId).collect(Collectors.toList());
                //通过班级考试ID获取该学员  在班级的名称
                List<GradeStudent> gradeStudentList = gradeStudentDao.listByIds(studentIds);
                if (CollectionUtils.isEmpty(gradeStudentList)) {
                    throw new BaseException("学员不存在!");
                }
                Map<Long, GradeStudent> gradeStudentMap = gradeStudentList.stream().collect(Collectors.toMap(GradeStudent::getId, item -> item));

                List<Long> gradeIds = resultList.stream().map(GradeExamStudentRelation::getGradeId).collect(Collectors.toList());
                //通过班级考试id 获取考试学员的班级信息
                List<GradeInfo> gradeInfoList = gradeInfoDao.lsitByIds(gradeIds);
                if (CollectionUtils.isEmpty(gradeInfoList)) {
                    throw new BaseException("班级不存在!");
                }
                Map<Long, GradeInfo> gradeInfoMap = gradeInfoList.stream().collect(Collectors.toMap(GradeInfo::getId, item -> item));

                for (GradeExamStudentRelation gradeExamStudentRelation : resultList) {
                    //创建行
                    row = sheet.createRow(++rowIndex);

                    //通过班级考试id 获取试卷信息
                    ExamInfo examInfo = examInfoDao.getById(gradeExamStudentRelation.getExamId());
                    if (ObjectUtil.isNull(examInfo)) {
                        logger.error("该试卷不存在");
                        continue;
                    }

                    //通过试卷信息获取 科目信息
                    ExamCategory examCategory = examCategoryDao.getById(examInfo.getSubjectId());
//                    if (ObjectUtil.isNull(examCategory)) {
//                        logger.error("该科目的试卷不存在!");
//                        continue;
//                    }

                    List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
                    if (CollectionUtil.isEmpty(examTitleList)) {
                        logger.error("考试试卷标题不存在");
                        continue;
                    }

                    // 计算总得分
                    List<AuthExamTitleScoreDTO> authExamTitleScoreDTO = ArrayListUtil.copy(examTitleList, AuthExamTitleScoreDTO.class);
                    int studentScore = 0;
                    for (AuthExamTitleScoreDTO titleScoreDTO : authExamTitleScoreDTO) {
                        GradeStudentExamTitleScore gradeStudentExamTitleScore = gradeStudentExamTitleScoreDao.getByRelationIdAndTitleId(gradeExamStudentRelation.getId(), titleScoreDTO.getId());
                        if (ObjectUtil.isNotNull(gradeStudentExamTitleScore)) {
                            titleScoreDTO.setScore(gradeStudentExamTitleScore.getScore());
                        } else {
                            titleScoreDTO.setScore(0);
                        }
                        // 总得分
                        studentScore = studentScore + titleScoreDTO.getScore();
                    }

                    // 计算大题总分
                    int titleScore = 0;
                    UserExtVO userExtVO = userExtVOMap.get(gradeExamStudentRelation.getUserNo());
                    GradeStudent gradeStudent = gradeStudentMap.get(gradeExamStudentRelation.getStudentId());
                    GradeInfo gradeInfo = gradeInfoMap.get(gradeExamStudentRelation.getGradeId());

                    if (ObjectUtil.isNotNull(gradeStudent)) {
                        row.createCell(0).setCellValue(gradeStudent.getNickname());
                    }
                    if (ObjectUtil.isNotNull(userExtVO)) {
                        row.createCell(1).setCellValue(userExtVO.getIdCardNo());
                    }
                    if (ObjectUtil.isNotNull(gradeInfo)) {
                        row.createCell(2).setCellValue(gradeInfo.getGradeName());
                    }
                    if (ObjectUtil.isNotNull(examCategory)) {
                        row.createCell(3).setCellValue(examCategory.getCategoryName());
                    }
                    row.createCell(4).setCellValue("总分:" + studentScore);
                    if (titleScore == 0) {
                        titleScore = studentScore;
                    }

                    List<GradeStudentExamTitleScore> gradeStudentExamTitleScoreList = gradeStudentExamTitleScoreDao.getByGradeExamIdAndStudentId(gradeExamStudentRelation.getGradeExamId(), gradeExamStudentRelation.getStudentId());
                    Map<Long, GradeStudentExamTitleScore> maps = gradeStudentExamTitleScoreList.stream().collect(Collectors.toMap(GradeStudentExamTitleScore::getTitleId, item -> item));
                    String title = "";
                    for (ExamTitle examTitle : examTitleList) {
                        GradeStudentExamTitleScore gradeStudentExamTitleScore = maps.get(examTitle.getId());
                        title = title + examTitle.getTitleName() + ":" + gradeStudentExamTitleScore.getScore() + "；";
                        //title = title + "\r\n";
                        logger.warn("firstRow={}, examTitle={}", firstRow, examTitle);
                    }
                    row.createCell(5).setCellValue(title);

             /*       if (examTitleList.size() == 1) {
                        row.createCell(0).setCellValue(gradeStudent.getNickname());  //学员姓名
                        row.createCell(1).setCellValue(userExtVO.getIdCardNo());  //学员身份证（用户身份证）
                        row.createCell(2).setCellValue(gradeInfo.getGradeName());  //班级名
                        row.createCell(3).setCellValue(examCategory.getCategoryName());  //科目
                        row.createCell(4).setCellValue("总分:" + studentScore);  //成绩
                        if (titleScore == 0) {
                            titleScore = studentScore;
                        }
                        row.createCell(5).setCellValue(examTitleDao.getById(examTitleList.get(0).getId()).getTitleName() + ":" + titleScore);
                    } else {
                        firstRow = firstRow + 1;
                        row4 = sheet.createRow(firstRow);
                        if (ObjectUtil.isNotNull(gradeStudent)) {
                            row4.createCell(0).setCellValue(gradeStudent.getNickname());
                        }
                        if (ObjectUtil.isNotNull(userExtVO)) {
                            row4.createCell(1).setCellValue(userExtVO.getIdCardNo());
                        }
                        if (ObjectUtil.isNotNull(gradeInfo)) {
                            row4.createCell(2).setCellValue(gradeInfo.getGradeName());
                        }
                        if (ObjectUtil.isNotNull(examCategory)) {
                            row4.createCell(3).setCellValue(examCategory.getCategoryName());
                        }
                        row4.createCell(4).setCellValue("总分:" + studentScore);

                        logger.warn("row4 = {},{},{},{},{}", row4.getCell(0), row4.getCell(1), row4.getCell(2), row4.getCell(3), row4.getCell(4));

                        lastRow = lastRow + examTitleList.size();  //获取大题名个数累加 合并行
                        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 0, 0));
                        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 1, 1));
                        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 2, 2));
                        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 3, 3));
                        sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, 4, 4));

                        //大题考试得分
                        List<GradeStudentExamTitleScore> gradeStudentExamTitleScoreList = gradeStudentExamTitleScoreDao.getByGradeExamIdAndStudentId(gradeExamStudentRelation.getGradeExamId(), gradeExamStudentRelation.getStudentId());
                        Map<Long, GradeStudentExamTitleScore> maps = gradeStudentExamTitleScoreList.stream().collect(Collectors.toMap(GradeStudentExamTitleScore::getTitleId, item -> item));
                        for (ExamTitle examTitle : examTitleList) {
                            GradeStudentExamTitleScore gradeStudentExamTitleScore = maps.get(examTitle.getId());
                            if (row4.getRowNum() == firstRow && row4.getCell(5) == null) {
                                row4.createCell(5).setCellValue(examTitle.getTitleName() + ":" + (titleScore + gradeStudentExamTitleScore.getScore()));
                            } else {
                                firstRow = firstRow + 1;
                                row5 = sheet.createRow(firstRow);
                                row5.createCell(5).setCellValue(examTitle.getTitleName() + ":" + (titleScore + gradeStudentExamTitleScore.getScore()));
                                logger.warn("row5={}", row5.getCell(5));
                            }
                            logger.warn("firstRow={}, examTitle={}, row5={}", firstRow, examTitle);
                        }
                    }*/
                }


                // 每当行数达到设置的值就刷新数据到硬盘,以清理内存
//                if (rowIndex % REPORT_CACHENUM == 0) {
//                    try {
//                        sheet.flushRows();
//                    } catch (IOException e) {
//                        logger.error("学员考试信息导出,刷新数据到硬盘失败！");
//                    }
//                }

                // 当记录达到每页的sheet记录数大小，创建新的sheet
//                if (rowIndex % SHEET_MAX_NUM == 0) {
//                    logger.info("学员考试信息导出，创建新sheet:{}", workbook.getNumberOfSheets() + 1);
//                    sheet = workbook.createSheet((workbook.getNumberOfSheets() + 1) + "表");
//                    rowIndex = 0;
//                    row2 = sheet.createRow(++rowIndex);
//                    // 创建sheet头部
//                    studentInfoLength = student.length;
//                    for (int j = 0; j < studentInfoLength; j++) {
//                        row2.createCell(j).setCellValue(student[j]);
//                        sheet.setColumnWidth(j, widths[j] * 512);
//                    }
//                }
                if (page.getPageCurrent() >= page.getTotalPage()) {
                    isGo = false;
                }
                bo.setPageCurrent(page.getPageCurrent() + 1);
            } while (isGo);

            writeExcel(response, workbook, fileName);

        } catch (Exception e) {
            logger.error("系统错误", e);
        }

    }

    /**
     * 写入文件
     */
    private void writeExcel(HttpServletResponse response, SXSSFWorkbook workbook, String fileName) throws IOException {
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");// 设置强制下载不打开
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String(fileName.getBytes(), StandardCharsets.ISO_8859_1) + ".xlsx");
        try {
            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            logger.error("系统错误", e);
        } finally {
            if (workbook != null) {
                workbook.close();
            }
            if (response != null) {
                response.flushBuffer();
            }
        }
    }
}
