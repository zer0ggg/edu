package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级信息分页查询对象--查询讲师创建的班级
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeInfoPageBO", description = "班级信息分页查询对象--查询讲师创建的班级")
public class AuthGradeInfoPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = -8620430812316792354L;

    @ApiModelProperty(value = "班级名称")
    private String gradeName;

    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)")
    private Integer verificationSet;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;
}
