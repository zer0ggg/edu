package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 试卷试题收藏
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemCollectionPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @JsonFormat(pattern = "yyyy-mm-dd HH:mm")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "用户编码")
    private Long userNo;

    @ApiModelProperty(value = "收藏类型(4:试卷,5:题目)")
    private Integer collectionType;

    @ApiModelProperty(value = "题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "年级名称--试卷专有")
    private String graName;

    @ApiModelProperty(value = "科目名称--试卷专有")
    private String subjectName;

    @ApiModelProperty(value = "年份名称--试卷专有")
    private String yearName;

    @ApiModelProperty(value = "来源名称--试卷专有")
    private String sourceName;

    @ApiModelProperty(value = "收藏ID")
    private Long collectionId;

    @ApiModelProperty(value = "收藏名称")
    private String collectionName;

    @ApiModelProperty(value = "学习人数")
    private Integer studyCount;

    @ApiModelProperty(value = "下载人数")
    private Integer downloadCount;

}
