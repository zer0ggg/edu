package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamEditREQ;
import com.roncoo.education.exam.common.req.UserExamListREQ;
import com.roncoo.education.exam.common.req.UserExamSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamListRESP;
import com.roncoo.education.exam.common.resp.UserExamViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户试卷 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户试卷管理")
@RestController
@RequestMapping("/exam/pc/user/exam")
public class PcUserExamController {

    @Autowired
    private PcUserExamBiz biz;

    @ApiOperation(value = "用户试卷列表", notes = "用户试卷列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamListRESP>> list(@RequestBody UserExamListREQ userExamListREQ) {
        return biz.list(userExamListREQ);
    }

    @ApiOperation(value = "用户试卷添加", notes = "用户试卷添加")
    @SysLog(value = "用户试卷添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamSaveREQ userExamSaveREQ) {
        return biz.save(userExamSaveREQ);
    }

    @ApiOperation(value = "用户试卷查看", notes = "用户试卷查看")
    @GetMapping(value = "/view")
    public Result<UserExamViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户试卷修改", notes = "用户试卷修改")
    @SysLog(value = "用户试卷修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamEditREQ userExamEditREQ) {
        return biz.edit(userExamEditREQ);
    }

    @ApiOperation(value = "用户试卷删除", notes = "用户试卷删除")
    @SysLog(value = "用户试卷删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
