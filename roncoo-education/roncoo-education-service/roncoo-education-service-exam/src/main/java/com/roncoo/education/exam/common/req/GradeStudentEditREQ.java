package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeStudentEditREQ", description = "修改")
public class GradeStudentEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键ID不能为空")
    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "用户角色(1:普通成员，2:管理员)")
    private Integer userRole;

    @ApiModelProperty(value = "昵称")
    private String nickname;
}
