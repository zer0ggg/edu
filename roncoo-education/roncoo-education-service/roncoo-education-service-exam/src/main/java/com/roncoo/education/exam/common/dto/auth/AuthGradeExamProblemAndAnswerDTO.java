package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 班级考试题目带答案响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamProblemAndAnswerDTO", description = "班级考试题目带答案响应对象")
public class AuthGradeExamProblemAndAnswerDTO implements Serializable {

    private static final long serialVersionUID = -5500717151261328722L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "题目答案")
    private String problemAnswer;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "题目分值")
    private Integer problemScore;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @ApiModelProperty(value = "个人分类ID")
    private Long personalId;

    @ApiModelProperty(value = "考点ID")
    private Long emphasisId;

    @ApiModelProperty(value = "填空数")
    private Integer optionCount;

    @ApiModelProperty(value = "题目状态")
    private Integer problemStatus;

    @ApiModelProperty(value = "选项")
    private List<AuthGradeExamProblemOptionAndAnswerDTO> optionList;

    @ApiModelProperty(value = "子题")
    private List<AuthGradeExamProblemAndAnswerDTO> childrenList;

    @ApiModelProperty(value = "用户答案ID")
    private Long userAnswerId;

    @ApiModelProperty(value = "用户答案")
    private String userAnswer;

    @ApiModelProperty(value = "答案状态(1:待评阅，2:已评阅)")
    private Integer answerStatus;

    @ApiModelProperty(value = "系统审核(1:未确定，2:系统审核，3:人工审核)")
    private Integer sysAudit;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "审核用户编号")
    private Long auditUserNo;

}
