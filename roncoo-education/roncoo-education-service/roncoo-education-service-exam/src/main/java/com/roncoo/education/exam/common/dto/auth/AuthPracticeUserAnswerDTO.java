package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户练习答案
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeUserAnswerDTO ", description="用户练习答案")
public class AuthPracticeUserAnswerDTO implements Serializable {

    @ApiModelProperty(value = "练习id")
    private Long practiceId;

    @ApiModelProperty(value = "题目父类id")
    private Long problemParentId;

    @ApiModelProperty(value = "题目ID")
    private Long problemId;

    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)")
    private Integer problemType;

    @ApiModelProperty(value = "题目分值")
    private Integer problemScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "回答内容")
    private String userAnswer;

    private static final long serialVersionUID = 1L;
}
