package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.common.polyv.PolyvCode;
import com.roncoo.education.common.polyv.PolyvSign;
import com.roncoo.education.common.polyv.PolyvSignResult;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthExamInfoBaseViewBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamInfoViewBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamVideoSignBO;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 试卷信息
 *
 * @author wujing
 */
@Component
public class AuthExamInfoBiz extends BaseBiz {

    @Autowired
    private ExamInfoDao dao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private ExamProblemCollectionDao examProblemCollectionDao;
    @Autowired
    private UserExamRecordDao userExamRecordDao;
    @Autowired
    private UserOrderExamRefDao userOrderExamRefDao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    public Result<AuthExamInfoBaseViewDTO> info(AuthExamInfoBaseViewBO viewBO) {
        // 试卷信息
        ExamInfo examInfo = dao.getById(viewBO.getExamId());
        if (StringUtils.isEmpty(examInfo)) {
            return Result.error("获取试卷失败");
        }

        AuthExamInfoBaseViewDTO viewDTO = BeanUtil.copyProperties(examInfo, AuthExamInfoBaseViewDTO.class);
        viewDTO.setLecturerUserNo(ThreadContext.userNo());
        getExamInfoBaseViewDTOCategoryInfo(viewDTO);

        //判断是否收藏
        ExamProblemCollection examProblemCollection = examProblemCollectionDao.getByUserNoAndCollectionIdAndCollectionType(ThreadContext.userNo(), examInfo.getId(), CollectionTypeEnum.EXAM.getCode());
        viewDTO.setIsCollect(ObjectUtil.isNotEmpty(examProblemCollection));

        // 判断是否已经购买
        UserOrderExamRef userOrderExamRef = userOrderExamRefDao.getByUserNoAndExamId(ThreadContext.userNo(), viewBO.getExamId());
        viewDTO.setIsPay(ObjectUtil.isNotEmpty(userOrderExamRef));

        // 是否考试中
        UserExamRecord userExamRecord = userExamRecordDao.getByUserNoAndExamIdAndExamStatusNotOver(ThreadContext.userNo(), examInfo.getId(), ExamStatusEnum.NOT_OVER.getCode());
        if (ObjectUtil.isNotEmpty(userExamRecord)) {
            viewDTO.setRecordId(userExamRecord.getId());
        }
        return Result.success(viewDTO);
    }


    /**
     * 试卷详情
     *
     * @param viewBO 查询参数
     * @return 试卷详情
     */
    public Result<AuthExamInfoViewDTO> view(AuthExamInfoViewBO viewBO) {
        logger.info(viewBO.toString());
        // 试卷信息
        ExamInfo examInfo = dao.getById(viewBO.getExamId());
        if (StringUtils.isEmpty(examInfo)) {
            return Result.error("获取试卷失败");
        }

        AuthExamInfoViewDTO viewDTO = BeanUtil.copyProperties(examInfo, AuthExamInfoViewDTO.class);
        getExamInfoViewDTOCategoryInfo(viewDTO);

        //判断是否收藏
        ExamProblemCollection examProblemCollection = examProblemCollectionDao.getByUserNoAndCollectionIdAndCollectionType(ThreadContext.userNo(), examInfo.getId(), CollectionTypeEnum.EXAM.getCode());
        viewDTO.setIsCollect(ObjectUtil.isNotEmpty(examProblemCollection));

        // 判断是否已经购买
        UserOrderExamRef userOrderExamRef = userOrderExamRefDao.getByUserNoAndExamId(ThreadContext.userNo(), viewBO.getExamId());
        viewDTO.setIsPay(ObjectUtil.isNotEmpty(userOrderExamRef));

        // 是否考试中
        UserExamRecord userExamRecord = userExamRecordDao.getByUserNoAndExamIdAndExamStatusNotOver(ThreadContext.userNo(), examInfo.getId(), ExamStatusEnum.NOT_OVER.getCode());
        if (ObjectUtil.isNotEmpty(userExamRecord)) {
            viewDTO.setRecordId(userExamRecord.getId());
        }

        // 试卷信息
        List<ExamTitle> titleList = examTitleDao.listByExamIdAndStatusId(examInfo.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(titleList)) {
            return Result.success(viewDTO);
        }
        List<AuthExamTitleDTO> titleDTOList = ArrayListUtil.copy(titleList, AuthExamTitleDTO.class);
        for (AuthExamTitleDTO examTitleDTO : titleDTOList) {
            // 获取标题下的小题关联
            List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleIdAndStatusId(examTitleDTO.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isEmpty(refList)) {
                continue;
            }
            //保留关联列表
            Map<Long, ExamTitleProblemRef> problemRefMap = refList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, examTitleProblemRef->examTitleProblemRef));
            //查出 试题主键
            List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
            List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
            Map<Long, ExamProblem> problemListMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, item -> item));

            // 获取小题
            List<AuthExamProblemDTO> problemDTOList = new ArrayList<>();
            if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitleDTO.getTitleType())) {
                for (ExamTitleProblemRef examTitleProblemRef : refList) {
                    ExamProblem problem = problemListMap.get(examTitleProblemRef.getProblemId());
                    if (ObjectUtil.isNotNull(problem) && ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
                        AuthExamProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamProblemDTO.class);
                        problemDTO.setChildrenList(listMaterialProblemSubProblem(problem.getId(), problemList));
                        problemDTOList.add(problemDTO);
                    }
                }
            } else {
                for (ExamTitleProblemRef examTitleProblemRef : refList) {
                    ExamProblem problem = problemListMap.get(examTitleProblemRef.getProblemId());
                    if (ObjectUtil.isNotNull(problem)) {
                        AuthExamProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamProblemDTO.class);
                        //保留试题关联中的分数值
                        problemDTO.setScore(problemRefMap.get(problemDTO.getId()).getScore());
                        if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitleDTO.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitleDTO.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitleDTO.getTitleType())) {
                            // 获取选项
                            List<ExamProblemOption> problemOptionList = examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId());
                            problemDTO.setOptionList(BeanUtil.copyProperties(problemOptionList, AuthExamProblemOptionDTO.class));
                        } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(examTitleDTO.getTitleType())) {
                            // 若是填空题、获取填空个数
                            problemDTO.setOptionCount(StrUtil.isBlank(problem.getProblemAnswer()) ? 0 : problem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                        }
                        problemDTOList.add(problemDTO);
                    }
                }
            }
            // 添加小题
            examTitleDTO.setProblemList(problemDTOList);
        }
        viewDTO.setTitleList(titleDTOList);

        // 修改浏览人数
        ExamInfo updateExamInfo = new ExamInfo();
        updateExamInfo.setId(examInfo.getId());
        updateExamInfo.setStudyCount(examInfo.getStudyCount() + 1);
        dao.updateById(updateExamInfo);
        return Result.success(viewDTO);
    }

    private List<AuthExamProblemDTO> listMaterialProblemSubProblem(Long materialExamProblemId, List<ExamProblem> problemList) {
        List<ExamProblem> subProblemList = problemList.stream().filter(problem -> materialExamProblemId.equals(problem.getParentId())).collect(Collectors.toList());

        List<AuthExamProblemDTO> childrenList = new ArrayList<>();
        for (ExamProblem subProblem : subProblemList) {
            AuthExamProblemDTO subProblemDTO = BeanUtil.copyProperties(subProblem, AuthExamProblemDTO.class);
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblemDTO.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblemDTO.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblemDTO.getProblemType())) {
                // 获取选项
                List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(subProblemDTO.getId());
                subProblemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamProblemOptionDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblemDTO.getProblemType())) {
                // 若是填空题、获取填空个数
                subProblemDTO.setOptionCount(StrUtil.isBlank(subProblem.getProblemAnswer()) ? 0 : subProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
            }
            childrenList.add(subProblemDTO);
        }
        return childrenList;
    }

    public Result<AuthExamVideoSignDTO> sign(AuthExamVideoSignBO bo) {
        // 查找用户信息
        UserExtVO userextVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userextVO)) {
            return Result.error("用户不存在，系统管理员");
        }
        if (StatusIdEnum.NO.getCode().equals(userextVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系客服");
        }
        ExamProblem examProblem = examProblemDao.getById(bo.getProblemId());
        if (ObjectUtil.isNull(examProblem)) {
            return Result.error("试题不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(examProblem.getStatusId())) {
            return Result.error("试题已禁用");
        }

        ExamInfo examInfo = dao.getById(bo.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("试卷不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(examInfo.getStatusId())) {
            return Result.error("试卷已禁用");
        }
        // 如果是免费获取播放sign值
        if (IsFreeEnum.FREE.getCode().equals(examInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(examProblem.getIsFree())) {
            return Result.success(getSign(bo));
        }

        // 课程会员免费、用戶是会员
        if (VipCheckUtil.checkVipIsFree(userextVO.getIsVip(), userextVO.getExpireTime(), examInfo.getFabPrice())) {
            return Result.success(getSign(bo));
        }

        // 校验是否购买
        UserOrderExamRef userOrderExamRef = userOrderExamRefDao.getByUserNoAndExamId(ThreadContext.userNo(), examInfo.getId());
        if (ObjectUtil.isNull(userOrderExamRef)) {
            // 未购买或者没支付情况
            return Result.error("收费课程，请先购买");
        }
        return Result.success(getSign(bo));
    }

    /**
     * 获取播放sign值
     *
     * @return sign值
     */
    private AuthExamVideoSignDTO getSign(AuthExamVideoSignBO bo) {
        ConfigPolyvVO sys = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sys)) {
            try {
                throw new Exception("找不到系统配置信息");
            } catch (Exception e) {
                logger.error("获取播放sign值失败", e);
            }
        }
        if (StringUtils.isEmpty(sys.getPolyvUseid()) || StringUtils.isEmpty(sys.getPolyvSecretkey())) {
            try {
                throw new Exception("useid或secretkey未配置");
            } catch (Exception e) {
                logger.error("获取播放sign值失败", e);
            }
        }
        PolyvSign polyvSign = new PolyvSign();
        polyvSign.setIp(bo.getIp());
        polyvSign.setUserNo(ThreadContext.userNo());
        polyvSign.setVid(bo.getVideoVid());
        PolyvSignResult signResult = PolyvUtil.getSignForH5(polyvSign, sys.getPolyvUseid(), sys.getPolyvSecretkey());
        AuthExamVideoSignDTO dto = BeanUtil.copyProperties(signResult, AuthExamVideoSignDTO.class);
        PolyvCode polyvCode = new PolyvCode();
        polyvCode.setProblemId(bo.getProblemId());
        polyvCode.setExamId(bo.getExamId());
        polyvCode.setUserNo(ThreadContext.userNo());
        polyvCode.setPeriodId(0L);
        dto.setCode(PolyvUtil.getPolyvCode(polyvCode));
        return dto;
    }

    /**
     * 获取试卷分类信息
     *
     * @param viewDTO 试卷查看对象
     */
    private void getExamInfoBaseViewDTOCategoryInfo(AuthExamInfoBaseViewDTO viewDTO) {
        // 获取年级名称
        ExamCategory gradeCategory = examCategoryDao.getById(viewDTO.getGradeId());
        if (ObjectUtil.isNotEmpty(gradeCategory)) {
            viewDTO.setGradeName(gradeCategory.getCategoryName());
        }
        // 获取科目名称
        ExamCategory subjectCategory = examCategoryDao.getById(viewDTO.getSubjectId());
        if (ObjectUtil.isNotEmpty(subjectCategory)) {
            viewDTO.setSubjectName(subjectCategory.getCategoryName());
        }
        // 年份ID
        ExamCategory yearCategory = examCategoryDao.getById(viewDTO.getYearId());
        if (ObjectUtil.isNotEmpty(yearCategory)) {
            viewDTO.setYearName(yearCategory.getCategoryName());
        }
        //获取来源名称
        ExamCategory sourceCategory = examCategoryDao.getById(viewDTO.getSourceId());
        if (ObjectUtil.isNotEmpty(sourceCategory)) {
            viewDTO.setSourceName(sourceCategory.getCategoryName());
        }
    }

    /**
     * 获取试卷分类信息
     *
     * @param viewDTO 试卷查看对象
     */
    private void getExamInfoViewDTOCategoryInfo(AuthExamInfoViewDTO viewDTO) {
        // 获取科目名称
        ExamCategory subjectCategory = examCategoryDao.getById(viewDTO.getSubjectId());
        if (ObjectUtil.isNotEmpty(subjectCategory)) {
            viewDTO.setSubjectName(subjectCategory.getCategoryName());
        }
        // 年份ID
        ExamCategory yearCategory = examCategoryDao.getById(viewDTO.getYearId());
        if (ObjectUtil.isNotEmpty(yearCategory)) {
            viewDTO.setYearName(yearCategory.getCategoryName());
        }
        //获取来源名称
        ExamCategory sourceCategory = examCategoryDao.getById(viewDTO.getSourceId());
        if (ObjectUtil.isNotEmpty(sourceCategory)) {
            viewDTO.setSourceName(sourceCategory.getCategoryName());
        }
    }

}
