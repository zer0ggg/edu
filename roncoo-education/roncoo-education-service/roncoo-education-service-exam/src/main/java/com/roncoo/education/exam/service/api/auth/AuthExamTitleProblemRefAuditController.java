package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.exam.service.api.auth.biz.AuthExamTitleProblemRefAuditBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题题目关联审核 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/exam/app/examTitleProblemRefAudit")
public class AuthExamTitleProblemRefAuditController {

    @Autowired
    private AuthExamTitleProblemRefAuditBiz biz;

}
