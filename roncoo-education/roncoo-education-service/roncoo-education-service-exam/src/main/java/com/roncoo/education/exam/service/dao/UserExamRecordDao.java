package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;

import java.util.List;

/**
 * 用户考试记录 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamRecordDao {

    /**
     * 保存用户考试记录
     *
     * @param record 用户考试记录
     * @return 影响记录数
     */
    int save(UserExamRecord record);

    /**
     * 根据ID删除用户考试记录
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 用户考试记录
     * @return 影响记录数
     */
    int updateById(UserExamRecord record);

    /**
     * 根据ID获取用户考试记录
     *
     * @param id 主键ID
     * @return 用户考试记录
     */
    UserExamRecord getById(Long id);

    /**
     * 根据ID获取用户考试记录--加行锁
     *
     * @param id 主键ID
     * @return 用户考试记录
     */
    UserExamRecord getByIdForUpdate(Long id);

    /**
     * 用户考试记录--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<UserExamRecord> listForPage(int pageCurrent, int pageSize, UserExamRecordExample example);

    /**
     * 根据用户编号和考试状态进行列出
     *
     * @param userNo     用户编号
     * @param examStatus 试卷状态
     * @return 用户考试记录
     */
    List<UserExamRecord> listByUserNoAndExamStatus(Long userNo, Integer examStatus);

    /**
     * 根据用户编号、试卷ID和考试状态进行列出
     *
     * @param userNo     用户编号
     * @param examId     试卷ID
     * @param examStatus 试卷状态
     * @return 用户考试记录
     */
    List<UserExamRecord> listByUserNoAndExamIdAndExamStatus(Long userNo, Long examId, Integer examStatus);

    /**
     * 根据用户编号、试卷id获取考试记录
     * @param userNo
     * @param examId
     * @return
     */
    UserExamRecord getByUserNoAndExamId(Long userNo, Long examId);

    /**
     * 根据用户编号、试卷ID和考试状态进行列出
     *
     * @param userNo     用户编号
     * @param examId     试卷ID
     * @param examStatus 试卷状态
     * @return 用户考试记录
     */
    UserExamRecord getByUserNoAndExamIdAndExamStatusNotOver(Long userNo, Long examId, Integer examStatus);
}
