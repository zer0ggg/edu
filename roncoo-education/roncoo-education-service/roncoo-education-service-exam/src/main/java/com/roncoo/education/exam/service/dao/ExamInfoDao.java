package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoExample;

import java.util.List;

/**
 * 试卷信息 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamInfoDao {

    /**
    * 保存试卷信息
    *
    * @param record 试卷信息
    * @return 影响记录数
    */
    int save(ExamInfo record);

    /**
    * 根据ID删除试卷信息
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷信息
    * @return 影响记录数
    */
    int updateById(ExamInfo record);

    /**
    * 根据ID获取试卷信息
    *
    * @param id 主键ID
    * @return 试卷信息
    */
    ExamInfo getById(Long id);

    /**
     * 根据ID和状态获取试卷信息
     * @param id
     * @param statusId
     * @return
     */
    ExamInfo getByIdAndStatusId(Long id,Integer statusId);

    /**
    * 试卷信息--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamInfo> listForPage(int pageCurrent, int pageSize, ExamInfoExample example);

    /**
     * 算数量
     * @param example
     * @return
     */
    int countByExample(ExamInfoExample example);

    /**
     * 根据ID集合获取试卷信息
     * @param examIds
     * @return
     */
    List<ExamInfo> listByIds(List<Long> examIds);
}
