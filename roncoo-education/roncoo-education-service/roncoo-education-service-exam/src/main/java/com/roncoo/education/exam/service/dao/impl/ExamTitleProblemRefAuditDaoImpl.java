package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamTitleProblemRefAuditMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷标题题目关联审核 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamTitleProblemRefAuditDaoImpl implements ExamTitleProblemRefAuditDao {

    @Autowired
    private ExamTitleProblemRefAuditMapper mapper;

    @Override
    public int save(ExamTitleProblemRefAudit record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamTitleProblemRefAudit record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamTitleProblemRefAudit getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamTitleProblemRefAudit> listForPage(int pageCurrent, int pageSize, ExamTitleProblemRefAuditExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleId(Long titleId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleIdAndStatusId(Long titleId,Integer statusId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleIdAndProblemParentIdAndStatusId(Long titleId, Long pId, Integer statusId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(pId);
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleIdAndProblemParentId(Long titleId, Long pId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(pId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleIdAndAuditStatus(Long titleId, Integer auditStatus) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andAuditStatusEqualTo(auditStatus);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByUserNoAndStatusId(Long userNo, Integer auditStatus) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(userNo);
        c.andStatusIdEqualTo(auditStatus);
        example.setOrderByClause("sort asc, status_id desc,  id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByTitleIdAndProblemId(Long titleId, Long problemId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        if (problemId != null) {
            c.andProblemIdEqualTo(problemId);
        }
        example.setOrderByClause("sort asc, status_id desc,  id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByTitleId(Long titleId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByTitleIdAndParentProblemId(Long titleId,Long parentProblemId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemParentIdEqualTo(parentProblemId);
        return this.mapper.deleteByExample(example);
    }
    @Override
    public ExamTitleProblemRefAudit getByTitleIdAndProblemId(Long titleId, Long problemId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(titleId);
        c.andProblemIdEqualTo(problemId);
        List<ExamTitleProblemRefAudit> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public int updateByTitleId(ExamTitleProblemRefAudit record) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andTitleIdEqualTo(record.getTitleId());
        return this.mapper.updateByExample(record,example);
    }

    @Override
    public int update(ExamTitleProblemRefAudit record, ExamTitleProblemRefAuditExample example) {
        return this.mapper.updateByExampleSelective(record,example);
    }

    @Override
    public List<ExamTitleProblemRefAudit> listByExamId(Long examId) {
        ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
        ExamTitleProblemRefAuditExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        return this.mapper.selectByExample(example);
    }


}
