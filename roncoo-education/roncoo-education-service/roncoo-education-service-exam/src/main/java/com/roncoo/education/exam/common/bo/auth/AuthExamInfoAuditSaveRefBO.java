package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 试卷信息审核列表
 *
 * @author zkpc
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditSaveRefBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷id", required = true)
    private Long id;

    @ApiModelProperty(value = "标题集合")
    private List<AuthExamTitleAuditSaveBO> titleList;

    @ApiModelProperty(value = "试卷分值")
    private Integer totalScore;

    @ApiModelProperty(value = "试题数量")
    private Integer problemQuantity;

}
