package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamRecommend;
import com.roncoo.education.exam.feign.qo.ExamRecommendQO;
import com.roncoo.education.exam.feign.vo.ExamRecommendVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamRecommendBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷推荐
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamRecommendController extends BaseController implements IFeignExamRecommend{

    @Autowired
    private FeignExamRecommendBiz biz;

	@Override
	public Page<ExamRecommendVO> listForPage(@RequestBody ExamRecommendQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamRecommendQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamRecommendQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamRecommendVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
