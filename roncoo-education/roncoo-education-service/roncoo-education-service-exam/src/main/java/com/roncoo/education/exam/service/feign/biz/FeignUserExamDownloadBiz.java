package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamDownloadQO;
import com.roncoo.education.exam.feign.vo.UserExamDownloadVO;
import com.roncoo.education.exam.service.dao.UserExamDownloadDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownload;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷下载
 *
 * @author wujing
 */
@Component
public class FeignUserExamDownloadBiz extends BaseBiz {

    @Autowired
    private UserExamDownloadDao dao;

	public Page<UserExamDownloadVO> listForPage(UserExamDownloadQO qo) {
	    UserExamDownloadExample example = new UserExamDownloadExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExamDownload> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamDownloadVO.class);
	}

	public int save(UserExamDownloadQO qo) {
		UserExamDownload record = BeanUtil.copyProperties(qo, UserExamDownload.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamDownloadVO getById(Long id) {
		UserExamDownload record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamDownloadVO.class);
	}

	public int updateById(UserExamDownloadQO qo) {
		UserExamDownload record = BeanUtil.copyProperties(qo, UserExamDownload.class);
		return dao.updateById(record);
	}

}
