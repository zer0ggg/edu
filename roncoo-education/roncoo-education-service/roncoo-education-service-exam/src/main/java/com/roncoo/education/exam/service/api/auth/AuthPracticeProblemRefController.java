package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.exam.service.api.auth.biz.AuthPracticeProblemRefBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 题目练习关联 UserApi接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-AUTH-题目练习关联")
@RestController
@RequestMapping("/exam/auth/practiceProblemRef")
public class AuthPracticeProblemRefController {

    @Autowired
    private AuthPracticeProblemRefBiz biz;

}
