package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ExcelToolUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeStudentPageDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.interfaces.IFeignUserMsg;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wujing
 */
@Component
public class AuthGradeStudentBiz extends BaseBiz {

    @Autowired
    private GradeStudentDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeApplyRecordDao gradeApplyRecordDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeStudentExamAnswerDao;
    @Autowired
    private GradeOperationLogDao gradeOperationLogDao;

    @Autowired
    private IFeignUser feignUser;
    @Autowired
    private IFeignLecturer feignLecturer;

    @Autowired
    private IFeignUserMsg feignUserMsg;


    /**
     * 分页列出
     *
     * @param bo 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthGradeStudentPageDTO>> listForPage(AuthGradeStudentPageBO bo) {
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，无法获取班级学生");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                return Result.error("不是班级管理员，无法获取班级学生");
            }
        }
        GradeStudentExample example = new GradeStudentExample();
        GradeStudentExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(bo.getGradeId());
        if (ObjectUtil.isNotNull(bo.getUserRole())) {
            c.andUserRoleEqualTo(bo.getUserRole());
        }
        if (ObjectUtil.isNotNull(bo.getNickname())) {
            c.andNicknameIsNotNull();
            c.andNicknameLike(PageUtil.like(bo.getNickname()));
        }
        Page<GradeStudent> resultPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(resultPage, AuthGradeStudentPageDTO.class));
    }

    /**
     * 加入班级
     *
     * @param bo 加入班级参数
     * @return 申请结果
     */
    public Result<String> gradeAdd(AuthGradeStudentAddGradeBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("加入班级-->用户：[{}]不存在，不能加入班级", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("加入班级-->用户：[{}]--状态：[{}]，用户状态异常，不能加入班级", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 获取班级
        GradeInfo gradeInfo = gradeInfoDao.getByInvitationCode(bo.getInvitationCode());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("无效邀请码");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("当前班级不可用");
        }
        if (gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("讲师不能加入自己班级");
        }

        // 判断是否是班级学生
        GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
        if (ObjectUtil.isNotNull(gradeStudent)) {
            return Result.error("已经是班级成员，不能重复加入");
        }
        //默认是申请状态
        boolean isAdd = false;
        if (GradeVerificationSetEnum.ANYONE.getCode().equals(gradeInfo.getVerificationSet())) {

            // 允许任何人加入
            GradeStudent saveStudent = new GradeStudent();
            saveStudent.setGradeId(gradeInfo.getId());
            saveStudent.setUserNo(ThreadContext.userNo());
            saveStudent.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            saveStudent.setUserRole(GradeStudentUserRoleEnum.USER.getCode());
            saveStudent.setNickname(bo.getNickname());
            dao.save(saveStudent);
            isAdd = true;
        } else {
            // 加入时需要验证
            GradeApplyRecord applyRecord = new GradeApplyRecord();
            applyRecord.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            applyRecord.setGradeId(gradeInfo.getId());
            applyRecord.setUserNo(userVO.getUserNo());
            applyRecord.setAuditStatus(GradeApplyAuditStatusEnum.WAIT.getCode());
            applyRecord.setApplyDescription(bo.getApplyDescription());
            applyRecord.setNickname(bo.getNickname());
            gradeApplyRecordDao.save(applyRecord);
        }
        try {
            // 推送站内信
            pushMsgForGradeAdd(gradeInfo, bo, isAdd);
        } catch (Exception e) {
            logger.warn("推送站内信失败", e);
        }
        return Result.success(isAdd ? "加入班级成功" : "成功提交加入班级申请");
    }

    /**
     * 修改学生信息
     *
     * @param bo 修改学生信息对象
     * @return 修改结果
     */
    public Result<String> edit(AuthGradeStudentEditBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("修改学生信息-->用户：[{}]不存在，不能设置或取消班级管理员", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("修改学生信息-->用户：[{}]--状态：[{}]，用户状态异常，不能设置或取消班级管理员", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 判断学生是否存在
        GradeStudent gradeStudent = dao.getById(bo.getId());
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("学生不存在");
        }

        //判断管理员
        if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
            logger.warn("修改学生信息-->用户：[{}]不是管理员", ThreadContext.userNo());
            // 判断是否为讲师或管理员
            GradeInfo gradeInfo = gradeInfoDao.getById(gradeStudent.getGradeId());
            if (gradeInfo.getLecturerUserNo().longValue() != ThreadContext.userNo()) {
                logger.warn("修改学生信息-->用户：[{}]不是讲师", ThreadContext.userNo());
                return Result.error("用户不是讲师或讲师，不能管理学员");
            }
        }

        GradeStudent updateGradeStudent = new GradeStudent();
        updateGradeStudent.setId(gradeStudent.getId());
        updateGradeStudent.setNickname(bo.getNickname());

        if (dao.updateById(updateGradeStudent) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }


    /**
     * 退出班级
     *
     * @param bo 退出班级参数
     * @return 退出结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> quit(AuthGradeStudentQuitBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("退出班级-->用户：[{}]不存在，退出班级失败", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("退出班级-->用户：[{}]--状态：[{}]，用户状态异常，退出班级失败", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 校验班级
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("班级状态不可用");
        }
        GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("不在该班级中或已经退出");
        }

        // 退出班级,记录日志
        dao.deleteById(gradeStudent.getId());

        // 删除学生相关的班级考试，考试成绩等
        deleteStudentGradeRelation(gradeInfo.getId(), gradeStudent.getId());

        // 记录退出日志
        createQuitGradeInfoLog(gradeInfo, gradeStudent);
        //给讲师发站内信
        try {
            pushMsgForQuit(gradeInfo, gradeStudent.getNickname());
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return Result.success("退出成功");
    }

    /**
     * 设置或取消班级管理员
     *
     * @param bo 变更对象
     * @return 变更结果
     */
    public Result<String> updateAdmin(AuthGradeStudentUpdateAdminBO bo) {
        // 判断用户状态
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("设置或取消班级管理员-->用户：[{}]不存在，不能设置或取消班级管理员", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("设置或取消班级管理员-->用户：[{}]--状态：[{}]，用户状态异常，不能设置或取消班级管理员", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 判断是否为讲师
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            logger.warn("设置或取消班级管理员-->用户：[{}]不是讲师，不能设置或取消班级管理员", ThreadContext.userNo());
            return Result.error("用户不是讲师，不能设置或取消班级管理员");
        }

        // 判断班级
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("班级状态不可用");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("不是当前班级讲师，不能操作");
        }

        // 判断学生
        GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), bo.getStudentUserNo());
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("不是该班级学生，无法设置");
        }

        // 更新
        if (GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
            gradeStudent.setUserRole(GradeStudentUserRoleEnum.USER.getCode());
        } else {
            gradeStudent.setUserRole(GradeStudentUserRoleEnum.ADMIN.getCode());
        }
        gradeStudent.setGmtCreate(null);
        gradeStudent.setGmtModified(null);
        dao.updateById(gradeStudent);

        //发送站内信
        try {
            pushMsgForSetAdmin(gradeInfo, gradeStudent.getUserNo(), GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole()));
        } catch (Exception e) {
            logger.error(e.toString());
        }
        // 记录日志
        createGradeInfoUpdateAdminLog(gradeInfo, gradeStudent);
        return Result.success("变更成功");
    }

    /**
     * 踢出班级
     *
     * @param bo 踢出对象
     * @return 踢出结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> kickOut(AuthGradeStudentKickOutBO bo) {
        if (ThreadContext.userNo().equals(bo.getStudentUserNo())) {
            return Result.error("不能踢除自己");
        }

        // 校验操作者信息
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            logger.warn("踢出班级-->操作用户：[{}]不存在，踢出班级失败", ThreadContext.userNo());
            return Result.error("用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            logger.warn("踢出班级-->操作用户：[{}]--状态：[{}]，用户状态异常，踢出班级失败", ThreadContext.userNo(), userVO.getStatusId());
            return Result.error("用户状态异常");
        }

        // 获取班级信息
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("班级状态不可用");
        }
        GradeStudent gradeAdminStudent = null;
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            gradeAdminStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeAdminStudent)) {
                return Result.error("不是该班级学生，不能操作该班级");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeAdminStudent.getUserRole())) {
                return Result.error("不是班级管理员，没有踢除权限");
            }
        }

        // 校验学员
        GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeInfo.getId(), bo.getStudentUserNo());
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("需要踢除的学生不属于该班级");
        }
        if (GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole()) && !gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("只有讲师才能删除管理员");
        }

        // 踢除学生
        dao.deleteById(gradeStudent.getId());

        // 删除学生相关的班级考试，考试成绩等
        deleteStudentGradeRelation(gradeInfo.getId(), gradeStudent.getId());
        try {
            pushMsgForKickOut(gradeInfo, gradeAdminStudent, gradeStudent);
        } catch (Exception e) {
            logger.error(e.toString());
        }
        // 记录踢除日志
        createKickOutGradeInfoLog(gradeInfo, userVO, gradeAdminStudent, gradeStudent);
        return Result.success("踢除成功");
    }

    /**
     * 删除学生班级关联
     *
     * @param gradeId   班级ID
     * @param studentId 学生ID
     */
    private void deleteStudentGradeRelation(Long gradeId, Long studentId) {
        // 删除用户班级考试
        gradeExamStudentRelationDao.deleteByGradeIdAndStudentId(gradeId, studentId);

        // 删除用户考试标题得分
        gradeStudentExamTitleScoreDao.deleteByGradeIdAndStudentId(gradeId, studentId);

        // 删除用户答案
        gradeStudentExamAnswerDao.deleteByGradeIdAndStudentId(gradeId, studentId);
    }

    /**
     * 保存退出班级日志
     *
     * @param gradeInfo 班级信息
     * @param student   学生信息
     */
    private void createQuitGradeInfoLog(GradeInfo gradeInfo, GradeStudent student) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.QUIT_GRADE.getCode());

            // 操作内容
            String operationContent = "学生：【" +
                    student.getNickname() +
                    "(" +
                    student.getUserNo() +
                    ")" +
                    "】退出班级【" +
                    gradeInfo.getGradeName() +
                    "(" +
                    gradeInfo.getId() +
                    ")" +
                    "】";
            operationLog.setOperationContent(operationContent);
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("学生【{}】退出班级【{}】，添加日志失败！", student.getUserNo(), student.getGradeId(), e);
        }
    }

    /**
     * 创建班级设置或取消管理员
     *
     * @param gradeInfo 班级信息
     * @param student   班级学生
     */
    private void createGradeInfoUpdateAdminLog(GradeInfo gradeInfo, GradeStudent student) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.KICK_OUT_GRADE.getCode());

            // 操作内容
            String operationContent = "学生：【" +
                    student.getNickname() +
                    "(" +
                    student.getUserNo() +
                    ")】" +
                    "被讲师修改为班级：【" +
                    gradeInfo.getGradeName() +
                    "(" +
                    gradeInfo.getId() +
                    ")】的" +
                    GradeStudentUserRoleEnum.byCode(student.getUserRole()).getDesc();
            operationLog.setOperationContent(operationContent);
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("学生【{}】被讲师【{}】在班级【{}】修改或设置为：【{}】，添加日志失败！", student.getUserNo(), gradeInfo.getLecturerUserNo(), student.getGradeId(), student.getUserRole(), e);
        }
    }

    /**
     * 踢出班级
     *
     * @param gradeInfo         班级信息
     * @param userVO            操作员信息
     * @param gradeAdminStudent 班级管理员学生
     * @param student           踢出结果
     */
    private void createKickOutGradeInfoLog(GradeInfo gradeInfo, UserVO userVO, GradeStudent gradeAdminStudent, GradeStudent student) {
        try {
            GradeOperationLog operationLog = new GradeOperationLog();
            operationLog.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            operationLog.setGradeId(gradeInfo.getId());
            operationLog.setOperatorUserNo(userVO.getUserNo());
            operationLog.setOperationType(GradeOperationLogTypeEnum.KICK_OUT_GRADE.getCode());

            // 操作内容
            String operationContent = "学生：【" +
                    student.getNickname() +
                    "(" +
                    student.getUserNo() +
                    ")" +
                    "】被";
            if (userVO.getUserNo().equals(gradeInfo.getLecturerUserNo())) {
                operationContent += "讲师";
            } else {
                operationContent += "管理员【" + gradeAdminStudent.getNickname() + "(" + gradeAdminStudent.getUserNo() + ")】";
            }
            operationContent += "踢出班级【" +
                    gradeInfo.getGradeName() +
                    "(" +
                    gradeInfo.getId() +
                    ")" +
                    "】";
            operationLog.setOperationContent(operationContent);
            gradeOperationLogDao.save(operationLog);
        } catch (Exception e) {
            logger.warn("学生【{}】被(讲师/管理员)【{}】踢出班级【{}】，添加日志失败！", student.getUserNo(), userVO.getUserNo(), student.getGradeId(), e);
        }
    }

    public Result<Boolean> isAdmin(AuthGradeStudentIsAdminBO bo) {
        List<GradeStudent> gradeStudentList = dao.listByUserNoAndUserRoleAndStatusId(ThreadContext.userNo(), GradeStudentUserRoleEnum.ADMIN.getCode(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(gradeStudentList)) {
            return Result.success(false);
        }
        return Result.success(true);
    }

    /**
     * 批量导入用户
     *
     * @param
     * @param
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> uploadExcel(MultipartFile file) {
        XSSFWorkbook wookbook = null;
        try {
            wookbook = new XSSFWorkbook(file.getInputStream());
            XSSFSheet sheet = wookbook.getSheetAt(0);
            // 		指的行数，一共有多少行+
            int rows = sheet.getLastRowNum();
            Long lecturerUserNo = null;
            Long gradeId = null;
            for (int i = 0; i <= rows; i++) {
                // 读取左上端单元格
                XSSFRow row = sheet.getRow(i);
                if (i == 1) {
                    String invitationCode = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 班级推荐码
                    if (StringUtils.isEmpty(invitationCode)) {
                        return Result.error("请输入班级推荐码");
                    }

                    // 获取班级
                    GradeInfo gradeInfo = gradeInfoDao.getByInvitationCode(invitationCode);
                    if (ObjectUtil.isNull(gradeInfo)) {
                        return Result.error("无效邀请码");
                    }
                    if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
                        return Result.error("当前班级不可用");
                    }
                    lecturerUserNo = gradeInfo.getLecturerUserNo();
                    gradeId = gradeInfo.getId();
                }
                if (i >= 3) {
                    // 行不为空
                    String mobile = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 学员手机号
                    if (StringUtils.isEmpty(mobile)) {
                        continue;
                    }
                    // 如果学员手机号为空不添加该学生
                    UserVO userVO = feignUser.getByMobile(mobile);
                    if (ObjectUtil.isNull(userVO)) {
                        continue;
                    }

                    // 如果用户昵称为空不添加该学生
                    String nickname = ExcelToolUtil.getXValue(row.getCell(1)).trim();// 用户昵称
                    if (StringUtils.isEmpty(nickname)) {
                        continue;
                    }

                    // 如果用户角色为空不添加该学生
                    String userRole = ExcelToolUtil.getXValue(row.getCell(2)).trim();// 用户角色(1:普通成员，2:管理员）
                    if (StringUtils.isEmpty(userRole)) {
                        continue;
                    }
                    // 如果用户角色输入不规范不添加该学生
                    if (!"1".equals(userRole) && !"2".equals(userRole)) {
                        continue;
                    }
                    if (lecturerUserNo.equals(userVO.getUserNo())) {
                        // 讲师不能加入自己班级
                        continue;
                    }
                    // 判断是否是班级学生
                    GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(gradeId, userVO.getUserNo());
                    if (ObjectUtil.isNotNull(gradeStudent)) {
                        // 已经是班级成员，不能重复加入
                        continue;
                    }

                    GradeStudent saveStudent = new GradeStudent();
                    saveStudent.setGradeId(gradeId);
                    saveStudent.setUserNo(userVO.getUserNo());
                    saveStudent.setLecturerUserNo(lecturerUserNo);
                    saveStudent.setUserRole(Integer.valueOf(userRole));
                    saveStudent.setNickname(nickname);
                    String remark = ExcelToolUtil.getXValue(row.getCell(3)).trim();// 备注
                    if (StringUtils.hasText(remark)) {
                        saveStudent.setRemark(remark);
                    }
                    dao.save(saveStudent);
                }
            }
            return Result.success("导入成功");
        } catch (IOException e) {
            logger.error("导入失败, {}", e);
            return Result.error("导入失败");
        }
    }

    /**
     * @param gradeInfo 班级信息
     * @param student   加入学生信息
     * @param isAdd     状态 0申请，1加入
     */
    private void pushMsgForGradeAdd(GradeInfo gradeInfo, AuthGradeStudentAddGradeBO student, boolean isAdd) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(gradeInfo.getLecturerUserNo());

        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        msg.setMsgTitle("【" + gradeInfo.getGradeName() + "】来新学员通知");
        if (isAdd) {
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE_TEACHER + "\">就刚才," + student.getNickname() + "加入了您的班级【" + gradeInfo.getGradeName() + "】</a>");
        } else {
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE_TEACHER + "\">就刚才," + student.getNickname() + "申请加入了您的班级【" + gradeInfo.getGradeName() + "】,快去处理吧</a>");
        }
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("新学员通知");
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(gradeInfo.getLecturerUserNo());
        userMsg.setMobile(lecturerVO.getLecturerMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);
        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }

    /**
     * @param gradeInfo  班级信息
     * @param toUserNo   接收人编号
     * @param isSetAdmin 状态 0解除，1设置管理员
     */
    private void pushMsgForSetAdmin(GradeInfo gradeInfo, Long toUserNo, boolean isSetAdmin) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();

        UserVO userVO = feignUser.getByUserNo(toUserNo);

        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        msg.setMsgTitle("【" + gradeInfo.getGradeName() + "】管理员通知");
        if (isSetAdmin) {
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE + "\">恭喜您，刚被设置为【" + gradeInfo.getGradeName() + "】的管理员,快去看看吧</a>");
        } else {
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE + "\">你被解除了【" + gradeInfo.getGradeName() + "】的管理员职位</a>");
        }
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("管理员通知");
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(userVO.getUserNo());
        userMsg.setMobile(userVO.getMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);

        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }

    /**
     * 发送站内消息--退出
     *
     * @param gradeInfo 班级信息
     * @param nickName  昵称
     */
    private void pushMsgForQuit(GradeInfo gradeInfo, String nickName) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();

        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(gradeInfo.getLecturerUserNo());

        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        msg.setMsgTitle("【" + gradeInfo.getGradeName() + "】退班通知");
        msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE_TEACHER + "\">" + nickName + "退出了【" + gradeInfo.getGradeName() + "】</a>");
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("讲师通知");
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(gradeInfo.getLecturerUserNo());
        userMsg.setMobile(lecturerVO.getLecturerMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);
        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }

    /**
     * 踢出班级
     *
     * @param gradeInfo 班级信息
     * @param student   踢出结果
     */
    private void pushMsgForKickOut(GradeInfo gradeInfo, GradeStudent gradeAdminStudent, GradeStudent student) {
        UserVO user = feignUser.getByUserNo(student.getUserNo());
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(gradeInfo.getLecturerUserNo());
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();

        //通知学员
        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        msg.setMsgTitle("【" + gradeInfo.getGradeName() + "】退班通知");
        msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE + "\">您被踢出了【" + gradeInfo.getGradeName() + "】</a>");
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("退班通知");
        msg.setSendTime(null);
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(user.getUserNo());
        userMsg.setMobile(user.getMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);

        list.add(qo);

        //讲师
        if (ObjectUtil.isNotNull(gradeAdminStudent)) {
            UserMsgAndMsgSaveQO lecturerSaveQO = new UserMsgAndMsgSaveQO();
            MsgQO lecturerMsgQO = new MsgQO();
            lecturerMsgQO.setId(IdWorker.getId());
            lecturerMsgQO.setMsgTitle("【" + gradeInfo.getGradeName() + "】退班通知");

            lecturerMsgQO.setMsgText("<a href=\"" + SysConfigConstants.EXAM_MYGRADE_TEACHER + "\">" + student.getNickname() + "被" + gradeAdminStudent.getNickname() + "踢出了【" + gradeInfo.getGradeName() + "】</a>");

            lecturerMsgQO.setStatusId(StatusIdEnum.YES.getCode());
            lecturerMsgQO.setIsTop(IsTopEnum.YES.getCode());
            lecturerMsgQO.setIsSend(IsSendEnum.YES.getCode());
            lecturerMsgQO.setIsTimeSend(IsTimeSendEnum.NO.getCode());
            lecturerMsgQO.setMsgType(MsgTypeEnum.SYSTEM.getCode());
            lecturerMsgQO.setBackRemark("退班通知");
            lecturerMsgQO.setSendTime(null);
            lecturerSaveQO.setMsg(lecturerMsgQO);

            UserMsgQO lecturerUserMsg = new UserMsgQO();
            lecturerUserMsg.setStatusId(StatusIdEnum.YES.getCode());
            lecturerUserMsg.setMsgId(lecturerMsgQO.getId());
            lecturerUserMsg.setUserNo(lecturerVO.getLecturerUserNo());
            lecturerUserMsg.setMobile(lecturerVO.getLecturerMobile());
            lecturerUserMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
            lecturerUserMsg.setMsgTitle(lecturerMsgQO.getMsgTitle());
            lecturerUserMsg.setIsRead(IsReadEnum.NO.getCode());
            lecturerUserMsg.setIsTop(IsTopEnum.YES.getCode());
            lecturerSaveQO.setUserMsg(lecturerUserMsg);

            list.add(lecturerSaveQO);
        }

        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }
}
