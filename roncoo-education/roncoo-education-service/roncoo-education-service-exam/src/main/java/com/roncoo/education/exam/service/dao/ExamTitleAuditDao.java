package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample;

import java.util.List;

/**
 * 试卷标题审核 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamTitleAuditDao {

    /**
    * 保存试卷标题审核
    *
    * @param record 试卷标题审核
    * @return 影响记录数
    */
    int save(ExamTitleAudit record);

    /**
    * 根据ID删除试卷标题审核
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 试卷标题审核
    * @return 影响记录数
    */
    int updateById(ExamTitleAudit record);

    /**
    * 根据ID获取试卷标题审核
    *
    * @param id 主键ID
    * @return 试卷标题审核
    */
    ExamTitleAudit getById(Long id);

    /**
    * 试卷标题审核--分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<ExamTitleAudit> listForPage(int pageCurrent, int pageSize, ExamTitleAuditExample example);

    /**
     * 根据试卷ID列出试卷标题信息
     *
     * @param examId
     * @return
     */
    List<ExamTitleAudit> listByExamId(Long examId);

    /**
     * 根据试卷ID和审核状态列出试卷标题信息
     *
     * @param examId
     * @return
     */
    List<ExamTitleAudit> listByExamIdAndAuditStatus(Long examId,Integer code);

    /**
     * 根据试卷ID删除试卷标题
     *
     * @param examId 试卷ID
     * @return 删除记录数
     */
    int deleteByExamId(Long examId);

    /**
     * 修改
     *
     * @param record 试卷标题审核
     * @return 影响记录数
     */
    int updateAuditStatus(ExamTitleAudit record, ExamTitleAuditExample example);

    /**
     * 修改
     * @param record
     * @param example
     * @return
     */
    int update(ExamTitleAudit record, ExamTitleAuditExample example);

}
