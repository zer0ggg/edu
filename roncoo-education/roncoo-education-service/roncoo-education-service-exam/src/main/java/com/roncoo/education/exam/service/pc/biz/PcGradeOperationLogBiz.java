package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.GradeOperationLogListREQ;
import com.roncoo.education.exam.common.resp.GradeOperationLogPageRESP;
import com.roncoo.education.exam.common.resp.GradeOperationLogViewRESP;
import com.roncoo.education.exam.service.dao.GradeOperationLogDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLog;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author wujing
 */
@Component
public class PcGradeOperationLogBiz extends BaseBiz {

    @Autowired
    private GradeOperationLogDao dao;

    /**
     * 列表
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeOperationLogPageRESP>> list(GradeOperationLogListREQ req) {
        GradeOperationLogExample example = new GradeOperationLogExample();
        Criteria c = example.createCriteria();
        if (ObjectUtil.isNotNull(req.getLecturerUserNo())) {
            c.andLecturerUserNoEqualTo(req.getLecturerUserNo());
        }
        if (ObjectUtil.isNotNull(req.getGradeId())) {
            c.andGradeIdEqualTo(req.getGradeId());
        }
        if (ObjectUtil.isNotNull(req.getOperatorUserNo())) {
            c.andOperatorUserNoEqualTo(req.getOperatorUserNo());
        }
        if (ObjectUtil.isNotNull(req.getOperationType())) {
            c.andOperationTypeEqualTo(req.getOperationType());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeOperationLog> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeOperationLogPageRESP> respPage = PageUtil.transform(page, GradeOperationLogPageRESP.class);
        return Result.success(respPage);
    }

    /**
     * 查看
     *
     * @param id 主键ID
     * @return
     */
    public Result<GradeOperationLogViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), GradeOperationLogViewRESP.class));
    }
}
