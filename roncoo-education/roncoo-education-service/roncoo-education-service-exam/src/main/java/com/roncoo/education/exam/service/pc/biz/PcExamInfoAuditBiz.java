package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.exam.common.es.EsExam;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.ExamInfoAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamInfoAuditViewRESP;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUserMsg;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 试卷信息审核
 *
 * @author wujing
 */
@Component
public class PcExamInfoAuditBiz extends BaseBiz {

    @Autowired
    private ExamInfoAuditDao dao;
    @Autowired
    private ExamTitleAuditDao titleAuditDao;
    @Autowired
    private ExamTitleProblemRefAuditDao problemRefAuditDao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao problemRefDao;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private UserExamCategoryDao userExamCategoryDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private IFeignUserMsg feignUserMsg;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    public Result<Page<ExamInfoAuditListRESP>> list(ExamInfoAuditListREQ req) {
        ExamInfoAuditExample example = new ExamInfoAuditExample();
        ExamInfoAuditExample.Criteria c = example.createCriteria();
        if (StringUtils.hasText(req.getExamName())) {
            c.andExamNameLike(PageUtil.like(req.getExamName()));
        }
        if (req.getSubjectId() != null) {
            c.andSubjectIdEqualTo(req.getSubjectId());
        }
        if (req.getYearId() != null) {
            c.andYearIdEqualTo(req.getYearId());
        }
        if (req.getSourceId() != null) {
            c.andSourceIdEqualTo(req.getSourceId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (req.getIsPutaway() != null) {
            c.andIsPutawayEqualTo(req.getIsPutaway());
        }
        if (req.getIsFree() != null) {
            c.andIsFreeEqualTo(req.getIsFree());
        }
        if (req.getAuditStatus() != null) {
            c.andAuditStatusEqualTo(req.getAuditStatus());
        }
        if (req.getSubmitAudit() != null) {
            c.andSubmitAuditEqualTo(req.getSubmitAudit());
        } else {
            c.andSubmitAuditEqualTo(IsSubmitAuditEnum.SUBMIT.getCode());
        }
        example.setOrderByClause("audit_status asc, sort asc, id desc");
        Page<ExamInfoAudit> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        if (page.getList().isEmpty() || page == null) {
            return Result.success(new Page<>());
        }
        Page<ExamInfoAuditListRESP> respPage = PageUtil.transform(page, ExamInfoAuditListRESP.class);
        List<Long> lecturerUserNos = respPage.getList().stream().map(ExamInfoAuditListRESP::getLecturerUserNo).collect(Collectors.toList());
        LecturerQO lecturerQO = new LecturerQO();
        lecturerQO.setLecturerUserNos(lecturerUserNos);
        List<LecturerVO> lecturerList = feignLecturer.listByLecturerUserNos(lecturerQO);
        Map<Long, LecturerVO> lecturerMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(lecturerList)) {
            lecturerMap = lecturerList.stream().collect(Collectors.toMap(LecturerVO::getLecturerUserNo, item -> item));
        }
        // 获取分类
        List<ExamCategory> examCategoryList = examCategoryDao.listCategoryTypeAll(ExamMainCategoryTypeEnum.EXAM.getCode());
        Map<Long, String> examCategoryMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(examCategoryList)) {
            examCategoryMap = examCategoryList.stream().collect(Collectors.toMap(ExamCategory::getId, item -> item.getCategoryName()));
        }
        List<Long> ids = respPage.getList().stream().map(ExamInfoAuditListRESP::getPersonalId).collect(Collectors.toList());

        // 获取用户试卷分类
        List<UserExamCategory> userExamCategoryList = userExamCategoryDao.listByIds(ids);
        Map<Long, String> userExamCategoryMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(userExamCategoryList)) {
            userExamCategoryMap = userExamCategoryList.stream().collect(Collectors.toMap(UserExamCategory::getId, item -> item.getCategoryName()));
        }

        // 补充分类名称
        for (ExamInfoAuditListRESP dto : respPage.getList()) {
            LecturerVO lecturerVO = lecturerMap.get(dto.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturerVO)) {
                dto.setLecturerName(lecturerVO.getLecturerName());
            }
            if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
                String subjectName = examCategoryMap.get(dto.getSubjectId());
                if (StringUtils.hasText(subjectName)) {
                    dto.setSubjectName(subjectName);
                }
            }
            if (dto.getYearId() != null && dto.getYearId() != 0) {
                String yearName = examCategoryMap.get(dto.getYearId());
                if (StringUtils.hasText(yearName)) {
                    dto.setYear(yearName);
                }
            }
            if (dto.getSourceId() != null && dto.getSourceId() != 0) {
                String sourceName = examCategoryMap.get(dto.getSourceId());
                if (StringUtils.hasText(sourceName)) {
                    dto.setSourceView(sourceName);
                }
            }
            if (dto.getPersonalId() != null && dto.getPersonalId() != 0) {
                String personalName = userExamCategoryMap.get(dto.getPersonalId());
                if (StringUtils.hasText(personalName)) {
                    dto.setPersonalName(personalName);
                }
            }
        }
        return Result.success(respPage);
    }

    public Result<String> isPutaway(ExamInfoAuditPutWayREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        ExamInfoAudit record = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (dao.updateById(record) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");

    }

    public Result<ExamInfoAuditViewRESP> view(Long id) {
        if (id == null) {
            return Result.error("id不能为空");
        }
        ExamInfoAudit record = dao.getById(id);
        if (ObjectUtils.isEmpty(record)) {
            return Result.error("id不正确");
        }
        ExamInfoAuditViewRESP resp = BeanUtil.copyProperties(record, ExamInfoAuditViewRESP.class);
        return Result.success(resp);
    }

    public Result<String> update(ExamInfoAuditEditREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        // 价格处理
        // 原价为0，试卷也是免费
        if (IsFreeEnum.FREE.getCode().equals(req.getIsFree())) {
            // 免费就设置价格为0(原价、优惠价)
            req.setOrgPrice(BigDecimal.valueOf(0));
            req.setFabPrice(BigDecimal.valueOf(0));
        } else {
            // 收费但价格为空
            if (req.getOrgPrice() == null) {
                return Result.error("请输入价格");
            }
            if (req.getFabPrice() == null) {
                req.setFabPrice(BigDecimal.valueOf(0));
            }
            // 原价小于0
            if (req.getOrgPrice().compareTo(BigDecimal.valueOf(0)) < 0 || req.getFabPrice().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("价格不能为负数");
            }
            if (req.getFabPrice().compareTo(req.getOrgPrice()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }
        ExamInfoAudit examInfoAudit = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        examInfoAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (dao.updateById(examInfoAudit) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    public Result<String> updateStatus(ExamInfoAuditUpdateStatusREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
            return Result.error("状态不正确");
        }
        ExamInfoAudit record = BeanUtil.copyProperties(req, ExamInfoAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (dao.updateById(record) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }

    /**
     * 试卷：全审核 审核成功：审核表置为审核通过，复制信息到实体表（更新和生成实体表） 审核不通过：审核表置为审核不通过，不做其他操作
     *
     * @param req 审核参数
     * @return 审核结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> audit(ExamInfoAuditToAuditREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!AuditStatusEnum.SUCCESS.getCode().equals(req.getAuditStatus()) && !AuditStatusEnum.FAIL.getCode().equals(req.getAuditStatus())) {
            throw new BaseException("审核状态不正确");
        }
        ExamInfoAudit examInfoAudit = dao.getById(req.getId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            throw new BaseException("找不到试卷信息");
        }

        // 审核通过，同步审核表到实体表，删除再插入
        if (AuditStatusEnum.SUCCESS.getCode().equals(req.getAuditStatus())) {
            ExamInfo examInfo = examInfoDao.getById(req.getId());
            ExamInfo exam = BeanUtil.copyProperties(examInfoAudit, ExamInfo.class);
            if (ObjectUtil.isNull(examInfo)) {
                //同步试卷
                examInfoDao.save(exam);
            } else {
                examInfoDao.updateById(exam);
            }
            examTitleDao.deleteByExamId(examInfoAudit.getId());
            problemRefDao.deleteByExamId(examInfoAudit.getId());
            // 同步标题
            List<ExamTitleAudit> examTitleAuditList = titleAuditDao.listByExamId(examInfoAudit.getId());
            if (CollectionUtil.isEmpty(examTitleAuditList)) {
                return Result.error("试卷未添加标题");
            } else {
                for (ExamTitleAudit examTitleAudit : examTitleAuditList) {
                    examTitleDao.save(BeanUtil.copyProperties(examTitleAudit, ExamTitle.class));
                }
            }
            // 同步关联标题关联试题信息
            List<ExamTitleProblemRefAudit> examTitleProblemRefAuditList = problemRefAuditDao.listByExamId(examInfoAudit.getId());
            if (CollectionUtil.isEmpty(examTitleProblemRefAuditList)) {
                return Result.error("试卷未添加标题关联试题");
            } else {
                for (ExamTitleProblemRefAudit examTitleProblemRefAudit : examTitleProblemRefAuditList) {
                    problemRefDao.save(BeanUtil.copyProperties(examTitleProblemRefAudit, ExamTitleProblemRef.class));
                }
            }
        }
        // 更新审核表
        examInfoAudit.setAuditStatus(req.getAuditStatus());
        examInfoAudit.setAuditOpinion(StringUtils.isEmpty(req.getAuditOpinion()) ? "" : req.getAuditOpinion());
        int resultNum = dao.updateById(examInfoAudit);
        if (resultNum > 0) {
            // 插入es或者更新es
            try {
                EsExam esExam = BeanUtil.copyProperties(examInfoAudit, EsExam.class);
                IndexQuery query = new IndexQueryBuilder().withObject(esExam).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsExam.EXAM));
                CALLBACK_EXECUTOR.execute(new MsgSend(examInfoAudit.getLecturerUserNo(), examInfoAudit.getExamName(), req.getAuditStatus(), req.getAuditOpinion()));
                return Result.success("审核成功");
            } catch (Exception e) {
                logger.warn("elasticsearch更新数据失败", e);
                throw new BaseException("操作失败");
            }
        }

        return Result.error("操作失败");

    }

    /**
     * 推送站内信
     */
    private void pushMsgForAuditExam(Long userNo, String examName, Integer auditStatus, String auditOpinion) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(userNo);

        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        if (AuditStatusEnum.SUCCESS.getCode().equals(auditStatus)) {
            msg.setMsgTitle("《" + examName + "》试卷审核成功");
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM + "\">尊敬的" + lecturerVO.getLecturerName() + "讲师,您的试卷：《" + examName + "》,已通过审核并同步到考试中心。快去看看吧。</a>");
        } else {
            msg.setMsgTitle("《" + examName + "》试卷审核失败");
            msg.setMsgText("<a href=\"" + SysConfigConstants.EXAM_ADMIN + "\">尊敬的" + lecturerVO.getLecturerName() + "讲师,您的试卷：《" + examName + "》,未通过审核,审核意见:" + auditOpinion + "</a>");
        }
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("试卷审核站内推送信");
        msg.setSendTime(null);
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(userNo);
        userMsg.setMobile(lecturerVO.getLecturerMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);
        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);
    }

    /**
     * 异步发送通知讲师消息
     */
    class MsgSend implements Runnable {
        private final Long userNo;
        private final String examName;
        private final Integer auditStatus;
        private final String auditOpinion;

        public MsgSend(Long userNo, String examName, Integer auditStatus, String auditOpinion) {
            this.userNo = userNo;
            this.examName = examName;
            this.auditStatus = auditStatus;
            this.auditOpinion = auditOpinion;
        }

        @Override
        public void run() {
            try {
                pushMsgForAuditExam(userNo, examName, auditStatus, auditOpinion);
            } catch (Exception e) {
                logger.error("异步更新时长失败", e);
            }
        }

    }
}
