package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamTitle;
import com.roncoo.education.exam.feign.qo.ExamTitleQO;
import com.roncoo.education.exam.feign.vo.ExamTitleVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamTitleBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamTitleController extends BaseController implements IFeignExamTitle{

    @Autowired
    private FeignExamTitleBiz biz;

	@Override
	public Page<ExamTitleVO> listForPage(@RequestBody ExamTitleQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamTitleQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamTitleQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamTitleVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
