package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 试卷标题题目关联审核
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamTitleProblemRefAuditSaveBO", description = "试卷标题题目关联审核")
public class AuthExamTitleProblemRefAuditSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题试题关联ID，更新操作，必传原关联ID")
    private Long refId = null;

    @NotNull(message = "题目ID不能为空")
    @ApiModelProperty(value = "题目ID",required = true)
    private Long problemId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "分值")
    private Integer score;

    @ApiModelProperty(value = "小题")
    private List<AuthExamTitleProblemRefAuditSaveBO> childrenList;
}
