package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.CollectionTypeEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionDeleteBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemCollectionPageBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemCollectionPageDTO;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.ExamProblemCollectionDao;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 试卷题目收藏
 *
 * @author wujing
 */
@Component
public class AuthExamProblemCollectionBiz extends BaseBiz {

    @Autowired
    private ExamProblemCollectionDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;

    /**
     * 试卷收藏分页列表接口
     *
     * @param bo 收藏分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthExamProblemCollectionPageDTO>> list(AuthExamProblemCollectionPageBO bo) {
        ExamProblemCollectionExample example = new ExamProblemCollectionExample();
        ExamProblemCollectionExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        c.andCollectionTypeEqualTo(bo.getCollectionType());
        if (ObjectUtil.isNotNull(bo.getProblemType())) {
            c.andProblemTypeEqualTo(bo.getProblemType());
        }
        example.setOrderByClause(" id desc ");
        Page<ExamProblemCollection> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthExamProblemCollectionPageDTO> listForPage = PageUtil.transform(page, AuthExamProblemCollectionPageDTO.class);
        for (AuthExamProblemCollectionPageDTO dto : listForPage.getList()) {
            if (dto.getCollectionType().equals(CollectionTypeEnum.EXAM.getCode())) {
                ExamInfo examInfo = examInfoDao.getById(dto.getCollectionId());
                if (ObjectUtil.isNull(examInfo)) {
                    continue;
                }

                dto.setCollectionName(examInfo.getExamName());
                dto.setStudyCount(examInfo.getStudyCount());
                dto.setDownloadCount(examInfo.getDownloadCount());

                // 获取年级名称
                ExamCategory graCategory = examCategoryDao.getById(examInfo.getGraId());
                if (ObjectUtil.isNotEmpty(graCategory)) {
                    dto.setGraName(graCategory.getCategoryName());
                }
                // 获取科目名称
                ExamCategory subjectCategory = examCategoryDao.getById(examInfo.getSubjectId());
                if (ObjectUtil.isNotEmpty(subjectCategory)) {
                    dto.setSubjectName(subjectCategory.getCategoryName());
                }
                // 年份ID
                ExamCategory yearCategory = examCategoryDao.getById(examInfo.getYearId());
                if (ObjectUtil.isNotEmpty(yearCategory)) {
                    dto.setYearName(yearCategory.getCategoryName());
                }
                //获取来源名称
                ExamCategory sourceCategory = examCategoryDao.getById(examInfo.getSourceId());
                if (ObjectUtil.isNotEmpty(sourceCategory)) {
                    dto.setSourceName(sourceCategory.getCategoryName());
                }
            } else if (dto.getCollectionType().equals(CollectionTypeEnum.TITLE.getCode())) {
                ExamProblem problem = examProblemDao.getById(dto.getCollectionId());
                if (ObjectUtil.isNotNull(problem)) {
                    dto.setCollectionName(problem.getProblemContent());
                }
            }
        }
        return Result.success(listForPage);
    }

    /**
     * 试卷收藏信息接口
     *
     * @param bo 收藏参数
     * @return 收藏结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> collect(AuthExamProblemCollectionBO bo) {
        logger.warn("收藏接收参数：{}", JSON.toJSONString(bo));
        // 根据userNo和试卷Id找用户试卷收藏信息
        ExamProblemCollection subjectCollect = dao.getByUserNoAndCollectionIdAndCollectionType(ThreadContext.userNo(), bo.getCollectionId(), bo.getCollectionType());
        // 如果已经收藏过,则直接返回
        if (ObjectUtil.isNotNull(subjectCollect)) {
            return Result.error(ResultEnum.EXAM_COLLECTION);
        }
        // 收藏试卷
        if (bo.getCollectionType().equals(CollectionTypeEnum.EXAM.getCode())) {
            // 试卷中的收藏数+1
            ExamInfo examInfo = examInfoDao.getById(bo.getCollectionId());
            if (ObjectUtil.isNull(examInfo)) {
                return Result.error("找不到试卷");
            }
            examInfo.setCollectionCount(examInfo.getCollectionCount() + 1);
            examInfoDao.updateById(examInfo);
        } else if (bo.getCollectionType().equals(CollectionTypeEnum.TITLE.getCode())) {
            // 收藏题目
            // 试题中的收藏数+1
            ExamProblem examProblem = examProblemDao.getById(bo.getCollectionId());
            if (ObjectUtil.isNull(examProblem)) {
                return Result.error("找不到试题");
            }
            examProblem.setCollectionCount(examProblem.getCollectionCount() + 1);
            examProblemDao.updateById(examProblem);
        }
        // 保存用户收藏信息
        ExamProblemCollection collection = new ExamProblemCollection();
        collection.setCollectionType(bo.getCollectionType());
        collection.setUserNo(ThreadContext.userNo());
        collection.setCollectionId(bo.getCollectionId());

        if (dao.save(collection) > 0) {
            return Result.success("收藏成功");
        }
        return Result.error(ResultEnum.EXAM_COLLECTION_FAIL);
    }

    /**
     * 删除试卷收藏信息
     *
     * @param bo 删除收藏记录
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(AuthExamProblemCollectionDeleteBO bo) {
        ExamProblemCollection examProblemCollection = dao.getByUserNoAndCollectionIdAndCollectionType(ThreadContext.userNo(), bo.getCollectionId(), bo.getCollectionType());
        if (ObjectUtil.isNull(examProblemCollection)) {
            return Result.error("收藏记录不存在");
        }

        // 用取消试卷
        if (examProblemCollection.getCollectionType().equals(CollectionTypeEnum.EXAM.getCode())) {
            // 试卷中的收藏数-1
            ExamInfo examInfo = examInfoDao.getById(examProblemCollection.getCollectionId());
            if (ObjectUtil.isNull(examInfo)) {
                return Result.error("找不到试卷信息");
            }
            examInfo.setCollectionCount(examInfo.getCollectionCount() - 1);
            examInfoDao.updateById(examInfo);
        } else if (examProblemCollection.getCollectionType().equals(CollectionTypeEnum.TITLE.getCode())) {
            // 如果收藏类型是题目,直接查找试卷用户收藏信息
            // 题目中的收藏数-1
            ExamProblem examProblem = examProblemDao.getById(examProblemCollection.getCollectionId());
            if (ObjectUtil.isNull(examProblem)) {
                return Result.error("找不到题目");
            }
            examProblem.setCollectionCount(examProblem.getCollectionCount() - 1);
            examProblemDao.updateById(examProblem);
        }

        // 根据用户编号、收藏ID删除收藏信息
        dao.deleteByUserNoAndCollectionId(ThreadContext.userNo(), examProblemCollection.getCollectionId());

        return Result.success("删除成功");
    }

}
