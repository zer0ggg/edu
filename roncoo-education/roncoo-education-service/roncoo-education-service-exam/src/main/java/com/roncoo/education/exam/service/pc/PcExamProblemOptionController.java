package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamProblemOptionEditREQ;
import com.roncoo.education.exam.common.req.ExamProblemOptionListREQ;
import com.roncoo.education.exam.common.req.ExamProblemOptionSaveREQ;
import com.roncoo.education.exam.common.resp.ExamProblemOptionListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemOptionViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamProblemOptionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷题目选项 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷题目选项管理")
@RestController
@RequestMapping("/exam/pc/exam/problem/option")
public class PcExamProblemOptionController {

    @Autowired
    private PcExamProblemOptionBiz biz;

    @ApiOperation(value = "试卷题目选项列表", notes = "试卷题目选项列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamProblemOptionListRESP>> list(@RequestBody ExamProblemOptionListREQ examProblemOptionListREQ) {
        return biz.list(examProblemOptionListREQ);
    }

    @ApiOperation(value = "试卷题目选项添加", notes = "试卷题目选项添加")
    @SysLog(value = "试卷题目选项添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamProblemOptionSaveREQ examProblemOptionSaveREQ) {
        return biz.save(examProblemOptionSaveREQ);
    }

    @ApiOperation(value = "试卷题目选项查看", notes = "试卷题目选项查看")
    @GetMapping(value = "/view")
    public Result<ExamProblemOptionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷题目选项修改", notes = "试卷题目选项修改")
    @SysLog(value = "试卷题目选项修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamProblemOptionEditREQ examProblemOptionEditREQ) {
        return biz.edit(examProblemOptionEditREQ);
    }

    @ApiOperation(value = "试卷题目选项删除", notes = "试卷题目选项删除")
    @SysLog(value = "试卷题目选项删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
