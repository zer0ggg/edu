package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamAnswerMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamAnswerExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户考试答案 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamAnswerDaoImpl implements UserExamAnswerDao {

    @Autowired
    private UserExamAnswerMapper mapper;

    @Override
    public int save(UserExamAnswer record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExamAnswer record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExamAnswer getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserExamAnswer> listForPage(int pageCurrent, int pageSize, UserExamAnswerExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserExamAnswer getByRecordIdAndTitleIdAndProblemId(Long recordId, Long titleId, Long problemId) {
        UserExamAnswerExample example = new UserExamAnswerExample();
        UserExamAnswerExample.Criteria c = example.createCriteria();
        c.andRecordIdEqualTo(recordId);
        c.andTitleIdEqualTo(titleId);
        c.andProblemIdEqualTo(problemId);
        List<UserExamAnswer> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<UserExamAnswer> listByRecordId(Long recordId) {
        UserExamAnswerExample example = new UserExamAnswerExample();
        UserExamAnswerExample.Criteria c = example.createCriteria();
        c.andRecordIdEqualTo(recordId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<UserExamAnswer> listByRecordIdAndTitleId(Long recordId, Long titleId) {
        UserExamAnswerExample example = new UserExamAnswerExample();
        UserExamAnswerExample.Criteria c = example.createCriteria();
        c.andRecordIdEqualTo(recordId);
        c.andTitleIdEqualTo(titleId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<UserExamAnswer> getProblemIdAndExamIdAndUserNo(Long problemId, Long examId, Long userNo) {
        UserExamAnswerExample example = new UserExamAnswerExample();
        UserExamAnswerExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        c.andExamIdEqualTo(examId);
        c.andUserNoEqualTo(userNo);
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<UserExamAnswer> getProblemIdAndExamId(Long problemId, Long examId) {
        UserExamAnswerExample example = new UserExamAnswerExample();
        UserExamAnswerExample.Criteria c = example.createCriteria();
        c.andProblemIdEqualTo(problemId);
        c.andExamIdEqualTo(examId);
        return this.mapper.selectByExampleWithBLOBs(example);
    }
}
