package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamTitleScoreEditREQ;
import com.roncoo.education.exam.common.req.UserExamTitleScoreListREQ;
import com.roncoo.education.exam.common.req.UserExamTitleScoreSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamTitleScoreListRESP;
import com.roncoo.education.exam.common.resp.UserExamTitleScoreViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamTitleScoreBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户考试标题得分 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户考试试卷标题得分管理")
@RestController
@RequestMapping("/exam/pc/user/exam/title/score")
public class PcUserExamTitleScoreController {

    @Autowired
    private PcUserExamTitleScoreBiz biz;

    @ApiOperation(value = "用户考试标题得分列表", notes = "用户考试标题得分列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamTitleScoreListRESP>> list(@RequestBody UserExamTitleScoreListREQ userExamTitleScoreListREQ) {
        return biz.list(userExamTitleScoreListREQ);
    }

    @ApiOperation(value = "用户考试标题得分添加", notes = "用户考试标题得分添加")
    @SysLog(value = "用户考试标题得分添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamTitleScoreSaveREQ userExamTitleScoreSaveREQ) {
        return biz.save(userExamTitleScoreSaveREQ);
    }

    @ApiOperation(value = "用户考试标题得分查看", notes = "用户考试标题得分查看")
    @GetMapping(value = "/view")
    public Result<UserExamTitleScoreViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户考试标题得分修改", notes = "用户考试标题得分修改")
    @SysLog(value = "用户考试标题得分修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamTitleScoreEditREQ userExamTitleScoreEditREQ) {
        return biz.edit(userExamTitleScoreEditREQ);
    }

    @ApiOperation(value = "用户考试标题得分删除", notes = "用户考试标题得分删除")
    @SysLog(value = "用户考试标题得分删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
