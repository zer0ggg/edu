package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExamTitleScoreExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public UserExamTitleScoreExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andExamIdIsNull() {
            addCriterion("exam_id is null");
            return (Criteria) this;
        }

        public Criteria andExamIdIsNotNull() {
            addCriterion("exam_id is not null");
            return (Criteria) this;
        }

        public Criteria andExamIdEqualTo(Long value) {
            addCriterion("exam_id =", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotEqualTo(Long value) {
            addCriterion("exam_id <>", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdGreaterThan(Long value) {
            addCriterion("exam_id >", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdGreaterThanOrEqualTo(Long value) {
            addCriterion("exam_id >=", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdLessThan(Long value) {
            addCriterion("exam_id <", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdLessThanOrEqualTo(Long value) {
            addCriterion("exam_id <=", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdIn(List<Long> values) {
            addCriterion("exam_id in", values, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotIn(List<Long> values) {
            addCriterion("exam_id not in", values, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdBetween(Long value1, Long value2) {
            addCriterion("exam_id between", value1, value2, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotBetween(Long value1, Long value2) {
            addCriterion("exam_id not between", value1, value2, "examId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNull() {
            addCriterion("record_id is null");
            return (Criteria) this;
        }

        public Criteria andRecordIdIsNotNull() {
            addCriterion("record_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecordIdEqualTo(Long value) {
            addCriterion("record_id =", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotEqualTo(Long value) {
            addCriterion("record_id <>", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThan(Long value) {
            addCriterion("record_id >", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdGreaterThanOrEqualTo(Long value) {
            addCriterion("record_id >=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThan(Long value) {
            addCriterion("record_id <", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdLessThanOrEqualTo(Long value) {
            addCriterion("record_id <=", value, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdIn(List<Long> values) {
            addCriterion("record_id in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotIn(List<Long> values) {
            addCriterion("record_id not in", values, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdBetween(Long value1, Long value2) {
            addCriterion("record_id between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andRecordIdNotBetween(Long value1, Long value2) {
            addCriterion("record_id not between", value1, value2, "recordId");
            return (Criteria) this;
        }

        public Criteria andTitleIdIsNull() {
            addCriterion("title_id is null");
            return (Criteria) this;
        }

        public Criteria andTitleIdIsNotNull() {
            addCriterion("title_id is not null");
            return (Criteria) this;
        }

        public Criteria andTitleIdEqualTo(Long value) {
            addCriterion("title_id =", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdNotEqualTo(Long value) {
            addCriterion("title_id <>", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdGreaterThan(Long value) {
            addCriterion("title_id >", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("title_id >=", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdLessThan(Long value) {
            addCriterion("title_id <", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdLessThanOrEqualTo(Long value) {
            addCriterion("title_id <=", value, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdIn(List<Long> values) {
            addCriterion("title_id in", values, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdNotIn(List<Long> values) {
            addCriterion("title_id not in", values, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdBetween(Long value1, Long value2) {
            addCriterion("title_id between", value1, value2, "titleId");
            return (Criteria) this;
        }

        public Criteria andTitleIdNotBetween(Long value1, Long value2) {
            addCriterion("title_id not between", value1, value2, "titleId");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIsNull() {
            addCriterion("sys_audit_total_score is null");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIsNotNull() {
            addCriterion("sys_audit_total_score is not null");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreEqualTo(Integer value) {
            addCriterion("sys_audit_total_score =", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotEqualTo(Integer value) {
            addCriterion("sys_audit_total_score <>", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreGreaterThan(Integer value) {
            addCriterion("sys_audit_total_score >", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_total_score >=", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreLessThan(Integer value) {
            addCriterion("sys_audit_total_score <", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreLessThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_total_score <=", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIn(List<Integer> values) {
            addCriterion("sys_audit_total_score in", values, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotIn(List<Integer> values) {
            addCriterion("sys_audit_total_score not in", values, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_total_score between", value1, value2, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_total_score not between", value1, value2, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIsNull() {
            addCriterion("sys_audit_score is null");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIsNotNull() {
            addCriterion("sys_audit_score is not null");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreEqualTo(Integer value) {
            addCriterion("sys_audit_score =", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotEqualTo(Integer value) {
            addCriterion("sys_audit_score <>", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreGreaterThan(Integer value) {
            addCriterion("sys_audit_score >", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_score >=", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreLessThan(Integer value) {
            addCriterion("sys_audit_score <", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreLessThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_score <=", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIn(List<Integer> values) {
            addCriterion("sys_audit_score in", values, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotIn(List<Integer> values) {
            addCriterion("sys_audit_score not in", values, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_score between", value1, value2, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_score not between", value1, value2, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreIsNull() {
            addCriterion("handwork_audit_total_score is null");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreIsNotNull() {
            addCriterion("handwork_audit_total_score is not null");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreEqualTo(Integer value) {
            addCriterion("handwork_audit_total_score =", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreNotEqualTo(Integer value) {
            addCriterion("handwork_audit_total_score <>", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreGreaterThan(Integer value) {
            addCriterion("handwork_audit_total_score >", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("handwork_audit_total_score >=", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreLessThan(Integer value) {
            addCriterion("handwork_audit_total_score <", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreLessThanOrEqualTo(Integer value) {
            addCriterion("handwork_audit_total_score <=", value, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreIn(List<Integer> values) {
            addCriterion("handwork_audit_total_score in", values, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreNotIn(List<Integer> values) {
            addCriterion("handwork_audit_total_score not in", values, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreBetween(Integer value1, Integer value2) {
            addCriterion("handwork_audit_total_score between", value1, value2, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditTotalScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("handwork_audit_total_score not between", value1, value2, "handworkAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreIsNull() {
            addCriterion("handwork_audit_score is null");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreIsNotNull() {
            addCriterion("handwork_audit_score is not null");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreEqualTo(Integer value) {
            addCriterion("handwork_audit_score =", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreNotEqualTo(Integer value) {
            addCriterion("handwork_audit_score <>", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreGreaterThan(Integer value) {
            addCriterion("handwork_audit_score >", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("handwork_audit_score >=", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreLessThan(Integer value) {
            addCriterion("handwork_audit_score <", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreLessThanOrEqualTo(Integer value) {
            addCriterion("handwork_audit_score <=", value, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreIn(List<Integer> values) {
            addCriterion("handwork_audit_score in", values, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreNotIn(List<Integer> values) {
            addCriterion("handwork_audit_score not in", values, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreBetween(Integer value1, Integer value2) {
            addCriterion("handwork_audit_score between", value1, value2, "handworkAuditScore");
            return (Criteria) this;
        }

        public Criteria andHandworkAuditScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("handwork_audit_score not between", value1, value2, "handworkAuditScore");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}