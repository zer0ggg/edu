package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PracticeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public PracticeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNull() {
            addCriterion("subject_id is null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIsNotNull() {
            addCriterion("subject_id is not null");
            return (Criteria) this;
        }

        public Criteria andSubjectIdEqualTo(Long value) {
            addCriterion("subject_id =", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotEqualTo(Long value) {
            addCriterion("subject_id <>", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThan(Long value) {
            addCriterion("subject_id >", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdGreaterThanOrEqualTo(Long value) {
            addCriterion("subject_id >=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThan(Long value) {
            addCriterion("subject_id <", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdLessThanOrEqualTo(Long value) {
            addCriterion("subject_id <=", value, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdIn(List<Long> values) {
            addCriterion("subject_id in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotIn(List<Long> values) {
            addCriterion("subject_id not in", values, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdBetween(Long value1, Long value2) {
            addCriterion("subject_id between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andSubjectIdNotBetween(Long value1, Long value2) {
            addCriterion("subject_id not between", value1, value2, "subjectId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdIsNull() {
            addCriterion("difficulty_id is null");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdIsNotNull() {
            addCriterion("difficulty_id is not null");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdEqualTo(Long value) {
            addCriterion("difficulty_id =", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdNotEqualTo(Long value) {
            addCriterion("difficulty_id <>", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdGreaterThan(Long value) {
            addCriterion("difficulty_id >", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdGreaterThanOrEqualTo(Long value) {
            addCriterion("difficulty_id >=", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdLessThan(Long value) {
            addCriterion("difficulty_id <", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdLessThanOrEqualTo(Long value) {
            addCriterion("difficulty_id <=", value, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdIn(List<Long> values) {
            addCriterion("difficulty_id in", values, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdNotIn(List<Long> values) {
            addCriterion("difficulty_id not in", values, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdBetween(Long value1, Long value2) {
            addCriterion("difficulty_id between", value1, value2, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andDifficultyIdNotBetween(Long value1, Long value2) {
            addCriterion("difficulty_id not between", value1, value2, "difficultyId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNull() {
            addCriterion("source_id is null");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNotNull() {
            addCriterion("source_id is not null");
            return (Criteria) this;
        }

        public Criteria andSourceIdEqualTo(Long value) {
            addCriterion("source_id =", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotEqualTo(Long value) {
            addCriterion("source_id <>", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThan(Long value) {
            addCriterion("source_id >", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThanOrEqualTo(Long value) {
            addCriterion("source_id >=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThan(Long value) {
            addCriterion("source_id <", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThanOrEqualTo(Long value) {
            addCriterion("source_id <=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIn(List<Long> values) {
            addCriterion("source_id in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotIn(List<Long> values) {
            addCriterion("source_id not in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdBetween(Long value1, Long value2) {
            addCriterion("source_id between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotBetween(Long value1, Long value2) {
            addCriterion("source_id not between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andYearIdIsNull() {
            addCriterion("year_id is null");
            return (Criteria) this;
        }

        public Criteria andYearIdIsNotNull() {
            addCriterion("year_id is not null");
            return (Criteria) this;
        }

        public Criteria andYearIdEqualTo(Long value) {
            addCriterion("year_id =", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdNotEqualTo(Long value) {
            addCriterion("year_id <>", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdGreaterThan(Long value) {
            addCriterion("year_id >", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdGreaterThanOrEqualTo(Long value) {
            addCriterion("year_id >=", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdLessThan(Long value) {
            addCriterion("year_id <", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdLessThanOrEqualTo(Long value) {
            addCriterion("year_id <=", value, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdIn(List<Long> values) {
            addCriterion("year_id in", values, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdNotIn(List<Long> values) {
            addCriterion("year_id not in", values, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdBetween(Long value1, Long value2) {
            addCriterion("year_id between", value1, value2, "yearId");
            return (Criteria) this;
        }

        public Criteria andYearIdNotBetween(Long value1, Long value2) {
            addCriterion("year_id not between", value1, value2, "yearId");
            return (Criteria) this;
        }

        public Criteria andRegionIsNull() {
            addCriterion("region is null");
            return (Criteria) this;
        }

        public Criteria andRegionIsNotNull() {
            addCriterion("region is not null");
            return (Criteria) this;
        }

        public Criteria andRegionEqualTo(String value) {
            addCriterion("region =", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotEqualTo(String value) {
            addCriterion("region <>", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionGreaterThan(String value) {
            addCriterion("region >", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionGreaterThanOrEqualTo(String value) {
            addCriterion("region >=", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionLessThan(String value) {
            addCriterion("region <", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionLessThanOrEqualTo(String value) {
            addCriterion("region <=", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionLike(String value) {
            addCriterion("region like", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotLike(String value) {
            addCriterion("region not like", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionIn(List<String> values) {
            addCriterion("region in", values, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotIn(List<String> values) {
            addCriterion("region not in", values, "region");
            return (Criteria) this;
        }

        public Criteria andRegionBetween(String value1, String value2) {
            addCriterion("region between", value1, value2, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotBetween(String value1, String value2) {
            addCriterion("region not between", value1, value2, "region");
            return (Criteria) this;
        }

        public Criteria andEmphasisIsNull() {
            addCriterion("emphasis is null");
            return (Criteria) this;
        }

        public Criteria andEmphasisIsNotNull() {
            addCriterion("emphasis is not null");
            return (Criteria) this;
        }

        public Criteria andEmphasisEqualTo(String value) {
            addCriterion("emphasis =", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisNotEqualTo(String value) {
            addCriterion("emphasis <>", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisGreaterThan(String value) {
            addCriterion("emphasis >", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisGreaterThanOrEqualTo(String value) {
            addCriterion("emphasis >=", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisLessThan(String value) {
            addCriterion("emphasis <", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisLessThanOrEqualTo(String value) {
            addCriterion("emphasis <=", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisLike(String value) {
            addCriterion("emphasis like", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisNotLike(String value) {
            addCriterion("emphasis not like", value, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisIn(List<String> values) {
            addCriterion("emphasis in", values, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisNotIn(List<String> values) {
            addCriterion("emphasis not in", values, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisBetween(String value1, String value2) {
            addCriterion("emphasis between", value1, value2, "emphasis");
            return (Criteria) this;
        }

        public Criteria andEmphasisNotBetween(String value1, String value2) {
            addCriterion("emphasis not between", value1, value2, "emphasis");
            return (Criteria) this;
        }

        public Criteria andTopicIdIsNull() {
            addCriterion("topic_id is null");
            return (Criteria) this;
        }

        public Criteria andTopicIdIsNotNull() {
            addCriterion("topic_id is not null");
            return (Criteria) this;
        }

        public Criteria andTopicIdEqualTo(Long value) {
            addCriterion("topic_id =", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdNotEqualTo(Long value) {
            addCriterion("topic_id <>", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdGreaterThan(Long value) {
            addCriterion("topic_id >", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdGreaterThanOrEqualTo(Long value) {
            addCriterion("topic_id >=", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdLessThan(Long value) {
            addCriterion("topic_id <", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdLessThanOrEqualTo(Long value) {
            addCriterion("topic_id <=", value, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdIn(List<Long> values) {
            addCriterion("topic_id in", values, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdNotIn(List<Long> values) {
            addCriterion("topic_id not in", values, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdBetween(Long value1, Long value2) {
            addCriterion("topic_id between", value1, value2, "topicId");
            return (Criteria) this;
        }

        public Criteria andTopicIdNotBetween(Long value1, Long value2) {
            addCriterion("topic_id not between", value1, value2, "topicId");
            return (Criteria) this;
        }

        public Criteria andGraIdIsNull() {
            addCriterion("gra_id is null");
            return (Criteria) this;
        }

        public Criteria andGraIdIsNotNull() {
            addCriterion("gra_id is not null");
            return (Criteria) this;
        }

        public Criteria andGraIdEqualTo(Long value) {
            addCriterion("gra_id =", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdNotEqualTo(Long value) {
            addCriterion("gra_id <>", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdGreaterThan(Long value) {
            addCriterion("gra_id >", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdGreaterThanOrEqualTo(Long value) {
            addCriterion("gra_id >=", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdLessThan(Long value) {
            addCriterion("gra_id <", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdLessThanOrEqualTo(Long value) {
            addCriterion("gra_id <=", value, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdIn(List<Long> values) {
            addCriterion("gra_id in", values, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdNotIn(List<Long> values) {
            addCriterion("gra_id not in", values, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdBetween(Long value1, Long value2) {
            addCriterion("gra_id between", value1, value2, "graId");
            return (Criteria) this;
        }

        public Criteria andGraIdNotBetween(Long value1, Long value2) {
            addCriterion("gra_id not between", value1, value2, "graId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdIsNull() {
            addCriterion("emphasis_id is null");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdIsNotNull() {
            addCriterion("emphasis_id is not null");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdEqualTo(Long value) {
            addCriterion("emphasis_id =", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdNotEqualTo(Long value) {
            addCriterion("emphasis_id <>", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdGreaterThan(Long value) {
            addCriterion("emphasis_id >", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdGreaterThanOrEqualTo(Long value) {
            addCriterion("emphasis_id >=", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdLessThan(Long value) {
            addCriterion("emphasis_id <", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdLessThanOrEqualTo(Long value) {
            addCriterion("emphasis_id <=", value, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdIn(List<Long> values) {
            addCriterion("emphasis_id in", values, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdNotIn(List<Long> values) {
            addCriterion("emphasis_id not in", values, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdBetween(Long value1, Long value2) {
            addCriterion("emphasis_id between", value1, value2, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andEmphasisIdNotBetween(Long value1, Long value2) {
            addCriterion("emphasis_id not between", value1, value2, "emphasisId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumIsNull() {
            addCriterion("the_radio_num is null");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumIsNotNull() {
            addCriterion("the_radio_num is not null");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumEqualTo(Integer value) {
            addCriterion("the_radio_num =", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumNotEqualTo(Integer value) {
            addCriterion("the_radio_num <>", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumGreaterThan(Integer value) {
            addCriterion("the_radio_num >", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("the_radio_num >=", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumLessThan(Integer value) {
            addCriterion("the_radio_num <", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumLessThanOrEqualTo(Integer value) {
            addCriterion("the_radio_num <=", value, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumIn(List<Integer> values) {
            addCriterion("the_radio_num in", values, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumNotIn(List<Integer> values) {
            addCriterion("the_radio_num not in", values, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumBetween(Integer value1, Integer value2) {
            addCriterion("the_radio_num between", value1, value2, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andTheRadioNumNotBetween(Integer value1, Integer value2) {
            addCriterion("the_radio_num not between", value1, value2, "theRadioNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumIsNull() {
            addCriterion("multiple_choice_num is null");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumIsNotNull() {
            addCriterion("multiple_choice_num is not null");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumEqualTo(Integer value) {
            addCriterion("multiple_choice_num =", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumNotEqualTo(Integer value) {
            addCriterion("multiple_choice_num <>", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumGreaterThan(Integer value) {
            addCriterion("multiple_choice_num >", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("multiple_choice_num >=", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumLessThan(Integer value) {
            addCriterion("multiple_choice_num <", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumLessThanOrEqualTo(Integer value) {
            addCriterion("multiple_choice_num <=", value, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumIn(List<Integer> values) {
            addCriterion("multiple_choice_num in", values, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumNotIn(List<Integer> values) {
            addCriterion("multiple_choice_num not in", values, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumBetween(Integer value1, Integer value2) {
            addCriterion("multiple_choice_num between", value1, value2, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andMultipleChoiceNumNotBetween(Integer value1, Integer value2) {
            addCriterion("multiple_choice_num not between", value1, value2, "multipleChoiceNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumIsNull() {
            addCriterion("estimate_num is null");
            return (Criteria) this;
        }

        public Criteria andEstimateNumIsNotNull() {
            addCriterion("estimate_num is not null");
            return (Criteria) this;
        }

        public Criteria andEstimateNumEqualTo(Integer value) {
            addCriterion("estimate_num =", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumNotEqualTo(Integer value) {
            addCriterion("estimate_num <>", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumGreaterThan(Integer value) {
            addCriterion("estimate_num >", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("estimate_num >=", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumLessThan(Integer value) {
            addCriterion("estimate_num <", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumLessThanOrEqualTo(Integer value) {
            addCriterion("estimate_num <=", value, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumIn(List<Integer> values) {
            addCriterion("estimate_num in", values, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumNotIn(List<Integer> values) {
            addCriterion("estimate_num not in", values, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumBetween(Integer value1, Integer value2) {
            addCriterion("estimate_num between", value1, value2, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andEstimateNumNotBetween(Integer value1, Integer value2) {
            addCriterion("estimate_num not between", value1, value2, "estimateNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumIsNull() {
            addCriterion("gap_filling_num is null");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumIsNotNull() {
            addCriterion("gap_filling_num is not null");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumEqualTo(Integer value) {
            addCriterion("gap_filling_num =", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumNotEqualTo(Integer value) {
            addCriterion("gap_filling_num <>", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumGreaterThan(Integer value) {
            addCriterion("gap_filling_num >", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("gap_filling_num >=", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumLessThan(Integer value) {
            addCriterion("gap_filling_num <", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumLessThanOrEqualTo(Integer value) {
            addCriterion("gap_filling_num <=", value, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumIn(List<Integer> values) {
            addCriterion("gap_filling_num in", values, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumNotIn(List<Integer> values) {
            addCriterion("gap_filling_num not in", values, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumBetween(Integer value1, Integer value2) {
            addCriterion("gap_filling_num between", value1, value2, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andGapFillingNumNotBetween(Integer value1, Integer value2) {
            addCriterion("gap_filling_num not between", value1, value2, "gapFillingNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumIsNull() {
            addCriterion("short_answer_num is null");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumIsNotNull() {
            addCriterion("short_answer_num is not null");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumEqualTo(Integer value) {
            addCriterion("short_answer_num =", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumNotEqualTo(Integer value) {
            addCriterion("short_answer_num <>", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumGreaterThan(Integer value) {
            addCriterion("short_answer_num >", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("short_answer_num >=", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumLessThan(Integer value) {
            addCriterion("short_answer_num <", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumLessThanOrEqualTo(Integer value) {
            addCriterion("short_answer_num <=", value, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumIn(List<Integer> values) {
            addCriterion("short_answer_num in", values, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumNotIn(List<Integer> values) {
            addCriterion("short_answer_num not in", values, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumBetween(Integer value1, Integer value2) {
            addCriterion("short_answer_num between", value1, value2, "shortAnswerNum");
            return (Criteria) this;
        }

        public Criteria andShortAnswerNumNotBetween(Integer value1, Integer value2) {
            addCriterion("short_answer_num not between", value1, value2, "shortAnswerNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}