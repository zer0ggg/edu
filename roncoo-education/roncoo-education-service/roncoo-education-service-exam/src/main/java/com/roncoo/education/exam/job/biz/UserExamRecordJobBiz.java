package com.roncoo.education.exam.job.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.enums.ExamAnswerStatusEnum;
import com.roncoo.education.common.core.enums.ExamProblemTypeEnum;
import com.roncoo.education.common.core.enums.ExamStatusEnum;
import com.roncoo.education.common.core.enums.ExamSysAuditEnum;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用户考试记录
 *
 * @author LYQ
 */
@Component
public class UserExamRecordJobBiz {

    @Autowired
    private UserExamRecordDao userExamRecordDao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private UserExamTitleScoreDao userExamTitleScoreDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private UserExamAnswerDao userExamAnswerDao;

    @Transactional(rollbackFor = Exception.class)
    public void handleExamClosed(Long recordId) {
        UserExamRecord userExamRecord = userExamRecordDao.getByIdForUpdate(recordId);
        if (ObjectUtil.isNull(userExamRecord)) {
            return;
        }
        ExamInfo examInfo = examInfoDao.getById(userExamRecord.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return;
        }

        List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
        if (CollectionUtil.isNotEmpty(examTitleList)) {
            for (ExamTitle examTitle : examTitleList) {
                UserExamTitleScore userExamTitleScore = handleExamTitleProblem(recordId, examTitle);
                userExamTitleScoreDao.save(userExamTitleScore);

                userExamRecord.setSysAuditTotalScore(userExamRecord.getSysAuditTotalScore() + userExamTitleScore.getSysAuditTotalScore());
                userExamRecord.setSysAuditScore(userExamRecord.getSysAuditScore() + userExamTitleScore.getSysAuditScore());
                userExamRecord.setHandworkAuditTotalScore(userExamRecord.getHandworkAuditTotalScore() + userExamTitleScore.getHandworkAuditTotalScore());
                userExamRecord.setHandworkAuditScore(userExamRecord.getHandworkAuditScore() + userExamTitleScore.getHandworkAuditScore());
            }
            userExamRecord.setExamStatus(userExamRecord.getHandworkAuditTotalScore() <= 0 ? ExamStatusEnum.COMPLETE.getCode() : ExamStatusEnum.FINISH.getCode());
        } else {
            userExamRecord.setExamStatus(ExamStatusEnum.FINISH.getCode());
        }

        userExamRecord.setGmtCreate(null);
        userExamRecord.setGmtModified(null);
        userExamRecordDao.updateById(userExamRecord);
    }

    private UserExamTitleScore handleExamTitleProblem(Long recordId, ExamTitle examTitle) {
        UserExamTitleScore userExamTitleScore = new UserExamTitleScore();
        userExamTitleScore.setExamId(examTitle.getExamId());
        userExamTitleScore.setRecordId(recordId);
        userExamTitleScore.setTitleId(examTitle.getId());
        userExamTitleScore.setScore(0);
        userExamTitleScore.setSysAuditTotalScore(0);
        userExamTitleScore.setSysAuditScore(0);
        userExamTitleScore.setHandworkAuditTotalScore(0);
        userExamTitleScore.setHandworkAuditScore(0);

        // 获取题目
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleId(examTitle.getId());
        if (CollectionUtil.isEmpty(refList)) {
            return userExamTitleScore;
        }
        Set<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toSet());
        List<ExamProblem> problemList = examProblemDao.listInIdList(new ArrayList<>(problemIdList));
        if (CollectionUtil.isEmpty(problemList)) {
            return userExamTitleScore;
        }
        Map<Long, Integer> problemScoreMap = refList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, ExamTitleProblemRef::getScore));

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, examTitle.getId());
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }
        Map<Long, UserExamAnswer> userExamAnswerMap = userExamAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));

        // 评阅
        if (ExamProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
            // 组合题评分
            materialProblemCheck(userExamTitleScore, problemList, userExamAnswerMap, problemScoreMap);
        } else {
            // 其他题目类型评分
            otherProblemCheck(userExamTitleScore, problemList, userExamAnswerMap, problemScoreMap);
        }

        return userExamTitleScore;
    }

    private void materialProblemCheck(UserExamTitleScore userExamTitleScore, List<ExamProblem> problemList, Map<Long, UserExamAnswer> userExamAnswerMap, Map<Long, Integer> problemScoreMap) {
        int sysAuditTotalScore = 0;
        int sysAuditScore = 0;
        int handworkAuditTotalScore = 0;

        List<ExamProblem> materialProblemList = problemList.stream().filter(problem -> ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())).collect(Collectors.toList());
        for (ExamProblem problem : materialProblemList) {
            // 获取小题
            List<ExamProblem> subProblemList = problemList.stream().filter(subProblem -> problem.getId().equals(subProblem.getParentId())).collect(Collectors.toList());
            for (ExamProblem subProblem : subProblemList) {
                // 获取用户答案
                UserExamAnswer userExamAnswer = userExamAnswerMap.get(subProblem.getId());
                // 获取小题分数
                int subProblemScore = problemScoreMap.get(subProblem.getId());

                if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(subProblem.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblem.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(subProblem.getProblemType())) {
                    sysAuditTotalScore += subProblemScore;
                    if (ObjectUtil.isNull(userExamAnswer)) {
                        continue;
                    }

                    UserExamAnswer updateUserExamAnswer = new UserExamAnswer();
                    updateUserExamAnswer.setId(userExamAnswer.getId());
                    updateUserExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                    updateUserExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());

                    if (subProblem.getProblemAnswer().equals(userExamAnswer.getUserAnswer())) {
                        sysAuditScore += subProblemScore;
                        updateUserExamAnswer.setScore(userExamAnswer.getProblemScore());
                    }
                    userExamAnswerDao.updateById(updateUserExamAnswer);
                } else {
                    if (ObjectUtil.isNull(userExamAnswer)) {
                        sysAuditTotalScore += subProblemScore;
                    } else {
                        handworkAuditTotalScore += subProblemScore;
                    }
                }
            }
        }
        userExamTitleScore.setSysAuditTotalScore(sysAuditTotalScore);
        userExamTitleScore.setSysAuditScore(sysAuditScore);
        userExamTitleScore.setHandworkAuditTotalScore(handworkAuditTotalScore);
    }

    private void otherProblemCheck(UserExamTitleScore userExamTitleScore, List<ExamProblem> problemList, Map<Long, UserExamAnswer> userExamAnswerMap, Map<Long, Integer> problemScoreMap) {
        int sysAuditTotalScore = 0;
        int sysAuditScore = 0;
        int handworkAuditTotalScore = 0;

        for (ExamProblem problem : problemList) {
            // 获取用户答案
            UserExamAnswer userExamAnswer = userExamAnswerMap.get(problem.getId());
            // 获取小题分数
            int problemScore = problemScoreMap.get(problem.getId());
            if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
                sysAuditTotalScore += problemScore;
                if (ObjectUtil.isNull(userExamAnswer)) {
                    continue;
                }

                UserExamAnswer updateUserExamAnswer = new UserExamAnswer();
                updateUserExamAnswer.setId(userExamAnswer.getId());
                updateUserExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                updateUserExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());

                if (problem.getProblemAnswer().equals(userExamAnswer.getUserAnswer())) {
                    sysAuditScore += problemScore;
                    updateUserExamAnswer.setScore(userExamAnswer.getProblemScore());
                }
                userExamAnswerDao.updateById(updateUserExamAnswer);
            } else {
                if (ObjectUtil.isNull(userExamAnswer)) {
                    sysAuditTotalScore += problemScore;
                } else {
                    handworkAuditTotalScore += problemScore;
                }
            }
        }
        userExamTitleScore.setSysAuditTotalScore(sysAuditTotalScore);
        userExamTitleScore.setSysAuditScore(sysAuditScore);
        userExamTitleScore.setHandworkAuditTotalScore(handworkAuditTotalScore);
    }
}

