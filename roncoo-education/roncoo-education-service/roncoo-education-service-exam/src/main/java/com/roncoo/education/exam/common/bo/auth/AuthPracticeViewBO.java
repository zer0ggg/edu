package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 获取练习信息
 * </p>
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeViewBO ", description="获取练习信息")
public class AuthPracticeViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    private Long id;
}
