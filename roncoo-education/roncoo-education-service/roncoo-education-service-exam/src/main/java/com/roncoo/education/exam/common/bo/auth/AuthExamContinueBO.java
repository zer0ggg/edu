package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 继续考试
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamContinueBO", description = "继续考试参数")
public class AuthExamContinueBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "考试记录ID不能为空")
    @ApiModelProperty(value = "考试记录ID", required = true)
    private Long recordId;
}
