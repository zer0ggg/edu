package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamInfoMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 试卷信息 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamInfoDaoImpl implements ExamInfoDao {

    @Autowired
    private ExamInfoMapper mapper;

    @Override
    public int save(ExamInfo record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamInfo record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamInfo getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamInfo> listForPage(int pageCurrent, int pageSize, ExamInfoExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int countByExample(ExamInfoExample example) {
        return this.mapper.countByExample(example);
    }

    @Override
    public List<ExamInfo> listByIds(List<Long> examIds) {
        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria c = example.createCriteria();
        c.andIdIn(examIds);
        return this.mapper.selectByExample(example);
    }

    @Override
    public ExamInfo getByIdAndStatusId(Long id,Integer statusId) {
        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria c = example.createCriteria();
        c.andIdEqualTo(id);
        c.andStatusIdEqualTo(statusId);
        List<ExamInfo> list = this.mapper.selectByExample(example);
        return CollectionUtils.isEmpty(list) ? null:list.get(0);
    }

}
