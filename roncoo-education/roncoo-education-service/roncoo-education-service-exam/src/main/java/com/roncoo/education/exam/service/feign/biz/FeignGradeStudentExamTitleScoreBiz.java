package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeStudentExamTitleScoreQO;
import com.roncoo.education.exam.feign.vo.GradeStudentExamTitleScoreVO;
import com.roncoo.education.exam.service.dao.GradeStudentExamTitleScoreDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScoreExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamTitleScoreExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeStudentExamTitleScoreBiz extends BaseBiz {

    @Autowired
    private GradeStudentExamTitleScoreDao dao;

	public Page<GradeStudentExamTitleScoreVO> listForPage(GradeStudentExamTitleScoreQO qo) {
	    GradeStudentExamTitleScoreExample example = new GradeStudentExamTitleScoreExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeStudentExamTitleScore> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeStudentExamTitleScoreVO.class);
	}

	public int save(GradeStudentExamTitleScoreQO qo) {
		GradeStudentExamTitleScore record = BeanUtil.copyProperties(qo, GradeStudentExamTitleScore.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeStudentExamTitleScoreVO getById(Long id) {
		GradeStudentExamTitleScore record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeStudentExamTitleScoreVO.class);
	}

	public int updateById(GradeStudentExamTitleScoreQO qo) {
		GradeStudentExamTitleScore record = BeanUtil.copyProperties(qo, GradeStudentExamTitleScore.class);
		return dao.updateById(record);
	}

}
