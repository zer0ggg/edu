package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.ExcelToolUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemOptionSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthExamProblemSaveBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemOptionViewDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemViewDTO;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.AuthExamProblemOptionViewRESP;
import com.roncoo.education.exam.common.resp.ExamProblemListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemViewRESP;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.ExamProblemOptionDao;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblem;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemExample.Criteria;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 试卷题目
 *
 * @author wujing
 */
@Component
public class PcExamProblemBiz extends BaseBiz {

    @Autowired
    private ExamProblemDao dao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private UserExamCategoryDao userExamCategoryDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    @Autowired
    private IFeignLecturer feignLecturer;

    /**
     * 试卷题目列表
     *
     * @param req 试卷题目分页查询参数
     * @return 试卷题目分页查询结果
     */
    public Result<Page<ExamProblemListRESP>> list(ExamProblemListREQ req) {
        ExamProblemExample example = new ExamProblemExample();
        Criteria c = example.createCriteria();
        if (StrUtil.isNotBlank(req.getProblemContent())) {
            c.andProblemContentLike(PageUtil.like(req.getProblemContent()));
        }
        if (ObjectUtil.isNotNull(req.getProblemType())) {
            c.andProblemTypeEqualTo(req.getProblemType());
        }
        c.andParentIdEqualTo(0L);
        example.setOrderByClause(" status_id desc, sort asc, id desc  ");
        Page<ExamProblem> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ExamProblemListRESP> listForPage = PageUtil.transform(page, ExamProblemListRESP.class);
        if (null == listForPage.getList() || listForPage.getList().isEmpty()) {
            return Result.success(new Page<>());
        }

        Set<Long> lecturerUserNos = listForPage.getList().stream().map(ExamProblemListRESP::getLecturerUserNo).collect(Collectors.toSet());
        LecturerQO lecturerQO = new LecturerQO();
        lecturerQO.setLecturerUserNos(new ArrayList<>(lecturerUserNos));
        List<LecturerVO> lecturerList = feignLecturer.listByLecturerUserNos(lecturerQO);
        if (CollectionUtil.isNotEmpty(lecturerList)) {
            Map<Long, LecturerVO> lecturerMap = lecturerList.stream().collect(Collectors.toMap(LecturerVO::getLecturerUserNo, item -> item));
            listForPage.getList().stream().forEach(item -> {
                LecturerVO lecturerVO = lecturerMap.get(item.getLecturerUserNo());
                if (ObjectUtil.isNotNull(lecturerVO)) {
                    item.setLecturerName(lecturerVO.getLecturerName());
                }
            });
        }

        // 获取分类
        List<ExamCategory> examCategoryList = examCategoryDao.listCategoryTypeAll(ExamMainCategoryTypeEnum.SUBJECT.getCode());
        if (CollectionUtil.isNotEmpty(examCategoryList)) {
            Map<Long, ExamCategory> examCategoryMap = examCategoryList.stream().collect(Collectors.toMap(ExamCategory::getId, item -> item));
            listForPage.getList().stream().forEach(item -> {
                // 科目名称
                ExamCategory examCategory = examCategoryMap.get(item.getSubjectId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setSubjectName(examCategory.getCategoryName());

                    //第二级则查出第一级
                    ExamCategory parent = examCategoryDao.getById(examCategory.getParentId());
                    if (ObjectUtil.isNotNull(parent)) {
                        item.setSubjectParentId(parent.getId());
                        item.setSubjectParentName(parent.getCategoryName());
                    }
                }

                // 年份
                examCategory = examCategoryMap.get(item.getYearId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setYear(examCategory.getCategoryName());
                }

                // 来源
                examCategory = examCategoryMap.get(item.getSourceId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setSourceView(examCategory.getCategoryName());

                }
                // 讲师分类
                examCategory = examCategoryMap.get(item.getPersonalId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    item.setPersonalName(examCategory.getCategoryName());
                }
            });

        }
        // 补充分类名称
        for (ExamProblemListRESP resp : listForPage.getList()) {
            //标题过长，截断
            if (StrUtil.length(resp.getProblemContent()) > 30) {
                resp.setProblemContent(resp.getProblemContent().substring(0, 30));
            }

        }
        return Result.success(listForPage);
    }

    /**
     * 试卷题目查看
     *
     * @param id 主键ID
     * @return 试卷题目
     */
    public Result<ExamProblemViewRESP> view(Long id) {
        ExamProblem problem = dao.getById(id);
        if (ObjectUtil.isNull(problem)) {
            return Result.error("获取不到该试题信息!");
        }
        ExamProblemViewRESP problemViewRESP = BeanUtil.copyProperties(problem, ExamProblemViewRESP.class);
        //选项集合
        List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(id);
        if (CollectionUtil.isNotEmpty(optionList)) {
            problemViewRESP.setOptionList(BeanUtil.copyProperties(optionList, AuthExamProblemOptionViewRESP.class));
        }

        //组合题，需要拿出子试题结合
        if (ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
            List<ExamProblem> subProblemList = dao.listByParentId(problem.getId());
            List<ExamProblemViewRESP> subProblemViewList = BeanUtil.copyProperties(subProblemList, ExamProblemViewRESP.class);

            for (ExamProblemViewRESP subProblemViewDto : subProblemViewList) {
                if (ExamProblemTypeEnum.THE_RADIO.getCode().equals(subProblemViewDto.getProblemType()) || ExamProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblemViewDto.getProblemType()) || ExamProblemTypeEnum.ESTIMATE.getCode().equals(subProblemViewDto.getProblemType())) {
                    //选项集合
                    List<ExamProblemOption> options = examProblemOptionDao.listByProblemIdWithBLOBs(subProblemViewDto.getId());
                    if (CollectionUtil.isNotEmpty(options)) {
                        subProblemViewDto.setOptionList(BeanUtil.copyProperties(options, AuthExamProblemOptionViewRESP.class));
                    }
                }
            }
            problemViewRESP.setItemList(subProblemViewList);
        }

        //  设置附属信息
        setAuthExamProblemViewAttach(problemViewRESP);

        return Result.success(problemViewRESP);
    }

    /**
     * 设置试题详情附属信息
     *
     * @param dto 试题
     */
    private void setAuthExamProblemViewAttach(ExamProblemViewRESP dto) {
        if (dto.getGradeId() != null && dto.getGradeId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getGradeId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setGradeName(examCategory.getCategoryName());
            }
        }
        if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSubjectName(examCategory.getCategoryName());
            }
        }
        if (dto.getEmphasisId() != null && dto.getEmphasisId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getEmphasisId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setEmphasis(examCategory.getCategoryName());
            }
        }
        if (dto.getYearId() != null && dto.getYearId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getYearId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setYear(examCategory.getCategoryName());
            }
        }
        if (dto.getSourceId() != null && dto.getSourceId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getSourceId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setSourceView(examCategory.getCategoryName());
            }
        }
        if (dto.getDifficultyId() != null && dto.getDifficultyId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getDifficultyId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setDifficulty(examCategory.getCategoryName());
            }
        }
        if (dto.getTopicId() != null && dto.getTopicId() != 0) {
            ExamCategory examCategory = examCategoryDao.getById(dto.getTopicId());
            if (ObjectUtil.isNotNull(examCategory)) {
                dto.setTopicName(examCategory.getCategoryName());
            }
        }
    }

    /**
     * 试卷题目修改
     *
     * @param req 试卷题目修改对象
     * @return 修改结果
     */
    public Result<String> edit(ExamProblemEditREQ req) {
        ExamProblem problem = dao.getById(req.getId());
        if (ObjectUtil.isNull(problem)) {
            return Result.error("获取不到该题目信息!");
        }

        ExamProblem updateProblem = BeanUtil.copyProperties(req, ExamProblem.class);
        updateProblem.setLecturerUserNo(problem.getLecturerUserNo());
        //如果视频id为空
        if (req.getVideoId() == null) {
            updateProblem.setVideoName(null);
            updateProblem.setVideoLength(null);
            updateProblem.setVideoType(null);
        }
        // 材料题（小题套小题）
        if (ProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
            //原ids
            List<ExamProblem> oldProblemList = dao.listByParentId(req.getId());
            //删除所有试题的选项
            for (ExamProblem subProblem : oldProblemList) {
                examProblemOptionDao.deleteByProblemId(subProblem.getId());
            }

            //删除失效的子试题ids = 原ids与传入ids的差集
            List<Long> delIds = oldProblemList.stream().map(ExamProblem::getId).collect(Collectors.toList());
            delIds.removeAll(req.getChildrenList().stream().filter(x -> x.getId() != null).map(AuthExamProblemSaveREQ::getId).collect(Collectors.toList()));
            for (Long delId : delIds) {
                dao.deleteById(delId);
            }
            //更新/保存试题与选项
            try {
                for (AuthExamProblemSaveREQ problemSaveREQ : req.getChildrenList()) {
                    if (StringUtils.isEmpty(problemSaveREQ.getProblemAnswer())) {
                        throw new BaseException("试题答案不能为空");
                    }
                    // 添加/更新小题
                    ExamProblem subProblem = null;
                    if (problemSaveREQ.getId() != null) {
                        subProblem = BeanUtil.copyProperties(problemSaveREQ, ExamProblem.class);
                        subProblem.setLecturerUserNo(problem.getLecturerUserNo());
                        dao.updateById(subProblem);

                    } else {
                        subProblem = BeanUtil.copyProperties(problemSaveREQ, ExamProblem.class);
                        subProblem.setParentId(problem.getId());
                        subProblem.setLecturerUserNo(problem.getLecturerUserNo());
                        subProblem.setId(IdWorker.getId());
                        dao.save(subProblem);
                    }

                    // 选项添加
                    if (!CollectionUtils.isEmpty(problemSaveREQ.getOptionList())) {
                        for (AuthExamProblemOptionUpdateREQ option : problemSaveREQ.getOptionList()) {
                            saveOption(option, subProblem.getId());
                        }
                    }
                }
            } catch (Exception e) {
                return Result.error(e.getMessage());
            }
        } else {
            // 单选题、多选题、判断题、不定项选择题、填空題、解答题
            //删除选项再重建选项
            examProblemOptionDao.deleteByProblemId(problem.getId());
            // 选项添加
            if (!CollectionUtils.isEmpty(req.getOptionList())) {
                for (AuthExamProblemOptionUpdateREQ option : req.getOptionList()) {
                    saveOption(option, problem.getId());
                }
            }
        }

        if (dao.updateById(updateProblem) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 添加选项
     *
     * @param req        选项保存对象
     * @param problemId 题目ID
     */
    private void saveOption(AuthExamProblemOptionUpdateREQ req, Long problemId) {
        ExamProblemOption record = BeanUtil.copyProperties(req, ExamProblemOption.class);
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setProblemId(problemId);
        examProblemOptionDao.save(record);
    }


    /**
     * 试卷题目删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 更新状态
     *
     * @param req 更新状态参数
     * @return 更新结果
     */
    public Result<String> updateStatus(ExamProblemUpdateStatusREQ req) {
        // 检验必传参数
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
            return Result.error("状态不正确");
        }
        ExamProblem problem = BeanUtil.copyProperties(req, ExamProblem.class);
        if (dao.updateById(problem) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }


    /**
     * 批量导入试题
     *
     * @param
     * @param
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> uploadExcel(MultipartFile file) {
        XSSFWorkbook wookbook = null;
        try {
            wookbook = new XSSFWorkbook(file.getInputStream());
            for (int sheetI = 0; sheetI < 5; sheetI++) {
                XSSFSheet sheet = wookbook.getSheetAt(sheetI);
                // 		指的行数，一共有多少行+
                int rows = sheet.getLastRowNum();
                Long lecturerUserNo = null;
                for (int i = 0; i <= rows; i++) {
                    if (i == 1) {
                        // 读取左上端单元格
                        XSSFRow row = sheet.getRow(i);
                        // 行不为空
                        String mobile = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 手机号
                        if (StringUtils.isEmpty(mobile)) {
                            continue;
                        }
                        LecturerVO lecturer = feignLecturer.getByLecturerMobile(mobile);

                        if (ObjectUtil.isNull(lecturer) || !StatusIdEnum.YES.getCode().equals(lecturer.getStatusId())) {
                            continue;
                        }
                        lecturerUserNo = lecturer.getLecturerUserNo();
                    }
                    if (i >= 3) {
                        if (lecturerUserNo == null) {
                            continue;
                        }

                        // 读取左上端单元格
                        XSSFRow row = sheet.getRow(i);
                        // 行不为空
                        if (row != null) {
                            // 获取到Excel文件中的所有的列
                            // **读取cell**
                            // 试题内容

                            ExamProblem problem = problemSave(row, sheetI);
                            if (problem == null) {
                                continue;
                            }
                            //保存试题
                            problem.setLecturerUserNo(lecturerUserNo);
                            problem.setIsFree(IsFreeEnum.FREE.getCode());
                            problem.setParentId(0L);
                            problem.setId(IdWorker.getId());

                            problem = optionSave(problem, row);
                            dao.save(problem);
                        }
                    }
                }
            }
            return Result.success("导入成功");
        } catch (IOException e) {
            logger.error("导入失败, {}", e);
            return Result.error("导入失败");
        }
    }

    // 获取试题信息
    private ExamProblem problemSave(XSSFRow row, int sheetI) {
        ExamProblem problem = new ExamProblem();
        String problemContent = ExcelToolUtil.getXValue(row.getCell(0)).trim();// 试题内容
        if (StringUtils.isEmpty(problemContent)) {
            return null;
        }
        problem.setProblemContent(problemContent);

        // 题类
        problem.setProblemType(sheetI + 1);

        //分数
        String score = ExcelToolUtil.getXValue(row.getCell(1)).trim();// 分数
        if (StringUtils.isEmpty(score)) {
            return null;
        }
        problem.setScore(Integer.valueOf(score));

        // 试题年份
        String year = ExcelToolUtil.getXValue(row.getCell(2)).trim();
        if (StringUtils.hasText(year)) {
            ExamCategory yearCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.YEAR.getCode(), null, year);
            if (ObjectUtil.isNull(yearCategory)) {
                yearCategory = new ExamCategory();
                yearCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                yearCategory.setExamCategoryType(ExamCategoryTypeEnum.YEAR.getCode());
                yearCategory.setCategoryName(year);
                yearCategory.setId(IdWorker.getId());
                examCategoryDao.save(yearCategory);
            }
            problem.setYearId(yearCategory.getId());
        }


        // 试题地区
        String region = ExcelToolUtil.getXValue(row.getCell(3)).trim();
        if (StringUtils.hasText(region)) {
            problem.setRegion(region);
        }


        // 试题年级
        String gra = ExcelToolUtil.getXValue(row.getCell(4)).trim();// 试题年级
        if (StringUtils.hasText(gra)) {
            ExamCategory graCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.SUBJECT.getCode(), null, gra);
            if (ObjectUtil.isNull(graCategory)) {
                graCategory = new ExamCategory();
                graCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                graCategory.setExamCategoryType(ExamCategoryTypeEnum.SUBJECT.getCode());
                graCategory.setCategoryName(gra);
                graCategory.setId(IdWorker.getId());
                examCategoryDao.save(graCategory);
            }
            problem.setGraId(graCategory.getId());
            // 试题科目
            String subject = ExcelToolUtil.getXValue(row.getCell(5)).trim();// 试题科目
            if (StringUtils.hasText(subject)) {
                ExamCategory examCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(null, null, graCategory.getId(), subject);
                if (StringUtils.isEmpty(examCategory)) {
                    examCategory = new ExamCategory();
                    examCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                    examCategory.setExamCategoryType(ExamCategoryTypeEnum.SUBJECT.getCode());
                    examCategory.setCategoryName(subject);
                    examCategory.setParentId(graCategory.getId());
                    examCategory.setId(IdWorker.getId());
                    examCategoryDao.save(examCategory);
                }
                problem.setSubjectId(examCategory.getId());
            }
        }

        // 试题难度
        String level = ExcelToolUtil.getXValue(row.getCell(6)).trim();// 试题难度
        if (StringUtils.hasText(level)) {
            ExamCategory levelCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.DIFFICULTY.getCode(), null, level);
            if (ObjectUtil.isNull(levelCategory)) {
                levelCategory = new ExamCategory();
                levelCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                levelCategory.setExamCategoryType(ExamCategoryTypeEnum.DIFFICULTY.getCode());
                levelCategory.setCategoryName(level);
                levelCategory.setId(IdWorker.getId());
                examCategoryDao.save(levelCategory);
            }
            problem.setDifficultyId(levelCategory.getId());
        }

        // 试题解答
        String analysis = ExcelToolUtil.getXValue(row.getCell(7)).trim();// 试题解答
        if (StringUtils.hasText(analysis)) {
            problem.setAnalysis(analysis);
        }

        //试题考点
        String emphasis = ExcelToolUtil.getXValue(row.getCell(8)).trim();// 试题考点
        if (StringUtils.hasText(emphasis)) {
            ExamCategory emphasisCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.EMPHASIS.getCode(), null, emphasis);
            if (ObjectUtil.isNull(emphasisCategory)) {
                emphasisCategory = new ExamCategory();
                emphasisCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                emphasisCategory.setExamCategoryType(ExamCategoryTypeEnum.EMPHASIS.getCode());
                emphasisCategory.setCategoryName(emphasis);
                emphasisCategory.setId(IdWorker.getId());
                examCategoryDao.save(emphasisCategory);
            }
            problem.setEmphasisId(emphasisCategory.getId());
            problem.setEmphasis(emphasis);
        }


        // 试题类
        String topic = ExcelToolUtil.getXValue(row.getCell(9)).trim();// 试题类
        if (StringUtils.hasText(topic)) {
            ExamCategory classificationCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.CLASSIFICATION.getCode(), null, topic);
            if (ObjectUtil.isNull(classificationCategory)) {
                classificationCategory = new ExamCategory();
                classificationCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                classificationCategory.setExamCategoryType(ExamCategoryTypeEnum.CLASSIFICATION.getCode());
                classificationCategory.setCategoryName(topic);
                classificationCategory.setId(IdWorker.getId());
                examCategoryDao.save(classificationCategory);
            }
            problem.setTopicId(classificationCategory.getId());
        }

        // 试题来源
        String source = ExcelToolUtil.getXValue(row.getCell(10)).trim();// 试题来源
        if (StringUtils.hasText(source)) {
            ExamCategory sourceCategory = examCategoryDao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(ExamMainCategoryTypeEnum.SUBJECT.getCode(), ExamCategoryTypeEnum.SOURCE.getCode(), null, source);
            if (ObjectUtil.isNull(sourceCategory)) {
                sourceCategory = new ExamCategory();
                sourceCategory.setCategoryType(ExamMainCategoryTypeEnum.SUBJECT.getCode());
                sourceCategory.setExamCategoryType(ExamCategoryTypeEnum.SOURCE.getCode());
                sourceCategory.setCategoryName(source);
                sourceCategory.setId(IdWorker.getId());
                examCategoryDao.save(sourceCategory);
            }
            problem.setSourceId(sourceCategory.getId());
        }

        return problem;
    }


    // 选项添加
    private ExamProblem optionSave(ExamProblem problem, XSSFRow row) {
        // 单选题、多选题、判断题
        if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.GAP_FILLING.getCode().equals(problem.getProblemType())) {
            // 选项A
            String optionA = ExcelToolUtil.getXValue(row.getCell(11)).trim();  // A
            if (StringUtils.hasText(optionA)) {
                ExamProblemOption recordOptionA = new ExamProblemOption();
                recordOptionA.setProblemId(problem.getId());
                recordOptionA.setSort(1);
                recordOptionA.setOptionContent(optionA);
                examProblemOptionDao.save(recordOptionA);
            }
            // 选项B
            String optionB = ExcelToolUtil.getXValue(row.getCell(12)).trim();  // B
            if (StringUtils.hasText(optionB)) {
                ExamProblemOption recordOptionB = new ExamProblemOption();
                recordOptionB.setProblemId(problem.getId());
                recordOptionB.setSort(2);
                recordOptionB.setOptionContent(optionB);
                examProblemOptionDao.save(recordOptionB);
            }
            // 选项C
            String optionC = ExcelToolUtil.getXValue(row.getCell(13)).trim();  // C
            if (StringUtils.hasText(optionC)) {
                ExamProblemOption recordOptionC = new ExamProblemOption();
                recordOptionC.setProblemId(problem.getId());
                recordOptionC.setSort(3);
                recordOptionC.setOptionContent(optionC);
                examProblemOptionDao.save(recordOptionC);
            }
            // 选项D
            String optionD = ExcelToolUtil.getXValue(row.getCell(14)).trim(); // D
            if (StringUtils.hasText(optionD)) {
                ExamProblemOption recordOptionD = new ExamProblemOption();
                recordOptionD.setProblemId(problem.getId());
                recordOptionD.setSort(4);
                recordOptionD.setOptionContent(optionD);
                examProblemOptionDao.save(recordOptionD);
            }
            // 选项E
            String optionE = ExcelToolUtil.getXValue(row.getCell(15)).trim(); // E
            if (StringUtils.hasText(optionE)) {
                ExamProblemOption recordOptionE = new ExamProblemOption();
                recordOptionE.setProblemId(problem.getId());
                recordOptionE.setSort(5);
                recordOptionE.setOptionContent(optionE);
                examProblemOptionDao.save(recordOptionE);
            }
            // 选项F
            String optionF = ExcelToolUtil.getXValue(row.getCell(16)).trim(); // F
            if (StringUtils.hasText(optionF)) {
                ExamProblemOption recordOptionF = new ExamProblemOption();
                recordOptionF.setProblemId(problem.getId());
                recordOptionF.setSort(6);
                recordOptionF.setOptionContent(optionF);
                examProblemOptionDao.save(recordOptionF);
            }
            // 选项G
            String optionG = ExcelToolUtil.getXValue(row.getCell(17)).trim(); // G
            if (StringUtils.hasText(optionG)) {
                ExamProblemOption recordOptionG = new ExamProblemOption();
                recordOptionG.setProblemId(problem.getId());
                recordOptionG.setSort(7);
                recordOptionG.setOptionContent(optionG);
                examProblemOptionDao.save(recordOptionG);
            }
            // 选项H
            String optionH = ExcelToolUtil.getXValue(row.getCell(18)).trim();// H
            if (StringUtils.hasText(optionH)) {
                ExamProblemOption recordOptionH = new ExamProblemOption();
                recordOptionH.setProblemId(problem.getId());
                recordOptionH.setSort(8);
                recordOptionH.setOptionContent(optionH);
                examProblemOptionDao.save(recordOptionH);
            }
            // 选项H
            String isAsr = ExcelToolUtil.getXValue(row.getCell(19)).trim();// 正确答案
            String[] ids = isAsr.split(",");
            String problemAnswer = "";
            for (String answer : ids) {
                if (answer.equals("A") && StringUtils.hasText(optionA)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionA;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionA;
                    }
                }
                if (answer.equals("B") && StringUtils.hasText(optionB)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionB;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionB;
                    }
                }
                if (answer.equals("C") && StringUtils.hasText(optionC)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionC;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionC;
                    }
                }
                if (answer.equals("D") && StringUtils.hasText(optionD)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionD;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionD;
                    }
                }
                if (answer.equals("E") && StringUtils.hasText(optionE)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionE;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionE;
                    }
                }
                if (answer.equals("F") && StringUtils.hasText(optionF)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionF;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionF;
                    }
                }
                if (answer.equals("G") && StringUtils.hasText(optionG)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionG;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionG;
                    }
                }
                if (answer.equals("H") && StringUtils.hasText(optionH)) {
                    if (StringUtils.isEmpty(problemAnswer)) {
                        problemAnswer = optionH;
                    } else {
                        problemAnswer = problemAnswer + "|" + optionH;
                    }
                }
            }
            problem.setProblemAnswer(problemAnswer);
        }
        // 判断题
        if (ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
            // 选项A
            String optionA = ExcelToolUtil.getXValue(row.getCell(11)).trim();
            ExamProblemOption recordOptionH = new ExamProblemOption();
            recordOptionH.setProblemId(problem.getId());
            recordOptionH.setSort(1);
            recordOptionH.setOptionContent(optionA);
            examProblemOptionDao.save(recordOptionH);

            // 选项B
            String optionB = ExcelToolUtil.getXValue(row.getCell(12)).trim();
            ExamProblemOption recordOptionB = new ExamProblemOption();
            recordOptionB.setProblemId(problem.getId());
            recordOptionB.setSort(2);
            recordOptionB.setOptionContent(optionB);
            examProblemOptionDao.save(recordOptionB);

            // 正确答案
            String isAsr = ExcelToolUtil.getXValue(row.getCell(13)).trim();// 正确答案
            if (StringUtils.isEmpty(isAsr)) {
                return problem;
            }
            if (isAsr.equals("A")) {
                problem.setProblemAnswer(optionA);
            }
            if (isAsr.equals("B")) {
                problem.setProblemAnswer(optionB);
            }
            return problem;
        }
        // 解答题
        if (ProblemTypeEnum.SHORT_ANSWER.getCode().equals(problem.getProblemType())) {
            String answer = ExcelToolUtil.getXValue(row.getCell(11)).trim();  // 标准答案
            if (StringUtils.isEmpty(answer)) {
                return problem;
            }
            problem.setProblemAnswer(answer);
        }
        return problem;
    }
}
