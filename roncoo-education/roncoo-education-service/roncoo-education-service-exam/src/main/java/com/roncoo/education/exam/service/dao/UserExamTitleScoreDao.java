package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample;

import java.util.List;

/**
 * 用户考试标题得分 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamTitleScoreDao {

    /**
     * 保存用户考试标题得分
     *
     * @param record 用户考试标题得分
     * @return 影响记录数
     */
    int save(UserExamTitleScore record);

    /**
     * 根据ID删除用户考试标题得分
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 用户考试标题得分
     * @return 影响记录数
     */
    int updateById(UserExamTitleScore record);

    /**
     * 根据ID获取用户考试标题得分
     *
     * @param id 主键ID
     * @return 用户考试标题得分
     */
    UserExamTitleScore getById(Long id);

    /**
     * 用户考试标题得分--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<UserExamTitleScore> listForPage(int pageCurrent, int pageSize, UserExamTitleScoreExample example);

    /**
     * 根据记录ID获取用户考试标题得分
     *
     * @param recordId 记录ID
     * @return 用户考试标题得分
     */
    List<UserExamTitleScore> listByRecordId(Long recordId);

    /**
     * 根据记录ID、试卷ID用户考试记录
     *
     * @param recordId
     * @param examId
     * @return
     */
    UserExamTitleScore getByRecordIdAndExamId(Long recordId, Long examId);
}
