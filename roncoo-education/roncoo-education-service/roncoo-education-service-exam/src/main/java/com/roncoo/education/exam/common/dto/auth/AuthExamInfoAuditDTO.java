package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 试卷信息审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamInfoAuditDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerName;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "年级时间")
    private String year;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "来源信息")
    private String sourceView;

    @ApiModelProperty(value = "讲师分类id")
    private Long personalId;

    @ApiModelProperty(value = "讲师分类名称")
    private String personalName;

    @ApiModelProperty(value = "试卷排序")
    private Integer examSort;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal fabPrice;

    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;

    @ApiModelProperty(value = "答卷时间（分钟）")
    private Integer answerTime;

    @ApiModelProperty(value = "总分")
    private Integer scoreAccount;

    @ApiModelProperty(value = "试卷总题量")
    private Integer subjectAccount;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "是否提交审核(1:提交，0:未提交)")
    private Integer submitAudit;

    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)")
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

    @ApiModelProperty(value = "是否公开(1:公开，0:不公开-绑定课程)")
    private Integer isPublic;

    @ApiModelProperty(value = "是否有审核通过(0:未审核,1:审核通过)")
    private Integer isAudit = 0;


}
