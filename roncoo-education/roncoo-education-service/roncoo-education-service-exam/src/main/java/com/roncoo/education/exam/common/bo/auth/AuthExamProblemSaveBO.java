package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 试卷题目
 * </p>
 *
 * @author wujing
 * @date 2020-05-20
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamProblemSaveBO", description = "试卷题目  保存")
public class AuthExamProblemSaveBO implements Serializable {

    private static final long serialVersionUID = -5553210456699039071L;

    @ApiModelProperty(value = "主键id,更新必须传入")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @NotNull(message = "题目类型不能为空")
    @ApiModelProperty(value = "题目类型(1:单选题；2:多选题；3:判断题；4:填空题；5:简答题；6:组合题)", required = true)
    private Integer problemType;

    @NotEmpty(message = "题目内容不能为空")
    @ApiModelProperty(value = "题目内容", required = true)
    private String problemContent;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点Id")
    private Long emphasisId;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionCount;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @NotNull(message = "分值不能为空")
    @ApiModelProperty(value = "分值", required = true)
    private Integer score;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @NotNull(message = "科目id不能为空")
    @ApiModelProperty(value = "科目id", required = true)
    private Long subjectId;

    @NotNull(message = "年份id不能为空")
    @ApiModelProperty(value = "年份id", required = true)
    private Long yearId;

    @NotNull(message = "来源id不能为空")
    @ApiModelProperty(value = "来源id", required = true)
    private Long sourceId;

    @NotNull(message = "科目id不能为空")
    @ApiModelProperty(value = "难度id", required = true)
    private Long difficultyId;

    @NotNull(message = "题类id不能为空")
    @ApiModelProperty(value = "题类id", required = true)
    private Long topicId;

    @ApiModelProperty(value = "用户分类id")
    private Long personalId;

    @ApiModelProperty(value = "是否公开(1:公开,0:不公开)")
    private Integer isPublic;

    @ApiModelProperty(value = "下一级试题集合")
    List<AuthExamProblemSaveBO> childrenList;

    @ApiModelProperty(value = "题目答案")
    private String problemAnswer;

    @ApiModelProperty(value = "试题选项集合")
    private List<AuthExamProblemOptionSaveBO> optionList;
}
