package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.IsPutawayEnum;
import com.roncoo.education.common.core.enums.ProblemTypeEnum;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.NumberUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamDownloadExportBO;
import com.roncoo.education.exam.common.util.ExamExportUtil;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户试卷下载
 *
 * @author wujing
 */
@Component
public class AuthUserExamDownloadBiz extends BaseBiz {

    @Autowired
    private UserExamDownloadDao dao;

    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private UserOrderExamRefDao userOrderExamRefDao;

    public void export(AuthUserExamDownloadExportBO bo, HttpServletRequest request, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();

        // 获取试卷
        ExamInfo examInfo = examInfoDao.getById(bo.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            throw new BaseException("试卷不存在");
        }
        if (!examInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            // 不是讲师，需要判断是否下载权限
            if (!IsPutawayEnum.YES.getCode().equals(examInfo.getIsPutaway())) {
                throw new BaseException("试卷已下架，不能下载");
            }
            if (!IsFreeEnum.FREE.getCode().equals(examInfo.getIsFree())) {
                UserOrderExamRef userOrderExamRef = userOrderExamRefDao.getByUserNoAndExamId(ThreadContext.userNo(), bo.getExamId());
                if (ObjectUtil.isNull(userOrderExamRef)) {
                    throw new BaseException("请先购买再进行下载");
                }
            }
        }

        sb.append("<div style=\"margin: 0px auto 20px; display: flex;  flex: 1; min-height: 400px;\">");

        //试卷名称
        sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");
        sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
        sb.append(examInfo.getExamName());
        sb.append("</div>");
        sb.append("</div>");

        String exportFileName = examInfo.getExamName();

        // 试卷标题
        listExamTitle(bo.getExamId(), bo.getShowAnswer(), sb);
        sb.append("</div>");

        //ftl模板文件路径
        String templateFileName = "template/examTemplate.ftl";


        String docSrcLocationPrex = "file:///C:/213792E5";
        String docSrcParent = "examTemplate.files";
        String nextPartId = "01D644B6.11D02A80";
        ExamExportUtil.export(request, response, templateFileName, exportFileName, docSrcLocationPrex, docSrcParent, nextPartId, sb.toString());

        // 更新下载人数
        ExamInfo updateExamInfo = new ExamInfo();
        updateExamInfo.setId(examInfo.getId());
        updateExamInfo.setDownloadCount(examInfo.getDownloadCount() + 1);
        examInfoDao.updateById(updateExamInfo);
    }

    /**
     * 列出试卷标题
     *
     * @param examId 试卷ID
     * @param sb     试卷流
     */
    private void listExamTitle(Long examId, boolean showAnswer, StringBuilder sb) {
        List<ExamTitle> titleList = examTitleDao.listByExamId(examId);
        if (CollectionUtil.isEmpty(titleList)) {
            return;
        }

        int titleIndex = 1;
        int problemIndex = 1;
        for (ExamTitle title : titleList) {
            // 试卷标题
            sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");
            sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
            sb.append(NumberUtil.intToChineseNumber(titleIndex)).append("、").append(title.getTitleName());
            sb.append("</div>");
            sb.append("</div>");

            // 标题题目
            problemIndex = listTitleProblem(title, problemIndex, showAnswer, sb);

            titleIndex++;
        }
    }

    /**
     * 列出标题题目
     *
     * @param title        试卷标题
     * @param problemIndex 题目下标
     * @param showAnswer   展示答案
     * @param sb           试卷流
     * @return 试题下标
     */
    private Integer listTitleProblem(ExamTitle title, Integer problemIndex, boolean showAnswer, StringBuilder sb) {
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleId(title.getId());
        if (CollectionUtil.isEmpty(refList)) {
            return problemIndex;
        }

        // 获取题目
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return problemIndex;
        }
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, problem -> problem));

        if (ProblemTypeEnum.MATERIAL.getCode().equals(title.getTitleType())) {
            // 组合题
            List<ExamTitleProblemRef> parentRefList = refList.stream().filter(ref -> ProblemTypeEnum.MATERIAL.getCode().equals(ref.getProblemType())).collect(Collectors.toList());
            for (ExamTitleProblemRef ref : parentRefList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }

                sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");

                sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
                sb.append(problemIndex).append("、");
                sb.append(formatContent(problem.getProblemContent()));
                sb.append("</div>");

                // 获取子题
                listSubProblem(problem, refList, problemMap, showAnswer, sb);
                sb.append("</div>");

                problemIndex++;
            }
        } else {
            // 其他题型
            for (ExamTitleProblemRef ref : refList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }

                sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");

                sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
                sb.append(problemIndex).append("、");
                sb.append(formatContent(problem.getProblemContent()));
                sb.append("</div>");

                // 选项

                listProblemOptionAndShowAnswer(problem, showAnswer, sb);
                sb.append("</div>");

                problemIndex++;
            }
        }

        return problemIndex;
    }

    /**
     * 列出小题
     *
     * @param parentProblem 父题目
     * @param refList       关联集合
     * @param problemMap    题目集合
     * @param showAnswer    展示答案
     * @param sb            试卷流
     */
    private void listSubProblem(ExamProblem parentProblem, List<ExamTitleProblemRef> refList, Map<Long, ExamProblem> problemMap, boolean showAnswer, StringBuilder sb) {
        List<ExamTitleProblemRef> subRefList = refList.stream().filter(ref -> parentProblem.getId().equals(ref.getProblemParentId())).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(subRefList)) {
            return;
        }

        int problemIndex = 1;
        for (ExamTitleProblemRef ref : subRefList) {
            ExamProblem problem = problemMap.get(ref.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }

            // 子题
            sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
            sb.append("(").append(problemIndex).append(")、");
            sb.append(" <span style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
            sb.append(formatContent(problem.getProblemContent()));
            sb.append("</span>");
            sb.append("</div>");

            // 选项
            listProblemOptionAndShowAnswer(problem, showAnswer, sb);

            problemIndex++;
        }
    }

    /**
     * 列出试题选项和展示答案
     *
     * @param problem    题目
     * @param showAnswer 展示答案
     * @param sb         试卷流
     */
    private void listProblemOptionAndShowAnswer(ExamProblem problem, boolean showAnswer, StringBuilder sb) {
        if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
            List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId());
            if (CollectionUtil.isEmpty(optionList)) {
                return;
            }

            int optionIndex = 1;
            StringBuilder correctAnswer = new StringBuilder();
            sb.append("<div style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
            for (ExamProblemOption option : optionList) {
                String optionNo = NumberUtil.intToLetter(optionIndex);
                sb.append("<div>&nbsp;&nbsp;&nbsp;");
                sb.append("<span>").append(optionNo).append("．</span>");
                sb.append("<span>").append(option.getOptionContent()).append("</span>");
                sb.append("</div>");

                if (checkAnswer(problem.getProblemAnswer(), option.getOptionContent())) {
                    correctAnswer.append(optionNo);
                }
                optionIndex++;
            }

            // 是否展示答案
            if (showAnswer) {
                sb.append("<div style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
                sb.append("<div>&nbsp;&nbsp;&nbsp;");
                sb.append("<span>").append("【答案】").append("</span>");
                sb.append("<span>").append(correctAnswer.toString()).append("</span>");
                sb.append("</div>");
                sb.append("</div>");
            }
            sb.append("</div>");
        } else {
            // 是否展示答案
            if (showAnswer) {
                sb.append("<div style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
                sb.append("<div>&nbsp;&nbsp;&nbsp;");
                sb.append("<span>").append("【答案】").append("</span>");
                if (ProblemTypeEnum.GAP_FILLING.getCode().equals(problem.getProblemType()) && StrUtil.isNotBlank(problem.getProblemAnswer())) {
                    sb.append("<span>").append(problem.getProblemAnswer().replaceAll("\\|", "、 ")).append("</span>");
                } else {
                    sb.append("<span>").append(formatContent(problem.getProblemAnswer())).append("</span>");
                }
                sb.append("</div>");
                sb.append("</div>");
            }
        }

    }

    /**
     * 格式化内容
     *
     * @param problemContent 题目内容
     * @return 处理后题目内容
     */
    private String formatContent(String problemContent) {
        if (StrUtil.isBlank(problemContent)) {
            return problemContent;
        }
        int startIndex = problemContent.indexOf("<p");
        if (startIndex == 0) {
            problemContent = "<span" + problemContent.substring(startIndex + 2);
            int endIndex = problemContent.indexOf("</p>");
            if (endIndex > 0) {
                problemContent = problemContent.substring(0, endIndex) + "</span>" + problemContent.substring(endIndex + 4);
            }
        }
        return problemContent;
    }

    /**
     * 校验答案
     *
     * @param problemAnswer 试题答案
     * @param optionContent 选项内容
     * @return 是否正确答案
     */
    private boolean checkAnswer(String problemAnswer, String optionContent) {
        if (StrUtil.isBlank(problemAnswer) || StrUtil.isBlank(optionContent)) {
            return false;
        }

        List<String> problemAnswerList = Arrays.asList(problemAnswer.split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION));
        return problemAnswerList.contains(optionContent);
    }
}
