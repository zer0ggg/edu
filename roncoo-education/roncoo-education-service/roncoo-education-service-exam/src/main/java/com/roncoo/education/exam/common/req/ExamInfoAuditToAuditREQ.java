package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamInfoAuditToAuditREQ", description="试卷审核操作")
public class ExamInfoAuditToAuditREQ implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id",required = true)
    private Long id;

    @NotNull(message = "审核状态不能为空")
    @ApiModelProperty(value = "审核状态(1:审核通过,2:审核不通过)",required = true)
    private Integer auditStatus;
    /**
     * 审核意见
     */
    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

}
