package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswerExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeStudentExamAnswerMapper {
    int countByExample(GradeStudentExamAnswerExample example);

    int deleteByExample(GradeStudentExamAnswerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeStudentExamAnswer record);

    int insertSelective(GradeStudentExamAnswer record);

    List<GradeStudentExamAnswer> selectByExampleWithBLOBs(GradeStudentExamAnswerExample example);

    List<GradeStudentExamAnswer> selectByExample(GradeStudentExamAnswerExample example);

    GradeStudentExamAnswer selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeStudentExamAnswer record, @Param("example") GradeStudentExamAnswerExample example);

    int updateByExampleWithBLOBs(@Param("record") GradeStudentExamAnswer record, @Param("example") GradeStudentExamAnswerExample example);

    int updateByExample(@Param("record") GradeStudentExamAnswer record, @Param("example") GradeStudentExamAnswerExample example);

    int updateByPrimaryKeySelective(GradeStudentExamAnswer record);

    int updateByPrimaryKeyWithBLOBs(GradeStudentExamAnswer record);

    int updateByPrimaryKey(GradeStudentExamAnswer record);
}
