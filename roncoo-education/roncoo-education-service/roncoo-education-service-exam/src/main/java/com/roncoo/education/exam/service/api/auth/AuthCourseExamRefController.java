package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefDeleteBO;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthCourseExamRefViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthCourseExamRefViewDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthCourseExamRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * 课程试卷关联 UserApi接口
 *
 * @author wujing
 * @date 2020-10-21
 */
@Api(tags = "API-AUTH-课程试卷关联")
@RestController
@RequestMapping("/exam/auth/course/exam/ref")
public class AuthCourseExamRefController {

    @Autowired
    private AuthCourseExamRefBiz biz;

    @ApiOperation(value = "保存接口", notes = "保存接口")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid AuthCourseExamRefSaveBO authCourseExamRefSaveBO) {
        return biz.save(authCourseExamRefSaveBO);
    }

    @ApiOperation(value = "查看接口", notes = "保存接口")
    @PostMapping(value = "/view")
    public Result<AuthCourseExamRefViewDTO> view(@RequestBody @Valid AuthCourseExamRefViewBO authCourseExamRefViewBO) {
        return biz.view(authCourseExamRefViewBO);
    }

    @ApiOperation(value = "删除接口", notes = "删除接口")
    @PostMapping(value = "/delete")
    public Result<String> delete(@RequestBody @Valid AuthCourseExamRefDeleteBO authCourseExamRefDeleteBO) {
        return biz.delete(authCourseExamRefDeleteBO);
    }


}
