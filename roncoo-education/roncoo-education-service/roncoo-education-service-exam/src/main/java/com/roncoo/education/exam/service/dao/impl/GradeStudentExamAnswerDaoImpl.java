package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.GradeStudentExamAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.GradeStudentExamAnswerMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswerExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 服务实现类
 *
 * @author wujing
 * @date 2020-06-10
 */
@Repository
public class GradeStudentExamAnswerDaoImpl implements GradeStudentExamAnswerDao {

    @Autowired
    private GradeStudentExamAnswerMapper mapper;

    @Override
    public int save(GradeStudentExamAnswer record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(GradeStudentExamAnswer record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public GradeStudentExamAnswer getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<GradeStudentExamAnswer> listForPage(int pageCurrent, int pageSize, GradeStudentExamAnswerExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int deleteByGradeId(Long gradeId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int deleteByGradeIdAndStudentId(Long gradeId, Long studentId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(gradeId);
        c.andStudentIdEqualTo(studentId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public GradeStudentExamAnswer getByRelationIdAndProblemId(Long relationId, Long problemId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        c.andProblemIdEqualTo(problemId);
        List<GradeStudentExamAnswer> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public GradeStudentExamAnswer getByRelationIdAndTitleIdProblemId(Long relationId,Long titleId, Long problemId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        c.andTitleIdEqualTo(titleId);
        c.andProblemIdEqualTo(problemId);
        List<GradeStudentExamAnswer> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<GradeStudentExamAnswer> listByRelationId(Long relationId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeStudentExamAnswer> listByRelationIdAndAnswerStatus(Long relationId, Integer answerStatus) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        c.andAnswerStatusEqualTo(answerStatus);
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<GradeStudentExamAnswer> listByRelationIdWithBLOBs(Long relationId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<GradeStudentExamAnswer> listByRelationIdAndTitleId(Long relationId, Long titleId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        c.andTitleIdEqualTo(titleId);
        return this.mapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public List<GradeStudentExamAnswer> listByRelationIdAndTitleIdWithBLOBs(Long relationId, Long titleId) {
        GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
        GradeStudentExamAnswerExample.Criteria c = example.createCriteria();
        c.andRelationIdEqualTo(relationId);
        c.andTitleIdEqualTo(titleId);
        return this.mapper.selectByExampleWithBLOBs(example);
    }

}
