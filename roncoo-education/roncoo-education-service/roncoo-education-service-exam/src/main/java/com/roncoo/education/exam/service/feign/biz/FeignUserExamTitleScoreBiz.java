package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamTitleScoreQO;
import com.roncoo.education.exam.feign.vo.UserExamTitleScoreVO;
import com.roncoo.education.exam.service.dao.UserExamTitleScoreDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScore;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamTitleScoreExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试标题得分
 *
 * @author wujing
 */
@Component
public class FeignUserExamTitleScoreBiz extends BaseBiz {

    @Autowired
    private UserExamTitleScoreDao dao;

	public Page<UserExamTitleScoreVO> listForPage(UserExamTitleScoreQO qo) {
	    UserExamTitleScoreExample example = new UserExamTitleScoreExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExamTitleScore> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamTitleScoreVO.class);
	}

	public int save(UserExamTitleScoreQO qo) {
		UserExamTitleScore record = BeanUtil.copyProperties(qo, UserExamTitleScore.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamTitleScoreVO getById(Long id) {
		UserExamTitleScore record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamTitleScoreVO.class);
	}

	public int updateById(UserExamTitleScoreQO qo) {
		UserExamTitleScore record = BeanUtil.copyProperties(qo, UserExamTitleScore.class);
		return dao.updateById(record);
	}

}
