package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiPracticeBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 练习信息 Api接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-练习信息")
@RestController
@RequestMapping("/exam/api/practice")
public class ApiPracticeController {

    @Autowired
    private ApiPracticeBiz biz;

}
