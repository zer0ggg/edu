package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 考试完成答案
 *
 * @author LYQ
 */
@Api(tags = {"考试完成答案"})
@Data
@Accessors(chain = true)
public class AuthExamFinishAnswerBO implements Serializable {

    private static final long serialVersionUID = 3019351914094923914L;

    @ApiModelProperty(value = "标题ID", required = true)
    private Long titleId;

    @ApiModelProperty(value = "题目父类ID")
    private Long problemParentId;

    @ApiModelProperty(value = "题目ID", required = true)
    private Long problemId;

    @ApiModelProperty(value = "答案")
    private String answer;
}
