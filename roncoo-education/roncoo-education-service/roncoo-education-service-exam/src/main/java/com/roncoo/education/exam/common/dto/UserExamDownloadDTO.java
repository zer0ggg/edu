package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户试卷下载
 * </p>
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExamDownloadDTO", description="用户试卷下载")
public class UserExamDownloadDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long Id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "文件格式（1：DOC; 2: DOCX）")
    private Integer fileFormat;

    @ApiModelProperty(value = "下载类型（1：普通试卷题干带参考答案 ；2：考试试卷只有题干，不带参考答案）")
    private Integer downloadType;
}
