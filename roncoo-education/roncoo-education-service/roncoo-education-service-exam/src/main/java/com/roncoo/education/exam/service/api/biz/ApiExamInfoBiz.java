package com.roncoo.education.exam.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.elasticsearch.EsPageUtil;
import com.roncoo.education.exam.common.bo.ExamInfoPageBO;
import com.roncoo.education.exam.common.bo.ExamInfoSearchBO;
import com.roncoo.education.exam.common.bo.ExamInfoViewBO;
import com.roncoo.education.exam.common.dto.*;
import com.roncoo.education.exam.common.es.EsExam;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 试卷信息
 *
 * @author wujing
 */
@Component
public class ApiExamInfoBiz extends BaseBiz {

    @Autowired
    private ExamInfoDao dao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 分页列出试卷
     *
     * @param examInfoPageBO 查询参数
     * @return 分页结果
     */
    public Result<Page<ExamInfoPageDTO>> list(ExamInfoPageBO examInfoPageBO) {
        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria criteria = example.createCriteria();
        //默认条件
        criteria.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        criteria.andIsPublicEqualTo(IsPubilcEnum.YES.getCode());
        criteria.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        //用户条件
        if (StrUtil.isNotBlank(examInfoPageBO.getExamName())) {
            criteria.andExamNameLike(PageUtil.like(examInfoPageBO.getExamName()));
        }
        if (ObjectUtil.isNotNull(examInfoPageBO.getGraId())) {
            criteria.andGraIdEqualTo(examInfoPageBO.getGraId());
        }
        if (ObjectUtil.isNotNull(examInfoPageBO.getSubjectId())) {
            criteria.andSubjectIdIn(listSubSubjectId(examInfoPageBO.getSubjectId()));
            //criteria.andSubjectIdEqualTo(examInfoPageBO.getSubjectId());
        }
        if (ObjectUtil.isNotNull(examInfoPageBO.getYearId())) {
            criteria.andYearIdEqualTo(examInfoPageBO.getYearId());
        }
        if (ObjectUtil.isNotNull(examInfoPageBO.getSourceId())) {
            criteria.andSourceIdEqualTo(examInfoPageBO.getSourceId());
        }
        criteria.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        //综合排序
        example.setOrderByClause(" sort asc, id desc ");
        //浏览次数
        if (ExamSortEnum.STUDY.getCode().equals(examInfoPageBO.getSortType())) {
            example.setOrderByClause(" sort asc, study_count desc, id desc ");
        }
        //下载次数
        if (ExamSortEnum.DOWNLOAD.getCode().equals(examInfoPageBO.getSortType())) {
            example.setOrderByClause(" sort asc, download_count desc, id desc ");
        }
        //获取数据
        Page<ExamInfo> page = dao.listForPage(examInfoPageBO.getPageCurrent(), examInfoPageBO.getPageSize(), example);
        Page<ExamInfoPageDTO> listForPage = PageUtil.transform(page, ExamInfoPageDTO.class);
        //补充分类名称
        for (ExamInfoPageDTO dto : listForPage.getList()) {
            if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setSubjectName(examCategory.getCategoryName());
                }
            }
        }
        return Result.success(listForPage);
    }

    /**
     * 试卷详情
     *
     * @param viewBO 查询参数
     * @return 试卷详情
     */
    public Result<ExamInfoViewDTO> view(ExamInfoViewBO viewBO) {
        // 试卷信息
        ExamInfo examInfo = dao.getById(viewBO.getExamId());
        if (StringUtils.isEmpty(examInfo)) {
            return Result.error("获取试卷失败");
        }

        ExamInfoViewDTO viewDTO = BeanUtil.copyProperties(examInfo, ExamInfoViewDTO.class);
        // 获取年级名称
        ExamCategory graCategory = examCategoryDao.getById(examInfo.getGraId());
        if (ObjectUtil.isNotEmpty(graCategory)) {
            viewDTO.setGraName(graCategory.getCategoryName());
        }
        // 获取科目名称
        ExamCategory courseCategory = examCategoryDao.getById(examInfo.getSubjectId());
        if (ObjectUtil.isNotEmpty(courseCategory)) {
            viewDTO.setSubjectName(courseCategory.getCategoryName());
        }
        // 年份ID
        ExamCategory yearCategory = examCategoryDao.getById(examInfo.getYearId());
        if (ObjectUtil.isNotEmpty(yearCategory)) {
            viewDTO.setYearName(yearCategory.getCategoryName());
        }
        //获取来源名称
        ExamCategory sourceCategory = examCategoryDao.getById(examInfo.getSourceId());
        if (ObjectUtil.isNotEmpty(sourceCategory)) {
            viewDTO.setSourceName(sourceCategory.getCategoryName());
        }

        // 试卷信息
        List<ExamTitle> titleList = examTitleDao.listByExamIdAndStatusId(examInfo.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(titleList)) {
            return Result.success(viewDTO);
        }
        List<ExamTitleDTO> titleDTOList = ArrayListUtil.copy(titleList, ExamTitleDTO.class);
        for (ExamTitleDTO examTitleDTO : titleDTOList) {
            // 获取标题下的小题关联
            List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleIdAndProblemParentIdAndStatusId(examTitleDTO.getId(), 0L, StatusIdEnum.YES.getCode());
            if (CollectionUtil.isEmpty(refList)) {
                continue;
            }
            //保留关联列表
            Map<Long, ExamTitleProblemRef> problemRefMap = refList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, examTitleProblemRef -> examTitleProblemRef));


            // 获取小题
            List<ExamProblemDTO> questionList = new ArrayList<>();
            for (ExamTitleProblemRef ref : refList) {
                ExamProblem examProblem = examProblemDao.getById(ref.getProblemId());
                if (ObjectUtil.isEmpty(examProblem)) {
                    continue;
                }
                ExamProblemDTO questionDTO = BeanUtil.copyProperties(examProblem, ExamProblemDTO.class);
                //保留试题关联中的分数值
                questionDTO.setScore(problemRefMap.get(examProblem.getId()).getScore());

                if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitleDTO.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitleDTO.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitleDTO.getTitleType())) {
                    // 获取选项
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(examProblem.getId());
                    questionDTO.setOptionList(BeanUtil.copyProperties(optionList, ExamProblemOptionDTO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(examTitleDTO.getTitleType())) {
                    // 若是填空题、获取填空个数
                    if (StrUtil.isNotBlank(examProblem.getProblemAnswer())) {
                        questionDTO.setOptionCount(examProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                    } else {
                        questionDTO.setOptionCount(0);
                    }
                } else if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitleDTO.getTitleType())) {
                    questionDTO.setChildrenList(listMaterialQuestionSubQuestion(examProblem));
                }
                questionList.add(questionDTO);
            }

            // 添加小题
            examTitleDTO.setProblemList(questionList);
        }
        viewDTO.setTitleList(titleDTOList);

        // 修改浏览人数
        ExamInfo updateExamInfo = new ExamInfo();
        updateExamInfo.setId(examInfo.getId());
        updateExamInfo.setStudyCount(examInfo.getStudyCount() + 1);
        dao.updateById(updateExamInfo);
        return Result.success(viewDTO);
    }

    private List<ExamProblemDTO> listMaterialQuestionSubQuestion(ExamProblem examProblem) {
        List<ExamProblem> subProblemList = examProblemDao.listByParentId(examProblem.getId());
        if (CollectionUtil.isEmpty(subProblemList)) {
            return null;
        }
        List<ExamProblemDTO> childrenList = new ArrayList<>();
        for (ExamProblem subProblem : subProblemList) {
            ExamProblemDTO subProblemDTO = BeanUtil.copyProperties(subProblem, ExamProblemDTO.class);
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblemDTO.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblemDTO.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblemDTO.getProblemType())) {
                // 获取选项
                List<ExamProblemOption> questionOptionList = examProblemOptionDao.listByProblemIdWithBLOBs(subProblem.getId());
                subProblemDTO.setOptionList(BeanUtil.copyProperties(questionOptionList, ExamProblemOptionDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblemDTO.getProblemType())) {
                // 若是填空题、获取填空个数
                if (StrUtil.isNotBlank(subProblem.getProblemAnswer())) {
                    subProblemDTO.setOptionCount(subProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                } else {
                    subProblemDTO.setOptionCount(0);
                }
            }
            childrenList.add(subProblemDTO);
        }
        return childrenList;
    }

    public Result<Page<ExamInfoSearchPageDTO>> searchList(ExamInfoSearchBO bo) {
        if (bo.getPageCurrent() <= 0) {
            bo.setPageCurrent(1);
        }
        if (bo.getPageSize() <= 0) {
            bo.setPageSize(20);
        }
        if (StringUtils.isEmpty(bo.getExamName())) {
            return Result.success(new Page<>());
        }
        NativeSearchQueryBuilder nsb = new NativeSearchQueryBuilder();
        if (bo.getIsHfield() != null && bo.getIsHfield().equals(IsHfield.YES.getCode())) {
            String heightField = "examName";
            HighlightBuilder.Field hfield = new HighlightBuilder.Field(heightField).preTags("<mark>").postTags("</mark>");
            nsb.withHighlightFields(hfield);// 高亮字段
        }
        nsb.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));// 评分排序（_source）
        nsb.withSort(new FieldSortBuilder("sort").order(SortOrder.ASC));// 排序（sort）
        nsb.withPageable(PageRequest.of(bo.getPageCurrent() - 1, bo.getPageSize()));
        // 复合查询，外套boolQuery
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        // 精确查询termQuery不分词，must参数等价于AND
        // 模糊查询multiMatchQuery，最佳字段best_fields
        qb.must(QueryBuilders.multiMatchQuery(bo.getExamName(), "examName", "keyword").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));
        nsb.withQuery(qb);
        SearchHits<EsExam> searchHits = elasticsearchRestTemplate.search(nsb.build(), EsExam.class, IndexCoordinates.of(EsExam.EXAM));
        return Result.success(EsPageUtil.transform(searchHits, bo.getPageCurrent(), bo.getPageSize(), ExamInfoSearchPageDTO.class));
    }

    /**
     * 获取子科目ID
     *
     * @param subjectId 科目ID
     * @return 子科目ID
     */
    private List<Long> listSubSubjectId(Long subjectId) {
        List<Long> resultList = new ArrayList<>();
        resultList.add(subjectId);

        List<ExamCategory> categoryList = examCategoryDao.listByParentId(subjectId);
        if (CollectionUtil.isNotEmpty(categoryList)) {
            for (ExamCategory category : categoryList) {
                resultList.add(category.getId());
                resultList.addAll(listSubSubjectId(category.getId()));
            }
        }
        return resultList;
    }
}
