package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷信息审核
 *
 * @author wujing
 */
@Component
public class ApiExamInfoAuditBiz extends BaseBiz {

    @Autowired
    private ExamInfoAuditDao dao;

}
