package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiPracticeProblemRefBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 题目练习关联 Api接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-题目练习关联")
@RestController
@RequestMapping("/exam/api/practiceProblemRef")
public class ApiPracticeProblemRefController {

    @Autowired
    private ApiPracticeProblemRefBiz biz;

}
