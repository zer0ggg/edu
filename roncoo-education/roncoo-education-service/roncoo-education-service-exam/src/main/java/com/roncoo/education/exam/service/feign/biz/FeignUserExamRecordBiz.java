package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.UserExamRecordQO;
import com.roncoo.education.exam.feign.vo.UserExamRecordVO;
import com.roncoo.education.exam.service.dao.UserExamRecordDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户考试记录
 *
 * @author wujing
 */
@Component
public class FeignUserExamRecordBiz extends BaseBiz {

    @Autowired
    private UserExamRecordDao dao;

	public Page<UserExamRecordVO> listForPage(UserExamRecordQO qo) {
	    UserExamRecordExample example = new UserExamRecordExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<UserExamRecord> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserExamRecordVO.class);
	}

	public int save(UserExamRecordQO qo) {
		UserExamRecord record = BeanUtil.copyProperties(qo, UserExamRecord.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserExamRecordVO getById(Long id) {
		UserExamRecord record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserExamRecordVO.class);
	}

	public int updateById(UserExamRecordQO qo) {
		UserExamRecord record = BeanUtil.copyProperties(qo, UserExamRecord.class);
		return dao.updateById(record);
	}

}
