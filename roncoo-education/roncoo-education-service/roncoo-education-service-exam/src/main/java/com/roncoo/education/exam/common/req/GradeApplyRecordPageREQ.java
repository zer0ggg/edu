package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 班级申请记录分页请求对象
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "GradeApplyRecordPageREQ", description = "班级申请记录分页请求对象")
public class GradeApplyRecordPageREQ extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "审核状态(1:待审核，2:同意，3:拒绝)")
    private Integer auditStatus;

}
