package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 班级考试学生关联分页响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationScoreDTO", description = "班级考试学生 分数返回")
public class AuthGradeExamStudentRelationScoreDTO implements Serializable {

    private static final long serialVersionUID = 6970667085344566196L;

    //考试记录信息
    @ApiModelProperty(value = "考试记录ID")
    private Long id;

    @ApiModelProperty(value = "开始答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginAnswerTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endAnswerTime;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginExamTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endExamTime;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;

    @ApiModelProperty(value = "评阅用户编号")
    private Long auditUserNo;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "标题分数")
    private List<AuthGradeTitleScoreDTO> titleScoreDTOS;

    //补充信息
    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)")
    private Integer answerShow;

    //答案展示时间
    @ApiModelProperty(value = "答案展示时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date answerShowTime;

    @ApiModelProperty(value = "试卷名称")
    private  String examName;

    @ApiModelProperty(value = "时长（分钟）")
    private Integer duration = 0;

}
