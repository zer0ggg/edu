package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamTitleAudit;
import com.roncoo.education.exam.feign.qo.ExamTitleAuditQO;
import com.roncoo.education.exam.feign.vo.ExamTitleAuditVO;
import com.roncoo.education.exam.feign.vo.ExamTitleGradeAuditVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamTitleAuditBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷标题审核
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamTitleAuditController extends BaseController implements IFeignExamTitleAudit{

    @Autowired
    private FeignExamTitleAuditBiz biz;

	@Override
	public Page<ExamTitleAuditVO> listForPage(@RequestBody ExamTitleAuditQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamTitleAuditQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamTitleAuditQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamTitleAuditVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public ExamTitleGradeAuditVO getByIdForGradeAudit(Long id) {
		return biz.getByIdForGradeAudit(id);
	}
}
