package com.roncoo.education.exam.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.ExamInfoPageBO;
import com.roncoo.education.exam.common.bo.ExamInfoSearchBO;
import com.roncoo.education.exam.common.bo.ExamInfoViewBO;
import com.roncoo.education.exam.common.dto.ExamInfoPageDTO;
import com.roncoo.education.exam.common.dto.ExamInfoSearchPageDTO;
import com.roncoo.education.exam.common.dto.ExamInfoViewDTO;
import com.roncoo.education.exam.service.api.biz.ApiExamInfoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷信息 Api接口
 *
 * @author wujing
 * @date 2020-06-03
 */
@Api(tags = "API-试卷信息管理")
@RestController
@RequestMapping("/exam/api/exam/info")
public class ApiExamInfoController {

    @Autowired
    private ApiExamInfoBiz biz;

    @ApiOperation(value = "试卷列表", notes = "试卷列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamInfoPageDTO>> list(@RequestBody ExamInfoPageBO examInfoPageBO) {
        return biz.list(examInfoPageBO);
    }

    @ApiOperation(value = "试卷详情", notes = "试卷详情（通过标题ID查 获取大题 接口）")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<ExamInfoViewDTO> view(@RequestBody ExamInfoViewBO examInfoViewBO) {
        return biz.view(examInfoViewBO);
    }

    @ApiOperation(value = "试卷搜索列表接口", notes = "根据试卷名称，进行模糊搜索")
    @RequestMapping(value = "/search/list", method = RequestMethod.POST)
    public Result<Page<ExamInfoSearchPageDTO>> view(@RequestBody ExamInfoSearchBO bo) {
        return biz.searchList(bo);
    }

}
