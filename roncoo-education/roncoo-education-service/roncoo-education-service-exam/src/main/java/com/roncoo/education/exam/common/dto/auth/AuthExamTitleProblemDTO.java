package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 试卷大题的小题信息
 *
 * @author LYQ
 */
@ApiModel(value = "AuthExamTitleProblemDTO", description = "试卷大题的小题信息")
@Data
@Accessors(chain = true)
public class AuthExamTitleProblemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "地区")
    private String region;

    @ApiModelProperty(value = "父类ID")
    private Long parentId;

    @ApiModelProperty(value = "题目类型")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "收藏人数")
    private Integer collectionCount;

    @ApiModelProperty(value = "分值")
    private Integer score;

    @ApiModelProperty(value = "视频类型")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "视频时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @ApiModelProperty(value = "年级ID")
    private Long gradeId;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "年份ID")
    private Long yearId;

    @ApiModelProperty(value = "来源ID")
    private Long sourceId;

    @ApiModelProperty(value = "难度ID")
    private Long difficultyId;

    @ApiModelProperty(value = "题类ID")
    private Long topicId;

    @ApiModelProperty(value = "个人分类ID")
    private Long personalId;

    @ApiModelProperty(value = "填空数")
    private Integer optionCount;

    @ApiModelProperty(value = "题目选项(1:单选题；2:多选题；3:判断题)")
    private List<AuthExamTitleProblemOptionDTO> optionList;

    @ApiModelProperty(value = "小题(6:组合题；)")
    private List<AuthExamTitleProblemDTO> childrenList;

}
