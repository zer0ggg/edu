package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 申请加入班级审核对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeApplyRecordAuditForBatchBO", description = "申请加入班级审核对象  批量操作")
public class AuthGradeApplyRecordBatchAuditBO implements Serializable {

    private static final long serialVersionUID = -1450921553475079492L;

    @NotNull(message = "班级id不能为空")
    @ApiModelProperty(value = "班级id", required = true)
    private Long gradeId;

    @NotEmpty(message = "是否审核通过不能为空")
    @ApiModelProperty(value = "记录IDs", required = true)
    private String recordIds;

    @NotNull(message = "是否审核通过不能为空")
    @ApiModelProperty(value = "是否审核通过 true:通过、false:不通过", required = true)
    private Boolean auditPassed;

    @NotBlank(message = "审核意见不能为空")
    @ApiModelProperty(value = "审核意见", required = true)
    private String auditOpinion;


}
