package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.UserExamCategoryEditREQ;
import com.roncoo.education.exam.common.req.UserExamCategoryListREQ;
import com.roncoo.education.exam.common.req.UserExamCategorySaveREQ;
import com.roncoo.education.exam.common.resp.UserExamCategoryListRESP;
import com.roncoo.education.exam.common.resp.UserExamCategoryViewRESP;
import com.roncoo.education.exam.service.dao.UserExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamCategoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户试卷分类
 *
 * @author wujing
 */
@Component
public class PcUserExamCategoryBiz extends BaseBiz {

	@Autowired
	private UserExamCategoryDao dao;

	/**
	 * 用户试卷分类列表
	 *
	 * @param userExamCategoryListREQ 用户试卷分类分页查询参数
	 * @return 用户试卷分类分页查询结果
	 */
	public Result<Page<UserExamCategoryListRESP>> list(UserExamCategoryListREQ userExamCategoryListREQ) {
		UserExamCategoryExample example = new UserExamCategoryExample();
		Criteria c = example.createCriteria();
		Page<UserExamCategory> page = dao.listForPage(userExamCategoryListREQ.getPageCurrent(), userExamCategoryListREQ.getPageSize(), example);
		Page<UserExamCategoryListRESP> respPage = PageUtil.transform(page, UserExamCategoryListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 用户试卷分类添加
	 *
	 * @param userExamCategorySaveREQ 用户试卷分类
	 * @return 添加结果
	 */
	public Result<String> save(UserExamCategorySaveREQ userExamCategorySaveREQ) {
		UserExamCategory record = BeanUtil.copyProperties(userExamCategorySaveREQ, UserExamCategory.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 用户试卷分类查看
	 *
	 * @param id 主键ID
	 * @return 用户试卷分类
	 */
	public Result<UserExamCategoryViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), UserExamCategoryViewRESP.class));
	}

	/**
	 * 用户试卷分类修改
	 *
	 * @param userExamCategoryEditREQ 用户试卷分类修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(UserExamCategoryEditREQ userExamCategoryEditREQ) {
		UserExamCategory record = BeanUtil.copyProperties(userExamCategoryEditREQ, UserExamCategory.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 用户试卷分类删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
