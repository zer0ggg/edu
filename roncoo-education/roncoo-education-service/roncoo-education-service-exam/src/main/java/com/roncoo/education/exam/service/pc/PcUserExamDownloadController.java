package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamDownloadEditREQ;
import com.roncoo.education.exam.common.req.UserExamDownloadListREQ;
import com.roncoo.education.exam.common.req.UserExamDownloadSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamDownloadListRESP;
import com.roncoo.education.exam.common.resp.UserExamDownloadViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamDownloadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户试卷下载 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户试卷下载管理")
@RestController
@RequestMapping("/exam/pc/user/exam/download")
public class PcUserExamDownloadController {

    @Autowired
    private PcUserExamDownloadBiz biz;

    @ApiOperation(value = "用户试卷下载列表", notes = "用户试卷下载列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamDownloadListRESP>> list(@RequestBody UserExamDownloadListREQ userExamDownloadListREQ) {
        return biz.list(userExamDownloadListREQ);
    }

    @ApiOperation(value = "用户试卷下载添加", notes = "用户试卷下载添加")
    @SysLog(value = "用户试卷下载添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamDownloadSaveREQ userExamDownloadSaveREQ) {
        return biz.save(userExamDownloadSaveREQ);
    }

    @ApiOperation(value = "用户试卷下载查看", notes = "用户试卷下载查看")
    @SysLog(value = "用户试卷下载添加")
    @GetMapping(value = "/view")
    public Result<UserExamDownloadViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户试卷下载修改", notes = "用户试卷下载修改")
    @SysLog(value = "用户试卷下载修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamDownloadEditREQ userExamDownloadEditREQ) {
        return biz.edit(userExamDownloadEditREQ);
    }

    @ApiOperation(value = "用户试卷下载删除", notes = "用户试卷下载删除")
    @SysLog(value = "用户试卷下载删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
