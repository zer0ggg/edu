package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.feign.qo.ExamCategoryQO;
import com.roncoo.education.exam.feign.vo.ExamCategoryVO;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷分类
 *
 * @author wujing
 */
@Component
public class FeignExamCategoryBiz extends BaseBiz {

    @Autowired
    private ExamCategoryDao dao;

	public Page<ExamCategoryVO> listForPage(ExamCategoryQO qo) {
	    ExamCategoryExample example = new ExamCategoryExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamCategory> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamCategoryVO.class);
	}

	public int save(ExamCategoryQO qo) {
		ExamCategory record = BeanUtil.copyProperties(qo, ExamCategory.class);
		record.setId(IdWorker.getId());
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamCategoryVO getById(Long id) {
		ExamCategory record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamCategoryVO.class);
	}

	public int updateById(ExamCategoryQO qo) {
		ExamCategory record = BeanUtil.copyProperties(qo, ExamCategory.class);
		return dao.updateById(record);
	}

}
