package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 班级考试学生关联分页响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationPageDTO", description = "班级考试学生关联分页响应对象")
public class AuthGradeExamStudentRelationPageDTO implements Serializable {

    private static final long serialVersionUID = 6970667085344566196L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师名称")
    private String lecturerUserName;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "上一级科目ID")
    private Long parentSubjectId;

    @ApiModelProperty(value = "上一级科目名称")
    private String parentSubjectName;

    @ApiModelProperty(value = "上上一级科目ID")
    private Long grantSubjectId;

    @ApiModelProperty(value = "上上一级科目名称")
    private String grantSubjectName;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3:考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    private Date answerShowTime;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)")
    private Integer compensateStatus;

    @ApiModelProperty(value = "考试状态(1:未考试，2:考试中，3:考试完成，4:批阅中，5:批阅完成)")
    private Integer examStatus;

    @ApiModelProperty(value = "年份")
    private String yearName;


    @ApiModelProperty(value = "开始答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginAnswerTime;

    @ApiModelProperty(value = "结束答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endAnswerTime;

    @ApiModelProperty(value = "截止结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cutoffEndTime;

    @ApiModelProperty(value = "考试开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginExamTime;

    @ApiModelProperty(value = "考试结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endExamTime;

    @ApiModelProperty(value = "学生考试查看状态（1：正常流程，2：超时可补交, 3：超时不可以补交）")
    private Integer studentExamShowStatus;

    @ApiModelProperty(value = "总成绩")
    private Integer score = 0;

}
