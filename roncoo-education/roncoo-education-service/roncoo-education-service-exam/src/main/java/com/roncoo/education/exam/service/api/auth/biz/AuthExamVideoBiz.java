package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.VideoStatusEnum;
import com.roncoo.education.common.core.enums.VideoTagEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadUrlFile;
import com.roncoo.education.exam.common.bo.auth.AuthExamWebUploadVideoRefBO;
import com.roncoo.education.exam.service.dao.ExamVideoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 试卷视频
 *
 * @author wujing
 */
@Component
public class AuthExamVideoBiz extends BaseBiz {

    @Autowired
    private ExamVideoDao dao;

    @Autowired
    private IFeignSysConfig feignSysConfig;

    public Result<Long> ref(AuthExamWebUploadVideoRefBO bo) {
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (StringUtils.isEmpty(configPolyvVO.getPolyvSecretkey()) || StringUtils.isEmpty(configPolyvVO.getPolyvWritetoken())) {
            return Result.error("未配置保利威信息!");
        }
        //获取水印logo
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.LOGO);

        Long videoId = IdWorker.getId();
        //章节视频库
        ExamVideo examVideo = new ExamVideo();
        examVideo.setId(videoId);
        examVideo.setVideoStatus(VideoStatusEnum.UPLOADING.getCode());
        examVideo.setVideoName(bo.getVideoName());
        //前端直接上传保利威
        if (!StringUtils.isEmpty(bo.getVid())) {
            examVideo.setVideoOssId(bo.getOssUrl());
            dao.save(examVideo);
            return Result.success(videoId);
        }

        //上传阿里云，再上传保利威
        UploadUrlFile uploadUrlFile = new UploadUrlFile();
        uploadUrlFile.setUrl(bo.getOssUrl());
        uploadUrlFile.setTitle(bo.getVideoName());
        uploadUrlFile.setDesc(bo.getVideoName());
        uploadUrlFile.setAsync("true");//异步处理
        uploadUrlFile.setState(videoId.toString());//自定义数据为视频id，上传完成回调时会返回处理
        uploadUrlFile.setTag(VideoTagEnum.EXAM.getCode());//视频类型，用于区分是试卷还是普通课程
        if (!StringUtils.isEmpty(sysConfigVO)) {
            uploadUrlFile.setWatermark(sysConfigVO.getConfigValue());
        }
        if (PolyvUtil.uploadUrlFile(uploadUrlFile, configPolyvVO.getPolyvWritetoken(), configPolyvVO.getPolyvSecretkey())) {
            //章节视频库
            examVideo.setVideoOssId(bo.getOssUrl());
            dao.save(examVideo);
            return Result.success(videoId);
        } else {
            //如果上传保利威失败，则删除阿里云的视频
            Aliyun aliyun = BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class);
            AliyunUtil.delete(bo.getOssUrl(), aliyun);
            return Result.error("上传失败!");
        }
    }
}
