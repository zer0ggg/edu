package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownload;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamDownloadExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamDownloadMapper {
    int countByExample(UserExamDownloadExample example);

    int deleteByExample(UserExamDownloadExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExamDownload record);

    int insertSelective(UserExamDownload record);

    List<UserExamDownload> selectByExample(UserExamDownloadExample example);

    UserExamDownload selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExamDownload record, @Param("example") UserExamDownloadExample example);

    int updateByExample(@Param("record") UserExamDownload record, @Param("example") UserExamDownloadExample example);

    int updateByPrimaryKeySelective(UserExamDownload record);

    int updateByPrimaryKey(UserExamDownload record);
}