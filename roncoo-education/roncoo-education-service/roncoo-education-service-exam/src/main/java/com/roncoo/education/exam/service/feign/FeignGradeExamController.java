package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeExam;
import com.roncoo.education.exam.feign.qo.GradeExamQO;
import com.roncoo.education.exam.feign.vo.GradeExamVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeExamBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeExamController extends BaseController implements IFeignGradeExam{

    @Autowired
    private FeignGradeExamBiz biz;

	@Override
	public Page<GradeExamVO> listForPage(@RequestBody GradeExamQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeExamQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeExamQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeExamVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
