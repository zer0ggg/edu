package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeApplyRecord;
import com.roncoo.education.exam.feign.qo.GradeApplyRecordQO;
import com.roncoo.education.exam.feign.vo.GradeApplyRecordVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeApplyRecordBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeApplyRecordController extends BaseController implements IFeignGradeApplyRecord{

    @Autowired
    private FeignGradeApplyRecordBiz biz;

	@Override
	public Page<GradeApplyRecordVO> listForPage(@RequestBody GradeApplyRecordQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeApplyRecordQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeApplyRecordQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeApplyRecordVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
