package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="GradeApplyRecordDTO", description="")
public class GradeApplyRecordDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "申请描述")
    private String applyDescription;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "审核状态(1:待审核，2:同意，3:拒绝)")
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;

    @ApiModelProperty(value = "审核用户编号")
    private Long auditUserNo;
}
