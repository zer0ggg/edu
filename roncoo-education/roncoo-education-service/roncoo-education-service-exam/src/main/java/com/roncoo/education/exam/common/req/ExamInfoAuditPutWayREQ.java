package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamInfoAuditPutWayREQ", description="试卷审核 上下架操作")
public class ExamInfoAuditPutWayREQ implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @NotNull(message = "主键id不能为空")
    @ApiModelProperty(value = "主键id",required = true)
    private Long id;

    @NotNull(message = "是否上架不能为空")
    @ApiModelProperty(value = "是否上架(1:上架，0:下架)",required = true)
    private Integer isPutaway;

}
