package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationGradeForPageREQ;
import com.roncoo.education.exam.common.req.GradeExamStudentRelationListREQ;
import com.roncoo.education.exam.common.resp.GradeExamStudentRelationGradeForPageRESP;
import com.roncoo.education.exam.common.resp.GradeExamStudentRelationListRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeExamStudentRelationBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 学生考试 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-学生班级考试管理")
@RestController
@RequestMapping("/exam/pc/grade/exam/student/relation")
public class PcGradeExamStudentRelationController {

    @Autowired
    private PcGradeExamStudentRelationBiz biz;

    @ApiOperation(value = "学生班级考试列表", notes = "学生班级考试列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeExamStudentRelationListRESP>> list(@RequestBody GradeExamStudentRelationListREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级用户考试成绩列表", notes = "班级用户考试成绩分列表")
    @PostMapping("/page/grade")
    public Result<Page<GradeExamStudentRelationGradeForPageRESP>> listGradeForPage(@RequestBody GradeExamStudentRelationGradeForPageREQ req){
        return biz.listGradeForPage(req);
    }


}
