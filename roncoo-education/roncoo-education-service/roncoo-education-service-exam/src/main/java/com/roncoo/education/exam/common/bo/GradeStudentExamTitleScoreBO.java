package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author wujing
 * @date 2020-06-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="GradeStudentExamTitleScoreBO", description="")
public class GradeStudentExamTitleScoreBO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "关联ID")
    private Long relationId;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试ID")
    private Long gradeExamId;

    @ApiModelProperty(value = "学生ID")
    private Long studentId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "标题ID")
    private Long titleId;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;
}
