package com.roncoo.education.exam.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamTitleAuditQO;
import com.roncoo.education.exam.feign.vo.ExamProblemGradeAuditVO;
import com.roncoo.education.exam.feign.vo.ExamTitleAuditVO;
import com.roncoo.education.exam.feign.vo.ExamTitleGradeAuditVO;
import com.roncoo.education.exam.service.dao.ExamProblemDao;
import com.roncoo.education.exam.service.dao.ExamTitleAuditDao;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample.Criteria;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 试卷标题审核
 *
 * @author wujing
 */
@Component
public class FeignExamTitleAuditBiz extends BaseBiz {

    @Autowired
    private ExamTitleAuditDao dao;
	@Autowired
	private ExamTitleProblemRefAuditDao problemRefAuditDao;
	@Autowired
	private ExamProblemDao problemDao;

	public Page<ExamTitleAuditVO> listForPage(ExamTitleAuditQO qo) {
	    ExamTitleAuditExample example = new ExamTitleAuditExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamTitleAudit> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamTitleAuditVO.class);
	}

	public int save(ExamTitleAuditQO qo) {
		ExamTitleAudit record = BeanUtil.copyProperties(qo, ExamTitleAudit.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamTitleAuditVO getById(Long id) {
		ExamTitleAudit record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamTitleAuditVO.class);
	}

	public int updateById(ExamTitleAuditQO qo) {
		ExamTitleAudit record = BeanUtil.copyProperties(qo, ExamTitleAudit.class);
		return dao.updateById(record);
	}

	public ExamTitleGradeAuditVO getByIdForGradeAudit(Long id) {
		ExamTitleAudit record = dao.getById(id);
		if(ObjectUtil.isEmpty(record)){
			return  null;
		}
		ExamTitleGradeAuditVO titleVo = BeanUtil.copyProperties(record, ExamTitleGradeAuditVO.class);
		List<ExamTitleProblemRefAudit> problemRefAuditList =  problemRefAuditDao.listByTitleId(id);
		if(CollectionUtils.isEmpty(problemRefAuditList)){
			return titleVo;
		}
		List<ExamProblemGradeAuditVO> problemList = new ArrayList<>();
		titleVo.setProblemList(problemList);
		return titleVo;
	}
}
