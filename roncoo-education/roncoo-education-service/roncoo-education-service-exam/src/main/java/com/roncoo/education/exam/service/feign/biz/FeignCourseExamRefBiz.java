package com.roncoo.education.exam.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.exam.service.dao.CourseExamRefDao;
import com.roncoo.education.exam.service.dao.ExamInfoAuditDao;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample.Criteria;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程试卷关联
 *
 * @author wujing
 */
@Component
public class FeignCourseExamRefBiz extends BaseBiz {

    @Autowired
    private CourseExamRefDao dao;
	@Autowired
	private ExamInfoDao examInfoDao;
    @Autowired
    private ExamInfoAuditDao examInfoAuditDao;

	public Page<CourseExamRefVO> listForPage(CourseExamRefQO qo) {
	    CourseExamRefExample example = new CourseExamRefExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<CourseExamRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseExamRefVO.class);
	}

	public int save(CourseExamRefQO qo) {
		CourseExamRef record = BeanUtil.copyProperties(qo, CourseExamRef.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseExamRefVO getById(Long id) {
		CourseExamRef record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseExamRefVO.class);
	}

	public int updateById(CourseExamRefQO qo) {
		CourseExamRef record = BeanUtil.copyProperties(qo, CourseExamRef.class);
		return dao.updateById(record);
	}

    public CourseExamRefVO getByRefIdAndRefType(CourseExamRefQO qo) {
		CourseExamRef record = dao.getByRefIdAndRefType(qo.getRefId(), qo.getRefType());
		if (ObjectUtil.isNull(record)) {
			return null;
		}
		ExamInfo examInfo = examInfoDao.getById(record.getExamId());
		if (ObjectUtil.isNotNull(examInfo)) {
			return BeanUtil.copyProperties(record, CourseExamRefVO.class);
		}
		return null;
    }

    public CourseExamRefVO getByRefIdAndRefTypeAudit(CourseExamRefQO qo) {
		CourseExamRef record = dao.getByRefIdAndRefType(qo.getRefId(), qo.getRefType());
		if (ObjectUtil.isNull(record)) {
			return null;
		}
		ExamInfoAudit examInfoAudit = examInfoAuditDao.getById(record.getExamId());
		if (ObjectUtil.isNotNull(examInfoAudit)) {
			CourseExamRefVO courseExamRefVO = BeanUtil.copyProperties(record, CourseExamRefVO.class);
			courseExamRefVO.setExamName(examInfoAudit.getExamName());
			return courseExamRefVO;
		}
		return null;
    }
}
