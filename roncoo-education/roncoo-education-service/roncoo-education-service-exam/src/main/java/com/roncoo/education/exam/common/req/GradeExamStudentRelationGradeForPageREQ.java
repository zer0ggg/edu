package com.roncoo.education.exam.common.req;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 学生考试接口
 *
 * @author hsq
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "GradeExamStudentRelationGradeForPageREQ", description = "班级用户考试成绩分页请求对象")
public class GradeExamStudentRelationGradeForPageREQ extends PageParam implements Serializable {

    @NotNull(message = "用户编号不能为空")
    @ApiModelProperty(value = "用户编号")
    private Long userNo;


    @ApiModelProperty(value = "学员名称")
    private String nickname;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "状态")
    private Long statusId;

    @ApiModelProperty(value = "开始注册时间")
    private String beginRegisterTime;

    @ApiModelProperty(value = "结束注册时间")
    private String endRegisterTime;

}
