package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample;

import java.util.List;

/**
 * 试卷标题题目关联审核 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamTitleProblemRefAuditDao {

    /**
     * 保存试卷标题题目关联审核
     *
     * @param record 试卷标题题目关联审核
     * @return 影响记录数
     */
    int save(ExamTitleProblemRefAudit record);

    /**
     * 根据ID删除试卷标题题目关联审核
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 试卷标题题目关联审核
     * @return 影响记录数
     */
    int updateById(ExamTitleProblemRefAudit record);

    /**
     * 根据ID获取试卷标题题目关联审核
     *
     * @param id 主键ID
     * @return 试卷标题题目关联审核
     */
    ExamTitleProblemRefAudit getById(Long id);

    /**
     * 试卷标题题目关联审核--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ExamTitleProblemRefAudit> listForPage(int pageCurrent, int pageSize, ExamTitleProblemRefAuditExample example);

    /**
     * 根据标题ID获取试卷标题题目关联信息
     *
     * @param titleId 标题ID
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleId(Long titleId);

    /**
     * 根据标题ID和状态获取试卷标题题目关联信息
     *
     * @param titleId 标题ID
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleIdAndStatusId(Long titleId, Integer statusId);

    /**
     * 根据标题ID和父id
     *
     * @param titleId 标题ID
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleIdAndProblemParentId(Long titleId, Long pId);

    /**
     * 根据标题ID和父id
     *
     * @param titleId 标题ID
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleIdAndProblemParentIdAndStatusId(Long titleId, Long pId, Integer statusId);

    /**
     * 根据标题ID和审核状态获取试卷标题题目关联信息
     *
     * @param titleId     标题ID
     * @param auditStatus 审核状态
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleIdAndAuditStatus(Long titleId, Integer auditStatus);

    /**
     * 根据讲师编号和审核状态获取试卷标题题目关联信息
     *
     * @param userNo      用户编号
     * @param auditStatus 审核状态
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByUserNoAndStatusId(Long userNo, Integer auditStatus);

    /**
     * 根据标题id和试题id获取试卷标题题目关联信息
     *
     * @param titleId   标题ID
     * @param problemId 题目ID
     * @return 试卷标题题目关联审核
     */
    List<ExamTitleProblemRefAudit> listByTitleIdAndProblemId(Long titleId, Long problemId);

    /**
     * 根据标题id删除
     *
     * @param titleId 标题ID
     * @return 试卷标题题目关联审核
     */
    int deleteByTitleId(Long titleId);

    /**
     * 根据标题id和父试题id删除
     *
     * @param titleId
     * @param parentProblemId
     * @return
     */
    int deleteByTitleIdAndParentProblemId(Long titleId, Long parentProblemId);

    /**
     * 根据标题ID和试题ID获取试卷标题题目关联
     *
     * @param titleId   标题ID
     * @param problemId 题目ID
     * @return 试卷信息关联
     */
    ExamTitleProblemRefAudit getByTitleIdAndProblemId(Long titleId, Long problemId);

    /**
     * 根据标题ID 更新
     *
     * @param record
     * @return
     */
    int updateByTitleId(ExamTitleProblemRefAudit record);

    /**
     * 自定义更新
     *
     * @param record
     * @return
     */
    int update(ExamTitleProblemRefAudit record, ExamTitleProblemRefAuditExample example);

    /**
     * 根据试卷ID获取标题题目关联信息
     *
     * @param examId
     * @return
     */
    List<ExamTitleProblemRefAudit> listByExamId(Long examId);
}
