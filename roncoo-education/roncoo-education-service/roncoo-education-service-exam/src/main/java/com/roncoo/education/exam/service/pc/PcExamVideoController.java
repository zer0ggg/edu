package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamVideoEditREQ;
import com.roncoo.education.exam.common.req.ExamVideoListREQ;
import com.roncoo.education.exam.common.req.ExamVideoSaveREQ;
import com.roncoo.education.exam.common.resp.ExamVideoListRESP;
import com.roncoo.education.exam.common.resp.ExamVideoViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamVideoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷视频 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷视频管理")
@RestController
@RequestMapping("/exam/pc/exam/video")
public class PcExamVideoController {

    @Autowired
    private PcExamVideoBiz biz;

    @ApiOperation(value = "试卷视频列表", notes = "试卷视频列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamVideoListRESP>> list(@RequestBody ExamVideoListREQ examVideoListREQ) {
        return biz.list(examVideoListREQ);
    }

    @ApiOperation(value = "试卷视频添加", notes = "试卷视频添加")
    @SysLog(value = "试卷视频添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamVideoSaveREQ examVideoSaveREQ) {
        return biz.save(examVideoSaveREQ);
    }

    @ApiOperation(value = "试卷视频查看", notes = "试卷视频查看")
    @GetMapping(value = "/view")
    public Result<ExamVideoViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷视频修改", notes = "试卷视频修改")
    @SysLog(value = "试卷视频修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamVideoEditREQ examVideoEditREQ) {
        return biz.edit(examVideoEditREQ);
    }

    @ApiOperation(value = "试卷视频删除", notes = "试卷视频删除")
    @SysLog(value = "试卷视频删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
