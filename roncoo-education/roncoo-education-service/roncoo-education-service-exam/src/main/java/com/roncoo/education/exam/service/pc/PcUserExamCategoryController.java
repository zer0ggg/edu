package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamCategoryEditREQ;
import com.roncoo.education.exam.common.req.UserExamCategoryListREQ;
import com.roncoo.education.exam.common.req.UserExamCategorySaveREQ;
import com.roncoo.education.exam.common.resp.UserExamCategoryListRESP;
import com.roncoo.education.exam.common.resp.UserExamCategoryViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户试卷分类 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户试卷分类管理")
@RestController
@RequestMapping("/exam/pc/user/exam/category")
public class PcUserExamCategoryController {

    @Autowired
    private PcUserExamCategoryBiz biz;
    @SysLog(value = "用户试卷分类删除")
    @ApiOperation(value = "用户试卷分类列表", notes = "用户试卷分类列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamCategoryListRESP>> list(@RequestBody UserExamCategoryListREQ userExamCategoryListREQ) {
        return biz.list(userExamCategoryListREQ);
    }

    @ApiOperation(value = "用户试卷分类添加", notes = "用户试卷分类添加")
    @SysLog(value = "用户试卷分类添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamCategorySaveREQ userExamCategorySaveREQ) {
        return biz.save(userExamCategorySaveREQ);
    }

    @ApiOperation(value = "用户试卷分类查看", notes = "用户试卷分类查看")
    @GetMapping(value = "/view")
    public Result<UserExamCategoryViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户试卷分类修改", notes = "用户试卷分类修改")
    @SysLog(value = "用户试卷分类修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamCategoryEditREQ userExamCategoryEditREQ) {
        return biz.edit(userExamCategoryEditREQ);
    }

    @ApiOperation(value = "用户试卷分类删除", notes = "用户试卷分类删除")
    @SysLog(value = "用户试卷分类删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
