package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamExample;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeExamDao {

    /**
     * 保存班级考试
     *
     * @param record 班级考试
     * @return 影响记录数
     */
    int save(GradeExam record);

    /**
     * 根据ID删除班级考试
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改班级考试
     *
     * @param record 班级考试
     * @return 影响记录数
     */
    int updateById(GradeExam record);

    /**
     * 根据ID获取班级考试
     *
     * @param id 主键ID
     * @return
     */
    GradeExam getById(Long id);

    /**
     * 班级考试--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeExam> listForPage(int pageCurrent, int pageSize, GradeExamExample example);

    /**
     * 根据班级ID删除班级考试
     *
     * @param gradeId 班级ID
     * @return 影响记录数
     */
    int deleteByGradeId(Long gradeId);

    /**
     * 根据班级ID统计班级考试
     *
     * @param gradeId 班级ID
     * @return 班级考试记录数
     */
    int countByGradeId(Long gradeId);

}
