package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.PracticeEditREQ;
import com.roncoo.education.exam.common.req.PracticeListREQ;
import com.roncoo.education.exam.common.req.PracticeSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeListRESP;
import com.roncoo.education.exam.common.resp.PracticeViewRESP;
import com.roncoo.education.exam.service.dao.PracticeDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 练习信息
 *
 * @author LHR
 */
@Component
public class PcPracticeBiz extends BaseBiz {

    @Autowired
    private PracticeDao dao;

    /**
    * 练习信息列表
    *
    * @param practiceListREQ 练习信息分页查询参数
    * @return 练习信息分页查询结果
    */
    public Result<Page<PracticeListRESP>> list(PracticeListREQ practiceListREQ) {
        PracticeExample example = new PracticeExample();
        Criteria c = example.createCriteria();
        Page<Practice> page = dao.listForPage(practiceListREQ.getPageCurrent(), practiceListREQ.getPageSize(), example);
        Page<PracticeListRESP> respPage = PageUtil.transform(page, PracticeListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 练习信息添加
    *
    * @param practiceSaveREQ 练习信息
    * @return 添加结果
    */
    public Result<String> save(PracticeSaveREQ practiceSaveREQ) {
        Practice record = BeanUtil.copyProperties(practiceSaveREQ, Practice.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 练习信息查看
    *
    * @param id 主键ID
    * @return 练习信息
    */
    public Result<PracticeViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), PracticeViewRESP.class));
    }


    /**
    * 练习信息修改
    *
    * @param practiceEditREQ 练习信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(PracticeEditREQ practiceEditREQ) {
        Practice record = BeanUtil.copyProperties(practiceEditREQ, Practice.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 练习信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
