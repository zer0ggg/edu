package com.roncoo.education.exam.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.GradeStudentUserRoleEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.GradeStudentEditREQ;
import com.roncoo.education.exam.common.req.GradeStudentPageREQ;
import com.roncoo.education.exam.common.req.GradeStudentSaveREQ;
import com.roncoo.education.exam.common.resp.GradeStudentListRESP;
import com.roncoo.education.exam.common.resp.GradeStudentViewRESP;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wujing
 */
@Component
public class PcGradeStudentBiz extends BaseBiz {

    @Autowired
    private GradeStudentDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeExamStudentRelationDao gradeExamStudentRelationDao;
    @Autowired
    private GradeStudentExamTitleScoreDao gradeStudentExamTitleScoreDao;
    @Autowired
    private GradeStudentExamAnswerDao gradeStudentExamAnswerDao;

    @Autowired
    private IFeignUser feignUser;

    /**
     * 列表
     *
     * @param req 分页查询参数
     * @return 分页查询结果
     */
    public Result<Page<GradeStudentListRESP>> list(GradeStudentPageREQ req) {
        GradeStudentExample example = new GradeStudentExample();
        Criteria c = example.createCriteria();
        c.andGradeIdEqualTo(req.getGradeId());
        if (ObjectUtil.isNotNull(req.getStatusId())) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (ObjectUtil.isNotNull(req.getUserNo())) {
            c.andUserNoEqualTo(req.getUserNo());
        }
        if (ObjectUtil.isNotNull(req.getUserRole())) {
            c.andUserRoleEqualTo(req.getUserRole());
        }
        if (StrUtil.isNotBlank(req.getNickname())) {
            c.andNicknameLike(PageUtil.like(req.getNickname()));
        }
        example.setOrderByClause("sort asc, id desc");
        Page<GradeStudent> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<GradeStudentListRESP> respPage = PageUtil.transform(page, GradeStudentListRESP.class);
        return Result.success(respPage);
    }

    /**
     * 添加
     *
     * @param req 班级学生添加对象
     * @return 添加结果
     */
    public Result<String> save(GradeStudentSaveREQ req) {
        // 判断班级
        GradeInfo gradeInfo = gradeInfoDao.getById(req.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }
        if (gradeInfo.getLecturerUserNo().equals(req.getUserNo())) {
            return Result.error("班级讲师不能成为班级成员");
        }

        // 判断用户
        GradeStudent gradeStudent = dao.getByGradeIdAndUserNo(req.getGradeId(), req.getUserNo());
        if (ObjectUtil.isNotNull(gradeStudent)) {
            return Result.error("已经是班级成员，无需重复加入");
        }
        UserVO userVO = feignUser.getByUserNo(req.getUserNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("用户不存在");
        }

        // 判断班级角色
        GradeStudentUserRoleEnum gradeStudentUserRoleEnum = GradeStudentUserRoleEnum.byCode(req.getUserRole());
        if (ObjectUtil.isNull(gradeStudentUserRoleEnum)) {
            return Result.error("用户角色不正确");
        }

        //保存学生
        GradeStudent saveGradeStudent = BeanUtil.copyProperties(req, GradeStudent.class);
        saveGradeStudent.setLecturerUserNo(gradeInfo.getLecturerUserNo());
        if (dao.save(saveGradeStudent) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 班级学员查看
     *
     * @param id 学员Id
     * @return 班级学员
     */
    public Result<GradeStudentViewRESP> view(Long id) {
        GradeStudent gradeStudent = dao.getById(id);
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("学员不存在");
        }
        return Result.success(BeanUtil.copyProperties(gradeStudent, GradeStudentViewRESP.class));
    }

    /**
     * 修改
     *
     * @param req 修改对象
     * @return 修改结果
     */
    public Result<String> edit(GradeStudentEditREQ req) {
        // 判断用户角色
        if (ObjectUtil.isNotNull(req.getUserRole())) {
            GradeStudentUserRoleEnum gradeStudentUserRoleEnum = GradeStudentUserRoleEnum.byCode(req.getUserRole());
            if (ObjectUtil.isNull(gradeStudentUserRoleEnum)) {
                return Result.error("用户角色不正确");
            }
        }

        // 更新
        GradeStudent gradeStudent = BeanUtil.copyProperties(req, GradeStudent.class);
        if (dao.updateById(gradeStudent) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> delete(Long id) {
        GradeStudent gradeStudent = dao.getById(id);
        if (ObjectUtil.isNull(gradeStudent)) {
            return Result.error("学生不存在");
        }

        // 删除考试答案
        gradeStudentExamAnswerDao.deleteByGradeIdAndStudentId(gradeStudent.getGradeId(), gradeStudent.getId());

        // 删除考试标题成绩
        gradeStudentExamTitleScoreDao.deleteByGradeIdAndStudentId(gradeStudent.getGradeId(), gradeStudent.getId());

        // 删除考试
        gradeExamStudentRelationDao.deleteByGradeIdAndStudentId(gradeStudent.getGradeId(), gradeStudent.getId());

        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
