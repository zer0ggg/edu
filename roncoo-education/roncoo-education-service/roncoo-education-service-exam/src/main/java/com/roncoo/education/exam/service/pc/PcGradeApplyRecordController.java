package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.GradeApplyRecordPageREQ;
import com.roncoo.education.exam.common.resp.GradeApplyRecordPageRESP;
import com.roncoo.education.exam.common.resp.GradeApplyRecordViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcGradeApplyRecordBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/exam/pc/grade/apply/record")
@Api(tags = "PC-班级申请记录管理")
public class PcGradeApplyRecordController {

    @Autowired
    private PcGradeApplyRecordBiz biz;

    @ApiOperation(value = "班级申请记录列表", notes = "班级申请记录列表")
    @PostMapping(value = "/list")
    public Result<Page<GradeApplyRecordPageRESP>> list(@RequestBody GradeApplyRecordPageREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "班级申请记录查看", notes = "班级申请记录查看")
    @GetMapping(value = "/view")
    public Result<GradeApplyRecordViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "班级申请记录删除", notes = "班级申请记录删除")
    @SysLog(value = "班级申请记录删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
