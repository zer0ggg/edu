package com.roncoo.education.exam.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 获取大题
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class ExamTitleListDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题类型")
    private Integer titleType;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "题目集合")
    private List<ExamTitleProblemDTO> problemList;

}
