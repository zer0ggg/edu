package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelation;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample;

import java.util.Date;
import java.util.List;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeExamStudentRelationDao {

    /**
     * 保存
     *
     * @param record
     * @return 影响记录数
     */
    int save(GradeExamStudentRelation record);

    /**
     * 根据ID删除
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record
     * @return 影响记录数
     */
    int updateById(GradeExamStudentRelation record);

    /**
     * 根据ID获取
     *
     * @param id 主键ID
     * @return
     */
    GradeExamStudentRelation getById(Long id);

    /**
     * --分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeExamStudentRelation> listForPage(int pageCurrent, int pageSize, GradeExamStudentRelationExample example);


    /**
     * 根据班级ID删除班级考试学生关联
     *
     * @param gradeId 班级ID
     * @return 影响记录数
     */
    int deleteByGradeId(Long gradeId);

    /**
     * 根据班级考试ID删除班级考试学生关联
     *
     * @param gradeExamId 班级考试ID
     * @return 影响记录数
     */
    int deleteByGradeExamId(Long gradeExamId);

    /**
     * 根据班级ID和学生ID删除考试学生关联
     *
     * @param gradeId   班级ID
     * @param studentId 学生ID
     * @return 影响记录数
     */
    int deleteByGradeIdAndStudentId(Long gradeId, Long studentId);

    /**
     * 根据id获取，加行锁
     *
     * @param id
     * @return
     */
    GradeExamStudentRelation getByIdForUpdate(Long id);

    /**
     * 根据班级考试ID进行统计
     *
     * @param gradeExamId 班级考试ID
     * @return 统计结果
     */
    int countByGradeExamId(Long gradeExamId);

    /**
     * 根据班级考试ID和考试状态进行统计
     *
     * @param gradeExamId 班级考试ID
     * @param examStatus  考试状态
     * @return 统计结果
     */
    int countByGradeExamIdAndExamStatus(Long gradeExamId, Integer examStatus);

    /**
     * 根据班级id、学员编号和考试状态 计算数量
     *
     * @param id
     * @param userNo
     * @param examStatus
     * @return
     */
    Integer countByGradeIdAndUserNoAndExamStatus(Long id, Long userNo, Integer examStatus);

    /**
     * 根据用户编号列出指定条数的提示考试记录
     *
     * @param userNo 用户编号
     * @param num    显示记录数
     * @return 学生考试
     */
    List<GradeExamStudentRelation> limitTipsListByUserNo(Long userNo, Integer num);

    /**
     * 根据班级考试Id列出学生试卷
     *
     * @param gradeExamId 班级考试ID
     * @return 学生试卷
     */
    List<GradeExamStudentRelation> listCompleteByGradeExamId(Long gradeExamId);

    /**
     * 根据班级考试Id列出学生试卷
     *
     * @param gradeExamId 班级考试ID
     * @return 学生试卷
     */
    List<GradeExamStudentRelation> listByGradeExamId(Long gradeExamId);

    /**
     * 根据班级考试ID和评阅状态进行统计
     * @param gradeExamId
     * @param auditStatus
     * @return
     */
    int countByGradeExamIdAndAuditStatus(Long gradeExamId, Integer auditStatus);

    /**
     * 根据用户编号、班级ID、考试ID、开始时间、结束时间获取学生试卷
     * @param userNo
     * @param gradeId
     * @param examId
     * @param beginTime
     * @param endTime
     * @return
     */
    GradeExamStudentRelation getByUserNoAndGradeIdAndExamIdAndBeginExamTimeAndExamTime(Long userNo, Long gradeId, Long examId, Date beginTime, Date endTime);
}
