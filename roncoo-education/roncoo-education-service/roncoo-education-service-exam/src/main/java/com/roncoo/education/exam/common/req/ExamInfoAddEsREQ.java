package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户试卷分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamInfoUpdateREQ", description="试卷  更新操作")
public class ExamInfoAddEsREQ implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    /**
     * 状态(1:有效;0:无效)
     */
    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 试卷排序
     */
    @ApiModelProperty(value = "试卷排序")
    private Integer examSort;
    /**
     * 试卷名称
     */
    @ApiModelProperty(value = "试卷名称")
    private String examName;
    /**
     * 地区
     */
    @ApiModelProperty(value = "地区")
    private String region;

    /**
     * 是否上架(1:上架，0:下架)
     */
    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;
    /**
     * 是否免费：1免费，0收费
     */
    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;
    /**
     * 优惠价
     */
    @ApiModelProperty(value = "优惠价")
    private BigDecimal fabPrice;
    /**
     * 原价
     */
    @ApiModelProperty(value = "原价")
    private BigDecimal orgPrice;
    /**
     * 答卷时间（分钟）
     */
    @ApiModelProperty(value = "答卷时间（分钟）")
    private Integer answerTime;
    /**
     * 总分
     */
    @ApiModelProperty(value = "总分")
    private Integer scoreAccount;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private Date beginTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private Date endTime;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 关键字
     */
    @ApiModelProperty(value = "关键字")
    private String keyword;


}
