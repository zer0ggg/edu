package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级信息详情查看对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeInfoViewBO", description = "班级信息详情查看对象")
public class AuthGradeInfoViewBO extends PageParam implements Serializable {

    private static final long serialVersionUID = -8620430812316792354L;

    @NotNull(message = "年级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

}
