package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.UserExamCategorySaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamCategoryBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamCategoryUpdateBO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamCategoryDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthUserExamCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户试卷分类 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@Api(tags = "API-AUTH-用户试卷分类管理")
@RestController
@RequestMapping("/exam/auth/user/exam/category")
public class AuthUserExamCategoryController {

    @Autowired
    private AuthUserExamCategoryBiz biz;

    @ApiOperation(value = "列表接口", notes = "考试分类列表")
    @PostMapping(value = "/list")
    public Result<Page<AuthUserExamCategoryDTO>> listForPage(@RequestBody AuthUserExamCategoryBO bo) {
        return biz.list(bo);
    }

    @ApiOperation(value = "添加接口", notes = "添加接口")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody @Valid UserExamCategorySaveBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "更新接口", notes = "更新接口")
    @PostMapping(value = "/update")
    public Result<String> update(@RequestBody @Valid AuthUserExamCategoryUpdateBO bo) {
        return biz.update(bo);
    }

    @ApiOperation(value = "删除接口", notes = "删除接口")
    @DeleteMapping(value = "/delete/{id}")
    public Result<String> delete(@PathVariable(value = "id") Long id) {
        return biz.delete(id);
    }


}
