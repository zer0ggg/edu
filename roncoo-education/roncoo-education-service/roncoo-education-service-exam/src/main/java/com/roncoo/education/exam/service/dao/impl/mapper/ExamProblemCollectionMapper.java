package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollection;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamProblemCollectionMapper {
    int countByExample(ExamProblemCollectionExample example);

    int deleteByExample(ExamProblemCollectionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamProblemCollection record);

    int insertSelective(ExamProblemCollection record);

    List<ExamProblemCollection> selectByExample(ExamProblemCollectionExample example);

    ExamProblemCollection selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamProblemCollection record, @Param("example") ExamProblemCollectionExample example);

    int updateByExample(@Param("record") ExamProblemCollection record, @Param("example") ExamProblemCollectionExample example);

    int updateByPrimaryKeySelective(ExamProblemCollection record);

    int updateByPrimaryKey(ExamProblemCollection record);
}