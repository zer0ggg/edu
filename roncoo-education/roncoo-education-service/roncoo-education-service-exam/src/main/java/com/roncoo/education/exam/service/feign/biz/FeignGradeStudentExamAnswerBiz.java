package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeStudentExamAnswerQO;
import com.roncoo.education.exam.feign.vo.GradeStudentExamAnswerVO;
import com.roncoo.education.exam.service.dao.GradeStudentExamAnswerDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswerExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswerExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeStudentExamAnswerBiz extends BaseBiz {

    @Autowired
    private GradeStudentExamAnswerDao dao;

	public Page<GradeStudentExamAnswerVO> listForPage(GradeStudentExamAnswerQO qo) {
	    GradeStudentExamAnswerExample example = new GradeStudentExamAnswerExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeStudentExamAnswer> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeStudentExamAnswerVO.class);
	}

	public int save(GradeStudentExamAnswerQO qo) {
		GradeStudentExamAnswer record = BeanUtil.copyProperties(qo, GradeStudentExamAnswer.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeStudentExamAnswerVO getById(Long id) {
		GradeStudentExamAnswer record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeStudentExamAnswerVO.class);
	}

	public int updateById(GradeStudentExamAnswerQO qo) {
		GradeStudentExamAnswer record = BeanUtil.copyProperties(qo, GradeStudentExamAnswer.class);
		return dao.updateById(record);
	}

}
