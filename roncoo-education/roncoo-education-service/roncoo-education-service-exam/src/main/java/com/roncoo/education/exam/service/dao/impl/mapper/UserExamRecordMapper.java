package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamRecordExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserExamRecordMapper {
    int countByExample(UserExamRecordExample example);

    int deleteByExample(UserExamRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserExamRecord record);

    int insertSelective(UserExamRecord record);

    List<UserExamRecord> selectByExample(UserExamRecordExample example);

    UserExamRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserExamRecord record, @Param("example") UserExamRecordExample example);

    int updateByExample(@Param("record") UserExamRecord record, @Param("example") UserExamRecordExample example);

    int updateByPrimaryKeySelective(UserExamRecord record);

    int updateByPrimaryKey(UserExamRecord record);
}