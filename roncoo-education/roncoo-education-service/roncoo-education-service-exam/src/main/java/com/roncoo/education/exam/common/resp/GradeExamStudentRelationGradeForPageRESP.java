package com.roncoo.education.exam.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/***
 *
 *
 * @author  hsq
 *
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeExamStudentRelationGradeForPageRESP", description = "班级用户考试成绩分页响应对象")
public class GradeExamStudentRelationGradeForPageRESP implements Serializable {


    @ApiModelProperty(value = "主键ID")
    private Long id;

    //姓名，身份证号，手机号码，科目，成绩（选择题，判断题，总数）
    @ApiModelProperty(value = "用户编号")

    private Long userNo;
    @ApiModelProperty(value = "学生ID")

    private Long studentId;
    @ApiModelProperty(value = "姓名")

    private String nickname;
    @ApiModelProperty(value = "身份证号")

    private String idCardNo;
    @ApiModelProperty(value = "手机号")

    private String mobile;

    @ApiModelProperty(value = "状态")
    private Integer statusId;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "科目ID")
    private Long subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "开始注册时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registerTime;


    @ApiModelProperty(value = "开始答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginAnswerTime;

    @ApiModelProperty(value = "结束答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endAnswerTime;

    @ApiModelProperty(value = "考试开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginExamTime;

    @ApiModelProperty(value = "考试结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endExamTime;

    @ApiModelProperty(value = "完成考试分钟")
    private Integer minute;

    @ApiModelProperty(value = "总成绩")
    private Integer score;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "标题分数集合")
    private List<ExamTitleScoreRESP> titlelist = new ArrayList<>();



}
