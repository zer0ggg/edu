package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ExamCategoryTypeEnum;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.ProblemTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.polyv.PolyvCode;
import com.roncoo.education.common.polyv.PolyvSign;
import com.roncoo.education.common.polyv.PolyvSignResult;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeGetLastBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeVideoSignBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeViewBO;
import com.roncoo.education.exam.common.dto.ExamProblemOptionDTO;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 练习信息
 *
 * @author LHR
 */
@Slf4j
@Component
public class AuthPracticeBiz extends BaseBiz {

    @Autowired
    private PracticeDao dao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private PracticeProblemRefDao practiceProblemRefDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private PracticeUserAnswerDao practiceUserAnswerDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    public Result<Long> save(AuthPracticeSaveBO bo) {
        Practice practice = BeanUtil.copyProperties(bo, Practice.class);
        Integer result = dao.save(practice);
        if (result < 1) {
            return Result.error("保存失败!");
        }

        //按照指定的科目选择
        if (bo.getSubjectId() != null) {
            List<ExamProblem> examProblemList = examProblemDao.listBySubjectIdAndIsFreeAndProblemType(bo.getSubjectId(), IsFreeEnum.FREE.getCode());
            if (CollectionUtil.isEmpty(examProblemList)) {
                return Result.error("没有符合条件的试题,请重新选择!");
            }
            //分配试题
            allotProblem(bo, practice.getId(), examProblemList);
        }else {
            //选择了全部，则按照所有分类进行30%的抽取
            List<ExamCategory> examCategoryList = examCategoryDao.listByExamCategoryTypeAndFloorAndStatusId(ExamCategoryTypeEnum.SUBJECT.getCode(), 1, StatusIdEnum.YES.getCode());
            for (ExamCategory examCategory : examCategoryList) {
                //根据类型获取符合条件的试题
                List<ExamProblem> examProblemList = examProblemDao.listBySubjectIdAndIsFreeAndProblemType(examCategory.getId(), IsFreeEnum.FREE.getCode());
                if (CollectionUtil.isEmpty(examProblemList)) {
                    continue;
                }
                //分配试题
                allotProblem(bo, practice.getId(), examProblemList);
            }
        }
        return Result.success(practice.getId());
    }

    /**
     * 分配试题
     * @param bo
     * @param practiceId
     * @param examProblemList
     */
    private void allotProblem(AuthPracticeSaveBO bo, Long practiceId, List<ExamProblem> examProblemList) {
        //根据对应试题数量获取30%的试题量
        Integer examProblemEnum = BigDecimal.valueOf(examProblemList.size()).multiply(BigDecimal.valueOf(0.3)).setScale(0, BigDecimal.ROUND_UP).intValue();
        //随机获取定位点
        Integer random = 0;
        if (examProblemList.size() == 1) {
            random = 0;
        }else {
            random = RandomUtil.randomInt(0, examProblemList.size() - examProblemEnum);
        }
        Integer theRadioNum = 0;
        Integer multipleChoiceNum = 0;
        Integer estimateNum = 0;
        for (Integer i = 0; i < examProblemEnum; i++) {
            PracticeProblemRef practiceProblemRef = new PracticeProblemRef();
            practiceProblemRef.setUserNo(ThreadContext.userNo());
            practiceProblemRef.setPracticeId(practiceId);
            practiceProblemRef.setProblemId(examProblemList.get(random + i).getId());
            practiceProblemRef.setProblemType(examProblemList.get(random + i).getProblemType());
            practiceProblemRefDao.save(practiceProblemRef);
            //更新练习数量
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(practiceProblemRef.getProblemType())) {
                theRadioNum = theRadioNum + 1;
            }
            if (ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(practiceProblemRef.getProblemType())) {
                multipleChoiceNum = multipleChoiceNum + 1;
            }
            if (ProblemTypeEnum.ESTIMATE.getCode().equals(practiceProblemRef.getProblemType())) {
                estimateNum = estimateNum + 1;
            }
        }
        Practice practice = new Practice();
        practice.setId(practiceId);
        practice.setTheRadioNum(theRadioNum);
        practice.setMultipleChoiceNum(multipleChoiceNum);
        practice.setEstimateNum(estimateNum);
        dao.updateById(practice);
    }

    public Result<Long> getLast(AuthPracticeGetLastBO bo) {
        Practice practice = dao.getLastByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNotEmpty(practice)) {
            return Result.success(practice.getId());
        }
        return null;
    }

    public Result<Page<AuthPracticeListForPageDTO>> listForPage(AuthPracticeListForPageDTO bo) {
        PracticeExample example = new PracticeExample();
        PracticeExample.Criteria criteria = example.createCriteria();
        criteria.andUserNoEqualTo(bo.getUserNo());
        example.setOrderByClause(" id desc");
        Page<Practice> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(page, AuthPracticeListForPageDTO.class));
    }

    public Result<AuthPracticeViewDTO> view(AuthPracticeViewBO bo) {
        Practice practice = dao.getById(bo.getId());
        if (ObjectUtil.isNull(practice)) {
            return Result.success(null);
        }
        AuthPracticeViewDTO dto = BeanUtil.copyProperties(practice, AuthPracticeViewDTO.class);
        List<PracticeProblemRef> list = practiceProblemRefDao.listByPracticeId(practice.getId());
        if (CollectionUtil.isEmpty(list)) {
            return Result.success(dto);
        }

        List<AuthPracticeProblemViewDTO> problemList = new ArrayList<>();
        for (PracticeProblemRef practiceProblemRef : list) {
            //获取练习试题详情
            ExamProblem examProblem = examProblemDao.getById(practiceProblemRef.getProblemId());
            if (ObjectUtil.isNull(examProblem)) {
                continue;
            }
            AuthPracticeProblemViewDTO authPracticeProblemViewDTO = BeanUtil.copyProperties(examProblem, AuthPracticeProblemViewDTO.class);
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(examProblem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examProblem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examProblem.getProblemType())) {
                // 获取选项
                List<ExamProblemOption> problemOptionList = examProblemOptionDao.listByProblemIdWithBLOBs(examProblem.getId());
                authPracticeProblemViewDTO.setOptionList(BeanUtil.copyProperties(problemOptionList, ExamProblemOptionDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(examProblem.getProblemType())) {
                // 若是填空题、获取填空个数
                authPracticeProblemViewDTO.setOptionCount(StrUtil.isBlank(examProblem.getProblemAnswer()) ? 0 : examProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
            }
            //获取用户该试题答案
            List<PracticeUserAnswer> answerList = practiceUserAnswerDao.listByUserNoAndPracticeId(ThreadContext.userNo(), practice.getId());
            if (CollectionUtil.isNotEmpty(answerList)) {
                authPracticeProblemViewDTO.setAnswerList(PageUtil.copyList(answerList, AuthPracticeUserAnswerDTO.class));
            }
            problemList.add(authPracticeProblemViewDTO);
        }
        dto.setProblemList(problemList);
        return Result.success(dto);
    }

    @Transactional
    public Result<Integer> delete(Long userNo, Long id) {
        if (null == userNo ) {
            return Result.error("用户编号不能为空!");
        }
        Practice practice = dao.getById(id);
        if (ObjectUtil.isNull(practice)) {
            return Result.success(null);
        }
        if (!userNo.equals(practice.getUserNo())) {
            return Result.error("用户信息不匹配,不允许删除!");
        }
        //删除用户答案
        practiceUserAnswerDao.deleteByUserNoAndPracticeId(userNo, id);
        practiceProblemRefDao.deleteByUserNoAndPracticeId(userNo, id);
        dao.deleteById(id);
        return Result.success(1);
    }

    public Result<AuthPracticeVideoSignDTO> sign(AuthPracticeVideoSignBO bo) {
        // 查找用户信息
        UserExtVO userextVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userextVO)) {
            return Result.error("用户不存在，系统管理员");
        }
        if (StatusIdEnum.NO.getCode().equals(userextVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系客服");
        }
        ExamProblem examProblem = examProblemDao.getById(bo.getProblemId());
        if (ObjectUtil.isNull(examProblem)) {
            return Result.error("试题不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(examProblem.getStatusId())) {
            return Result.error("试题已禁用");
        }

        return Result.success(getSign(bo));
    }

    /**
     * 获取播放sign值
     *
     * @return sign值
     */
    private AuthPracticeVideoSignDTO getSign(AuthPracticeVideoSignBO bo) {
        ConfigPolyvVO sys = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sys)) {
            try {
                throw new Exception("找不到系统配置信息");
            } catch (Exception e) {
                logger.error("获取播放sign值失败", e);
            }
        }
        if (StringUtils.isEmpty(sys.getPolyvUseid()) || StringUtils.isEmpty(sys.getPolyvSecretkey())) {
            try {
                throw new Exception("useid或secretkey未配置");
            } catch (Exception e) {
                logger.error("获取播放sign值失败", e);
            }
        }

        PolyvSign polyvSign = new PolyvSign();
        polyvSign.setIp(bo.getIp());
        polyvSign.setUserNo(ThreadContext.userNo());
        polyvSign.setVid(bo.getVideoVid());
        PolyvSignResult signResult = PolyvUtil.getSignForH5(polyvSign, sys.getPolyvUseid(), sys.getPolyvSecretkey());
        AuthPracticeVideoSignDTO dto = BeanUtil.copyProperties(signResult, AuthPracticeVideoSignDTO.class);
        PolyvCode polyvCode = new PolyvCode();
        polyvCode.setUserNo(ThreadContext.userNo());
        polyvCode.setProblemId(bo.getProblemId());
        polyvCode.setPeriodId(0L);
        polyvCode.setExamId(0L);
        dto.setCode(PolyvUtil.getPolyvCode(polyvCode));
        return dto;
    }

}
