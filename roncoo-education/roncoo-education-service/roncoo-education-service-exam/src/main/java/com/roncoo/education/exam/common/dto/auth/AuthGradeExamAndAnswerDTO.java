package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 班级考试带答案响应对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamAndAnswerDTO", description = "班级考试带答案响应对象")
public class AuthGradeExamAndAnswerDTO implements Serializable {

    private static final long serialVersionUID = -1711154082249051562L;

    @ApiModelProperty(value = "考试ID")
    private Long id;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "讲师用户名称")
    private String lecturerUserName;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级名称")
    private String gradeName;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;

    @ApiModelProperty(value = "答案展示(1:不展示，2:交卷即展示，3::考试结束后展示，4:自定义)")
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    private Date answerShowTime;

    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)")
    private Integer auditType;

    @ApiModelProperty(value = "评阅状态（1：未评阅、2，系统评阅，3：评阅中、4：评阅完成）")
    private Integer auditStatus;

    @ApiModelProperty(value = "试卷标题")
    private List<AuthGradeExamTitleAndAnswerDTO> titleList;
}
