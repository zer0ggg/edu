package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample;

import java.util.List;

/**
 * 用户试卷 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface UserExamDao {

    /**
     * 保存用户试卷
     *
     * @param record 用户试卷
     * @return 影响记录数
     */
    int save(UserExam record);

    /**
     * 根据ID删除用户试卷
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 用户试卷
     * @return 影响记录数
     */
    int updateById(UserExam record);

    /**
     * 根据ID获取用户试卷
     *
     * @param id 主键ID
     * @return 用户试卷
     */
    UserExam getById(Long id);

    /**
     * 用户试卷--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<UserExam> listForPage(int pageCurrent, int pageSize, UserExamExample example);


    /**
     * 根据用户编号和试卷ID获取用户试卷
     *
     * @param userNo 用户编号
     * @param examId 试卷ID
     * @return 用户试卷
     */
    UserExam getByUserNoAndExamId(Long userNo, Long examId);

    /**
     * 根据用户编号和试卷ID列出用户试卷
     *
     * @param userNo 用户编号
     * @return 用户试卷
     */
    List<UserExam> listByUserNo(Long userNo);

}
