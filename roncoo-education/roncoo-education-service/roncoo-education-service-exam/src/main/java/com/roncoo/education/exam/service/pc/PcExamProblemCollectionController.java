package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamProblemCollectionEditREQ;
import com.roncoo.education.exam.common.req.ExamProblemCollectionListREQ;
import com.roncoo.education.exam.common.req.ExamProblemCollectionSaveREQ;
import com.roncoo.education.exam.common.resp.ExamProblemCollectionListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemCollectionViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamProblemCollectionBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷题目收藏 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷题目收藏管理")
@RestController
@RequestMapping("/exam/pc/exam/problem/collection")
public class PcExamProblemCollectionController {

    @Autowired
    private PcExamProblemCollectionBiz biz;

    @ApiOperation(value = "试卷题目收藏列表", notes = "试卷题目收藏列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamProblemCollectionListRESP>> list(@RequestBody ExamProblemCollectionListREQ examProblemCollectionListREQ) {
        return biz.list(examProblemCollectionListREQ);
    }

    @ApiOperation(value = "试卷题目收藏添加", notes = "试卷题目收藏添加")
    @SysLog(value = "试卷题目收藏添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamProblemCollectionSaveREQ examProblemCollectionSaveREQ) {
        return biz.save(examProblemCollectionSaveREQ);
    }

    @ApiOperation(value = "试卷题目收藏查看", notes = "试卷题目收藏查看")
    @GetMapping(value = "/view")
    public Result<ExamProblemCollectionViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷题目收藏修改", notes = "试卷题目收藏修改")
    @SysLog(value = "试卷题目收藏修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamProblemCollectionEditREQ examProblemCollectionEditREQ) {
        return biz.edit(examProblemCollectionEditREQ);
    }

    @ApiOperation(value = "试卷题目收藏删除", notes = "试卷题目收藏删除")
    @SysLog(value = "试卷题目收藏删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
