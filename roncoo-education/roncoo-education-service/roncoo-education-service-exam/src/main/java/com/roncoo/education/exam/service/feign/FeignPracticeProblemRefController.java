package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignPracticeProblemRef;
import com.roncoo.education.exam.feign.qo.PracticeProblemRefQO;
import com.roncoo.education.exam.feign.vo.PracticeProblemRefVO;
import com.roncoo.education.exam.service.feign.biz.FeignPracticeProblemRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 题目练习关联
 *
 * @author LHR
 * @date 2020-10-10
 */
@RestController
public class FeignPracticeProblemRefController extends BaseController implements IFeignPracticeProblemRef{

    @Autowired
    private FeignPracticeProblemRefBiz biz;

	@Override
	public Page<PracticeProblemRefVO> listForPage(@RequestBody PracticeProblemRefQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody PracticeProblemRefQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody PracticeProblemRefQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public PracticeProblemRefVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
