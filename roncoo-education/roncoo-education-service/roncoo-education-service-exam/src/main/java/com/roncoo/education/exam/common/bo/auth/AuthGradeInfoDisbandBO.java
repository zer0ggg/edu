package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级解散对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeInfoDisbandBO", description = "班级解散对象")
public class AuthGradeInfoDisbandBO implements Serializable {

    private static final long serialVersionUID = 2389163737908803479L;

    @NotNull(message = "主键ID")
    @ApiModelProperty(value = "主键ID", required = true)
    private Long id;
}
