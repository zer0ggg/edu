package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户考试记录
 *
 * @author LYQ
 */
@Component
public class AuthUserExamRecordBiz extends BaseBiz {

    @Autowired
    private UserExamRecordDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;
    @Autowired
    private UserExamAnswerDao userExamAnswerDao;
    @Autowired
    private UserExamDao userExamDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private UserExamTitleScoreDao userExamTitleScoreDao;
    @Autowired
    private UserOrderExamRefDao userOrderExamRefDao;
    @Autowired
    private CourseExamRefDao courseExamRefDao;

    /**
     * 分页列出用户考试记录
     *
     * @param bo 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthUserExamRecordPageDTO>> listForPage(AuthUserExamRecordPageBO bo) {
        UserExamRecordExample example = new UserExamRecordExample();
        UserExamRecordExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        example.setOrderByClause("sort asc, id desc");
        Page<UserExamRecord> userExamRecordPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserExamRecordPageDTO> resultPage = PageUtil.transform(userExamRecordPage, AuthUserExamRecordPageDTO.class);
        if (CollectionUtil.isEmpty(resultPage.getList())) {
            return Result.success(resultPage);
        }
        for (AuthUserExamRecordPageDTO pageDTO : resultPage.getList()) {
            pageDTO.setBeginTime(pageDTO.getGmtCreate());

            // 获取试卷信息
            ExamInfo examInfo = examInfoDao.getById(pageDTO.getExamId());
            if (ObjectUtil.isNotNull(examInfo)) {
                pageDTO.setExamName(examInfo.getExamName());
                pageDTO.setCountBuy(examInfo.getCountBuy());
                pageDTO.setStudyCount(examInfo.getStudyCount());
                pageDTO.setCollectionCount(examInfo.getCollectionCount());
                pageDTO.setDownloadCount(examInfo.getDownloadCount());
                pageDTO.setGraId(examInfo.getGraId());
                pageDTO.setSubjectId(examInfo.getSubjectId());
                pageDTO.setYearId(examInfo.getYearId());
                pageDTO.setSourceId(examInfo.getSourceId());

                // 获取分类信息
                getExamRecordPageCategoryInfo(pageDTO);


            }
        }
        return Result.success(resultPage);
    }

    /**
     * 在线考试
     *
     * @param onlineBO 在线考试参数
     * @return 处理结果
     */
    public Result<AuthExamOnlineDTO> examOnline(AuthExamOnlineBO onlineBO) {
        // 判断当前用户是否在考试
        List<UserExamRecord> userExamRecordList = dao.listByUserNoAndExamStatus(ThreadContext.userNo(), ExamStatusEnum.NOT_OVER.getCode());
        if (CollectionUtil.isNotEmpty(userExamRecordList)) {
            return Result.error("用户处于考试用，不能开始新的考试");
        }
        // 获取试卷信息
        ExamInfo examInfo = examInfoDao.getById(onlineBO.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("获取试卷信息失败");
        }
        //TODO 判断是否已购买
        if (!IsFreeEnum.FREE.getCode().equals(examInfo.getIsFree())) {
            UserOrderExamRef userOrderExamRef = userOrderExamRefDao.getByUserNoAndExamId(ThreadContext.userNo(), examInfo.getId());
            if (ObjectUtil.isNull(userOrderExamRef)) {
                return Result.error("当前试卷未购买，无法开始考试");
            }
        }
        // 获取试卷标题
        List<ExamTitle> examTitleList = examTitleDao.listByExamIdAndStatusId(examInfo.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(examTitleList)) {
            return Result.error("获取试卷大题失败");
        }
        List<AuthExamTitleListDTO> examTitleListDTOList = new ArrayList<>();
        for (ExamTitle examTitle : examTitleList) {
            AuthExamTitleListDTO titleListDTO = BeanUtil.copyProperties(examTitle, AuthExamTitleListDTO.class);
            // 获取标题试题关系
            List<AuthExamTitleProblemDTO> problemList;
            if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
                // 组合题
                problemList = getCombinationProblemByTitleId(examTitle.getId());
            } else {
                // 其他类型题目
                problemList = getOtherProblemByTitleId(examTitle.getId(), examTitle.getTitleType());
            }
            titleListDTO.setProblemList(problemList);
            examTitleListDTOList.add(titleListDTO);
        }

        // 生成考试记录
        UserExamRecord record = new UserExamRecord();
        record.setUserNo(ThreadContext.userNo());
        record.setExamId(examInfo.getId());
        record.setTotalScore(examInfo.getTotalScore());
        dao.save(record);

        // 添加我的考试
        UserExam userExam = userExamDao.getByUserNoAndExamId(ThreadContext.userNo(), examInfo.getId());
        if (ObjectUtil.isNull(userExam)) {
            userExam = new UserExam();
            userExam.setUserNo(ThreadContext.userNo());
            userExam.setExamId(examInfo.getId());
            userExamDao.save(userExam);
        }

        // 封装返回开考参数
        AuthExamOnlineDTO onlineDTO = new AuthExamOnlineDTO();
        onlineDTO.setRecordId(record.getId());
        onlineDTO.setExamName(examInfo.getExamName());
        onlineDTO.setAnswerTime(examInfo.getAnswerTime() * 60);
        onlineDTO.setTotalScore(examInfo.getTotalScore());
        onlineDTO.setTitleList(examTitleListDTOList);
        return Result.success(onlineDTO);
    }

    /**
     * 继续考试
     *
     * @param bo 继续考试参数
     * @return 处理结果
     */
    public Result<AuthExamContinueDTO> examContinue(AuthExamContinueBO bo) {
        // 校验考试记录
        UserExamRecord userExamRecord = dao.getById(bo.getRecordId());
        if (ObjectUtil.isNull(userExamRecord) || !userExamRecord.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("考试记录不存在");
        }
        ExamInfo examInfo = examInfoDao.getById(userExamRecord.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("试卷不存在");
        }
        if (!DateUtil.offsetMinute(userExamRecord.getGmtCreate(), examInfo.getAnswerTime()).after(new Date())) {
            return Result.error("当前考试已结束");
        }

        // 获取试卷标题
        List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
        if (CollectionUtil.isEmpty(examTitleList)) {
            return Result.error("获取试卷大题失败");
        }

        List<AuthExamTitleListWithUserAnswerDTO> examTitleDTOList = new ArrayList<>();
        for (ExamTitle examTitle : examTitleList) {
            AuthExamTitleListWithUserAnswerDTO titleDTO = BeanUtil.copyProperties(examTitle, AuthExamTitleListWithUserAnswerDTO.class);
            // 获取标题的题目
            List<AuthExamTitleProblemWithUserAnswerDTO> problemDTOList;
            if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
                // 组合题
                problemDTOList = getCombinationProblemWithUserAnswerByTitleId(userExamRecord.getId(), examTitle.getId());
            } else {
                // 其他类型题目
                problemDTOList = getOtherProblemWithUserAnswerByTitleId(userExamRecord.getId(), examTitle);
            }
            titleDTO.setProblemList(problemDTOList);
            examTitleDTOList.add(titleDTO);
        }


        long a = DateUtil.offsetMinute(userExamRecord.getGmtCreate(), examInfo.getAnswerTime()).getTime();
        long b = new Date().getTime();
        int c = (int) ((a - b) / 1000);

        //封装返回
        AuthExamContinueDTO continueDTO = new AuthExamContinueDTO();
        continueDTO.setRecordId(userExamRecord.getId());
        continueDTO.setExamName(examInfo.getExamName());
        continueDTO.setBeginTime(userExamRecord.getGmtCreate());
        continueDTO.setAnswerTime(c);
        continueDTO.setTotalScore(examInfo.getTotalScore());
        continueDTO.setTitleList(examTitleDTOList);
        return Result.success(continueDTO);
    }


    /**
     * 保存试题答案
     *
     * @param bo 试题答案
     * @return 保存结果
     */
    public Result<String> saveProblemAnswer(AuthExamSaveProblemAnswerBO bo) {
        // 判断考试记录
        UserExamRecord userExamRecord = dao.getById(bo.getRecordId());
        if (ObjectUtil.isNull(userExamRecord) || !userExamRecord.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("考试不存在");
        }
        if (!ExamStatusEnum.NOT_OVER.getCode().equals(userExamRecord.getExamStatus())) {
            return Result.error("当前考试不是考试中，不能提交答案");
        }

        // 判断试卷
        ExamInfo examInfo = examInfoDao.getById(userExamRecord.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("试卷不存在");
        }
        if (!DateUtil.offsetMinute(userExamRecord.getGmtCreate(), examInfo.getAnswerTime()).after(new Date())) {
            return Result.error("当前考试已结束");
        }

        // 判断标题是否存在
        ExamTitle examTitle = examTitleDao.getById(bo.getTitleId());
        if (ObjectUtil.isNull(examTitle) || !examInfo.getId().equals(examTitle.getExamId())) {
            return Result.error("试卷标题不存在");
        }

        // 判断题目
        ExamProblem examProblem = examProblemDao.getById(bo.getProblemId());
        if (ObjectUtil.isNull(examProblem)) {
            return Result.error("题目不存在");
        }
        ExamTitleProblemRef subExamTitleProblemRef = examTitleProblemRefDao.getByTitleIdAndProblemParentIdAndProblemId(examTitle.getId(), examProblem.getParentId(), examProblem.getId());
        if (ObjectUtil.isNull(subExamTitleProblemRef)) {
            return Result.error("试卷不存在该题目");
        }

        // 保存或者更新用户答案
        UserExamAnswer userExamAnswer = userExamAnswerDao.getByRecordIdAndTitleIdAndProblemId(userExamRecord.getId(), examTitle.getId(), examProblem.getId());
        if (ObjectUtil.isNotNull(userExamAnswer)) {
            userExamAnswer.setUserAnswer(StrUtil.isBlank(bo.getAnswerContent()) ? "" : bo.getAnswerContent());
            userExamAnswerDao.updateById(userExamAnswer);
            return Result.success("保存成功");
        }

        userExamAnswer = new UserExamAnswer();
        userExamAnswer.setUserNo(ThreadContext.userNo());
        userExamAnswer.setRecordId(bo.getRecordId());
        userExamAnswer.setExamId(examInfo.getId());
        userExamAnswer.setTitleId(examTitle.getId());
        userExamAnswer.setProblemParentId(examProblem.getParentId());
        userExamAnswer.setProblemId(examProblem.getId());
        userExamAnswer.setProblemType(examProblem.getProblemType());
        userExamAnswer.setUserAnswer(bo.getAnswerContent());
        userExamAnswer.setProblemScore(subExamTitleProblemRef.getScore());
        userExamAnswer.setScore(0);
        userExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.WAIT.getCode());
        userExamAnswerDao.save(userExamAnswer);
        return Result.success("保存成功");
    }

    /**
     * 考试完成
     *
     * @param finishBO 考试完成处理参数
     * @return 处理结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthExamFinishDTO> examFinish(AuthExamFinishBO finishBO) {
        UserExamRecord examRecord = dao.getByIdForUpdate(finishBO.getRecordId());
        if (ObjectUtil.isNull(examRecord) || !ThreadContext.userNo().equals(examRecord.getUserNo())) {
            return Result.error("用户考试记录不存在");
        }
        if (!ExamStatusEnum.NOT_OVER.getCode().equals(examRecord.getExamStatus())) {
            // 不是考试中直接返回
            return getExamFinisResult(examRecord);
        }
        ExamInfo examInfo = examInfoDao.getById(examRecord.getExamId());
        if (ObjectUtil.isEmpty(examInfo)) {
            return Result.error("试卷不存在");
        }

        // 结束时间
        Date beginTime = examRecord.getGmtCreate();
        Date endTime = new Date();
        int answerTime = (int) DateUtil.between(beginTime, endTime, DateUnit.MINUTE);

        // 获取试卷标题
        List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
        if (ObjectUtil.isEmpty(examTitleList)) {
            return Result.success(createAuthExamFinishDTO(examInfo.getExamName(), beginTime, answerTime, endTime, 0, 0, 0, 0, null));
        }

        List<UserExamTitleScore> userExamTitleScoreList = new ArrayList<>();
        List<AuthExamFinishTitleScoreDTO> titleScoreList = new ArrayList<>();
        for (ExamTitle examTitle : examTitleList) {
            int titleTotalScore = 0;
            // 获取标题题目关联（包含了该题在当前试卷的得分）
            List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleId(examTitle.getId());
            if (CollectionUtil.isEmpty(refList)) {
                titleScoreList.add(createFinishTitleScore(examTitle, titleTotalScore, 0, 0));
                continue;
            }

            // 统计分数
            titleTotalScore = refList.stream().mapToInt(ExamTitleProblemRef::getScore).sum();

            // 判分
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitle.getTitleType())) {
                // 单选题、多选或判断题
                UserExamTitleScore userExamTitleScore = theRadioOrMultipleChoiceOrEstimateCheck(finishBO.getRecordId(), examTitle, refList);

                userExamTitleScore.setSysAuditTotalScore(titleTotalScore);
                userExamTitleScoreList.add(userExamTitleScore);

                // 添加计分结果
                titleScoreList.add(createFinishTitleScore(examTitle, titleTotalScore, userExamTitleScore.getScore(), 0));
            } else if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
                // 组合题判分
                UserExamTitleScore userExamTitleScore = materialCheck(finishBO.getRecordId(), examTitle, refList);

                userExamTitleScoreList.add(userExamTitleScore);
                titleScoreList.add(createFinishTitleScore(examTitle, userExamTitleScore.getSysAuditTotalScore() + userExamTitleScore.getHandworkAuditTotalScore(), userExamTitleScore.getScore(), userExamTitleScore.getHandworkAuditTotalScore()));
            } else {
                // 其他类型判分 -- 填空题、解答题
                UserExamTitleScore userExamTitleScore = otherProblemCheck(finishBO.getRecordId(), examTitle, refList);
                userExamTitleScoreList.add(userExamTitleScore);
                titleScoreList.add(createFinishTitleScore(examTitle, titleTotalScore, 0, userExamTitleScore.getHandworkAuditTotalScore()));
            }
        }

        // 统计分数
        int sysAuditTotalScore = userExamTitleScoreList.stream().mapToInt(UserExamTitleScore::getSysAuditTotalScore).sum();
        int sysAuditScore = userExamTitleScoreList.stream().mapToInt(UserExamTitleScore::getSysAuditScore).sum();
        int handworkAuditTotalScore = userExamTitleScoreList.stream().mapToInt(UserExamTitleScore::getHandworkAuditTotalScore).sum();

        // 保存考试标题成绩
        for (UserExamTitleScore userExamTitleScore : userExamTitleScoreList) {
            userExamTitleScoreDao.save(userExamTitleScore);
        }

        // 修改考试记录
        UserExamRecord updateExamRecord = new UserExamRecord();
        updateExamRecord.setId(examRecord.getId());
        updateExamRecord.setAnswerTime(answerTime);
        updateExamRecord.setEndTime(endTime);
        updateExamRecord.setTotalScore(examInfo.getTotalScore());
        updateExamRecord.setScore(sysAuditScore);
        updateExamRecord.setSysAuditTotalScore(sysAuditTotalScore);
        updateExamRecord.setSysAuditScore(sysAuditScore);
        updateExamRecord.setHandworkAuditTotalScore(handworkAuditTotalScore);
        updateExamRecord.setExamStatus(handworkAuditTotalScore > 0 ? ExamStatusEnum.FINISH.getCode() : ExamStatusEnum.COMPLETE.getCode());
        dao.updateById(updateExamRecord);

        // 返回
        return Result.success(createAuthExamFinishDTO(examInfo.getExamName(), beginTime, answerTime, endTime, examInfo.getTotalScore(), sysAuditScore, sysAuditTotalScore, handworkAuditTotalScore, titleScoreList));
    }

    /**
     * 根据标题ID获取组合题下的题目
     *
     * @param titleId 标题ID
     * @return 小题集合
     */
    private List<AuthExamTitleProblemDTO> getCombinationProblemByTitleId(Long titleId) {
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleIdAndStatusId(titleId, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(refList)) {
            return null;
        }
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return null;
        }

        // 转Map
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, problem -> problem));

        // 获取组合题
        List<ExamTitleProblemRef> materialProblemRefList = refList.stream().filter(problemRef -> ExamProblemTypeEnum.MATERIAL.getCode().equals(problemRef.getProblemType())).collect(Collectors.toList());

        List<AuthExamTitleProblemDTO> problemDTOList = new ArrayList<>();
        for (ExamTitleProblemRef problemRef : materialProblemRefList) {
            ExamProblem problem = problemMap.get(problemRef.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }
            AuthExamTitleProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamTitleProblemDTO.class);

            // 获取小题
            List<ExamTitleProblemRef> subProblemRefList = refList.stream().filter(subProblemRef -> problem.getId().equals(subProblemRef.getProblemParentId())).collect(Collectors.toList());

            int problemScore = 0;
            List<AuthExamTitleProblemDTO> subProblemDTOList = new ArrayList<>();
            for (ExamTitleProblemRef subProblemRef : subProblemRefList) {
                ExamProblem subProblem = problemMap.get(subProblemRef.getProblemId());
                if (ObjectUtil.isNull(subProblem)) {
                    continue;
                }

                AuthExamTitleProblemDTO subProblemDTO = BeanUtil.copyProperties(subProblem, AuthExamTitleProblemDTO.class);
                subProblemDTO.setScore(subProblemRef.getScore());
                problemScore += subProblemRef.getScore();

                if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblem.getProblemType())) {
                    // 获取选项
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdAndStatusId(subProblem.getId(), StatusIdEnum.YES.getCode());
                    subProblemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamTitleProblemOptionDTO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblem.getProblemType())) {
                    // 填空题--获取填空数
                    subProblemDTO.setOptionCount(StrUtil.isBlank(subProblem.getProblemAnswer()) ? 0 : subProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                }
                subProblemDTOList.add(subProblemDTO);
            }

            problemDTO.setScore(problemScore);
            problemDTO.setChildrenList(subProblemDTOList);
            problemDTOList.add(problemDTO);
        }
        return problemDTOList;
    }

    /**
     * 根据标题ID获取组合题下的题目--带用户答案
     *
     * @param recordId 记录ID
     * @param titleId  标题ID
     * @return 小题集合
     */
    private List<AuthExamTitleProblemWithUserAnswerDTO> getCombinationProblemWithUserAnswerByTitleId(Long recordId, Long titleId) {
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleIdAndStatusId(titleId, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(refList)) {
            return null;
        }
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return null;
        }
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, problem -> problem));

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, titleId);
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }
        Map<Long, UserExamAnswer> userExamAnswerMap = userExamAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));

        // 获取组合题
        List<ExamTitleProblemRef> materialProblemRefList = refList.stream().filter(problemRef -> ExamProblemTypeEnum.MATERIAL.getCode().equals(problemRef.getProblemType())).collect(Collectors.toList());

        // 循环
        List<AuthExamTitleProblemWithUserAnswerDTO> problemDTOList = new ArrayList<>();
        for (ExamTitleProblemRef problemRef : materialProblemRefList) {
            ExamProblem problem = problemMap.get(problemRef.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }
            AuthExamTitleProblemWithUserAnswerDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamTitleProblemWithUserAnswerDTO.class);

            List<ExamTitleProblemRef> subProblemRefList = refList.stream().filter(subProblemRef -> problem.getId().equals(subProblemRef.getProblemParentId())).collect(Collectors.toList());
            // 获取组合题小题
            int problemScore = 0;
            List<AuthExamTitleProblemWithUserAnswerDTO> subProblemDTOList = new ArrayList<>();
            for (ExamTitleProblemRef subProblemRef : subProblemRefList) {
                ExamProblem subProblem = examProblemDao.getById(subProblemRef.getProblemId());
                if (ObjectUtil.isNull(subProblem)) {
                    continue;
                }
                problemScore += subProblemRef.getScore();

                AuthExamTitleProblemWithUserAnswerDTO subProblemDTO = BeanUtil.copyProperties(subProblem, AuthExamTitleProblemWithUserAnswerDTO.class);
                subProblemDTO.setScore(subProblemRef.getScore());
                UserExamAnswer userExamAnswer = userExamAnswerMap.get(subProblem.getId());
                if (ObjectUtil.isNotNull(userExamAnswer)) {
                    subProblemDTO.setUserAnswer(userExamAnswer.getUserAnswer());
                }

                if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblemRef.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblemRef.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblemRef.getProblemType())) {
                    // 获取选项
                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdAndStatusId(subProblem.getId(), StatusIdEnum.YES.getCode());
                    subProblemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamProblemOptionDTO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblemRef.getProblemType())) {
                    // 填空题--获取填空数
                    subProblemDTO.setOptionCount(StrUtil.isBlank(subProblem.getProblemAnswer()) ? 0 : subProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                }
                subProblemDTOList.add(subProblemDTO);
            }
            problemDTO.setScore(problemScore);
            problemDTO.setChildrenList(subProblemDTOList);
            problemDTOList.add(problemDTO);
        }
        return problemDTOList;
    }

    /**
     * 根据标题ID获其他类型标题下的小题目
     *
     * @param titleId 标题ID
     * @return 小题集合
     */
    private List<AuthExamTitleProblemDTO> getOtherProblemByTitleId(Long titleId, Integer titleType) {
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleIdAndStatusId(titleId, StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(refList)) {
            return null;
        }

        List<AuthExamTitleProblemDTO> problemDTOList = new ArrayList<>();
        for (ExamTitleProblemRef problemRef : refList) {
            ExamProblem problem = examProblemDao.getById(problemRef.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }

            AuthExamTitleProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamTitleProblemDTO.class);
            problemDTO.setScore(problemRef.getScore());
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(titleType) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(titleType) || ProblemTypeEnum.ESTIMATE.getCode().equals(titleType)) {
                // 获取选项
                List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdAndStatusId(problem.getId(), StatusIdEnum.YES.getCode());
                problemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamTitleProblemOptionDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(titleType)) {
                // 填空题--获取填空数
                problemDTO.setOptionCount(StrUtil.isBlank(problem.getProblemAnswer()) ? 0 : problem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
            }
            problemDTOList.add(problemDTO);
        }
        return problemDTOList;
    }

    /**
     * 根据标题ID获其他类型标题下的小题目--带答案
     *
     * @param recordId  记录ID
     * @param examTitle 试卷标题
     * @return 小题集合
     */
    private List<AuthExamTitleProblemWithUserAnswerDTO> getOtherProblemWithUserAnswerByTitleId(Long recordId, ExamTitle examTitle) {
        List<ExamTitleProblemRef> problemRefList = examTitleProblemRefDao.listByTitleIdAndStatusId(examTitle.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isEmpty(problemRefList)) {
            return null;
        }

        //保留关联列表
        Map<Long, ExamTitleProblemRef> problemRefMap = problemRefList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, examTitleProblemRef->examTitleProblemRef));
        //查出 试题主键
        List<Long> problemIdList = problemRefList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);

        List<AuthExamTitleProblemWithUserAnswerDTO> problemDTOList = new ArrayList<>();
        if (!ProblemTypeEnum.MATERIAL.getCode().equals(examTitle.getTitleType())) {
            for (ExamProblem problem : problemList) {
                AuthExamTitleProblemWithUserAnswerDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamTitleProblemWithUserAnswerDTO.class);
                //保留试题关联中的分数值
                problemDTO.setScore(problemRefMap.get(problemDTO.getId()).getScore());
                if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitle.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitle.getTitleType())) {
                    // 获取选项
                    List<ExamProblemOption> problemOptionList = examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId());
                    problemDTO.setOptionList(BeanUtil.copyProperties(problemOptionList, AuthExamProblemOptionDTO.class));
                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(examTitle.getTitleType())) {
                    // 若是填空题、获取填空个数
                    problemDTO.setOptionCount(StrUtil.isBlank(problem.getProblemAnswer()) ? 0 : problem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                }
                problemDTOList.add(problemDTO);
            }
        }

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, examTitle.getId());
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }
        Map<Long, UserExamAnswer> userProblemAnswerMap = userExamAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));

        problemDTOList.stream().forEach(item -> {
            UserExamAnswer userExamAnswer = userProblemAnswerMap.get(item.getId());
            if (ObjectUtil.isNotNull(userExamAnswer)) {
                item.setUserAnswer(userExamAnswer.getUserAnswer());
            }
        });
        return problemDTOList;
    }

    /**
     * 单选、多选或者判断题判断
     *
     * @param recordId  记录ID
     * @param examTitle 试卷标题
     * @param refList   试卷标题题目关联集合
     * @return 标题得分
     */
    private UserExamTitleScore theRadioOrMultipleChoiceOrEstimateCheck(Long recordId, ExamTitle examTitle, List<ExamTitleProblemRef> refList) {
        // 保存标题得分
        UserExamTitleScore userExamTitleScore = new UserExamTitleScore();
        userExamTitleScore.setExamId(examTitle.getExamId());
        userExamTitleScore.setRecordId(recordId);
        userExamTitleScore.setTitleId(examTitle.getId());
        userExamTitleScore.setScore(0);
        userExamTitleScore.setSysAuditTotalScore(0);
        userExamTitleScore.setSysAuditScore(0);
        userExamTitleScore.setHandworkAuditTotalScore(0);
        userExamTitleScore.setHandworkAuditScore(0);

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, examTitle.getId());
        if (CollectionUtil.isEmpty(userExamAnswerList)) {
            return userExamTitleScore;
        }

        // 获取试题答案
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return userExamTitleScore;
        }

        // 转Map
        Map<Long, Integer> problemScoreMap = refList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, ExamTitleProblemRef::getScore));
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, problem -> problem));

        // 判分
        int totalScore = 0;
        for (UserExamAnswer userExamAnswer : userExamAnswerList) {
            userExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
            userExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());
            ExamProblem problem = problemMap.get(userExamAnswer.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                userExamAnswer.setScore(0);
                userExamAnswerDao.updateById(userExamAnswer);
                continue;
            }

            int score = 0;
            // 单选或判断题判分
            if (problem.getProblemAnswer().equals(userExamAnswer.getUserAnswer())) {
                userExamAnswer.setScore(problemScoreMap.get(problem.getId()));
                score = userExamAnswer.getScore();
            }

            // 更新答案
            userExamAnswer.setScore(score);
            userExamAnswerDao.updateById(userExamAnswer);

            // 统计该标题总得分
            totalScore += score;
        }
        userExamTitleScore.setScore(totalScore);
        userExamTitleScore.setSysAuditScore(totalScore);

        return userExamTitleScore;
    }

    /**
     * 组合题判断
     *
     * @param recordId  记录ID
     * @param examTitle 试卷标题
     * @param refList   试卷标题题目关联集合
     * @return 标题得分
     */
    private UserExamTitleScore materialCheck(Long recordId, ExamTitle examTitle, List<ExamTitleProblemRef> refList) {
        // 保存标题得分
        UserExamTitleScore userExamTitleScore = new UserExamTitleScore();
        userExamTitleScore.setExamId(examTitle.getExamId());
        userExamTitleScore.setRecordId(recordId);
        userExamTitleScore.setTitleId(examTitle.getId());
        userExamTitleScore.setScore(0);
        userExamTitleScore.setSysAuditTotalScore(0);
        userExamTitleScore.setSysAuditScore(0);
        userExamTitleScore.setHandworkAuditTotalScore(0);
        userExamTitleScore.setHandworkAuditScore(0);

        // 获取试题答案
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return userExamTitleScore;
        }

        // 组合题ID集合
        List<Long> materialProblemIdList = refList.stream().filter(problemRef -> ExamProblemTypeEnum.MATERIAL.getCode().equals(problemRef.getProblemType())).map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        // 分值
        Map<Long, Integer> problemScoreMap = refList.stream().collect(Collectors.toMap(ExamTitleProblemRef::getProblemId, ExamTitleProblemRef::getScore));

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, examTitle.getId());
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }
        Map<Long, UserExamAnswer> userExamAnswerMap = userExamAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));


        int sysAuditTotalScore = 0;
        int handworkAuditTotalScore = 0;
        int sysAuditScore = 0;
        // 循环
        for (Long materialProblemId : materialProblemIdList) {
            // 获取组合题小题
            List<ExamProblem> subProblemList = problemList.stream().filter(subProblem -> materialProblemId.equals(subProblem.getParentId())).collect(Collectors.toList());
            if (CollectionUtil.isEmpty(subProblemList)) {
                continue;
            }

            for (ExamProblem subProblem : subProblemList) {
                UserExamAnswer subUserProblemAnswer = userExamAnswerMap.get(subProblem.getId());
                if (ObjectUtil.isNull(subUserProblemAnswer)) {
                    sysAuditTotalScore += problemScoreMap.get(subProblem.getId());
                    continue;
                }

                // 答案为空，系统自动判断为0分
                if (StrUtil.isBlank(subUserProblemAnswer.getUserAnswer())) {
                    sysAuditTotalScore += problemScoreMap.get(subProblem.getId());

                    UserExamAnswer updateUserExamAnswer = new UserExamAnswer();
                    updateUserExamAnswer.setId(subUserProblemAnswer.getId());
                    updateUserExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                    updateUserExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());
                    userExamAnswerDao.updateById(updateUserExamAnswer);
                    continue;
                }

                if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.SHORT_ANSWER.getCode().equals(subProblem.getProblemType())) {
                    // 填空题、解答题
                    handworkAuditTotalScore += problemScoreMap.get(subProblem.getId());
                } else {
                    sysAuditTotalScore += problemScoreMap.get(subProblem.getId());

                    UserExamAnswer updateUserExamAnswer = new UserExamAnswer();
                    updateUserExamAnswer.setId(subUserProblemAnswer.getId());
                    updateUserExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                    updateUserExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());
                    if (subProblem.getProblemAnswer().equals(subUserProblemAnswer.getUserAnswer())) {
                        updateUserExamAnswer.setScore(subUserProblemAnswer.getProblemScore());
                        sysAuditScore += subUserProblemAnswer.getProblemScore();
                    }
                    userExamAnswerDao.updateById(updateUserExamAnswer);
                }
            }
        }

        userExamTitleScore.setScore(sysAuditScore);
        userExamTitleScore.setSysAuditTotalScore(sysAuditTotalScore);
        userExamTitleScore.setSysAuditScore(sysAuditScore);
        userExamTitleScore.setHandworkAuditTotalScore(handworkAuditTotalScore);
        return userExamTitleScore;
    }

    private UserExamTitleScore otherProblemCheck(Long recordId, ExamTitle examTitle, List<ExamTitleProblemRef> refList) {
        // 保存标题得分
        UserExamTitleScore userExamTitleScore = new UserExamTitleScore();
        userExamTitleScore.setExamId(examTitle.getExamId());
        userExamTitleScore.setRecordId(recordId);
        userExamTitleScore.setTitleId(examTitle.getId());
        userExamTitleScore.setScore(0);
        userExamTitleScore.setSysAuditTotalScore(0);
        userExamTitleScore.setSysAuditScore(0);
        userExamTitleScore.setHandworkAuditTotalScore(0);
        userExamTitleScore.setHandworkAuditScore(0);

        // 获取用户答案
        List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordIdAndTitleId(recordId, examTitle.getId());
        if (userExamAnswerList == null) {
            userExamAnswerList = new ArrayList<>();
        }
        Map<Long, UserExamAnswer> userExamAnswerMap = userExamAnswerList.stream().collect(Collectors.toMap(UserExamAnswer::getProblemId, userExamAnswer -> userExamAnswer));


        int sysAuditTotalScore = 0;
        int handworkAuditTotalScore = 0;
        for (ExamTitleProblemRef problemRef : refList) {
            UserExamAnswer userExamAnswer = userExamAnswerMap.get(problemRef.getProblemId());
            if (ObjectUtil.isNull(userExamAnswer)) {
                sysAuditTotalScore += problemRef.getScore();
                continue;
            }
            if (StrUtil.isBlank(userExamAnswer.getUserAnswer())) {
                sysAuditTotalScore += userExamAnswer.getProblemScore();

                UserExamAnswer updateUserExamAnswer = new UserExamAnswer();
                updateUserExamAnswer.setId(userExamAnswer.getId());
                updateUserExamAnswer.setAnswerStatus(ExamAnswerStatusEnum.COMPLETE.getCode());
                updateUserExamAnswer.setSysAudit(ExamSysAuditEnum.SYSTEM.getCode());
                userExamAnswerDao.updateById(updateUserExamAnswer);
                continue;
            }

            handworkAuditTotalScore += problemRef.getScore();
        }

        userExamTitleScore.setSysAuditTotalScore(sysAuditTotalScore);
        userExamTitleScore.setHandworkAuditTotalScore(handworkAuditTotalScore);
        return userExamTitleScore;
    }

    /**
     * 创建考试完成审核总分
     *
     * @param totalScore         总分
     * @param score              得分
     * @param checkTotalScore    已判得分
     * @param notCheckTotalScore 未判得分
     * @param titleScoreList     标题得分集合
     * @return 判分结果
     */
    private AuthExamFinishDTO createAuthExamFinishDTO(String examName, Date beginTime, Integer answerTime, Date endTime, Integer totalScore, Integer score, Integer checkTotalScore, Integer notCheckTotalScore, List<AuthExamFinishTitleScoreDTO> titleScoreList) {
        AuthExamFinishDTO dto = new AuthExamFinishDTO();
        dto.setExamName(examName);
        dto.setBeginTime(beginTime);
        dto.setAnswerTime(answerTime);
        dto.setEndTime(endTime);
        dto.setTotalScore(totalScore);
        dto.setScore(score);
        dto.setCheckTotalScore(checkTotalScore);
        dto.setNotCheckTotalScore(notCheckTotalScore);
        dto.setTitleScoreList(titleScoreList);
        return dto;
    }

    /**
     * 创建标题得分结果
     *
     * @param examTitle       试卷标题
     * @param totalScore      总分
     * @param score           得分
     * @param needManualScore 需要人工评分分值
     * @return 得分结果
     */
    private AuthExamFinishTitleScoreDTO createFinishTitleScore(ExamTitle examTitle, Integer totalScore, Integer score, Integer needManualScore) {
        AuthExamFinishTitleScoreDTO dto = new AuthExamFinishTitleScoreDTO();
        dto.setTitleId(examTitle.getId());
        dto.setTitleName(examTitle.getTitleName());
        dto.setTotalScore(totalScore);
        dto.setScore(score);
        dto.setNeedManualScore(needManualScore);
        return dto;
    }

    /**
     * 获取考试完成结果
     *
     * @param examRecord 考试记录
     * @return 考试完成结果
     */
    private Result<AuthExamFinishDTO> getExamFinisResult(UserExamRecord examRecord) {
        if (!ExamStatusEnum.FINISH.getCode().equals(examRecord.getExamStatus())) {
            return Result.error("考试状态异常");
        }
        ExamInfo examInfo = examInfoDao.getById(examRecord.getExamId());
        if (ObjectUtil.isEmpty(examInfo)) {
            return Result.error("试卷不存在");
        }

        AuthExamFinishDTO finishDTO = new AuthExamFinishDTO();
        finishDTO.setExamName(examInfo.getExamName());
        finishDTO.setBeginTime(examRecord.getGmtCreate());
        finishDTO.setAnswerTime(examRecord.getAnswerTime());
        finishDTO.setEndTime(examRecord.getEndTime());

        List<ExamTitle> examTitleList = examTitleDao.listByExamId(examInfo.getId());
        if (CollectionUtil.isEmpty(examTitleList)) {
            logger.warn("试卷：[{}]获取试卷标题为空", examInfo.getId());
            finishDTO.setTotalScore(examRecord.getTotalScore());
            finishDTO.setScore(0);
            finishDTO.setCheckTotalScore(0);
            finishDTO.setNotCheckTotalScore(0);
            return Result.success(finishDTO);
        }

        // 获取用户考试标题成绩
        List<UserExamTitleScore> userExamTitleScoreList = userExamTitleScoreDao.listByRecordId(examRecord.getId());
        if (userExamTitleScoreList == null) {
            userExamTitleScoreList = new ArrayList<>();
        }
        Map<Long, UserExamTitleScore> userExamTitleScoreMap = userExamTitleScoreList.stream().collect(Collectors.toMap(UserExamTitleScore::getTitleId, userExamTitleScore -> userExamTitleScore));

        int checkTotalScore = 0;
        int notCheckTotalScore = 0;
        List<AuthExamFinishTitleScoreDTO> titleScoreList = new ArrayList<>();
        for (ExamTitle examTitle : examTitleList) {
            AuthExamFinishTitleScoreDTO titleScoreDTO = new AuthExamFinishTitleScoreDTO();
            titleScoreDTO.setTitleId(examTitle.getId());
            titleScoreDTO.setTitleName(examTitle.getTitleName());

            UserExamTitleScore userExamTitleScore = userExamTitleScoreMap.get(examTitle.getId());
            titleScoreDTO.setTotalScore(userExamTitleScore.getSysAuditTotalScore() + userExamTitleScore.getHandworkAuditTotalScore());
            titleScoreDTO.setScore(userExamTitleScore.getScore());
            titleScoreDTO.setNeedManualScore(userExamTitleScore.getHandworkAuditTotalScore());
            titleScoreList.add(titleScoreDTO);

            checkTotalScore += userExamTitleScore.getSysAuditTotalScore();
            notCheckTotalScore += userExamTitleScore.getHandworkAuditTotalScore();
        }

        finishDTO.setTotalScore(examRecord.getTotalScore());
        finishDTO.setScore(examRecord.getScore());
        finishDTO.setCheckTotalScore(checkTotalScore);
        finishDTO.setNotCheckTotalScore(notCheckTotalScore);
        finishDTO.setTitleScoreList(titleScoreList);
        return Result.success(finishDTO);
    }

    /**
     * 判断用户是否处于考试中
     *
     * @param userNo 用户编号
     * @return 校验结果
     */
    public Result<AuthCheckUserExamDTO> checkUserExam(Long userNo) {
        AuthCheckUserExamDTO dto = new AuthCheckUserExamDTO();
        // 判断当前用户是否在考试
        List<UserExamRecord> userExamRecordList = dao.listByUserNoAndExamStatus(userNo, ExamStatusEnum.NOT_OVER.getCode());
        if (CollectionUtil.isNotEmpty(userExamRecordList)) {
            dto.setIsExamination(true);
            UserExamRecord record = userExamRecordList.get(0);
            dto.setRecordId(record.getId());
            dto.setExamId(record.getExamId());

            ExamInfo examInfo = examInfoDao.getById(record.getExamId());
            if (ObjectUtil.isNotNull(examInfo)) {
                dto.setExamName(examInfo.getExamName());
            }
        } else {
            dto.setIsExamination(false);
        }
        return Result.success(dto);
    }

    /**
     * 获取考试记录分页对象的分类信息
     *
     * @param pageDTO 分页对象
     */
    private void getExamRecordPageCategoryInfo(AuthUserExamRecordPageDTO pageDTO) {
        // 获取年级名称
        ExamCategory graCategory = examCategoryDao.getById(pageDTO.getGraId());
        if (ObjectUtil.isNotEmpty(graCategory)) {
            pageDTO.setGraName(graCategory.getCategoryName());
        }
        // 获取科目名称
        ExamCategory subjectCategory = examCategoryDao.getById(pageDTO.getSubjectId());
        if (ObjectUtil.isNotEmpty(subjectCategory)) {
            pageDTO.setSubjectName(subjectCategory.getCategoryName());
        }
        // 年份ID
        ExamCategory yearCategory = examCategoryDao.getById(pageDTO.getYearId());
        if (ObjectUtil.isNotEmpty(yearCategory)) {
            pageDTO.setYearName(yearCategory.getCategoryName());
        }
        //获取来源名称
        ExamCategory sourceCategory = examCategoryDao.getById(pageDTO.getSourceId());
        if (ObjectUtil.isNotEmpty(sourceCategory)) {
            pageDTO.setSourceName(sourceCategory.getCategoryName());
        }
    }

    public Result<AuthExamAnalysisDTO> examAnalysis(AuthExamAnalysisBO bo) {
        UserExamRecord userExamRecord = dao.getByUserNoAndExamId(ThreadContext.userNo(), bo.getExamId());
        if (ObjectUtil.isNull(userExamRecord)) {
            return Result.error("获取不该用户的考试信息!");
        }
        //1、根据对应的试卷id获取所有的试题科目分类
        //examTitleProblemRefDao.listSubjectIdByExamId(bo.getExamId());



        /*List<UserExamAnswer> userExamAnswerList = userExamAnswerDao.listByRecordId(userExamRecord.getId());
        if (CollectionUtil.isEmpty(userExamAnswerList)) {
            return Result.error("没有答案信息!");
        }
        for (UserExamAnswer userExamAnswer : userExamAnswerList) {

        }*/
        return null;
    }
}
