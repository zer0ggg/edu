package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignUserExamAnswer;
import com.roncoo.education.exam.feign.qo.UserExamAnswerQO;
import com.roncoo.education.exam.feign.vo.UserExamAnswerVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserExamAnswerBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户考试答案
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserExamAnswerController extends BaseController implements IFeignUserExamAnswer{

    @Autowired
    private FeignUserExamAnswerBiz biz;

	@Override
	public Page<UserExamAnswerVO> listForPage(@RequestBody UserExamAnswerQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody UserExamAnswerQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody UserExamAnswerQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public UserExamAnswerVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
