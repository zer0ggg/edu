package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoAuditExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamInfoAuditMapper {
    int countByExample(ExamInfoAuditExample example);

    int deleteByExample(ExamInfoAuditExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamInfoAudit record);

    int insertSelective(ExamInfoAudit record);

    List<ExamInfoAudit> selectByExample(ExamInfoAuditExample example);

    ExamInfoAudit selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamInfoAudit record, @Param("example") ExamInfoAuditExample example);

    int updateByExample(@Param("record") ExamInfoAudit record, @Param("example") ExamInfoAuditExample example);

    int updateByPrimaryKeySelective(ExamInfoAudit record);

    int updateByPrimaryKey(ExamInfoAudit record);
}