package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级信息分页查询对象--查询班级成员管理的班级
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AuthGradeStudentGradeInfoByRolePageBO", description = "班级信息分页查询对象--查询班级成员管理的班级")
public class AuthGradeStudentGradeInfoByRolePageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = -8620430812316792354L;

    @ApiModelProperty(value = "班级名称")
    private String gradeName;

    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)")
    private Integer verificationSet;

    @ApiModelProperty(value = "邀请码")
    private String invitationCode;

    @ApiModelProperty(value = "学员类型(1:学员，2:管理员)")
    private Integer userRole;
}
