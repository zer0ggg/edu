package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 用户试卷答案标题
 *
 * @author LYQ
 */
@ApiModel(value = "用户试卷答案标题", description = "用户试卷答案标题")
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerTitleDTO implements Serializable {

    private static final long serialVersionUID = -7210325976699134094L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "标题类型")
    private Integer titleType;

    @ApiModelProperty(value = "小题集合")
    private List<AuthUserExamAnswerProblemDTO> problemList;
}
