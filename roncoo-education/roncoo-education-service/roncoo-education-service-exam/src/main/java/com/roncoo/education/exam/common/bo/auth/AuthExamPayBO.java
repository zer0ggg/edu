package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷购买参数
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamPayBO", description = "试卷购买参数")
public class AuthExamPayBO implements Serializable {

    private static final long serialVersionUID = 2875919141368040821L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;

    @NotNull(message = "支付方式不能为空")
    @ApiModelProperty(value = "支付方式(1微信扫码，2支付宝支付，3积分支付，4手工录单，5小程序支付)", required = true)
    private Integer payType;

    @NotNull(message = "购买渠道不能为空")
    @ApiModelProperty(value = "购买渠道：1:PC端，2:APP端，3:微信", required = true)
    private Integer channelType;

    @ApiModelProperty(value = "用户备注")
    private String remark;
}
