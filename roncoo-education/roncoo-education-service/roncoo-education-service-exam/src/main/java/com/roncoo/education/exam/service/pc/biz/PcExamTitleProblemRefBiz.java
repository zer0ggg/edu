package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefListREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefViewRESP;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联
 *
 * @author wujing
 */
@Component
public class PcExamTitleProblemRefBiz extends BaseBiz {

	@Autowired
	private ExamTitleProblemRefDao dao;

	/**
	 * 试卷标题题目关联列表
	 *
	 * @param examTitleProblemRefListREQ 试卷标题题目关联分页查询参数
	 * @return 试卷标题题目关联分页查询结果
	 */
	public Result<Page<ExamTitleProblemRefListRESP>> list(ExamTitleProblemRefListREQ examTitleProblemRefListREQ) {
		ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
		Criteria c = example.createCriteria();
		Page<ExamTitleProblemRef> page = dao.listForPage(examTitleProblemRefListREQ.getPageCurrent(), examTitleProblemRefListREQ.getPageSize(), example);
		Page<ExamTitleProblemRefListRESP> respPage = PageUtil.transform(page, ExamTitleProblemRefListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷标题题目关联添加
	 *
	 * @param examTitleProblemRefSaveREQ 试卷标题题目关联
	 * @return 添加结果
	 */
	public Result<String> save(ExamTitleProblemRefSaveREQ examTitleProblemRefSaveREQ) {
		ExamTitleProblemRef record = BeanUtil.copyProperties(examTitleProblemRefSaveREQ, ExamTitleProblemRef.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷标题题目关联查看
	 *
	 * @param id 主键ID
	 * @return 试卷标题题目关联
	 */
	public Result<ExamTitleProblemRefViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamTitleProblemRefViewRESP.class));
	}

	/**
	 * 试卷标题题目关联修改
	 *
	 * @param examTitleProblemRefEditREQ 试卷标题题目关联修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamTitleProblemRefEditREQ examTitleProblemRefEditREQ) {
		ExamTitleProblemRef record = BeanUtil.copyProperties(examTitleProblemRefEditREQ, ExamTitleProblemRef.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷标题题目关联删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
