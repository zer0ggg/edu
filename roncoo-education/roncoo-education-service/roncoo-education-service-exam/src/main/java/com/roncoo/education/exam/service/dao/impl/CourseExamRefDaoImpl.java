package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.CourseExamRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.CourseExamRefMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.CourseExamRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 课程试卷关联 服务实现类
 *
 * @author wujing
 * @date 2020-10-21
 */
@Repository
public class CourseExamRefDaoImpl implements CourseExamRefDao {

    @Autowired
    private CourseExamRefMapper mapper;

    @Override
    public int save(CourseExamRef record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseExamRef record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseExamRef getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseExamRef> listForPage(int pageCurrent, int pageSize, CourseExamRefExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public CourseExamRef getByRefIdAndExamId(Long refId, Long examId) {
        CourseExamRefExample example = new CourseExamRefExample();
        CourseExamRefExample.Criteria c = example.createCriteria();
        c.andRefIdEqualTo(refId);
        c.andExamIdEqualTo(examId);
        List<CourseExamRef> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseExamRef getByRefIdAndRefType(Long refId, Integer refType) {
        CourseExamRefExample example = new CourseExamRefExample();
        CourseExamRefExample.Criteria c = example.createCriteria();
        c.andRefIdEqualTo(refId);
        c.andRefTypeEqualTo(refType);
        List<CourseExamRef> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }


}
