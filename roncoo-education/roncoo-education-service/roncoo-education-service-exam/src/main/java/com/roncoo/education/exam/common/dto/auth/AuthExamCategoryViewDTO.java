package com.roncoo.education.exam.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 考试信息分类
 *
 * @author forest
 *
 */
@Data
@Accessors(chain = true)
public class AuthExamCategoryViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "分类编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	/**
	 * 分类名称
	 */
	@ApiModelProperty(value = "分类名称")
	private String categoryName;
	/**
	 * 层级
	 */
	@ApiModelProperty(value = "分类层级")
	private Integer floor;

	/**
	 * 分类备注信息
	 */
	@ApiModelProperty(value = "备注")
	private String remark;

	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;

}
