package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联
 *
 * @author wujing
 */
@Component
public class AuthExamTitleProblemRefBiz extends BaseBiz {

    @Autowired
    private ExamTitleProblemRefDao dao;

}
