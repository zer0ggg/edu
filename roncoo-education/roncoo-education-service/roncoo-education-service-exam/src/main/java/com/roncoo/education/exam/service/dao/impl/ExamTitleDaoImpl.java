package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.exam.service.dao.ExamTitleDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamTitleMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitle;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷标题 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamTitleDaoImpl implements ExamTitleDao {

    @Autowired
    private ExamTitleMapper mapper;

    @Override
    public int save(ExamTitle record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamTitle record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamTitle getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamTitle> listForPage(int pageCurrent, int pageSize, ExamTitleExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ExamTitle> listByExamIdAndStatusId(Long examId, Integer statusId) {
        ExamTitleExample example = new ExamTitleExample();
        ExamTitleExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitle> listByExamId(Long examId) {
        ExamTitleExample example = new ExamTitleExample();
        ExamTitleExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        example.setOrderByClause("status_id desc, sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByExamId(Long examId) {
        ExamTitleExample example = new ExamTitleExample();
        ExamTitleExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        return this.mapper.deleteByExample(example);
    }
}
