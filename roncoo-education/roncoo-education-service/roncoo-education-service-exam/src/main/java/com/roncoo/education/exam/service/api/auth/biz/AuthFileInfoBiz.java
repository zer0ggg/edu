package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.FileInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件信息
 *
 * @author wujing
 */
@Component
public class AuthFileInfoBiz extends BaseBiz {

    @Autowired
    private FileInfoDao dao;

}
