package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeStudent;
import com.roncoo.education.exam.feign.qo.GradeStudentQO;
import com.roncoo.education.exam.feign.vo.GradeStudentVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeStudentBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeStudentController extends BaseController implements IFeignGradeStudent{

    @Autowired
    private FeignGradeStudentBiz biz;

	@Override
	public Page<GradeStudentVO> listForPage(@RequestBody GradeStudentQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeStudentQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeStudentQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeStudentVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
