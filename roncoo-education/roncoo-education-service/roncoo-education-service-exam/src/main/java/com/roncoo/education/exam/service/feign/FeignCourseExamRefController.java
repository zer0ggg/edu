package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.exam.service.feign.biz.FeignCourseExamRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程试卷关联
 *
 * @author wujing
 * @date 2020-10-21
 */
@RestController
public class FeignCourseExamRefController extends BaseController implements IFeignCourseExamRef{

    @Autowired
    private FeignCourseExamRefBiz biz;

	@Override
	public Page<CourseExamRefVO> listForPage(@RequestBody CourseExamRefQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseExamRefQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseExamRefQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public CourseExamRefVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

    @Override
    public CourseExamRefVO getByRefIdAndRefType(@RequestBody CourseExamRefQO courseExamRefQO) {
        return biz.getByRefIdAndRefType(courseExamRefQO);
    }

    @Override
    public CourseExamRefVO getByRefIdAndRefTypeAudit(@RequestBody CourseExamRefQO courseExamRefQO) {
        return biz.getByRefIdAndRefTypeAudit(courseExamRefQO);
    }
}
