package com.roncoo.education.exam.service.api;

import com.roncoo.education.exam.service.api.biz.ApiCourseExamRefBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 课程试卷关联 Api接口
 *
 * @author wujing
 * @date 2020-10-21
 */
@Api(tags = "API-课程试卷关联")
@RestController
@RequestMapping("/exam/api/course/exam/ref")
public class ApiCourseExamRefController {

    @Autowired
    private ApiCourseExamRefBiz biz;

}
