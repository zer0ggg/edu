package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.ExamAnswerStatusEnum;
import com.roncoo.education.common.core.enums.IsPutawayEnum;
import com.roncoo.education.common.core.enums.ProblemCategoryEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamAnalysisBO;
import com.roncoo.education.exam.common.bo.auth.AuthUserExamPageBO;
import com.roncoo.education.exam.common.dto.auth.AuthExamInfoDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemCategoryDTO;
import com.roncoo.education.exam.common.dto.auth.AuthUserExamAnalysisDTO;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户试卷
 *
 * @author wujing
 */
@Component
public class AuthUserExamBiz extends BaseBiz {

    @Autowired
    private UserExamDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private UserExamAnswerDao userExamAnswerDao;


    /**
     * 列出用户试卷
     *
     * @param pageBO 分页查询参数
     * @return 分页结果
     */
    public Result<Page<AuthExamInfoDTO>> list(AuthUserExamPageBO pageBO) {
        List<UserExam> userExamList = dao.listByUserNo(ThreadContext.userNo());
        if (CollectionUtil.isEmpty(userExamList)) {
            return Result.success(new Page<>(0, 0, pageBO.getPageCurrent(), pageBO.getPageSize(), new ArrayList<>()));
        }

        List<Long> examIdList = userExamList.stream().map(UserExam::getExamId).collect(Collectors.toList());

        ExamInfoExample example = new ExamInfoExample();
        ExamInfoExample.Criteria criteria = example.createCriteria();
        //默认条件
        criteria.andIdIn(examIdList);
        criteria.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        criteria.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        //用户条件
        if (StrUtil.isNotBlank(pageBO.getExamName())) {
            criteria.andExamNameLike(PageUtil.like(pageBO.getExamName()));
        }
        if (ObjectUtil.isNotNull(pageBO.getSubjectId())) {
            criteria.andSubjectIdEqualTo(pageBO.getSubjectId());
        }
        if (ObjectUtil.isNotNull(pageBO.getYearId())) {
            criteria.andYearIdEqualTo(pageBO.getYearId());
        }
        if (ObjectUtil.isNotNull(pageBO.getSourceId())) {
            criteria.andSourceIdEqualTo(pageBO.getSourceId());
        }
        //综合排序
        example.setOrderByClause(" sort asc, id desc ");

        //获取数据
        Page<ExamInfo> page = examInfoDao.listForPage(pageBO.getPageCurrent(), pageBO.getPageSize(), example);
        Page<AuthExamInfoDTO> listForPage = PageUtil.transform(page, AuthExamInfoDTO.class);
        return Result.success(listForPage);
    }


    public Result<AuthUserExamAnalysisDTO> analysis(AuthUserExamAnalysisBO bo) {
        ExamInfo examInfo = examInfoDao.getById(bo.getExamId());
        if (ObjectUtil.isNull(examInfo)) {
            return Result.error("找不试卷信息");
        }
        AuthUserExamAnalysisDTO dto = new AuthUserExamAnalysisDTO();
        // 重复获取试卷ID
        List<Long> problemIdList = examTitleProblemRefDao.getByExamIdAndDuplicateProblemId(bo.getExamId());
        if (CollectionUtil.isEmpty(problemIdList)) {
            return Result.error("获取不到试题");
        }
        // 获取试卷所有试题分类ID
        List<Long> examProblemCategoryIdList = examProblemCategoryList(problemIdList, bo.getProblemCategory());
        List<AuthExamProblemCategoryDTO> list = new ArrayList<>();
        for (Long problemCategoryId : examProblemCategoryIdList) {
            AuthExamProblemCategoryDTO authExamProblemCategoryDTO = new AuthExamProblemCategoryDTO();
            ExamCategory examCategory = examCategoryDao.getById(problemCategoryId);
            if (ObjectUtil.isNotNull(examCategory)) {
                authExamProblemCategoryDTO.setName(examCategory.getCategoryName());
                // 根据分类汇总错题数
                authExamProblemCategoryDTO.setValue(errorSum(problemCategoryId, bo));
            }
            list.add(authExamProblemCategoryDTO);
        }
        dto.setList(list);
        return Result.success(dto);
    }

    public Integer errorSum(Long problemCategoryId, AuthUserExamAnalysisBO bo) {
        // 错误数
        Integer errorSum = 0;
        // 年级id
        List<ExamProblem> examProblemList = examProblemDao.ListByGraIdAndProblemCategory(problemCategoryId, bo.getProblemCategory());
        if (CollectionUtil.isNotEmpty(examProblemList)) {
            for (ExamProblem examProblem : examProblemList) {
                // 根据试题ID和用户编号列出用户考试记录
                List<UserExamAnswer> list = new ArrayList<>();
                if (bo.getLecturerUserNo() == null) {
                    // 用户汇总考试记录
                    list = userExamAnswerDao.getProblemIdAndExamIdAndUserNo(examProblem.getId(), bo.getExamId(), ThreadContext.userNo());
                } else {
                    // 讲师汇总用户考试记录
                    list = userExamAnswerDao.getProblemIdAndExamId(examProblem.getId(), bo.getExamId());
                }
                if (CollectionUtil.isNotEmpty(list)) {
                    // 错误数
                    errorSum = summary(errorSum, list);
                }
            }
        }
        return errorSum;
    }


    private Integer summary(Integer errorSum, List<UserExamAnswer> list) {
        for (UserExamAnswer userExamAnswer : list) {
            if (ExamAnswerStatusEnum.COMPLETE.getCode().equals(userExamAnswer.getAnswerStatus())) {
                // 小于等零错误 +错误数
                if (userExamAnswer.getScore() <= 0) {
                    errorSum = errorSum + 1;
                }
            }
        }
        return errorSum;
    }


    private List<Long> examProblemCategoryList(List<Long> problemIdList, Integer problemCategory) {
        List<Long> list = new ArrayList<>();
        for (Long problemId : problemIdList) {
            ExamProblem examProblem = examProblemDao.getById(problemId);
            if (ObjectUtil.isNotNull(examProblem)) {
                if (!list.contains(examProblem.getGraId()) && ProblemCategoryEnum.GRA.getCode().equals(problemCategory)) {
                    // 年级id
                    list.add(examProblem.getGraId());
                }
                if (!list.contains(examProblem.getSubjectId()) && ProblemCategoryEnum.SUBJECT.getCode().equals(problemCategory)) {
                    // 科目id
                    list.add(examProblem.getSubjectId());
                }
                if (!list.contains(examProblem.getYearId()) && ProblemCategoryEnum.YEAR.getCode().equals(problemCategory)) {
                    // 年份id
                    list.add(examProblem.getYearId());
                }
                if (!list.contains(examProblem.getSourceId()) && ProblemCategoryEnum.SOURCE.getCode().equals(problemCategory)) {
                    // 来源id
                    list.add(examProblem.getSourceId());
                }
                if (!list.contains(examProblem.getDifficultyId()) && ProblemCategoryEnum.DIFFICULTY.getCode().equals(problemCategory)) {
                    // 难度id
                    list.add(examProblem.getDifficultyId());
                }
                if (!list.contains(examProblem.getTopicId()) && ProblemCategoryEnum.TOPIC.getCode().equals(problemCategory)) {
                    // 题类id
                    list.add(examProblem.getTopicId());
                }
                if (!list.contains(examProblem.getEmphasisId()) && ProblemCategoryEnum.EMPHASIS.getCode().equals(problemCategory)) {
                    // 考点id
                    list.add(examProblem.getEmphasisId());
                }
            }
        }
        return list;
    }
}
