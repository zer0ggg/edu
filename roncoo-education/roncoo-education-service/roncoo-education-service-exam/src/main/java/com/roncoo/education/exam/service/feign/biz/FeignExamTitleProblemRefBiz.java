package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamTitleProblemRefQO;
import com.roncoo.education.exam.feign.vo.ExamTitleProblemRefVO;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联
 *
 * @author wujing
 */
@Component
public class FeignExamTitleProblemRefBiz extends BaseBiz {

    @Autowired
    private ExamTitleProblemRefDao dao;

	public Page<ExamTitleProblemRefVO> listForPage(ExamTitleProblemRefQO qo) {
	    ExamTitleProblemRefExample example = new ExamTitleProblemRefExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamTitleProblemRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamTitleProblemRefVO.class);
	}

	public int save(ExamTitleProblemRefQO qo) {
		ExamTitleProblemRef record = BeanUtil.copyProperties(qo, ExamTitleProblemRef.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamTitleProblemRefVO getById(Long id) {
		ExamTitleProblemRef record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamTitleProblemRefVO.class);
	}

	public int updateById(ExamTitleProblemRefQO qo) {
		ExamTitleProblemRef record = BeanUtil.copyProperties(qo, ExamTitleProblemRef.class);
		return dao.updateById(record);
	}

}
