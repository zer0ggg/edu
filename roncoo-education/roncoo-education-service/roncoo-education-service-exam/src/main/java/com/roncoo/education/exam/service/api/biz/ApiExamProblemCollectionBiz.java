package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamProblemCollectionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目收藏
 *
 * @author wujing
 */
@Component
public class ApiExamProblemCollectionBiz extends BaseBiz {

    @Autowired
    private ExamProblemCollectionDao dao;

}
