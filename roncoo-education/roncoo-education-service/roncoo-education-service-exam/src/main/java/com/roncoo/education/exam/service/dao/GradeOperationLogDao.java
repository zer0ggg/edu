package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLog;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample;

/**
 *  服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeOperationLogDao {

    /**
    * 保存
    *
    * @param record 
    * @return 影响记录数
    */
    int save(GradeOperationLog record);

    /**
    * 根据ID删除
    *
    * @param id 主键ID
    * @return 影响记录数
    */
    int deleteById(Long id);

    /**
    * 修改试卷分类
    *
    * @param record 
    * @return 影响记录数
    */
    int updateById(GradeOperationLog record);

    /**
    * 根据ID获取
    *
    * @param id 主键ID
    * @return 
    */
    GradeOperationLog getById(Long id);

    /**
    * --分页查询
    *
    * @param pageCurrent 当前页
    * @param pageSize    分页大小
    * @param example     查询条件
    * @return 分页结果
    */
    Page<GradeOperationLog> listForPage(int pageCurrent, int pageSize, GradeOperationLogExample example);

}
