package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignGradeOperationLog;
import com.roncoo.education.exam.feign.qo.GradeOperationLogQO;
import com.roncoo.education.exam.feign.vo.GradeOperationLogVO;
import com.roncoo.education.exam.service.feign.biz.FeignGradeOperationLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 *
 * @author wujing
 * @date 2020-06-10
 */
@RestController
public class FeignGradeOperationLogController extends BaseController implements IFeignGradeOperationLog{

    @Autowired
    private FeignGradeOperationLogBiz biz;

	@Override
	public Page<GradeOperationLogVO> listForPage(@RequestBody GradeOperationLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody GradeOperationLogQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody GradeOperationLogQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public GradeOperationLogVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
