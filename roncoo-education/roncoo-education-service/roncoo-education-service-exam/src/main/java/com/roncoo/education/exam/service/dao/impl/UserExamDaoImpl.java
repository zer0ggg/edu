package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserExamDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserExamMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExam;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserExamExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户试卷 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserExamDaoImpl implements UserExamDao {

    @Autowired
    private UserExamMapper mapper;

    @Override
    public int save(UserExam record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserExam record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserExam getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserExam> listForPage(int pageCurrent, int pageSize, UserExamExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserExam getByUserNoAndExamId(Long userNo, Long examId) {
        UserExamExample example = new UserExamExample();
        UserExamExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamIdEqualTo(examId);
        List<UserExam> resultList = this.mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public List<UserExam> listByUserNo(Long userNo) {
        UserExamExample example = new UserExamExample();
        UserExamExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        return this.mapper.selectByExample(example);
    }

}
