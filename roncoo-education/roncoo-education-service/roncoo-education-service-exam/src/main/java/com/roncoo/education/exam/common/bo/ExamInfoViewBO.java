package com.roncoo.education.exam.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷详情
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class ExamInfoViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
}
