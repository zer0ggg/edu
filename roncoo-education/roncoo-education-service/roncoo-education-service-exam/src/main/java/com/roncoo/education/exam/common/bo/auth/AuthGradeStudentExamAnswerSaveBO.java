package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "AuthGradeStudentExamAnswerSaveBO", description = "班级学生考试答案保存请求对象")
public class AuthGradeStudentExamAnswerSaveBO implements Serializable {

    private static final long serialVersionUID = -2260794236591289146L;

    @ApiModelProperty(value = "关联ID", required = true)
    private Long relationId;

    @ApiModelProperty(value = "标题ID", required = true)
    private Long titleId;

    @ApiModelProperty(value = "题目ID", required = true)
    private Long problemId;

    @ApiModelProperty(value = "用户答案", required = true)
    private String userAnswer;
}
