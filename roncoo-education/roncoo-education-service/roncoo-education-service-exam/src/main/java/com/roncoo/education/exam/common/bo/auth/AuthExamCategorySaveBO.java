package com.roncoo.education.exam.common.bo.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 考试分类
 * @author zkpc
 *
 */
@Data
@Accessors(chain = true)
public class AuthExamCategorySaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类类型不能为空")
    @ApiModelProperty(value = "分类类型(1:试卷科目,2:试卷年份,3：试卷来源,4：试题难度,5：试题类型)", required = true)
    private Integer examCategoryType;

    @ApiModelProperty(value = "父Id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    @NotEmpty(message = "分类名称不能为空")
    @ApiModelProperty(value = "分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "排序")
    private Integer sort;

}
