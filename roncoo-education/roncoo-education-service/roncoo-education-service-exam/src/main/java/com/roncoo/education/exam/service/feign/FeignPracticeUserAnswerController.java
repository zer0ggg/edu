package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignPracticeUserAnswer;
import com.roncoo.education.exam.feign.qo.PracticeUserAnswerQO;
import com.roncoo.education.exam.feign.vo.PracticeUserAnswerVO;
import com.roncoo.education.exam.service.feign.biz.FeignPracticeUserAnswerBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户考试答案
 *
 * @author LHR
 * @date 2020-10-10
 */
@RestController
public class FeignPracticeUserAnswerController extends BaseController implements IFeignPracticeUserAnswer{

    @Autowired
    private FeignPracticeUserAnswerBiz biz;

	@Override
	public Page<PracticeUserAnswerVO> listForPage(@RequestBody PracticeUserAnswerQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody PracticeUserAnswerQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody PracticeUserAnswerQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public PracticeUserAnswerVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
