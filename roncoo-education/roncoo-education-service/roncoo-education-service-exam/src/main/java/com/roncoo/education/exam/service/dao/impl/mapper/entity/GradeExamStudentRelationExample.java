package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GradeExamStudentRelationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public GradeExamStudentRelationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andGradeIdIsNull() {
            addCriterion("grade_id is null");
            return (Criteria) this;
        }

        public Criteria andGradeIdIsNotNull() {
            addCriterion("grade_id is not null");
            return (Criteria) this;
        }

        public Criteria andGradeIdEqualTo(Long value) {
            addCriterion("grade_id =", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdNotEqualTo(Long value) {
            addCriterion("grade_id <>", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdGreaterThan(Long value) {
            addCriterion("grade_id >", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdGreaterThanOrEqualTo(Long value) {
            addCriterion("grade_id >=", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdLessThan(Long value) {
            addCriterion("grade_id <", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdLessThanOrEqualTo(Long value) {
            addCriterion("grade_id <=", value, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdIn(List<Long> values) {
            addCriterion("grade_id in", values, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdNotIn(List<Long> values) {
            addCriterion("grade_id not in", values, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdBetween(Long value1, Long value2) {
            addCriterion("grade_id between", value1, value2, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeIdNotBetween(Long value1, Long value2) {
            addCriterion("grade_id not between", value1, value2, "gradeId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdIsNull() {
            addCriterion("grade_exam_id is null");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdIsNotNull() {
            addCriterion("grade_exam_id is not null");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdEqualTo(Long value) {
            addCriterion("grade_exam_id =", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdNotEqualTo(Long value) {
            addCriterion("grade_exam_id <>", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdGreaterThan(Long value) {
            addCriterion("grade_exam_id >", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdGreaterThanOrEqualTo(Long value) {
            addCriterion("grade_exam_id >=", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdLessThan(Long value) {
            addCriterion("grade_exam_id <", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdLessThanOrEqualTo(Long value) {
            addCriterion("grade_exam_id <=", value, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdIn(List<Long> values) {
            addCriterion("grade_exam_id in", values, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdNotIn(List<Long> values) {
            addCriterion("grade_exam_id not in", values, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdBetween(Long value1, Long value2) {
            addCriterion("grade_exam_id between", value1, value2, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andGradeExamIdNotBetween(Long value1, Long value2) {
            addCriterion("grade_exam_id not between", value1, value2, "gradeExamId");
            return (Criteria) this;
        }

        public Criteria andExamIdIsNull() {
            addCriterion("exam_id is null");
            return (Criteria) this;
        }

        public Criteria andExamIdIsNotNull() {
            addCriterion("exam_id is not null");
            return (Criteria) this;
        }

        public Criteria andExamIdEqualTo(Long value) {
            addCriterion("exam_id =", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotEqualTo(Long value) {
            addCriterion("exam_id <>", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdGreaterThan(Long value) {
            addCriterion("exam_id >", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdGreaterThanOrEqualTo(Long value) {
            addCriterion("exam_id >=", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdLessThan(Long value) {
            addCriterion("exam_id <", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdLessThanOrEqualTo(Long value) {
            addCriterion("exam_id <=", value, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdIn(List<Long> values) {
            addCriterion("exam_id in", values, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotIn(List<Long> values) {
            addCriterion("exam_id not in", values, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdBetween(Long value1, Long value2) {
            addCriterion("exam_id between", value1, value2, "examId");
            return (Criteria) this;
        }

        public Criteria andExamIdNotBetween(Long value1, Long value2) {
            addCriterion("exam_id not between", value1, value2, "examId");
            return (Criteria) this;
        }

        public Criteria andStudentIdIsNull() {
            addCriterion("student_id is null");
            return (Criteria) this;
        }

        public Criteria andStudentIdIsNotNull() {
            addCriterion("student_id is not null");
            return (Criteria) this;
        }

        public Criteria andStudentIdEqualTo(Long value) {
            addCriterion("student_id =", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdNotEqualTo(Long value) {
            addCriterion("student_id <>", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdGreaterThan(Long value) {
            addCriterion("student_id >", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdGreaterThanOrEqualTo(Long value) {
            addCriterion("student_id >=", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdLessThan(Long value) {
            addCriterion("student_id <", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdLessThanOrEqualTo(Long value) {
            addCriterion("student_id <=", value, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdIn(List<Long> values) {
            addCriterion("student_id in", values, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdNotIn(List<Long> values) {
            addCriterion("student_id not in", values, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdBetween(Long value1, Long value2) {
            addCriterion("student_id between", value1, value2, "studentId");
            return (Criteria) this;
        }

        public Criteria andStudentIdNotBetween(Long value1, Long value2) {
            addCriterion("student_id not between", value1, value2, "studentId");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andViewStatusIsNull() {
            addCriterion("view_status is null");
            return (Criteria) this;
        }

        public Criteria andViewStatusIsNotNull() {
            addCriterion("view_status is not null");
            return (Criteria) this;
        }

        public Criteria andViewStatusEqualTo(Integer value) {
            addCriterion("view_status =", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusNotEqualTo(Integer value) {
            addCriterion("view_status <>", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusGreaterThan(Integer value) {
            addCriterion("view_status >", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("view_status >=", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusLessThan(Integer value) {
            addCriterion("view_status <", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusLessThanOrEqualTo(Integer value) {
            addCriterion("view_status <=", value, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusIn(List<Integer> values) {
            addCriterion("view_status in", values, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusNotIn(List<Integer> values) {
            addCriterion("view_status not in", values, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusBetween(Integer value1, Integer value2) {
            addCriterion("view_status between", value1, value2, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andViewStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("view_status not between", value1, value2, "viewStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusIsNull() {
            addCriterion("exam_status is null");
            return (Criteria) this;
        }

        public Criteria andExamStatusIsNotNull() {
            addCriterion("exam_status is not null");
            return (Criteria) this;
        }

        public Criteria andExamStatusEqualTo(Integer value) {
            addCriterion("exam_status =", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusNotEqualTo(Integer value) {
            addCriterion("exam_status <>", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusGreaterThan(Integer value) {
            addCriterion("exam_status >", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("exam_status >=", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusLessThan(Integer value) {
            addCriterion("exam_status <", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusLessThanOrEqualTo(Integer value) {
            addCriterion("exam_status <=", value, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusIn(List<Integer> values) {
            addCriterion("exam_status in", values, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusNotIn(List<Integer> values) {
            addCriterion("exam_status not in", values, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusBetween(Integer value1, Integer value2) {
            addCriterion("exam_status between", value1, value2, "examStatus");
            return (Criteria) this;
        }

        public Criteria andExamStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("exam_status not between", value1, value2, "examStatus");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeIsNull() {
            addCriterion("begin_answer_time is null");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeIsNotNull() {
            addCriterion("begin_answer_time is not null");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeEqualTo(Date value) {
            addCriterion("begin_answer_time =", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeNotEqualTo(Date value) {
            addCriterion("begin_answer_time <>", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeGreaterThan(Date value) {
            addCriterion("begin_answer_time >", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("begin_answer_time >=", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeLessThan(Date value) {
            addCriterion("begin_answer_time <", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeLessThanOrEqualTo(Date value) {
            addCriterion("begin_answer_time <=", value, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeIn(List<Date> values) {
            addCriterion("begin_answer_time in", values, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeNotIn(List<Date> values) {
            addCriterion("begin_answer_time not in", values, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeBetween(Date value1, Date value2) {
            addCriterion("begin_answer_time between", value1, value2, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andBeginAnswerTimeNotBetween(Date value1, Date value2) {
            addCriterion("begin_answer_time not between", value1, value2, "beginAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeIsNull() {
            addCriterion("end_answer_time is null");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeIsNotNull() {
            addCriterion("end_answer_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeEqualTo(Date value) {
            addCriterion("end_answer_time =", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeNotEqualTo(Date value) {
            addCriterion("end_answer_time <>", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeGreaterThan(Date value) {
            addCriterion("end_answer_time >", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_answer_time >=", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeLessThan(Date value) {
            addCriterion("end_answer_time <", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_answer_time <=", value, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeIn(List<Date> values) {
            addCriterion("end_answer_time in", values, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeNotIn(List<Date> values) {
            addCriterion("end_answer_time not in", values, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeBetween(Date value1, Date value2) {
            addCriterion("end_answer_time between", value1, value2, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andEndAnswerTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_answer_time not between", value1, value2, "endAnswerTime");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIsNull() {
            addCriterion("sys_audit_total_score is null");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIsNotNull() {
            addCriterion("sys_audit_total_score is not null");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreEqualTo(Integer value) {
            addCriterion("sys_audit_total_score =", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotEqualTo(Integer value) {
            addCriterion("sys_audit_total_score <>", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreGreaterThan(Integer value) {
            addCriterion("sys_audit_total_score >", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_total_score >=", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreLessThan(Integer value) {
            addCriterion("sys_audit_total_score <", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreLessThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_total_score <=", value, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreIn(List<Integer> values) {
            addCriterion("sys_audit_total_score in", values, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotIn(List<Integer> values) {
            addCriterion("sys_audit_total_score not in", values, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_total_score between", value1, value2, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditTotalScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_total_score not between", value1, value2, "sysAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIsNull() {
            addCriterion("sys_audit_score is null");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIsNotNull() {
            addCriterion("sys_audit_score is not null");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreEqualTo(Integer value) {
            addCriterion("sys_audit_score =", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotEqualTo(Integer value) {
            addCriterion("sys_audit_score <>", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreGreaterThan(Integer value) {
            addCriterion("sys_audit_score >", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_score >=", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreLessThan(Integer value) {
            addCriterion("sys_audit_score <", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreLessThanOrEqualTo(Integer value) {
            addCriterion("sys_audit_score <=", value, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreIn(List<Integer> values) {
            addCriterion("sys_audit_score in", values, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotIn(List<Integer> values) {
            addCriterion("sys_audit_score not in", values, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_score between", value1, value2, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andSysAuditScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("sys_audit_score not between", value1, value2, "sysAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreIsNull() {
            addCriterion("lecturer_audit_total_score is null");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreIsNotNull() {
            addCriterion("lecturer_audit_total_score is not null");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreEqualTo(Integer value) {
            addCriterion("lecturer_audit_total_score =", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreNotEqualTo(Integer value) {
            addCriterion("lecturer_audit_total_score <>", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreGreaterThan(Integer value) {
            addCriterion("lecturer_audit_total_score >", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("lecturer_audit_total_score >=", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreLessThan(Integer value) {
            addCriterion("lecturer_audit_total_score <", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreLessThanOrEqualTo(Integer value) {
            addCriterion("lecturer_audit_total_score <=", value, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreIn(List<Integer> values) {
            addCriterion("lecturer_audit_total_score in", values, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreNotIn(List<Integer> values) {
            addCriterion("lecturer_audit_total_score not in", values, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreBetween(Integer value1, Integer value2) {
            addCriterion("lecturer_audit_total_score between", value1, value2, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditTotalScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("lecturer_audit_total_score not between", value1, value2, "lecturerAuditTotalScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreIsNull() {
            addCriterion("lecturer_audit_score is null");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreIsNotNull() {
            addCriterion("lecturer_audit_score is not null");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreEqualTo(Integer value) {
            addCriterion("lecturer_audit_score =", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreNotEqualTo(Integer value) {
            addCriterion("lecturer_audit_score <>", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreGreaterThan(Integer value) {
            addCriterion("lecturer_audit_score >", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("lecturer_audit_score >=", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreLessThan(Integer value) {
            addCriterion("lecturer_audit_score <", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreLessThanOrEqualTo(Integer value) {
            addCriterion("lecturer_audit_score <=", value, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreIn(List<Integer> values) {
            addCriterion("lecturer_audit_score in", values, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreNotIn(List<Integer> values) {
            addCriterion("lecturer_audit_score not in", values, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreBetween(Integer value1, Integer value2) {
            addCriterion("lecturer_audit_score between", value1, value2, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andLecturerAuditScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("lecturer_audit_score not between", value1, value2, "lecturerAuditScore");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoIsNull() {
            addCriterion("audit_user_no is null");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoIsNotNull() {
            addCriterion("audit_user_no is not null");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoEqualTo(Long value) {
            addCriterion("audit_user_no =", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoNotEqualTo(Long value) {
            addCriterion("audit_user_no <>", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoGreaterThan(Long value) {
            addCriterion("audit_user_no >", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("audit_user_no >=", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoLessThan(Long value) {
            addCriterion("audit_user_no <", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoLessThanOrEqualTo(Long value) {
            addCriterion("audit_user_no <=", value, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoIn(List<Long> values) {
            addCriterion("audit_user_no in", values, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoNotIn(List<Long> values) {
            addCriterion("audit_user_no not in", values, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoBetween(Long value1, Long value2) {
            addCriterion("audit_user_no between", value1, value2, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andAuditUserNoNotBetween(Long value1, Long value2) {
            addCriterion("audit_user_no not between", value1, value2, "auditUserNo");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameIsNull() {
            addCriterion("grade_exam_name is null");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameIsNotNull() {
            addCriterion("grade_exam_name is not null");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameEqualTo(String value) {
            addCriterion("grade_exam_name =", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameNotEqualTo(String value) {
            addCriterion("grade_exam_name <>", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameGreaterThan(String value) {
            addCriterion("grade_exam_name >", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameGreaterThanOrEqualTo(String value) {
            addCriterion("grade_exam_name >=", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameLessThan(String value) {
            addCriterion("grade_exam_name <", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameLessThanOrEqualTo(String value) {
            addCriterion("grade_exam_name <=", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameLike(String value) {
            addCriterion("grade_exam_name like", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameNotLike(String value) {
            addCriterion("grade_exam_name not like", value, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameIn(List<String> values) {
            addCriterion("grade_exam_name in", values, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameNotIn(List<String> values) {
            addCriterion("grade_exam_name not in", values, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameBetween(String value1, String value2) {
            addCriterion("grade_exam_name between", value1, value2, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andGradeExamNameNotBetween(String value1, String value2) {
            addCriterion("grade_exam_name not between", value1, value2, "gradeExamName");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeIsNull() {
            addCriterion("cutoff_end_time is null");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeIsNotNull() {
            addCriterion("cutoff_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeEqualTo(Date value) {
            addCriterion("cutoff_end_time =", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeNotEqualTo(Date value) {
            addCriterion("cutoff_end_time <>", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeGreaterThan(Date value) {
            addCriterion("cutoff_end_time >", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cutoff_end_time >=", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeLessThan(Date value) {
            addCriterion("cutoff_end_time <", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("cutoff_end_time <=", value, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeIn(List<Date> values) {
            addCriterion("cutoff_end_time in", values, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeNotIn(List<Date> values) {
            addCriterion("cutoff_end_time not in", values, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeBetween(Date value1, Date value2) {
            addCriterion("cutoff_end_time between", value1, value2, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andCutoffEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("cutoff_end_time not between", value1, value2, "cutoffEndTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeIsNull() {
            addCriterion("begin_exam_time is null");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeIsNotNull() {
            addCriterion("begin_exam_time is not null");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeEqualTo(Date value) {
            addCriterion("begin_exam_time =", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeNotEqualTo(Date value) {
            addCriterion("begin_exam_time <>", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeGreaterThan(Date value) {
            addCriterion("begin_exam_time >", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("begin_exam_time >=", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeLessThan(Date value) {
            addCriterion("begin_exam_time <", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeLessThanOrEqualTo(Date value) {
            addCriterion("begin_exam_time <=", value, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeIn(List<Date> values) {
            addCriterion("begin_exam_time in", values, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeNotIn(List<Date> values) {
            addCriterion("begin_exam_time not in", values, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeBetween(Date value1, Date value2) {
            addCriterion("begin_exam_time between", value1, value2, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andBeginExamTimeNotBetween(Date value1, Date value2) {
            addCriterion("begin_exam_time not between", value1, value2, "beginExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeIsNull() {
            addCriterion("end_exam_time is null");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeIsNotNull() {
            addCriterion("end_exam_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeEqualTo(Date value) {
            addCriterion("end_exam_time =", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeNotEqualTo(Date value) {
            addCriterion("end_exam_time <>", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeGreaterThan(Date value) {
            addCriterion("end_exam_time >", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_exam_time >=", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeLessThan(Date value) {
            addCriterion("end_exam_time <", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_exam_time <=", value, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeIn(List<Date> values) {
            addCriterion("end_exam_time in", values, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeNotIn(List<Date> values) {
            addCriterion("end_exam_time not in", values, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeBetween(Date value1, Date value2) {
            addCriterion("end_exam_time between", value1, value2, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andEndExamTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_exam_time not between", value1, value2, "endExamTime");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNull() {
            addCriterion("audit_status is null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIsNotNull() {
            addCriterion("audit_status is not null");
            return (Criteria) this;
        }

        public Criteria andAuditStatusEqualTo(Integer value) {
            addCriterion("audit_status =", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotEqualTo(Integer value) {
            addCriterion("audit_status <>", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThan(Integer value) {
            addCriterion("audit_status >", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("audit_status >=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThan(Integer value) {
            addCriterion("audit_status <", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusLessThanOrEqualTo(Integer value) {
            addCriterion("audit_status <=", value, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusIn(List<Integer> values) {
            addCriterion("audit_status in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotIn(List<Integer> values) {
            addCriterion("audit_status not in", values, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusBetween(Integer value1, Integer value2) {
            addCriterion("audit_status between", value1, value2, "auditStatus");
            return (Criteria) this;
        }

        public Criteria andAuditStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("audit_status not between", value1, value2, "auditStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
