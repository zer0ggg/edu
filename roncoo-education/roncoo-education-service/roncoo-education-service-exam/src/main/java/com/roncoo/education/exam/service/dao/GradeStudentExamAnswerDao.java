package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudentExamAnswerExample;

import java.util.List;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeStudentExamAnswerDao {

    /**
     * 保存
     *
     * @param record
     * @return 影响记录数
     */
    int save(GradeStudentExamAnswer record);

    /**
     * 根据ID删除
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record
     * @return 影响记录数
     */
    int updateById(GradeStudentExamAnswer record);

    /**
     * 根据ID获取
     *
     * @param id 主键ID
     * @return
     */
    GradeStudentExamAnswer getById(Long id);

    /**
     * 班级学生考试答案--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeStudentExamAnswer> listForPage(int pageCurrent, int pageSize, GradeStudentExamAnswerExample example);

    /**
     * 根据班级ID删除用户考试答案
     *
     * @param gradeId 班级ID
     * @return 影响记录数
     */
    int deleteByGradeId(Long gradeId);

    /**
     * 根据班级ID和学生ID删除户考试答案
     *
     * @param gradeId   班级ID
     * @param studentId 学生ID
     * @return 影响记录数
     */
    int deleteByGradeIdAndStudentId(Long gradeId, Long studentId);

    /**
     * 根据关联ID、标题id和题目ID 获取
     *
     * @param relationId 关联ID
     * @param problemId  题目ID
     * @return 用户考试答案
     */
    GradeStudentExamAnswer getByRelationIdAndProblemId(Long relationId, Long problemId);

    /**
     * 根据关联ID和题目ID
     *
     * @param relationId 关联ID
     * @param problemId  题目ID
     * @return 用户考试答案
     */
    GradeStudentExamAnswer getByRelationIdAndTitleIdProblemId(Long relationId, Long titleId,Long problemId);

    /**
     * 根据关联ID获取用户考试答案
     *
     * @param relationId 关联ID
     * @return 用户考试答案
     */
    List<GradeStudentExamAnswer> listByRelationId(Long relationId);

    /**
     * 根据关联ID和评阅状态获取用户考试答案集合
     * @param relationId
     * @param answerStatus
     * @return
     */
    List<GradeStudentExamAnswer> listByRelationIdAndAnswerStatus(Long relationId,Integer answerStatus);

    /**
     * 根据关联ID获取用户考试答案,查出大字段
     *
     * @param relationId 关联ID
     * @return 用户考试答案
     */
    List<GradeStudentExamAnswer> listByRelationIdWithBLOBs(Long relationId);

    /**
     * 根据关联ID和标题ID
     *
     * @param id
     * @param titleId
     * @return
     */
    List<GradeStudentExamAnswer> listByRelationIdAndTitleId(Long id, Long titleId);

    /**
     * 根据关联ID和标题ID,含大字段
     *
     * @param id
     * @param titleId
     * @return
     */
    List<GradeStudentExamAnswer> listByRelationIdAndTitleIdWithBLOBs(Long id, Long titleId);
}
