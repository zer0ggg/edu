package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 课程试卷关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseExamRefSaveREQ", description="课程试卷关联添加")
public class CourseExamRefSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;



    @NotNull(message = "课程分类不能为空")
    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)")
    private Integer courseCategory;

    @NotNull(message = "课程ID不能为空")
    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @NotNull(message = "关联类型不能为空")
    @ApiModelProperty(value = "关联类型：1课程，2章节，3课时")
    private Integer refType;

    @NotNull(message = "关联ID不能为空")
    @ApiModelProperty(value = "关联ID")
    private Long refId;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
}
