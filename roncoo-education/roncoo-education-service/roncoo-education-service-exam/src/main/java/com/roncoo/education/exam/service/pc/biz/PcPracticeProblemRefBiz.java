package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.PracticeProblemRefEditREQ;
import com.roncoo.education.exam.common.req.PracticeProblemRefListREQ;
import com.roncoo.education.exam.common.req.PracticeProblemRefSaveREQ;
import com.roncoo.education.exam.common.resp.PracticeProblemRefListRESP;
import com.roncoo.education.exam.common.resp.PracticeProblemRefViewRESP;
import com.roncoo.education.exam.service.dao.PracticeProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 题目练习关联
 *
 * @author LHR
 */
@Component
public class PcPracticeProblemRefBiz extends BaseBiz {

    @Autowired
    private PracticeProblemRefDao dao;

    /**
    * 题目练习关联列表
    *
    * @param practiceProblemRefListREQ 题目练习关联分页查询参数
    * @return 题目练习关联分页查询结果
    */
    public Result<Page<PracticeProblemRefListRESP>> list(PracticeProblemRefListREQ practiceProblemRefListREQ) {
        PracticeProblemRefExample example = new PracticeProblemRefExample();
        Criteria c = example.createCriteria();
        Page<PracticeProblemRef> page = dao.listForPage(practiceProblemRefListREQ.getPageCurrent(), practiceProblemRefListREQ.getPageSize(), example);
        Page<PracticeProblemRefListRESP> respPage = PageUtil.transform(page, PracticeProblemRefListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 题目练习关联添加
    *
    * @param practiceProblemRefSaveREQ 题目练习关联
    * @return 添加结果
    */
    public Result<String> save(PracticeProblemRefSaveREQ practiceProblemRefSaveREQ) {
        PracticeProblemRef record = BeanUtil.copyProperties(practiceProblemRefSaveREQ, PracticeProblemRef.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 题目练习关联查看
    *
    * @param id 主键ID
    * @return 题目练习关联
    */
    public Result<PracticeProblemRefViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), PracticeProblemRefViewRESP.class));
    }


    /**
    * 题目练习关联修改
    *
    * @param practiceProblemRefEditREQ 题目练习关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(PracticeProblemRefEditREQ practiceProblemRefEditREQ) {
        PracticeProblemRef record = BeanUtil.copyProperties(practiceProblemRefEditREQ, PracticeProblemRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 题目练习关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
