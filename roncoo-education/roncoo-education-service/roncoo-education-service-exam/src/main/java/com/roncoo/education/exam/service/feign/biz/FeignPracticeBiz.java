package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.PracticeQO;
import com.roncoo.education.exam.feign.vo.PracticeVO;
import com.roncoo.education.exam.service.dao.PracticeDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 练习信息
 *
 * @author LHR
 */
@Component
public class FeignPracticeBiz extends BaseBiz {

    @Autowired
    private PracticeDao dao;

	public Page<PracticeVO> listForPage(PracticeQO qo) {
	    PracticeExample example = new PracticeExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<Practice> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, PracticeVO.class);
	}

	public int save(PracticeQO qo) {
		Practice record = BeanUtil.copyProperties(qo, Practice.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public PracticeVO getById(Long id) {
		Practice record = dao.getById(id);
		return BeanUtil.copyProperties(record, PracticeVO.class);
	}

	public int updateById(PracticeQO qo) {
		Practice record = BeanUtil.copyProperties(qo, Practice.class);
		return dao.updateById(record);
	}

}
