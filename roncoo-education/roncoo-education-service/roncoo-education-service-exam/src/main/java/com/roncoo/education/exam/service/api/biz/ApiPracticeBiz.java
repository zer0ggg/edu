package com.roncoo.education.exam.service.api.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.PracticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 练习信息
 *
 * @author LHR
 */
@Component
public class ApiPracticeBiz extends BaseBiz {

    @Autowired
    private PracticeDao dao;

}
