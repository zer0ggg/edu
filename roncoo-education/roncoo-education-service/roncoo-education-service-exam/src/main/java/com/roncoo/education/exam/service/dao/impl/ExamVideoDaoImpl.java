package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.exam.service.dao.ExamVideoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamVideoMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamVideoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷视频 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamVideoDaoImpl implements ExamVideoDao {

    @Autowired
    private ExamVideoMapper mapper;

    @Override
    public int save(ExamVideo record) {
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamVideo record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamVideo getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamVideo> listForPage(int pageCurrent, int pageSize, ExamVideoExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public ExamVideo getByVideoVid(String vid) {
        ExamVideoExample examVideoExample = new ExamVideoExample();
        ExamVideoExample.Criteria c = examVideoExample.createCriteria();
        c.andVideoVidEqualTo(vid);
        List<ExamVideo> list = this.mapper.selectByExample(examVideoExample);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public int updateByVid(ExamVideo record) {
        ExamVideoExample examVideoExample = new ExamVideoExample();
        ExamVideoExample.Criteria c = examVideoExample.createCriteria();
        c.andVideoVidEqualTo(record.getVideoVid());
        return this.mapper.updateByExampleSelective(record, examVideoExample);
    }


}
