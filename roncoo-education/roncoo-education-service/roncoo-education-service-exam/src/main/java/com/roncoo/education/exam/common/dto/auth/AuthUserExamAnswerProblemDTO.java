package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 用户试卷答案小题
 *
 * @author LYQ
 */
@ApiModel(value = "用户试卷答案小题", description = "用户试卷答案小题")
@Data
@Accessors(chain = true)
public class AuthUserExamAnswerProblemDTO implements Serializable {

    private static final long serialVersionUID = 3272745133873574771L;

    @ApiModelProperty(value = "ID主键")
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "讲师编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "题目类型")
    private Integer problemType;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "视频名称")
    private String videoName;

    @ApiModelProperty(value = "时长")
    private String videoLength;

    @ApiModelProperty(value = "视频VID")
    private String videoVid;

    @ApiModelProperty(value = "分值")
    private Integer score;

    @ApiModelProperty(value = "用户得分")
    private Integer userScore;

    @ApiModelProperty(value = "用户答案")
    private String userAnswer;

    @ApiModelProperty(value = "题目答案")
    private String problemAnswer;

    private Integer problemStatus;

    @ApiModelProperty(value = "关联小题")
    private List<AuthUserExamAnswerProblemDTO> childrenList;

    @ApiModelProperty(value = "小题选项")
    private List<AuthUserExamAnswerProblemOptionDTO> optionList;
}
