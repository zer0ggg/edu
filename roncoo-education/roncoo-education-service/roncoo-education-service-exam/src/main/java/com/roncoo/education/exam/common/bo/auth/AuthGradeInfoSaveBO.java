package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 班级信息
 * </p>
 *
 * @author wujing
 * @date 2020-06-01
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeInfoSaveBO", description = "班级保存对象")
public class AuthGradeInfoSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "班级名称不能为空")
    @ApiModelProperty(value = "班级名称", required = true)
    private String gradeName;

    @NotBlank(message = "班级简介不能为空")
    @ApiModelProperty(value = "班级简介", required = true)
    private String gradeIntro;

    @NotNull(message = "班级权限不能为空")
    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)", required = true)
    private Integer verificationSet;
}
