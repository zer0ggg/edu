package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 课程试卷关联
 * </p>
 *
 * @author wujing
 * @date 2020-10-21
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthCourseExamRefViewBO", description="课程试卷关联")
public class AuthCourseExamRefViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "课程ID不能为空")
    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @NotNull(message = "课程类型不能为空")
    @ApiModelProperty(value = "课程类型：1课程，2章节，3课时")
    private Integer courseType;

    @NotNull(message = "关联ID不能为空")
    @ApiModelProperty(value = "关联ID")
    private Long refId;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID")
    private Long examId;
}
