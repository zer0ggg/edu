package com.roncoo.education.exam.common.bo.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 班级考试布置对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeExamSaveBO", description = "班级考试布置对象")
public class AuthGradeExamSaveBO implements Serializable {

    private static final long serialVersionUID = -7753019257932890611L;

    @NotBlank(message = "班级考试名次不能为空")
    @ApiModelProperty(value = "班级考试名称", required = true)
    private String gradeExamName;

    @NotNull(message = "试卷ID不能为空")
    @ApiModelProperty(value = "试卷ID", required = true)
    private Long examId;

    @NotNull(message = "考试开始时间不能为空")
    @ApiModelProperty(value = "考试开始时间", required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    @NotNull(message = "考试结束时间不能为空")
    @ApiModelProperty(value = "考试结束时间", required = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @NotNull(message = "班级ID不能为空")
    @ApiModelProperty(value = "班级ID", required = true)
    private Long gradeId;

    @NotEmpty(message = "学生ID为必传")
    @Size(min = 1, message = "学生ID集合不能为空")
    @ApiModelProperty(value = "学生ID集合", required = true)
    private List<Long> studentIdList;

    @NotNull(message = "答案展示不能为空")
    @ApiModelProperty(value = "答案展示", required = true)
    private Integer answerShow;

    @ApiModelProperty(value = "答案展示时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date answerShowTime;

    @NotNull(message = "评阅类型不能为空")
    @ApiModelProperty(value = "评阅类型(1:不评阅，2:评阅)", required = true)
    private Integer auditType;

    @NotNull(message = "补交状态不能为空")
    @ApiModelProperty(value = "补交状态(1:关闭，2:开启)", required = true)
    private Integer compensateStatus;
}
