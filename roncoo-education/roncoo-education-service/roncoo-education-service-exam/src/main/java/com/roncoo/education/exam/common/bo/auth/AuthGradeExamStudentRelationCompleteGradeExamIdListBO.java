package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级学生考试提示列表请求对象
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthGradeExamStudentRelationGradeExamIdListBO", description = "班级学生考试列表请求对象")
public class AuthGradeExamStudentRelationCompleteGradeExamIdListBO implements Serializable {

    private static final long serialVersionUID = 1087298828115084717L;

    @NotNull(message = "班级考试ID不能为空")
    @ApiModelProperty(value = "班级考试ID", required = true)
    private Long gradeExamId;
}
