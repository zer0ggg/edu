package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamTitleEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleListREQ;
import com.roncoo.education.exam.common.req.ExamTitleSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamTitleBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷标题 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷标题管理")
@RestController
@RequestMapping("/exam/pc/exam/title")
public class PcExamTitleController {

    @Autowired
    private PcExamTitleBiz biz;

    @ApiOperation(value = "试卷标题列表", notes = "试卷标题列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamTitleListRESP>> list(@RequestBody ExamTitleListREQ examTitleListREQ) {
        return biz.list(examTitleListREQ);
    }

    @ApiOperation(value = "试卷标题添加", notes = "试卷标题添加")
    @SysLog(value = "试卷标题添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamTitleSaveREQ examTitleSaveREQ) {
        return biz.save(examTitleSaveREQ);
    }

    @ApiOperation(value = "试卷标题查看", notes = "试卷标题查看")
    @GetMapping(value = "/view")
    public Result<ExamTitleViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷标题修改", notes = "试卷标题修改")
    @SysLog(value = "试卷标题修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamTitleEditREQ examTitleEditREQ) {
        return biz.edit(examTitleEditREQ);
    }

    @ApiOperation(value = "试卷标题删除", notes = "试卷标题删除")
    @SysLog(value = "试卷标题删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
