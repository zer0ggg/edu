package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 删除练习信息
 * </p>
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthPracticeDeleteBO ", description = "删除练习信息")
public class AuthPracticeDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    private Long id;
}
