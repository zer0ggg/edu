package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamTitleProblemRefAuditMapper {
    int countByExample(ExamTitleProblemRefAuditExample example);

    int deleteByExample(ExamTitleProblemRefAuditExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamTitleProblemRefAudit record);

    int insertSelective(ExamTitleProblemRefAudit record);

    List<ExamTitleProblemRefAudit> selectByExample(ExamTitleProblemRefAuditExample example);

    ExamTitleProblemRefAudit selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamTitleProblemRefAudit record, @Param("example") ExamTitleProblemRefAuditExample example);

    int updateByExample(@Param("record") ExamTitleProblemRefAudit record, @Param("example") ExamTitleProblemRefAuditExample example);

    int updateByPrimaryKeySelective(ExamTitleProblemRefAudit record);

    int updateByPrimaryKey(ExamTitleProblemRefAudit record);
}