package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户考试记录
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserExamRecordSaveREQ", description="用户考试记录添加")
public class UserExamRecordSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "答卷时间")
    private Integer answerTime;

    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅分值")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "人工评阅分值")
    private Integer handworkAuditTotalScore;

    @ApiModelProperty(value = "人工评阅得分")
    private Integer handworkAuditScore;

    @ApiModelProperty(value = "试卷状态(1:考试中、2:考试完成、3:评阅完成)")
    private Integer examStatus;
}
