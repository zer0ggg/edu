package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeExamStudentRelationQO;
import com.roncoo.education.exam.feign.vo.GradeExamStudentRelationVO;
import com.roncoo.education.exam.service.dao.GradeExamStudentRelationDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelation;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeExamStudentRelationBiz extends BaseBiz {

    @Autowired
    private GradeExamStudentRelationDao dao;

	public Page<GradeExamStudentRelationVO> listForPage(GradeExamStudentRelationQO qo) {
	    GradeExamStudentRelationExample example = new GradeExamStudentRelationExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeExamStudentRelation> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeExamStudentRelationVO.class);
	}

	public int save(GradeExamStudentRelationQO qo) {
		GradeExamStudentRelation record = BeanUtil.copyProperties(qo, GradeExamStudentRelation.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeExamStudentRelationVO getById(Long id) {
		GradeExamStudentRelation record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeExamStudentRelationVO.class);
	}

	public int updateById(GradeExamStudentRelationQO qo) {
		GradeExamStudentRelation record = BeanUtil.copyProperties(qo, GradeExamStudentRelation.class);
		return dao.updateById(record);
	}

}
