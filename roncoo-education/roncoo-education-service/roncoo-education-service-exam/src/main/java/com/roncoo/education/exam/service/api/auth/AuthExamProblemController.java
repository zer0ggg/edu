package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthExamProblemViewDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthExamProblemBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * 试卷题目 UserApi接口
 *
 * @author wujing
 * @date 2020-05-20
 */
@RestController
@RequestMapping("/exam/auth/exam/problem")
@Api(tags = "API-AUTH--题目管理")
public class AuthExamProblemController {

    @Autowired
    private AuthExamProblemBiz biz;

    @ApiOperation(value = "分页接口", notes = "分页接口")
    @PostMapping(value = "/list")
    public Result<Page<AuthExamProblemPageDTO>> listForPage(@RequestBody AuthExamProblemPageBO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "详情接口", notes = "详情接口")
    @GetMapping(value = "/view/{id}")
    public Result<AuthExamProblemViewDTO> view(@PathVariable(value = "id") Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "保存接口", notes = "保存接口")
    @PostMapping(value = "/save")
    public Result<Integer> save(@RequestBody @Valid AuthExamProblemSaveBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "删除接口", notes = "删除接口")
    @PostMapping(value = "/delete")
    public Result<String> delete(@RequestBody AuthExamProblemDeleteBO bo) {
        return biz.delete(bo);
    }

    @ApiOperation(value = "更新接口", notes = "更新接口")
    @PostMapping(value = "/update")
    public Result<Integer> update(@RequestBody @Valid AuthExamProblemUpdateBO bo) {
        return biz.update(bo);
    }

    @ApiOperation(value = "导入试题", notes = "导入试题")
    @PostMapping(value = "/upload/excel")
    public Result<String> uploadExcel(@RequestParam("file") MultipartFile file) {
        return biz.uploadExcel(file);
    }

}
