package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.ExamCategoryListEditRESP;
import com.roncoo.education.exam.common.resp.ExamCategoryRESP;
import com.roncoo.education.exam.service.dao.ExamCategoryDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategory;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamCategoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 试卷分类
 *
 * @author wujing
 */
@Component
public class PcExamCategoryBiz extends BaseBiz {

	@Autowired
	private ExamCategoryDao dao;

	/**
	 * 试卷分类列表
	 *
	 * @param req 试卷分类分页查询参数
	 * @return 试卷分类分页查询结果
	 */
	public Result<Page<ExamCategoryRESP>> list(ExamCategoryPageREQ req) {
		ExamCategoryExample example = new ExamCategoryExample();
		Criteria c = example.createCriteria();
		if (req.getStatusId() != null) {
			c.andStatusIdEqualTo(req.getStatusId());
		}
		if (req.getExamCategoryType() != null) {
			c.andExamCategoryTypeEqualTo(req.getExamCategoryType());
		}
		if (req.getCategoryType() != null) {
			c.andCategoryTypeEqualTo(req.getCategoryType());
		}
		if (!StringUtils.isEmpty(req.getCategoryName())) {
			c.andCategoryNameLike(PageUtil.like(req.getCategoryName()));
		} else {
			c.andFloorEqualTo(1);
		}
		example.setOrderByClause(" status_id desc, sort asc, id desc ");
		Page<ExamCategory> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
		Page<ExamCategoryRESP> RespPage = PageUtil.transform(page, ExamCategoryRESP.class);
		for (ExamCategoryRESP resp : RespPage.getList()) {
			resp.setChildrenList(recursionList(resp.getId()));
		}
		return Result.success(RespPage);
	}

	/**
	 * 递归展示分类
	 *
	 * @param parentId
	 * @return
	 */
	private List<ExamCategoryRESP> recursionList(Long parentId) {
		List<ExamCategoryRESP> list = new ArrayList<>();
		List<ExamCategory> categoryList = dao.listByParentId(parentId);
		if (!CollectionUtils.isEmpty(categoryList)) {
			for (ExamCategory examCategory : categoryList) {
				ExamCategoryRESP resp = BeanUtil.copyProperties(examCategory, ExamCategoryRESP.class);
				resp.setChildrenList(recursionList(examCategory.getId()));
				list.add(resp);
			}
		}
		return list;
	}

	/**
	 * 试卷分类添加
	 *
	 * @param req 试卷分类
	 * @return 添加结果
	 */
	public Result<String> save(ExamCategorySaveREQ req) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		ExamCategory record = BeanUtil.copyProperties(req, ExamCategory.class);
		record.setId(IdWorker.getId());
		record.setStatusId(StatusIdEnum.YES.getCode());
		// 补充参数
		if (req.getParentId() == null || req.getParentId() == 0) {
			record.setFloor(1);
		} else {
			ExamCategory pCategory = dao.getById(req.getParentId());
			record.setCategoryType(pCategory.getCategoryType());
			record.setExamCategoryType(pCategory.getExamCategoryType());
			record.setFloor(pCategory.getFloor() + 1);
		}

		// 防止名称重复
		ExamCategory oldRecord = dao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(record.getCategoryType(), record.getExamCategoryType(), record.getParentId(), record.getCategoryName());
		if (!ObjectUtils.isEmpty(oldRecord)) {
			return Result.error("名称重复，请检查");
		}
		record.setId(IdWorker.getId());
		if (dao.save(record) > 0) {
			return Result.success("操作成功");
		}
		return Result.error("操作失败");
	}

	/**
	 * 试卷分类查看
	 *
	 * @param id 主键ID
	 * @return 试卷分类
	 */
	public Result<ExamCategoryRESP> view(Long id) {
		if (id == null) {
			return Result.error("id不能为空");
		}
		ExamCategory record = dao.getById(id);
		if (ObjectUtils.isEmpty(record)) {
			return Result.error("id不正确");
		}
		ExamCategoryRESP category = BeanUtil.copyProperties(record, ExamCategoryRESP.class);
		category.setChildrenList(recursionList(category.getId()));
		return Result.success(category);
	}

	/**
	 * 试卷分类修改
	 *
	 * @param req 试卷分类修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamCategoryEditREQ req) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		ExamCategory record = dao.getById(req.getId());
		if (ObjectUtils.isEmpty(record)) {
			return Result.error("id不正确");
		}
		// 防止名称重复
		ExamCategory oldRecord = dao.getByCategoryTypeAndExamCategoryTypeAndParentIdAndFloorAndCategoryName(record.getCategoryType(), record.getExamCategoryType(), record.getParentId(), req.getCategoryName());
		if (!ObjectUtils.isEmpty(oldRecord) && oldRecord.getId().longValue() != req.getId().longValue()) {
			return Result.error("名称重复，请检查!");
		}
		ExamCategory category = BeanUtil.copyProperties(req, ExamCategory.class);
		category.setRemark(req.getRemark());
		if (dao.updateById(category) > 0) {
			return Result.success("操作成功");
		}
		return Result.error("操作失败");
	}

	/**
	 * 试卷分类删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (id == null) {
			return Result.error("id不能为空");
		}
		ExamCategory record = dao.getById(id);
		if (ObjectUtils.isEmpty(record)) {
			return Result.error("id不正确");
		}
		List<ExamCategory> sonList = dao.listByParentId(id);
		if (!CollectionUtils.isEmpty(sonList)) {
			return Result.error("删除失败，请先删除子一级分类");
		}
		if (dao.deleteById(id) > 0) {
			return Result.success("操作成功");
		}
		return Result.error("操作失败");
	}

	public Result<String> updateStatus(ExamCategoryUpdateStatusREQ req) {
		BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
		if (!result.isPass()) {
			return Result.error(result.getErrMsgString());
		}
		if (!StatusIdEnum.YES.getCode().equals(req.getStatusId()) && !StatusIdEnum.NO.getCode().equals(req.getStatusId())) {
			return Result.success("请输入正确状态值(1:正常，0:禁用)");
		}
		ExamCategory bean = BeanUtil.copyProperties(req, ExamCategory.class);
		if (dao.updateById(bean) > 0) {
			return Result.success("操作成功");
		}
		return Result.error("操作失败");

	}

	/**
	 * 获取考试分类信息列表
	 *
	 * @return
	 */
	public Result<ExamCategoryListEditRESP> listEdit(ExamCategoryListEditREQ req) {
		ExamCategoryListEditRESP dto = new ExamCategoryListEditRESP();
		// 根据分类类型、层级查询、分类类型、可用状态的考试分类集合
		List<ExamCategory> oneCategoryList = dao.listByFloorAndStatusIdAndCategoryTypeAndExamCategoryType(1, StatusIdEnum.YES.getCode(), req.getCategoryType(),req.getExamCategoryType());
		if (CollectionUtils.isEmpty(oneCategoryList)) {
			return Result.success(dto);
		}

		List<ExamCategoryRESP> examCategoryList = new ArrayList<>();

		for (ExamCategory examCategory : oneCategoryList) {
			ExamCategoryRESP examCategoryDTO = BeanUtil.copyProperties(examCategory, ExamCategoryRESP.class);
			// 递归获取考试分类
			examCategoryDTO.setChildrenList(recursionList(examCategory.getId()));
			examCategoryList.add(examCategoryDTO);
		}
		dto.setExamCategoryList(examCategoryList);
		return Result.success(dto);
	}
}
