package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.exam.feign.interfaces.IFeignUserOrderExamRef;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefQO;
import com.roncoo.education.exam.feign.qo.UserOrderExamRefSaveQO;
import com.roncoo.education.exam.feign.vo.UserOrderExamRefVO;
import com.roncoo.education.exam.service.feign.biz.FeignUserOrderExamRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户订单试卷关联
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignUserOrderExamRefController extends BaseController implements IFeignUserOrderExamRef {

    @Autowired
    private FeignUserOrderExamRefBiz biz;

    @Override
    public UserOrderExamRefVO getByUserNoAndExamId(@RequestBody UserOrderExamRefQO qo) {
        return biz.getByUserNoAndExamId(qo);
    }

    @Override
    public int save(@RequestBody UserOrderExamRefSaveQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(Long id) {
        return biz.deleteById(id);
    }
}
