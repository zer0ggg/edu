package com.roncoo.education.exam.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.ExamTitleAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.ExamTitleAuditMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleAuditExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 试卷标题审核 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class ExamTitleAuditDaoImpl implements ExamTitleAuditDao {

    @Autowired
    private ExamTitleAuditMapper mapper;

    @Override
    public int save(ExamTitleAudit record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ExamTitleAudit record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ExamTitleAudit getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<ExamTitleAudit> listForPage(int pageCurrent, int pageSize, ExamTitleAuditExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<ExamTitleAudit> listByExamId(Long examId) {
        ExamTitleAuditExample example = new ExamTitleAuditExample();
        ExamTitleAuditExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<ExamTitleAudit> listByExamIdAndAuditStatus(Long examId, Integer code) {
        ExamTitleAuditExample example = new ExamTitleAuditExample();
        ExamTitleAuditExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        if (code != null) {
            c.andAuditStatusEqualTo(code);
        }
        example.setOrderByClause("sort asc, id desc");
        return this.mapper.selectByExample(example);
    }

    @Override
    public int deleteByExamId(Long examId) {
        ExamTitleAuditExample example = new ExamTitleAuditExample();
        ExamTitleAuditExample.Criteria c = example.createCriteria();
        c.andExamIdEqualTo(examId);
        return this.mapper.deleteByExample(example);
    }

    @Override
    public int updateAuditStatus(ExamTitleAudit record, ExamTitleAuditExample example) {
        return 0;
    }

    @Override
    public int update(ExamTitleAudit record, ExamTitleAuditExample example) {
        return this.mapper.updateByExampleSelective(record,example);
    }

}
