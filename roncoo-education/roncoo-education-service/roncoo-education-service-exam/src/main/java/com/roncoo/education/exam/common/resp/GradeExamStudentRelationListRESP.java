package com.roncoo.education.exam.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 班级学生考试列表响应对象
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeExamStudentRelationListRESP", description = "班级学生考试列表响应对象")
public class GradeExamStudentRelationListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "状态ID(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "班级ID")
    private Long gradeId;

    @ApiModelProperty(value = "班级考试ID")
    private Long gradeExamId;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    @ApiModelProperty(value = "学生ID")
    private Long studentId;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "学员昵称")
    private String nickname;

    @ApiModelProperty(value = "用户角色")
    private Integer userRole;

    @ApiModelProperty(value = "查看状态(1:未查看，2:已查看，3:已忽略)")
    private Integer viewStatus;

    @ApiModelProperty(value = "考试状态(1:未考试，2:考试中，3:考试完成，4:批阅中，5:批阅完成)")
    private Integer examStatus;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private Date beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "系统评阅总分")
    private Integer sysAuditTotalScore;

    @ApiModelProperty(value = "系统评阅得分")
    private Integer sysAuditScore;

    @ApiModelProperty(value = "讲师评阅总分")
    private Integer lecturerAuditTotalScore;

    @ApiModelProperty(value = "讲师评阅得分")
    private Integer lecturerAuditScore;

    @ApiModelProperty(value = "评阅用户编号")
    private Long auditUserNo;

    @ApiModelProperty(value = "班级考试名称")
    private String gradeExamName;

    @ApiModelProperty(value = "截止结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cutoffEndTime;

    @ApiModelProperty(value = "开始答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginAnswerTime;

    @ApiModelProperty(value = "结束答卷时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endAnswerTime;
}