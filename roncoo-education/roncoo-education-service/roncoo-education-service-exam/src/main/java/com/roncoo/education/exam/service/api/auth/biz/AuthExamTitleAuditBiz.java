package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamTitleAuditDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题审核
 *
 * @author wujing
 */
@Component
public class AuthExamTitleAuditBiz extends BaseBiz {

    @Autowired
    private ExamTitleAuditDao dao;

}
