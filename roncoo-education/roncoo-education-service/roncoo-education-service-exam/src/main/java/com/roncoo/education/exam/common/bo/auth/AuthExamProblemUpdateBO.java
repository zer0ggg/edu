package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 试卷信息审核表-更新
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamProblemUpdateBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "试题ID不能为空")
    @ApiModelProperty(value = "主键id", required = true)
    private Long id;

    @ApiModelProperty(value = "考点Id")
    private Long emphasisId;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "难度id")
    private Long difficultyId;

    @ApiModelProperty(value = "题类id")
    private Long topicId;

    @ApiModelProperty(value = "题目内容")
    private String problemContent;

    @ApiModelProperty(value = "区域")
    private String region;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "考点")
    private String emphasis;

    @ApiModelProperty(value = "难度等级")
    private Integer examLevel;

    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "分值", required = true)
    private Integer score;

    @ApiModelProperty(value = "视频类型(1:解答视频,2:试题视频)")
    private Integer videoType;

    @ApiModelProperty(value = "视频ID")
    private Long videoId;

    @ApiModelProperty(value = "是否公开(1:公开,0:不公开)")
    private Integer isPublic;

    @ApiModelProperty(value = "下一级试题集合")
    List<AuthExamProblemSaveBO> childrenList;

    @ApiModelProperty(value = "题目答案")
    private String problemAnswer;

    @ApiModelProperty(value = "试题选项集合")
    private List<AuthExamProblemOptionSaveBO> optionList;


}
