package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷标题审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamTitleAuditSortBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标题排序
     */
    @NotNull(message = "标题排序不能为空")
    @ApiModelProperty(value = "标题排序",required = true)
    private Integer titleSort;
    /**
     * 试卷ID
     */
    @NotNull(message = "标题ID不能为空")
    @ApiModelProperty(value = "标题ID",required = true)
    private Long Id;

}
