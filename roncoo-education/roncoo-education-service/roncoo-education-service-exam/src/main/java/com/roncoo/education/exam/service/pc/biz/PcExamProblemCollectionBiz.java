package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamProblemCollectionEditREQ;
import com.roncoo.education.exam.common.req.ExamProblemCollectionListREQ;
import com.roncoo.education.exam.common.req.ExamProblemCollectionSaveREQ;
import com.roncoo.education.exam.common.resp.ExamProblemCollectionListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemCollectionViewRESP;
import com.roncoo.education.exam.service.dao.ExamProblemCollectionDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollection;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemCollectionExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷题目收藏
 *
 * @author wujing
 */
@Component
public class PcExamProblemCollectionBiz extends BaseBiz {

	@Autowired
	private ExamProblemCollectionDao dao;

	/**
	 * 试卷题目收藏列表
	 *
	 * @param examProblemCollectionListREQ 试卷题目收藏分页查询参数
	 * @return 试卷题目收藏分页查询结果
	 */
	public Result<Page<ExamProblemCollectionListRESP>> list(ExamProblemCollectionListREQ examProblemCollectionListREQ) {
		ExamProblemCollectionExample example = new ExamProblemCollectionExample();
		Criteria c = example.createCriteria();
		Page<ExamProblemCollection> page = dao.listForPage(examProblemCollectionListREQ.getPageCurrent(), examProblemCollectionListREQ.getPageSize(), example);
		Page<ExamProblemCollectionListRESP> respPage = PageUtil.transform(page, ExamProblemCollectionListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷题目收藏添加
	 *
	 * @param examProblemCollectionSaveREQ 试卷题目收藏
	 * @return 添加结果
	 */
	public Result<String> save(ExamProblemCollectionSaveREQ examProblemCollectionSaveREQ) {
		ExamProblemCollection record = BeanUtil.copyProperties(examProblemCollectionSaveREQ, ExamProblemCollection.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷题目收藏查看
	 *
	 * @param id 主键ID
	 * @return 试卷题目收藏
	 */
	public Result<ExamProblemCollectionViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamProblemCollectionViewRESP.class));
	}

	/**
	 * 试卷题目收藏修改
	 *
	 * @param examProblemCollectionEditREQ 试卷题目收藏修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamProblemCollectionEditREQ examProblemCollectionEditREQ) {
		ExamProblemCollection record = BeanUtil.copyProperties(examProblemCollectionEditREQ, ExamProblemCollection.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷题目收藏删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
