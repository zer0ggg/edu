package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加班级信息请求对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "GradeInfoSaveREQ", description = "添加班级信息请求对象")
public class GradeInfoSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "讲师用户编号不能为空")
    @ApiModelProperty(value = "讲师用户编号", required = true)
    private Long lecturerUserNo;

    @NotBlank(message = "班级名称不能为空")
    @ApiModelProperty(value = "班级名称", required = true)
    private String gradeName;

    @NotBlank(message = "班级简介不能为空")
    @ApiModelProperty(value = "班级简介", required = true)
    private String gradeIntro;

    @NotNull(message = "验证设置不能为空")
    @ApiModelProperty(value = "验证设置(1:允许任何人加入，2:加入时需要验证)", required = true)
    private Integer verificationSet;

    @ApiModelProperty(value = "后台排序")
    private Integer backstageSort;

}
