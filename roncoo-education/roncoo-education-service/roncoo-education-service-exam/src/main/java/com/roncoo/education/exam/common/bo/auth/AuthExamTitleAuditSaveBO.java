package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 试卷标题审核表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthExamTitleAuditSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标题id,更新操作，必传原标题id")
    private Long id = null;

    @ApiModelProperty(value = "标题排序")
    private Integer sort;

    @NotNull(message = "标题名称不能为空")
    @ApiModelProperty(value = "标题名称", required = true)
    private String titleName;

    @NotNull(message = "标题类型不能为空")
    @ApiModelProperty(value = "标题类型", required = true)
    private Integer titleType;

    @ApiModelProperty(value = "题目")
    private List<AuthExamTitleProblemRefAuditSaveBO> problemList;

}
