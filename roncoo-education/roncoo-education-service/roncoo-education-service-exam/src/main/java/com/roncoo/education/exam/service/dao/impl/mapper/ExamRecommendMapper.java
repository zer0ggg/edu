package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommend;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamRecommendExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamRecommendMapper {
    int countByExample(ExamRecommendExample example);

    int deleteByExample(ExamRecommendExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamRecommend record);

    int insertSelective(ExamRecommend record);

    List<ExamRecommend> selectByExample(ExamRecommendExample example);

    ExamRecommend selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamRecommend record, @Param("example") ExamRecommendExample example);

    int updateByExample(@Param("record") ExamRecommend record, @Param("example") ExamRecommendExample example);

    int updateByPrimaryKeySelective(ExamRecommend record);

    int updateByPrimaryKey(ExamRecommend record);
}