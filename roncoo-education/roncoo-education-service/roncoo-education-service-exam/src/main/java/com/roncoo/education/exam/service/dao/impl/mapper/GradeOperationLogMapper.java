package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLog;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeOperationLogMapper {
    int countByExample(GradeOperationLogExample example);

    int deleteByExample(GradeOperationLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeOperationLog record);

    int insertSelective(GradeOperationLog record);

    List<GradeOperationLog> selectByExample(GradeOperationLogExample example);

    GradeOperationLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeOperationLog record, @Param("example") GradeOperationLogExample example);

    int updateByExample(@Param("record") GradeOperationLog record, @Param("example") GradeOperationLogExample example);

    int updateByPrimaryKeySelective(GradeOperationLog record);

    int updateByPrimaryKey(GradeOperationLog record);
}
