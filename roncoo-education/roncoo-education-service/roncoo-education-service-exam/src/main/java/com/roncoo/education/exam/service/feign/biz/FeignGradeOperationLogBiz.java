package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.GradeOperationLogQO;
import com.roncoo.education.exam.feign.vo.GradeOperationLogVO;
import com.roncoo.education.exam.service.dao.GradeOperationLogDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLog;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeOperationLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 *
 * @author wujing
 */
@Component
public class FeignGradeOperationLogBiz extends BaseBiz {

    @Autowired
    private GradeOperationLogDao dao;

	public Page<GradeOperationLogVO> listForPage(GradeOperationLogQO qo) {
	    GradeOperationLogExample example = new GradeOperationLogExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<GradeOperationLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, GradeOperationLogVO.class);
	}

	public int save(GradeOperationLogQO qo) {
		GradeOperationLog record = BeanUtil.copyProperties(qo, GradeOperationLog.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public GradeOperationLogVO getById(Long id) {
		GradeOperationLog record = dao.getById(id);
		return BeanUtil.copyProperties(record, GradeOperationLogVO.class);
	}

	public int updateById(GradeOperationLogQO qo) {
		GradeOperationLog record = BeanUtil.copyProperties(qo, GradeOperationLog.class);
		return dao.updateById(record);
	}

}
