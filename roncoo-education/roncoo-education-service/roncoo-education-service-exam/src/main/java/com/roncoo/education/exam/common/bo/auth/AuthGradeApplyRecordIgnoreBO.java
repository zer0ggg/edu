package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 申请记录忽略请求对象
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthGradeApplyRecordIgnoreBO", description = "申请记录忽略请求对象")
public class AuthGradeApplyRecordIgnoreBO implements Serializable {

    private static final long serialVersionUID = -1439012482557686498L;

    @NotNull(message = "申请记录不能为空")
    @ApiModelProperty(value = "申请记录ID", required = true)
    private Long id;
}
