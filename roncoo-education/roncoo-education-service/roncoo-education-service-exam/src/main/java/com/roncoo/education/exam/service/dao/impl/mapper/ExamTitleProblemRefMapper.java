package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExamTitleProblemRefMapper {
    int countByExample(ExamTitleProblemRefExample example);

    int deleteByExample(ExamTitleProblemRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExamTitleProblemRef record);

    int insertSelective(ExamTitleProblemRef record);

    List<ExamTitleProblemRef> selectByExample(ExamTitleProblemRefExample example);

    ExamTitleProblemRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExamTitleProblemRef record, @Param("example") ExamTitleProblemRefExample example);

    int updateByExample(@Param("record") ExamTitleProblemRef record, @Param("example") ExamTitleProblemRefExample example);

    int updateByPrimaryKeySelective(ExamTitleProblemRef record);

    int updateByPrimaryKey(ExamTitleProblemRef record);
}