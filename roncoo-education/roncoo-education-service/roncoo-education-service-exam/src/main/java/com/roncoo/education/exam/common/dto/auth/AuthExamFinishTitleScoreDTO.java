package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 考试结束标题评分
 *
 * @author LYQ
 */
@Api(tags = {"考试结束标题评分"})
@Data
@Accessors(chain = true)
public class AuthExamFinishTitleScoreDTO implements Serializable {

    private static final long serialVersionUID = 7742675137492119735L;

    @ApiModelProperty(value = "标题ID")
    private Long titleId;

    @ApiModelProperty(value = "标题名称")
    private String titleName;

    @ApiModelProperty(value = "总分")
    private Integer totalScore;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "需要人工评分分值")
    private Integer needManualScore;
}
