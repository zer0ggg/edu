package com.roncoo.education.exam.common.bo.auth;

import com.roncoo.education.common.core.base.PageParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 试卷试题收藏
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class AuthExamProblemCollectionPageBO extends PageParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "收藏类型不能为空")
    @ApiModelProperty(value = "收藏类型(4:试卷,5:题目)", required = true)
    private Integer collectionType;

    @ApiModelProperty(value = "题目类型(1:单选题,2:多选题,3:判断题,4:填空题,5:简答题,6:组合题)")
    private Integer problemType;
}
