package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.UserExamAnswerEditREQ;
import com.roncoo.education.exam.common.req.UserExamAnswerListREQ;
import com.roncoo.education.exam.common.req.UserExamAnswerSaveREQ;
import com.roncoo.education.exam.common.resp.UserExamAnswerListRESP;
import com.roncoo.education.exam.common.resp.UserExamAnswerViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcUserExamAnswerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户考试答案 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-用户考试答案管理")
@RestController
@RequestMapping("/exam/pc/user/exam/answer")
public class PcUserExamAnswerController {

    @Autowired
    private PcUserExamAnswerBiz biz;

    @ApiOperation(value = "用户考试答案列表", notes = "用户考试答案列表")
    @PostMapping(value = "/list")
    public Result<Page<UserExamAnswerListRESP>> list(@RequestBody UserExamAnswerListREQ userExamAnswerListREQ) {
        return biz.list(userExamAnswerListREQ);
    }

    @ApiOperation(value = "用户考试答案添加", notes = "用户考试答案添加")
    @SysLog(value = "用户考试答案添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserExamAnswerSaveREQ userExamAnswerSaveREQ) {
        return biz.save(userExamAnswerSaveREQ);
    }

    @ApiOperation(value = "用户考试答案查看", notes = "用户考试答案查看")
    @GetMapping(value = "/view")
    public Result<UserExamAnswerViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户考试答案修改", notes = "用户考试答案修改")
    @SysLog(value = "用户考试答案修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserExamAnswerEditREQ userExamAnswerEditREQ) {
        return biz.edit(userExamAnswerEditREQ);
    }

    @ApiOperation(value = "用户考试答案删除", notes = "用户考试答案删除")
    @SysLog(value = "用户考试答案删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
