package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.FileInfoQO;
import com.roncoo.education.exam.feign.vo.FileInfoVO;
import com.roncoo.education.exam.service.dao.FileInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.FileInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件信息
 *
 * @author wujing
 */
@Component
public class FeignFileInfoBiz extends BaseBiz {

    @Autowired
    private FileInfoDao dao;

	public Page<FileInfoVO> listForPage(FileInfoQO qo) {
	    FileInfoExample example = new FileInfoExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<FileInfo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, FileInfoVO.class);
	}

	public int save(FileInfoQO qo) {
		FileInfo record = BeanUtil.copyProperties(qo, FileInfo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public FileInfoVO getById(Long id) {
		FileInfo record = dao.getById(id);
		return BeanUtil.copyProperties(record, FileInfoVO.class);
	}

	public int updateById(FileInfoQO qo) {
		FileInfo record = BeanUtil.copyProperties(qo, FileInfo.class);
		return dao.updateById(record);
	}

}
