package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefExample;

import java.util.List;

/**
 * 试卷标题题目关联 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamTitleProblemRefDao {

    /**
     * 保存试卷标题题目关联
     *
     * @param record 试卷标题题目关联
     * @return 影响记录数
     */
    int save(ExamTitleProblemRef record);

    /**
     * 根据ID删除试卷标题题目关联
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 试卷标题题目关联
     * @return 影响记录数
     */
    int updateById(ExamTitleProblemRef record);

    /**
     * 根据ID获取试卷标题题目关联
     *
     * @param id 主键ID
     * @return 试卷标题题目关联
     */
    ExamTitleProblemRef getById(Long id);

    /**
     * 试卷标题题目关联--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ExamTitleProblemRef> listForPage(int pageCurrent, int pageSize, ExamTitleProblemRefExample example);

    /**
     * 根据标题ID和试题ID获取试卷标题题目关联
     *
     * @param titleId   标题ID
     * @param problemId 题目ID
     * @return 试卷信息关联
     */
    ExamTitleProblemRef getByTitleIdAndProblemId(Long titleId, Long problemId);

    /**
     * 根据标题ID和试题ID获取试卷标题题目关联
     *
     * @param titleId         标题ID
     * @param problemParentId 题目父类ID
     * @param problemId       题目ID
     * @return 试卷信息关联
     */
    ExamTitleProblemRef getByTitleIdAndProblemParentIdAndProblemId(Long titleId, Long problemParentId, Long problemId);

    /**
     * 根据试卷ID和状态查找题目试卷信息关联列表
     *
     * @param titleId  标题ID
     * @param statusId 状态ID
     * @return 试卷信息关联
     */
    List<ExamTitleProblemRef> listByTitleIdAndStatusId(Long titleId, Integer statusId);

    /**
     * 根据讲师编号和状态获取试卷标题题目关联信息
     *
     * @param userNo   用户编号
     * @param statusId 状态ID
     * @return 标题题目关联
     */
    List<ExamTitleProblemRef> listByUserNoAndStatusId(Long userNo, Integer statusId);

    /**
     * 根据标题id和题目id获取试卷标题题目关联信息
     *
     * @param titleId   标题ID
     * @param problemId 题目ID
     * @return 标题题目关联
     */
    List<ExamTitleProblemRef> listByTitleIdOrProblemId(Long titleId, Long problemId);

    /**
     * 根据题目id删除
     *
     * @param titleId 标题ID
     * @return 影响记录数
     */
    int deleteByTitleId(Long titleId);

    /**
     * 根据试卷ID删除
     *
     * @param examId 试卷ID
     * @return 影响记录数
     */
    int deleteByExamId(Long examId);

    /**
     * 根据题目id和父试题id删除
     *
     * @param titleId 标题ID
     * @return 影响记录数
     */
    int deleteByTitleIdAndParentProblemId(Long titleId, Long parentProblemId);

    /**
     * 根据标题ID查找题目试卷信息关联列表
     *
     * @param titleId 标题ID
     * @return 标题题目关联
     */
    List<ExamTitleProblemRef> listByTitleId(Long titleId);

    /**
     * 根据标题ID和题目父类ID列出标题题目关联
     *
     * @param titleId         标题ID
     * @param problemParentId 题目父类ID
     * @return 标题题目关联
     */
    List<ExamTitleProblemRef> listByTitleIdAndProblemParentId(Long titleId, Long problemParentId);

    /**
     * 根据试卷ID、父题目id、状态查找题目试卷信息关联列表
     *
     * @param titleId  标题ID
     * @param statusId 状态ID
     * @return 试卷信息关联
     */
    List<ExamTitleProblemRef> listByTitleIdAndProblemParentIdAndStatusId(Long titleId, Long problemParentId, Integer statusId);

    /**
     * 根据试卷ID去重试题ID查找题目试卷信息关联列表
     *
     * @param examId
     * @return
     */
    List<Long> getByExamIdAndDuplicateProblemId(Long examId);
}
