package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswer;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeUserAnswerExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PracticeUserAnswerMapper {
    int countByExample(PracticeUserAnswerExample example);

    int deleteByExample(PracticeUserAnswerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PracticeUserAnswer record);

    int insertSelective(PracticeUserAnswer record);

    List<PracticeUserAnswer> selectByExampleWithBLOBs(PracticeUserAnswerExample example);

    List<PracticeUserAnswer> selectByExample(PracticeUserAnswerExample example);

    PracticeUserAnswer selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PracticeUserAnswer record, @Param("example") PracticeUserAnswerExample example);

    int updateByExampleWithBLOBs(@Param("record") PracticeUserAnswer record, @Param("example") PracticeUserAnswerExample example);

    int updateByExample(@Param("record") PracticeUserAnswer record, @Param("example") PracticeUserAnswerExample example);

    int updateByPrimaryKeySelective(PracticeUserAnswer record);

    int updateByPrimaryKeyWithBLOBs(PracticeUserAnswer record);

    int updateByPrimaryKey(PracticeUserAnswer record);
}