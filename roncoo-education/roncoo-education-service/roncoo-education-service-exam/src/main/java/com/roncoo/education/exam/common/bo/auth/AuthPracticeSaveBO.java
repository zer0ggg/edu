package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 练习信息保存
 * </p>
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeSaveBO ", description="练习信息保存")
public class AuthPracticeSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "科目id")
    private Long subjectId;

    @ApiModelProperty(value = "难度id")
    private Long difficultyId;

    @ApiModelProperty(value = "来源id")
    private Long sourceId;

    @ApiModelProperty(value = "年份id")
    private Long yearId;

    @ApiModelProperty(value = "题类id")
    private Long topicId;

    @ApiModelProperty(value = "年级id")
    private Long graId;

    @ApiModelProperty(value = "考点id")
    private Long emphasisId;

    @ApiModelProperty(value = "名称")
    private String name;
}
