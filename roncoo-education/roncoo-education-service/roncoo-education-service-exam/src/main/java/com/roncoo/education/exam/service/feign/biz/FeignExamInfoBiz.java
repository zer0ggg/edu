package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.ExamInfoQO;
import com.roncoo.education.exam.feign.vo.ExamInfoVO;
import com.roncoo.education.exam.service.dao.ExamInfoDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷信息
 *
 * @author wujing
 */
@Component
public class FeignExamInfoBiz extends BaseBiz {

    @Autowired
    private ExamInfoDao dao;

	public Page<ExamInfoVO> listForPage(ExamInfoQO qo) {
	    ExamInfoExample example = new ExamInfoExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ExamInfo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, ExamInfoVO.class);
	}

	public int save(ExamInfoQO qo) {
		ExamInfo record = BeanUtil.copyProperties(qo, ExamInfo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ExamInfoVO getById(Long id) {
		ExamInfo record = dao.getById(id);
		return BeanUtil.copyProperties(record, ExamInfoVO.class);
	}

	public int updateById(ExamInfoQO qo) {
		ExamInfo record = BeanUtil.copyProperties(qo, ExamInfo.class);
		return dao.updateById(record);
	}

	public ExamInfoVO getByIdAndStatusId(Long id,Integer statusId) {
		ExamInfo record = dao.getByIdAndStatusId(id,statusId);
		return BeanUtil.copyProperties(record, ExamInfoVO.class);
	}

}
