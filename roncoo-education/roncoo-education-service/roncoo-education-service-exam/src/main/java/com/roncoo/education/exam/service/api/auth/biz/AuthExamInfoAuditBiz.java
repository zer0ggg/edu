package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.*;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 试卷信息审核
 *
 * @author wujing
 */
@Component
public class AuthExamInfoAuditBiz extends BaseBiz {

    @Autowired
    private ExamInfoAuditDao dao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleAuditDao examTitleAuditDao;
    @Autowired
    private ExamTitleProblemRefAuditDao examTitleProblemRefAuditDao;
    @Autowired
    private ExamCategoryDao examCategoryDao;
    @Autowired
    private UserExamCategoryDao userExamCategoryDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    @Autowired
    private ExamCategoryDao categoryDao;
    @Autowired
    private IFeignLecturer feignLecturer;

    /**
     * 分页列表
     *
     * @param bo 试卷分页参数
     * @return 分页结果
     */
    public Result<Page<AuthExamInfoAuditDTO>> listForPage(AuthExamInfoAuditListBO bo) {
        ExamInfoAuditExample example = new ExamInfoAuditExample();
        ExamInfoAuditExample.Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());

        if (ObjectUtil.isNotNull(bo.getIsPublic())) {
            c.andIsPublicEqualTo(bo.getIsPublic());
        }
        if (ObjectUtil.isNotNull(bo.getIsPutaway())) {
            c.andIsPutawayEqualTo(bo.getIsPutaway());
        }
        if (ObjectUtil.isNotNull(bo.getIsFree())) {
            c.andIsFreeEqualTo(bo.getIsFree());
        }
        if (StrUtil.isNotBlank(bo.getExamName())) {
            c.andExamNameLike(PageUtil.like(bo.getExamName()));
        }
        if (ObjectUtil.isNotNull(bo.getAuditStatus())) {
            c.andAuditStatusEqualTo(bo.getAuditStatus());
        } else {
            c.andAuditStatusLessThan(Constants.FREEZE);
        }
        if (ObjectUtil.isNotNull(bo.getSubjectId())) {
            c.andSubjectIdEqualTo(bo.getSubjectId());
        }
        if (ObjectUtil.isNotNull(bo.getYearId())) {
            c.andYearIdEqualTo(bo.getYearId());
        }
        if (ObjectUtil.isNotNull(bo.getSourceId())) {
            c.andSourceIdEqualTo(bo.getSourceId());
        }

        example.setOrderByClause("exam_sort asc,audit_status asc,id desc");
        Page<ExamInfoAudit> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        if (page.getList().isEmpty() || page == null) {
            return Result.success(new Page<>());
        }
        Page<AuthExamInfoAuditDTO> dtoPage = PageUtil.transform(page, AuthExamInfoAuditDTO.class);
        // 获取讲师信息
        LecturerVO lecturer = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());

        List<Long> id = page.getList().stream().map(ExamInfoAudit::getId).collect(Collectors.toList());

        List<ExamInfo> examInfoList = examInfoDao.listByIds(id);
        Map<Long, Long> examInfoListMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(examInfoList)) {
            examInfoListMap = examInfoList.stream().collect(Collectors.toMap(ExamInfo::getId, item -> item.getId()));

        }
        //补充分类名称
        for (AuthExamInfoAuditDTO dto : dtoPage.getList()) {
            if (!ObjectUtil.isNull(lecturer)) {
                dto.setLecturerName(lecturer.getLecturerName());
            }

            if (examInfoListMap.get(dto.getId()) != null) {
                dto.setIsAudit(1);
            }

            if (dto.getSubjectId() != null && dto.getSubjectId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getSubjectId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setSubjectName(examCategory.getCategoryName());
                }
            }
            if (dto.getYearId() != null && dto.getYearId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getYearId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setYear(examCategory.getCategoryName());
                }
            }
            if (dto.getSourceId() != null && dto.getSourceId() != 0) {
                ExamCategory examCategory = examCategoryDao.getById(dto.getSourceId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setSourceView(examCategory.getCategoryName());
                }
            }
            if (dto.getPersonalId() != null && dto.getPersonalId() != 0) {
                UserExamCategory examCategory = userExamCategoryDao.getById(dto.getPersonalId());
                if (ObjectUtil.isNotNull(examCategory)) {
                    dto.setPersonalName(examCategory.getCategoryName());
                }
            }
        }
        return Result.success(dtoPage);
    }

    public Result<AuthExamInfoAuditDTO> view(AuthExamInfoAuditViewBO viewBO) {
        ExamInfoAudit record = dao.getById(viewBO.getId());
        if (!record.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("未查询到试卷信息");
        }
        return Result.success(BeanUtil.copyProperties(record, AuthExamInfoAuditDTO.class));
    }


    /**
     * 试卷上下架
     *
     * @param bo 上下架参数
     * @return 处理结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> isPutaway(AuthExamInfoAuditIsPutWayBO bo) {
        ExamInfoAudit examInfoAudit = dao.getById(bo.getId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("试卷不存在");
        }
        if (!examInfoAudit.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有操作权限");
        }

        ExamInfoAudit bean = new ExamInfoAudit();
        bean.setId(bo.getId());
        IsPutawayEnum isPutawayEnum = IsPutawayEnum.byCode(bo.getIsPutaway());
        if (ObjectUtil.isNull(isPutawayEnum)) {
            return Result.error("上下架不正确");
        }
        bean.setIsPutaway(isPutawayEnum.getCode());

        // 处于审核成功状态的记录，更改上下架状态 ，不影响审核状态，且同步上下架状态到实体表
        // 不处于审核成功状态，不允许更改上下架状态，返回提示
        ExamInfo examRecord = examInfoDao.getById(bo.getId());
        if (ObjectUtil.isNotNull(examRecord) && AuditStatusEnum.SUCCESS.getCode().equals(examInfoAudit.getAuditStatus())) {
            examRecord.setIsPutaway(bo.getIsPutaway());
            dao.updateById(bean);
            examInfoDao.updateById(examRecord);
            return Result.success("操作成功");
        } else {
            return Result.error("请先提交审核");
        }
    }

    public Result<String> update(AuthExamInfoAuditUpdateBO bo) {
        ExamInfoAudit examInfoAudit = dao.getById(bo.getId());
        if (!ThreadContext.userNo().equals(examInfoAudit.getLecturerUserNo())) {
            return Result.error("试卷不存在");
        }
        //校验分类id
        if (bo.getSubjectId() != null && bo.getSubjectId() != 0) {
            List<ExamCategory> categoryList = examCategoryDao.listByIdAndCategoryType(bo.getSubjectId(), ExamCategoryTypeEnum.SUBJECT.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                return Result.error("科目id不正确");
            }
        }
        if (bo.getYearId() != null && bo.getYearId() != 0) {
            List<ExamCategory> categoryList = examCategoryDao.listByIdAndCategoryType(bo.getYearId(), ExamCategoryTypeEnum.YEAR.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                return Result.error("年份id不正确");
            }
        }
        if (bo.getSourceId() != null && bo.getSourceId() != 0) {
            List<ExamCategory> categoryList = examCategoryDao.listByIdAndCategoryType(bo.getSourceId(), ExamCategoryTypeEnum.SOURCE.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                return Result.error("来源id不正确");
            }
        }

        //价格处理
        if (IsFreeEnum.FREE.getCode().equals(bo.getIsFree())) {
            // 免费就设置价格为0(原价、优惠价)
            bo.setOrgPrice(BigDecimal.ZERO);
            bo.setFabPrice(BigDecimal.ZERO);
        } else {
            // 收费但价格为空
            if (bo.getOrgPrice() == null) {
                return Result.error("请输入价格");
            }
            if (bo.getFabPrice() == null) {
                bo.setFabPrice(BigDecimal.ZERO);
            }
            // 原价小于0
            if (bo.getOrgPrice().compareTo(BigDecimal.ZERO) <= 0 || bo.getFabPrice().compareTo(BigDecimal.ZERO) <= 0) {
                return Result.error("价格不能必须大于0");
            }
            if (bo.getFabPrice().compareTo(bo.getOrgPrice()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }
        ExamInfoAudit newRecord = BeanUtil.copyProperties(bo, ExamInfoAudit.class);
        newRecord.setAuditOpinion("");
        newRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        newRecord.setGmtModified(new Date());
        int flag = dao.updateById(newRecord);
        if (flag > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");

    }

    public Result<String> save(AuthExamInfoAuditAddBO bo) {
        //检验是否是讲师
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("请先申请成为讲师");
        }

        ExamInfoAudit record = BeanUtil.copyProperties(bo, ExamInfoAudit.class);
        //检验是正确分类
        validCategory(record);
        //价格处理
        if (IsFreeEnum.FREE.getCode().equals(bo.getIsFree())) {
            // 免费就设置价格为0(原价、优惠价)
            bo.setOrgPrice(BigDecimal.ZERO);
            bo.setFabPrice(BigDecimal.ZERO);
        } else {
            // 收费但价格为空
            if (ObjectUtil.isNull(bo.getOrgPrice())) {
                return Result.error("请输入价格");
            }
            if (ObjectUtil.isNull(bo.getFabPrice())) {
                bo.setFabPrice(BigDecimal.ZERO);
            }
            // 原价小于0
            if (bo.getOrgPrice().compareTo(BigDecimal.ZERO) <= 0 || bo.getFabPrice().compareTo(BigDecimal.ZERO) <= 0) {
                return Result.error("价格必须大于0");
            }
            if (bo.getFabPrice().compareTo(bo.getOrgPrice()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }
        //补充数据
        record.setLecturerUserNo(lecturerVO.getLecturerUserNo());
        if (bo.getGraId() != null) {
            record.setGraId(bo.getGraId());
        }
        record.setId(IdWorker.getId());
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setIsPutaway(IsPutawayEnum.YES.getCode());
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        int flag = dao.save(record);
        if (flag > 0) {
            return Result.success(record.getId().toString());
        }
        return Result.error("操作失败");

    }

    /**
     * 校验试题分类id
     *
     * @param bo 试卷信息
     */
    private void validCategory(ExamInfoAudit bo) {
        //校验分类id
        if (bo.getSubjectId() != null && bo.getSubjectId() != 0) {
            List<ExamCategory> categoryList = categoryDao.listByIdAndCategoryType(bo.getSubjectId(), ExamCategoryTypeEnum.SUBJECT.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                throw new BaseException("科目id不正确");
            }
        }
        if (bo.getYearId() != null && bo.getYearId() != 0) {
            List<ExamCategory> categoryList = categoryDao.listByIdAndCategoryType(bo.getYearId(), ExamCategoryTypeEnum.YEAR.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                throw new BaseException("年份id不正确");
            }
        }
        if (bo.getSourceId() != null && bo.getSourceId() != 0) {
            List<ExamCategory> categoryList = categoryDao.listByIdAndCategoryType(bo.getSourceId(), ExamCategoryTypeEnum.SOURCE.getCode());
            if (CollectionUtils.isEmpty(categoryList)) {
                throw new BaseException("来源id不正确");
            }
        }
        if (bo.getPersonalId() != null && bo.getPersonalId() != 0) {
            UserExamCategory category = userExamCategoryDao.getById(bo.getPersonalId());
            if (ObjectUtils.isEmpty(category)) {
                throw new BaseException("用户分类id不正确");
            }
        }

    }

    /**
     * 试卷预览
     *
     * @param previewBO 预览参数
     * @return 试卷信息
     */
    public Result<AuthExamInfoAuditPreviewDTO> preview(AuthExamInfoAuditPreviewBO previewBO) {
        ExamInfoAudit examInfo = dao.getById(previewBO.getExamId());
        if (ObjectUtils.isEmpty(examInfo) || !examInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("未询到试卷信息");
        }
        AuthExamInfoAuditPreviewDTO previewDTO = BeanUtil.copyProperties(examInfo, AuthExamInfoAuditPreviewDTO.class);

        //查询大题集合
        List<ExamTitleAudit> titleList = examTitleAuditDao.listByExamIdAndAuditStatus(examInfo.getId(), null);
        if (CollectionUtil.isEmpty(titleList)) {
            return Result.success(previewDTO);
        }

        //遍历大题
        List<AuthExamTitleAuditPreviewDTO> titleAuditPreviewDTOList = new ArrayList<>();
        for (ExamTitleAudit examTitleAudit : titleList) {
            AuthExamTitleAuditPreviewDTO titleAuditPreviewDTO = BeanUtil.copyProperties(examTitleAudit, AuthExamTitleAuditPreviewDTO.class);

            // 获得大题下得试题关联集合
            List<ExamTitleProblemRefAudit> examTitleProblemRefAuditList = examTitleProblemRefAuditDao.listByTitleId(examTitleAudit.getId());
            if (CollectionUtil.isNotEmpty(examTitleProblemRefAuditList)) {
                //保留关联id和分数
                Map<Long, ExamTitleProblemRefAudit> problemRefMap = examTitleProblemRefAuditList.stream().collect(Collectors.toMap(ExamTitleProblemRefAudit::getProblemId, examTitleProblemRefAudit -> examTitleProblemRefAudit));
                List<Long> problemIdList = examTitleProblemRefAuditList.stream().map(ExamTitleProblemRefAudit::getProblemId).collect(Collectors.toList());
                List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
                if (CollectionUtil.isNotEmpty(problemList)) {
                    Map<Long, ExamProblem> problemListMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, item -> item));
                    List<AuthExamProblemDTO> problemDTOList = new ArrayList<>();
                    if (ProblemTypeEnum.MATERIAL.getCode().equals(examTitleAudit.getTitleType())) {
                        for (ExamTitleProblemRefAudit examTitleProblemRefAudit : examTitleProblemRefAuditList) {
                            ExamProblem problem = problemListMap.get(examTitleProblemRefAudit.getProblemId());
                            if (ObjectUtil.isNotNull(problem) && ExamProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
                                AuthExamProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamProblemDTO.class);
                                //补充标题试题关联主键
                                problemDTO.setRefId(examTitleProblemRefAudit.getId());
                                // 组合题获取关联小题
                                problemDTO.setChildrenList(listMaterialProblemSubProblem(problem.getId(), problemList, problemRefMap));
                                problemDTOList.add(problemDTO);
                            }
                        }
                    } else {
                        for (ExamTitleProblemRefAudit examTitleProblemRefAudit : examTitleProblemRefAuditList) {
                            ExamProblem problem = problemListMap.get(examTitleProblemRefAudit.getProblemId());
                            if (ObjectUtil.isNotNull(problem)) {
                                AuthExamProblemDTO problemDTO = BeanUtil.copyProperties(problem, AuthExamProblemDTO.class);
                                problemDTO.setRefId(examTitleProblemRefAudit.getId());
                                problemDTO.setScore(examTitleProblemRefAudit.getScore());
                                if (ProblemTypeEnum.THE_RADIO.getCode().equals(examTitleAudit.getTitleType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(examTitleAudit.getTitleType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(examTitleAudit.getTitleType())) {
                                    // 获取选项
                                    List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(problem.getId());
                                    problemDTO.setOptionList(BeanUtil.copyProperties(optionList, AuthExamProblemOptionDTO.class));
                                } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(examTitleAudit.getTitleType())) {
                                    // 填空题 获取填空数
                                    problemDTO.setOptionCount(StrUtil.isBlank(problem.getProblemAnswer()) ? 0 : problem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
                                }
                                problemDTOList.add(problemDTO);
                            }
                        }
                    }
                    titleAuditPreviewDTO.setProblemList(problemDTOList);
                }
            }
            titleAuditPreviewDTOList.add(titleAuditPreviewDTO);
        }
        previewDTO.setTitleList(titleAuditPreviewDTOList);
        return Result.success(previewDTO);
    }

    /**
     * 组卷操作
     *
     * @param bo 组卷参数
     * @return 组卷结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> saveExamRef(AuthExamInfoAuditSaveRefBO bo) {
        ExamInfoAudit examInfoAudit = dao.getById(bo.getId());
        if (!ThreadContext.userNo().equals(examInfoAudit.getLecturerUserNo())) {
            return Result.error("试卷不存在");
        }

        // 保存试卷试题
        if (CollectionUtil.isEmpty(bo.getTitleList())) {
            throw new BaseException("试卷标题不能为空");
        }
        //操作标题
        saveOrUpdateTitles(ThreadContext.userNo(), bo.getId(), bo.getTitleList());

        // 保存总题量
        examInfoAudit.setTotalScore(bo.getTotalScore());
        examInfoAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        dao.updateById(examInfoAudit);
        return Result.success("组卷成功");

    }

    /**
     * 组卷操作：保存/修改/删除标题
     *
     * @param userNo 用户编号
     * @param examId 试卷ID
     * @param titles 标题集合
     */
    private void saveOrUpdateTitles(Long userNo, Long examId, List<AuthExamTitleAuditSaveBO> titles) {
        // 传入标题集合(含有id的)
        List<AuthExamTitleAuditSaveBO> inTitleIdList = titles.stream().filter(x -> x.getId() != null).collect(Collectors.toList());
        // 已有的标题
        List<ExamTitleAudit> oldList = examTitleAuditDao.listByExamId(examId);
        Map<Long, ExamTitleAudit> oldRefMap = oldList.stream().collect(Collectors.toMap(ExamTitleAudit::getId, examTitleAudit -> examTitleAudit));

        //删除失效 标题
        if (CollectionUtil.isEmpty(inTitleIdList)) {
            //删除所有:传入标题没有id值，删除已有全部记录
            //删除 标题与标题试题关联
            List<Long> delIds = oldList.stream().map(ExamTitleAudit::getId).collect(Collectors.toList());
            deleteTitleAndTitleRef(delIds);
        } else {
            //删除失效部分
            //需要删除id =已有的与传入的差集
            List<Long> delIds = oldList.stream().map(ExamTitleAudit::getId).collect(Collectors.toList());
            delIds.removeAll(inTitleIdList.stream().map(AuthExamTitleAuditSaveBO::getId).collect(Collectors.toList()));
            //删除 标题与标题试题关联
            deleteTitleAndTitleRef(delIds);
        }


        //更新或新增标题
        for (AuthExamTitleAuditSaveBO titleAuditSaveBO : titles) {
            if (CollectionUtil.isEmpty(titleAuditSaveBO.getProblemList())) {
                throw new BaseException("试卷标题【" + titleAuditSaveBO.getTitleName() + "】下题目不能为空");
            }
            Long titleId = titleAuditSaveBO.getId();
            if (titleId != null) {
                //更新标题，不允许修改类型
                if (!titleAuditSaveBO.getTitleType().equals(oldRefMap.get(titleAuditSaveBO.getId()).getTitleType())) {
                    throw new BaseException("标题【" + titleAuditSaveBO.getTitleName() + "】更新，不可以修改类型");
                }
                ExamTitleAudit oldRecord = BeanUtil.copyProperties(titleAuditSaveBO, ExamTitleAudit.class);
                oldRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                examTitleAuditDao.updateById(oldRecord);
            } else {
                //新增标题
                ExamTitleAudit newRecord = BeanUtil.copyProperties(titleAuditSaveBO, ExamTitleAudit.class);
                newRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                newRecord.setStatusId(StatusIdEnum.YES.getCode());
                newRecord.setLecturerUserNo(userNo);
                newRecord.setExamId(examId);
                examTitleAuditDao.save(newRecord);
                //将id塞回
                titleId = newRecord.getId();
            }

            //操作试题关联
            saveOrUpdateProblemRefList(userNo, examId, titleId, titleAuditSaveBO.getProblemList());

        }
    }


    /**
     * 组卷操作：标题试题关联
     *
     * @param userNo      用户编号
     * @param examId      试卷ID
     * @param titleId     标题ID
     * @param problemList 题目集合
     */
    private void saveOrUpdateProblemRefList(Long userNo, Long examId, Long titleId, List<AuthExamTitleProblemRefAuditSaveBO> problemList) {

        //删除 试题关联ids
        //查出试题集合（排除子一级）
        List<ExamTitleProblemRefAudit> oldRefList = examTitleProblemRefAuditDao.listByTitleIdAndProblemParentId(titleId, 0L);
        if (!CollectionUtil.isEmpty(oldRefList)) {
            Map<Long, ExamTitleProblemRefAudit> oldRefMap = oldRefList.stream().collect(Collectors.toMap(ExamTitleProblemRefAudit::getId, examTitleProblemRefAudit -> examTitleProblemRefAudit));
            // 删除 试题关联ids = = 已有的id与传入id的差集
            List<Long> delIds = oldRefList.stream().map(ExamTitleProblemRefAudit::getId).collect(Collectors.toList());
            delIds.removeAll(problemList.stream().filter(x -> x.getRefId() != null).map(AuthExamTitleProblemRefAuditSaveBO::getRefId).collect(Collectors.toList()));

            //删除失效 试题关联
            deleteProblemRef(delIds, oldRefMap);
        }

        // 更新或保存试题关联
        for (AuthExamTitleProblemRefAuditSaveBO refAuditSaveBO : problemList) {
            ExamProblem problem = examProblemDao.getById(refAuditSaveBO.getProblemId());
            if (ObjectUtil.isEmpty(problem)) {
                logger.info("试卷试题id【" + refAuditSaveBO.getProblemId() + "】不正确");
                throw new BaseException("组卷异常，请联系管理员");
            }
            if (refAuditSaveBO.getRefId() == null) {
                //新增试题关联
                ExamTitleProblemRefAudit newRecord = BeanUtil.copyProperties(refAuditSaveBO, ExamTitleProblemRefAudit.class);
                newRecord.setLecturerUserNo(userNo);
                newRecord.setExamId(examId);
                newRecord.setTitleId(titleId);
                newRecord.setProblemType(problem.getProblemType());
                newRecord.setStatusId(StatusIdEnum.YES.getCode());
                newRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                examTitleProblemRefAuditDao.save(newRecord);

            } else {
                //更新试题关联：只更新分值与排序
                ExamTitleProblemRefAudit updateRecord = examTitleProblemRefAuditDao.getById(refAuditSaveBO.getRefId());
                if (ObjectUtil.isEmpty(updateRecord)) {
                    logger.info("试卷标题试题关联id【" + refAuditSaveBO.getRefId() + "】不正确");
                    throw new BaseException("组卷异常，请联系管理员");
                }
                updateRecord.setScore(refAuditSaveBO.getScore());
                updateRecord.setSort(refAuditSaveBO.getSort());
                updateRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                examTitleProblemRefAuditDao.updateById(updateRecord);

            }

            //组合题，处理子一级 试题关联
            if (ProblemTypeEnum.MATERIAL.getCode().equals(problem.getProblemType())) {
                // 原子试题 关联id
                List<ExamTitleProblemRefAudit> oldSubRefList = examTitleProblemRefAuditDao.listByTitleIdAndProblemParentId(titleId, problem.getId());
                Map<Long, ExamTitleProblemRefAudit> oldRefMap1 = oldSubRefList.stream().collect(Collectors.toMap(ExamTitleProblemRefAudit::getId, ExamTitleProblemRefAudit -> ExamTitleProblemRefAudit));

                //删除 不存在的关联关系（组合题删除部分子试题）= 已存在关联id与子试题id的差集
                List<Long> delRefIds = oldSubRefList.stream().map(ExamTitleProblemRefAudit::getId).collect(Collectors.toList());
                delRefIds.removeAll(refAuditSaveBO.getChildrenList().stream().filter(x -> x.getRefId() != null).map(AuthExamTitleProblemRefAuditSaveBO::getRefId).collect(Collectors.toList()));

                // 删除失效的子一级试题
                deleteProblemRef(delRefIds, oldRefMap1);

                // 组合题子试题
                for (AuthExamTitleProblemRefAuditSaveBO subRefSaveBO : refAuditSaveBO.getChildrenList()) {
                    ExamProblem subProblem = examProblemDao.getById(subRefSaveBO.getProblemId());
                    if (ObjectUtil.isEmpty(subProblem)) {
                        logger.info("试题id【" + subRefSaveBO.getProblemId() + "】不正确");
                        throw new BaseException("组卷异常，请联系管理员");
                    }
                    //更新/保存操作
                    if (subRefSaveBO.getRefId() == null) {
                        //新增试题关联
                        ExamTitleProblemRefAudit newSubRecord = BeanUtil.copyProperties(subRefSaveBO, ExamTitleProblemRefAudit.class);
                        newSubRecord.setLecturerUserNo(userNo);
                        newSubRecord.setExamId(examId);
                        newSubRecord.setTitleId(titleId);
                        newSubRecord.setProblemParentId(refAuditSaveBO.getProblemId());
                        newSubRecord.setProblemType(subProblem.getProblemType());
                        newSubRecord.setStatusId(StatusIdEnum.YES.getCode());
                        newSubRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                        examTitleProblemRefAuditDao.save(newSubRecord);
                    } else {
                        //更新试题关联：只更新分值与排序
                        ExamTitleProblemRefAudit updateSubRecord = examTitleProblemRefAuditDao.getById(subRefSaveBO.getRefId());
                        if (ObjectUtil.isEmpty(updateSubRecord)) {
                            logger.info("关联id【" + subRefSaveBO.getRefId() + "】不正确");
                            throw new BaseException("组卷异常，请联系管理员");
                        }
                        updateSubRecord.setScore(subRefSaveBO.getScore());
                        updateSubRecord.setSort(subRefSaveBO.getSort());
                        updateSubRecord.setAuditStatus(AuditStatusEnum.WAIT.getCode());
                        examTitleProblemRefAuditDao.updateById(updateSubRecord);
                    }
                }
            }
        }
    }

    /**
     * 组卷操作：删除失效标题及其标题下的标题试题关联
     *
     * @param titleIdList 需要删除的标题ID集合
     */
    private void deleteTitleAndTitleRef(List<Long> titleIdList) {
        for (Long titleId : titleIdList) {
            //删除标题
            examTitleAuditDao.deleteById(titleId);
            //删除标题试题关联
            examTitleProblemRefAuditDao.deleteByTitleId(titleId);
        }
    }

    /**
     * 删除 标题试题关联，如果是需要删除父一级试题，同时删除所有子一级试题
     *
     * @param delIds    待删除试题的关联id
     * @param oldRefMap 试题集合
     */
    private void deleteProblemRef(List<Long> delIds, Map<Long, ExamTitleProblemRefAudit> oldRefMap) {
        for (Long refId : delIds) {
            ExamTitleProblemRefAudit oldRecord = oldRefMap.get(refId);
            //删除本级
            examTitleProblemRefAuditDao.deleteById(refId);

            //删除下一级
            if (ProblemTypeEnum.MATERIAL.getCode().equals(oldRecord.getProblemType())) {
                ExamTitleProblemRefAuditExample subExample = new ExamTitleProblemRefAuditExample();
                ExamTitleProblemRefAuditExample.Criteria c = subExample.createCriteria();
                c.andTitleIdEqualTo(oldRecord.getTitleId());
                c.andProblemParentIdEqualTo(oldRecord.getProblemId());
                examTitleProblemRefAuditDao.deleteByTitleIdAndParentProblemId(oldRecord.getTitleId(), oldRecord.getProblemId());
            }
        }


    }

    /**
     * @param materialProblemId 组合题ID
     * @param problemList       题目集合
     * @param problemRefMap     试题id-标题试题关联 对应map
     * @return 试卷题目集合
     */
    private List<AuthExamProblemDTO> listMaterialProblemSubProblem(Long materialProblemId, List<ExamProblem> problemList, Map<Long, ExamTitleProblemRefAudit> problemRefMap) {
        List<ExamProblem> subProblemList = problemList.stream().filter(problem -> materialProblemId.equals(problem.getParentId())).collect(Collectors.toList());
        List<AuthExamProblemDTO> subProblemDTOList = new ArrayList<>();
        for (ExamProblem subProblem : subProblemList) {
            AuthExamProblemDTO subProblemDTO = BeanUtil.copyProperties(subProblem, AuthExamProblemDTO.class);
            //补充标题试题关联主键
            subProblemDTO.setRefId(problemRefMap.get(subProblemDTO.getId()).getId());
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(subProblem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(subProblem.getProblemType())) {
                // 获取选项
                List<ExamProblemOption> subProblemOptionList = examProblemOptionDao.listByProblemIdWithBLOBs(subProblem.getId());
                subProblemDTO.setOptionList(BeanUtil.copyProperties(subProblemOptionList, AuthExamProblemOptionDTO.class));
            } else if (ProblemTypeEnum.GAP_FILLING.getCode().equals(subProblem.getProblemType())) {
                subProblemDTO.setOptionCount(StrUtil.isBlank(subProblem.getProblemAnswer()) ? 0 : subProblem.getProblemAnswer().split(Constants.EXAM.PROBLEM_ANSWER_SEPARATION).length);
            }

            subProblemDTOList.add(subProblemDTO);
        }
        return subProblemDTOList;
    }

    public Result<String> submitAudit(AuthExamInfoAuditIsSubmitAuditBO bo) {
        ExamInfoAudit examInfoAudit = dao.getById(bo.getId());
        if (ObjectUtil.isNull(examInfoAudit)) {
            return Result.error("试卷不存在");
        }
        if (!examInfoAudit.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有操作权限");
        }
        IsSubmitAuditEnum isSubmitAuditEnum = IsSubmitAuditEnum.byCode(bo.getSubmitAudit());
        if (ObjectUtil.isNull(isSubmitAuditEnum)) {
            return Result.error("提交审核状态不正确");
        }
        ExamInfoAudit bean = new ExamInfoAudit();
        bean.setId(bo.getId());
        bean.setSubmitAudit(bo.getSubmitAudit());
        // 进入待审核
        bean.setAuditOpinion("");
        bean.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (dao.updateById(bean) > 0) {
            return Result.success("操作成功");
        }
        return Result.error("操作失败");
    }
}
