package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeSaveProblemAnswerBO;
import com.roncoo.education.exam.service.api.auth.biz.AuthPracticeUserAnswerBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户练习答案 UserApi接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-AUTH-用户练习答案")
@RestController
@RequestMapping("/exam/auth/practice/user/answer")
public class AuthPracticeUserAnswerController {

    @Autowired
    private AuthPracticeUserAnswerBiz biz;

    @ApiOperation(value = "保存练习答案", notes = "保存练习答案")
    @PostMapping(value = "/save")
    public Result<String> savePracticeAnswer(@RequestBody @Valid AuthPracticeSaveProblemAnswerBO bo) {
        return biz.savePracticeAnswer(bo);
    }

}
