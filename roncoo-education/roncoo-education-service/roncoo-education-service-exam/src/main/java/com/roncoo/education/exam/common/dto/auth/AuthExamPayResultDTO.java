package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 考试支付返回结果
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "AuthExamPayResultDTO", description = "考试支付返回结果")
public class AuthExamPayResultDTO implements Serializable {

    private static final long serialVersionUID = -2949878526984939294L;

    @ApiModelProperty(value = "订单号", required = true)
    private Long orderNo;

    @ApiModelProperty(value = "支付信息", required = true)
    private String payMessage;

    @ApiModelProperty(value = "试卷名称", required = true)
    private String examName;

    @ApiModelProperty(value = "支付价格", required = true)
    private BigDecimal price;

    @ApiModelProperty(value = "支付方式", required = true)
    private Integer payType;
}
