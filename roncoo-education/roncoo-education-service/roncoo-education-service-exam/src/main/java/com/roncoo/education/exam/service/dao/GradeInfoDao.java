package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfoExample;

import java.util.List;

/**
 * 服务类
 *
 * @author wujing
 * @date 2020-06-10
 */
public interface GradeInfoDao {

    /**
     * 保存
     *
     * @param record
     * @return 影响记录数
     */
    int save(GradeInfo record);

    /**
     * 根据ID删除
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record
     * @return 影响记录数
     */
    int updateById(GradeInfo record);

    /**
     * 根据ID获取
     *
     * @param id 主键ID
     * @return
     */
    GradeInfo getById(Long id);

    /**
     * --分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<GradeInfo> listForPage(int pageCurrent, int pageSize, GradeInfoExample example);

    /**
     * 根据讲师用户编号获取班级信息
     *
     * @param lecturerUserNo 讲师用户编号
     * @return 班级信息
     */
    List<GradeInfo> listByLecturerUserNo(Long lecturerUserNo);

    /**
     * 根据邀请码获取班级信息
     *
     * @param invitationCode 邀请码
     * @return 班级信息
     */
    GradeInfo getByInvitationCode(String invitationCode);

    /**
     * 根据ID集合获取班级信息
     *
     * @param gradeIds
     * @return
     */
    List<GradeInfo> lsitByIds(List<Long> gradeIds);
}
