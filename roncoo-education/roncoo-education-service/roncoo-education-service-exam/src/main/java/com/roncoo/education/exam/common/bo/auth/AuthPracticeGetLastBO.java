package com.roncoo.education.exam.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 练习信息获取
 * </p>
 *
 * @author LHR
 * @date 2020-10-10
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AuthPracticeGetLastBO ", description="练习信息获取")
public class AuthPracticeGetLastBO implements Serializable {

    private static final long serialVersionUID = 1L;

}
