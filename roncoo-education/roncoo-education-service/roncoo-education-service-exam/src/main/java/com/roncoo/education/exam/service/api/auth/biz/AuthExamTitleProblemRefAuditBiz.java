package com.roncoo.education.exam.service.api.auth.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefAuditDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联审核
 *
 * @author wujing
 */
@Component
public class AuthExamTitleProblemRefAuditBiz extends BaseBiz {

    @Autowired
    private ExamTitleProblemRefAuditDao dao;

}
