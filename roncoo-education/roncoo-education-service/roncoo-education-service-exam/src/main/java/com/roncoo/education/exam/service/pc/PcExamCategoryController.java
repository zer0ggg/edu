package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.*;
import com.roncoo.education.exam.common.resp.ExamCategoryListEditRESP;
import com.roncoo.education.exam.common.resp.ExamCategoryRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 试卷分类 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷分类管理")
@RestController
@RequestMapping("/exam/pc/exam/category")
public class PcExamCategoryController {

    @Autowired
    private PcExamCategoryBiz biz;

    @ApiOperation(value = "试卷分类列表", notes = "试卷分类列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamCategoryRESP>> list(@RequestBody ExamCategoryPageREQ examCategoryPageREQ) {
        return biz.list(examCategoryPageREQ);
    }


    @ApiOperation(value = "试卷分类添加", notes = "试卷分类添加")
    @SysLog(value = "试卷分类添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ExamCategorySaveREQ examCategorySaveREQ) {
        return biz.save(examCategorySaveREQ);
    }

    @ApiOperation(value = "试卷分类查看", notes = "试卷分类查看")
    @GetMapping(value = "/view")
    public Result<ExamCategoryRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "试卷分类修改", notes = "试卷分类修改")
    @SysLog(value = "试卷分类修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamCategoryEditREQ examCategoryEditREQ) {
        return biz.edit(examCategoryEditREQ);
    }


    @ApiOperation(value = "试卷分类删除", notes = "试卷分类删除")
    @SysLog(value = "试卷分类删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "修改状态", notes = "修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ExamCategoryUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }

    @ApiOperation(value = "分类列表接口(试卷、试题修改使用)", notes = "分类列表接口(试卷、试题修改使用)")
    @PostMapping(value = "/list/edit")
    public Result<ExamCategoryListEditRESP> listEdit(@RequestBody ExamCategoryListEditREQ examCategoryListEditREQ) {
        return biz.listEdit(examCategoryListEditREQ);
    }
}
