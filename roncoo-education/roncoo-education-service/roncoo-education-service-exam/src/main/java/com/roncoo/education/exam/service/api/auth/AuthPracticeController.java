package com.roncoo.education.exam.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeGetLastBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeSaveBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeVideoSignBO;
import com.roncoo.education.exam.common.bo.auth.AuthPracticeViewBO;
import com.roncoo.education.exam.common.dto.auth.AuthPracticeListForPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthPracticeVideoSignDTO;
import com.roncoo.education.exam.common.dto.auth.AuthPracticeViewDTO;
import com.roncoo.education.exam.service.api.auth.biz.AuthPracticeBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 练习信息 UserApi接口
 *
 * @author LHR
 * @date 2020-10-10
 */
@Api(tags = "API-AUTH-练习信息")
@RestController
@RequestMapping("/exam/auth/practice")
public class AuthPracticeController {

    @Autowired
    private AuthPracticeBiz biz;

    @ApiOperation(value = "保存练习接口", notes = "保存练习接口")
    @PostMapping(value = "/save")
    public Result<Long> save(@RequestBody @Valid AuthPracticeSaveBO bo) {
        return biz.save(bo);
    }

    @ApiOperation(value = "获取用户上一次练习记录", notes = "获取用户上一次练习记录")
    @PostMapping(value = "/get/last")
    public Result<Long> getLast(@RequestBody @Valid AuthPracticeGetLastBO bo) {
        return biz.getLast(bo);
    }

    @ApiOperation(value = "分页获取用户练习记录", notes = "分页获取用户练习记录")
    @PostMapping(value = "/listForPage")
    public Result<Page<AuthPracticeListForPageDTO>> listForPage(@RequestBody @Valid AuthPracticeListForPageDTO bo) {
        return biz.listForPage(bo);
    }

    @ApiOperation(value = "获取练习详情信息", notes = "获取练习详情信息")
    @PostMapping(value = "/view")
    public Result<AuthPracticeViewDTO> view(@RequestBody @Valid AuthPracticeViewBO bo) {
        return biz.view(bo);
    }

    @ApiOperation(value = "删除练习", notes = "删除练习")
    @RequestMapping(value = "/delete/{userNo}/{id}")
    public Result<Integer> delete(@PathVariable(value = "userNo") String userNo, @PathVariable(value = "id") String id) {
        return biz.delete(Long.valueOf(userNo), Long.valueOf(id));
    }

    @ApiOperation(value = "播放获取sign值接口", notes = "播放获取sign值接口")
    @PostMapping(value = "/sign")
    public Result<AuthPracticeVideoSignDTO> sign(@RequestBody @Valid AuthPracticeVideoSignBO bo) {
        return biz.sign(bo);
    }
}
