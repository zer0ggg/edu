package com.roncoo.education.exam.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.exam.common.req.ExamProblemEditREQ;
import com.roncoo.education.exam.common.req.ExamProblemListREQ;
import com.roncoo.education.exam.common.req.ExamProblemUpdateStatusREQ;
import com.roncoo.education.exam.common.resp.ExamProblemListRESP;
import com.roncoo.education.exam.common.resp.ExamProblemViewRESP;
import com.roncoo.education.exam.service.pc.biz.PcExamProblemBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 试卷题目 Pc接口
 *
 * @author wujing
 */
@Api(tags = "PC-试卷题目管理")
@RestController
@RequestMapping("/exam/pc/exam/problem")
public class PcExamProblemController {

    @Autowired
    private PcExamProblemBiz biz;

    @ApiOperation(value = "试卷题目列表", notes = "试卷题目列表")
    @PostMapping(value = "/list")
    public Result<Page<ExamProblemListRESP>> list(@RequestBody ExamProblemListREQ req) {
        return biz.list(req);
    }

    @ApiOperation(value = "试卷题目查看", notes = "试卷题目查看")
    @GetMapping(value = "/view")
    public Result<ExamProblemViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "试卷题目修改", notes = "试卷题目修改")
    @SysLog(value = "试卷题目修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ExamProblemEditREQ examProblemEditREQ) {
        return biz.edit(examProblemEditREQ);
    }

    @ApiOperation(value = "试卷题目删除", notes = "试卷题目删除")
    @SysLog(value = "试卷题目删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    @ApiOperation(value = "修改状态", notes = "修改状态")
    @SysLog(value = "试卷题目修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ExamProblemUpdateStatusREQ req) {
        return biz.updateStatus(req);
    }

    @ApiOperation(value = "导入试题", notes = "导入试题")
    @SysLog(value = "导入试题")
    @PostMapping(value = "/upload/excel")
    public Result<String> uploadExcel(@RequestParam("file") MultipartFile file) {
        return biz.uploadExcel(file);
    }

}
