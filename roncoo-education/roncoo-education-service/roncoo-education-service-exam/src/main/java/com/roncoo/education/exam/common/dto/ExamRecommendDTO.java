package com.roncoo.education.exam.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 试卷推荐
 * </p>
 *
 * @author wujing
 * @date 2020-06-03
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamRecommendDTO", description="试卷推荐")
public class ExamRecommendDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:有效;0:无效)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "试卷ID")
    private Long examId;

    //补充参数
    @ApiModelProperty(value = "学习人数")
    private Integer studyCount;
}
