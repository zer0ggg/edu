package com.roncoo.education.exam.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.feign.qo.PracticeProblemRefQO;
import com.roncoo.education.exam.feign.vo.PracticeProblemRefVO;
import com.roncoo.education.exam.service.dao.PracticeProblemRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeProblemRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 题目练习关联
 *
 * @author LHR
 */
@Component
public class FeignPracticeProblemRefBiz extends BaseBiz {

    @Autowired
    private PracticeProblemRefDao dao;

	public Page<PracticeProblemRefVO> listForPage(PracticeProblemRefQO qo) {
	    PracticeProblemRefExample example = new PracticeProblemRefExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<PracticeProblemRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, PracticeProblemRefVO.class);
	}

	public int save(PracticeProblemRefQO qo) {
		PracticeProblemRef record = BeanUtil.copyProperties(qo, PracticeProblemRef.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public PracticeProblemRefVO getById(Long id) {
		PracticeProblemRef record = dao.getById(id);
		return BeanUtil.copyProperties(record, PracticeProblemRefVO.class);
	}

	public int updateById(PracticeProblemRefQO qo) {
		PracticeProblemRef record = BeanUtil.copyProperties(qo, PracticeProblemRef.class);
		return dao.updateById(record);
	}

}
