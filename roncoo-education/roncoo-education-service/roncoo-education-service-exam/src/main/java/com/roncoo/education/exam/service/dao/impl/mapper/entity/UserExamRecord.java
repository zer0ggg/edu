package com.roncoo.education.exam.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class UserExamRecord implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long userNo;

    private Long examId;

    private Integer answerTime;

    private Date endTime;

    private Integer totalScore;

    private Integer score;

    private Integer sysAuditTotalScore;

    private Integer sysAuditScore;

    private Integer handworkAuditTotalScore;

    private Integer handworkAuditScore;

    private Integer examStatus;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Integer getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Integer answerTime) {
        this.answerTime = answerTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getSysAuditTotalScore() {
        return sysAuditTotalScore;
    }

    public void setSysAuditTotalScore(Integer sysAuditTotalScore) {
        this.sysAuditTotalScore = sysAuditTotalScore;
    }

    public Integer getSysAuditScore() {
        return sysAuditScore;
    }

    public void setSysAuditScore(Integer sysAuditScore) {
        this.sysAuditScore = sysAuditScore;
    }

    public Integer getHandworkAuditTotalScore() {
        return handworkAuditTotalScore;
    }

    public void setHandworkAuditTotalScore(Integer handworkAuditTotalScore) {
        this.handworkAuditTotalScore = handworkAuditTotalScore;
    }

    public Integer getHandworkAuditScore() {
        return handworkAuditScore;
    }

    public void setHandworkAuditScore(Integer handworkAuditScore) {
        this.handworkAuditScore = handworkAuditScore;
    }

    public Integer getExamStatus() {
        return examStatus;
    }

    public void setExamStatus(Integer examStatus) {
        this.examStatus = examStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", userNo=").append(userNo);
        sb.append(", examId=").append(examId);
        sb.append(", answerTime=").append(answerTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", totalScore=").append(totalScore);
        sb.append(", score=").append(score);
        sb.append(", sysAuditTotalScore=").append(sysAuditTotalScore);
        sb.append(", sysAuditScore=").append(sysAuditScore);
        sb.append(", handworkAuditTotalScore=").append(handworkAuditTotalScore);
        sb.append(", handworkAuditScore=").append(handworkAuditScore);
        sb.append(", examStatus=").append(examStatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}