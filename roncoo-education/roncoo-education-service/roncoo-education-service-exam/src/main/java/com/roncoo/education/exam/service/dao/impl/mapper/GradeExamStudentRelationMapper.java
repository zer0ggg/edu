package com.roncoo.education.exam.service.dao.impl.mapper;

import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelation;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeExamStudentRelationExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GradeExamStudentRelationMapper {
    int countByExample(GradeExamStudentRelationExample example);

    int deleteByExample(GradeExamStudentRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(GradeExamStudentRelation record);

    int insertSelective(GradeExamStudentRelation record);

    List<GradeExamStudentRelation> selectByExample(GradeExamStudentRelationExample example);

    GradeExamStudentRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") GradeExamStudentRelation record, @Param("example") GradeExamStudentRelationExample example);

    int updateByExample(@Param("record") GradeExamStudentRelation record, @Param("example") GradeExamStudentRelationExample example);

    int updateByPrimaryKeySelective(GradeExamStudentRelation record);

    int updateByPrimaryKey(GradeExamStudentRelation record);
}
