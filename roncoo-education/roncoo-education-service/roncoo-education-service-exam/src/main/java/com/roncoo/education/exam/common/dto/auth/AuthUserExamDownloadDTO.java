package com.roncoo.education.exam.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户试卷下载
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserExamDownloadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long Id;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;
    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    private Long userNoCopy;
    /**
     * 试题ID
     */
    @ApiModelProperty(value = "试题ID")
    private Long subjectId;
    /**
     * 文件格式（1：DOC; 2: DOCX）
     */
    @ApiModelProperty(value = "文件格式（1：DOC; 2: DOCX）")
    private Integer fileFormat;
    /**
     * 下载类型（1：普通试卷题干带参考答案 ；2：考试试卷只有题干，不带参考答案）
     */
    @ApiModelProperty(value = "下载类型（1：普通试卷题干带参考答案 ；2：考试试卷只有题干，不带参考答案）")
    private Integer downloadType;

}
