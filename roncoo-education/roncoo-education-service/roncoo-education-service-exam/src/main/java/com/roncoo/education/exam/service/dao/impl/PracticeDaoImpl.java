package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.PracticeDao;
import com.roncoo.education.exam.service.dao.impl.mapper.PracticeMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.Practice;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.PracticeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 练习信息 服务实现类
 *
 * @author LHR
 * @date 2020-10-10
 */
@Repository
public class PracticeDaoImpl implements PracticeDao {

    @Autowired
    private PracticeMapper mapper;

    @Override
    public int save(Practice record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Practice record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Practice getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<Practice> listForPage(int pageCurrent, int pageSize, PracticeExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public Practice getLastByUserNo(Long userNo) {
        PracticeExample example = new PracticeExample();
        PracticeExample.Criteria criteria = example.createCriteria();
        criteria.andUserNoEqualTo(userNo);
        example.setOrderByClause(" id desc");
        List<Practice> list = this.mapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

}
