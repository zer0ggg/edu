package com.roncoo.education.exam.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblem;
import com.roncoo.education.exam.feign.qo.ExamProblemQO;
import com.roncoo.education.exam.feign.vo.ExamProblemVO;
import com.roncoo.education.exam.service.feign.biz.FeignExamProblemBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 试卷题目
 *
 * @author wujing
 * @date 2020-06-03
 */
@RestController
public class FeignExamProblemController extends BaseController implements IFeignExamProblem{

    @Autowired
    private FeignExamProblemBiz biz;

	@Override
	public Page<ExamProblemVO> listForPage(@RequestBody ExamProblemQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ExamProblemQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ExamProblemQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public ExamProblemVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}

	@Override
	public int updateByVideoId(@RequestBody ExamProblemQO examProblemQO) {
		return biz.updateByVideoId(examProblemQO);
	}

	@Override
	public int updateByVid(ExamProblemQO examProblemQO) {
		return biz.updateByVid(examProblemQO);
	}
}
