package com.roncoo.education.exam.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.exam.common.bo.auth.*;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordPageDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordSummaryDTO;
import com.roncoo.education.exam.common.dto.auth.AuthGradeApplyRecordTipsListDTO;
import com.roncoo.education.exam.service.dao.GradeApplyRecordDao;
import com.roncoo.education.exam.service.dao.GradeInfoDao;
import com.roncoo.education.exam.service.dao.GradeStudentDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecord;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeApplyRecordExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeInfo;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.GradeStudent;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignMsg;
import com.roncoo.education.user.feign.interfaces.IFeignUser;
import com.roncoo.education.user.feign.interfaces.IFeignUserMsg;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.qo.UserMsgAndMsgSaveQO;
import com.roncoo.education.user.feign.qo.UserMsgQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wujing
 */
@Component
public class AuthGradeApplyRecordBiz extends BaseBiz {

    @Autowired
    private GradeApplyRecordDao dao;
    @Autowired
    private GradeInfoDao gradeInfoDao;
    @Autowired
    private GradeStudentDao gradeStudentDao;

    @Autowired
    private IFeignUser feignUser;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignMsg feignMsg;
    @Autowired
    private IFeignUserMsg feignUserMsg;

    /**
     * 分页列出
     *
     * @param bo 分页查询
     * @return 分页结果
     */
    public Result<Page<AuthGradeApplyRecordPageDTO>> listForPage(AuthGradeApplyRecordPageBO bo) {
        List<Long> gradeIdList = new ArrayList<>();
        if (ObjectUtil.isNotNull(bo.getGradeId())) {
            GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
            if (ObjectUtil.isNull(gradeInfo)) {
                return Result.success(new Page<>());
            }
            if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
                GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
                if (ObjectUtil.isNull(gradeStudent) || !GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                    return Result.success(new Page<>());
                }
            }
            gradeIdList.add(bo.getGradeId());
        } else {
            List<GradeStudent> gradeStudentList = gradeStudentDao.listByUserNoAndUserRole(ThreadContext.userNo(), GradeStudentUserRoleEnum.ADMIN.getCode());

            if (CollectionUtil.isNotEmpty(gradeStudentList)) {
                gradeIdList.addAll(gradeStudentList.stream().map(GradeStudent::getGradeId).collect(Collectors.toList()));
            }

            List<GradeInfo> gradeInfoList = gradeInfoDao.listByLecturerUserNo(ThreadContext.userNo());
            if (CollectionUtil.isNotEmpty(gradeInfoList)) {
                gradeIdList.addAll(gradeInfoList.stream().map(GradeInfo::getId).collect(Collectors.toList()));
            }
        }

        GradeApplyRecordExample example = new GradeApplyRecordExample();
        GradeApplyRecordExample.Criteria c = example.createCriteria();
        c.andGradeIdIn(gradeIdList);
        if (ObjectUtil.isNotNull(bo.getAuditStatus())) {
            c.andAuditStatusEqualTo(bo.getAuditStatus());
        }
        example.setOrderByClause("sort asc, id desc");

        Page<GradeApplyRecord> recordPage = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthGradeApplyRecordPageDTO> resultPage = PageUtil.transform(recordPage, AuthGradeApplyRecordPageDTO.class);

        Map<Long, GradeInfo> cacheMap = new HashMap<>();
        for (AuthGradeApplyRecordPageDTO pageDTO : resultPage.getList()) {
            GradeInfo gradeInfo = cacheMap.get(pageDTO.getGradeId());
            if (ObjectUtil.isNull(gradeInfo)) {
                gradeInfo = gradeInfoDao.getById(pageDTO.getGradeId());
            }
            if (ObjectUtil.isNotNull(gradeInfo)) {
                pageDTO.setGradeName(gradeInfo.getGradeName());
                cacheMap.put(gradeInfo.getId(), gradeInfo);
            }
        }

        return Result.success(resultPage);
    }

    /**
     * 申请记录审核
     *
     * @param bo 审核参数
     * @return 审核结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> audit(AuthGradeApplyRecordAuditBO bo) {
        GradeApplyRecord applyRecord = dao.getByIdForUpdate(bo.getRecordId());
        if (ObjectUtil.isNull(applyRecord)) {
            return Result.error("申请记录不存在");
        }
        if (!GradeApplyAuditStatusEnum.WAIT.getCode().equals(applyRecord.getAuditStatus())) {
            return Result.error("申请记录已处理，不需要重复处理");
        }

        // 校验班级
        GradeInfo gradeInfo = gradeInfoDao.getById(applyRecord.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在或已解散");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("当前班级不可用，暂时无法操作");
        }

        // 校验管理员用户
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("管理员不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("管理员状态异常");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            // 审核人员不是讲师
            GradeStudent gradeAdminStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(),ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeAdminStudent)) {
                return Result.error("不是班级成员，无法操作");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeAdminStudent.getUserRole())) {
                return Result.error("不是班级管理员，无法操作");
            }
        }

        // 处理审核
        GradeApplyRecord updateRecord = new GradeApplyRecord();
        updateRecord.setId(applyRecord.getId());
        updateRecord.setAuditUserNo(ThreadContext.userNo());
        if (!bo.getAuditPassed()) {
            // 审核不通过
            updateRecord.setAuditStatus(GradeApplyAuditStatusEnum.REFUSAL.getCode());
            updateRecord.setAuditOpinion(bo.getAuditOpinion());
            dao.updateById(updateRecord);
            //发站内信
            try {
                pushMsgForGradeAdd(gradeInfo.getGradeName(),applyRecord.getUserNo(),false,bo.getAuditOpinion());
            } catch (Exception e) {
                logger.warn("推送站内信失败", e);
            }
            return Result.success("审核处理成功");
        }

        // 校验学员
        UserVO studentUserVO = feignUser.getByUserNo(applyRecord.getUserNo());
        if (ObjectUtil.isNull(studentUserVO)) {
            return Result.error("学员不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(studentUserVO.getStatusId())) {
            return Result.error("学员状态异常");
        }

        updateRecord.setAuditStatus(GradeApplyAuditStatusEnum.ASSENT.getCode());
        updateRecord.setAuditOpinion(bo.getAuditOpinion());
        dao.updateById(updateRecord);
        GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), applyRecord.getUserNo());
        // 保存学生班级关系
        if (ObjectUtil.isNull(gradeStudent)) {
            GradeStudent saveStudent = new GradeStudent();
            saveStudent.setGradeId(gradeInfo.getId());
            saveStudent.setLecturerUserNo(gradeInfo.getLecturerUserNo());
            saveStudent.setUserNo(applyRecord.getUserNo());
            saveStudent.setUserRole(GradeStudentUserRoleEnum.USER.getCode());
            saveStudent.setNickname(applyRecord.getNickname());
            gradeStudentDao.save(saveStudent);
            //加入成功，给学员发站内信
            try {
                pushMsgForGradeAdd(gradeInfo.getGradeName(),applyRecord.getUserNo(),true,bo.getAuditOpinion());
            } catch (Exception e) {
                logger.warn("推送站内信失败", e);
            }
        }

        return Result.success("审核处理成功");
    }

    /**
     * 学员加入班级成功，给发站内信
     * @param gradeName
     * @param userNo
     * @param isSuccess 成功通过
     */
    private void pushMsgForGradeAdd(String gradeName,Long userNo,Boolean isSuccess,String auditOpinion) {
        List<UserMsgAndMsgSaveQO> list = new ArrayList<>();
        UserMsgAndMsgSaveQO qo = new UserMsgAndMsgSaveQO();
        UserVO userVO = feignUser.getByUserNo(userNo);
        MsgQO msg = new MsgQO();
        msg.setId(IdWorker.getId());
        if(isSuccess){
            msg.setMsgTitle("【"+gradeName+"】入班成功通知");
            msg.setMsgText("<a href=\""+SysConfigConstants.EXAM_MYGRADE+"\">您已经成功加入班级【"+gradeName+"】,快去看看吧。</a>");
        }else {
            msg.setMsgTitle("【"+gradeName+"】入班未通过通知");
            msg.setMsgText("<a href=\""+SysConfigConstants.EXAM_MYGRADE+"\">您加入班级【"+gradeName+"】的申请未通过:"+auditOpinion+"</a>");
        }
        msg.setStatusId(StatusIdEnum.YES.getCode());
        msg.setIsTop(IsTopEnum.YES.getCode());
        msg.setIsSend(IsSendEnum.YES.getCode());
        msg.setIsTimeSend(IsTimeSendEnum.NO.getCode());
        msg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        msg.setBackRemark("入班通知");
        msg.setSendTime(null);
        qo.setMsg(msg);

        UserMsgQO userMsg = new UserMsgQO();
        userMsg.setStatusId(StatusIdEnum.YES.getCode());
        userMsg.setMsgId(msg.getId());
        userMsg.setUserNo(userNo);
        userMsg.setMobile(userVO.getMobile());
        userMsg.setMsgType(MsgTypeEnum.SYSTEM.getCode());
        userMsg.setMsgTitle(msg.getMsgTitle());
        userMsg.setIsRead(IsReadEnum.NO.getCode());
        userMsg.setIsTop(IsTopEnum.YES.getCode());
        qo.setUserMsg(userMsg);

        list.add(qo);
        feignUserMsg.batchSaveUserMsgAndMsg(list);

    }


    /**
     * 批量审核
     * @param bo
     * @return
     */
    public Result<String> batchAudit(AuthGradeApplyRecordBatchAuditBO bo) {
        //检验

        // 校验班级
        GradeInfo gradeInfo = gradeInfoDao.getById(bo.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在或已解散");
        }
        if (!StatusIdEnum.YES.getCode().equals(gradeInfo.getStatusId())) {
            return Result.error("当前班级不可用，暂时无法操作");
        }
        // 校验管理员用户
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("管理员不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("管理员状态异常");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            // 审核人员不是讲师
            GradeStudent gradeAdminStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeAdminStudent)) {
                return Result.error("不是班级成员，无法操作");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeAdminStudent.getUserRole())) {
                return Result.error("不是班级管理员，无法操作");
            }
        }
        String[] ids = bo.getRecordIds().split(",");
        try {
            for (String idStr : ids) {
                Long id = Long.valueOf(idStr);
                GradeApplyRecord applyRecord = dao.getByIdForUpdate(id);
                // 申请记录不存在,已经关联过，也跳过
                if (ObjectUtil.isNull(applyRecord)) {
                    logger.info("{}的申请记录id={}不存在,不处理该记录", applyRecord.getNickname(), id);
                    continue;
                }
                //检验  申请来自本班级
                if (applyRecord.getGradeId().longValue() != gradeInfo.getId()) {
                    logger.info("{}的申请记录id={},异常，不在班级范围内,不处理该记录", applyRecord.getNickname(), id);
                    continue;
                }
                if (!GradeApplyAuditStatusEnum.WAIT.getCode().equals(applyRecord.getAuditStatus())) {
                    logger.info("{}的申请记录id={}已处理,不处理该记录", applyRecord.getNickname(), id);
                    continue;
                }
                // 处理审核
                GradeApplyRecord updateRecord = new GradeApplyRecord();
                updateRecord.setId(applyRecord.getId());
                updateRecord.setAuditUserNo(ThreadContext.userNo());
                if (!bo.getAuditPassed()) {
                    // 审核不通过
                    updateRecord.setAuditStatus(GradeApplyAuditStatusEnum.REFUSAL.getCode());
                    updateRecord.setAuditOpinion(bo.getAuditOpinion());
                    dao.updateById(updateRecord);
                    logger.info("{}的申请记录id={}已处理,审核处理成功", applyRecord.getNickname(), id);
                    continue;
                }

                // 校验学员
                UserVO studentUserVO = feignUser.getByUserNo(applyRecord.getUserNo());
                if (ObjectUtil.isNull(studentUserVO)) {
                    logger.info("{}学员不存在", applyRecord.getNickname());
                    continue;
                }
                if (!StatusIdEnum.YES.getCode().equals(studentUserVO.getStatusId())) {
                    logger.info("{}学员状态异常", applyRecord.getNickname());
                    continue;
                }

                updateRecord.setAuditStatus(GradeApplyAuditStatusEnum.ASSENT.getCode());
                updateRecord.setAuditOpinion(bo.getAuditOpinion());
                dao.updateById(updateRecord);
                GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(gradeInfo.getId(), applyRecord.getUserNo());
                // 保存学生班级关系
                if (ObjectUtil.isNull(gradeStudent)) {
                    GradeStudent saveStudent = new GradeStudent();
                    saveStudent.setGradeId(gradeInfo.getId());
                    saveStudent.setLecturerUserNo(gradeInfo.getLecturerUserNo());
                    saveStudent.setUserNo(applyRecord.getUserNo());
                    saveStudent.setUserRole(GradeStudentUserRoleEnum.USER.getCode());
                    saveStudent.setNickname(applyRecord.getNickname());
                    gradeStudentDao.save(saveStudent);
                    //加入成功，给学员发站内信
                    try {
                        pushMsgForGradeAdd(gradeInfo.getGradeName(),applyRecord.getUserNo(),true,bo.getAuditOpinion());
                    } catch (Exception e) {
                        logger.warn("推送站内信失败", e);
                    }
                }
            }


        } catch (Exception e) {
            logger.info(e.toString());
        }
        return Result.success("操作结束");
    }

    public Result<AuthGradeApplyRecordSummaryDTO> summary(AuthGradeApplyRecordSummaryBO bo) {
        AuthGradeApplyRecordSummaryDTO dto = new AuthGradeApplyRecordSummaryDTO();
        int count = dao.countByGradeIdAndAudit(bo.getGradeId(), null);
        int waitCount = dao.countByGradeIdAndAudit(bo.getGradeId(), GradeApplyAuditStatusEnum.WAIT.getCode());
        int assentCount = dao.countByGradeIdAndAudit(bo.getGradeId(), GradeApplyAuditStatusEnum.ASSENT.getCode());
        int refusalCount = dao.countByGradeIdAndAudit(bo.getGradeId(), GradeApplyAuditStatusEnum.REFUSAL.getCode());
        dto.setCount(count);
        dto.setWaitCount(waitCount);
        dto.setAssentCount(assentCount);
        dto.setRefusalCount(refusalCount);
        return Result.success(dto);
    }

    public Result<List<AuthGradeApplyRecordTipsListDTO>> tipsList(AuthGradeApplyRecordTipsListBO bo) {
        // 判断用户身份
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("当前用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("状态异常，不能操作");
        }

        List<Long> lecturerUserNoList = new ArrayList<>();
        if (bo.getIsLecturer()) {
            lecturerUserNoList.add(ThreadContext.userNo());
        } else {
            // 获取用户作为管理员所在的班级
            List<GradeStudent> gradeStudentList = gradeStudentDao.listByUserNoAndUserRoleAndStatusId(ThreadContext.userNo(), GradeStudentUserRoleEnum.ADMIN.getCode(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(gradeStudentList)) {
                lecturerUserNoList.addAll(gradeStudentList.stream().map(GradeStudent::getLecturerUserNo).collect(Collectors.toList()));
            } else {
                return Result.success(Collections.emptyList());
            }
        }

        List<GradeApplyRecord> gradeApplyRecordList = dao.limitTipsListInLecturerUserNo(lecturerUserNoList, bo.getNum());
        if (CollectionUtil.isEmpty(gradeApplyRecordList)) {
            return Result.success(Collections.emptyList());
        }

        Map<Long, LecturerVO> lecturerCacheMap = new HashMap<>();
        Map<Long, GradeInfo> gradeInfoCacheMap = new HashMap<>();
        List<AuthGradeApplyRecordTipsListDTO> dtoList = BeanUtil.copyProperties(gradeApplyRecordList, AuthGradeApplyRecordTipsListDTO.class);
        for (AuthGradeApplyRecordTipsListDTO dto : dtoList) {
            // 获取讲师名称
            LecturerVO lecturerVO = lecturerCacheMap.get(dto.getLecturerUserNo());
            if (ObjectUtil.isNull(lecturerVO)) {
                lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            }
            if (ObjectUtil.isNotNull(lecturerVO)) {
                dto.setLecturerName(lecturerVO.getLecturerName());
                lecturerCacheMap.put(dto.getLecturerUserNo(), lecturerVO);
            }

            // 获取班级名称
            GradeInfo gradeInfo = gradeInfoCacheMap.get(dto.getGradeId());
            if (ObjectUtil.isNull(gradeInfo)) {
                gradeInfo = gradeInfoDao.getById(dto.getGradeId());
            }
            if (ObjectUtil.isNotNull(gradeInfo)) {
                dto.setGradeName(gradeInfo.getGradeName());
                gradeInfoCacheMap.put(dto.getGradeId(), gradeInfo);
            }
        }
        return Result.success(dtoList);
    }

    /**
     * 申请记录忽略
     *
     * @param bo 忽略请求对象
     * @return 处理结果
     */
    public Result<String> recordViewIgnore(AuthGradeApplyRecordIgnoreBO bo) {
        GradeApplyRecord applyRecord = dao.getById(bo.getId());
        if (ObjectUtil.isNull(applyRecord)) {
            return Result.error("申请记录不存在");
        }

        //判断班级，忽略与班级状态无关
        GradeInfo gradeInfo = gradeInfoDao.getById(applyRecord.getGradeId());
        if (ObjectUtil.isNull(gradeInfo)) {
            return Result.error("班级不存在");
        }

        // 判断用户身份
        UserVO userVO = feignUser.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userVO)) {
            return Result.error("当前用户不存在");
        }
        if (!StatusIdEnum.YES.getCode().equals(userVO.getStatusId())) {
            return Result.error("状态异常，不能操作");
        }
        if (!gradeInfo.getLecturerUserNo().equals(ThreadContext.userNo())) {
            GradeStudent gradeStudent = gradeStudentDao.getByGradeIdAndUserNo(applyRecord.getGradeId(), ThreadContext.userNo());
            if (ObjectUtil.isNull(gradeStudent)) {
                return Result.error("不是班级成员，没有操作权限");
            }
            if (!GradeStudentUserRoleEnum.ADMIN.getCode().equals(gradeStudent.getUserRole())) {
                return Result.error("不是班级管理员，没有操作权限");
            }
        }

        // 只有未查看才能忽略，否则直接跳过
        if (GradeViewStatusEnum.NO.getCode().equals(applyRecord.getViewStatus())) {
            GradeApplyRecord updateApplyRecord = new GradeApplyRecord();
            updateApplyRecord.setId(applyRecord.getId());
            updateApplyRecord.setViewStatus(GradeViewStatusEnum.IGNORE.getCode());
            if (dao.updateById(updateApplyRecord) > 0) {
                return Result.success("处理成功");
            }
            return Result.error("处理失败");
        }

        return Result.success("处理成功");
    }
}
