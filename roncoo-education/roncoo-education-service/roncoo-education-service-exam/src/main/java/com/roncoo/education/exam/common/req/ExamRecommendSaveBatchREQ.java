package com.roncoo.education.exam.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * <p>
 * 试卷推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ExamRecommendSaveBatchREQ", description="批量添加")
public class ExamRecommendSaveBatchREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "试卷IDs不能为空")
    @ApiModelProperty(value = "试卷IDs",required = true)
    private String examIds;
}
