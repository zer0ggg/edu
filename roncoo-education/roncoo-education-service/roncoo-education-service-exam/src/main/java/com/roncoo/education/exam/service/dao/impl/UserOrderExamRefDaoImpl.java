package com.roncoo.education.exam.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.exam.service.dao.UserOrderExamRefDao;
import com.roncoo.education.exam.service.dao.impl.mapper.UserOrderExamRefMapper;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRef;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.UserOrderExamRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户订单试卷关联 服务实现类
 *
 * @author wujing
 * @date 2020-06-03
 */
@Repository
public class UserOrderExamRefDaoImpl implements UserOrderExamRefDao {

    @Autowired
    private UserOrderExamRefMapper mapper;

    @Override
    public int save(UserOrderExamRef record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserOrderExamRef record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public UserOrderExamRef getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserOrderExamRef> listForPage(int pageCurrent, int pageSize, UserOrderExamRefExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public UserOrderExamRef getByUserNoAndExamId(Long userNo, Long examId) {
        UserOrderExamRefExample example = new UserOrderExamRefExample();
        UserOrderExamRefExample.Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andExamIdEqualTo(examId);
        List<UserOrderExamRef> resultList = mapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }
}
