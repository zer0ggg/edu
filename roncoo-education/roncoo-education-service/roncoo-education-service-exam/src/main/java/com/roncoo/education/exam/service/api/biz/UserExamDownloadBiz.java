package com.roncoo.education.exam.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.enums.ProblemTypeEnum;
import com.roncoo.education.common.core.tools.NumberUtil;
import com.roncoo.education.exam.common.util.ExamExportUtil;
import com.roncoo.education.exam.service.dao.*;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户试卷下载
 */
@Component
public class UserExamDownloadBiz {

    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamTitleDao examTitleDao;
    @Autowired
    private ExamTitleProblemRefDao examTitleProblemRefDao;
    @Autowired
    private ExamProblemDao examProblemDao;
    @Autowired
    private ExamProblemOptionDao examProblemOptionDao;

    public void export(Long examId, HttpServletRequest request, HttpServletResponse response) {
        StringBuilder sb = new StringBuilder();

        //试卷名称
        String exportFileName = "exam";

        // 获取试卷
        ExamInfo examInfo = examInfoDao.getById(examId);
        if (ObjectUtil.isNotNull(examInfo)) {
            sb.append("<div style=\"margin: 0px auto 20px; display: flex;  flex: 1; min-height: 400px;\">");

            //试卷名称
            sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");
            sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
            sb.append(examInfo.getExamName());
            sb.append("</div>");
            sb.append("</div>");

            exportFileName = examInfo.getExamName();

            // 试卷标题
            listExamTitle(examId, sb);
            sb.append("</div>");
        } else {
            sb.append("<h1>试卷不存在</h1>");
        }

        System.out.println("============================================");
        System.out.println(sb.toString());
        System.out.println("============================================");

        //ftl模板文件路径
        String templateFileName = "template/examTemplate.ftl";


        String docSrcLocationPrex = "file:///C:/213792E5";
        String docSrcParent = "examTemplate.files";
        String nextPartId = "01D644B6.11D02A80";
        ExamExportUtil.export(request, response, templateFileName, exportFileName, docSrcLocationPrex, docSrcParent, nextPartId, sb.toString());

    }

    private void listExamTitle(Long examId, StringBuilder sb) {
        List<ExamTitle> titleList = examTitleDao.listByExamId(examId);
        if (CollectionUtil.isEmpty(titleList)) {
            return;
        }

        int titleIndex = 1;
        int problemIndex = 1;
        for (ExamTitle title : titleList) {
            // 试卷标题
            sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");
            sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
            sb.append(NumberUtil.intToChineseNumber(titleIndex)).append("、").append(title.getTitleName());
            sb.append("</div>");
            sb.append("</div>");

            // 标题题目
            problemIndex = listTitleProblem(title, problemIndex, sb);

            titleIndex++;
        }
    }

    private Integer listTitleProblem(ExamTitle title, Integer problemIndex, StringBuilder sb) {
        List<ExamTitleProblemRef> refList = examTitleProblemRefDao.listByTitleId(title.getId());
        if (CollectionUtil.isEmpty(refList)) {
            return problemIndex;
        }

        // 获取题目
        List<Long> problemIdList = refList.stream().map(ExamTitleProblemRef::getProblemId).collect(Collectors.toList());
        List<ExamProblem> problemList = examProblemDao.listInIdList(problemIdList);
        if (CollectionUtil.isEmpty(problemList)) {
            return problemIndex;
        }
        Map<Long, ExamProblem> problemMap = problemList.stream().collect(Collectors.toMap(ExamProblem::getId, problem -> problem));

        if (ProblemTypeEnum.MATERIAL.getCode().equals(title.getTitleType())) {
            // 组合题
            List<ExamTitleProblemRef> parentRefList = refList.stream().filter(ref -> ProblemTypeEnum.MATERIAL.getCode().equals(ref.getProblemType())).collect(Collectors.toList());
            for (ExamTitleProblemRef ref : parentRefList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }

                sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");

                sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
                sb.append(problemIndex).append("、");
//                sb.append("<div style=\"flex: 1\">");
                //String problemContent = problem.getProblemContent().replaceAll("</p><p>", "<br>").replaceAll("<p>", "").replaceAll("</p>", "");
                //sb.append(problemContent);
                sb.append(getProblemContent(problem.getProblemContent()));
//                sb.append("</div>");
                sb.append("</div>");

                // 获取子题
                listSubProblem(problem, refList, problemMap, sb);
                sb.append("</div>");

                problemIndex++;
            }
        } else {
            // 其他题型
            for (ExamTitleProblemRef ref : refList) {
                ExamProblem problem = problemMap.get(ref.getProblemId());
                if (ObjectUtil.isNull(problem)) {
                    continue;
                }

                sb.append("<div style=\"padding: 10px 20px; background:#fff; margin-bottom: 10px; border-radius: 8px; overflow: hidden;\">");

                sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
                sb.append(problemIndex).append("、");
//                sb.append("<div style=\"flex: 1\">");
                //sb.append(problem.getProblemContent().replaceAll("</p><p>", "<br>").replaceAll("<p>", "").replaceAll("</p>", ""));
                sb.append(getProblemContent(problem.getProblemContent()));
//                sb.append("</div>");
                sb.append("</div>");

                // 选项
                if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
                    listProblemOption(ref.getProblemId(), sb);
                }
                sb.append("</div>");

                problemIndex++;
            }
        }

        return problemIndex;
    }

    private void listProblemOption(Long problemId, StringBuilder sb) {
        List<ExamProblemOption> optionList = examProblemOptionDao.listByProblemIdWithBLOBs(problemId);
        if (CollectionUtil.isEmpty(optionList)) {
            return;
        }

        int optionIndex = 1;
        sb.append("<div style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
        for (ExamProblemOption option : optionList) {
            sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;");
            sb.append("<span>").append(NumberUtil.intToLetter(optionIndex)).append("．</span>");
            sb.append("<span>").append(option.getOptionContent()).append("</span>");
            sb.append("</div>");

            optionIndex++;
        }
        sb.append("</div>");
    }

    private void listSubProblem(ExamProblem parentProblem, List<ExamTitleProblemRef> refList, Map<Long, ExamProblem> problemMap, StringBuilder sb) {
        List<ExamTitleProblemRef> subRefList = refList.stream().filter(ref -> parentProblem.getId().equals(ref.getProblemParentId())).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(subRefList)) {
            return;
        }

        int problemIndex = 1;
        for (ExamTitleProblemRef ref : subRefList) {
            ExamProblem problem = problemMap.get(ref.getProblemId());
            if (ObjectUtil.isNull(problem)) {
                continue;
            }

            // 子题
            sb.append("<div style=\"font-weight: 700; font-style: normal; font-size: 16px; line-height: 42px; display: flex;\">");
            sb.append("(").append(problemIndex).append(")、");
            sb.append(" <div style=\"font-weight: 400; font-style: normal; font-size: 12px; color: #333333; line-height: 28px;\">");
            //sb.append(problem.getProblemContent());
//            sb.append(problem.getProblemContent().replaceAll("</p><p>", "<br>\t").replaceAll("<p>", "").replaceAll("</p>", ""));
            sb.append(getProblemContent(problem.getProblemContent()));
            sb.append("</div>");
            sb.append("</div>");

            // 选项
            if (ProblemTypeEnum.THE_RADIO.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.MULTIPLE_CHOICE.getCode().equals(problem.getProblemType()) || ProblemTypeEnum.ESTIMATE.getCode().equals(problem.getProblemType())) {
                listProblemOption(ref.getProblemId(), sb);
            }

            problemIndex++;
        }
    }

    private String getProblemContent(String problemContent) {
        int startIndex = problemContent.indexOf("<p");
        if (startIndex == 0) {
            problemContent = "<span" + problemContent.substring(startIndex + 2);
            int endIndex = problemContent.indexOf("</p>");
            if (endIndex > 0) {
                problemContent = problemContent.substring(0, endIndex) + "</span>" + problemContent.substring(endIndex + 4);
            }
        }
        return problemContent;
    }
}
