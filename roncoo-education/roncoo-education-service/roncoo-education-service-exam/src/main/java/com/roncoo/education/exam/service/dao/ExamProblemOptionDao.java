package com.roncoo.education.exam.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOption;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamProblemOptionExample;

import java.util.List;

/**
 * 试卷题目选项 服务类
 *
 * @author wujing
 * @date 2020-06-03
 */
public interface ExamProblemOptionDao {

    /**
     * 保存试卷题目选项
     *
     * @param record 试卷题目选项
     * @return 影响记录数
     */
    int save(ExamProblemOption record);

    /**
     * 根据ID删除试卷题目选项
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 试卷题目选项
     * @return 影响记录数
     */
    int updateById(ExamProblemOption record);

    /**
     * 根据ID获取试卷题目选项
     *
     * @param id 主键ID
     * @return 试卷题目选项
     */
    ExamProblemOption getById(Long id);

    /**
     * 试卷题目选项--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<ExamProblemOption> listForPage(int pageCurrent, int pageSize, ExamProblemOptionExample example);

    /**
     * 根据题目ID和状态ID获取题目选项
     *
     * @param problemId 题目ID
     * @param statusId  状态ID
     * @return 题目选项
     */
    List<ExamProblemOption> listByProblemIdAndStatusId(Long problemId, Integer statusId);

    /**
     * 根据试题ID获取选项信息
     *
     * @param problemId 题目ID
     * @return
     */
    List<ExamProblemOption> listByProblemId(Long problemId);

    /**
     * 根据试题ID获取选项信息
     *
     * @param problemId 小题ID
     * @return
     */
    List<ExamProblemOption> listByProblemIdWithBLOBs(Long problemId);


    /**
     * 根据题目id删除选项
     *
     * @param problemId
     * @return
     */
    int deleteByProblemId(Long problemId);
}
