package com.roncoo.education.exam.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditEditREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditListREQ;
import com.roncoo.education.exam.common.req.ExamTitleProblemRefAuditSaveREQ;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefAuditListRESP;
import com.roncoo.education.exam.common.resp.ExamTitleProblemRefAuditViewRESP;
import com.roncoo.education.exam.service.dao.ExamTitleProblemRefAuditDao;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAudit;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample;
import com.roncoo.education.exam.service.dao.impl.mapper.entity.ExamTitleProblemRefAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 试卷标题题目关联审核
 *
 * @author wujing
 */
@Component
public class PcExamTitleProblemRefAuditBiz extends BaseBiz {

	@Autowired
	private ExamTitleProblemRefAuditDao dao;

	/**
	 * 试卷标题题目关联审核列表
	 *
	 * @param examTitleProblemRefAuditListREQ 试卷标题题目关联审核分页查询参数
	 * @return 试卷标题题目关联审核分页查询结果
	 */
	public Result<Page<ExamTitleProblemRefAuditListRESP>> list(ExamTitleProblemRefAuditListREQ examTitleProblemRefAuditListREQ) {
		ExamTitleProblemRefAuditExample example = new ExamTitleProblemRefAuditExample();
		Criteria c = example.createCriteria();
		Page<ExamTitleProblemRefAudit> page = dao.listForPage(examTitleProblemRefAuditListREQ.getPageCurrent(), examTitleProblemRefAuditListREQ.getPageSize(), example);
		Page<ExamTitleProblemRefAuditListRESP> respPage = PageUtil.transform(page, ExamTitleProblemRefAuditListRESP.class);
		return Result.success(respPage);
	}

	/**
	 * 试卷标题题目关联审核添加
	 *
	 * @param examTitleProblemRefAuditSaveREQ 试卷标题题目关联审核
	 * @return 添加结果
	 */
	public Result<String> save(ExamTitleProblemRefAuditSaveREQ examTitleProblemRefAuditSaveREQ) {
		ExamTitleProblemRefAudit record = BeanUtil.copyProperties(examTitleProblemRefAuditSaveREQ, ExamTitleProblemRefAudit.class);
		if (dao.save(record) > 0) {
			return Result.success("添加成功");
		}
		return Result.error("添加失败");
	}

	/**
	 * 试卷标题题目关联审核查看
	 *
	 * @param id 主键ID
	 * @return 试卷标题题目关联审核
	 */
	public Result<ExamTitleProblemRefAuditViewRESP> view(Long id) {
		return Result.success(BeanUtil.copyProperties(dao.getById(id), ExamTitleProblemRefAuditViewRESP.class));
	}

	/**
	 * 试卷标题题目关联审核修改
	 *
	 * @param examTitleProblemRefAuditEditREQ 试卷标题题目关联审核修改对象
	 * @return 修改结果
	 */
	public Result<String> edit(ExamTitleProblemRefAuditEditREQ examTitleProblemRefAuditEditREQ) {
		ExamTitleProblemRefAudit record = BeanUtil.copyProperties(examTitleProblemRefAuditEditREQ, ExamTitleProblemRefAudit.class);
		if (dao.updateById(record) > 0) {
			return Result.success("编辑成功");
		}
		return Result.error("编辑失败");
	}

	/**
	 * 试卷标题题目关联审核删除
	 *
	 * @param id ID主键
	 * @return 删除结果
	 */
	public Result<String> delete(Long id) {
		if (dao.deleteById(id) > 0) {
			return Result.success("删除成功");
		}
		return Result.error("删除失败");
	}
}
