package com.roncoo.education.exam.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 试卷信息表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class ExamInfoLecturerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 讲师用户编号
	 */
	@ApiModelProperty(value = "讲师编号", required = true)
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lecturerUserNo;
	/**
	 * 讲师名称
	 */
	@ApiModelProperty(value = "讲师名称", required = true)
	private String lecturerName;
	/**
	 * 职位
	 */
	@ApiModelProperty(value = "讲师职位", required = true)
	private String position;
	/**
	 * 年龄
	 */
	@ApiModelProperty(value = "讲师年龄", required = true)
	private Integer age;
	/**
	 * 性别(1:为男,0：为女)
	 */
	@ApiModelProperty(value = "讲师性别(1:为男,0：为女)", required = true)
	private Integer sex;
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "讲师头像", required = true)
	private String imgLogo;
	/**
	 * 简介
	 */
	@ApiModelProperty(value = "讲师介绍", required = true)
	private String introduce;
}
