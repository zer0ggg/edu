package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.VipCheckUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryDownloadBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryListBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessorySaveBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseAccessoryDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseAccessoryListDTO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * 附件信息
 *
 * @author wujing
 */
@Component
public class AuthCourseAccessoryBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private CourseAccessoryDao dao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseChapterAuditDao courseChapterAuditDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;

    /**
     * 附件下载接口
     *
     * @param bo
     * @return
     */
    public Result<String> download(AuthCourseAccessoryDownloadBO bo) {
        if (bo.getId() == null) {
            return Result.error("附件ID不能为空");
        }
        CourseAccessory accessory = dao.getById(bo.getId());
        if (ObjectUtil.isNull(accessory)) {
            return Result.error("找不到附件信息");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("用户不存在，请联系客服");
        }
        if (StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系客服");
        }
        if (RefTypeEnum.PERIOD.getCode().equals(accessory.getRefType())) {
            CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(accessory.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriod) || !StatusIdEnum.YES.getCode().equals(courseChapterPeriod.getStatusId())) {
                return Result.error("找不到课时信息");
            }
            Course course = courseDao.getById(courseChapterPeriod.getCourseId());
            if (ObjectUtil.isNull(course)) {
                return Result.error("课程不存在");
            }
            if (!StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
                return Result.error("课程已禁用");
            }
            if (IsFreeEnum.CHARGE.getCode().equals(courseChapterPeriod.getIsFree())) {
                // 校验课程是否购买
                int isPay = checkCourse(userExtVO, courseChapterPeriod.getCourseId());
                if (IsPayEnum.NO.getCode().equals(isPay)) {
                    return Result.error("购买后才可以下载");
                }
            }
        } else if (RefTypeEnum.COURSE.getCode().equals(accessory.getRefType())) {
            Course course = courseDao.getById(accessory.getRefId());
            if (ObjectUtil.isNull(course)) {
                return Result.error("课程不存在");
            }
            if (!StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
                return Result.error("课程已禁用");
            }
            // 校验课程是否购买
            int isPay = checkCourse(userExtVO, accessory.getRefId());
            if (IsPayEnum.NO.getCode().equals(isPay)) {
                return Result.error("购买后才可以下载");
            }
        }

        // 设置过期时间为1分钟
        Date expires = new Date(System.currentTimeMillis() + 1000 * 60);
        String acUrl = AliyunUtil.getUrlSign(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), accessory.getAcUrl(), expires);
        accessory.setDownloadCount(accessory.getDownloadCount() + 1);
        dao.updateById(accessory);
        return Result.success(acUrl);

    }

    /**
     * 附件信息保存接口
     *
     * @param bo
     * @author kyh
     */
    public Result<Integer> save(AuthCourseAccessorySaveBO bo) {
        if (StringUtils.isEmpty(bo.getAcName())) {
            return Result.error("附件名称不能为空");
        }
        if (StringUtils.isEmpty(bo.getAcUrl())) {
            return Result.error("附件地址不能为空");
        }
        if (bo.getRefId() == null) {
            return Result.error("关联ID不能为空");
        }
        if (bo.getCourseCategory() == null) {
            return Result.error("课程分类不能为空");
        }
        if (bo.getRefType() == null) {
            return Result.error("关联类型不能为空");
        }
        if (RefTypeEnum.COURSE.getCode().equals(bo.getRefType())) {
            CourseAudit courseAudit = courseAuditDao.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseAudit) || !StatusIdEnum.YES.getCode().equals(courseAudit.getStatusId())) {
                return Result.error("找不到课程审核信息");
            }
        } else if (RefTypeEnum.CHAPTER.getCode().equals(bo.getRefType())) {
            CourseChapterAudit courseChapterAudit = courseChapterAuditDao.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterAudit) || !StatusIdEnum.YES.getCode().equals(courseChapterAudit.getStatusId())) {
                return Result.error("找不到章节审核信息");
            }
        } else {
            CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(bo.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriodAudit) || !StatusIdEnum.YES.getCode().equals(courseChapterPeriodAudit.getStatusId())) {
                return Result.error("找不到课时审核信息");
            }
        }
        CourseAccessory accessory = BeanUtil.copyProperties(bo, CourseAccessory.class);
        int results = dao.save(accessory);
        if (results > 0) {
            return Result.success(results);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }

    /**
     * 附件信息删除接口
     *
     * @param bo
     * @author kyh
     */
    public Result<Integer> delete(AuthCourseAccessoryDeleteBO bo) {
        if (bo.getId() == null) {
            return Result.error("主键ID不能为空");
        }
        CourseAccessory accessory = dao.getById(bo.getId());
        if (ObjectUtil.isNull(accessory)) {
            return Result.error("找不到附件信息");
        }
        accessory.setStatusId(Constants.FREEZE);
        int results = dao.updateById(accessory);
        if (results > 0) {
            AliyunUtil.delete(accessory.getAcUrl(), BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
            return Result.success(results);
        }
        return Result.error(ResultEnum.COURSE_DELETE_FAIL);
    }

    /**
     * 附件列出接口
     *
     * @param bo
     * @author kyh
     */
    public Result<AuthCourseAccessoryListDTO> list(AuthCourseAccessoryListBO bo) {
        if (bo.getRefId() == null) {
            return Result.error("关联ID不能为空");
        }
        AuthCourseAccessoryListDTO dto = new AuthCourseAccessoryListDTO();
        List<CourseAccessory> list = dao.listByRefIdAndStatusId(bo.getRefId(), StatusIdEnum.YES.getCode());
        dto.setList(ArrayListUtil.copy(list, AuthCourseAccessoryDTO.class));
        return Result.success(dto);
    }

    /**
     * 校验是否已购买或免费 （1："已支付", 0："未支付");
     *
     * @param userExtVO
     * @param courseId
     * @return
     */
    private Integer checkCourse(UserExtVO userExtVO, Long courseId) {
        int isPay = 1;
        Course course = courseDao.getById(courseId);
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            isPay = 0;
        }
        if (IsFreeEnum.CHARGE.getCode().equals(course.getIsFree())) {
            // 根据用户编号、课程ID获取 用户课程关联信息
            UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(userExtVO.getUserNo(), courseId);
            if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
                // 未购买或者没支付情况
                isPay = 0;
            } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
                isPay = 0;
            }
            // 课程会员免费、用戶是会员
            if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), course.getCourseDiscount())) {
                isPay = 1;
            }
        }

        return isPay;
    }

}
