package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterPeriodListBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterPeriodListDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseChapterPeriodBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课时信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/course/chapter/period")
public class AuthCourseChapterPeriodController extends BaseController {

	@Autowired
	private AuthCourseChapterPeriodBiz biz;

	/**
	 * 课时列出接口
	 *
	 * @param authCourseChapterPeriodListBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时列出接口", notes = "根据章节ID列出课时信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<AuthCourseChapterPeriodListDTO> listByChapterId(@RequestBody AuthCourseChapterPeriodListBO authCourseChapterPeriodListBO) {
		return biz.listByChapterId(authCourseChapterPeriodListBO);
	}

}
