package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthUserOrderCourseRefPageBO;
import com.roncoo.education.course.common.dto.auth.AuthUserOrderCourseRefPageDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthUserOrderCourseRefBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/user/order/course/ref")
public class AuthUserOrderCourseRefController {

	@Autowired
	private AuthUserOrderCourseRefBiz biz;

	/**
	 * 用户普我的课程、直播、试卷 列表接口
	 */
	@ApiOperation(value = "用户普我的课程、直播、试卷 列表接口", notes = "用户普我的课程、直播、试卷 列表接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthUserOrderCourseRefPageDTO>> listForPage(@RequestBody AuthUserOrderCourseRefPageBO authUserOrderCourseRefPageBO) {
		return biz.listForPage(authUserOrderCourseRefPageBO);
	}

}
