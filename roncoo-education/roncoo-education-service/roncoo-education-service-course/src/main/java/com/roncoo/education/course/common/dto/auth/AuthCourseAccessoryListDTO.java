package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 附件集合
 *
 * @author kyh
 *
 */
@Data
@Accessors(chain = true)
public class AuthCourseAccessoryListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 附件集合
	 */
	@ApiModelProperty(value = "附件集合")
	private List<AuthCourseAccessoryDTO> list = new ArrayList<>();

}
