/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 列出直播课程信息实体
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthCoursePageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程分类(1:普通课程;2:直播课程,3:试卷)
     */
    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:试卷)", required = true)
    private Integer courseCategory;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数")
    private Integer pageSize = 20;

}
