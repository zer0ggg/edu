package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ResourceRecommendMapper {
    int countByExample(ResourceRecommendExample example);

    int deleteByExample(ResourceRecommendExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ResourceRecommend record);

    int insertSelective(ResourceRecommend record);

    List<ResourceRecommend> selectByExample(ResourceRecommendExample example);

    ResourceRecommend selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ResourceRecommend record, @Param("example") ResourceRecommendExample example);

    int updateByExample(@Param("record") ResourceRecommend record, @Param("example") ResourceRecommendExample example);

    int updateByPrimaryKeySelective(ResourceRecommend record);

    int updateByPrimaryKey(ResourceRecommend record);
}
