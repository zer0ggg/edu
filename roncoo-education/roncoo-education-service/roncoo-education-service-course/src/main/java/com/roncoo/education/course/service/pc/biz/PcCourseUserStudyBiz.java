package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseUserStudyEditREQ;
import com.roncoo.education.course.common.req.CourseUserStudyListREQ;
import com.roncoo.education.course.common.req.CourseUserStudySaveREQ;
import com.roncoo.education.course.common.resp.CourseUserStudyListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyViewRESP;
import com.roncoo.education.course.service.dao.CourseUserStudyDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudy;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudyExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudyExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程用户关联
 *
 * @author wujing
 */
@Component
public class PcCourseUserStudyBiz extends BaseBiz {

    @Autowired
    private CourseUserStudyDao dao;

    /**
    * 课程用户关联列表
    *
    * @param courseUserStudyListREQ 课程用户关联分页查询参数
    * @return 课程用户关联分页查询结果
    */
    public Result<Page<CourseUserStudyListRESP>> list(CourseUserStudyListREQ courseUserStudyListREQ) {
        CourseUserStudyExample example = new CourseUserStudyExample();
        Criteria c = example.createCriteria();
        Page<CourseUserStudy> page = dao.listForPage(courseUserStudyListREQ.getPageCurrent(), courseUserStudyListREQ.getPageSize(), example);
        Page<CourseUserStudyListRESP> respPage = PageUtil.transform(page, CourseUserStudyListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课程用户关联添加
    *
    * @param courseUserStudySaveREQ 课程用户关联
    * @return 添加结果
    */
    public Result<String> save(CourseUserStudySaveREQ courseUserStudySaveREQ) {
        CourseUserStudy record = BeanUtil.copyProperties(courseUserStudySaveREQ, CourseUserStudy.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课程用户关联查看
    *
    * @param id 主键ID
    * @return 课程用户关联
    */
    public Result<CourseUserStudyViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseUserStudyViewRESP.class));
    }


    /**
    * 课程用户关联修改
    *
    * @param courseUserStudyEditREQ 课程用户关联修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseUserStudyEditREQ courseUserStudyEditREQ) {
        CourseUserStudy record = BeanUtil.copyProperties(courseUserStudyEditREQ, CourseUserStudy.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课程用户关联删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
