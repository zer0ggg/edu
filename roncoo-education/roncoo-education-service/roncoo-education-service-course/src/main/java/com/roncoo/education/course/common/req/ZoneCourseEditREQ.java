package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区课程关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneCourseEditREQ", description="专区课程关联修改")
public class ZoneCourseEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "主键")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

}
