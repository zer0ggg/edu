package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区课程关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneCourseSaveREQ", description="专区课程关联添加")
public class ZoneCourseSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull(message = "专区编号不能为空")
    @ApiModelProperty(value = "专区编号",required = true)
    private Long zoneId;

    @NotNull(message = "课程ID不能为空")
    @ApiModelProperty(value = "课程ID",required = true)
    private Long courseId;
}
