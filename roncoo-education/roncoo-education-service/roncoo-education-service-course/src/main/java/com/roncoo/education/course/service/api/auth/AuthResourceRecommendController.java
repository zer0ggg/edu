package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.course.service.api.auth.biz.AuthResourceRecommendBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 文库推荐 UserApi接口
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@RestController
@RequestMapping("/course/auth/resourceRecommend")
public class AuthResourceRecommendController {

    @Autowired
    private AuthResourceRecommendBiz biz;

}
