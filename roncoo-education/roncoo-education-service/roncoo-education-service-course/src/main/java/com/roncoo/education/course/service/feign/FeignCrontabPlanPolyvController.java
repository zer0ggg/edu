package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignCrontabPlanPolyv;
import com.roncoo.education.course.feign.qo.CrontabPlanPolyvQO;
import com.roncoo.education.course.feign.vo.CrontabPlanPolyvVO;
import com.roncoo.education.course.service.feign.biz.FeignCrontabPlanPolyvBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 直播转点存定时任务
 *
 * @author wujing
 */
@RestController
public class FeignCrontabPlanPolyvController extends BaseController implements IFeignCrontabPlanPolyv{

	@Autowired
	private FeignCrontabPlanPolyvBiz biz;

	@Override
	public Page<CrontabPlanPolyvVO> listForPage(@RequestBody CrontabPlanPolyvQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody CrontabPlanPolyvQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@RequestBody Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody CrontabPlanPolyvQO qo){
		return biz.updateById(qo);
	}

    @Override
	public CrontabPlanPolyvVO getById(@RequestBody Long id){
		return biz.getById(id);
	}

	@Override
	public void handleScheduledTasks() {
		biz.handleScheduledTasks();
	}

}
