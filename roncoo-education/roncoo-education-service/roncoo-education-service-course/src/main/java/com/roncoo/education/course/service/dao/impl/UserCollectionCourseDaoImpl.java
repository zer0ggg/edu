package com.roncoo.education.course.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.UserCollectionCourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.UserCollectionCourseMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourse;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserCollectionCourseDaoImpl implements UserCollectionCourseDao {
	@Autowired
	private UserCollectionCourseMapper userCollectionCourseMapper;

	@Override
    public int save(UserCollectionCourse record) {
		record.setId(IdWorker.getId());
		return this.userCollectionCourseMapper.insertSelective(record);
	}

	@Override
    public int deleteById(Long id) {
		return this.userCollectionCourseMapper.deleteByPrimaryKey(id);
	}

	@Override
    public int updateById(UserCollectionCourse record) {
		return this.userCollectionCourseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
    public int updateByExampleSelective(UserCollectionCourse record, UserCollectionCourseExample example) {
		return this.userCollectionCourseMapper.updateByExampleSelective(record, example);
	}

	@Override
    public UserCollectionCourse getById(Long id) {
		return this.userCollectionCourseMapper.selectByPrimaryKey(id);
	}

	@Override
    public Page<UserCollectionCourse> listForPage(int pageCurrent, int pageSize, UserCollectionCourseExample example) {
		int count = this.userCollectionCourseMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<UserCollectionCourse>(count, totalPage, pageCurrent, pageSize, this.userCollectionCourseMapper.selectByExample(example));
	}

	@Override
	public UserCollectionCourse getByUserAndCourseId(Long userNo, Long courseId) {
		UserCollectionCourseExample example = new UserCollectionCourseExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		criteria.andCourseIdEqualTo(courseId);
		List<UserCollectionCourse> list = this.userCollectionCourseMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public UserCollectionCourse getByUserAndChapterId(Long userNo, Long chapterId) {
		UserCollectionCourseExample example = new UserCollectionCourseExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		criteria.andChapterIdEqualTo(chapterId);
		List<UserCollectionCourse> list = this.userCollectionCourseMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public UserCollectionCourse getByUserAndPeriodId(Long userNo, Long periodId) {
		UserCollectionCourseExample example = new UserCollectionCourseExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserNoEqualTo(userNo);
		criteria.andPeriodIdEqualTo(periodId);
		List<UserCollectionCourse> list = this.userCollectionCourseMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}
}
