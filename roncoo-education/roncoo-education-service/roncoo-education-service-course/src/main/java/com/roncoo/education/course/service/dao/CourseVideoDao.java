package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample;

import java.util.List;

public interface CourseVideoDao {
    int save(CourseVideo record);

    int deleteById(Long id);

    int updateById(CourseVideo record);

    CourseVideo getById(Long id);

    Page<CourseVideo> listForPage(int pageCurrent, int pageSize, CourseVideoExample example);

    /**
     * 根据视频编号、状态查询课程视频信息集合
     *
     * @param chapterId
     * @param statusId
     * @return
     */
    List<CourseVideo> listByChapterIdAndStatusId(Long chapterId, Integer statusId);

    /**
     * 根据vid更新时长
     *
     * @param courseVideo
     * @return
     */
    Integer updateDurationByVid(CourseVideo courseVideo);

    /**
     * 更加videoVid获取视频信息
     *
     * @param videoVid
     * @return
     */
    CourseVideo getByVid(String videoVid);

    /**
     * 根据视频备份平台和视频备份ID获取课程视频
     *
     * @param videoBackupPlatform 视频备份平台
     * @param videoBackupVid      视频备份ID
     * @return 课程视频
     */
    CourseVideo getByVideoBackupPlatformAndVideoBackupVid(Integer videoBackupPlatform, String videoBackupVid);

    /**
     * 根据点播平台和视频ID获取视频信息
     *
     * @param vodPlatform 点播平台
     * @param videoVid    视频VID
     * @return 课程视频
     */
    CourseVideo getByVodPlatformAndVideoVid(Integer vodPlatform, String videoVid);
}
