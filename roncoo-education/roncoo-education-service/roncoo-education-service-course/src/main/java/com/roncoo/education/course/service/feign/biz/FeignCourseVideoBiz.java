package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.course.feign.qo.CourseVideoQO;
import com.roncoo.education.course.feign.vo.CourseVideoVO;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;

/**
 * 课程视频信息
 *
 * @author wuyun
 */
@Component
public class FeignCourseVideoBiz extends BaseBiz {

	@Autowired
	private IFeignSysConfig feignSysConfig;

	@Autowired
	private CourseChapterPeriodDao courseChapterPeriodDao;
	@Autowired
	private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
	@Autowired
	private CourseVideoDao dao;

	public Page<CourseVideoVO> listForPage(CourseVideoQO qo) {
		CourseVideoExample example = new CourseVideoExample();
		example.setOrderByClause(" id desc ");
		Page<CourseVideo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseVideoVO.class);
	}

	public int save(CourseVideoQO qo) {
		CourseVideo record = BeanUtil.copyProperties(qo, CourseVideo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseVideoVO getById(Long id) {
		CourseVideo record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseVideoVO.class);
	}

	public int updateById(CourseVideoQO qo) {
		CourseVideo record = BeanUtil.copyProperties(qo, CourseVideo.class);
		return dao.updateById(record);
	}

	/**
	 * 定时任务-视频处理
	 *
	 * @param targetFile
	 * @author wuyun
	 */
	public void handleScheduledTasks(File targetFile) {
		Long videoId = Long.valueOf(StrUtil.getPrefix(targetFile.getName()));
		CourseVideo courseVideo = dao.getById(videoId);
		if (ObjectUtil.isNull(courseVideo)) {
			try {
				throw new Exception("找不到视频信息");
			} catch (Exception e) {
				logger.error("定时任务-视频处理失败，原因：", e);
			}
		}
		ConfigPolyvVO sys = feignSysConfig.getPolyv();
		if (ObjectUtil.isNull(sys)) {
			try {
				throw new Exception("找不到系统配置信息");
			} catch (Exception e) {
				logger.error("定时任务-视频处理失败，原因：", e);
			}
		}
		if (StringUtils.isEmpty(sys.getPolyvWritetoken())) {
			try {
				throw new Exception("writetoken没配置");
			} catch (Exception e) {
				logger.error("定时任务-视频处理失败，原因：", e);
			}
		}

		String videoVid = PolyvUtil.sdkUploadFile(targetFile.getName(), courseVideo.getVideoName(), SystemUtil.VIDEO_PATH, VideoTagEnum.COURSE.getCode(), courseVideo.getVideoName(),  sys.getPolyvUseid(), sys.getPolyvSecretkey());
		if (ObjectUtil.isNotNull(videoVid)) {
			if (String.valueOf(IsBackupEnum.YES.getCode()).equals(feignSysConfig.getByConfigKey(SysConfigConstants.IS_BACKUP_ALIYUN).getConfigValue())) {
				// 2、异步上传到阿里云
				String videoOssId = AliyunUtil.uploadVideo(PlatformEnum.COURSE, targetFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
				courseVideo.setVideoOssId(videoOssId);
			}

			// 上传
			courseVideo.setVideoVid(videoVid);
			courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
			dao.updateById(courseVideo);

			// 更新课时审核视频信息
			List<CourseChapterPeriodAudit> periodAuditList = courseChapterPeriodAuditDao.listByVideoId(videoId);
			if (CollectionUtil.isNotEmpty(periodAuditList)) {
				for (CourseChapterPeriodAudit periodAudit : periodAuditList) {
					periodAudit.setVideoVid(videoVid);
					periodAudit.setIsVideo(IsVideoEnum.YES.getCode());
					courseChapterPeriodAuditDao.updateById(periodAudit);
				}
			}

			// 更新课时视频信息
			List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByVideoId(videoId);
			if (CollectionUtil.isNotEmpty(periodList)) {
				for (CourseChapterPeriod period : periodList) {
					period.setVideoVid(videoVid);
					period.setIsVideo(IsVideoEnum.YES.getCode());
					courseChapterPeriodDao.updateById(period);
				}
			}
		}
		// 4、成功删除本地文件
		if (targetFile.isFile() && targetFile.exists()) {
			if (!targetFile.delete()) {
				logger.error("删除本地文件失败");
			}
		}
	}
}
