package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 查找带回课程
 * </p>
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseFindBackRESP", description = "查找带回课程")
public class CourseFindBackRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "讲师用户编码")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "课程封面")
    private String courseLogo;

    @ApiModelProperty(value = "一级分类名")
    private String categoryName1;

    @ApiModelProperty(value = "二级分类名")
    private String categoryName2;

    @ApiModelProperty(value = "三级分类名")
    private String categoryName3;

    @ApiModelProperty(value = "一级分类ID")
    private Long categoryId1;

    @ApiModelProperty(value = "二级分类ID")
    private Long categoryId2;

    @ApiModelProperty(value = "三级分类ID")
    private Long categoryId3;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类")
    private Integer courseCategory;

    @ApiModelProperty(value = "是否免费")
    private Integer isFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal courseDiscount;
}
