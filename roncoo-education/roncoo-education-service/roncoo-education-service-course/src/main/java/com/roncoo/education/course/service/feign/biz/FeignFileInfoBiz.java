package com.roncoo.education.course.service.feign.biz;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.FileTypeEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.FileInfoQO;
import com.roncoo.education.course.feign.vo.FileInfoVO;
import com.roncoo.education.course.feign.vo.UsedSpaceVO;
import com.roncoo.education.course.service.dao.FileInfoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfoExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 文件信息表
 *
 * @author wujing
 */
@Component
public class FeignFileInfoBiz {

	@Autowired
	private FileInfoDao dao;

	public Page<FileInfoVO> listForPage(FileInfoQO qo) {
	    FileInfoExample example = new FileInfoExample();
		Criteria c = example.createCriteria();
		if (StringUtils.hasText(qo.getFileName())) {
			c.andFileNameLike(PageUtil.like(qo.getFileName()));
		}
		if (qo.getFileType() != null) {
			c.andFileTypeEqualTo(qo.getFileType());
		}
	    example.setOrderByClause(" id desc ");
        Page<FileInfo> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, FileInfoVO.class);
	}

	public int save(FileInfoQO qo) {
        FileInfo record = BeanUtil.copyProperties(qo, FileInfo.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public FileInfoVO getById(Long id) {
	    FileInfo record = dao.getById(id);
		return BeanUtil.copyProperties(record, FileInfoVO.class);
	}

	public int updateById(FileInfoQO qo) {
	    FileInfo record = BeanUtil.copyProperties(qo, FileInfo.class);
		return dao.updateById(record);
	}

	public UsedSpaceVO usedSpace() {
		UsedSpaceVO usedSpace = new UsedSpaceVO();
		// 统计所用总空间
		String totalInOrgNo = dao.count();
		usedSpace.setTotalInOrgNo(totalInOrgNo);
		// 统计附件使用总空间
		String totalInAccessory = dao.countByFileType(FileTypeEnum.ACCESSORY.getCode());
		usedSpace.setTotalInAccessory(totalInAccessory);
		// 统计图片总空间
		String totalInPicture = dao.countByFileType(FileTypeEnum.PICTURE.getCode());
		usedSpace.setTotalInPicture(totalInPicture);
		// 统计视频总空间
		String totalInVideo = dao.countByFileType(FileTypeEnum.VIDEO.getCode());
		usedSpace.setTotalInVideo(totalInVideo);
		// 返回下载数
		return usedSpace;
	}

}
