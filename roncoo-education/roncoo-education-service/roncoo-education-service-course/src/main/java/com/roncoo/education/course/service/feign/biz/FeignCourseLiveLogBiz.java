package com.roncoo.education.course.service.feign.biz;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.CourseLiveLogQO;
import com.roncoo.education.course.feign.vo.CourseLiveLogVO;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 直播记录表
 *
 * @author wujing
 */
@Component
public class FeignCourseLiveLogBiz {

	@Autowired
	private CourseLiveLogDao dao;

	public Page<CourseLiveLogVO> listForPage(CourseLiveLogQO qo) {
		CourseLiveLogExample example = new CourseLiveLogExample();
		example.setOrderByClause(" id desc ");
		Page<CourseLiveLog> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseLiveLogVO.class);
	}

	public int save(CourseLiveLogQO qo) {
		CourseLiveLog record = BeanUtil.copyProperties(qo, CourseLiveLog.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseLiveLogVO getById(Long id) {
		CourseLiveLog record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseLiveLogVO.class);
	}

	public int updateById(CourseLiveLogQO qo) {
		CourseLiveLog record = BeanUtil.copyProperties(qo, CourseLiveLog.class);
		return dao.updateById(record);
	}

	public CourseLiveLogVO getByChannelIdAndLiveStatus(CourseLiveLogQO qo) {
		CourseLiveLog record = dao.getByChannelIdAndLiveStatus(qo.getChannelId(), qo.getLiveStatus());
		return BeanUtil.copyProperties(record, CourseLiveLogVO.class);
	}

}
