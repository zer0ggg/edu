package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.Constants;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.AuthCourseAuditListDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseAuditSaveDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseAuditViewDTO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAuditExample.Criteria;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Component
public class AuthCourseAuditBiz extends BaseBiz {

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseChapterAuditDao courseChapterAuditDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseIntroduceAuditDao courseIntroduceAuditDao;
    @Autowired
    private CourseAccessoryDao courseAccessoryDao;

    /**
     * 讲师课程分页接口
     *
     * @param authCourseAuditListBO
     * @return
     * @author wuyun
     */
    public Result<Page<AuthCourseAuditListDTO>> listForPage(AuthCourseAuditListBO authCourseAuditListBO) {
        CourseAuditExample example = new CourseAuditExample();
        Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        c.andCourseCategoryEqualTo(authCourseAuditListBO.getCourseCategory());
        c.andStatusIdLessThan(Constants.FREEZE);
        if (authCourseAuditListBO.getAuditStatus() != null) {
            c.andAuditStatusEqualTo(authCourseAuditListBO.getAuditStatus());
        }
        if (!StringUtils.isEmpty(authCourseAuditListBO.getCourseName())) {
            c.andCourseNameLike(PageUtil.rightLike(authCourseAuditListBO.getCourseName()));
        }
        example.setOrderByClause("audit_status asc, sort asc, id desc");
        Page<CourseAudit> page = courseAuditDao.listForPage(authCourseAuditListBO.getPageCurrent(), authCourseAuditListBO.getPageSize(), example);
        Page<AuthCourseAuditListDTO> dtoPage = PageUtil.transform(page, AuthCourseAuditListDTO.class);
        for (AuthCourseAuditListDTO dto : dtoPage.getList()) {
            // 查找课程关联考试信息
            CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
            courseExamRefQO.setRefId(dto.getId());
            courseExamRefQO.setRefType(RefTypeEnum.COURSE.getCode());
            CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
            if (ObjectUtil.isNotNull(courseExamRefVO)) {
                dto.setExamId(courseExamRefVO.getExamId());
                dto.setCourseExamRefId(courseExamRefVO.getId());
                dto.setExamName(courseExamRefVO.getExamName());
            }

            // 课程类型为文库时返回章节ID
            if (CourseCategoryEnum.RESOURCES.getCode().equals(dto.getCourseCategory())) {
                List<CourseChapterAudit> chapterAuditList = courseChapterAuditDao.listByCourseId(dto.getId());
                if (ObjectUtil.isNotNull(chapterAuditList) && StatusIdEnum.YES.getCode().equals(chapterAuditList.get(0).getStatusId())) {
                    dto.setChapterId(chapterAuditList.get(0).getId());
                }
            }
            // 校验如果正式表存在不可直接删除
            Course course = courseDao.getById(dto.getId());
            if (ObjectUtil.isNull(course)) {
                dto.setIsDelete(IsDeleteEnum.YES.getCode());
            } else {
                dto.setIsDelete(IsDeleteEnum.NO.getCode());
            }
        }
        return Result.success(dtoPage);
    }

    /**
     * 讲师课程详情接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<AuthCourseAuditViewDTO> view(AuthCourseAuditViewBO bo) {
        if (StringUtils.isEmpty(bo.getId())) {
            return Result.error("课程id不能为空");
        }
        // 查询课程信息
        CourseAudit courseAudit = courseAuditDao.getById(bo.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到该课程信息");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("找不到该课程信息");
        }
        AuthCourseAuditViewDTO dto = BeanUtil.copyProperties(courseAudit, AuthCourseAuditViewDTO.class);
        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            // 课程介绍
            CourseIntroduceAudit courseIntroduceAudit = courseIntroduceAuditDao.getById(courseAudit.getIntroduceId());
            dto.setIntroduce(courseIntroduceAudit.getIntroduce());
        }
        return Result.success(dto);
    }

    /**
     * 讲师课程保存接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthCourseAuditSaveDTO> save(AuthCourseAuditSaveBO bo) {
        //校验敏感词
        /*SensitiveUtil.init(feignSensitiveWordLibrary.listAll());
        String introduce = SensitiveUtil.getFindedFirstSensitive(bo.getIntroduce());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(introduce)) {
            return Result.error("存在敏感词{" + introduce + "}");
        }
        String courseName = SensitiveUtil.getFindedFirstSensitive(bo.getCourseName());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(courseName)) {
            return Result.error("存在敏感词{" + courseName + "}");
        }*/

        // 判断是否是讲师
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("请先申请成为讲师");
        }
        if (CourseCategoryEnum.LIVE.getCode().equals(bo.getCourseCategory())) {
            if (IsLiveEnum.NO.getCode().equals(lecturerVO.getIsLive())) {
                return Result.error("请先申请直播功能");
            }
        }

        // 原价为0，课程也是免费
        if (bo.getCourseOriginal().equals(BigDecimal.ZERO) || StringUtils.isEmpty(bo.getCourseOriginal())) {
            bo.setIsFree(IsFreeEnum.FREE.getCode());
        }

        if (IsFreeEnum.FREE.getCode().equals(bo.getIsFree())) {
            // 课程免费就设置价格为0(原价、优惠价)
            bo.setCourseOriginal(BigDecimal.valueOf(0));
            bo.setCourseDiscount(BigDecimal.valueOf(0));
        } else {
            // 课程收费但价格为空
            if (bo.getCourseOriginal() == null) {
                return Result.error("请输入价格");
            }
            if (bo.getCourseDiscount() == null) {
                bo.setCourseDiscount(BigDecimal.valueOf(0));
            }
            // 原价小于0
            if (bo.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) < 0 || bo.getCourseDiscount().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("售价不能为负数");
            }
            if (bo.getCourseDiscount().compareTo(bo.getCourseOriginal()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }
        // 课程
        CourseAudit record = BeanUtil.copyProperties(bo, CourseAudit.class);
        // 课程介绍
        if (StringUtils.hasText(bo.getIntroduce())) {
            CourseIntroduceAudit courseIntroduceAudit = new CourseIntroduceAudit();
            courseIntroduceAudit.setIntroduce(bo.getIntroduce());
            courseIntroduceAuditDao.save(courseIntroduceAudit);

            record.setIntroduceId(courseIntroduceAudit.getId());

        }
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setIsPutaway(IsPutawayEnum.YES.getCode());
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        record.setLecturerUserNo(ThreadContext.userNo());
        record.setId(IdWorker.getId());

        // 成功返回参数
        AuthCourseAuditSaveDTO dto = BeanUtil.copyProperties(record, AuthCourseAuditSaveDTO.class);
        if (CourseCategoryEnum.RESOURCES.getCode().equals(bo.getCourseCategory())) {
            // 课程分类为文库默认添加一个章节
            CourseChapterAudit courseChapterAudit = new CourseChapterAudit();
            courseChapterAudit.setCourseId(record.getId());
            courseChapterAudit.setChapterName("第一章");
            courseChapterAudit.setChapterOriginal(BigDecimal.valueOf(0));
            courseChapterAudit.setChapterDiscount(BigDecimal.valueOf(0));
            courseChapterAudit.setId(IdWorker.getId());
            courseChapterAuditDao.save(courseChapterAudit);
            dto.setChapterId(courseChapterAudit.getId());
        }

        // 查询更新后的课程审核信息
        if (courseAuditDao.save(record) > 0) {
            if (CollectionUtils.isNotEmpty(bo.getAccessoryList())) {
                // 添加附件信息
                for (AuthCourseAccessoryBO authAccessoryBO : bo.getAccessoryList()) {
                    CourseAccessory courseAccessory = new CourseAccessory();
                    courseAccessory.setAcName(authAccessoryBO.getAcName());
                    courseAccessory.setAcUrl(authAccessoryBO.getAcUrl());
                    courseAccessory.setCourseCategory(bo.getCourseCategory());
                    courseAccessory.setRefId(record.getId());
                    courseAccessory.setRefName(record.getCourseName());
                    courseAccessory.setRefType(RefTypeEnum.COURSE.getCode());
                    courseAccessoryDao.save(courseAccessory);
                }
            }
            return Result.success(dto);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }

    /**
     * 讲师课程更新接口
     *
     * @param authCourseAuditUpdateBO
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> update(AuthCourseAuditUpdateBO authCourseAuditUpdateBO) {
        if (authCourseAuditUpdateBO.getId() == null) {
            return Result.error("课程ID不能为空");
        }

        //校验敏感词
        /*SensitiveUtil.init(feignSensitiveWordLibrary.listAll());
        String introduce = SensitiveUtil.getFindedFirstSensitive(authCourseAuditUpdateBO.getIntroduce());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(introduce)) {
            return Result.error("存在敏感词{" + introduce + "}");
        }
        String courseName = SensitiveUtil.getFindedFirstSensitive(authCourseAuditUpdateBO.getCourseName());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(courseName)) {
            return Result.error("存在敏感词{" + courseName + "}");
        }*/

        // 原价为0，课程也是免费
        if (authCourseAuditUpdateBO.getCourseOriginal().equals(BigDecimal.ZERO) || StringUtils.isEmpty(authCourseAuditUpdateBO.getCourseOriginal())) {
            authCourseAuditUpdateBO.setIsFree(IsFreeEnum.FREE.getCode());
        }

        if (IsFreeEnum.FREE.getCode().equals(authCourseAuditUpdateBO.getIsFree())) {
            // 课程免费就设置价格为0(原价、优惠价)
            authCourseAuditUpdateBO.setCourseOriginal(BigDecimal.valueOf(0));
            authCourseAuditUpdateBO.setCourseDiscount(BigDecimal.valueOf(0));
        } else {
            // 课程收费但价格为空
            if (authCourseAuditUpdateBO.getCourseOriginal() == null) {
                return Result.error("请输入价格");
            }
            if (authCourseAuditUpdateBO.getCourseDiscount() == null) {
                authCourseAuditUpdateBO.setCourseDiscount(BigDecimal.valueOf(0));
            }
            // 原价小于0
            if (authCourseAuditUpdateBO.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) < 0 || authCourseAuditUpdateBO.getCourseDiscount().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("售价不能为负数");
            }
            if (authCourseAuditUpdateBO.getCourseDiscount().compareTo(authCourseAuditUpdateBO.getCourseOriginal()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }

        CourseAudit courseAudit = courseAuditDao.getById(authCourseAuditUpdateBO.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }

        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            // 课程简介更新
            CourseIntroduceAudit courseIntroduceAudit = new CourseIntroduceAudit();
            courseIntroduceAudit.setId(courseAudit.getIntroduceId());
            courseIntroduceAudit.setIntroduce(authCourseAuditUpdateBO.getIntroduce());
            courseIntroduceAuditDao.updateById(courseIntroduceAudit);
        }

        // 课程更新
        CourseAudit record = BeanUtil.copyProperties(authCourseAuditUpdateBO, CourseAudit.class);
        if (authCourseAuditUpdateBO.getCategoryId1() == null) {
            record.setCategoryId1(0L);
        }
        if (authCourseAuditUpdateBO.getCategoryId2() == null) {
            record.setCategoryId2(0L);
        }
        if (authCourseAuditUpdateBO.getCategoryId3() == null) {
            record.setCategoryId3(0L);
        }
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        record.setAuditOpinion("");
        if (courseAuditDao.updateById(record) > 0) {
            if (CollectionUtils.isNotEmpty(authCourseAuditUpdateBO.getAccessoryList())) {

                // 查找该文库区课程的附件信息
                List<CourseAccessory> courseAccessoryList = courseAccessoryDao.listByRefIdAndStatusId(courseAudit.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isNotEmpty(courseAccessoryList)) {
                    for (CourseAccessory courseAccessory : courseAccessoryList) {
                        // 删除原有的附件信息
                        courseAccessoryDao.deleteById(courseAccessory.getId());
                    }
                }
                // 添加附件信息
                for (AuthCourseAccessoryBO authCourseAccessoryBO : authCourseAuditUpdateBO.getAccessoryList()) {
                    CourseAccessory courseAccessory = new CourseAccessory();
                    courseAccessory.setAcName(authCourseAccessoryBO.getAcName());
                    courseAccessory.setAcUrl(authCourseAccessoryBO.getAcUrl());
                    courseAccessory.setCourseCategory(courseAudit.getCourseCategory());
                    courseAccessory.setRefId(courseAudit.getId());
                    courseAccessory.setRefType(RefTypeEnum.COURSE.getCode());
                    courseAccessoryDao.save(courseAccessory);
                }
            }
            return Result.success(1);
        }
        return Result.error(ResultEnum.COURSE_UPDATE_FAIL);
    }

    /**
     * 讲师课程上下架接口
     *
     * @param authCourseAuditStandBO
     * @return
     * @author wuyun
     */
    public Result<Integer> stand(AuthCourseAuditStandBO authCourseAuditStandBO) {
        if (StringUtils.isEmpty(authCourseAuditStandBO.getId())) {
            return Result.error("课程ID不能为空");
        }
        CourseAudit courseAudit = courseAuditDao.getById(authCourseAuditStandBO.getId());
        if (courseAudit == null) {
            return Result.error("id不正确");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("传入的useNo与该课程的讲师useNo不一致");
        }
        CourseAudit record = BeanUtil.copyProperties(authCourseAuditStandBO, CourseAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        int result = courseAuditDao.updateById(record);
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_UPDATE_FAIL);
    }

    /**
     * 讲师课程删除接口
     *
     * @param authCourseAuditDeleteBO
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> delete(AuthCourseAuditDeleteBO authCourseAuditDeleteBO) {
        if (authCourseAuditDeleteBO.getId() == null) {
            return Result.error("id不能为空");
        }
        CourseAudit courseAudit = courseAuditDao.getById(authCourseAuditDeleteBO.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("传入的useNo与该课程的讲师useNo不一致");
        }
        // 查找课程，存在则不删除
        Course course = courseDao.getByCourseIdAndStatusId(courseAudit.getId(), StatusIdEnum.YES.getCode());
        if (ObjectUtil.isNotNull(course)) {
            return Result.error("该课程已审核，无法删除");
        }
        // 删除章节
        courseChapterAuditDao.deleteByCourseId(courseAudit.getId());
        // 删除课时
        courseChapterPeriodAuditDao.deleteByCourseId(courseAudit.getId());
        return Result.success(courseAuditDao.deleteById(courseAudit.getId()));
    }
}
