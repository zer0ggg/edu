/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.common.polyv.live.result.PLChannelGetResult;
import com.roncoo.education.common.polyv.live.result.PLChannelWatchAuthResult;
import com.roncoo.education.course.common.req.LiveCallbackREQ;
import com.roncoo.education.course.feign.qo.*;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 保利威视回调-直播
 *
 * @author kyh
 */
@Component
@Slf4j
public class FeignCallbackPolyvLiveBiz extends BaseBiz {

    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private CourseDao coursedao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseLiveLogDao courseLiveLogDao;
    @Autowired
    private CourseUserStudyLogDao courseUserStudyLogDao;
    @Autowired
    private CrontabPlanPolyvDao crontabPlanPolyvDao;
    @Autowired
    private CourseVideoDao courseVideoDao;
    @Autowired
    private FileInfoDao fileInfoDao;
    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    /**
     * 保利威视，直播状态回调接口
     *
     * @param polyvLiveStatusQO 回调参数
     * @return 校验结果
     */
    @Transactional(rollbackFor = Exception.class)
    public String callbackPolyvLiveStatus(PolyvLiveStatusQO polyvLiveStatusQO) {
        logger.warn("直播状态回调结果:{}", polyvLiveStatusQO.toString());
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return "fail";
        }

        //防止出现网络抖动，保利威回调end,查询频道直播状态，如果为live则不处理
        String getLiveStatus = PLChannelUtil.getLiveStatus(configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret(), polyvLiveStatusQO.getChannelId());
        if (PolyvLiveStatusEnum.END.getCode().equals(polyvLiveStatusQO.getStatus()) && PolyvLiveStatusEnum.NOW.getCode().equals(getLiveStatus)) {
            logger.warn("直播出现网络抖动或者网络问题，{}---{}", polyvLiveStatusQO.getStatus(), getLiveStatus);
            return "fail";
        }

        // 直播记录判断
        CourseLiveLog courseLiveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.POLYV.getCode(), polyvLiveStatusQO.getChannelId());
        if (ObjectUtil.isNull(courseLiveLog)) {
            logger.error("保利威视，直播状态回调失败 ,找不到直播记录信息");
            return "fail";
        }
        if (PolyvLiveStatusEnum.NOW.getCode().equals(polyvLiveStatusQO.getStatus()) && !LiveStatusEnum.NOT.getCode().equals(courseLiveLog.getLiveStatus())) {
            logger.error("保利威视直播状态--开播回调，没有找到未开播记录");
            return "fail";
        }
        if (PolyvLiveStatusEnum.END.getCode().equals(polyvLiveStatusQO.getStatus()) && !LiveStatusEnum.NOW.getCode().equals(courseLiveLog.getLiveStatus())) {
            logger.error("保利威视直播状态--直播结束回调，没有找到直播中记录");
            return "fail";
        }

        CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriodAudit)) {
            logger.error("保利威视，直播状态回调失败 ,找不到课时审核信息");
            return "fail";
        }
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod)) {
            logger.error("保利威视，直播状态回调失败 ,找不到课时信息");
            return "fail";
        }

        if (PolyvLiveStatusEnum.NOW.getCode().equals(polyvLiveStatusQO.getStatus())) {
            courseLiveLog.setBeginTime(new Date());
            // 直播记录正在直播状态
            courseLiveLog.setLiveStatus(LiveStatusEnum.NOW.getCode());
            courseChapterPeriod.setLiveStatus(LiveStatusEnum.NOW.getCode());
            courseChapterPeriodAudit.setLiveStatus(LiveStatusEnum.NOW.getCode());
        } else if (PolyvLiveStatusEnum.END.getCode().equals(polyvLiveStatusQO.getStatus())) {
            // 更改状态为待生成回放
            courseLiveLog.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
            courseLiveLog.setEndTime(new Date());
            courseChapterPeriod.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
            courseChapterPeriodAudit.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
        }

        //加入场次id
        courseLiveLog.setSessionId(polyvLiveStatusQO.getSessionId());
        courseLiveLogDao.updateById(courseLiveLog);
        courseChapterPeriodDao.updateById(courseChapterPeriod);
        courseChapterPeriodAuditDao.updateById(courseChapterPeriodAudit);

        // 清除聊天室记录
        PLChannelUtil.cleanChat(polyvLiveStatusQO.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        return "success";
    }

    /**
     * 保利威视，视频授权播放接口
     *
     * @param polyvLiveAuthQO 回调参数
     * @return 校验结果
     */
    @Transactional(rollbackFor = Exception.class)
    public String callbackPolyvLiveAuth(PolyvLiveAuthQO polyvLiveAuthQO) {
        logger.warn("视频授权播放结果:{}", polyvLiveAuthQO.toString());
        Map<String, Object> map = new HashMap<>();

        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            map.put("status", 0);
            map.put("errorUrl", "");
            logger.error("保利威配置错误");
            return JSUtil.toJSONString(map);
        }

        // 根据用户ID校验缓存是否存在
        if (!myRedisTemplate.hasKey(RedisPreEnum.LIVE_CHANNEL.getCode() + polyvLiveAuthQO.getUserid())) {
            map.put("status", 0);
            map.put("errorUrl", "");
            logger.error("userid值为空");
            return JSUtil.toJSONString(map);
        }

        // 从redis取频道号、课时信息
        String json = myRedisTemplate.get(RedisPreEnum.LIVE_CHANNEL.getCode() + polyvLiveAuthQO.getUserid());
        logger.error("从redis取频道号={}", json);

        // json = json.substring(1).substring(0, json.length() - 2);
        LiveCallbackREQ liveCallbackREQ = JSON.parseObject(json, LiveCallbackREQ.class);
        if (ObjectUtil.isEmpty(liveCallbackREQ)) {
            map.put("status", 0);
            map.put("errorUrl", "");
            logger.error("获取课时等信息出错!");
            return JSUtil.toJSONString(map);
        }

        // 根据key删除redis缓存
        myRedisTemplate.delete(RedisPreEnum.LIVE_CHANNEL.getCode() + polyvLiveAuthQO.getUserid());

        // 根据频道号获取频道观看条件
        PLChannelWatchAuthResult result = PLChannelUtil.watchAuthChannel(liveCallbackREQ.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(result)) {
            logger.error("找不到频道观看条件");
            return "fail";
        }
        // sign与token校验不通过
        String sign = PLChannelUtil.getSign(result.getExternalKey(), polyvLiveAuthQO.getUserid(), polyvLiveAuthQO.getTs());
        if (!sign.equals(polyvLiveAuthQO.getToken())) {
            map.put("status", 0);
            // 返回错误跳转的域名
            map.put("errorUrl", "");
            logger.error("sign与token校验不通过");
            return JSUtil.toJSONString(map);
        }

        UserExtVO userExtVO = feignUserExt.getByUserNo(Long.valueOf(polyvLiveAuthQO.getUserid()));
        if (ObjectUtil.isNull(userExtVO) || StatusIdEnum.NO.getCode().equals(userExtVO.getStatusId())) {
            map.put("status", 0);
            // 返回错误跳转的域名
            map.put("errorUrl", "");
            logger.error("用户异常异常");
            return JSUtil.toJSONString(map);
        }

        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(liveCallbackREQ.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod) || StatusIdEnum.NO.getCode().equals(courseChapterPeriod.getStatusId())) {
            map.put("status", 0);
            // 返回错误跳转的域名
            map.put("errorUrl", "");
            logger.error("找不到课时信息");
            return JSUtil.toJSONString(map);
        }

        // 章节校验
        CourseChapter chapterInfo = courseChapterDao.getById(courseChapterPeriod.getChapterId());
        if (ObjectUtil.isNull(chapterInfo) || StatusIdEnum.NO.getCode().equals(chapterInfo.getStatusId())) {
            map.put("status", 0);
            map.put("errorUrl", "");// 返回错误跳转的域名
            logger.error("找不到课时信息");
            return JSUtil.toJSONString(map);
        }

        Course course = coursedao.getById(courseChapterPeriod.getCourseId());
        if (ObjectUtil.isNull(course) || StatusIdEnum.NO.getCode().equals(course.getStatusId()) || !CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            map.put("status", 0);
            map.put("errorUrl", "");// 返回错误跳转的域名
            logger.error("课程异常");
            return JSUtil.toJSONString(map);
        }

        // 课程、章节、课时任一个为免费该课时都可以免费观看
        if (IsFreeEnum.FREE.getCode().equals(courseChapterPeriod.getIsFree()) || IsFreeEnum.FREE.getCode().equals(chapterInfo.getIsFree()) || IsFreeEnum.FREE.getCode().equals(course.getIsFree())) {
            // 免费课时，可以播放
            map.put("status", 1);
            map.put("errorUrl", "");// 返回错误跳转的域名
            logger.error("该课时可以免费观看");
            return JSUtil.toJSONString(map);
        }

        // 收费课程
        // 是否购买了该课程
        Integer isPay;
        UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(userExtVO.getUserNo(), courseChapterPeriod.getCourseId());
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            // 未购买或者没支付情况
            isPay = IsPayEnum.NO.getCode();
        } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
            // 已过期情况
            isPay = IsPayEnum.NO.getCode();
        } else {
            // 订单状态为已支付
            isPay = IsPayEnum.YES.getCode();
        }
        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), course.getCourseDiscount())) {
            isPay = IsPayEnum.YES.getCode();
        }

        if (isPay.equals(IsPayEnum.NO.getCode())) {
            map.put("status", 0);
            map.put("errorUrl", "");// 返回错误跳转的域名
            logger.error("该课时可以免费观看");
            return JSUtil.toJSONString(map);
        }
        // 更新课时学生人数
        courseChapterPeriod.setCountStudy(courseChapterPeriod.getCountStudy() + 1);
        courseChapterPeriodDao.updateById(courseChapterPeriod);

        // 更新课程学生人数
        course.setCountStudy(course.getCountStudy() + 1);
        coursedao.updateById(course);

        CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(liveCallbackREQ.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriodAudit) || StatusIdEnum.NO.getCode().equals(courseChapterPeriodAudit.getStatusId())) {
            map.put("status", 0);
            map.put("errorUrl", "");// 返回错误跳转的域名
            logger.error("找不到审核课时信息");
            return JSUtil.toJSONString(map);
        }

        // 更新课时学生人数
        courseChapterPeriodAudit.setCountStudy(courseChapterPeriodAudit.getCountStudy() + 1);
        courseChapterPeriodAuditDao.updateById(courseChapterPeriodAudit);

        // 记录课程学习日志
        CourseUserStudyLog courseUserStudyLog = new CourseUserStudyLog();
        courseUserStudyLog.setCourseId(course.getId());
        courseUserStudyLog.setCourseName(course.getCourseName());
        courseUserStudyLog.setChapterId(chapterInfo.getId());
        courseUserStudyLog.setChapterName(chapterInfo.getChapterName());
        courseUserStudyLog.setPeriodId(courseChapterPeriod.getId());
        courseUserStudyLog.setPeriodName(courseChapterPeriod.getPeriodName());
        courseUserStudyLog.setUserNo(Long.valueOf(polyvLiveAuthQO.getUserid()));
        courseUserStudyLog.setCourseCategory(CourseCategoryEnum.LIVE.getCode());
        courseUserStudyLogDao.save(courseUserStudyLog);

        map.put("status", 1);
        map.put("userid", userExtVO.getUserNo().toString());
        if (StringUtils.isEmpty(userExtVO.getNickname())) {
            map.put("nickname", userExtVO.getMobile().substring(0, 3) + "***" + userExtVO.getMobile().substring(7, 11));
        } else {
            map.put("nickname", userExtVO.getNickname());
        }
        map.put("avatar", userExtVO.getHeadImgUrl());
        return JSUtil.toJSONString(map);

    }

    /**
     * 保利威视，回放生成回调通知接口
     */
    @Transactional(rollbackFor = Exception.class)
    public String callbackLiveFileUrl(PolyvLiveFileUrlQO polyvLiveFileUrlQO) {
        logger.warn("回放生成回调结果:{}", polyvLiveFileUrlQO.toString());

        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return "fail";
        }

        PLChannelGetResult plChannelGetResult = PLChannelUtil.getChannel(polyvLiveFileUrlQO.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isEmpty(plChannelGetResult)) {
            logger.error("该频道号不存在或者被删除，频道号={}", polyvLiveFileUrlQO.getChannelId());
            return "fail";
        }

        //如果为三分屏则不处理，在重制课件处理
        /*if (LiveSceneEnum.PPT.getCode().equals(pLChannelGetResult.getScene())) {
            return "success";
        }*/

        // 1.根据频道号和状态查找记录中当前状态的最后一条
        CourseLiveLog courseLiveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.POLYV.getCode(), polyvLiveFileUrlQO.getChannelId());
        if (ObjectUtil.isNull(courseLiveLog)) {
            logger.error("找不到频道开播记录");
            return "fail";
        }

        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(periodAudit)) {
            logger.error("找不到课时审核信息");
            return "fail";
        }
        periodAudit.setPlayback(polyvLiveFileUrlQO.getFileUrl());
        periodAudit.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        courseChapterPeriodAuditDao.updateById(periodAudit);

        CourseChapterPeriod period = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(period)) {
            logger.error("找不到课时信息");
            return "fail";
        }
        period.setPlayback(polyvLiveFileUrlQO.getFileUrl());
        period.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        courseChapterPeriodDao.updateById(period);

        // 更改状态为待转存
        courseLiveLog.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        courseLiveLog.setFileUrl(polyvLiveFileUrlQO.getFileUrl());
        courseLiveLogDao.updateById(courseLiveLog);

        // 开启了阿里云备份，且是普通直播才备份----三分屏需要重制课件后才备份
        String value = feignSysConfig.getByConfigKey(SysConfigConstants.POLYV_LIVE_BACKUP).getConfigValue();
        if ("true".equals(value) && LiveSceneEnum.ALONE.getCode().equals(plChannelGetResult.getScene())) {
            uploadOSS(polyvLiveFileUrlQO.getFileUrl(), period.getPeriodName(), courseLiveLog);
        }

        // 转存成点播
        String convertResult = PLChannelUtil.listConvert(configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret(), polyvLiveFileUrlQO.getChannelId(), polyvLiveFileUrlQO.getFileId(), period.getPeriodName());
        if ("success".equals(convertResult)) {
            return "success";
        }

        // 失败存入数据库，交给定时任务处理-调用保利威视转存接口（限制10分钟内只能调用一次）
        CrontabPlanPolyv crontabPlanPolyv = new CrontabPlanPolyv();
        crontabPlanPolyv.setChannelId(polyvLiveFileUrlQO.getChannelId());
        crontabPlanPolyv.setPlayback(polyvLiveFileUrlQO.getFileUrl());
        crontabPlanPolyv.setPeriodName(period.getPeriodName());
        crontabPlanPolyv.setSessionId(courseLiveLog.getSessionId());
        // crontabPlanPolyv.setPolyvCategoryId(SystemUtil.POLYV_CATEGORY_ID);// 转存的目录id
        int result = crontabPlanPolyvDao.save(crontabPlanPolyv);
        if (result > 0) {
            return "success";
        }
        logger.error("保利威视，回放生成回调失败");
        return "fail";
    }

    public void uploadOSS(String fileUrl, String fileName, CourseLiveLog courseLiveLog) {
        if (StringUtils.isNotEmpty(fileUrl)) {
            CALLBACK_EXECUTOR.execute(() -> {
                //上传阿里云
                String videoOssId = AliyunUtil.uploadVideo(PlatformEnum.COURSE, fileUrl, fileName, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
                if (StringUtils.isEmpty(videoOssId)) {
                    logger.warn("上传阿里云备份失败!");
                } else {
                    courseLiveLog.setVideoOssId(videoOssId);
                    courseLiveLogDao.updateById(courseLiveLog);
                }
            });
        } else {
            logger.error("重制课程回调处理失败！");
        }
    }

    /**
     * 保利威视，转存成功回调通知接口
     */
    @Transactional(rollbackFor = Exception.class)
    public String callbackLiveSave(PolyvLiveSaveQO polyvLiveSaveQO) {
        logger.warn("转存成功回调结果:{}", polyvLiveSaveQO.toString());

        //根据频道号和场次id获取直播记录
        CourseLiveLog courseLiveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.POLYV.getCode(), polyvLiveSaveQO.getChannelId());

        Long vId = IdWorker.getId();
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
        courseVideo.setChapterId(courseLiveLog.getChapterId());
        courseVideo.setCourseId(courseLiveLog.getCourseId());
        courseVideo.setId(vId);
        courseVideo.setStatusId(StatusIdEnum.YES.getCode());
        courseVideo.setVideoLength(polyvLiveSaveQO.getDuration());
        courseVideo.setVideoName(polyvLiveSaveQO.getTitle());
        courseVideo.setVideoVid(polyvLiveSaveQO.getVid());
        courseVideoDao.save(courseVideo);

        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(courseLiveLog.getPeriodId());
        periodAudit.setVideoId(vId);
        periodAudit.setVideoVid(polyvLiveSaveQO.getVid());
        periodAudit.setVideoName(polyvLiveSaveQO.getTitle());
        periodAudit.setVideoLength(polyvLiveSaveQO.getDuration());
        periodAudit.setLiveStatus(LiveStatusEnum.END.getCode());
        //直播存放的id
        periodAudit.setLiveVid(polyvLiveSaveQO.getVideoId());
        courseChapterPeriodAuditDao.updateById(periodAudit);

        CourseChapterPeriod period = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        period.setVideoId(vId);
        period.setVideoVid(polyvLiveSaveQO.getVid());
        period.setVideoName(polyvLiveSaveQO.getTitle());
        period.setVideoLength(polyvLiveSaveQO.getDuration());
        period.setLiveStatus(LiveStatusEnum.END.getCode());
        //直播存放的id
        period.setLiveVid(polyvLiveSaveQO.getVideoId());
        courseChapterPeriodDao.updateById(period);

        // 更改状态为结束
        courseLiveLog.setLiveStatus(LiveStatusEnum.END.getCode());
        int result = courseLiveLogDao.updateById(courseLiveLog);
        if (result > 0) {
            return "success";
        } else {
            logger.error("保利威视，转存成功回调失败");
            return "fail";
        }
    }

    /**
     * 1、获取回课时信息
     * 2、根据url下载视频到本地
     * 3、读取本地视频上传到保利威
     * 4、更新课时vid
     * 5、删除本地视频
     */
    public String callbackLiveRemakes(PolyvLiveRemakesQO polyvLiveRemakesQO) {
        logger.warn("重制课件成功回调结果:{}", polyvLiveRemakesQO.toString());
        //获取直播记录
        CourseLiveLog courseLiveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.POLYV.getCode(), polyvLiveRemakesQO.getChannelId());
        //获取审核课时信息
        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(courseLiveLog.getPeriodId());
        //获取课时信息
        CourseChapterPeriod period = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());

        // 获取系统配置信息
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return "fail";
        }

        CourseVideo courseVideo = courseVideoDao.getByVid(period.getVideoVid());
        if (ObjectUtil.isEmpty(courseVideo)) {
            logger.error("获取不到原转存视频信息! vid={}", period.getVideoVid());
        }

        //先删除转存的不完整视频
        String deleteFile = PolyvUtil.deleteFile(period.getVideoVid(), configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvSecretkey());
        if (StringUtils.isEmpty(deleteFile)) {
            logger.error("重制课件删除原视频失败!");
        }

        //先备份阿里云
        String value = feignSysConfig.getByConfigKey(SysConfigConstants.POLYV_LIVE_BACKUP).getConfigValue();
        if ("true".equals(value)) {
            uploadOSS(polyvLiveRemakesQO.getUrl(), period.getPeriodName(), courseLiveLog);
        }
        //上传保利威转存成点播
        uploadPolyv(polyvLiveRemakesQO, periodAudit, period, courseVideo, configPolyvVO);
        return "success";
    }

    private void uploadPolyv(PolyvLiveRemakesQO polyvLiveRemakesQO, CourseChapterPeriodAudit periodAudit, CourseChapterPeriod period, CourseVideo courseVideo, ConfigPolyvVO configPolyvVO) {
        String fileName = DownloadUtil.download(polyvLiveRemakesQO.getUrl(), SystemUtil.POLYV_CALLBACK_REMAKES);
        File file = new File(SystemUtil.POLYV_CALLBACK_REMAKES + fileName);

        if (StringUtils.isNotEmpty(fileName)) {
            CALLBACK_EXECUTOR.execute(() -> {
                //上传保利威
                String videoVid = PolyvUtil.sdkUploadFile(file.getName(), periodAudit.getPeriodName(), SystemUtil.POLYV_CALLBACK_REMAKES, VideoTagEnum.LIVE_PLAYBACK.getCode(), null, configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvSecretkey());
                if (videoVid == null) {
                    // 上传异常，不再进行处理，定时任务会继续进行处理
                    logger.warn("重制课件处理，上传异常，不再进行处理，定时任务会继续进行处理，场次id={}", polyvLiveRemakesQO.getSessionId());
                    return;
                }
                courseVideo.setVideoVid(videoVid);
                courseVideoDao.updateById(courseVideo);
                periodAudit.setVideoVid(videoVid);
                periodAudit.setLiveStatus(LiveStatusEnum.END.getCode());
                courseChapterPeriodAuditDao.updateById(periodAudit);
                period.setVideoVid(videoVid);
                period.setLiveStatus(LiveStatusEnum.END.getCode());
                courseChapterPeriodDao.updateById(period);


                // 记录文件信息
                FileInfo fileInfo = new FileInfo();
                fileInfo.setFileName(period.getVideoName());
                fileInfo.setFileType(FileTypeEnum.VIDEO.getCode());
                fileInfo.setFileSize(file.length());
                fileInfoDao.save(fileInfo);

                // 4、成功删除本地文件
                if (file.isFile() && file.exists()) {
                    if (!file.delete()) {
                        logger.error("删除本地文件失败");
                    }
                }
            });
        } else {
            logger.error("重制课程回调处理失败！");
        }
    }

}
