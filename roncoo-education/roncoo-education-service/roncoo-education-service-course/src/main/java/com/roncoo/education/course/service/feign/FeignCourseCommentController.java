package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignCourseComment;
import com.roncoo.education.course.feign.qo.CourseCommentQO;
import com.roncoo.education.course.feign.vo.CourseCommentVO;
import com.roncoo.education.course.service.feign.biz.FeignCourseCommentBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程评论
 *
 * @author Quanf
 * @date 2020-09-03
 */
@RestController
public class FeignCourseCommentController extends BaseController implements IFeignCourseComment{

    @Autowired
    private FeignCourseCommentBiz biz;

	@Override
	public Page<CourseCommentVO> listForPage(@RequestBody CourseCommentQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseCommentQO qo) {
	    return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	    return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseCommentQO qo) {
	    return biz.updateById(qo);
	}

	@Override
	public CourseCommentVO getById(@PathVariable(value = "id") Long id) {
	    return biz.getById(id);
	}
}
