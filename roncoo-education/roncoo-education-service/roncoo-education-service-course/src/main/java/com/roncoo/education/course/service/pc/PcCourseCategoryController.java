package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseCategoryChooseRESP;
import com.roncoo.education.course.common.resp.CourseCategoryListRESP;
import com.roncoo.education.course.common.resp.CourseCategoryViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseCategoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程分类 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/category")
@Api(value = "course-课程分类", tags = {"course-课程分类"})
public class PcCourseCategoryController {

    @Autowired
    private PcCourseCategoryBiz biz;

    @ApiOperation(value = "课程分类列表", notes = "课程分类列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseCategoryListRESP>> list(@RequestBody CourseCategoryListREQ courseCategoryListREQ) {
        return biz.list(courseCategoryListREQ);
    }



    @ApiOperation(value = "课程分类添加", notes = "课程分类添加")
    @SysLog(value = "课程分类添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseCategorySaveREQ courseCategorySaveREQ) {
        return biz.save(courseCategorySaveREQ);
    }

    @ApiOperation(value = "课程分类查看", notes = "课程分类查看")
    @GetMapping(value = "/view")
    public Result<CourseCategoryViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "课程分类修改", notes = "课程分类修改")
    @SysLog(value = "课程分类修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseCategoryEditREQ courseCategoryEditREQ) {
        return biz.edit(courseCategoryEditREQ);
    }


    @ApiOperation(value = "课程分类修改状态", notes = "课程分类修改状态")
    @SysLog(value = "课程分类修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatusId(@RequestBody CourseCategoryUpdateStatusREQ courseCategoryUpdateStatusREQ) {
        return biz.updateStatusId(courseCategoryUpdateStatusREQ);
    }


    @ApiOperation(value = "课程分类删除", notes = "课程分类删除")
    @SysLog(value = "课程分类删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }

    /**
     * 普通课程分类列表接口
     *
     * @author kyh
     */
    @ApiOperation(value = "课程分类列表接口", notes = "课程分类列表")
    @RequestMapping(value = "/choose", method = RequestMethod.POST)
    public Result<CourseCategoryChooseRESP> choose(@RequestBody CourseCategoryChooseREQ courseCategoryChooseREQ) {
        return biz.choose(courseCategoryChooseREQ);
    }

}
