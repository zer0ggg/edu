package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.CourseInfoPageBO;
import com.roncoo.education.course.common.bo.CourseInfoSearchBO;
import com.roncoo.education.course.common.bo.CourseVideoBO;
import com.roncoo.education.course.common.dto.CourseInfoPageDTO;
import com.roncoo.education.course.common.dto.CourseInfoSearchPageDTO;
import com.roncoo.education.course.common.dto.CourseViewDTO;
import com.roncoo.education.course.service.api.biz.ApiCourseBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/api/course")
public class ApiCourseController extends BaseController {

	@Autowired
	private ApiCourseBiz biz;

	/**
	 * 普通课程、直播、试卷 课程信息列表接口
	 *
	 * @param courseInfoPageBO
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "普通课程、直播、试卷列表接口（未登录）", notes = "根据条件分页列出普通课程、直播、试卷信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<CourseInfoPageDTO>> list(@RequestBody CourseInfoPageBO courseInfoPageBO) {
		return biz.list(courseInfoPageBO);
	}

	/**
	 * 普通课程、直播、文库详情接口(未登录)
	 *
	 * @param courseVideoBO
	 * @return
	 */
	@ApiOperation(value = "普通课程、直播、文库详情接口(未登录)", notes = "根据课程ID获取普通课程、直播、文库信息(未登录)")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<CourseViewDTO> view(@RequestBody CourseVideoBO courseVideoBO) {
		return biz.view(courseVideoBO);
	}

	/**
	 * 搜索课程接口
	 *
	 * @param courseInfoSearchBO
	 * @return
	 */
	@ApiOperation(value = "课程搜索列表接口", notes = "根据课程名称，进行模糊搜索")
	@RequestMapping(value = "/search/list", method = RequestMethod.POST)
	public Result<Page<CourseInfoSearchPageDTO>> view(@RequestBody CourseInfoSearchBO courseInfoSearchBO) {
		return biz.searchList(courseInfoSearchBO);
	}

	/**
	 * 课程介绍查看（小程序使用）
	 */
	@ApiOperation(value = "课程介绍详情信息接口（小程序使用）", notes = "课程介绍详情信息接口（小程序使用）")
	@RequestMapping(value = "/view/introduce/{id}", method = RequestMethod.GET)
	public String view(@PathVariable(value = "id") Long id) {
		return biz.view(id);
	}

}
