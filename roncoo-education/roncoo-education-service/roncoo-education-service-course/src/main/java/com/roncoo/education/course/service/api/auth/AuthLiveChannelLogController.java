package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogPageBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogSaveBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogViewBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseLiveLogPageDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseLiveLogViewDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthLiveChannelLogBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 直播记录表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/live/channel/log")
public class AuthLiveChannelLogController {

    @Autowired
    private AuthLiveChannelLogBiz biz;

    @ApiOperation(value = "直播记录保存接口", notes = "直播记录保存接口")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result<Integer> save(@RequestBody AuthCourseLiveLogSaveBO authLiveChannelLogSaveBO) {
        return biz.save(authLiveChannelLogSaveBO);
    }

    @ApiOperation(value = "直播记录分页列表接口", notes = "直播记录分页列表接口")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<Page<AuthCourseLiveLogPageDTO>> listForPage(@RequestBody AuthCourseLiveLogPageBO authLiveChannelLogPageBO) {
        return biz.listForPage(authLiveChannelLogPageBO);
    }

    /**
     * 直播记录查看接口
     *
     * @author kyh
     */
    @ApiOperation(value = "直播记录查看接口", notes = "直播记录查看接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<AuthCourseLiveLogViewDTO> view(@RequestBody AuthCourseLiveLogViewBO authLiveChannelLogViewBO) {
        return biz.view(authLiveChannelLogViewBO);
    }
}
