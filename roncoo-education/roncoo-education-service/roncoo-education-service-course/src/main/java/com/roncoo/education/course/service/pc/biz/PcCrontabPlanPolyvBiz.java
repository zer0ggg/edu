package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CrontabPlanPolyvEditREQ;
import com.roncoo.education.course.common.req.CrontabPlanPolyvListREQ;
import com.roncoo.education.course.common.req.CrontabPlanPolyvSaveREQ;
import com.roncoo.education.course.common.resp.CrontabPlanPolyvListRESP;
import com.roncoo.education.course.common.resp.CrontabPlanPolyvViewRESP;
import com.roncoo.education.course.service.dao.CrontabPlanPolyvDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 直播转点存定时任务
 *
 * @author wujing
 */
@Component
public class PcCrontabPlanPolyvBiz extends BaseBiz {

    @Autowired
    private CrontabPlanPolyvDao dao;

    /**
    * 直播转点存定时任务列表
    *
    * @param crontabPlanPolyvListREQ 直播转点存定时任务分页查询参数
    * @return 直播转点存定时任务分页查询结果
    */
    public Result<Page<CrontabPlanPolyvListRESP>> list(CrontabPlanPolyvListREQ crontabPlanPolyvListREQ) {
        CrontabPlanPolyvExample example = new CrontabPlanPolyvExample();
        //Criteria c = example.createCriteria();
        Page<CrontabPlanPolyv> page = dao.listForPage(crontabPlanPolyvListREQ.getPageCurrent(), crontabPlanPolyvListREQ.getPageSize(), example);
        Page<CrontabPlanPolyvListRESP> respPage = PageUtil.transform(page, CrontabPlanPolyvListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 直播转点存定时任务添加
    *
    * @param crontabPlanPolyvSaveREQ 直播转点存定时任务
    * @return 添加结果
    */
    public Result<String> save(CrontabPlanPolyvSaveREQ crontabPlanPolyvSaveREQ) {
        CrontabPlanPolyv record = BeanUtil.copyProperties(crontabPlanPolyvSaveREQ, CrontabPlanPolyv.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 直播转点存定时任务查看
    *
    * @param id 主键ID
    * @return 直播转点存定时任务
    */
    public Result<CrontabPlanPolyvViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CrontabPlanPolyvViewRESP.class));
    }


    /**
    * 直播转点存定时任务修改
    *
    * @param crontabPlanPolyvEditREQ 直播转点存定时任务修改对象
    * @return 修改结果
    */
    public Result<String> edit(CrontabPlanPolyvEditREQ crontabPlanPolyvEditREQ) {
        CrontabPlanPolyv record = BeanUtil.copyProperties(crontabPlanPolyvEditREQ, CrontabPlanPolyv.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 直播转点存定时任务删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
