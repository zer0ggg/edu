package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查找带回课程参数
 *
 * @author Quanf
 */
@Data
@ApiModel(value = "CourseFindBackREQ", description = "查找带回课程参数")
public class CourseFindBackREQ implements Serializable {

    @ApiModelProperty(value = "课程类型")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;

    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
