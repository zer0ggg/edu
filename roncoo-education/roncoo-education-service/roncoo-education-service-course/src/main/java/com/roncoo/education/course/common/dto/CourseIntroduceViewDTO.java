package com.roncoo.education.course.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 小程序课程介绍信息
 *
 */
@Data
@Accessors(chain = true)
public class CourseIntroduceViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 课程简介
	 */
	private String introduce;
}
