package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseChapterPeriodEditREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodListREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterPeriodListRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodViewRESP;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 课时信息
 *
 * @author wujing
 */
@Component
public class PcCourseChapterPeriodBiz extends BaseBiz {

    @Autowired
    private CourseChapterPeriodDao dao;

    /**
    * 课时信息列表
    *
    * @param req 课时信息分页查询参数
    * @return 课时信息分页查询结果
    */
    public Result<Page<CourseChapterPeriodListRESP>> list(CourseChapterPeriodListREQ req) {
        CourseChapterPeriodExample example = new CourseChapterPeriodExample();
        Criteria c = example.createCriteria();
        if (req.getChapterId() != null) {
            c.andChapterIdEqualTo(req.getChapterId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (StringUtils.hasText(req.getPeriodName())) {
            c.andPeriodNameLike(PageUtil.like(req.getPeriodName()));
        }
        example.setOrderByClause("sort asc, id asc");
        Page<CourseChapterPeriod> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseChapterPeriodListRESP> respPage = PageUtil.transform(page, CourseChapterPeriodListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课时信息添加
    *
    * @param courseChapterPeriodSaveREQ 课时信息
    * @return 添加结果
    */
    public Result<String> save(CourseChapterPeriodSaveREQ courseChapterPeriodSaveREQ) {
        CourseChapterPeriod record = BeanUtil.copyProperties(courseChapterPeriodSaveREQ, CourseChapterPeriod.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课时信息查看
    *
    * @param id 主键ID
    * @return 课时信息
    */
    public Result<CourseChapterPeriodViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseChapterPeriodViewRESP.class));
    }


    /**
    * 课时信息修改
    *
    * @param courseChapterPeriodEditREQ 课时信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseChapterPeriodEditREQ courseChapterPeriodEditREQ) {
        CourseChapterPeriod record = BeanUtil.copyProperties(courseChapterPeriodEditREQ, CourseChapterPeriod.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课时信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
