package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.course.common.dto.ResourceRecommendDTO;
import com.roncoo.education.course.common.dto.ResourceRecommendListDTO;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.ResourceRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文库推荐
 *
 * @author wujing
 */
@Component
public class ApiResourceRecommendBiz {

    @Autowired
    private ResourceRecommendDao dao;
    @Autowired
    private CourseDao courseDao;

    public Result<ResourceRecommendListDTO> list() {
        List<ResourceRecommend> list = dao.listByStatusId(StatusIdEnum.YES.getCode());
        ResourceRecommendListDTO dto = new ResourceRecommendListDTO();
        if (CollectionUtils.isEmpty(list)) {
            return Result.success(dto);
        }
        List<ResourceRecommendDTO> listDTO = PageUtil.copyList(list, ResourceRecommendDTO.class);
        for (ResourceRecommendDTO resourceRecommendDTO : listDTO) {
            Course course = courseDao.getById(resourceRecommendDTO.getResourceId());
            if (ObjectUtil.isNotNull(course)) {
                resourceRecommendDTO.setResourceName(course.getCourseName());
                resourceRecommendDTO.setResourceLogo(course.getCourseLogo());
            }
        }
        dto.setList(listDTO);
        return Result.success(dto);

    }

}
