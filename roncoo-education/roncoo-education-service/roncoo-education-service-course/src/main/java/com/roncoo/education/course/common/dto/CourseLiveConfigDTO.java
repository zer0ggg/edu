package com.roncoo.education.course.common.dto;

import com.roncoo.education.common.core.enums.LivePlatformEnum;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigTalkFunVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 课程直播配置
 *
 * @author LYQ
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseLiveConfigDTO implements Serializable {

    private static final long serialVersionUID = 6808662354706421137L;

    /**
     * 直播方式
     */
    private LivePlatformEnum livePlatformEnum;

    /**
     * 保利威配置
     */
    private ConfigPolyvVO configPolyvVO;

    /**
     * 欢拓配置
     */
    private ConfigTalkFunVO configTalkFunVO;
}
