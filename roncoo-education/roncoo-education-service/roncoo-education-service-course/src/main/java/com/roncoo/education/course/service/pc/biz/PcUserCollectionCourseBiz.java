package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.UserCollectionCourseEditREQ;
import com.roncoo.education.course.common.req.UserCollectionCourseListREQ;
import com.roncoo.education.course.common.req.UserCollectionCourseSaveREQ;
import com.roncoo.education.course.common.resp.UserCollectionCourseListRESP;
import com.roncoo.education.course.common.resp.UserCollectionCourseViewRESP;
import com.roncoo.education.course.service.dao.UserCollectionCourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourse;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户收藏课程
 *
 * @author wujing
 */
@Component
public class PcUserCollectionCourseBiz extends BaseBiz {

    @Autowired
    private UserCollectionCourseDao dao;

    /**
     * 用户收藏课程列表
     *
     * @param userCollectionCourseListREQ 用户收藏课程分页查询参数
     * @return 用户收藏课程分页查询结果
     */
    public Result<Page<UserCollectionCourseListRESP>> list(UserCollectionCourseListREQ userCollectionCourseListREQ) {
        UserCollectionCourseExample example = new UserCollectionCourseExample();
        Criteria c = example.createCriteria();
        // 用户编号
        if (ObjectUtil.isNotEmpty(userCollectionCourseListREQ.getUserNo())) {
            c.andUserNoEqualTo(userCollectionCourseListREQ.getUserNo());
        }
        // 收藏类型
        if (ObjectUtil.isNotEmpty(userCollectionCourseListREQ.getCollectionType())) {
            c.andCollectionTypeEqualTo(userCollectionCourseListREQ.getCollectionType());
        }
        example.setOrderByClause(" id desc ");
        Page<UserCollectionCourse> page = dao.listForPage(userCollectionCourseListREQ.getPageCurrent(), userCollectionCourseListREQ.getPageSize(), example);
        Page<UserCollectionCourseListRESP> respPage = PageUtil.transform(page, UserCollectionCourseListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 用户收藏课程添加
     *
     * @param userCollectionCourseSaveREQ 用户收藏课程
     * @return 添加结果
     */
    public Result<String> save(UserCollectionCourseSaveREQ userCollectionCourseSaveREQ) {
        UserCollectionCourse record = BeanUtil.copyProperties(userCollectionCourseSaveREQ, UserCollectionCourse.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户收藏课程查看
     *
     * @param id 主键ID
     * @return 用户收藏课程
     */
    public Result<UserCollectionCourseViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserCollectionCourseViewRESP.class));
    }


    /**
     * 用户收藏课程修改
     *
     * @param userCollectionCourseEditREQ 用户收藏课程修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserCollectionCourseEditREQ userCollectionCourseEditREQ) {
        UserCollectionCourse record = BeanUtil.copyProperties(userCollectionCourseEditREQ, UserCollectionCourse.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户收藏课程删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
