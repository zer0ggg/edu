package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseListRESP;
import com.roncoo.education.course.common.resp.CourseRecommendListRESP;
import com.roncoo.education.course.common.resp.CourseRecommendViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseRecommendBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程推荐 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/recommend")
@Api(value = "course-课程推荐", tags = {"course-课程推荐"})
public class PcCourseRecommendController {

    @Autowired
    private PcCourseRecommendBiz biz;

    @ApiOperation(value = "课程推荐列表", notes = "课程推荐列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseRecommendListRESP>> list(@RequestBody CourseRecommendListREQ courseRecommendListREQ) {
        return biz.list(courseRecommendListREQ);
    }

    @ApiOperation(value = "课程列表", notes = "课程列表")
    @PostMapping(value = "/courseList")
    public Result<Page<CourseListRESP>> courseList(@RequestBody CourseListREQ courseListREQ) {
        return biz.list(courseListREQ);
    }


    @ApiOperation(value = "课程推荐添加", notes = "课程推荐添加")
    @SysLog(value = "课程推荐添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseRecommendSaveREQ courseRecommendSaveREQ) {
        return biz.save(courseRecommendSaveREQ);
    }


    @ApiOperation(value = "批量添加", notes = "批量添加")
    @SysLog(value = "批量添加")
    @PostMapping(value = "/save/batch")
    public Result<String> saveBatch(@RequestBody CourseRecommendSaveBatchREQ req) {
        return biz.saveBatch(req);
    }

    @ApiOperation(value = "课程推荐查看", notes = "课程推荐查看")
    @GetMapping(value = "/view")
    public Result<CourseRecommendViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "课程推荐修改", notes = "课程推荐修改")
    @SysLog(value = "课程推荐修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseRecommendEditREQ courseRecommendEditREQ) {
        return biz.edit(courseRecommendEditREQ);
    }


    @ApiOperation(value = "课程推荐删除", notes = "课程推荐删除")
    @SysLog(value = "课程推荐删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
