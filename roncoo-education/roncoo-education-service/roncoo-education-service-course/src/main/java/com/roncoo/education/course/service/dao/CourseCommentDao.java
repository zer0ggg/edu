package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;

import java.util.List;

/**
 * 课程评论 服务类
 *
 * @author Quanf
 * @date 2020-09-05
 */
public interface CourseCommentDao {

    /**
     * 保存课程评论
     *
     * @param record 课程评论
     * @return 影响记录数
     */
    int save(CourseComment record);

    /**
     * 根据ID删除课程评论
     *
     * @param id 主键ID
     * @return 影响记录数
     */
    int deleteById(Long id);

    /**
     * 修改试卷分类
     *
     * @param record 课程评论
     * @return 影响记录数
     */
    int updateById(CourseComment record);

    /**
     * 根据ID获取课程评论
     *
     * @param id 主键ID
     * @return 课程评论
     */
    CourseComment getById(Long id);

    /**
     * 课程评论--分页查询
     *
     * @param pageCurrent 当前页
     * @param pageSize    分页大小
     * @param example     查询条件
     * @return 分页结果
     */
    Page<CourseComment> listForPage(int pageCurrent, int pageSize, CourseCommentExample example);


    /**
     * 根据父类id、状态查找评论信息
     *
     * @param parentId 父类id
     * @param statusId 状态
     * @return 评论信息
     */
    List<CourseComment> listByParentIdAndStatusId(Long parentId, Integer statusId);

}
