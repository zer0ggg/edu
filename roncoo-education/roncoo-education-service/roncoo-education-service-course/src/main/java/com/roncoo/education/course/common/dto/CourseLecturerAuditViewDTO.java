package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 管理员预览课程
 */
@Data
@Accessors(chain = true)
public class CourseLecturerAuditViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 讲师用户编号
     */
    @ApiModelProperty(value = "讲师编号", required = true)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lecturerUserNo;
    /**
     * 讲师名称
     */
    @ApiModelProperty(value = "讲师名称", required = true)
    private String lecturerName;
    /**
     * 讲师手机
     */
    @ApiModelProperty(value = "讲师手机", required = true)
    private String lecturerMobile;
    /**
     * 讲师邮箱
     */
    @ApiModelProperty(value = "讲师邮箱", required = true)
    private String lecturerEmail;
    /**
     * 职位
     */
    @ApiModelProperty(value = "职位", required = true)
    private String position;
    /**
     * 头像
     */
    @ApiModelProperty(value = "头像", required = true)
    private String headImgUrl;
    /**
     * 简介
     */
    @ApiModelProperty(value = "简介", required = true)
    private String introduce;

}
