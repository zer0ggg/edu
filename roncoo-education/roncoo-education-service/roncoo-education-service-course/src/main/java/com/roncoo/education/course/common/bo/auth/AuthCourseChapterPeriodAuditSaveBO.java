package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodAuditSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "章节ID", required = true)
    private Long chapterId;

    @ApiModelProperty(value = "课时名称", required = true)
    private String periodName;

    @ApiModelProperty(value = "课时描述", required = false)
    private String periodDesc;

    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "直播开始时间")
    private String startTime;

    @ApiModelProperty(value = "直播结束时间")
    private String endTime;

    @ApiModelProperty(value = "是否需要人脸对比(0:否，1:是)")
    private Integer isFaceContras;

    @ApiModelProperty(value = "排序")
    private Integer sort;

}
