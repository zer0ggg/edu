package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseIntroduceEditREQ;
import com.roncoo.education.course.common.req.CourseIntroduceListREQ;
import com.roncoo.education.course.common.req.CourseIntroduceSaveREQ;
import com.roncoo.education.course.common.resp.CourseIntroduceListRESP;
import com.roncoo.education.course.common.resp.CourseIntroduceViewRESP;
import com.roncoo.education.course.service.dao.CourseIntroduceDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduce;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程介绍信息
 *
 * @author wujing
 */
@Component
public class PcCourseIntroduceBiz extends BaseBiz {

    @Autowired
    private CourseIntroduceDao dao;

    /**
    * 课程介绍信息列表
    *
    * @param courseIntroduceListREQ 课程介绍信息分页查询参数
    * @return 课程介绍信息分页查询结果
    */
    public Result<Page<CourseIntroduceListRESP>> list(CourseIntroduceListREQ courseIntroduceListREQ) {
        CourseIntroduceExample example = new CourseIntroduceExample();
        Criteria c = example.createCriteria();
        Page<CourseIntroduce> page = dao.listForPage(courseIntroduceListREQ.getPageCurrent(), courseIntroduceListREQ.getPageSize(), example);
        Page<CourseIntroduceListRESP> respPage = PageUtil.transform(page, CourseIntroduceListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课程介绍信息添加
    *
    * @param courseIntroduceSaveREQ 课程介绍信息
    * @return 添加结果
    */
    public Result<String> save(CourseIntroduceSaveREQ courseIntroduceSaveREQ) {
        CourseIntroduce record = BeanUtil.copyProperties(courseIntroduceSaveREQ, CourseIntroduce.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课程介绍信息查看
    *
    * @param id 主键ID
    * @return 课程介绍信息
    */
    public Result<CourseIntroduceViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseIntroduceViewRESP.class));
    }


    /**
    * 课程介绍信息修改
    *
    * @param courseIntroduceEditREQ 课程介绍信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseIntroduceEditREQ courseIntroduceEditREQ) {
        CourseIntroduce record = BeanUtil.copyProperties(courseIntroduceEditREQ, CourseIntroduce.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课程介绍信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
