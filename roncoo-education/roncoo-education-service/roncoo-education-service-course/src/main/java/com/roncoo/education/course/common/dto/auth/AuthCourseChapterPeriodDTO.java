package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课时信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "课时id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 课时名称
	 */
	@ApiModelProperty(value = "课时名称")
	private String periodName;
	/**
	 * 课时描述
	 */
	@ApiModelProperty(value = "课时描述")
	private String periodDesc;
	/**
	 * 是否免费：1免费，0收费
	 */
	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;
	/**
	 * 原价
	 */
	@ApiModelProperty(value = "原价")
	private BigDecimal periodOriginal;
	/**
	 * 是否存在视频(1存在，0否)
	 */
	@ApiModelProperty(value = "是否存在视频(1存在，0否)")
	private Integer isVideo;
	/**
	 * 视频编号
	 */
	@ApiModelProperty(value = "视频编号")
	private Long videoNo;
	/**
	 * 视频名称
	 */
	@ApiModelProperty(value = "视频名称")
	private String videoName;
	/**
	 * 时长
	 */
	@ApiModelProperty(value = "时长")
	private String videoLength;
	/**
	 * 视频Vid
	 */
	@ApiModelProperty(value = "视频Vid")
	private String videoVid;
	/**
	 * 阿里云oas
	 */
	@ApiModelProperty(value = "阿里云oas")
	private String videoOasId;

	/**
	 * 直播状态(1:未开播,2:正在直播,3:回放)
	 */
	@ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:回放)")
	private Integer liveStatus;
	/**
	 * 直播回放url
	 */
	@ApiModelProperty(value = "直播回放url")
	private String playback;
	/**
	 * 直播开始时间
	 */
	@ApiModelProperty(value = "直播开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	/**
	 * 直播结束时间
	 */
	@ApiModelProperty(value = "直播结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	@ApiModelProperty(value = "课程试卷关联主键id")
	private Long courseExamRefId;

	@ApiModelProperty(value = "试卷ID")
	private Long examId;

	@ApiModelProperty(value = "试卷名称")
	private String examName;
}
