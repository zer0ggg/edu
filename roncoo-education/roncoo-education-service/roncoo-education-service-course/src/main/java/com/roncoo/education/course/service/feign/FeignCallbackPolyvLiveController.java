package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.course.feign.interfaces.IFeignCallbackPolyvLive;
import com.roncoo.education.course.feign.qo.*;
import com.roncoo.education.course.service.feign.biz.FeignCallbackPolyvLiveBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignCallbackPolyvLiveController extends BaseController implements IFeignCallbackPolyvLive {

    @Autowired
    private FeignCallbackPolyvLiveBiz biz;


    @Override
    public String callbackPolyvLiveStatus(@RequestBody PolyvLiveStatusQO polyvLiveStatusQO) {
        return biz.callbackPolyvLiveStatus(polyvLiveStatusQO);
    }

    @Override
    public String callbackPolyvLiveAuth(@RequestBody PolyvLiveAuthQO polyvLiveAuthQO) {
        return biz.callbackPolyvLiveAuth(polyvLiveAuthQO);
    }

    @Override
    public String callbackLiveFileUrl(@RequestBody PolyvLiveFileUrlQO polyvLiveFileUrlQO) {
        return biz.callbackLiveFileUrl(polyvLiveFileUrlQO);
    }

    @Override
    public String callbackLiveSave(@RequestBody PolyvLiveSaveQO polyvLiveSaveQO) {
        return biz.callbackLiveSave(polyvLiveSaveQO);
    }

    @Override
    public String callbackLiveRemakes(@RequestBody PolyvLiveRemakesQO polyvLiveRemakesQO) {
        return biz.callbackLiveRemakes(polyvLiveRemakesQO);
    }
}