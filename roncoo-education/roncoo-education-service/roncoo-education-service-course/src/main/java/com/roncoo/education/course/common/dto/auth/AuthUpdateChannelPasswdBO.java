/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 修改频道号密码
 */
@Data
@Accessors(chain = true)
public class AuthUpdateChannelPasswdBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "频道号不能为空")
    @ApiModelProperty(value = "频道号", required = true)
    private String channelId;

    @NotBlank(message = "频道密码不能为空")
    @ApiModelProperty(value = "密码")
    private String passwd;

}
