package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.common.polyv.live.request.PLChannelCreateRequest;
import com.roncoo.education.common.polyv.live.result.PLChannelGetAccounts;
import com.roncoo.education.common.polyv.live.result.PLChannelGetResult;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.common.talk.TalkFunUtil;
import com.roncoo.education.common.talk.request.CourseAccessRequest;
import com.roncoo.education.common.talk.request.CourseAddRequest;
import com.roncoo.education.common.talk.request.CourseGetRequest;
import com.roncoo.education.common.talk.response.*;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogPageBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogSaveBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseLiveLogViewBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseLiveLogPageDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseLiveLogViewDTO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample.Criteria;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigLivePlatformVO;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigTalkFunVO;
import com.roncoo.education.system.feign.vo.ConfigWebsiteVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 直播记录表
 *
 * @author wujing
 */
@Component
public class AuthLiveChannelLogBiz extends BaseBiz {

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseLiveLogDao dao;

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> save(AuthCourseLiveLogSaveBO bo) {
        BeanValidateUtil.ValidationResult validationResult = BeanValidateUtil.validate(bo);
        if (!validationResult.isPass()) {
            return Result.error(validationResult.getErrMsgString());
        }

        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(ThreadContext.userNo());
        if (IsLiveEnum.NO.getCode().equals(lecturerVO.getIsLive())) {
            return Result.error("没有直播权限,请先申请直播!");
        }

        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(bo.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod) || StatusIdEnum.NO.getCode().equals(courseChapterPeriod.getStatusId())) {
            return Result.error("该课时异常");
        }
        CourseChapter courseChapter = courseChapterDao.getById(courseChapterPeriod.getChapterId());
        if (ObjectUtil.isNull(courseChapter) || StatusIdEnum.NO.getCode().equals(courseChapter.getStatusId())) {
            return Result.error("该章节异常");
        }

        Course course = courseDao.getById(courseChapterPeriod.getCourseId());
        if (ObjectUtil.isNull(course) || StatusIdEnum.NO.getCode().equals(course.getStatusId())) {
            return Result.error("该课程异常");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("该课程异常");
        }

        // 校验直播功能
        ConfigLivePlatformVO livePlatformVO = feignSysConfig.getLivePlatform();
        if (ObjectUtil.isNull(livePlatformVO.getLivePlatform())) {
            return Result.error("平台没有设置直播平台");
        }

        // 直播记录处理
        CourseLiveLog nowLiveLog = dao.getByPeriodIdAndLiveStatus(courseChapterPeriod.getId(), LiveStatusEnum.NOW.getCode());
        if (ObjectUtil.isNotNull(nowLiveLog)) {
            CourseLiveLog updateLiveLog = new CourseLiveLog();
            updateLiveLog.setId(nowLiveLog.getId());
            updateLiveLog.setLiveStatus(LiveStatusEnum.END.getCode());
            dao.updateById(updateLiveLog);
        }
        CourseLiveLog notLiveLog = dao.getByPeriodIdAndLiveStatus(courseChapterPeriod.getId(), LiveStatusEnum.NOT.getCode());
        if (ObjectUtil.isNotNull(notLiveLog)) {
            // 删除旧的直播频道--可以不做处理，但是在相应的直播平台会产生很多垃圾数据
            deleteOldChannel(notLiveLog);
        }

        LivePlatformEnum livePlatformEnum = LivePlatformEnum.byCode(livePlatformVO.getLivePlatform());
        if (LivePlatformEnum.POLYV.equals(livePlatformEnum)) {
            return polyvLiveSetting(bo, lecturerVO, course, courseChapter, courseChapterPeriod);
        } else if (LivePlatformEnum.TALK_FUN.equals(livePlatformEnum)) {
            return talkFunLiveSetting(bo, lecturerVO, course, courseChapter, courseChapterPeriod);
        } else {
            logger.warn("直播方式[编码：{}--描述：{}]暂未实现相关功能", livePlatformEnum.getCode(), livePlatformEnum.getDesc());
            return Result.error("直播：[" + livePlatformEnum.getDesc() + "]相关功能正在开发中");
        }
    }

    public Result<Page<AuthCourseLiveLogPageDTO>> listForPage(AuthCourseLiveLogPageBO bo) {
        if (bo.getLecturerUserNo() == null) {
            return Result.error("讲师编号不能为空");
        }
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(bo.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("讲师异常");
        }

        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andLecturerUserNoEqualTo(bo.getLecturerUserNo());
        if (bo.getLiveStatus() == 1) {
            c.andLiveStatusEqualTo(LiveStatusEnum.NOT.getCode());
        } else {
            c.andLiveStatusGreaterThan(LiveStatusEnum.NOT.getCode());
        }
        example.setOrderByClause(" live_status asc, id desc ");
        Page<CourseLiveLog> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthCourseLiveLogPageDTO> pageDTO = PageUtil.transform(page, AuthCourseLiveLogPageDTO.class);

        Map<String, Object> configCacheMap = new HashMap<>();
        for (AuthCourseLiveLogPageDTO dto : pageDTO.getList()) {
            if (LivePlatformEnum.POLYV.getCode().equals(dto.getLivePlatform())) {
                // 保利威
                Object configObj = configCacheMap.get(LivePlatformEnum.POLYV.name());
                ConfigPolyvVO configPolyvVO;
                if (ObjectUtil.isNull(configObj)) {
                    configPolyvVO = feignSysConfig.getPolyv();
                    if (ObjectUtil.isEmpty(configPolyvVO) || StrUtil.isBlank(configPolyvVO.getPolyvAppID()) || StrUtil.isBlank(configPolyvVO.getPolyvAppSecret())) {
                        logger.error("保利威配置错误，参数={}", configPolyvVO);
                        return Result.error("保利威配置错误");
                    }
                    configCacheMap.put(LivePlatformEnum.POLYV.name(), configPolyvVO);
                } else {
                    configPolyvVO = (ConfigPolyvVO) configObj;
                }

                PLChannelGetResult plChannelGetResult = PLChannelUtil.getChannel(dto.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
                if (ObjectUtil.isNotNull(plChannelGetResult)) {
                    dto.setScene(plChannelGetResult.getScene());
                    dto.setChannelPasswd(plChannelGetResult.getChannelPasswd());
                }
                dto.setLiveUrl(PLChannelUtil.getLoginUrl(dto.getChannelId()));
            } else if (LivePlatformEnum.TALK_FUN.getCode().equals(dto.getLivePlatform())) {
                // 欢拓
                Object configObj = configCacheMap.get(LivePlatformEnum.TALK_FUN.name());
                ConfigTalkFunVO configTalkFunVO;
                if (ObjectUtil.isNull(configObj)) {
                    configTalkFunVO = feignSysConfig.getTalkFun();
                    if (ObjectUtil.isEmpty(configTalkFunVO) || StrUtil.isBlank(configTalkFunVO.getTalkFunOpenId()) || StrUtil.isBlank(configTalkFunVO.getTalkFunOpenToken())) {
                        logger.error("欢拓配置错误，参数={}", configTalkFunVO);
                        return Result.error("欢拓配置错误");
                    }
                    configCacheMap.put(LivePlatformEnum.TALK_FUN.name(), configTalkFunVO);
                } else {
                    configTalkFunVO = (ConfigTalkFunVO) configObj;
                }

                // 获取课程信息
                CourseGetRequest getRequest = new CourseGetRequest();
                getRequest.setCourseId(Integer.parseInt(dto.getChannelId()));
                CourseGetResponse getResponse = TalkFunUtil.courseGet(getRequest, configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
                if (ObjectUtil.isNotNull(getResponse)) {
//                    dto.setScene(getResponse.getScenes() == 1 ? LiveSceneEnum.PPT.getCode() : LiveSceneEnum.ALONE.getCode());
                    dto.setChannelPasswd(getResponse.getZhuBoKey());
                }

                // 获取开播地址
                CourseLaunchResponse launchResponse = TalkFunUtil.courseLaunch(Integer.parseInt(dto.getChannelId()), configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
                if (ObjectUtil.isNotNull(launchResponse)) {
                    dto.setLiveUrl(launchResponse.getUrl());
                }
            } else {
                logger.warn("直播平台：{}没有实现相关处理逻辑", dto.getLivePlatform());
                return Result.error("直播配置异常，请联系管理员");
            }

        }
        return Result.success(pageDTO);
    }

    /**
     * 直播记录查看接口
     *
     * @param bo 直播记录查看参数
     * @author kyh
     */
    public Result<AuthCourseLiveLogViewDTO> view(AuthCourseLiveLogViewBO bo) {
        if (bo.getPeriodId() == null) {
            return Result.error("课时ID不能为空");
        }
        CourseLiveLog liveLog = dao.getByPeriodIdAndLiveStatus(bo.getPeriodId(), LiveStatusEnum.NOT.getCode());
        if (ObjectUtil.isNull(liveLog)) {
            return Result.success(null);
        }

        if (LivePlatformEnum.POLYV.getCode().equals(liveLog.getLivePlatform())) {
            // 保利威
            return getPolyvView(liveLog);
        } else if (LivePlatformEnum.TALK_FUN.getCode().equals(liveLog.getLivePlatform())) {
            // 欢拓
            return getTalkFunView(liveLog);
        } else {
            return Result.error("直播平台配置异常");
        }
    }

    /**
     * 删除旧频道编号
     *
     * @param liveLog 直播记录
     */
    private void deleteOldChannel(CourseLiveLog liveLog) {
        if (LivePlatformEnum.POLYV.getCode().equals(liveLog.getLivePlatform())) {
            ConfigPolyvVO sysVo = feignSysConfig.getPolyv();
            PLChannelUtil.deleteChannel(liveLog.getChannelId(), sysVo.getPolyvAppID(), sysVo.getPolyvUseid(), sysVo.getPolyvAppSecret());
        } else if (LivePlatformEnum.TALK_FUN.getCode().equals(liveLog.getLivePlatform())) {
            // TODO 欢拓的需要删除主播和删除课程--目前主播是没有删除接口的
            ConfigTalkFunVO sysVo = feignSysConfig.getTalkFun();
            TalkFunUtil.courseDelete(Integer.parseInt(liveLog.getChannelId()), sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        }

        // 删除直播记录
        dao.deleteById(liveLog.getId());
    }

    /**
     * 保利威直播设置
     *
     * @param bo                  请求参数
     * @param lecturerVO          讲师信息
     * @param course              课程信息
     * @param courseChapter       章节信息
     * @param courseChapterPeriod 课时信息
     * @return 处理结果
     */
    private Result<Integer> polyvLiveSetting(AuthCourseLiveLogSaveBO bo, LecturerVO lecturerVO, Course course, CourseChapter courseChapter, CourseChapterPeriod courseChapterPeriod) {
        ConfigPolyvVO sysVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sysVO)) {
            return Result.error("配置不存在");
        }
        if (StringUtils.isEmpty(sysVO.getPolyvLiveNotify())) {
            return Result.error("未配置保利威直播外部授权回调");
        }
        if (StringUtils.isEmpty(sysVO.getPolyvAppID())) {
            return Result.error("未配置保利威直播AppID");
        }
        if (StringUtils.isEmpty(sysVO.getPolyvUseid())) {
            return Result.error("未配置保利威useId");
        }
        if (StringUtils.isEmpty(sysVO.getPolyvAppSecret())) {
            return Result.error("未配置保利威直播AppSecret");
        }

        // 创建频道号
        PLChannelCreateRequest plChannelCreateRequest = new PLChannelCreateRequest();
        plChannelCreateRequest.setName(RandomUtil.randomNumbers(6));
        plChannelCreateRequest.setChannelPasswd(RandomUtil.randomNumbers(6));
        //创建三分屏频道号
        if (LiveSceneEnum.PPT.getCode().equals(bo.getScene())) {
            plChannelCreateRequest.setScene("ppt");
        } else if (LiveSceneEnum.ALONE.getCode().equals(bo.getScene())) {
            plChannelCreateRequest.setScene("alone");
        }
        PLChannelGetResult plChannelGetResult = PLChannelUtil.createChannel(plChannelCreateRequest, sysVO.getPolyvAppID(), sysVO.getPolyvUseid(), sysVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(plChannelGetResult)) {
            throw new BaseException("创建频道号信息异常!");
        }
        //设置观看条件
        PLChannelUtil.setAuth(plChannelGetResult.getChannelId(), sysVO.getPolyvLiveNotify(), sysVO.getPolyvAppID(), sysVO.getPolyvUseid(), sysVO.getPolyvAppSecret());

        String channelId = String.valueOf(plChannelGetResult.getChannelId());
        CourseLiveLog log = BeanUtil.copyProperties(bo, CourseLiveLog.class);
        log.setChannelId(channelId);
        log.setChannelPasswd(plChannelCreateRequest.getChannelPasswd());
        log.setLecturerUserNo(course.getLecturerUserNo());
        log.setCourseId(courseChapterPeriod.getCourseId());
        log.setCourseName(course.getCourseName());
        log.setChapterId(courseChapterPeriod.getChapterId());
        log.setChapterName(courseChapter.getChapterName());
        log.setPeriodId(bo.getPeriodId());
        log.setPeriodName(courseChapterPeriod.getPeriodName());
        log.setLivePlatform(LivePlatformEnum.POLYV.getCode());
        log.setLiveScene(bo.getScene());
        int results = dao.save(log);

        if (results > 0) {
            ConfigWebsiteVO websiteVO = feignSysConfig.getWebsite();
            if (StringUtils.isNotEmpty(lecturerVO.getHeadImgUrl())) {
                // 设置直播详情页图标
                File lecturerImgUrl = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), lecturerVO.getHeadImgUrl());
                PLChannelUtil.setCoverImgChannel(channelId, lecturerImgUrl, sysVO.getPolyvAppID(), sysVO.getPolyvAppSecret());
            } else {
                // 设置直播详情页图标
                File orgLogo = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), websiteVO.getLogo());
                PLChannelUtil.setCoverImgChannel(channelId, orgLogo, sysVO.getPolyvAppID(), sysVO.getPolyvAppSecret());
            }

            if (!lecturerVO.getLecturerName().isEmpty()) {
                // 设置直播主持人名称
                PLChannelUtil.setPublisherChannel(channelId, lecturerVO.getLecturerName(), sysVO.getPolyvAppID(), sysVO.getPolyvUseid(), sysVO.getPolyvAppSecret());
            }
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(course.getIntroduceId());

            if (!courseIntroduce.getIntroduce().isEmpty()) {
                // 设置直播介绍
                PLChannelUtil.setMenu(channelId, courseIntroduce.getIntroduce(), sysVO.getPolyvAppID(), sysVO.getPolyvUseid(), sysVO.getPolyvAppSecret());
            }
            // 设置观看页名称
            PLChannelUtil.updateChannel(channelId, course.getCourseName(), sysVO.getPolyvAppID(), sysVO.getPolyvAppSecret());

            return Result.success(results);
        }
        return Result.error("该课程异常");
    }

    /**
     * 欢拓直播设置
     *
     * @param bo                  课程直播记录保存参数
     * @param lecturerVO          讲师信息
     * @param course              课程信息
     * @param courseChapter       章节信息
     * @param courseChapterPeriod 课时信息
     * @return 设置结果
     */
    private Result<Integer> talkFunLiveSetting(AuthCourseLiveLogSaveBO bo, LecturerVO lecturerVO, Course course, CourseChapter courseChapter, CourseChapterPeriod courseChapterPeriod) {
        ConfigTalkFunVO sysVO = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(sysVO)) {
            logger.warn("获取欢拓直播配置失败，配置不存在");
            return Result.error("配置不存在");
        }
        if (StrUtil.isBlank(sysVO.getTalkFunOpenId()) || StrUtil.isBlank(sysVO.getTalkFunOpenToken())) {
            logger.error("欢拓直播配置未设置，请设置");
            return Result.error("直播信息未配置，请联系管理员");
        }


        // 创建三分屏
        CourseAddRequest addRequest = new CourseAddRequest();

        addRequest.setCourseName(course.getCourseName());
        addRequest.setAccount(String.valueOf(IdWorker.getId()));
        addRequest.setStartTime(courseChapterPeriod.getStartTime());
        addRequest.setEndTime(courseChapterPeriod.getEndTime());
        // 设置主播名称
        addRequest.setNickname(lecturerVO.getLecturerName());
        CourseAddRequest.Option option = new CourseAddRequest.Option();

        //创建三分屏频道号
        if (LiveSceneEnum.ALONE.getCode().equals(bo.getScene())) {
            // 大班
            option.setModeType(3);
        } else if (LiveSceneEnum.PPT.getCode().equals(bo.getScene())) {
            // 二分屏（大班）
            option.setModeType(3);
        } else if (LiveSceneEnum.SMALLCLASS.getCode().equals(bo.getScene())) {
            // 小班
            option.setModeType(5);
            // 小班课类型
            // 小班课类型： 1V1
            option.setSmallType(bo.getSmallType());
            if (bo.getSmallType().equals(1)) {
                option.setStreamMode(2);
            } else {
                option.setStreamMode(1);
            }
        } else if (LiveSceneEnum.LARGEINTERACTIVE.getCode().equals(bo.getScene())) {
            // 大班互动
            option.setModeType(6);
        }
        // 生活直播
        option.setScenes(2);
        logger.warn("开播信息={}",option);
        option.setPassword(RandomUtil.randomNumbers(6));
        addRequest.setOption(option);
        CourseAddResponse addResponse = TalkFunUtil.courseAdd(addRequest, sysVO.getTalkFunOpenId(), sysVO.getTalkFunOpenToken());
        if (ObjectUtil.isNull(addResponse)) {
            throw new BaseException("创建频道号信息异常!");
        }

        CourseLiveLog log = BeanUtil.copyProperties(bo, CourseLiveLog.class);
        log.setChannelId(String.valueOf(addResponse.getCourseId()));
        log.setChannelPasswd(option.getPassword());
        log.setLecturerUserNo(course.getLecturerUserNo());
        log.setCourseId(courseChapterPeriod.getCourseId());
        log.setCourseName(course.getCourseName());
        log.setChapterId(courseChapterPeriod.getChapterId());
        log.setChannelPasswd(option.getPassword());
        log.setChapterName(courseChapter.getChapterName());
        log.setPeriodId(courseChapterPeriod.getId());
        log.setPeriodName(courseChapterPeriod.getPeriodName());
        log.setLivePlatform(LivePlatformEnum.TALK_FUN.getCode());
        log.setLiveScene(bo.getScene());
        int results = dao.save(log);

        if (results > 0) {
            // 设置头像
            if (StringUtils.isNotEmpty(lecturerVO.getHeadImgUrl())) {
                File lecturerImgUrl = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), lecturerVO.getHeadImgUrl());
                TalkFunUtil.zhuBoPortrait(addRequest.getAccount(), lecturerImgUrl, sysVO.getTalkFunOpenId(), sysVO.getTalkFunOpenToken());
            } else {
                ConfigWebsiteVO websiteVO = feignSysConfig.getWebsite();
                File orgLogo = AliyunUtil.download(BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class), websiteVO.getLogo());
                TalkFunUtil.zhuBoPortrait(addRequest.getAccount(), orgLogo, sysVO.getTalkFunOpenId(), sysVO.getTalkFunOpenToken());
            }
            return Result.success(results);
        }
        return Result.error("该课程异常");
    }

    /**
     * 获取保利威直播频道信息
     *
     * @param liveLog 直播记录
     * @return 频道信息
     */
    private Result<AuthCourseLiveLogViewDTO> getPolyvView(CourseLiveLog liveLog) {
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StrUtil.isBlank(configPolyvVO.getPolyvAppID()) || StrUtil.isBlank(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return Result.error("保利威配置错误");
        }

        AuthCourseLiveLogViewDTO authCourseLiveLogViewDTO = BeanUtil.copyProperties(liveLog, AuthCourseLiveLogViewDTO.class);
        authCourseLiveLogViewDTO.setWebLiveUrl(SystemUtil.WEB_LIVE_URL.concat(liveLog.getChannelId()));
        PLChannelGetAccounts plChannelGetAccounts = PLChannelUtil.accounts(liveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(plChannelGetAccounts)) {
            return Result.error("获取直播频道助教信息异常");
        }
        PLChannelGetResult plChannelGetResult = PLChannelUtil.getChannel(liveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(plChannelGetResult)) {
            return Result.error("获取直播频道信息异常");
        }

        // 获取助教信息
        authCourseLiveLogViewDTO.setChannelPasswd(plChannelGetResult.getChannelPasswd());
        authCourseLiveLogViewDTO.setAssistantChannelId(plChannelGetAccounts.getAccount());
        authCourseLiveLogViewDTO.setAssistantPasswd(plChannelGetAccounts.getPasswd());
        authCourseLiveLogViewDTO.setPushFlowUrl(plChannelGetResult.getUrl());
        authCourseLiveLogViewDTO.setScene(liveLog.getLiveScene());
        authCourseLiveLogViewDTO.setAssistantUrl("https://live.polyv.net/teacher.html");
        return Result.success(authCourseLiveLogViewDTO);
    }

    /**
     * 获取欢拓直播课程信息
     *
     * @param liveLog 直播记录
     * @return 课程信息
     */
    private Result<AuthCourseLiveLogViewDTO> getTalkFunView(CourseLiveLog liveLog) {
        ConfigTalkFunVO configTalkFunVO = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(configTalkFunVO) || StrUtil.isBlank(configTalkFunVO.getTalkFunOpenId()) || StrUtil.isBlank(configTalkFunVO.getTalkFunOpenToken())) {
            return Result.error("直播配置异常");
        }

        // 获取直播信息
        CourseLaunchResponse launchResponse = TalkFunUtil.courseLaunch(Integer.parseInt(liveLog.getChannelId()), configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
        if (ObjectUtil.isNull(launchResponse)) {
            return Result.error("直播地址获取失败");
        }

        CourseGetRequest getRequest = new CourseGetRequest();
        getRequest.setCourseId(Integer.parseInt(liveLog.getChannelId()));
        CourseGetResponse getResponse = TalkFunUtil.courseGet(getRequest, configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
        if (ObjectUtil.isNull(getResponse)) {
            return Result.error("直播信息获取失败");
        }
        // 获取助教授权登录地址
        CourseAccessRequest accessRequest = new CourseAccessRequest();
        accessRequest.setCourseId(Integer.parseInt(liveLog.getChannelId()));
        accessRequest.setUid(liveLog.getChannelId());
        accessRequest.setNickname("助教");
        accessRequest.setRole("admin");
        CourseAccessRequest.Option option = new CourseAccessRequest.Option();
        option.setTimes(1);
        option.setSsl(true);
        CourseAccessResponse accessResponse = TalkFunUtil.courseAccess(accessRequest, configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
        if (ObjectUtil.isNull(accessResponse)) {
            return Result.error("助教信息获取失败");
        }

        // 获取直播推流地址
        CoursePushRtmpUrlResponse pushRtmpUrlResponse = TalkFunUtil.coursePushRtmpUrl(Integer.parseInt(liveLog.getChannelId()), configTalkFunVO.getTalkFunOpenId(), configTalkFunVO.getTalkFunOpenToken());
        if (ObjectUtil.isNull(pushRtmpUrlResponse)) {
            return Result.error("获取直播推流地址失败");
        }

        AuthCourseLiveLogViewDTO dto = BeanUtil.copyProperties(liveLog, AuthCourseLiveLogViewDTO.class);
        dto.setWebLiveUrl(launchResponse.getUrl());
        dto.setPushFlowUrl(pushRtmpUrlResponse.getPushAddr());
        dto.setAssistantChannelId(liveLog.getChannelId());
        dto.setAssistantUrl(accessResponse.getLiveUrl());
        dto.setScene(liveLog.getLiveScene());
        dto.setBid(getResponse.getBid());
        return Result.success(dto);
    }
}
