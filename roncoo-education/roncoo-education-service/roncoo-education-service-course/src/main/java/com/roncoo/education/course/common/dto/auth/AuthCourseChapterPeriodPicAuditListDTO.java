package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 课时图片审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodPicAuditListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课时图片集合
	 */
	@ApiModelProperty(value = "课时图片集合")
	private List<AuthCourseChapterPeriodPicAuditDTO> list = new ArrayList<>();

}
