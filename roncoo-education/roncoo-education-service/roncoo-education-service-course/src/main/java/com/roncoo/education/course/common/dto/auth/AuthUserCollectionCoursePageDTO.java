package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserCollectionCoursePageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date gmtCreate;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long userNo;
	/**
	 * 用户手机号码
	 */
	@ApiModelProperty(value = "用户手机号码")
	private String mobile;
	/**
	 * 课时id
	 */
	@ApiModelProperty(value = "课时id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long periodId;
	/**
	 * 课时名称
	 */
	@ApiModelProperty(value = "课时名称")
	private String periodName;
	/**
	 * 章节id
	 */
	@ApiModelProperty(value = "章节id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long chapterId;
	/**
	 * 章节名称
	 */
	@ApiModelProperty(value = "章节名称")
	private String chapterName;
	/**
	 * 课程id
	 */
	@ApiModelProperty(value = "课程id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	/**
	 * 课程分类(1:普通课程;2:直播课程,4文库)
	 */
	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4文库)")
	private Integer courseCategory;
	/**
	 * 课程图片
	 */
	@ApiModelProperty(value = "课程图片")
	private String courseImg;
	/**
	 * 收藏类型:1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "收藏类型:1课程，2章节，3课时")
	private Integer collectionType;
}
