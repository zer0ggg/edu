/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课时信息
 * </p>
 *
 * @author wujing123
 */
@Data
@Accessors(chain = true)
public class AuthCourseSignDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "直播平台(1:保利威、2:阿里云点播)")
    private Integer vodPlatform;

    @ApiModelProperty(value = "h5播放时间戳", required = true)
    private String ts;

    @ApiModelProperty(value = "h5播放校验签名", required = true)
    private String sign;

    @ApiModelProperty(value = "h5播放token值", required = true)
    private String token;

    @ApiModelProperty(value = "跑马灯自定义的code值", required = true)
    private String code;

    @ApiModelProperty(value = "封面图片--阿里云点播")
    private String coverUrl;

    @ApiModelProperty(value = "视频播放源--阿里云点播")
    private String source;

    @ApiModelProperty(value = "剩余人脸对比次数")
    private Integer residueContrastTotal;
}
