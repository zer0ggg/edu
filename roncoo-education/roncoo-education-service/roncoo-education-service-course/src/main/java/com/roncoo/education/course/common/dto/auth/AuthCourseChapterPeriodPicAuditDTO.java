package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 课时图片审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodPicAuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	private Date gmtModified;
	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 章节ID
	 */
	@ApiModelProperty(value = "章节ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long chapterId;
	/**
	 * 课时ID
	 */
	@ApiModelProperty(value = "课时id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long periodId;
	/**
	 * 图片ID
	 */
	@ApiModelProperty(value = "图片ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long picId;
	/**
	 * 图片类型(1:试卷题目,2:试卷答案)
	 */
	@ApiModelProperty(value = " 图片类型(1:试卷题目,2:试卷答案)")
	private Integer picType;
	/**
	 * 图片地址
	 */
	@ApiModelProperty(value = "图片地址")
	private String picUrl;
	/**
	 * 标题
	 */
	@ApiModelProperty(value = "标题")
	private String title;
	/**
	 * 审核状态(0:待审核;1:审核通过;2:审核不通过)
	 */
	@ApiModelProperty(value = "审核状态(0:待审核;1:审核通过;2:审核不通过)")
	private Integer auditStatus;
}
