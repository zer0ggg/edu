package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthCourseUserStudyLogPageBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseUserStudyLogPageDTO;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.CourseUserStudyLogDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudyLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudyLogExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseUserStudyLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程信息
 *
 * @author wujing
 */
@Component
public class AuthCourseUserStudyLogBiz extends BaseBiz {

    @Autowired
    private CourseUserStudyLogDao courseUserStudyLogDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterPeriodDao periodDao;

    /**
     * 最近学习日志分页列出接口
     *
     * @author wuyun
     */
    public Result<Page<AuthCourseUserStudyLogPageDTO>> list(AuthCourseUserStudyLogPageBO authCourseUserStudyLogPageBO) {
        CourseUserStudyLogExample example = new CourseUserStudyLogExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(ThreadContext.userNo());
        example.setOrderByClause("id desc");
        Page<CourseUserStudyLog> page = courseUserStudyLogDao.listForPage(authCourseUserStudyLogPageBO.getPageCurrent(), authCourseUserStudyLogPageBO.getPageSize(), example);
        Page<AuthCourseUserStudyLogPageDTO> dtoList = PageUtil.transform(page, AuthCourseUserStudyLogPageDTO.class);
        for (AuthCourseUserStudyLogPageDTO dto : dtoList.getList()) {
            // 查看课程信息
            Course course = courseDao.getById(dto.getCourseId());
            if (ObjectUtil.isNotNull(course)) {
                dto.setCourseName(course.getCourseName());
            }
            // 查看课时信息
            CourseChapterPeriod period = periodDao.getById(dto.getPeriodId());
            if (ObjectUtil.isNotNull(period)) {
                dto.setPeriodName(period.getPeriodName());
            }
        }
        return Result.success(dtoList);
    }

}
