package com.roncoo.education.course.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.CourseVideoMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseVideoDaoImpl implements CourseVideoDao {
    @Autowired
    private CourseVideoMapper courseVideoMapper;

    @Override
    public int save(CourseVideo record) {
        return this.courseVideoMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.courseVideoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseVideo record) {
        return this.courseVideoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseVideo getById(Long id) {
        return this.courseVideoMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseVideo> listForPage(int pageCurrent, int pageSize, CourseVideoExample example) {
        int count = this.courseVideoMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<CourseVideo>(count, totalPage, pageCurrent, pageSize, this.courseVideoMapper.selectByExample(example));
    }

    @Override
    public List<CourseVideo> listByChapterIdAndStatusId(Long chapterId, Integer statusId) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        c.andChapterIdEqualTo(chapterId);
        c.andStatusIdEqualTo(statusId);
        return this.courseVideoMapper.selectByExample(example);
    }

    @Override
    public Integer updateDurationByVid(CourseVideo courseVideo) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        c.andVideoVidEqualTo(courseVideo.getVideoVid());
        return this.courseVideoMapper.updateByExampleSelective(courseVideo, example);
    }

    @Override
    public CourseVideo getByVid(String videoVid) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        c.andVideoVidEqualTo(videoVid);
        List<CourseVideo> list = this.courseVideoMapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseVideo getByVideoBackupPlatformAndVideoBackupVid(Integer videoBackupPlatform, String videoBackupVid) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        c.andVideoBackupPlatformEqualTo(videoBackupPlatform);
        c.andVideoBackupVidEqualTo(videoBackupVid);
        List<CourseVideo> resultList = courseVideoMapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }

    @Override
    public CourseVideo getByVodPlatformAndVideoVid(Integer vodPlatform, String videoVid) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        c.andVodPlatformEqualTo(vodPlatform);
        c.andVideoVidEqualTo(videoVid);
        List<CourseVideo> resultList = courseVideoMapper.selectByExample(example);
        return CollectionUtil.isEmpty(resultList) ? null : resultList.get(0);
    }
}
