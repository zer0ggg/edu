package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignResourceRecommend;
import com.roncoo.education.course.feign.qo.ResourceRecommendQO;
import com.roncoo.education.course.feign.vo.ResourceRecommendVO;
import com.roncoo.education.course.service.feign.biz.FeignResourceRecommendBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 文库推荐
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@RestController
public class FeignResourceRecommendController extends BaseController implements IFeignResourceRecommend{

    @Autowired
    private FeignResourceRecommendBiz biz;


	@Override
	public Page<ResourceRecommendVO> listForPage(@RequestBody ResourceRecommendQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody ResourceRecommendQO qo) {
	return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
	return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody ResourceRecommendQO qo) {
	return biz.updateById(qo);
	}

	@Override
	public ResourceRecommendVO getById(@PathVariable(value = "id") Long id) {
	return biz.getById(id);
	}
}
