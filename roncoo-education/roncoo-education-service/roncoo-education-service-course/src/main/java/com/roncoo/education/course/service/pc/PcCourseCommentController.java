package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseCommentEditREQ;
import com.roncoo.education.course.common.req.CourseCommentListREQ;
import com.roncoo.education.course.common.req.CourseCommentSaveREQ;
import com.roncoo.education.course.common.resp.CourseCommentListRESP;
import com.roncoo.education.course.common.resp.CourseCommentViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseCommentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程评论 Pc接口
 *
 * @author Quanf
 */
@Api(tags = "PC-课程评论")
@RestController
@RequestMapping("/course/pc/course/comment")
public class PcCourseCommentController {

    @Autowired
    private PcCourseCommentBiz biz;

    @ApiOperation(value = "课程评论列表", notes = "课程评论列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseCommentListRESP>> list(@RequestBody CourseCommentListREQ courseCommentListREQ) {
        return biz.list(courseCommentListREQ);
    }

    @ApiOperation(value = "课程评论添加", notes = "课程评论添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseCommentSaveREQ courseCommentSaveREQ) {
        return biz.save(courseCommentSaveREQ);
    }

    @ApiOperation(value = "课程评论查看", notes = "课程评论查看")
    @GetMapping(value = "/view")
    public Result<CourseCommentViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程评论修改", notes = "课程评论修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseCommentEditREQ courseCommentEditREQ) {
        return biz.edit(courseCommentEditREQ);
    }

    @ApiOperation(value = "课程评论删除", notes = "课程评论删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
