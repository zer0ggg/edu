package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 附件保存
 *
 * @author kyh
 *
 */
@Data
@Accessors(chain = true)
public class AuthCourseAccessoryBO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 附件名称
	 */
	@ApiModelProperty(value = "附件名称", required = true)
	private String acName;
	/**
	 * 附件地址
	 */
	@ApiModelProperty(value = "附件地址", required = true)
	private String acUrl;
}
