package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class AuthCourseCommentUserSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "parentId父ID")
    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @NotNull(message = "courseId课程ID")
    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @NotNull(message = "courseUserNo被评论者用户编号")
    @ApiModelProperty(value = "被评论者用户编号")
    private Long courseUserNo;

    @NotNull(message = "content评论内容")
    @ApiModelProperty(value = "评论内容")
    private String content;

    @NotNull(message = "userIp用户IP")
    @ApiModelProperty(value = "用户IP")
    private String userIp;

    @NotNull(message = "userTerminal用户终端")
    @ApiModelProperty(value = "用户终端")
    private String userTerminal;

}
