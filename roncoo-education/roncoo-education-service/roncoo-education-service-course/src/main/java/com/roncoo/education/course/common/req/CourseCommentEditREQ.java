package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程评论
 * </p>
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseCommentEditREQ", description = "课程评论修改")
public class CourseCommentEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "评论内容")
    private String content;
}
