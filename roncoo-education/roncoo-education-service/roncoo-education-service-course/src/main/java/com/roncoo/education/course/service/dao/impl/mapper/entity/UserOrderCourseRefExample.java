package com.roncoo.education.course.service.dao.impl.mapper.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserOrderCourseRefExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected int limitStart = -1;

    protected int pageSize = -1;

    public UserOrderCourseRefExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(int limitStart) {
        this.limitStart=limitStart;
    }

    public int getLimitStart() {
        return limitStart;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNull() {
            addCriterion("status_id is null");
            return (Criteria) this;
        }

        public Criteria andStatusIdIsNotNull() {
            addCriterion("status_id is not null");
            return (Criteria) this;
        }

        public Criteria andStatusIdEqualTo(Integer value) {
            addCriterion("status_id =", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotEqualTo(Integer value) {
            addCriterion("status_id <>", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThan(Integer value) {
            addCriterion("status_id >", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("status_id >=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThan(Integer value) {
            addCriterion("status_id <", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdLessThanOrEqualTo(Integer value) {
            addCriterion("status_id <=", value, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdIn(List<Integer> values) {
            addCriterion("status_id in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotIn(List<Integer> values) {
            addCriterion("status_id not in", values, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdBetween(Integer value1, Integer value2) {
            addCriterion("status_id between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andStatusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("status_id not between", value1, value2, "statusId");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoIsNull() {
            addCriterion("lecturer_user_no is null");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoIsNotNull() {
            addCriterion("lecturer_user_no is not null");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoEqualTo(Long value) {
            addCriterion("lecturer_user_no =", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoNotEqualTo(Long value) {
            addCriterion("lecturer_user_no <>", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoGreaterThan(Long value) {
            addCriterion("lecturer_user_no >", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("lecturer_user_no >=", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoLessThan(Long value) {
            addCriterion("lecturer_user_no <", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoLessThanOrEqualTo(Long value) {
            addCriterion("lecturer_user_no <=", value, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoIn(List<Long> values) {
            addCriterion("lecturer_user_no in", values, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoNotIn(List<Long> values) {
            addCriterion("lecturer_user_no not in", values, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoBetween(Long value1, Long value2) {
            addCriterion("lecturer_user_no between", value1, value2, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andLecturerUserNoNotBetween(Long value1, Long value2) {
            addCriterion("lecturer_user_no not between", value1, value2, "lecturerUserNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNull() {
            addCriterion("user_no is null");
            return (Criteria) this;
        }

        public Criteria andUserNoIsNotNull() {
            addCriterion("user_no is not null");
            return (Criteria) this;
        }

        public Criteria andUserNoEqualTo(Long value) {
            addCriterion("user_no =", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotEqualTo(Long value) {
            addCriterion("user_no <>", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThan(Long value) {
            addCriterion("user_no >", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoGreaterThanOrEqualTo(Long value) {
            addCriterion("user_no >=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThan(Long value) {
            addCriterion("user_no <", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoLessThanOrEqualTo(Long value) {
            addCriterion("user_no <=", value, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoIn(List<Long> values) {
            addCriterion("user_no in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotIn(List<Long> values) {
            addCriterion("user_no not in", values, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoBetween(Long value1, Long value2) {
            addCriterion("user_no between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andUserNoNotBetween(Long value1, Long value2) {
            addCriterion("user_no not between", value1, value2, "userNo");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNull() {
            addCriterion("course_category is null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIsNotNull() {
            addCriterion("course_category is not null");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryEqualTo(Integer value) {
            addCriterion("course_category =", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotEqualTo(Integer value) {
            addCriterion("course_category <>", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThan(Integer value) {
            addCriterion("course_category >", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_category >=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThan(Integer value) {
            addCriterion("course_category <", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryLessThanOrEqualTo(Integer value) {
            addCriterion("course_category <=", value, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryIn(List<Integer> values) {
            addCriterion("course_category in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotIn(List<Integer> values) {
            addCriterion("course_category not in", values, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryBetween(Integer value1, Integer value2) {
            addCriterion("course_category between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseCategoryNotBetween(Integer value1, Integer value2) {
            addCriterion("course_category not between", value1, value2, "courseCategory");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNull() {
            addCriterion("course_type is null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIsNotNull() {
            addCriterion("course_type is not null");
            return (Criteria) this;
        }

        public Criteria andCourseTypeEqualTo(Integer value) {
            addCriterion("course_type =", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotEqualTo(Integer value) {
            addCriterion("course_type <>", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThan(Integer value) {
            addCriterion("course_type >", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_type >=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThan(Integer value) {
            addCriterion("course_type <", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeLessThanOrEqualTo(Integer value) {
            addCriterion("course_type <=", value, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeIn(List<Integer> values) {
            addCriterion("course_type in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotIn(List<Integer> values) {
            addCriterion("course_type not in", values, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeBetween(Integer value1, Integer value2) {
            addCriterion("course_type between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("course_type not between", value1, value2, "courseType");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Long value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Long value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Long value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Long value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Long value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Long> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Long> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Long value1, Long value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Long value1, Long value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andRefIdIsNull() {
            addCriterion("ref_id is null");
            return (Criteria) this;
        }

        public Criteria andRefIdIsNotNull() {
            addCriterion("ref_id is not null");
            return (Criteria) this;
        }

        public Criteria andRefIdEqualTo(Long value) {
            addCriterion("ref_id =", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdNotEqualTo(Long value) {
            addCriterion("ref_id <>", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdGreaterThan(Long value) {
            addCriterion("ref_id >", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdGreaterThanOrEqualTo(Long value) {
            addCriterion("ref_id >=", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdLessThan(Long value) {
            addCriterion("ref_id <", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdLessThanOrEqualTo(Long value) {
            addCriterion("ref_id <=", value, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdIn(List<Long> values) {
            addCriterion("ref_id in", values, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdNotIn(List<Long> values) {
            addCriterion("ref_id not in", values, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdBetween(Long value1, Long value2) {
            addCriterion("ref_id between", value1, value2, "refId");
            return (Criteria) this;
        }

        public Criteria andRefIdNotBetween(Long value1, Long value2) {
            addCriterion("ref_id not between", value1, value2, "refId");
            return (Criteria) this;
        }

        public Criteria andIsPayIsNull() {
            addCriterion("is_pay is null");
            return (Criteria) this;
        }

        public Criteria andIsPayIsNotNull() {
            addCriterion("is_pay is not null");
            return (Criteria) this;
        }

        public Criteria andIsPayEqualTo(Integer value) {
            addCriterion("is_pay =", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotEqualTo(Integer value) {
            addCriterion("is_pay <>", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayGreaterThan(Integer value) {
            addCriterion("is_pay >", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_pay >=", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayLessThan(Integer value) {
            addCriterion("is_pay <", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayLessThanOrEqualTo(Integer value) {
            addCriterion("is_pay <=", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayIn(List<Integer> values) {
            addCriterion("is_pay in", values, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotIn(List<Integer> values) {
            addCriterion("is_pay not in", values, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayBetween(Integer value1, Integer value2) {
            addCriterion("is_pay between", value1, value2, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotBetween(Integer value1, Integer value2) {
            addCriterion("is_pay not between", value1, value2, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsStudyIsNull() {
            addCriterion("is_study is null");
            return (Criteria) this;
        }

        public Criteria andIsStudyIsNotNull() {
            addCriterion("is_study is not null");
            return (Criteria) this;
        }

        public Criteria andIsStudyEqualTo(Integer value) {
            addCriterion("is_study =", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyNotEqualTo(Integer value) {
            addCriterion("is_study <>", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyGreaterThan(Integer value) {
            addCriterion("is_study >", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_study >=", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyLessThan(Integer value) {
            addCriterion("is_study <", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyLessThanOrEqualTo(Integer value) {
            addCriterion("is_study <=", value, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyIn(List<Integer> values) {
            addCriterion("is_study in", values, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyNotIn(List<Integer> values) {
            addCriterion("is_study not in", values, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyBetween(Integer value1, Integer value2) {
            addCriterion("is_study between", value1, value2, "isStudy");
            return (Criteria) this;
        }

        public Criteria andIsStudyNotBetween(Integer value1, Integer value2) {
            addCriterion("is_study not between", value1, value2, "isStudy");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(Long value) {
            addCriterion("order_no =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(Long value) {
            addCriterion("order_no <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(Long value) {
            addCriterion("order_no >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(Long value) {
            addCriterion("order_no >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(Long value) {
            addCriterion("order_no <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(Long value) {
            addCriterion("order_no <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<Long> values) {
            addCriterion("order_no in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<Long> values) {
            addCriterion("order_no not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(Long value1, Long value2) {
            addCriterion("order_no between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(Long value1, Long value2) {
            addCriterion("order_no not between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andExpireTimeIsNull() {
            addCriterion("expire_time is null");
            return (Criteria) this;
        }

        public Criteria andExpireTimeIsNotNull() {
            addCriterion("expire_time is not null");
            return (Criteria) this;
        }

        public Criteria andExpireTimeEqualTo(Date value) {
            addCriterion("expire_time =", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeNotEqualTo(Date value) {
            addCriterion("expire_time <>", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeGreaterThan(Date value) {
            addCriterion("expire_time >", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("expire_time >=", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeLessThan(Date value) {
            addCriterion("expire_time <", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeLessThanOrEqualTo(Date value) {
            addCriterion("expire_time <=", value, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeIn(List<Date> values) {
            addCriterion("expire_time in", values, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeNotIn(List<Date> values) {
            addCriterion("expire_time not in", values, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeBetween(Date value1, Date value2) {
            addCriterion("expire_time between", value1, value2, "expireTime");
            return (Criteria) this;
        }

        public Criteria andExpireTimeNotBetween(Date value1, Date value2) {
            addCriterion("expire_time not between", value1, value2, "expireTime");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIsNull() {
            addCriterion("course_length is null");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIsNotNull() {
            addCriterion("course_length is not null");
            return (Criteria) this;
        }

        public Criteria andCourseLengthEqualTo(String value) {
            addCriterion("course_length =", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotEqualTo(String value) {
            addCriterion("course_length <>", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthGreaterThan(String value) {
            addCriterion("course_length >", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthGreaterThanOrEqualTo(String value) {
            addCriterion("course_length >=", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLessThan(String value) {
            addCriterion("course_length <", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLessThanOrEqualTo(String value) {
            addCriterion("course_length <=", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthLike(String value) {
            addCriterion("course_length like", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotLike(String value) {
            addCriterion("course_length not like", value, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthIn(List<String> values) {
            addCriterion("course_length in", values, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotIn(List<String> values) {
            addCriterion("course_length not in", values, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthBetween(String value1, String value2) {
            addCriterion("course_length between", value1, value2, "courseLength");
            return (Criteria) this;
        }

        public Criteria andCourseLengthNotBetween(String value1, String value2) {
            addCriterion("course_length not between", value1, value2, "courseLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIsNull() {
            addCriterion("study_length is null");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIsNotNull() {
            addCriterion("study_length is not null");
            return (Criteria) this;
        }

        public Criteria andStudyLengthEqualTo(BigDecimal value) {
            addCriterion("study_length =", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotEqualTo(BigDecimal value) {
            addCriterion("study_length <>", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthGreaterThan(BigDecimal value) {
            addCriterion("study_length >", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("study_length >=", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthLessThan(BigDecimal value) {
            addCriterion("study_length <", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("study_length <=", value, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthIn(List<BigDecimal> values) {
            addCriterion("study_length in", values, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotIn(List<BigDecimal> values) {
            addCriterion("study_length not in", values, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("study_length between", value1, value2, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyLengthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("study_length not between", value1, value2, "studyLength");
            return (Criteria) this;
        }

        public Criteria andStudyProcessIsNull() {
            addCriterion("study_process is null");
            return (Criteria) this;
        }

        public Criteria andStudyProcessIsNotNull() {
            addCriterion("study_process is not null");
            return (Criteria) this;
        }

        public Criteria andStudyProcessEqualTo(Integer value) {
            addCriterion("study_process =", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessNotEqualTo(Integer value) {
            addCriterion("study_process <>", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessGreaterThan(Integer value) {
            addCriterion("study_process >", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessGreaterThanOrEqualTo(Integer value) {
            addCriterion("study_process >=", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessLessThan(Integer value) {
            addCriterion("study_process <", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessLessThanOrEqualTo(Integer value) {
            addCriterion("study_process <=", value, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessIn(List<Integer> values) {
            addCriterion("study_process in", values, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessNotIn(List<Integer> values) {
            addCriterion("study_process not in", values, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessBetween(Integer value1, Integer value2) {
            addCriterion("study_process between", value1, value2, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andStudyProcessNotBetween(Integer value1, Integer value2) {
            addCriterion("study_process not between", value1, value2, "studyProcess");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdIsNull() {
            addCriterion("last_study_period_id is null");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdIsNotNull() {
            addCriterion("last_study_period_id is not null");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdEqualTo(Long value) {
            addCriterion("last_study_period_id =", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdNotEqualTo(Long value) {
            addCriterion("last_study_period_id <>", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdGreaterThan(Long value) {
            addCriterion("last_study_period_id >", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdGreaterThanOrEqualTo(Long value) {
            addCriterion("last_study_period_id >=", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdLessThan(Long value) {
            addCriterion("last_study_period_id <", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdLessThanOrEqualTo(Long value) {
            addCriterion("last_study_period_id <=", value, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdIn(List<Long> values) {
            addCriterion("last_study_period_id in", values, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdNotIn(List<Long> values) {
            addCriterion("last_study_period_id not in", values, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdBetween(Long value1, Long value2) {
            addCriterion("last_study_period_id between", value1, value2, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyPeriodIdNotBetween(Long value1, Long value2) {
            addCriterion("last_study_period_id not between", value1, value2, "lastStudyPeriodId");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeIsNull() {
            addCriterion("last_study_time is null");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeIsNotNull() {
            addCriterion("last_study_time is not null");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeEqualTo(Date value) {
            addCriterion("last_study_time =", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeNotEqualTo(Date value) {
            addCriterion("last_study_time <>", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeGreaterThan(Date value) {
            addCriterion("last_study_time >", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("last_study_time >=", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeLessThan(Date value) {
            addCriterion("last_study_time <", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeLessThanOrEqualTo(Date value) {
            addCriterion("last_study_time <=", value, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeIn(List<Date> values) {
            addCriterion("last_study_time in", values, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeNotIn(List<Date> values) {
            addCriterion("last_study_time not in", values, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeBetween(Date value1, Date value2) {
            addCriterion("last_study_time between", value1, value2, "lastStudyTime");
            return (Criteria) this;
        }

        public Criteria andLastStudyTimeNotBetween(Date value1, Date value2) {
            addCriterion("last_study_time not between", value1, value2, "lastStudyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}