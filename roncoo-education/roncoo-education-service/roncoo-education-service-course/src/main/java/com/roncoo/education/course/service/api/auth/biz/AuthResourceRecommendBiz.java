package com.roncoo.education.course.service.api.auth.biz;

import com.roncoo.education.course.service.dao.ResourceRecommendDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文库推荐
 *
 * @author wujing
 */
@Component
public class AuthResourceRecommendBiz {

    @Autowired
    private ResourceRecommendDao dao;

}
