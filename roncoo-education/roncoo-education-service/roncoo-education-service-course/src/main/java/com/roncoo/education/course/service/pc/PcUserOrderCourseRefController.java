package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.UserOrderCourseRefEditREQ;
import com.roncoo.education.course.common.req.UserOrderCourseRefListREQ;
import com.roncoo.education.course.common.req.UserOrderCourseRefSaveREQ;
import com.roncoo.education.course.common.resp.UserOrderCourseRefListRESP;
import com.roncoo.education.course.common.resp.UserOrderCourseRefViewRESP;
import com.roncoo.education.course.service.pc.biz.PcUserOrderCourseRefBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户订单课程关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/user/order/course/ref")
@Api(value = "course-用户订单课程关联", tags = {"course-用户订单课程关联"})
public class PcUserOrderCourseRefController {

    @Autowired
    private PcUserOrderCourseRefBiz biz;

    @ApiOperation(value = "用户订单课程关联列表", notes = "用户订单课程关联列表")
    @PostMapping(value = "/list")
    public Result<Page<UserOrderCourseRefListRESP>> list(@RequestBody UserOrderCourseRefListREQ userOrderCourseRefListREQ) {
        return biz.list(userOrderCourseRefListREQ);
    }

    @ApiOperation(value = "用户订单课程关联添加", notes = "用户订单课程关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserOrderCourseRefSaveREQ userOrderCourseRefSaveREQ) {
        return biz.save(userOrderCourseRefSaveREQ);
    }

    @ApiOperation(value = "用户订单课程关联查看", notes = "用户订单课程关联查看")
    @GetMapping(value = "/view")
    public Result<UserOrderCourseRefViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "用户订单课程关联修改", notes = "用户订单课程关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserOrderCourseRefEditREQ userOrderCourseRefEditREQ) {
        return biz.edit(userOrderCourseRefEditREQ);
    }

    @ApiOperation(value = "用户订单课程关联删除", notes = "用户订单课程关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
