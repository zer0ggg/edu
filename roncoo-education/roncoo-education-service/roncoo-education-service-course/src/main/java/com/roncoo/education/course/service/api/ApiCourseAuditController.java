package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.CourseAuditViewBO;
import com.roncoo.education.course.common.dto.CourseAuditViewDTO;
import com.roncoo.education.course.service.api.biz.ApiCourseAuditBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/api/course/audit")
public class ApiCourseAuditController extends BaseController {

	@Autowired
	private ApiCourseAuditBiz biz;

	/**
	 * 普通课程、直播、试卷详情接口(管理员预览)
	 */
	@ApiOperation(value = "普通课程、直播、文库详情接口(管理员预览)", notes = "根据课程ID返回普通课程、直播、文库详情信息(管理员预览)")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<CourseAuditViewDTO> view(@RequestBody CourseAuditViewBO courseAuditViewBO) {
		return biz.view(courseAuditViewBO);
	}

}
