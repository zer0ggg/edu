package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.CategoryTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseCategoryChooseListRESP;
import com.roncoo.education.course.common.resp.CourseCategoryChooseRESP;
import com.roncoo.education.course.common.resp.CourseCategoryListRESP;
import com.roncoo.education.course.common.resp.CourseCategoryViewRESP;
import com.roncoo.education.course.service.dao.CourseCategoryDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCategory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCategoryExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCategoryExample.Criteria;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程分类
 *
 * @author wujing
 */
@Component
public class PcCourseCategoryBiz extends BaseBiz {

    @Autowired
    private CourseCategoryDao dao;

    /**
     * 课程分类列表
     *
     * @param courseCategoryListREQ 课程分类分页查询参数
     * @return 课程分类分页查询结果
     */
    public Result<Page<CourseCategoryListRESP>> list(CourseCategoryListREQ courseCategoryListREQ) {
        CourseCategoryExample example = new CourseCategoryExample();
        Criteria c = example.createCriteria();
        if (courseCategoryListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(courseCategoryListREQ.getStatusId());
        }
        if (courseCategoryListREQ.getCategoryType() != null) {
            c.andCategoryTypeEqualTo(courseCategoryListREQ.getCategoryType());
        }
        if (!StringUtils.isEmpty(courseCategoryListREQ.getCategoryName())) {
            c.andCategoryNameLike(PageUtil.like(courseCategoryListREQ.getCategoryName()));
        } else {
            c.andFloorEqualTo(1);
        }
        example.setOrderByClause(" category_type asc, sort asc, id desc ");
        Page<CourseCategory> page = dao.listForPage(courseCategoryListREQ.getPageCurrent(), courseCategoryListREQ.getPageSize(), example);
        Page<CourseCategoryListRESP> respPage = PageUtil.transform(page, CourseCategoryListRESP.class);
        for (CourseCategoryListRESP courseCategoryListRESP : respPage.getList()) {
            courseCategoryListRESP.setChildrenList(recursionList(courseCategoryListRESP.getId()));
        }
        return Result.success(respPage);
    }


    /**
     * 课程分类添加
     *
     * @param courseCategorySaveREQ 课程分类
     * @return 添加结果
     */
    public Result<String> save(CourseCategorySaveREQ courseCategorySaveREQ) {
        CourseCategory record = BeanUtil.copyProperties(courseCategorySaveREQ, CourseCategory.class);
        if (record.getParentId() != 0 || record.getFloor() != 1) {
            record.setFloor(record.getFloor() + 1);
        }
        if (record.getFloor() > 3) {
            return Result.error("只能添加三分类");
        }
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 课程分类查看
     *
     * @param id 主键ID
     * @return 课程分类
     */
    public Result<CourseCategoryViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseCategoryViewRESP.class));
    }


    /**
     * 课程分类修改
     *
     * @param courseCategoryEditREQ 课程分类修改对象
     * @return 修改结果
     */
    public Result<String> edit(CourseCategoryEditREQ courseCategoryEditREQ) {
        CourseCategory record = BeanUtil.copyProperties(courseCategoryEditREQ, CourseCategory.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 修改状态
     *
     * @param courseCategoryUpdateStatusREQ 课程分类状态修改参数
     * @return 修改结果
     */
    public Result<String> updateStatusId(CourseCategoryUpdateStatusREQ courseCategoryUpdateStatusREQ) {
        StatusIdEnum statusIdEnum = StatusIdEnum.byCode(courseCategoryUpdateStatusREQ.getStatusId());
        if (ObjectUtil.isNull(statusIdEnum)) {
            return Result.error("状态不正确");
        }
        CourseCategory viewCourseCategory = dao.getById(courseCategoryUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(viewCourseCategory)) {
            return Result.error("分类不存在");
        }

        CourseCategory record = BeanUtil.copyProperties(courseCategoryUpdateStatusREQ, CourseCategory.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 课程分类删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        CourseCategory courseCategory = dao.getById(id);
        if (ObjectUtil.isNull(courseCategory)) {
            return Result.error("找不到课程分类信息");
        }
        List<CourseCategory> list = dao.listByParentId(id);
        if (list.size() > 0) {
            return Result.error("删除失败，请先删除子分类");
        }
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 递归展示分类
     */
    private List<CourseCategoryListRESP> recursionList(Long parentId) {
        List<CourseCategoryListRESP> list = new ArrayList<>();
        List<CourseCategory> CourseCategoryList = dao.listByParentId(parentId);
        if (CollectionUtils.isNotEmpty(CourseCategoryList)) {
            for (CourseCategory courseCategory : CourseCategoryList) {
                CourseCategoryListRESP resp = BeanUtil.copyProperties(courseCategory, CourseCategoryListRESP.class);
                resp.setChildrenList(recursionList(courseCategory.getId()));
                list.add(resp);
            }
        }
        return list;
    }

    /**
     * 获取课程分类列表
     *
     * @return
     * @author wuyun
     */
    public Result<CourseCategoryChooseRESP> choose(CourseCategoryChooseREQ bo) {
        if (bo.getCategoryType() == null) {
            bo.setCategoryType(CategoryTypeEnum.COURSE.getCode());
        }
        CourseCategoryChooseRESP dto = new CourseCategoryChooseRESP();
        // 根据分类类型、层级查询可用状态的课程分类集合
        List<CourseCategory> oneCategoryList = dao.listByCategoryTypeAndFloorAndStatusId(bo.getCategoryType(), 1, StatusIdEnum.YES.getCode());
        if (CollectionUtils.isEmpty(oneCategoryList)) {
            return Result.success(dto);
        }
        List<CourseCategoryChooseListRESP> oneList = new ArrayList<>();
        // 查找一级分类下的二级分类
        for (CourseCategory courseCategory : oneCategoryList) {
            // 设置一级分类
            CourseCategoryChooseListRESP oneCategory = new CourseCategoryChooseListRESP();
            oneCategory.setLabel(courseCategory.getCategoryName());
            oneCategory.setValue(courseCategory.getId());
            // 查找一级分类下的二级分类
            List<CourseCategory> twoCategoryList = dao.listByParentIdAndStatusId(courseCategory.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtils.isEmpty(twoCategoryList)) {
                oneList.add(oneCategory);
                dto.setList(oneList);
                continue;
            }
            List<CourseCategoryChooseListRESP> twoList = new ArrayList<>();

            for (CourseCategory twoCategory : twoCategoryList) {
                CourseCategoryChooseListRESP twoDto = new CourseCategoryChooseListRESP();
                twoDto.setLabel(twoCategory.getCategoryName());
                twoDto.setValue(twoCategory.getId());
                List<CourseCategoryChooseListRESP> threeList = new ArrayList<>();
                List<CourseCategory> threeCategoryList = dao.listByParentIdAndStatusId(twoCategory.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isNotEmpty(threeCategoryList)) {
                    for (CourseCategory threeCategory : threeCategoryList) {
                        CourseCategoryChooseListRESP three = new CourseCategoryChooseListRESP();
                        three.setLabel(threeCategory.getCategoryName());
                        three.setValue(threeCategory.getId());
                        threeList.add(three);
                    }
                    // 复制三级分类信息返回
                    twoDto.setChildren(threeList);
                }
                twoList.add(twoDto);
            }
            oneCategory.setChildren(twoList);
            // 复制二级分类信息返回
            oneList.add(oneCategory);
            dto.setList(oneList);
        }
        return Result.success(dto);
    }
}
