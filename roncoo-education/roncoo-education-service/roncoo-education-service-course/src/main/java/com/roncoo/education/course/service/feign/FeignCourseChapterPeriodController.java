package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignCourseChapterPeriod;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.service.feign.biz.FeignCourseChapterPeriodBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@RestController
public class FeignCourseChapterPeriodController extends BaseController implements IFeignCourseChapterPeriod {

	@Autowired
	private FeignCourseChapterPeriodBiz biz;

	@Override
	public Page<CourseChapterPeriodVO> listForPage(@RequestBody CourseChapterPeriodQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseChapterPeriodQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseChapterPeriodQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public CourseChapterPeriodVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

    @Override
    public CourseChapterPeriodVO getByCourseChapterPeriodId(@RequestBody CourseChapterPeriodQO qo) {
		return biz.getByCourseChapterPeriodId(qo);
    }

    @Override
    public List<CourseChapterPeriodVO> ListByCourseId(@RequestBody CourseChapterPeriodQO qo) {
        return biz.ListByCourseId(qo);
    }

    @Override
    public List<CourseChapterPeriodVO> listByIds(@RequestBody CourseChapterPeriodQO qo) {
        return biz.listByIds(qo);
    }

}
