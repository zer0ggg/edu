package com.roncoo.education.course.service.feign;

import com.roncoo.education.course.feign.interfaces.IFeignCallbackTalkFunLive;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveDocumentAddQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLivePlaybackQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStartQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStopQO;
import com.roncoo.education.course.service.feign.biz.FeignCallbackTalkFunLiveBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 欢拓直播回调
 *
 * @author LYQ
 */
@RestController
public class FeignCallbackTalkFunLiveController implements IFeignCallbackTalkFunLive {

    @Autowired
    private FeignCallbackTalkFunLiveBiz biz;

    @Override
    public Boolean liveStart(@RequestBody TalkFunCallbackLiveStartQO liveStartQO) {
        return biz.liveStart(liveStartQO);
    }

    @Override
    public Boolean liveStop(@RequestBody TalkFunCallbackLiveStopQO liveStopQO) {
        return biz.liveStop(liveStopQO);
    }

    @Override
    public Boolean livePlayback(@RequestBody TalkFunCallbackLivePlaybackQO livePlaybackQO) {
        return biz.livePlayback(livePlaybackQO);
    }

    @Override
    public Boolean liveDocumentAdd(@RequestBody TalkFunCallbackLiveDocumentAddQO liveDocumentAddQO) {
        return biz.liveDocumentAdd(liveDocumentAddQO);
    }
}
