package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CrontabPlanPolyvEditREQ;
import com.roncoo.education.course.common.req.CrontabPlanPolyvListREQ;
import com.roncoo.education.course.common.req.CrontabPlanPolyvSaveREQ;
import com.roncoo.education.course.common.resp.CrontabPlanPolyvListRESP;
import com.roncoo.education.course.common.resp.CrontabPlanPolyvViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCrontabPlanPolyvBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 直播转点存定时任务 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/crontab/plan/polyv")
@Api(value = "course-直播转点存定时任务", tags = {"course-直播转点存定时任务"})
public class PcCrontabPlanPolyvController {

    @Autowired
    private PcCrontabPlanPolyvBiz biz;

    @ApiOperation(value = "直播转点存定时任务列表", notes = "直播转点存定时任务列表")
    @PostMapping(value = "/list")
    public Result<Page<CrontabPlanPolyvListRESP>> list(@RequestBody CrontabPlanPolyvListREQ crontabPlanPolyvListREQ) {
        return biz.list(crontabPlanPolyvListREQ);
    }

    @ApiOperation(value = "直播转点存定时任务添加", notes = "直播转点存定时任务添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CrontabPlanPolyvSaveREQ crontabPlanPolyvSaveREQ) {
        return biz.save(crontabPlanPolyvSaveREQ);
    }

    @ApiOperation(value = "直播转点存定时任务查看", notes = "直播转点存定时任务查看")
    @GetMapping(value = "/view")
    public Result<CrontabPlanPolyvViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "直播转点存定时任务修改", notes = "直播转点存定时任务修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CrontabPlanPolyvEditREQ crontabPlanPolyvEditREQ) {
        return biz.edit(crontabPlanPolyvEditREQ);
    }

    @ApiOperation(value = "直播转点存定时任务删除", notes = "直播转点存定时任务删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
