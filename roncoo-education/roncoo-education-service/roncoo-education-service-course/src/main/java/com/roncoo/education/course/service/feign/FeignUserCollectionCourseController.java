package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignUserCollectionCourse;
import com.roncoo.education.course.feign.qo.UserCollectionCourseQO;
import com.roncoo.education.course.feign.vo.UserCollectionCourseVO;
import com.roncoo.education.course.service.feign.biz.FeignUserCollectionCourseBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@RestController
public class FeignUserCollectionCourseController extends BaseController implements IFeignUserCollectionCourse{

	@Autowired
	private FeignUserCollectionCourseBiz biz;

	@Override
	public Page<UserCollectionCourseVO> listForPage(@RequestBody UserCollectionCourseQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody UserCollectionCourseQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@RequestBody Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody UserCollectionCourseQO qo){
		return biz.updateById(qo);
	}

    @Override
	public UserCollectionCourseVO getById(@RequestBody Long id){
		return biz.getById(id);
	}

}
