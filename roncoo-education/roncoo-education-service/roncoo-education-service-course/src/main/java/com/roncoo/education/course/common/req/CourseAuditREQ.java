package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程信息-审核
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseAuditREQ", description = "课程信息-审核")
public class CourseAuditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)", required = true)
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见", required = true)
    private String auditOpinion;
}
