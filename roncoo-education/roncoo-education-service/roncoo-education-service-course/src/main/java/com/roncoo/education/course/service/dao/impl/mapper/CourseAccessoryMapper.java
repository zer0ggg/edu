package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseAccessoryMapper {
    int countByExample(CourseAccessoryExample example);

    int deleteByExample(CourseAccessoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseAccessory record);

    int insertSelective(CourseAccessory record);

    List<CourseAccessory> selectByExample(CourseAccessoryExample example);

    CourseAccessory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseAccessory record, @Param("example") CourseAccessoryExample example);

    int updateByExample(@Param("record") CourseAccessory record, @Param("example") CourseAccessoryExample example);

    int updateByPrimaryKeySelective(CourseAccessory record);

    int updateByPrimaryKey(CourseAccessory record);
}
