package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseLiveLogEditREQ;
import com.roncoo.education.course.common.req.CourseLiveLogListREQ;
import com.roncoo.education.course.common.req.CourseLiveLogSaveREQ;
import com.roncoo.education.course.common.resp.CourseLiveLogListRESP;
import com.roncoo.education.course.common.resp.CourseLiveLogViewRESP;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 直播记录
 *
 * @author wujing
 */
@Component
public class PcCourseLiveLogBiz extends BaseBiz {

    @Autowired
    private CourseLiveLogDao dao;

    /**
     * 直播记录列表
     *
     * @param courseLiveLogListREQ 直播记录分页查询参数
     * @return 直播记录分页查询结果
     */
    public Result<Page<CourseLiveLogListRESP>> list(CourseLiveLogListREQ courseLiveLogListREQ) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        Page<CourseLiveLog> page = dao.listForPage(courseLiveLogListREQ.getPageCurrent(), courseLiveLogListREQ.getPageSize(), example);
        Page<CourseLiveLogListRESP> respPage = PageUtil.transform(page, CourseLiveLogListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 直播记录添加
     *
     * @param courseLiveLogSaveREQ 直播记录
     * @return 添加结果
     */
    public Result<String> save(CourseLiveLogSaveREQ courseLiveLogSaveREQ) {
        // TODO 需要改造（兼用欢拓）
        CourseLiveLog record = BeanUtil.copyProperties(courseLiveLogSaveREQ, CourseLiveLog.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 直播记录查看
     *
     * @param id 主键ID
     * @return 直播记录
     */
    public Result<CourseLiveLogViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseLiveLogViewRESP.class));
    }


    /**
     * 直播记录修改
     *
     * @param courseLiveLogEditREQ 直播记录修改对象
     * @return 修改结果
     */
    public Result<String> edit(CourseLiveLogEditREQ courseLiveLogEditREQ) {
        CourseLiveLog record = BeanUtil.copyProperties(courseLiveLogEditREQ, CourseLiveLog.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 直播记录删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
