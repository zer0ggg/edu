package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SqlUtil;
import com.roncoo.education.common.elasticsearch.EsPageUtil;
import com.roncoo.education.course.common.bo.CourseInfoPageBO;
import com.roncoo.education.course.common.bo.CourseInfoSearchBO;
import com.roncoo.education.course.common.bo.CourseVideoBO;
import com.roncoo.education.course.common.dto.*;
import com.roncoo.education.course.common.es.EsCourse;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseExample.Criteria;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.user.feign.interfaces.IFeignChosen;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.ChosenVO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程信息
 *
 * @author wujing
 */
@Component
public class ApiCourseBiz extends BaseBiz {

    /**
     * 课程简介模板
     */
    private static final String TEMPLATE1 = "course.ftl";

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignChosen feignChosen;
    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    @Autowired
    private CourseAccessoryDao accessoryDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

    /**
     * 课程信息列表接口
     *
     * @param courseInfoPageBO 课程信息分页参数
     * @return 课程信息分页结果
     * @author wuyun
     */
    public Result<Page<CourseInfoPageDTO>> list(CourseInfoPageBO courseInfoPageBO) {
        if (courseInfoPageBO.getCourseCategory() == null) {
            return Result.error("课程分类不能为空");
        }
        CourseExample example = new CourseExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        c.andCourseCategoryEqualTo(courseInfoPageBO.getCourseCategory());
        if (courseInfoPageBO.getLecturerUserNo() != null) {
            c.andLecturerUserNoEqualTo(courseInfoPageBO.getLecturerUserNo());
        }
        if (!StringUtils.isEmpty(courseInfoPageBO.getCategoryId1())) {
            c.andCategoryId1EqualTo(courseInfoPageBO.getCategoryId1());
        }
        if (!StringUtils.isEmpty(courseInfoPageBO.getCategoryId2())) {
            c.andCategoryId2EqualTo(courseInfoPageBO.getCategoryId2());
        }
        if (!StringUtils.isEmpty(courseInfoPageBO.getCategoryId3())) {
            c.andCategoryId3EqualTo(courseInfoPageBO.getCategoryId3());
        }
        if (!StringUtils.isEmpty(courseInfoPageBO.getIsFree())) {
            c.andIsFreeEqualTo(courseInfoPageBO.getIsFree());
        }
        if (IsVipFreeEnum.FREE.getCode().equals(courseInfoPageBO.getIsVipFree())) {
            c.andCourseOriginalNotEqualTo(new BigDecimal(0));
            c.andCourseDiscountEqualTo(new BigDecimal(0));
        }
        if (!StringUtils.isEmpty(courseInfoPageBO.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(SqlUtil.checkSql(courseInfoPageBO.getCourseName())));
        }

        example.setOrderByClause(" course_sort asc, id desc ");
        Page<Course> page = courseDao.listForPage(courseInfoPageBO.getPageCurrent(), courseInfoPageBO.getPageSize(), example);
        Page<CourseInfoPageDTO> listForPage = PageUtil.transform(page, CourseInfoPageDTO.class);
        for (CourseInfoPageDTO dto : listForPage.getList()) {
            LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getLecturerName())) {
                dto.setLecturerName(lecturerVO.getLecturerName());
            }
            if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getHeadImgUrl())) {
                dto.setHeadImgUrl(lecturerVO.getHeadImgUrl());
            }
            if (CourseCategoryEnum.LIVE.getCode().equals(dto.getCourseCategory())) {
                // 获取直播信息
                live(dto);
            }
        }
        return Result.success(listForPage);
    }

    /**
     * 课程详情接口
     *
     * @return 课程详情
     */
    public Result<CourseViewDTO> view(CourseVideoBO courseVideoBO) {
        if (courseVideoBO.getCourseId() == null) {
            return Result.error("课程ID不能为空");
        }
        // 课程信息
        Course course = courseDao.getById(courseVideoBO.getCourseId());
        if (ObjectUtil.isNull(course) || StatusIdEnum.NO.getCode().equals(course.getStatusId())) {
            // 课程不可以用户
            return Result.error(ResultEnum.COURSE_DISABLE);
        }
        CourseViewDTO dto = BeanUtil.copyProperties(course, CourseViewDTO.class);

        // 查找课程关联考试信息
        CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
        courseExamRefQO.setRefId(dto.getId());
        courseExamRefQO.setRefType(RefTypeEnum.COURSE.getCode());
        CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
        if (ObjectUtil.isNotNull(courseExamRefVO)) {
            dto.setExamId(courseExamRefVO.getExamId());
            dto.setCourseExamRefId(courseExamRefVO.getId());
            dto.setExamName(courseExamRefVO.getExamName());
        }

        if (dto.getIntroduceId() != null && !dto.getIntroduceId().equals(0L)) {
            // 课程介绍
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(dto.getIntroduceId());
            dto.setIntroduce(BeanUtil.copyProperties(courseIntroduce, CourseIntroduceDTO.class).getIntroduce());
        }

        // 讲师信息
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
        if (StringUtils.isEmpty(lecturerVO)) {
            return Result.error("根据讲师用户编号没找到对应的讲师信息!");
        }
        dto.setLecturer(BeanUtil.copyProperties(lecturerVO, LecturerDTO.class));
        // 如果为分销商品，设置分销比例和推荐码
        ChosenVO chosenVO = feignChosen.getByCourseId(course.getId());
        if (ObjectUtil.isNotNull(chosenVO) && StatusIdEnum.YES.getCode().equals(chosenVO.getStatusId())) {
            dto.setChosenPercent(chosenVO.getChosenPercent());
        }
        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            // 获取课程第一章
            List<CourseChapter> list = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(dto.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(list)) {
                // 获取课程第一章第一节课时
                List<CourseChapterPeriod> period = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortAsc(list.get(0).getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(period)) {
                    dto.setPeriodName(period.get(0).getPeriodName());
                    dto.setStartTime(period.get(0).getStartTime());
                }
            }
            // 获取课程第一章
            List<CourseChapter> listDesc = courseChapterDao.listByCourseIdAndStatusIdAndSortDesc(dto.getId(), StatusIdEnum.YES.getCode());
            // 获取课程第一章第一节课时
            if (CollectionUtil.isNotEmpty(listDesc)) {
                List<CourseChapterPeriod> periodDesc = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortDesc(listDesc.get(0).getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(periodDesc)) {
                    dto.setEndTime(periodDesc.get(0).getEndTime());
                }
            }
            // 获取直播课程正在直播信息
            CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getByCourseIdAndLiveStatus(dto.getId(), LiveStatusEnum.NOW.getCode());
            if (ObjectUtil.isNotNull(courseChapterPeriod)) {
                dto.setLiveStatus(LiveStatusEnum.NOW.getCode());
            } else {
                dto.setLiveStatus(LiveStatusEnum.NOT.getCode());
            }
        }

        // 课时附件信息
        List<CourseAccessory> periodAccessoryList = accessoryDao.listByRefIdAndRefTypeAndStatusId(dto.getId(), RefTypeEnum.COURSE.getCode(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(periodAccessoryList)) {
            dto.setAccessoryList(ArrayListUtil.copy(periodAccessoryList, CourseAccessoryDTO.class));
        }

        return Result.success(dto);
    }

    /**
     * 课程搜索列表接口
     *
     * @author wuyun
     */
    public Result<Page<CourseInfoSearchPageDTO>> searchList(CourseInfoSearchBO bo) {
        if (bo.getPageCurrent() <= 0) {
            bo.setPageCurrent(1);
        }
        if (bo.getPageSize() <= 0) {
            bo.setPageSize(20);
        }
        if (bo.getCourseCategory() == null) {
            bo.setCourseCategory(CourseCategoryEnum.ORDINARY.getCode());
        }

        if (StringUtils.isEmpty(bo.getCourseName())) {
            return Result.success(new Page<>());
        }
        NativeSearchQueryBuilder nsb = new NativeSearchQueryBuilder();
        if (bo.getIsHfield() != null && bo.getIsHfield().equals(IsHfield.YES.getCode())) {
            String heightField = "courseName";
            HighlightBuilder.Field hField = new HighlightBuilder.Field(heightField).preTags("<mark>").postTags("</mark>");
            // 高亮字段
            nsb.withHighlightFields(hField);
        }
        // 评分排序（_source）
        nsb.withSort(SortBuilders.scoreSort().order(SortOrder.DESC));
        // 课程排序（courseSort）
        nsb.withSort(new FieldSortBuilder("courseSort").order(SortOrder.DESC));
        nsb.withPageable(PageRequest.of(bo.getPageCurrent() - 1, bo.getPageSize()));
        // 复合查询，外套boolQuery
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        // 精确查询termQuery不分词，must参数等价于AND
        // 模糊查询multiMatchQuery，最佳字段best_fields
        qb.must(QueryBuilders.termQuery("statusId", StatusIdEnum.YES.getCode()));
        qb.must(QueryBuilders.termQuery("isPutaway", IsPutawayEnum.YES.getCode()));
        qb.must(QueryBuilders.termQuery("courseCategory", bo.getCourseCategory()));
        qb.must(QueryBuilders.multiMatchQuery(bo.getCourseName(), "courseName", "lecturerName").type(MultiMatchQueryBuilder.Type.BEST_FIELDS));
        nsb.withQuery(qb);
        SearchHits<EsCourse> searchHits = elasticsearchRestTemplate.search(nsb.build(), EsCourse.class, IndexCoordinates.of(EsCourse.COURSE));
        return Result.success(EsPageUtil.transform(searchHits, bo.getPageCurrent(), bo.getPageSize(), CourseInfoSearchPageDTO.class));
    }

    public String view(Long id) {
        Map<String, Object> model = new HashMap<>();
        CourseIntroduceViewDTO dto = new CourseIntroduceViewDTO();
        // 课程信息
        Course course = courseDao.getById(id);
        if (ObjectUtil.isNotNull(course)) {
            dto.setCourseName(course.getCourseName());
            // 课程介绍
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(course.getIntroduceId());
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                dto.setIntroduce(courseIntroduce.getIntroduce());
            }
        }
        model.put("course", dto);
        return getTextByTemplate(TEMPLATE1, model);
    }

    private void live(CourseInfoPageDTO dto) {
        // 获取课程第一章
        List<CourseChapter> list = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(dto.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(list)) {
            // 获取课程第一章第一节课时
            List<CourseChapterPeriod> period = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortAsc(list.get(0).getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(period)) {
                dto.setStartTime(period.get(0).getStartTime());
            }
        }
        // 获取课程第一章
        List<CourseChapter> listDesc = courseChapterDao.listByCourseIdAndStatusIdAndSortDesc(dto.getId(), StatusIdEnum.YES.getCode());
        // 获取课程第一章第一节课时
        if (CollectionUtil.isNotEmpty(listDesc)) {
            List<CourseChapterPeriod> periodDesc = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortDesc(listDesc.get(0).getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(periodDesc)) {
                dto.setEndTime(periodDesc.get(0).getEndTime());
            }
        }
    }

    private String getTextByTemplate(String template, Object model) {
        try {
            return FreeMarkerTemplateUtils.processTemplateIntoString(freeMarkerConfigurer.getConfiguration().getTemplate(template), model);
        } catch (Exception e) {
            logger.error("小程序创建页面模板失败，原因", e);
            return "";
        }
    }

}
