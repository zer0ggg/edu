package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseUserStudyEditREQ;
import com.roncoo.education.course.common.req.CourseUserStudyListREQ;
import com.roncoo.education.course.common.req.CourseUserStudySaveREQ;
import com.roncoo.education.course.common.resp.CourseUserStudyListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseUserStudyBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程用户关联 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/user/study")
@Api(value = "course-课程用户关联", tags = {"course-课程用户关联"})
public class PcCourseUserStudyController {

    @Autowired
    private PcCourseUserStudyBiz biz;

    @ApiOperation(value = "课程用户关联列表", notes = "课程用户关联列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseUserStudyListRESP>> list(@RequestBody CourseUserStudyListREQ courseUserStudyListREQ) {
        return biz.list(courseUserStudyListREQ);
    }

    @ApiOperation(value = "课程用户关联添加", notes = "课程用户关联添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseUserStudySaveREQ courseUserStudySaveREQ) {
        return biz.save(courseUserStudySaveREQ);
    }

    @ApiOperation(value = "课程用户关联查看", notes = "课程用户关联查看")
    @GetMapping(value = "/view")
    public Result<CourseUserStudyViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程用户关联修改", notes = "课程用户关联修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseUserStudyEditREQ courseUserStudyEditREQ) {
        return biz.edit(courseUserStudyEditREQ);
    }

    @ApiOperation(value = "课程用户关联删除", notes = "课程用户关联删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
