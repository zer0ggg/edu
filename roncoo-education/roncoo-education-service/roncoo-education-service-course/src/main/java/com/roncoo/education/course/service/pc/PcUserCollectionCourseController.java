package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.UserCollectionCourseEditREQ;
import com.roncoo.education.course.common.req.UserCollectionCourseListREQ;
import com.roncoo.education.course.common.req.UserCollectionCourseSaveREQ;
import com.roncoo.education.course.common.resp.UserCollectionCourseListRESP;
import com.roncoo.education.course.common.resp.UserCollectionCourseViewRESP;
import com.roncoo.education.course.service.pc.biz.PcUserCollectionCourseBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户收藏课程 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/user/collection/course")
@Api(value = "course-用户收藏课程", tags = {"course-用户收藏课程"})
public class PcUserCollectionCourseController {

    @Autowired
    private PcUserCollectionCourseBiz biz;

    @ApiOperation(value = "用户收藏课程列表", notes = "用户收藏课程列表")
    @PostMapping(value = "/list")
    public Result<Page<UserCollectionCourseListRESP>> list(@RequestBody UserCollectionCourseListREQ userCollectionCourseListREQ) {
        return biz.list(userCollectionCourseListREQ);
    }

    @ApiOperation(value = "用户收藏课程添加", notes = "用户收藏课程添加")
    @SysLog(value = "用户收藏课程添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody UserCollectionCourseSaveREQ userCollectionCourseSaveREQ) {
        return biz.save(userCollectionCourseSaveREQ);
    }

    @ApiOperation(value = "用户收藏课程查看", notes = "用户收藏课程查看")
    @GetMapping(value = "/view")
    public Result<UserCollectionCourseViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "用户收藏课程修改", notes = "用户收藏课程修改")
    @SysLog(value = "用户收藏课程修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody UserCollectionCourseEditREQ userCollectionCourseEditREQ) {
        return biz.edit(userCollectionCourseEditREQ);
    }


    @ApiOperation(value = "用户收藏课程删除", notes = "用户收藏课程删除")
    @SysLog(value = "用户收藏课程删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
