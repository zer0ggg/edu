package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.course.common.bo.CourseRecommendBO;
import com.roncoo.education.course.common.dto.CourseRecommendDTO;
import com.roncoo.education.course.common.dto.CourseRecommendListDTO;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.CourseRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseRecommend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 * 课程推荐
 *
 * @author kyh
 *
 */
@Component
public class ApiCourseRecommendBiz {

	@Autowired
	private CourseDao courseDao;

	@Autowired
	private CourseRecommendDao dao;

	/**
	 * 根据分类ID获取推荐课程信息
	 *
	 * @author kyh
	 */
	public Result<CourseRecommendListDTO> list(CourseRecommendBO bo) {
		List<CourseRecommend> list = dao.listByCategoryIdAndStatusId(bo.getCategoryId(), StatusIdEnum.YES.getCode());
		CourseRecommendListDTO dto = new CourseRecommendListDTO();
		if (CollectionUtil.isEmpty(list)) {
			return Result.success(dto);
		}
		List<CourseRecommendDTO> courseRecommendList = PageUtil.copyList(list, CourseRecommendDTO.class);
		for (CourseRecommendDTO courseRecommend: courseRecommendList) {
			Course course = courseDao.getById(courseRecommend.getCourseId());
			if (ObjectUtil.isNotNull(course)) {
				courseRecommend.setCourseLogo(course.getCourseLogo());
				courseRecommend.setCourseName(course.getCourseName());
				courseRecommend.setCourseDiscount(course.getCourseDiscount());
				courseRecommend.setCourseOriginal(course.getCourseOriginal());
				courseRecommend.setIsFree(course.getIsFree());
				courseRecommend.setCourseCategory(course.getCourseCategory());

			}
		}
		dto.setList(courseRecommendList);
		return Result.success(dto);
	}

}
