package com.roncoo.education.course.service.api.biz;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.CategoryTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.bo.CourseCategoryListBO;
import com.roncoo.education.course.common.dto.CourseCategoryDTO;
import com.roncoo.education.course.common.dto.CourseCategoryListDTO;
import com.roncoo.education.course.common.dto.CourseCategoryThreeDTO;
import com.roncoo.education.course.common.dto.CourseCategoryTwoDTO;
import com.roncoo.education.course.service.dao.CourseCategoryDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCategory;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程分类
 *
 * @author wujing
 */
@Component
public class ApiCourseCategoryBiz {

    @Autowired
    private CourseCategoryDao dao;

    /**
     * 获取课程分类列表
     *
     * @return
     * @author wuyun
     */
    public Result<CourseCategoryListDTO> list(CourseCategoryListBO bo) {
        if (bo.getCategoryType() == null) {
            bo.setCategoryType(CategoryTypeEnum.COURSE.getCode());
        }
        CourseCategoryListDTO dto = new CourseCategoryListDTO();
        // 根据分类类型、层级查询可用状态的课程分类集合
        List<CourseCategory> oneCategoryList = dao.listByCategoryTypeAndFloorAndStatusId(bo.getCategoryType(), 1, StatusIdEnum.YES.getCode());
        if (CollectionUtils.isEmpty(oneCategoryList)) {
            return Result.success(dto);
        }
        List<CourseCategoryDTO> oneList = new ArrayList<>();
        // 查找一级分类下的二级分类
        for (CourseCategory courseCategory : oneCategoryList) {
            // 设置一级分类
            CourseCategoryDTO oneCategory = BeanUtil.copyProperties(courseCategory, CourseCategoryDTO.class);

            // 查找一级分类下的二级分类
            List<CourseCategory> twoCategoryList = dao.listByParentIdAndStatusId(courseCategory.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtils.isEmpty(twoCategoryList)) {
                oneList.add(oneCategory);
                dto.setCourseCategoryList(oneList);
                continue;
            }
            List<CourseCategoryTwoDTO> twoList = new ArrayList<>();
            List<CourseCategoryThreeDTO> threeList;
            for (CourseCategory twoCategory : twoCategoryList) {
                CourseCategoryTwoDTO twoDto;
                twoDto = BeanUtil.copyProperties(twoCategory, CourseCategoryTwoDTO.class);
                List<CourseCategory> threeCategoryList = dao.listByParentIdAndStatusId(twoCategory.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isNotEmpty(threeCategoryList)) {
                    // 复制三级分类信息返回
                    threeList = ArrayListUtil.copy(threeCategoryList, CourseCategoryThreeDTO.class);
                    twoDto.setThreeList(threeList);
                }
                twoList.add(twoDto);
            }
            oneCategory.setTwoList(twoList);
            // 复制二级分类信息返回
            oneList.add(oneCategory);
            dto.setCourseCategoryList(oneList);
        }
        return Result.success(dto);
    }

}
