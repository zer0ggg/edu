package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程用户学习日志-汇总
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseUserStudyLogStatisticalRESP", description = "课程用户学习日志列表-汇总")
public class CourseUserStudyLogStatisticalRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总观看数（根据课程汇总，该总观看数为人数（去重），根据汇总汇总，该总观看数为课程数（去重））")
    private String numberTotal;

    @ApiModelProperty(value = "本周总观看数（根据课程汇总，该总观看数为人数（去重），根据汇总汇总，该总观看数为课程数（去重））")
    private String numberWeeks;

    @ApiModelProperty(value = "本周观看时长")
    private String weeksWatchLength;

    @ApiModelProperty(value = "总观看时长")
    private String watchLengthTotal;

    @ApiModelProperty(value = "x轴数据")
    private List<String> axis = new ArrayList<>();

    @ApiModelProperty(value = "数据")
    private List<CourseUserStudyLogStatisticalListRESP> list = new ArrayList<>();
}
