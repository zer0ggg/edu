package com.roncoo.education.course.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 附件信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseAccessoryBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1:正常，2:禁用)
	 */
	private Integer statusId;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 课程分类(1:普通课程;2:直播课程,3:试卷)
	 */
	private Integer courseCategory;
	/**
	 * 关联类型：1课程，2章节，3课时
	 */
	private Integer refType;
	/**
	 * 关联ID
	 */
	private Long refId;
	/**
	 * 附件名称
	 */
	private String acName;
	/**
	 * 附件地址
	 */
	private String acUrl;
	/**
	 * 下载人数
	 */
	private Integer downloadCount;
}
