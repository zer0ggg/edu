package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.dto.ResourceRecommendListDTO;
import com.roncoo.education.course.service.api.biz.ApiResourceRecommendBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * <p>
 * 文库推荐 Api接口
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@RestController
@RequestMapping("/course/api/resource/recommend")
public class ApiResourceRecommendController {

    @Autowired
    private ApiResourceRecommendBiz biz;

    /**
     * 文库推荐列表接口
     *
     */
    @ApiOperation(value = "文库推荐列表接口", notes = "文库推荐列表接口")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<ResourceRecommendListDTO> list() {
        return biz.list();
    }


}
