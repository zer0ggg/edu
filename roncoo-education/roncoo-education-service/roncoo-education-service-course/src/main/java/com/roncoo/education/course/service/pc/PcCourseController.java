package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseFindBackRESP;
import com.roncoo.education.course.common.resp.CourseListRESP;
import com.roncoo.education.course.common.resp.CourseViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 课程信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course")
@Api(value = "course-课程信息", tags = {"course-课程信息"})
public class PcCourseController {

    @Autowired
    private PcCourseBiz biz;

    @ApiOperation(value = "课程信息列表", notes = "课程信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseListRESP>> list(@RequestBody CourseListREQ courseListREQ) {
        return biz.list(courseListREQ);
    }

    @ApiOperation(value = "课程信息添加", notes = "课程信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseSaveREQ courseSaveREQ) {
        return biz.save(courseSaveREQ);
    }

    @ApiOperation(value = "课程信息查看", notes = "课程信息查看")
    @GetMapping(value = "/view")
    public Result<CourseViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "课程信息修改", notes = "课程信息修改")
    @SysLog(value = "课程信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseEditREQ courseEditREQ) {
        return biz.edit(courseEditREQ);
    }


    @ApiOperation(value = "课程信息删除", notes = "课程信息删除")
    @SysLog(value = "课程信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "一键导入ES", notes = "一键导入ES")
    @SysLog(value = "一键导入ES")
    @PostMapping(value = "/es/add")
    public Result<String> addEs(@RequestBody CourseAddEsREQ courseAddEsREQ) {
        return biz.addEs(courseAddEsREQ);
    }


    @ApiOperation(value = "更新状态", notes = "更新状态")
    @SysLog(value = "更新状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody CourseUpdateStatusREQ courseUpdateStatusREQ) {
        return biz.updateStatus(courseUpdateStatusREQ);
    }

    @ApiOperation(value = "更新是否上下架", notes = "更新是否上下架")
    @SysLog(value = "更新是否上下架")
    @PutMapping(value = "/update/putaway")
    public Result<String> updateStatus(@RequestBody CourseUpdateIsPutawayREQ courseUpdateIsPutawayREQ) {
        return biz.updateIsPutAway(courseUpdateIsPutawayREQ);
    }

    @ApiOperation(value = "上传文件", notes = "上传文件")
    @SysLog(value = "上传文件")
    @PostMapping(value = "/upload")
    public Result<String> upload(@RequestParam("file") MultipartFile multipartFile) {
        return biz.upload(multipartFile);
    }

    @ApiOperation(value = "课程查找带回", notes = "课程查找带回")
    @PostMapping(value = "/find/back")
    public Result<Page<CourseFindBackRESP>> findBack(@RequestBody CourseFindBackREQ courseFindBackREQ) {
        return biz.findBack(courseFindBackREQ);
    }
}
