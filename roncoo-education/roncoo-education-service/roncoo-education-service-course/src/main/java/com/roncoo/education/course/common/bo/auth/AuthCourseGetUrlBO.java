/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 获取课时code值实体类
 *
 * @author forest
 */
@Data
@Accessors(chain = true)
public class AuthCourseGetUrlBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "课时ID不能为空")
    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;
}
