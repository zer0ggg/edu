package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseChapterEditREQ;
import com.roncoo.education.course.common.req.CourseChapterListREQ;
import com.roncoo.education.course.common.req.CourseChapterSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterListRESP;
import com.roncoo.education.course.common.resp.CourseChapterViewRESP;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapter;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 章节信息
 *
 * @author wujing
 */
@Component
public class PcCourseChapterBiz extends BaseBiz {

    @Autowired
    private CourseChapterDao dao;

    /**
    * 章节信息列表
    *
    * @param req 章节信息分页查询参数
    * @return 章节信息分页查询结果
    */
    public Result<Page<CourseChapterListRESP>> list(CourseChapterListREQ req) {
        CourseChapterExample example = new CourseChapterExample();
        Criteria c = example.createCriteria();
        if (req.getCourseId() != null) {
            c.andCourseIdEqualTo(req.getCourseId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (StringUtils.hasText(req.getChapterName())) {
            c.andChapterNameLike(PageUtil.like(req.getChapterName()));
        }
        example.setOrderByClause("sort asc, id asc");
        Page<CourseChapter> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseChapterListRESP> respPage = PageUtil.transform(page, CourseChapterListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 章节信息添加
    *
    * @param courseChapterSaveREQ 章节信息
    * @return 添加结果
    */
    public Result<String> save(CourseChapterSaveREQ courseChapterSaveREQ) {
        CourseChapter record = BeanUtil.copyProperties(courseChapterSaveREQ, CourseChapter.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 章节信息查看
    *
    * @param id 主键ID
    * @return 章节信息
    */
    public Result<CourseChapterViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseChapterViewRESP.class));
    }


    /**
    * 章节信息修改
    *
    * @param courseChapterEditREQ 章节信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseChapterEditREQ courseChapterEditREQ) {
        CourseChapter record = BeanUtil.copyProperties(courseChapterEditREQ, CourseChapter.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 章节信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
