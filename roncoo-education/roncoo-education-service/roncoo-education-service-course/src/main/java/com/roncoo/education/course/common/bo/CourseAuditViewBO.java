package com.roncoo.education.course.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程信息-审核查看(管理员预览)
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseAuditViewBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty(value = "id", required = true)
	private Long id;
}
