package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRef;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample;

import java.util.List;

public interface UserOrderCourseRefDao {
    int save(UserOrderCourseRef record);

    int deleteById(Long id);

    int updateById(UserOrderCourseRef record);

    int updateByExampleSelective(UserOrderCourseRef record, UserOrderCourseRefExample example);

    UserOrderCourseRef getById(Long id);

    Page<UserOrderCourseRef> listForPage(int pageCurrent, int pageSize, UserOrderCourseRefExample example);

    /**
     * 根据用户编号、关联ID获取用户课程关联信息
     *
     * @param userNo
     * @param refId
     * @author kyh
     */
    UserOrderCourseRef getByUserNoAndRefId(Long userNo, Long refId);

    /**
     * 根据用户编号、课程id、课程类型查找课程关联
     *
     * @param userNo
     * @param refId
     * @param courseType
     * @return
     */
    UserOrderCourseRef getByUserNoAndRefIdAndCourseType(Long userNo, Long refId, Integer courseType);

    /**
     * 汇总观看时长
     *
     * @param userNo
     * @param courseId
     * @param weekStart
     * @param weekEnd
     * @return
     */
    String getWatchLengthTotalUserNoAndCourseIdAndGmtModified(Long userNo, Long courseId, String weekStart, String weekEnd);

    /**
     * 列出排行
     *
     * @param userNo
     * @param courseId
     * @param ranking
     * @return
     */
    List<UserOrderCourseRef> listRankingUserNoAndCourseId(Long userNo, Long courseId, Integer ranking);

    /**
     * 获取课程观看数
     *
     * @param userNo
     * @param courseId
     * @param weekStart
     * @param weekEnd
     * @return
     */
    String numbergetByUserNoAndCourseId(Long userNo, Long courseId, String weekStart, String weekEnd);

    /**
     * 根据课程ID获取用户课程关联信息
     *
     * @param courseId
     * @return
     */
    List<UserOrderCourseRef> listCourseId(Long courseId);
}
