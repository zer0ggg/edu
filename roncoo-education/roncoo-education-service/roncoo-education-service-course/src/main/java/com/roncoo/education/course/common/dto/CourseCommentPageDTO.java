package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class CourseCommentPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "评论时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;


    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "被评论用户编号")
    private Long courseUserNo;

    @ApiModelProperty(value = "被评论者昵称")
    private String courseNickname;

    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "评论者昵称")
    private String nickname;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论者IP")
    private String userIp;

    @ApiModelProperty(value = "评论者用户头像")
    private String userImg;

    @ApiModelProperty(value = "评论者终端")
    private String userTerminal;

    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "被评论者用户头像")
    private String courseUserImg;

    @ApiModelProperty(value = "回复内容")
    private List<CourseCommentPageDTO> courseCommentList;

}
