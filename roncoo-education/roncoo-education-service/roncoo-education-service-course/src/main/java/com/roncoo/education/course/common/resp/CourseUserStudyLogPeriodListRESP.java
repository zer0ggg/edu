package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程用户学习日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseUserStudyLogPeriodListRESP", description="课程用户学习日志列表")
public class CourseUserStudyLogPeriodListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课时ID")
    private Long id;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课时名称")
    private String periodName;

    @ApiModelProperty(value = "视频vid")
    private String videoVid;

    @ApiModelProperty(value = "课时总时长")
    private String videoLength;

    @ApiModelProperty(value = "观看时长")
    private String watchLength;

    @ApiModelProperty(value = "观看进度")
    private String watckProgress;
}
