package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.AuthCourseVideoListDTO;
import com.roncoo.education.course.common.dto.auth.AuthWebUploadVideoGetUrlDTO;
import com.roncoo.education.course.common.dto.auth.AuthWebUploadVideoRefreshDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseVideoBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程视频信息
 *
 * @author wuyun
 */
@RestController
@RequestMapping(value = "/course/auth/course/video")
public class AuthCourseVideoController extends BaseController {

    @Autowired
    private AuthCourseVideoBiz biz;

    @ApiOperation(value = "讲师视频库保存接口", notes = "讲师视频库保存接口")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result<Integer> save(@RequestBody AuthCourseVideoSaveBO authCourseVideoSaveBO) {
        return biz.save(authCourseVideoSaveBO);
    }

    @ApiOperation(value = "章节视频库列出接口", notes = "根据章节ID列出讲师章节视频库信息")
    @RequestMapping(value = "/list/chapter", method = RequestMethod.POST)
    public Result<AuthCourseVideoListDTO> listByChapterId(@RequestBody AuthCourseVideoBO authCourseVideoBO) {
        return biz.listByChapterId(authCourseVideoBO);
    }

    @ApiOperation(value = "删除接口", notes = "讲师视频信息删除")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Result<Integer> updateById(@RequestBody AuthCourseVideoDeleteBO authCourseVideoDeleteBO) {
        return biz.updateById(authCourseVideoDeleteBO);
    }

    @ApiOperation(value = "前端上传视频关联章节接口", notes = "前端上传视频关联章节接口")
    @RequestMapping(value = "/ref", method = RequestMethod.POST)
    public Result<String> ref(@RequestBody AuthWebUploadVideoRefBO bo) {
        return biz.ref(bo);
    }

    @ApiOperation(value = "获取视频上传地址")
    @PostMapping(value = "/get/upload/url")
    public Result<AuthWebUploadVideoGetUrlDTO> getUploadUrl(@RequestBody AuthWebUploadVideoGetUrlBO bo) {
        return biz.getUploadUrl(bo);
    }

    @ApiOperation(value = "刷新视频上传凭证")
    @PostMapping(value = "/refresh/upload/url")
    public Result<AuthWebUploadVideoRefreshDTO> uploadRefresh(@RequestBody AuthWebUploadVideoRefreshBO bo) {
        return biz.uploadRefresh(bo);
    }

}
