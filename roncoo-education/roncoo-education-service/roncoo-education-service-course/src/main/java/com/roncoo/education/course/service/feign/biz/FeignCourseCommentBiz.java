package com.roncoo.education.course.service.feign.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.CourseCommentQO;
import com.roncoo.education.course.feign.vo.CourseCommentVO;
import com.roncoo.education.course.service.dao.CourseCommentDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程评论
 *
 * @author Quanf
 */
@Component
public class FeignCourseCommentBiz extends BaseBiz {

    @Autowired
    private CourseCommentDao dao;

	public Page<CourseCommentVO> listForPage(CourseCommentQO qo) {
	    CourseCommentExample example = new CourseCommentExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<CourseComment> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CourseCommentVO.class);
	}

	public int save(CourseCommentQO qo) {
		CourseComment record = BeanUtil.copyProperties(qo, CourseComment.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseCommentVO getById(Long id) {
		CourseComment record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseCommentVO.class);
	}

	public int updateById(CourseCommentQO qo) {
		CourseComment record = BeanUtil.copyProperties(qo, CourseComment.class);
		return dao.updateById(record);
	}

}
