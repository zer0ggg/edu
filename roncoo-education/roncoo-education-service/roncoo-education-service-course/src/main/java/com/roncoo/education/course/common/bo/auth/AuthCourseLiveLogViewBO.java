package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 直播记录表
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthCourseLiveLogViewBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课时ID
	 */
	@ApiModelProperty(value = "课时ID", required = true)
	private Long periodId;

}
