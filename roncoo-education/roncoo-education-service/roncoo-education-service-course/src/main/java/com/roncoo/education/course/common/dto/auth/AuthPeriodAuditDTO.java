package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthPeriodAuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "课时id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 审核状态(0:待审核;1:审核通过;2:审核不通过)
	 */
	@ApiModelProperty(value = "审核状态(0:待审核;1:审核通过;2:审核不通过)")
	private Integer auditStatus;
	/**
	 * 课时名称
	 */
	@ApiModelProperty(value = "课时名称")
	private String periodName;
	/**
	 * 课时描述
	 */
	@ApiModelProperty(value = "课时描述")
	private String periodDesc;
	/**
	 * 是否免费：1免费，0收费
	 */
	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;
	/**
	 * 原价
	 */
	@ApiModelProperty(value = "原价")
	private BigDecimal periodOriginal;
	/**
	 * 是否存在视频(1存在，0否)
	 */
	@ApiModelProperty(value = "是否存在视频(1存在，0否)")
	private Integer isVideo;
	/**
	 * 视频编号
	 */
	@ApiModelProperty(value = "视频编号")
	private Long videoNo;
	/**
	 * 视频名称
	 */
	@ApiModelProperty(value = "视频名称")
	private String videoName;
	/**
	 * 时长
	 */
	@ApiModelProperty(value = "时长")
	private String videoLength;
	/**
	 * 视频Vid
	 */
	@ApiModelProperty(value = "视频Vid")
	private String videoVid;
	/**
	 * 阿里云oas
	 */
	@ApiModelProperty(value = "阿里云oas")
	private String videoOasId;

	/**
	 * 直播状态(1:未开播,2:正在直播,3:回放)
	 */
	@ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:回放)")
	private Integer liveStatus;
	/**
	 * 直播回放url
	 */
	@ApiModelProperty(value = "直播回放url")
	private String playback;
	/**
	 * 直播开始时间
	 */
	@ApiModelProperty(value = "直播开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;
	/**
	 * 直播结束时间
	 */
	@ApiModelProperty(value = "直播结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	/**
	 * 文档数量
	 */
	@ApiModelProperty(value = "文档数量")
	private Integer docNum = 0;

	/**
	 * 试卷题目图片数
	 */
	@ApiModelProperty(value = "试卷题目图片数")
	private Integer captionCount = 0;

	/**
	 * 试卷答案图片数
	 */
	@ApiModelProperty(value = "试卷答案图片数")
	private Integer resultCount = 0;
	/**
	 * 是否需要人脸对比(0:否，1:是)
	 */
	@ApiModelProperty(value = "是否需要人脸对比(0:否，1:是)")
	private Integer isFaceContras;

	@ApiModelProperty(value = "课程试卷关联主键id")
	private Long courseExamRefId;

	@ApiModelProperty(value = "试卷ID")
	private Long examId;

	@ApiModelProperty(value = "试卷名称")
	private String examName;

	@ApiModelProperty(value = "是否已审核通过(0:否，1:是)")
	private Integer isHasAudit = 0;
}
