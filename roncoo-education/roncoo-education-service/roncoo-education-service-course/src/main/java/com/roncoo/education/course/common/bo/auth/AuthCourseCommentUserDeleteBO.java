package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class AuthCourseCommentUserDeleteBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "parentId父ID")
    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @NotNull(message = "id课程评论ID")
    @ApiModelProperty(value = "课程评论ID")
    private Long id;

    @NotNull(message = "courseId课程ID")
    @ApiModelProperty(value = "课程ID")
    private Long courseId;
}
