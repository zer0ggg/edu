package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.UserOrderCourseRefDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRef;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@Component
public class FeignUserOrderCourseRefBiz extends BaseBiz {

    @Autowired
    private UserOrderCourseRefDao dao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Page<UserOrderCourseRefVO> listForPage(UserOrderCourseRefQO qo) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        example.setOrderByClause(" id desc ");
        Page<UserOrderCourseRef> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, UserOrderCourseRefVO.class);
    }

    public int save(UserOrderCourseRefQO qo) {
        UserOrderCourseRef record = BeanUtil.copyProperties(qo, UserOrderCourseRef.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public UserOrderCourseRefVO getById(Long id) {
        UserOrderCourseRef record = dao.getById(id);
        return BeanUtil.copyProperties(record, UserOrderCourseRefVO.class);
    }

    public int updateById(UserOrderCourseRefQO qo) {
        UserOrderCourseRef record = BeanUtil.copyProperties(qo, UserOrderCourseRef.class);
        return dao.updateById(record);
    }

    public UserOrderCourseRefVO getByUserNoAndRefIdAndCourseType(UserOrderCourseRefQO qo) {
        UserOrderCourseRef userOrderCourseRef = dao.getByUserNoAndRefIdAndCourseType(qo.getUserNo(), qo.getRefId(), qo.getCourseType());
        return BeanUtil.copyProperties(userOrderCourseRef, UserOrderCourseRefVO.class);
    }

    public UserOrderCourseRefVO getByUserNoAndRefId(UserOrderCourseRefQO qo) {
        UserOrderCourseRef userOrderCourseRef = dao.getByUserNoAndRefId(qo.getUserNo(), qo.getRefId());
        return BeanUtil.copyProperties(userOrderCourseRef, UserOrderCourseRefVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public int updateByUserNoAndRefId(UserOrderCourseRefQO userOrderCourseRefQO) {
        // 根据用户编号、关联ID、课程类型
        UserOrderCourseRef userOrderCourseRef = dao.getByUserNoAndRefIdAndCourseType(userOrderCourseRefQO.getUserNo(), userOrderCourseRefQO.getRefId(), userOrderCourseRefQO.getCourseType());
        Course course = new Course();
        course.setId(userOrderCourseRefQO.getRefId());
        //课程视频总时长
        String videoLength = "";
        videoLength = videoLength(videoLength, userOrderCourseRefQO.getRefId());
        course.setVideoLength(videoLength);
        courseDao.updateById(course);

        UserOrderCourseRef record = BeanUtil.copyProperties(userOrderCourseRefQO, UserOrderCourseRef.class);
        record.setLastStudyTime(new Date());
        // 课程视频总时长
        record.setCourseLength(course.getVideoLength());
        if (ObjectUtil.isNotNull(userOrderCourseRef)) {
            record.setId(userOrderCourseRef.getId());
            record.setCourseLength(course.getVideoLength());
            BigDecimal videoLengthBigDecimal = new BigDecimal(DateUtil.getSecond(course.getVideoLength()));
            // 校验学习时长是否大于课程视频时长
            if (userOrderCourseRefQO.getStudyLength().compareTo(videoLengthBigDecimal) == 1) {
                record.setStudyLength(videoLengthBigDecimal);
                record.setStudyProcess(100);
                return dao.updateById(record);
            }

            //离结束大于5秒
            BigDecimal watchProgress = userOrderCourseRefQO.getStudyLength().divide(videoLengthBigDecimal, 2, BigDecimal.ROUND_HALF_UP);
            if (watchProgress.compareTo(BigDecimal.valueOf(0.95)) >= 0) {
                if (record.getStudyLength().compareTo(BigDecimal.ZERO) > 0 && StringUtils.hasText(course.getVideoLength())) {
                    watchProgress = record.getStudyLength().divide(videoLengthBigDecimal, 2, BigDecimal.ROUND_DOWN);
                }
            }
            //观看进度(观看总进度)
            record.setStudyProcess(Integer.valueOf(watchProgress.multiply(BigDecimal.valueOf(100)).intValue()));
            if (record.getStudyProcess() > 100) {
                record.setStudyProcess(100);
            }
            return dao.updateById(record);
        }
        return 1;
    }

    private String videoLength(String videoLength, Long courseId) {
        String result = myRedisTemplate.get("course_video_length_" + courseId);
        if (null != result) {
            return result;
        }
        List<CourseChapterPeriod> list = courseChapterPeriodDao.listByCourseIdAndStatusId(courseId, StatusIdEnum.YES.getCode());
        int countTime = 0;
        for (CourseChapterPeriod courseChapterPeriod : list) {
            if (StringUtils.hasText(courseChapterPeriod.getVideoLength())) {
                countTime = countTime + DateUtil.getSecond(courseChapterPeriod.getVideoLength());
            }
        }
        if (countTime != 0) {
            // 返回课程视频总时长
            videoLength = cn.hutool.core.date.DateUtil.secondToTime(countTime);
        }
        return myRedisTemplate.set("course_video_length_" + courseId, videoLength, 1, TimeUnit.HOURS);
    }

}
