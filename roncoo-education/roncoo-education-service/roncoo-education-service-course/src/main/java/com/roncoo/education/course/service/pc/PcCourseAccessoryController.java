package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.CourseAccessoryEditREQ;
import com.roncoo.education.course.common.req.CourseAccessoryListREQ;
import com.roncoo.education.course.common.req.CourseAccessorySaveREQ;
import com.roncoo.education.course.common.resp.CourseAccessoryListRESP;
import com.roncoo.education.course.common.resp.CourseAccessoryViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseAccessoryBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 附件信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/accessory")
@Api(value = "course-附件信息", tags = {"course-附件信息"})
public class PcCourseAccessoryController {

    @Autowired
    private PcCourseAccessoryBiz biz;

    @ApiOperation(value = "附件信息列表", notes = "附件信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseAccessoryListRESP>> list(@RequestBody CourseAccessoryListREQ courseAccessoryListREQ) {
        return biz.list(courseAccessoryListREQ);
    }

    @ApiOperation(value = "附件信息添加", notes = "附件信息添加")
    @SysLog(value = "附件信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseAccessorySaveREQ courseAccessorySaveREQ) {
        return biz.save(courseAccessorySaveREQ);
    }

    @ApiOperation(value = "附件信息查看", notes = "附件信息查看")
    @GetMapping(value = "/view")
    public Result<CourseAccessoryViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "附件信息修改", notes = "附件信息修改")
    @SysLog(value = "附件信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseAccessoryEditREQ courseAccessoryEditREQ) {
        return biz.edit(courseAccessoryEditREQ);
    }

    @ApiOperation(value = "附件信息删除", notes = "附件信息删除")
    @SysLog(value = "附件信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
