package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 章节信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 章节id
	 */
	@ApiModelProperty(value = "章节id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 课程id
	 */
	@ApiModelProperty(value = "课程id")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 章节名称
	 */
	@ApiModelProperty(value = "章节名称")
	private String chapterName;
	/**
	 * 是否免费：1免费，0收费
	 */
	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;
	/**
	 * 原价
	 */
	@ApiModelProperty(value = "原价")
	private BigDecimal periodOriginal;
	/**
	 * 优惠价
	 */
	@ApiModelProperty(value = "优惠价")
	private BigDecimal chapterDiscount;

}
