package com.roncoo.education.course.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.jdbc.AbstractBaseJdbc;
import com.roncoo.education.course.service.dao.UserOrderCourseRefDao;
import com.roncoo.education.course.service.dao.impl.mapper.UserOrderCourseRefMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRef;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Repository
public class UserOrderCourseRefDaoImpl extends AbstractBaseJdbc implements UserOrderCourseRefDao {
    @Autowired
    private UserOrderCourseRefMapper userOrderCourseRefMapper;

    @Override
    public int save(UserOrderCourseRef record) {
        record.setId(IdWorker.getId());
        return this.userOrderCourseRefMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.userOrderCourseRefMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(UserOrderCourseRef record) {
        return this.userOrderCourseRefMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(UserOrderCourseRef record, UserOrderCourseRefExample example) {
        return this.userOrderCourseRefMapper.updateByExampleSelective(record, example);
    }

    @Override
    public UserOrderCourseRef getById(Long id) {
        return this.userOrderCourseRefMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<UserOrderCourseRef> listForPage(int pageCurrent, int pageSize, UserOrderCourseRefExample example) {
        int count = this.userOrderCourseRefMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<UserOrderCourseRef>(count, totalPage, pageCurrent, pageSize, this.userOrderCourseRefMapper.selectByExample(example));
    }

    @Override
    public UserOrderCourseRef getByUserNoAndRefId(Long userNo, Long refId) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andRefIdEqualTo(refId);
        List<UserOrderCourseRef> list = this.userOrderCourseRefMapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public UserOrderCourseRef getByUserNoAndRefIdAndCourseType(Long userNo, Long refId, Integer courseType) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        Criteria c = example.createCriteria();
        c.andUserNoEqualTo(userNo);
        c.andRefIdEqualTo(refId);
        c.andCourseTypeEqualTo(courseType);
        List<UserOrderCourseRef> list = this.userOrderCourseRefMapper.selectByExample(example);
        if (CollectionUtil.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public String getWatchLengthTotalUserNoAndCourseIdAndGmtModified(Long userNo, Long courseId, String weekStart, String weekEnd) {
        StringBuilder builder = new StringBuilder();
        builder.append("select sum(study_length) as studyLength from user_order_course_ref where 1");
        if (userNo != null) {
            builder.append(" and user_no = '").append(userNo).append("'");
        }
        if (courseId != null) {
            builder.append(" and course_id = '").append(courseId).append("'");
        }
        if (weekStart != null) {
            builder.append(" and last_study_time >= '").append(weekStart).append("'");
        }
        if (weekEnd != null) {
            builder.append(" and last_study_time <= '").append(weekEnd).append("'");
        }
        String count = "0";
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("studyLength"))) {
            count = String.valueOf(map.get("studyLength"));
        }
        return count;
    }

    @Override
    public List<UserOrderCourseRef> listRankingUserNoAndCourseId(Long userNo, Long courseId, Integer ranking) {
        StringBuilder builder = new StringBuilder();
        builder.append("select user_no as userNo, course_id as courseId, course_length as courseLength, study_length as studyLength, study_process as studyProcess from user_order_course_ref where 1");
        if (userNo != null) {
            builder.append(" and user_no = '").append(userNo).append("'");
        }
        if (courseId != null) {
            builder.append(" and course_id = '").append(courseId).append("'");
        }
        builder.append(" order by study_length desc limit 0,").append(ranking);
        return queryForObjectList(builder.toString(), UserOrderCourseRef.class);
    }

    @Override
    public String numbergetByUserNoAndCourseId(Long userNo, Long courseId, String weekStart, String weekEnd) {
        StringBuilder builder = new StringBuilder();
        if (userNo != null) {
            builder.append("select count(*) as count from user_order_course_ref where 1");
            builder.append(" and user_no = '").append(userNo).append("'");
        }
        if (courseId != null) {
            builder.append("select count(distinct user_no) as count from user_order_course_ref where 1");
            builder.append(" and course_id = '").append(courseId).append("'");
        }
        if (weekStart != null) {
            builder.append(" and last_study_time >= '").append(weekStart).append("'");
        }
        if (weekEnd != null) {
            builder.append(" and last_study_time <= '").append(weekEnd).append("'");
        }
        String count = "";
        Map<String, Object> map = queryForMap(builder.toString());
        if (map != null && !StringUtils.isEmpty(map.get("count"))) {
            count = String.valueOf(map.get("count"));
        }
        return count;
    }

    @Override
    public List<UserOrderCourseRef> listCourseId(Long courseId) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(courseId);
        return this.userOrderCourseRefMapper.selectByExample(example);
    }
}
