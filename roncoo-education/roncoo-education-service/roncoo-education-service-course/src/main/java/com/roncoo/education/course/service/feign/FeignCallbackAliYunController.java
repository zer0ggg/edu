package com.roncoo.education.course.service.feign;

import com.roncoo.education.course.feign.interfaces.IFeignCallbackAliYun;
import com.roncoo.education.course.feign.qo.AliYunVideoAnalysisCompleteQO;
import com.roncoo.education.course.feign.qo.AliYunVideoUploadComplateQO;
import com.roncoo.education.course.service.feign.biz.FeignCallbackAliYunBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 阿里云回调处理
 *
 * @author LYQ
 */
@RestController
public class FeignCallbackAliYunController implements IFeignCallbackAliYun {

    @Autowired
    private FeignCallbackAliYunBiz biz;

    @Override
    public Boolean uploadComplete(@RequestBody AliYunVideoUploadComplateQO qo) {
        return biz.uploadComplete(qo);
    }

    @Override
    public Boolean videoAnalysisComplete(@RequestBody AliYunVideoAnalysisCompleteQO qo) {
        return biz.videoAnalysisComplete(qo);
    }
}
