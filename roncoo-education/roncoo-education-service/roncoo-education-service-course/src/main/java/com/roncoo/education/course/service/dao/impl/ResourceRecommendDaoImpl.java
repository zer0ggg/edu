package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.ResourceRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.ResourceRecommendMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 文库推荐 服务实现类
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@Repository
public class ResourceRecommendDaoImpl implements ResourceRecommendDao {

    @Autowired
    private ResourceRecommendMapper mapper;

    @Override
    public Page<ResourceRecommend> listForPage(int pageCurrent, int pageSize, ResourceRecommendExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<ResourceRecommend>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public int save(ResourceRecommend record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(ResourceRecommend record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public ResourceRecommend getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ResourceRecommend> listByStatusId(Integer statusId) {
        ResourceRecommendExample example = new ResourceRecommendExample();
        ResourceRecommendExample.Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(statusId);
        example.setOrderByClause(" sort asc, id desc ");
        return this.mapper.selectByExample(example);
    }

    @Override
    public ResourceRecommend getByResourceId(Long resourceId) {
        ResourceRecommendExample example = new ResourceRecommendExample();
        ResourceRecommendExample.Criteria c = example.createCriteria();
        c.andResourceIdEqualTo(resourceId);
        List<ResourceRecommend> list = this.mapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
