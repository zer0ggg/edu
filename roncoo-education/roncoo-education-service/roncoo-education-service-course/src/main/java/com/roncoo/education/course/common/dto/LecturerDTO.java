package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 讲师信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class LecturerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "讲师用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lecturerUserNo;

	@ApiModelProperty(value = "讲师名称")
	private String lecturerName;
	/**
	 * 头像
	 */
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	/**
	 * 简介
	 */
	@ApiModelProperty(value = "简介")
	private String introduce;
}
