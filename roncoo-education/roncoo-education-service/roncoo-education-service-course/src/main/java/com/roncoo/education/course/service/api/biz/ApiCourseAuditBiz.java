package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.bo.CourseAuditViewBO;
import com.roncoo.education.course.common.dto.CourseAccessoryPreviewDTO;
import com.roncoo.education.course.common.dto.CourseAuditViewDTO;
import com.roncoo.education.course.common.dto.CourseIntroduceDTO;
import com.roncoo.education.course.common.dto.CourseLecturerAuditViewDTO;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.CourseAuditDao;
import com.roncoo.education.course.service.dao.CourseIntroduceAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceAudit;
import com.roncoo.education.user.feign.interfaces.IFeignLecturerAudit;
import com.roncoo.education.user.feign.vo.LecturerAuditVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Component
public class ApiCourseAuditBiz extends BaseBiz {

    @Autowired
    private IFeignLecturerAudit feignLecturerAudit;

    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseIntroduceAuditDao courseIntroduceAuditDao;
    @Autowired
    private CourseAccessoryDao courseAccessoryDao;


    /**
     * 课程详情接口(管理员预览)
     *
     * @return 课程详情
     */
    public Result<CourseAuditViewDTO> view(CourseAuditViewBO bo) {
        if (bo.getId() == null) {
            return Result.error("课程ID不能为空");
        }
        // 课程信息
        CourseAudit courseAudit = courseAuditDao.getById(bo.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到该课程信息");
        }
        CourseAuditViewDTO dto = BeanUtil.copyProperties(courseAudit, CourseAuditViewDTO.class);

        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            // 课程介绍
            CourseIntroduceAudit courseIntroduce = courseIntroduceAuditDao.getById(courseAudit.getIntroduceId());
            dto.setIntroduce(BeanUtil.copyProperties(courseIntroduce, CourseIntroduceDTO.class).getIntroduce());
        }
        LecturerAuditVO lecturerAuditVO = feignLecturerAudit.getByLecturerUserNo(courseAudit.getLecturerUserNo());
        if (StringUtils.isEmpty(lecturerAuditVO)) {
            return Result.error("根据课程对应的讲师用户编号没找到对应的讲师信息!");
        }
        // 返回课程讲师信息
        dto.setLecturer(BeanUtil.copyProperties(lecturerAuditVO, CourseLecturerAuditViewDTO.class));
        List<CourseAccessory> list = courseAccessoryDao.listByRefIdAndStatusId(courseAudit.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(list)) {
            dto.setAccessory(ArrayListUtil.copy(list, CourseAccessoryPreviewDTO.class));
        }
        return Result.success(dto);
    }
}
