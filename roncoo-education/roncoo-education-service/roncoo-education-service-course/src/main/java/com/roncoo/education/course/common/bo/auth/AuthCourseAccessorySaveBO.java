package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 附件保存
 *
 * @author kyh
 *
 */
@Data
@Accessors(chain = true)
public class AuthCourseAccessorySaveBO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 课程分类(1:普通课程;2:直播课程,3:试卷)
	 */
	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4:文库)", required = true)
	private Integer courseCategory;
	/**
	 * 课程类型：1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "关联类型：1课程，2章节，3课时", required = true)
	private Integer refType;
	/**
	 * 附件名称
	 */
	@ApiModelProperty(value = "附件名称", required = true)
	private String acName;
	/**
	 * 附件地址
	 */
	@ApiModelProperty(value = "附件地址", required = true)
	private String acUrl;
	/**
	 * 关联ID
	 */
	@ApiModelProperty(value = "关联ID", required = true)
	private Long refId;
}
