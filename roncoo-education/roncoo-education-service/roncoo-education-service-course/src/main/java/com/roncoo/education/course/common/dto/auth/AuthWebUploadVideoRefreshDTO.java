package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 刷新视频上传凭证
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthWebUploadVideoRefreshDTO", description = "刷新视频上传凭证")
public class AuthWebUploadVideoRefreshDTO implements Serializable {

    private static final long serialVersionUID = -1505202994532527541L;

    @ApiModelProperty(value = "视频Vid", required = true)
    private String videoVid;

    @ApiModelProperty(value = "上传凭证", required = true)
    private String uploadAuth;

    @ApiModelProperty(value = "上传地址", required = true)
    private String uploadAddress;

}
