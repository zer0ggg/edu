package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.UserOrderCourseRefEditREQ;
import com.roncoo.education.course.common.req.UserOrderCourseRefListREQ;
import com.roncoo.education.course.common.req.UserOrderCourseRefSaveREQ;
import com.roncoo.education.course.common.resp.UserOrderCourseRefListRESP;
import com.roncoo.education.course.common.resp.UserOrderCourseRefViewRESP;
import com.roncoo.education.course.service.dao.UserOrderCourseRefDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRef;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户订单课程关联
 *
 * @author wujing
 */
@Component
public class PcUserOrderCourseRefBiz extends BaseBiz {

    @Autowired
    private UserOrderCourseRefDao dao;

    /**
     * 用户订单课程关联列表
     *
     * @param req 用户订单课程关联分页查询参数
     * @return 用户订单课程关联分页查询结果
     */
    public Result<Page<UserOrderCourseRefListRESP>> list(UserOrderCourseRefListREQ req) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        Criteria c = example.createCriteria();
        Page<UserOrderCourseRef> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<UserOrderCourseRefListRESP> respPage = PageUtil.transform(page, UserOrderCourseRefListRESP.class);
        return Result.success(respPage);
    }


    /**
     * 用户订单课程关联添加
     *
     * @param userOrderCourseRefSaveREQ 用户订单课程关联
     * @return 添加结果
     */
    public Result<String> save(UserOrderCourseRefSaveREQ userOrderCourseRefSaveREQ) {
        UserOrderCourseRef record = BeanUtil.copyProperties(userOrderCourseRefSaveREQ, UserOrderCourseRef.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 用户订单课程关联查看
     *
     * @param id 主键ID
     * @return 用户订单课程关联
     */
    public Result<UserOrderCourseRefViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), UserOrderCourseRefViewRESP.class));
    }


    /**
     * 用户订单课程关联修改
     *
     * @param userOrderCourseRefEditREQ 用户订单课程关联修改对象
     * @return 修改结果
     */
    public Result<String> edit(UserOrderCourseRefEditREQ userOrderCourseRefEditREQ) {
        UserOrderCourseRef record = BeanUtil.copyProperties(userOrderCourseRefEditREQ, UserOrderCourseRef.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 用户订单课程关联删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

}
