package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourse;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserCollectionCourseMapper {
    int countByExample(UserCollectionCourseExample example);

    int deleteByExample(UserCollectionCourseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserCollectionCourse record);

    int insertSelective(UserCollectionCourse record);

    List<UserCollectionCourse> selectByExample(UserCollectionCourseExample example);

    UserCollectionCourse selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserCollectionCourse record, @Param("example") UserCollectionCourseExample example);

    int updateByExample(@Param("record") UserCollectionCourse record, @Param("example") UserCollectionCourseExample example);

    int updateByPrimaryKeySelective(UserCollectionCourse record);

    int updateByPrimaryKey(UserCollectionCourse record);
}
