package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignCourse;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.LiveCourseVO;
import com.roncoo.education.course.service.feign.biz.FeignCourseBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程信息
 *
 * @author wujing
 */
@RestController
public class FeignCourseController extends BaseController implements IFeignCourse {

	@Autowired
	private FeignCourseBiz biz;

	@Override
	public Page<CourseVO> listForPage(@RequestBody CourseQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@PathVariable(value = "id") Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public CourseVO getById(@PathVariable(value = "id") Long id) {
		return biz.getById(id);
	}

	@Override
	public CourseVO getByIdAndStatusId(@PathVariable(value = "id") Long id,@PathVariable(value = "statusId") Integer statusId) {
		return biz.getByIdAndStatusId(id,statusId);
	}

	@Override
	public LiveCourseVO getLiveCourseWithTimeByIdAndStatusId(@PathVariable(value = "id") Long id,@PathVariable(value = "statusId") Integer statusId) {
		return biz.getLiveCourseWithTimeByIdAndStatusId(id,statusId);
	}

	@Override
	public CourseVO getByCourseId(@PathVariable(value = "id") Long id) {
		return biz.getByCourseId(id);
	}

	@Override
	public CourseVO getByIdAndCourseCategory(@RequestBody CourseQO qo) {
		return biz.getByIdAndCourseCategory(qo);
	}

	@Override
	public boolean addEs(@RequestBody CourseQO qo) {
		return biz.addEs(qo);
	}

    @Override
    public CourseVO getByCourseIdAndStatusId(@RequestBody CourseQO qo) {
        return biz.getByCourseIdAndStatusId(qo);
    }

    @Override
    public List<CourseVO> listByIds(@RequestBody CourseQO courseQO) {
        return biz.listByIds(courseQO);
    }

    @Override
	public int orderComplete(String json) {
		return 0;
	}


}
