package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程信息修改
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseUpdateStatusREQ", description = "课程信息更新状态")
public class CourseUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

}
