package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCourseDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCoursePageBO;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCourseSaveBO;
import com.roncoo.education.course.common.dto.auth.AuthUserCollectionCoursePageDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthUserCollectionCourseBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/user/collection/course")
public class AuthUserCollectionCourseController {

	@Autowired
	private AuthUserCollectionCourseBiz biz;

	/**
	 * 用户收藏课程分页列出接口
	 *
	 * @param authUserCollectionCoursePageBO
	 * @return
	 * @author kyh
	 */
	@ApiOperation(value = "分页列出接口", notes = "用户收藏课程分页列出接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<AuthUserCollectionCoursePageDTO>> list(@RequestBody AuthUserCollectionCoursePageBO authUserCollectionCoursePageBO) {
		return biz.list(authUserCollectionCoursePageBO);
	}

	/**
	 * 收藏接口
	 *
	 * @param authUserCollectionCourseSaveBO
	 * @return
	 * @author kyh
	 */
	@ApiOperation(value = "收藏接口", notes = "收藏接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthUserCollectionCourseSaveBO authUserCollectionCourseSaveBO) {
		return biz.save(authUserCollectionCourseSaveBO);
	}

	/**
	 * 用户取消收藏课程
	 *
	 * @return
	 * @author kyh
	 */
	@ApiOperation(value = "用户取消收藏课程", notes = "用户取消收藏课程")
	@RequestMapping(value = "/delet", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthUserCollectionCourseDeleteBO authUserCollectionCourseDeleteBO) {
		return biz.delete(authUserCollectionCourseDeleteBO);
	}

}
