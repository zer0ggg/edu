package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourse;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample;

public interface UserCollectionCourseDao {
	int save(UserCollectionCourse record);

	int deleteById(Long id);

	int updateById(UserCollectionCourse record);

	int updateByExampleSelective(UserCollectionCourse record, UserCollectionCourseExample example);

	UserCollectionCourse getById(Long id);

	Page<UserCollectionCourse> listForPage(int pageCurrent, int pageSize, UserCollectionCourseExample example);

	/**
	 * 根据用户编号、课程ID获取用户收藏课程信息
	 * 
	 * @param userNo
	 * @param courseId
	 * @author kyh
	 */
	UserCollectionCourse getByUserAndCourseId(Long userNo, Long courseId);

	/**
	 * 根据用户编号、章节ID获取用户收藏课程信息
	 * 
	 * @param userNo
	 * @param chapterId
	 * @author kyh
	 */
	UserCollectionCourse getByUserAndChapterId(Long userNo, Long chapterId);

	/**
	 * 根据用户编号、课时ID获取用户收藏课程信息
	 * 
	 * @param userNo
	 * @param periodId
	 * @author kyh
	 */
	UserCollectionCourse getByUserAndPeriodId(Long userNo, Long periodId);
}