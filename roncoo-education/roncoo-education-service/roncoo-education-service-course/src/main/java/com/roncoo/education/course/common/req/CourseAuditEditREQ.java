package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 课程信息-审核
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseAuditEditREQ", description = "课程信息-审核修改")
public class CourseAuditEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "主键不能为空")
    @ApiModelProperty(value = "主键",required = true)
    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程封面")
    private String courseLogo;

    @ApiModelProperty(value = "课程介绍ID")
    private Long introduceId;

    @ApiModelProperty(value = "课程介绍ID")
    private String introduce;

    @NotNull(message = "是否免费不能为空")
    @ApiModelProperty(value = "是否免费：1免费，0收费",required = true)
    private Integer isFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "SEO关键词")
    private String keywords;

    @ApiModelProperty(value = "SEO描述")
    private String description;

    @ApiModelProperty(value = "附件集合", required = false)
    private List<CourseAccessorySaveREQ> accessoryList;

}
