package com.roncoo.education.course.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 课程用户学习日志
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseUserStudyLogListRESP", description="课程用户学习日志列表")
public class CourseUserStudyLogListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "用户手机")
    private String mobile;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程类型：1课程，2章节，3课时")
    private Integer courseType;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "关联ID")
    private Long refId;

    @ApiModelProperty(value = "课程总时长")
    private String courseLength;

    @ApiModelProperty(value = "学习时长")
    private BigDecimal studyLength;

    @ApiModelProperty(value = "学习时长")
    private String stringStudyLength;

    @ApiModelProperty(value = "学习进度")
    private Integer studyProcess;

    @ApiModelProperty(value = "最近学习时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Data lastStudyTime;

    @ApiModelProperty(value = "最近学习课时id")
    private Long lastStudyPeriodId;

    @ApiModelProperty(value = "最近学习课时名称")
    private String lastStudyPeriodName;

}
