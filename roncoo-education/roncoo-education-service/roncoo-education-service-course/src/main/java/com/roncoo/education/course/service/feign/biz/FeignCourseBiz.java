package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.es.EsCourse;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.feign.vo.CourseChapterVO;
import com.roncoo.education.course.feign.vo.CourseVO;
import com.roncoo.education.course.feign.vo.LiveCourseVO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 课程信息
 *
 * @author wujing
 */
@Component
public class FeignCourseBiz extends BaseBiz {

    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    @Autowired
    private CourseDao dao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private CourseIntroduceAuditDao courseIntroduceAuditDao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;

    public Page<CourseVO> listForPage(CourseQO qo) {
        CourseExample example = new CourseExample();
        Criteria c = example.createCriteria();
        if (qo.getNotInCourseNoList() != null && !qo.getNotInCourseNoList().isEmpty()) {
            c.andIdNotIn(qo.getNotInCourseNoList());
        }
        if (qo.getCategoryId1() != null) {
            c.andCategoryId1EqualTo(qo.getCategoryId1());
        }
        if (!StringUtils.isEmpty(qo.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(qo.getCourseName()));
        }
        if (qo.getStatusId() != null) {
            c.andStatusIdEqualTo(qo.getStatusId());
        }

        if (qo.getCourseCategory() != null) {
            c.andCourseCategoryEqualTo(qo.getCourseCategory());
        }
        if (qo.getIsFree() != null) {
            c.andIsFreeEqualTo(qo.getIsFree());
        }
        if (qo.getIsPutaway() != null) {
            c.andIsPutawayEqualTo(qo.getIsPutaway());
        }
        example.setOrderByClause(" status_id desc, is_putaway desc, course_sort asc, id desc ");
        Page<Course> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        Page<CourseVO> courseInfoVoPage = PageUtil.transform(page, CourseVO.class);
        // 获取分类名称
        for (CourseVO courseVO : courseInfoVoPage.getList()) {
            if (courseVO.getCategoryId1() != null && courseVO.getCategoryId1() != 0) {
                CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId1());
                if (!StringUtils.isEmpty(courseCategory)) {
                    courseVO.setCategoryName1(courseCategory.getCategoryName());
                }
            }
            if (courseVO.getCategoryId2() != null && courseVO.getCategoryId2() != 0) {
                CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId2());
                if (!StringUtils.isEmpty(courseCategory)) {
                    courseVO.setCategoryName2(courseCategory.getCategoryName());
                }
            }
            if (courseVO.getCategoryId3() != null && courseVO.getCategoryId3() != 0) {
                CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId3());
                if (!StringUtils.isEmpty(courseCategory)) {
                    courseVO.setCategoryName3(courseCategory.getCategoryName());
                }
            }
        }
        return courseInfoVoPage;
    }

    public int save(CourseQO qo) {
        Course record = BeanUtil.copyProperties(qo, Course.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public CourseVO getById(Long id) {
        // 根据id查找课程信息
        Course record = dao.getById(id);
        CourseVO course = BeanUtil.copyProperties(record, CourseVO.class);
        if (ObjectUtil.isNotNull(course)) {
            // 根据id查找课程简介信息
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(record.getIntroduceId());
            // 把课程简介带回课程信息
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                course.setIntroduce(courseIntroduce.getIntroduce());
            }
        }
        return course;
    }

    public CourseVO getByIdAndStatusId(Long id, Integer statusId) {
        // 根据id查找课程信息
        Course record = dao.getByCourseIdAndStatusId(id, statusId);
        CourseVO course = BeanUtil.copyProperties(record, CourseVO.class);
        if (ObjectUtil.isNotNull(course)) {
            LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(course.getLecturerUserNo());
            if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getLecturerName())) {
                course.setLecturerName(lecturerVO.getLecturerName());
            }
            if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getHeadImgUrl())) {
                course.setHeadImgUrl(lecturerVO.getHeadImgUrl());
            }
            // 根据id查找课程简介信息
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(record.getIntroduceId());
            // 把课程简介带回课程信息
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                course.setIntroduce(courseIntroduce.getIntroduce());
            }
        }
        return course;
    }

    public LiveCourseVO getLiveCourseWithTimeByIdAndStatusId(Long id, Integer statusId) {
        // 根据id查找课程信息
        Course record = dao.getByCourseIdAndStatusId(id, statusId);
        if (ObjectUtil.isEmpty(record)) {
            return null;
        }
        LiveCourseVO course = BeanUtil.copyProperties(record, LiveCourseVO.class);
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(course.getLecturerUserNo());
        if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getLecturerName())) {
            course.setLecturerName(lecturerVO.getLecturerName());
        }
        if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getHeadImgUrl())) {
            course.setHeadImgUrl(lecturerVO.getHeadImgUrl());
        }
        // 根据id查找课程简介信息
        CourseIntroduce courseIntroduce = courseIntroduceDao.getById(record.getIntroduceId());
        // 把课程简介带回课程信息
        if (ObjectUtil.isNotNull(courseIntroduce)) {
            course.setIntroduce(courseIntroduce.getIntroduce());
        }
        // 获取课程第一章
        List<CourseChapter> list = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(course.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(list)) {
            // 获取课程第一章第一节课时
            List<CourseChapterPeriod> period = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortAsc(list.get(0).getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(period)) {
                course.setStartTime(period.get(0).getStartTime());
            }
        }
        // 获取课程第一章
        List<CourseChapter> listDesc = courseChapterDao.listByCourseIdAndStatusIdAndSortDesc(course.getId(), StatusIdEnum.YES.getCode());
        // 获取课程第一章第一节课时
        if (CollectionUtil.isNotEmpty(listDesc)) {
            List<CourseChapterPeriod> periodDesc = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortDesc(listDesc.get(0).getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(periodDesc)) {
                course.setEndTime(periodDesc.get(0).getEndTime());
            }
        }
        return course;
    }

    @Transactional(rollbackFor = Exception.class)
    public int updateById(CourseQO qo) {
        if (IsFreeEnum.FREE.getCode().equals(qo.getIsFree())) {
            qo.setCourseOriginal(BigDecimal.ZERO);
            qo.setCourseDiscount(BigDecimal.ZERO);
        }
        Course course = dao.getById(qo.getId());
        if (ObjectUtil.isNull(course)) {
            throw new BaseException("找不到课程信息");
        }

        Course record = BeanUtil.copyProperties(qo, Course.class);
        if (record.getIntroduceId() != null && !record.getIntroduceId().equals(0L)) {
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(record.getIntroduceId());
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                courseIntroduce.setId(record.getIntroduceId());
                courseIntroduce.setIntroduce(qo.getIntroduce());
                courseIntroduceDao.updateById(courseIntroduce);
            }
        }
        dao.updateById(record);
        // 同步更新审核
        CourseAudit courseAudit = BeanUtil.copyProperties(qo, CourseAudit.class);
        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            CourseIntroduceAudit courseIntroduceAudit = courseIntroduceAuditDao.getById(courseAudit.getIntroduceId());
            if (ObjectUtil.isNotNull(courseIntroduceAudit)) {
                courseIntroduceAudit.setId(courseAudit.getIntroduceId());
                courseIntroduceAudit.setIntroduce(qo.getIntroduce());
                courseIntroduceAuditDao.updateById(courseIntroduceAudit);
            }
        }
        return courseAuditDao.updateById(courseAudit);
    }

    public CourseVO getByCourseId(Long id) {
        Course record = dao.getById(id);
        if (ObjectUtil.isNull(record)) {
            throw new BaseException("找不到课程信息");
        }
        CourseVO courseVO = BeanUtil.copyProperties(record, CourseVO.class);
        // 获取分类名称
        if (courseVO.getCategoryId1() != null && courseVO.getCategoryId1() != 0) {
            CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId1());
            courseVO.setCategoryName1(courseCategory.getCategoryName());
        }
        if (courseVO.getCategoryId2() != null && courseVO.getCategoryId2() != 0) {
            CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId2());
            courseVO.setCategoryName2(courseCategory.getCategoryName());
        }
        if (courseVO.getCategoryId3() != null && courseVO.getCategoryId3() != 0) {
            CourseCategory courseCategory = courseCategoryDao.getById(courseVO.getCategoryId3());
            courseVO.setCategoryName3(courseCategory.getCategoryName());
        }
        // 章节
        List<CourseChapter> ChapterList = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(courseVO.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(ChapterList)) {
            List<CourseChapterVO> courseChapterVOList = new ArrayList<>();
            for (CourseChapter courseChapter : ChapterList) {
                // 课时
                List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortAsc(courseChapter.getId(), StatusIdEnum.YES.getCode());
                CourseChapterVO courseChapterVO = BeanUtil.copyProperties(courseChapter, CourseChapterVO.class);
                courseChapterVO.setCourseChapterPeriodVOList(PageUtil.copyList(periodList, CourseChapterPeriodVO.class));
                courseChapterVOList.add(courseChapterVO);
            }
            courseVO.setCourseChapterVOList(courseChapterVOList);
        }
        return courseVO;
    }

    public CourseVO getByCourseIdAndStatusId(CourseQO qo) {
        Course course = dao.getByCourseIdAndStatusId(qo.getId(), qo.getStatusId());
        return BeanUtil.copyProperties(course, CourseVO.class);
    }

    public CourseVO getByIdAndCourseCategory(CourseQO qo) {
        Course course = dao.getByCouseIdAndCourseCategory(qo.getId(), qo.getCourseCategory());
        return BeanUtil.copyProperties(course, CourseVO.class);
    }

    /**
     * 导入ES
     *
     * @param qo
     */
    public boolean addEs(CourseQO qo) {
        if (StringUtils.isEmpty(qo.getIds())) {
            return false;
        }
        String[] courseInfoId = qo.getIds().split(",");
        List<IndexQuery> queries = new ArrayList<>();
        try {
            // 查询讲师名称并插入es
            for (String i : courseInfoId) {
                Course course = dao.getById(Long.valueOf(i));
                EsCourse esCourseQO = BeanUtil.copyProperties(course, EsCourse.class);
                LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(course.getLecturerUserNo());
                if (!ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getLecturerName())) {
                    esCourseQO.setLecturerName(lecturerVO.getLecturerName());
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esCourseQO).build();
                queries.add(query);
            }
            elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsCourse.COURSE));
        } catch (Exception e) {
            logger.warn("elasticsearch更新数据失败", e);
        }
        return true;
    }

    public List<CourseVO> listByIds(CourseQO courseQO) {
        List<Course> list = dao.listByIds(courseQO.getListId());
        return ArrayListUtil.copy(list, CourseVO.class);
    }
}
