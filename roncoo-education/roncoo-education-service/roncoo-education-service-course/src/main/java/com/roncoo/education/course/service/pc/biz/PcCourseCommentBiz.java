package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.NOUtil;
import com.roncoo.education.course.common.req.CourseCommentEditREQ;
import com.roncoo.education.course.common.req.CourseCommentListREQ;
import com.roncoo.education.course.common.req.CourseCommentSaveREQ;
import com.roncoo.education.course.common.resp.CourseCommentListRESP;
import com.roncoo.education.course.common.resp.CourseCommentViewRESP;
import com.roncoo.education.course.service.dao.CourseCommentDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程评论
 *
 * @author Quanf
 */
@Component
public class PcCourseCommentBiz extends BaseBiz {

    @Autowired
    private CourseCommentDao dao;

    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private CourseDao courseDao;

    /**
     * 课程评论列表
     *
     * @param courseCommentListREQ 课程评论分页查询参数
     * @return 课程评论分页查询结果
     */
    public Result<Page<CourseCommentListRESP>> list(CourseCommentListREQ courseCommentListREQ) {
        CourseCommentExample example = new CourseCommentExample();
        Criteria c = example.createCriteria();
        // 没有查询条件时显示0L
        if (StringUtils.isEmpty(courseCommentListREQ.getNickname()) && StringUtils.isEmpty(courseCommentListREQ.getContent())) {
            c.andParentIdEqualTo(0L);
        }
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andCourseIdEqualTo(courseCommentListREQ.getCourseId());
        // 有查询条件时，模糊出所有
        if (!StringUtils.isEmpty(courseCommentListREQ.getNickname())) {
            c.andNicknameLike(PageUtil.like(courseCommentListREQ.getNickname()));
        }
        if (!StringUtils.isEmpty(courseCommentListREQ.getContent())) {
            c.andContentLike(PageUtil.like(courseCommentListREQ.getContent()));
        }
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<CourseComment> page = dao.listForPage(courseCommentListREQ.getPageCurrent(), courseCommentListREQ.getPageSize(), example);
        Page<CourseCommentListRESP> dto = PageUtil.transform(page, CourseCommentListRESP.class);
        for (CourseCommentListRESP courseComment : dto.getList()) {
            CourseComment comment = dao.getById(courseComment.getId());
            if (ObjectUtil.isNotNull(comment)) {
                if (StringUtils.hasText(comment.getContent())) {
                    courseComment.setContent(comment.getContent());
                }
            }
            // 被评论者用户
            UserExtVO courseUser = feignUserExt.getByUserNo(courseComment.getCourseUserNo());
            if (ObjectUtil.isNotNull(courseUser)) {
                if (StringUtils.isEmpty(courseComment.getCourseNickname())) {
                    if (StringUtils.isEmpty(courseUser.getNickname())) {
                        courseComment.setCourseNickname(courseUser.getMobile().substring(0, 3) + "****" + courseUser.getMobile().substring(7));
                    } else {
                        courseComment.setCourseNickname(courseUser.getNickname());
                    }
                }
                if (StringUtils.hasText(courseUser.getHeadImgUrl())) {
                    courseComment.setCourseUserImg(courseUser.getHeadImgUrl());
                }
            }
            // 评论者用户
            UserExtVO user = feignUserExt.getByUserNo(courseComment.getUserNo());
            if (ObjectUtil.isNotNull(user)) {
                if (StringUtils.isEmpty(courseComment.getNickname())) {
                    if (StringUtils.isEmpty(user.getNickname())) {
                        courseComment.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
                    } else {
                        courseComment.setNickname(user.getNickname());
                    }
                }
                if (StringUtils.hasText(user.getHeadImgUrl())) {
                    courseComment.setUserImg(user.getHeadImgUrl());
                }
            }
            courseComment.setCourseCommentList(recursionList(courseComment.getId()));
        }
        return Result.success(dto);
    }

    /**
     * 展示第二级
     */
    private List<CourseCommentListRESP> recursionList(Long parentId) {
        List<CourseCommentListRESP> list = new ArrayList<>();
        List<CourseComment> courseCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(courseCommentList)) {
            for (CourseComment courseComment : courseCommentList) {
                CourseCommentListRESP vo = BeanUtil.copyProperties(courseComment, CourseCommentListRESP.class);
                CourseComment comment = dao.getById(vo.getId());
                if (ObjectUtil.isNotNull(comment)) {
                    vo.setContent(comment.getContent());
                }
                // 被评论者用户
                UserExtVO courseUser = feignUserExt.getByUserNo(vo.getCourseUserNo());
                if (ObjectUtil.isNotNull(courseUser)) {
                    if (StringUtils.isEmpty(courseComment.getCourseNickname())) {
                        if (StringUtils.isEmpty(courseUser.getNickname())) {
                            vo.setCourseNickname(courseUser.getMobile().substring(0, 3) + "****" + courseUser.getMobile().substring(7));
                        } else {
                            vo.setCourseNickname(courseUser.getNickname());
                        }
                    }
                    if (StringUtils.hasText(courseUser.getHeadImgUrl())) {
                        vo.setCourseUserImg(courseUser.getHeadImgUrl());
                    }
                }
                // 评论者用户
                UserExtVO user = feignUserExt.getByUserNo(vo.getUserNo());
                if (ObjectUtil.isNotNull(user)) {
                    if (StringUtils.isEmpty(courseComment.getNickname())) {
                        if (StringUtils.isEmpty(user.getNickname())) {
                            vo.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
                        } else {
                            vo.setNickname(user.getNickname());
                        }
                    }
                    if (StringUtils.hasText(user.getHeadImgUrl())) {
                        vo.setUserImg(user.getHeadImgUrl());
                    }
                }
                list.add(vo);
            }
        }
        return list;
    }


    /**
     * 课程评论添加
     *
     * @param courseCommentSaveREQ 课程评论
     * @return 添加结果
     */
    public Result<String> save(CourseCommentSaveREQ courseCommentSaveREQ) {
        // 根据课程ID查询课程信息
        Course course = courseDao.getById(courseCommentSaveREQ.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("课程信息不存在");
        }

        // 被评论者用户信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(course.getLecturerUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("被评论者用户教育信息不存在");
        }

        // 添加评论
        CourseComment courseComment = new CourseComment();
        courseComment.setParentId(courseCommentSaveREQ.getParentId());
        courseComment.setCourseId(courseCommentSaveREQ.getCourseId());
        courseComment.setCourseName(course.getCourseName());
        courseComment.setCourseCategory(course.getCourseCategory());
        courseComment.setCourseUserNo(course.getLecturerUserNo());
        courseComment.setCourseNickname(userExtVO.getNickname());
        courseComment.setContent(courseCommentSaveREQ.getContent());
        courseComment.setUserNo(NOUtil.getUserNo());
        courseComment.setNickname(courseCommentSaveREQ.getNickname());
        if (dao.save(courseComment) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 课程评论查看
     *
     * @param id 主键ID
     * @return 课程评论
     */
    public Result<CourseCommentViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseCommentViewRESP.class));
    }


    /**
     * 课程评论修改
     *
     * @param courseCommentEditREQ 课程评论修改对象
     * @return 修改结果
     */
    public Result<String> edit(CourseCommentEditREQ courseCommentEditREQ) {
        CourseComment record = BeanUtil.copyProperties(courseCommentEditREQ, CourseComment.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 课程评论删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        // 查询评论信息
        CourseComment courseComment = dao.getById(id);
        if (ObjectUtil.isNull(courseComment)) {
            return Result.error("该评论不存在");
        }
        // 查询博客信息
        Course course = courseDao.getById(courseComment.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("课程信息不存在");
        }
        courseComment.setStatusId(StatusIdEnum.NO.getCode());
        if (dao.updateById(courseComment) > 0) {
            // 只统计一级的评论，更新博客信息
            if (courseComment.getParentId() == 0) {
                // 删除该一级评论下的所有二级评论
                List<CourseComment> courseCommentList = dao.listByParentIdAndStatusId(id, StatusIdEnum.YES.getCode());
                if (CollectionUtils.isEmpty(courseCommentList)) {
                    return Result.success("删除成功");
                }
                for (CourseComment comment : courseCommentList) {
                    comment.setStatusId(StatusIdEnum.NO.getCode());
                    dao.updateById(comment);
                }
            }
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
