package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;

import java.util.List;

public interface CrontabPlanPolyvDao {
    int save(CrontabPlanPolyv record);

    int deleteById(Long id);

    int updateById(CrontabPlanPolyv record);

    int updateByExampleSelective(CrontabPlanPolyv record, CrontabPlanPolyvExample example);

    CrontabPlanPolyv getById(Long id);

    Page<CrontabPlanPolyv> listForPage(int pageCurrent, int pageSize, CrontabPlanPolyvExample example);

    /**
     * 根据时间升序查询
     *
     * @param example 查询条件
     * @return
     */
    List<CrontabPlanPolyv> listForTimeAsc(CrontabPlanPolyvExample example);
}
