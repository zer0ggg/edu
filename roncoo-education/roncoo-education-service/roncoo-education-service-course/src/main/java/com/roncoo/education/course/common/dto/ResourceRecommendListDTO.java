package com.roncoo.education.course.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 文库推荐
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ResourceRecommendDTO", description="文库推荐")
public class ResourceRecommendListDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     *文库推荐列表
     */
    @ApiModelProperty(value = "文库推荐列表", required = true)
    private List<ResourceRecommendDTO> list = new ArrayList<>();

}
