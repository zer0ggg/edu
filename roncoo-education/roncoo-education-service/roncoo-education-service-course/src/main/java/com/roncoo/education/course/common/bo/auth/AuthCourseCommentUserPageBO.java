package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class AuthCourseCommentUserPageBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 被评论者用户编号
     */
    @ApiModelProperty(value = "被评论者用户编号")
    private Long courseUserNo;
    /**
     * 评论者用户编号
     */
    @ApiModelProperty(value = "评论者用户编号")
    private Long commentUserNo;
    /**
     * 课程分类(1点播,2直播,4文库)
     */
    @NotNull(message = "courseCategory课程分类(1点播,2直播,4文库)")
    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)")
    private Integer courseCategory;
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageCurrent = 1;
    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer pageSize = 20;
}
