package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample;

public interface CourseLiveLogDao {
    int save(CourseLiveLog record);

    int deleteById(Long id);

    int updateById(CourseLiveLog record);

    int updateByExampleSelective(CourseLiveLog record, CourseLiveLogExample example);

    CourseLiveLog getById(Long id);

    Page<CourseLiveLog> listForPage(int pageCurrent, int pageSize, CourseLiveLogExample example);

    /**
     * 根据频道编码、直播状态获取直播记录信息
     *
     * @param channelId
     * @param liveStatus
     * @author kyh
     */
    CourseLiveLog getByChannelIdAndLiveStatus(String channelId, Integer liveStatus);

    /**
     * 根据频道号和直播状态获取直播记录
     *
     * @param periodId
     * @param liveStatus
     * @return
     */
    CourseLiveLog getByPeriodIdAndLiveStatus(Long periodId, Integer liveStatus);

    /**
     * 根据直播平台和频道ID获取直播记录
     *
     * @param livePlatform 直播平台
     * @param channelId    频道ID
     * @return 直播记录
     */
    CourseLiveLog getByLivePlatformAndChannelId(Integer livePlatform, String channelId);

    /**
     * 根据频道号和场次id获取直播记录
     *
     * @param channelId
     * @param sessionId
     * @return
     */
    CourseLiveLog getByChannelIdAndSessionId(String channelId, String sessionId);

    /**
     * 根据课时id获取直播记录
     *
     * @param periodId
     * @return
     */
    CourseLiveLog getByPeriodId(Long periodId);
}
