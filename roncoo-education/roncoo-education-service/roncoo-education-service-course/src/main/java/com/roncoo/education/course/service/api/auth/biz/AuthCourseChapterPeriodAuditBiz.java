package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.*;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Component
public class AuthCourseChapterPeriodAuditBiz extends BaseBiz {

    @Autowired
    private CourseVideoDao courseVideoDao;
    @Autowired
    private CourseAccessoryDao accessoryDao;
    @Autowired
    private CourseChapterPeriodDao periodDao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseChapterAuditDao chapterAuditDao;
    @Autowired
    private CourseChapterPeriodAuditDao periodAuditDao;
    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    /**
     * 课时列出接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<AuthPeriodAuditListDTO> listByChapterId(AuthCourseChapterPeriodAuditBO bo) {
        if (bo.getChapterId() == null) {
            return Result.error("章节ID不能为空");
        }
        CourseChapterAudit courseChapter = chapterAuditDao.getById(bo.getChapterId());
        if (ObjectUtil.isNull(courseChapter)) {
            return Result.error("找不到章节信息");
        }
        CourseAudit courseAudit = courseAuditDao.getById(courseChapter.getCourseId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }

        // 根据章节ID查询课时审核信息表
        List<CourseChapterPeriodAudit> periodAuditList = periodAuditDao.listByChapterIdAndStatusId(bo.getChapterId(), StatusIdEnum.YES.getCode());
        AuthPeriodAuditListDTO dto = new AuthPeriodAuditListDTO();
        if (CollectionUtil.isNotEmpty(periodAuditList)) {
            List<Long> periodIds = periodAuditList.stream().map(CourseChapterPeriodAudit::getId).collect(Collectors.toList());
            // 根据课时ID集合获取课时信息
            List<CourseChapterPeriod> periodlist = periodDao.listByIds(periodIds);
            Map<Long, CourseChapterPeriod> periodlistMap = new HashMap<>();
            if (CollectionUtil.isEmpty(periodlist)) {
                periodlistMap = periodlist.stream().collect(Collectors.toMap(CourseChapterPeriod::getId, item -> item));

            }
            List<AuthPeriodAuditDTO> periodAuditDTOList = ArrayListUtil.copy(periodAuditList, AuthPeriodAuditDTO.class);
            dto.setUserPeriodAuditList(periodAuditDTOList);
            for (AuthPeriodAuditDTO authPeriodAuditDTO : periodAuditDTOList) {
                if (ObjectUtil.isNotNull(periodlistMap.get(authPeriodAuditDTO.getId()))) {
                    authPeriodAuditDTO.setIsHasAudit(1);
                }
                // 查找课程关联考试信息
                CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
                courseExamRefQO.setRefId(authPeriodAuditDTO.getId());
                courseExamRefQO.setRefType(RefTypeEnum.PERIOD.getCode());
                CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
                if (ObjectUtil.isNotNull(courseExamRefVO)) {
                    authPeriodAuditDTO.setExamId(courseExamRefVO.getExamId());
                    authPeriodAuditDTO.setCourseExamRefId(courseExamRefVO.getId());
                    authPeriodAuditDTO.setExamName(courseExamRefVO.getExamName());
                }
                // 获取课时视频、附件、图片、直播信息
                getPeriod(authPeriodAuditDTO.getId(), authPeriodAuditDTO);
            }
        }
        return Result.success(dto);
    }

    /**
     * 课时查看接口
     *
     * @param authCourseChapterPeriodAuditViewBO
     * @return
     * @author wuyun
     */
    public Result<AuthPeriodAuditViewDTO> view(AuthCourseChapterPeriodAuditViewBO authCourseChapterPeriodAuditViewBO) {
        if (authCourseChapterPeriodAuditViewBO.getId() == null) {
            return Result.error("课时ID不能为空");
        }
        CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(authCourseChapterPeriodAuditViewBO.getId());
        if (ObjectUtil.isNull(periodAudit)) {
            return Result.error("找不到课时信息");
        }
        CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课时信息");
        }
        return Result.success(BeanUtil.copyProperties(periodAudit, AuthPeriodAuditViewDTO.class));
    }

    /**
     * 课时删除接口
     *
     * @param authCourseChapterPeriodAuditDeleteBO
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> delete(AuthCourseChapterPeriodAuditDeleteBO authCourseChapterPeriodAuditDeleteBO) {
        if (authCourseChapterPeriodAuditDeleteBO.getId() == null) {
            return Result.error("课时ID不能为空");
        }
        CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(authCourseChapterPeriodAuditDeleteBO.getId());
        if (ObjectUtil.isNull(periodAudit)) {
            return Result.error("找不到课时信息");
        }
        CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课时信息");
        }

        // 更新课时接口状态为已冻结，设置为未审核
        periodAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        periodAudit.setStatusId(Constants.FREEZE);
        int result = periodAuditDao.updateById(periodAudit);
        if (result > 0) {
            // 更新章节信息审核表状态为未审核
            CourseChapterAudit chapterAudit = new CourseChapterAudit();
            chapterAudit.setId(periodAudit.getChapterId());
            chapterAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
            chapterAuditDao.updateById(chapterAudit);
            // 更新课程信息审核表状态为未审核
            CourseAudit courseAudit = new CourseAudit();
            courseAudit.setAuditStatus(AuditStatusEnum.WAIT.getCode());
            courseAudit.setId(course.getId());
            courseAuditDao.updateById(courseAudit);
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_DELETE_FAIL);
    }

    /**
     * 课时添加接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<AuthCourseChapterPeriodAuditSaveDTO> save(AuthCourseChapterPeriodAuditSaveBO bo) {
        if (bo.getChapterId() == null) {
            return Result.error("chapterId不能为空");
        }
        if (StringUtils.isEmpty(bo.getPeriodName())) {
            return Result.error("课时名称不能为空");
        }
        if (bo.getIsFree() == null) {
            return Result.error("isFree不能为空");
        }
        CourseChapterAudit chapterAudit = chapterAuditDao.getById(bo.getChapterId());
        if (ObjectUtil.isNull(chapterAudit)) {
            return Result.error("找不到章节信息");
        }
        CourseAudit course = courseAuditDao.getById(chapterAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        //是否需要人脸对比--系统不开启人脸识别，课程则默认不需要
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.ISFACECONTRAS);
        if (ObjectUtil.isNotEmpty(sysConfigVO) && String.valueOf(IsFaceContrasEnum.NO.getCode()).equals(sysConfigVO.getConfigValue())) {
            bo.setIsFaceContras(IsFaceContrasEnum.NO.getCode());
        }
        CourseChapterPeriodAudit record = BeanUtil.copyProperties(bo, CourseChapterPeriodAudit.class);
        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            if (StrUtil.isBlank(bo.getStartTime()) || StrUtil.isBlank(bo.getEndTime())) {
                return Result.error("直播开始时间和直播结束时间不能为空");
            }
            Date startTime = DateUtil.parse(bo.getStartTime());
            Date endTime = DateUtil.parse(bo.getEndTime());
            long between = cn.hutool.core.date.DateUtil.between(startTime, endTime, DateUnit.SECOND, false);
            if (between <= 0) {
                return Result.error("直播结束时间必须大于开始时间");
            }
            if (between > 21600) {
                return Result.error("直播时长不能大于6小时");
            }
            record.setStartTime(startTime);
            record.setEndTime(endTime);
        }

        record.setCourseId(chapterAudit.getCourseId());
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            record.setLiveStatus(LiveStatusEnum.NOT.getCode());
        }
        int results = periodAuditDao.save(record);
        if (results > 0) {
            // 更新课程审核表、章节审核表的审核状态为待审核
            chapterAuditDao.updateAuditStatusByChapterNo(AuditStatusEnum.WAIT.getCode(), record.getChapterId());
            courseAuditDao.updateAuditStatusBycourseId(AuditStatusEnum.WAIT.getCode(), record.getCourseId());
            // 再复制回dto进行返回
            AuthCourseChapterPeriodAuditSaveDTO dto = BeanUtil.copyProperties(record, AuthCourseChapterPeriodAuditSaveDTO.class);
            return Result.success(dto);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }

    /**
     * 课时更新接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> update(AuthCourseChapterPeriodAuditUpdateBO bo) {
        if (bo.getId() == null) {
            return Result.error("课时ID不能为空");
        }
        if (StringUtils.isEmpty(bo.getPeriodName())) {
            return Result.error("课时名称不能为空");
        }
        if (bo.getIsFree() == null) {
            return Result.error("isFree不能为空");
        }
        CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(bo.getId());
        if (ObjectUtil.isNull(periodAudit)) {
            return Result.error("找不到课时信息");
        }
        CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        //是否需要人脸对比--系统不开启人脸识别，课程则默认不需要
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.ISFACECONTRAS);
        if (ObjectUtil.isNotEmpty(sysConfigVO) && String.valueOf(IsFaceContrasEnum.NO.getCode()).equals(sysConfigVO.getConfigValue())) {
            bo.setIsFaceContras(IsFaceContrasEnum.NO.getCode());
        }

        CourseChapterPeriodAudit record = BeanUtil.copyProperties(bo, CourseChapterPeriodAudit.class);
        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            if (StrUtil.isBlank(bo.getStartTime()) || StrUtil.isBlank(bo.getEndTime())) {
                return Result.error("直播开始时间和直播结束时间不能为空");
            }
            Date startTime = DateUtil.parse(bo.getStartTime());
            Date endTime = DateUtil.parse(bo.getEndTime());
            long between = cn.hutool.core.date.DateUtil.between(startTime, endTime, DateUnit.HOUR, false);
            if (between <= 0) {
                return Result.error("直播结束时间必须大于开始时间");
            }
            if (between > 6) {
                return Result.error("直播时长不能大于6小时");
            }
            record.setStartTime(startTime);
            record.setEndTime(endTime);
        }
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        int result = periodAuditDao.updateById(record);
        if (result > 0) {
            courseAuditDao.updateAuditStatusBycourseId(AuditStatusEnum.WAIT.getCode(), periodAudit.getCourseId());
            chapterAuditDao.updateAuditStatusByChapterNo(AuditStatusEnum.WAIT.getCode(), periodAudit.getChapterId());
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_UPDATE_FAIL);
    }

    /**
     * 更新课时排序接口
     *
     * @param authCourseChapterPeriodAuditSortBO
     * @return
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> sort(AuthCourseChapterPeriodAuditSortBO authCourseChapterPeriodAuditSortBO) {
        if (CollectionUtil.isEmpty(authCourseChapterPeriodAuditSortBO.getPeriodIdList())) {
            return Result.error("课时信息不能为空");
        }
        if (CollectionUtil.isNotEmpty(authCourseChapterPeriodAuditSortBO.getPeriodIdList())) {
            int i = 1;
            for (Long periodId : authCourseChapterPeriodAuditSortBO.getPeriodIdList()) {
                // 更新课时审核信息表的排序
                periodAuditDao.updateSortByPeriodId(i++, periodId);
                CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(periodId);
                CourseChapterPeriod period = periodDao.getById(periodId);
                CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
                if (ObjectUtil.isNull(course)) {
                    return Result.error("找不到课程信息");
                }
                if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
                    return Result.error("找不到课程信息");
                }
                // 更新课时信息集合
                if (ObjectUtil.isNotNull(period)) {
                    CourseChapterPeriod record = new CourseChapterPeriod();
                    record.setId(period.getId());
                    record.setSort(periodAudit.getSort());
                    periodDao.updateById(record);
                }
            }
            return Result.success(i);
        }
        return Result.error("设置排序失败");
    }

    public Result<Integer> videoSave(AuthCourseChapterPeriodAuditVideoSaveBO bo) {
        if (bo.getPeriodId() == null) {
            return Result.error("课时ID不能为空");
        }
        CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(bo.getPeriodId());
        if (ObjectUtil.isNull(periodAudit) || !StatusIdEnum.YES.getCode().equals(periodAudit.getStatusId())) {
            return Result.error("找不到课时信息");
        }
        CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        if (bo.getVideoId() != null) {
            CourseVideo courseVideo = courseVideoDao.getById(bo.getVideoId());
            if (ObjectUtil.isNull(courseVideo) || !StatusIdEnum.YES.getCode().equals(courseVideo.getStatusId())) {
                return Result.error("视频异常");
            }
            periodAudit.setIsVideo(IsVideoEnum.YES.getCode());
            periodAudit.setVideoId(bo.getVideoId());
            periodAudit.setVideoName(courseVideo.getVideoName());
            periodAudit.setVideoLength(courseVideo.getVideoLength());
            periodAudit.setVideoVid(courseVideo.getVideoVid());
        } else {
            // 视频id 为空删除课时视频
            periodAudit.setVideoId(0L);
            periodAudit.setVideoName("");
            periodAudit.setVideoLength("");
            periodAudit.setVideoVid("");
            periodAudit.setIsVideo(IsVideoEnum.NO.getCode());
        }
        int result = periodAuditDao.updateById(periodAudit);
        if (result > 0) {
            // 更新课程、章节、课时 审核状态为待审核
            courseAuditDao.updateAuditStatusBycourseId(AuditStatusEnum.WAIT.getCode(), periodAudit.getCourseId());
            chapterAuditDao.updateAuditStatusByChapterNo(AuditStatusEnum.WAIT.getCode(), periodAudit.getChapterId());
            periodAuditDao.updateAuditStatusByPeriodId(AuditStatusEnum.WAIT.getCode(), periodAudit.getId());
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }

    /**
     * 查找课时视频信息
     *
     * @param bo
     * @author kyh
     */
    public Result<AuthPeriodAuditVideoViewDTO> videoView(AuthCourseChapterPeriodAuditVideoViewBO bo) {
        if (bo.getPeriodId() == null) {
            return Result.error("课时ID不能为空");
        }
        CourseChapterPeriodAudit periodAudit = periodAuditDao.getById(bo.getPeriodId());
        if (ObjectUtil.isNull(periodAudit) || !StatusIdEnum.YES.getCode().equals(periodAudit.getStatusId())) {
            return Result.error("找不到课时信息");
        }
        CourseAudit course = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        return Result.success(BeanUtil.copyProperties(periodAudit, AuthPeriodAuditVideoViewDTO.class));
    }

    /**
     * 获取课时视频、附件、图片
     *
     * @param periodId
     * @param authPeriodAuditDTO
     */
    private void getPeriod(Long periodId, AuthPeriodAuditDTO authPeriodAuditDTO) {
        // 根据课时ID统计附件数
        List<CourseAccessory> list = accessoryDao.listByRefIdAndRefTypeAndStatusId(periodId, RefTypeEnum.PERIOD.getCode(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(list)) {
            authPeriodAuditDTO.setDocNum(list.size());
        }
    }

}
