package com.roncoo.education.course.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.util.Date;

public class CourseVideo implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Integer sort;

    private Long courseId;

    private Long chapterId;

    private String videoName;

    private Integer videoStatus;

    private String videoLength;

    private Integer vodPlatform;

    private String videoVid;

    private Integer videoBackupPlatform;

    private String videoBackupVid;

    private String videoOssId;

    private Boolean callbackHandleComplete;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName == null ? null : videoName.trim();
    }

    public Integer getVideoStatus() {
        return videoStatus;
    }

    public void setVideoStatus(Integer videoStatus) {
        this.videoStatus = videoStatus;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength == null ? null : videoLength.trim();
    }

    public Integer getVodPlatform() {
        return vodPlatform;
    }

    public void setVodPlatform(Integer vodPlatform) {
        this.vodPlatform = vodPlatform;
    }

    public String getVideoVid() {
        return videoVid;
    }

    public void setVideoVid(String videoVid) {
        this.videoVid = videoVid == null ? null : videoVid.trim();
    }

    public Integer getVideoBackupPlatform() {
        return videoBackupPlatform;
    }

    public void setVideoBackupPlatform(Integer videoBackupPlatform) {
        this.videoBackupPlatform = videoBackupPlatform;
    }

    public String getVideoBackupVid() {
        return videoBackupVid;
    }

    public void setVideoBackupVid(String videoBackupVid) {
        this.videoBackupVid = videoBackupVid == null ? null : videoBackupVid.trim();
    }

    public String getVideoOssId() {
        return videoOssId;
    }

    public void setVideoOssId(String videoOssId) {
        this.videoOssId = videoOssId == null ? null : videoOssId.trim();
    }

    public Boolean getCallbackHandleComplete() {
        return callbackHandleComplete;
    }

    public void setCallbackHandleComplete(Boolean callbackHandleComplete) {
        this.callbackHandleComplete = callbackHandleComplete;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", sort=").append(sort);
        sb.append(", courseId=").append(courseId);
        sb.append(", chapterId=").append(chapterId);
        sb.append(", videoName=").append(videoName);
        sb.append(", videoStatus=").append(videoStatus);
        sb.append(", videoLength=").append(videoLength);
        sb.append(", vodPlatform=").append(vodPlatform);
        sb.append(", videoVid=").append(videoVid);
        sb.append(", videoBackupPlatform=").append(videoBackupPlatform);
        sb.append(", videoBackupVid=").append(videoBackupVid);
        sb.append(", videoOssId=").append(videoOssId);
        sb.append(", callbackHandleComplete=").append(callbackHandleComplete);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}