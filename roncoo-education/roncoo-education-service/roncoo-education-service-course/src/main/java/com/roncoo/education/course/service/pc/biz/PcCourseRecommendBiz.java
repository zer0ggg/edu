package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.BeanValidateUtil;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseListRESP;
import com.roncoo.education.course.common.resp.CourseRecommendListRESP;
import com.roncoo.education.course.common.resp.CourseRecommendViewRESP;
import com.roncoo.education.course.service.dao.CourseCategoryDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.CourseRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCategory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseRecommendExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * 课程推荐
 *
 * @author wujing
 */
@Component
public class PcCourseRecommendBiz extends BaseBiz {

    @Autowired
    private CourseRecommendDao dao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private PcCourseBiz pcCourseBiz;

    /**
     * 课程推荐列表
     *
     * @param req 课程推荐分页查询参数
     * @return 课程推荐分页查询结果
     */
    public Result<Page<CourseRecommendListRESP>> list(CourseRecommendListREQ req) {
        CourseRecommendExample example = new CourseRecommendExample();
        Criteria c = example.createCriteria();
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if(req.getCategoryId() != null){
            c.andCategoryIdEqualTo(req.getCategoryId());
        }
        example.setOrderByClause("sort asc, id desc");
        Page<CourseRecommend> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseRecommendListRESP> respPage = PageUtil.transform(page, CourseRecommendListRESP.class);
        for (CourseRecommendListRESP resp : respPage.getList()) {
            Course course = courseDao.getById(resp.getCourseId());
            if (ObjectUtil.isNotNull(course)) {
                resp.setCourseName(course.getCourseName());
                resp.setCourseCategory(course.getCourseCategory());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 课程推荐添加
     *
     * @param courseRecommendSaveREQ 课程推荐
     * @return 添加结果
     */
    public Result<String> save(CourseRecommendSaveREQ courseRecommendSaveREQ) {
        CourseRecommend courseRecommend = dao.getByCategoryIdAndCourseId(courseRecommendSaveREQ.getCategoryId(), courseRecommendSaveREQ.getCourseId());
        if (ObjectUtil.isNotNull(courseRecommend)) {
            return Result.error("已添加推荐课程");
        }
//        List<CourseRecommend> list = dao.listByCategoryIdAndStatusId(courseRecommendSaveREQ.getCategoryId(), StatusIdEnum.YES.getCode());
////        if (list.size() >= 5) {
////            return Result.error("课程只展示5个");
////        }
        CourseRecommend record = BeanUtil.copyProperties(courseRecommendSaveREQ, CourseRecommend.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 课程推荐批量添加
     * @param req
     * @return
     */
    public Result<String> saveBatch(CourseRecommendSaveBatchREQ req) {
        BeanValidateUtil.ValidationResult result =BeanValidateUtil.validate(req);
        if(!result.isPass()){
            return Result.error(result.getErrMsgString());
        }
        CourseCategory category = courseCategoryDao.getById(req.getCategoryId());
        if(ObjectUtils.isEmpty(category)){
            return Result.error("分类id不正确");
        }
        String[] courseIds = req.getCourseIds().split(",");
        try {
            for(String  courseIdStr: courseIds){
                Long courseId = Long.valueOf(courseIdStr);
                Course course = courseDao.getByCourseIdAndStatusId(courseId, StatusIdEnum.YES.getCode());
                if(ObjectUtils.isEmpty(course)){
                    //文库不存在
                    continue;
                }
                CourseRecommend courseRecommend = dao.getByCategoryIdAndCourseId(req.getCategoryId(), courseId);
                if(!ObjectUtils.isEmpty(courseRecommend)){
                    //已添加
                    continue;
                }
                CourseRecommend record = new CourseRecommend();
                record.setCategoryId(req.getCategoryId());
                record.setCourseId(courseId);
                dao.save(record);
            }
        }catch (Exception e){
            logger.error(e.toString());
        }

        return Result.success("添加成功");
    }



    /**
     * 课程推荐查看
     *
     * @param id 主键ID
     * @return 课程推荐
     */
    public Result<CourseRecommendViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseRecommendViewRESP.class));
    }


    /**
     * 课程推荐修改
     *
     * @param courseRecommendEditREQ 课程推荐修改对象
     * @return 修改结果
     */
    public Result<String> edit(CourseRecommendEditREQ courseRecommendEditREQ) {
        CourseRecommend record = BeanUtil.copyProperties(courseRecommendEditREQ, CourseRecommend.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 课程推荐删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    public Result<Page<CourseListRESP>> list(CourseListREQ courseListREQ) {
        return pcCourseBiz.list(courseListREQ);
    }


}
