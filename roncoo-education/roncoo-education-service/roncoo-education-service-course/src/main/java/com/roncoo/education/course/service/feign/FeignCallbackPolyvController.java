package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.course.feign.interfaces.IFeignCallbackPolyv;
import com.roncoo.education.course.feign.qo.PolyvVideoQO;
import com.roncoo.education.course.service.feign.biz.FeignCallbackPolyvBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 保利威视频回调
 */
@RestController
public class FeignCallbackPolyvController extends BaseController implements IFeignCallbackPolyv {

    @Autowired
    private FeignCallbackPolyvBiz biz;

    @Override
    public String callbackPolyvVideo(@RequestBody PolyvVideoQO polyvVideoQO) {
        return biz.callbackPolyvVideo(polyvVideoQO);
    }
}
