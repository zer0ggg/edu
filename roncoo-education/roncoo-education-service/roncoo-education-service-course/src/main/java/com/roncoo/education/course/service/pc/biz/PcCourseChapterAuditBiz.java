package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.common.req.CourseChapterAuditEditREQ;
import com.roncoo.education.course.common.req.CourseChapterAuditListREQ;
import com.roncoo.education.course.common.req.CourseChapterAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterAuditListRESP;
import com.roncoo.education.course.common.resp.CourseChapterAuditViewRESP;
import com.roncoo.education.course.service.dao.CourseChapterAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterAuditExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 章节信息-审核
 *
 * @author wujing
 */
@Component
public class PcCourseChapterAuditBiz extends BaseBiz {

    @Autowired
    private CourseChapterAuditDao dao;

    /**
    * 章节信息-审核列表
    *
    * @param req 章节信息-审核分页查询参数
    * @return 章节信息-审核分页查询结果
    */
    public Result<Page<CourseChapterAuditListRESP>> list(CourseChapterAuditListREQ req) {
        CourseChapterAuditExample example = new CourseChapterAuditExample();
            Criteria c = example.createCriteria();
            if (req.getCourseId() != null) {
                c.andCourseIdEqualTo(req.getCourseId());
            }
            if (req.getStatusId() != null) {
                c.andStatusIdEqualTo(req.getStatusId());
            }
            if (StringUtils.hasText(req.getChapterName())) {
                c.andChapterNameLike(PageUtil.like(req.getChapterName()));
            }
            example.setOrderByClause("sort asc, id asc");
        Page<CourseChapterAudit> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseChapterAuditListRESP> respPage = PageUtil.transform(page, CourseChapterAuditListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 章节信息-审核添加
    *
    * @param courseChapterAuditSaveREQ 章节信息-审核
    * @return 添加结果
    */
    public Result<String> save(CourseChapterAuditSaveREQ courseChapterAuditSaveREQ) {
        CourseChapterAudit record = BeanUtil.copyProperties(courseChapterAuditSaveREQ, CourseChapterAudit.class);
        record.setId(IdWorker.getId());
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 章节信息-审核查看
    *
    * @param id 主键ID
    * @return 章节信息-审核
    */
    public Result<CourseChapterAuditViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseChapterAuditViewRESP.class));
    }


    /**
    * 章节信息-审核修改
    *
    * @param courseChapterAuditEditREQ 章节信息-审核修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseChapterAuditEditREQ courseChapterAuditEditREQ) {
        CourseChapterAudit record = BeanUtil.copyProperties(courseChapterAuditEditREQ, CourseChapterAudit.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 章节信息-审核删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
