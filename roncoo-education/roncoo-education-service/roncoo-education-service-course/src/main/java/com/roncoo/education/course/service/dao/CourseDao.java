package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseExample;

import java.util.List;

public interface CourseDao {
    int save(Course record);

    int deleteById(Long id);

    int updateById(Course record);

    Course getById(Long id);

    Page<Course> listForPage(int pageCurrent, int pageSize, CourseExample example);

    /**
     * 根据课程编号和状态获取课程信息
     *
     * @param courseId
     * @param StatusId
     * @return
     */
    Course getByCourseIdAndStatusId(Long courseId, Integer StatusId);

    /**
     * 根据课程名称获取课程信息集合
     *
     * @param courseName
     * @return
     */
    List<Course> listByCourseName(String courseName);

    /**
     * 根据二级分类ID和状态获取课程信息
     *
     * @param categoryId2
     * @param statusId
     * @return
     */
    List<Course> listBycategoryId2AndStatusId(Long categoryId2, Integer statusId);

    /**
     * 根据课程id、课程类型查找课程信息
     *
     * @param id
     * @param courseCategory
     * @return
     */
    Course getByCouseIdAndCourseCategory(Long id, Integer courseCategory);

    /**
     * 获取课程信息集合
     *
     * @return
     */
    List<Course> getCourseList();

    /**
     * 根据课程分类获取课程信息集合
     *
     * @param courseCategory
     * @return
     */
    List<Course> listByCourseCategory(Integer courseCategory);

    /**
     * 根据状态获取课程信息集合
     *
     * @param statusId
     * @return
     */
    List<Course> listByStatusId(Integer statusId);

    /**
     * 根据id集合获取课程信息
     *
     * @param listId
     * @return
     */
    List<Course> listByIds(List<Long> listId);
}
