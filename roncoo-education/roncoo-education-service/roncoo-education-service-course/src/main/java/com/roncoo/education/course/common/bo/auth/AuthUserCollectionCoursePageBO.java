package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户收藏课程分类列表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserCollectionCoursePageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程分类(1:普通课程;2:直播课程,3:试卷)
	 */
	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:试卷)")
	private Integer courseCategory;
	/**
	 * 收藏类型:1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "收藏类型:1课程，2章节，3课时")
	private Integer collectionType;

	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页", required = true)
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数", required = true)
	private Integer pageSize = 20;
}
