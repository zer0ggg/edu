package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRef;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserOrderCourseRefMapper {
    int countByExample(UserOrderCourseRefExample example);

    int deleteByExample(UserOrderCourseRefExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserOrderCourseRef record);

    int insertSelective(UserOrderCourseRef record);

    List<UserOrderCourseRef> selectByExample(UserOrderCourseRefExample example);

    UserOrderCourseRef selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserOrderCourseRef record, @Param("example") UserOrderCourseRefExample example);

    int updateByExample(@Param("record") UserOrderCourseRef record, @Param("example") UserOrderCourseRefExample example);

    int updateByPrimaryKeySelective(UserOrderCourseRef record);

    int updateByPrimaryKey(UserOrderCourseRef record);
}