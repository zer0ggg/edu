package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class AuthCourseCommentUserSaveDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "被评论者用户编号")
    private Long courseUserNo;

    @ApiModelProperty(value = "被评论者用户头像")
    private String courseUserImg;

    @ApiModelProperty(value = "被评论者用户昵称")
    private String courseNickname;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "评论者用户头像")
    private String userImg;

    @ApiModelProperty(value = "评论者用户昵称")
    private String nickname;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)")
    private Integer courseCategory;

}
