package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 刷新视频上传凭证
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthWebUploadVideoRefreshBO", description = "刷新视频上传凭证")
public class AuthWebUploadVideoRefreshBO implements Serializable {

    private static final long serialVersionUID = 3937038198817941377L;

    @NotBlank(message = "视频Vid不能为空")
    @ApiModelProperty(value = "视频Vid", required = true)
    private String videoVid;
}
