package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.roncoo.education.course.common.dto.CourseAccessoryDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 讲师用户编码
     */
    @ApiModelProperty(value = "讲师用户编码")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lecturerUserNo;
    @ApiModelProperty(value = "分类ID1")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId1;
    @ApiModelProperty(value = "分类ID2")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId2;
    @ApiModelProperty(value = "分类ID3")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId3;
    /**
     * 是否免费：1免费，0收费
     */
    @ApiModelProperty(value = "是否免费：1免费，0收费")
    private Integer isFree;
    /**
     * 购买人数
     */
    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;
    /**
     * 学习人数
     */
    @ApiModelProperty(value = "学习人数")
    private Integer countStudy;
    /**
     * 总课时数
     */
    @ApiModelProperty(value = "总课时数")
    private Integer periodTotal;
    /**
     * 课程名称
     */
    @ApiModelProperty(value = "课程名称")
    private String courseName;
    /**
     * 课程封面
     */
    @ApiModelProperty(value = "课程封面")
    private String courseLogo;
    /**
     * 课程原价
     */
    @ApiModelProperty(value = "课程原价")
    private BigDecimal courseOriginal;
    /**
     * 课程优惠价
     */
    @ApiModelProperty(value = "课程优惠价")
    private BigDecimal courseDiscount;
    /**
     * 课程介绍
     */
    @ApiModelProperty(value = "课程介绍")
    private String introduce;

    /**
     * 讲师信息
     */
    @ApiModelProperty(value = "讲师信息")
    private AuthLecturerDTO lecturer;
    /**
     * 是否购买
     */
    @ApiModelProperty(value = "是否支付(1:已支付;0:未支付)")
    private Integer isPay;
    /**
     * 课程类型：(1普通,2直播，4文库)
     */
    @ApiModelProperty(value = "课程类型：(1普通,2直播，4文库)")
    private Integer courseCategory;

    /**
     * 直播状态(1:未开播,2:正在直播,3:回放)
     */
    @ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:回放)")
    private Integer liveStatus;
    /**
     * 课时是否免费
     */
    @ApiModelProperty(value = "课时是否免费(1:免费;0:收费)")
    private Integer isFreePeriod;
    /**
     * 是否上架(1:上架，0:下架)
     */
    @ApiModelProperty(value = "是否上架(1:上架，0:下架)")
    private Integer isPutaway;
    /**
     * 课时名称
     */
    @ApiModelProperty(value = "课时名称")
    private String periodName;
    /**
     * 直播开始时间
     */
    @ApiModelProperty(value = "直播开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 直播结束时间
     */
    @ApiModelProperty(value = "直播结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 正在直播课时id
     */
    @ApiModelProperty(value = "正在直播课时ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long periodId;

    /**
     * 用户是否收藏课程(1:已收藏,0:未收藏)
     */
    @ApiModelProperty(value = "用户是否收藏课程(1:已收藏,0:未收藏")
    private Integer isCollectionCourse;

    /**
     * 用户是否关注讲师(1:已关注,0:未关注)
     */
    @ApiModelProperty(value = " 用户是否关注讲师(1:已关注,0:未关注)")
    private Integer isAttentionLecturer;

    /**
     * 课程学习进度
     */
    @ApiModelProperty(value = "课程学习进度")
    private AuthCourseUserStudyLogDTO authCourseUserStudyLog;

    /**
     * 课时附件信息集合
     */
    @ApiModelProperty(value = "课时附件信息集合")
    private List<CourseAccessoryDTO> accessoryList = new ArrayList<>();
    /**
     * 推荐码
     */
    @ApiModelProperty(value = "推荐码")
    private String referralCode;

    /**
     * 分销比例
     */
    @ApiModelProperty(value = "分销比例")
    private BigDecimal chosenPercent;

    @ApiModelProperty(value = "试卷ID(如果讲师未设置返回null)")
    private Long examId;

}
