package com.roncoo.education.course.service.dao.impl.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UserOrderCourseRef implements Serializable {
    private Long id;

    private Date gmtCreate;

    private Date gmtModified;

    private Integer statusId;

    private Long lecturerUserNo;

    private Long userNo;

    private Integer courseCategory;

    private Integer courseType;

    private Long courseId;

    private Long refId;

    private Integer isPay;

    private Integer isStudy;

    private Long orderNo;

    private Date expireTime;

    private String courseLength;

    private BigDecimal studyLength;

    private Integer studyProcess;

    private Long lastStudyPeriodId;

    private Date lastStudyTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Long getLecturerUserNo() {
        return lecturerUserNo;
    }

    public void setLecturerUserNo(Long lecturerUserNo) {
        this.lecturerUserNo = lecturerUserNo;
    }

    public Long getUserNo() {
        return userNo;
    }

    public void setUserNo(Long userNo) {
        this.userNo = userNo;
    }

    public Integer getCourseCategory() {
        return courseCategory;
    }

    public void setCourseCategory(Integer courseCategory) {
        this.courseCategory = courseCategory;
    }

    public Integer getCourseType() {
        return courseType;
    }

    public void setCourseType(Integer courseType) {
        this.courseType = courseType;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    public Integer getIsStudy() {
        return isStudy;
    }

    public void setIsStudy(Integer isStudy) {
        this.isStudy = isStudy;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getCourseLength() {
        return courseLength;
    }

    public void setCourseLength(String courseLength) {
        this.courseLength = courseLength == null ? null : courseLength.trim();
    }

    public BigDecimal getStudyLength() {
        return studyLength;
    }

    public void setStudyLength(BigDecimal studyLength) {
        this.studyLength = studyLength;
    }

    public Integer getStudyProcess() {
        return studyProcess;
    }

    public void setStudyProcess(Integer studyProcess) {
        this.studyProcess = studyProcess;
    }

    public Long getLastStudyPeriodId() {
        return lastStudyPeriodId;
    }

    public void setLastStudyPeriodId(Long lastStudyPeriodId) {
        this.lastStudyPeriodId = lastStudyPeriodId;
    }

    public Date getLastStudyTime() {
        return lastStudyTime;
    }

    public void setLastStudyTime(Date lastStudyTime) {
        this.lastStudyTime = lastStudyTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", statusId=").append(statusId);
        sb.append(", lecturerUserNo=").append(lecturerUserNo);
        sb.append(", userNo=").append(userNo);
        sb.append(", courseCategory=").append(courseCategory);
        sb.append(", courseType=").append(courseType);
        sb.append(", courseId=").append(courseId);
        sb.append(", refId=").append(refId);
        sb.append(", isPay=").append(isPay);
        sb.append(", isStudy=").append(isStudy);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", expireTime=").append(expireTime);
        sb.append(", courseLength=").append(courseLength);
        sb.append(", studyLength=").append(studyLength);
        sb.append(", studyProcess=").append(studyProcess);
        sb.append(", lastStudyPeriodId=").append(lastStudyPeriodId);
        sb.append(", lastStudyTime=").append(lastStudyTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}