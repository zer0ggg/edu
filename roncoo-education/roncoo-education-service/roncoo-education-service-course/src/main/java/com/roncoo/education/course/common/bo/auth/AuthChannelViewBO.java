/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 获取频道号详情信息
 */
@Data
@Accessors(chain = true)
public class AuthChannelViewBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课时ID
     */
    @ApiModelProperty(value = "课时ID", required = true)
    private Long periodId;
    /**
     * 直播场景(alone 活动拍摄, ppt 三分屏)
     */
    @ApiModelProperty(value = "直播场景(alone 活动拍摄, ppt 三分屏)", required = true)
    private String scene;
}
