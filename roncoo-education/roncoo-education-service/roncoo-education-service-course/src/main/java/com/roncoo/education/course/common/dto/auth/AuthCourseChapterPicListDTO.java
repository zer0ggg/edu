package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 章节图片集合
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPicListDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 章节图片集合
	 */
	@ApiModelProperty(value = "章节图片集合")
	private List<AuthCourseChapterPicDTO> list = new ArrayList<>();
}
