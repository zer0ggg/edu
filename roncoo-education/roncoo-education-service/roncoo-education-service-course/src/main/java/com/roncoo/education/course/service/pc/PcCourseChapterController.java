package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseChapterEditREQ;
import com.roncoo.education.course.common.req.CourseChapterListREQ;
import com.roncoo.education.course.common.req.CourseChapterSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterListRESP;
import com.roncoo.education.course.common.resp.CourseChapterViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseChapterBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 章节信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/chapter")
@Api(value = "course-章节信息", tags = {"course-章节信息"})
public class PcCourseChapterController {

    @Autowired
    private PcCourseChapterBiz biz;

    @ApiOperation(value = "章节信息列表", notes = "章节信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseChapterListRESP>> list(@RequestBody CourseChapterListREQ courseChapterListREQ) {
        return biz.list(courseChapterListREQ);
    }

    @ApiOperation(value = "章节信息添加", notes = "章节信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseChapterSaveREQ courseChapterSaveREQ) {
        return biz.save(courseChapterSaveREQ);
    }

    @ApiOperation(value = "章节信息查看", notes = "章节信息查看")
    @GetMapping(value = "/view")
    public Result<CourseChapterViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "章节信息修改", notes = "章节信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseChapterEditREQ courseChapterEditREQ) {
        return biz.edit(courseChapterEditREQ);
    }

    @ApiOperation(value = "章节信息删除", notes = "章节信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
