package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 章节信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 章节集合信息
	 */
	@ApiModelProperty(value = "章节集合信息")
	private List<AuthCourseChapterDTO> chapterList = new ArrayList<>();
}
