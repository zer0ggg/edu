package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 讲师直播频道信息
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthChannelViewDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "直播平台(1:保利威、2:欢拓)")
    private Integer livePlatform;

    @ApiModelProperty(value = "频道编码")
    private String channelId;

    @ApiModelProperty(value = "频道密码")
    private String channelPasswd;

    @ApiModelProperty(value = "直播推流地址")
    private String pushFlowUrl;

    @ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏课)")
    private String scene;

    @ApiModelProperty(value = "助教频道")
    private String assistantChannelId;

    @ApiModelProperty(value = "助教密码")
    private String assistantPasswd;

    @ApiModelProperty(value = "助教链接")
    private String assistantUrl;

    @ApiModelProperty(value = "web端开播地址")
    private String webLiveUrl;

    @ApiModelProperty(value = "直播路径")
    private String liveUrl;
    /**
     * 	欢拓系统的主播id
     */
    @ApiModelProperty(value = "欢拓系统的主播id")
    private Integer bid;

}
