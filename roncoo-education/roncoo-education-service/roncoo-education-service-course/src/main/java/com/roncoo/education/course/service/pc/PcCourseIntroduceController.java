package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseIntroduceEditREQ;
import com.roncoo.education.course.common.req.CourseIntroduceListREQ;
import com.roncoo.education.course.common.req.CourseIntroduceSaveREQ;
import com.roncoo.education.course.common.resp.CourseIntroduceListRESP;
import com.roncoo.education.course.common.resp.CourseIntroduceViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseIntroduceBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程介绍信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/introduce")
@Api(value = "course-课程介绍信息", tags = {"course-课程介绍信息"})
public class PcCourseIntroduceController {

    @Autowired
    private PcCourseIntroduceBiz biz;

    @ApiOperation(value = "课程介绍信息列表", notes = "课程介绍信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseIntroduceListRESP>> list(@RequestBody CourseIntroduceListREQ courseIntroduceListREQ) {
        return biz.list(courseIntroduceListREQ);
    }

    @ApiOperation(value = "课程介绍信息添加", notes = "课程介绍信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseIntroduceSaveREQ courseIntroduceSaveREQ) {
        return biz.save(courseIntroduceSaveREQ);
    }

    @ApiOperation(value = "课程介绍信息查看", notes = "课程介绍信息查看")
    @GetMapping(value = "/view")
    public Result<CourseIntroduceViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程介绍信息修改", notes = "课程介绍信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseIntroduceEditREQ courseIntroduceEditREQ) {
        return biz.edit(courseIntroduceEditREQ);
    }

    @ApiOperation(value = "课程介绍信息删除", notes = "课程介绍信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
