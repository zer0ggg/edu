package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 直播转点存定时任务
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CrontabPlanPolyvEditREQ", description="直播转点存定时任务修改")
public class CrontabPlanPolyvEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "频道号")
    private String channelId;

    @ApiModelProperty(value = "回放")
    private String playback;

    @ApiModelProperty(value = "课时名称")
    private String periodName;

    @ApiModelProperty(value = "保利威视类型ID")
    private String polyvCategoryId;

    @ApiModelProperty(value = "失败次数")
    private Integer failCount;
}
