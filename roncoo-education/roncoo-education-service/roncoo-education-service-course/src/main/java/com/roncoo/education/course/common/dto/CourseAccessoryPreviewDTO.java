package com.roncoo.education.course.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 附件信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseAccessoryPreviewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 附件名称
	 */
	@ApiModelProperty(value = "附件名称")
	private String acName;
}
