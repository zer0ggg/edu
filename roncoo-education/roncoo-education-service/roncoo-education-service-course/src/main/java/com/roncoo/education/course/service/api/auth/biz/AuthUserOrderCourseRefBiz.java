package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthUserOrderCourseRefPageBO;
import com.roncoo.education.course.common.dto.auth.AuthUserOrderCourseRefPageDTO;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.UserOrderCourseRefDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserOrderCourseRefExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@Component
public class AuthUserOrderCourseRefBiz {

    @Autowired
    private IFeignUserExt feignUserExt;

    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;

    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;

    /**
     * 获取用户课程信息
     */
    public Result<Page<AuthUserOrderCourseRefPageDTO>> listForPage(AuthUserOrderCourseRefPageBO bo) {
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用户异常,请联系管理员");
        }
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        Criteria criteria = example.createCriteria();
        criteria.andUserNoEqualTo(ThreadContext.userNo());
        if (bo.getCourseCategory() != null) {
            criteria.andCourseCategoryEqualTo(bo.getCourseCategory());
        }
        if (bo.getCourseType() != null) {
            criteria.andCourseTypeEqualTo(bo.getCourseType());
        }
        example.setOrderByClause(" last_study_time desc ");
        Page<UserOrderCourseRef> page = userOrderCourseRefDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthUserOrderCourseRefPageDTO> listForPage = PageUtil.transform(page, AuthUserOrderCourseRefPageDTO.class);
        if (CollectionUtil.isEmpty(listForPage.getList())) {
            return Result.success(listForPage);
        }
        for (AuthUserOrderCourseRefPageDTO dto : listForPage.getList()) {
            // 获取课程信息
            Course course = courseDao.getById(dto.getCourseId());
            if (ObjectUtil.isEmpty(course)) {
                continue;
            }
            if (StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
                dto.setCourseLogo(course.getCourseLogo());
                dto.setCourseName(course.getCourseName());
            }
            // 课程学习记录
            if (dto.getLastStudyPeriodId() != null) {
                CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(dto.getLastStudyPeriodId());
                if (ObjectUtil.isNotNull(courseChapterPeriod)) {
                    dto.setPeriodName(courseChapterPeriod.getPeriodName());
                }
                dto.setLastStudyTime(dto.getGmtCreate());
            } else {
                // 没有对应的用户学习记录,则直接查找课程的第一章信息返回
                // 注意:当第一章没购买的时候
                List<CourseChapter> chapterList = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(dto.getCourseId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(chapterList)) {
                    dto.setChapterId(chapterList.get(0).getId());
                    dto.setChapterName(chapterList.get(0).getChapterName());
                }
            }
        }
        return Result.success(listForPage);
    }

}
