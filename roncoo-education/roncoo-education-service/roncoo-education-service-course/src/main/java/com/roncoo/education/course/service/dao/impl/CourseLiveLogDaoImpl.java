package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.impl.mapper.CourseLiveLogMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseLiveLogDaoImpl implements CourseLiveLogDao {
    @Autowired
    private CourseLiveLogMapper courseLiveLogMapper;

    @Override
    public int save(CourseLiveLog record) {
        record.setId(IdWorker.getId());
        return this.courseLiveLogMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.courseLiveLogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseLiveLog record) {
        return this.courseLiveLogMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(CourseLiveLog record, CourseLiveLogExample example) {
        return this.courseLiveLogMapper.updateByExampleSelective(record, example);
    }

    @Override
    public CourseLiveLog getById(Long id) {
        return this.courseLiveLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseLiveLog> listForPage(int pageCurrent, int pageSize, CourseLiveLogExample example) {
        int count = this.courseLiveLogMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.courseLiveLogMapper.selectByExample(example));
    }

    @Override
    public CourseLiveLog getByChannelIdAndLiveStatus(String channelId, Integer liveStatus) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andChannelIdEqualTo(channelId);
        c.andLiveStatusEqualTo(liveStatus);
        List<CourseLiveLog> list = this.courseLiveLogMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseLiveLog getByPeriodIdAndLiveStatus(Long periodId, Integer liveStatus) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andPeriodIdEqualTo(periodId);
        c.andLiveStatusEqualTo(liveStatus);
        List<CourseLiveLog> list = this.courseLiveLogMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseLiveLog getByLivePlatformAndChannelId(Integer livePlatform, String channelId) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andLivePlatformEqualTo(livePlatform);
        c.andChannelIdEqualTo(channelId);
        List<CourseLiveLog> list = this.courseLiveLogMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseLiveLog getByChannelIdAndSessionId(String channelId, String sessionId) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andChannelIdEqualTo(channelId);
        c.andSessionIdEqualTo(sessionId);
        List<CourseLiveLog> list = this.courseLiveLogMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public CourseLiveLog getByPeriodId(Long periodId) {
        CourseLiveLogExample example = new CourseLiveLogExample();
        Criteria c = example.createCriteria();
        c.andPeriodIdEqualTo(periodId);
        List<CourseLiveLog> list = this.courseLiveLogMapper.selectByExample(example);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

}
