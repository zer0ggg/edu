package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课时图片
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseChapterPeriodAuditPicDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 章节ID
	 */
	@ApiModelProperty(value = "章节ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long chapterId;
	/**
	 * 课时ID
	 */
	@ApiModelProperty(value = "课时ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long periodId;
	/**
	 * 图片ID
	 */
	@ApiModelProperty(value = "图片ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long picId;
	/**
	 * 图片类型(1:试卷题目,2:试卷答案)
	 */
	@ApiModelProperty(value = "图片类型(1:试卷题目,2:试卷答案)")
	private Integer picType;
	/**
	 * 图片地址
	 */
	@ApiModelProperty(value = "图片地址")
	private String picUrl;
	/**
	 * 标题
	 */
	@ApiModelProperty(value = "标题")
	private String title;
}
