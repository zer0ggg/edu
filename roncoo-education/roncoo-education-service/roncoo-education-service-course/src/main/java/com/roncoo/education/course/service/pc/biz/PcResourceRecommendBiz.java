package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.ResourceRecommendListRESP;
import com.roncoo.education.course.common.resp.ResourceRecommendViewRESP;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.ResourceRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 文库推荐
 *
 * @author wujing
 */
@Component
public class PcResourceRecommendBiz extends BaseBiz {

    @Autowired
    private ResourceRecommendDao dao;
    @Autowired
    private CourseDao courseDao;

    /**
     * 文库推荐列表
     *
     * @param req 文库推荐分页查询参数
     * @return 文库推荐分页查询结果
     */
    public Result<Page<ResourceRecommendListRESP>> list(ResourceRecommendListREQ req) {
        ResourceRecommendExample example = new ResourceRecommendExample();
        Criteria c = example.createCriteria();
        //模糊查询名称
        List<Course> courseList = null;
        List<Long> courseIdList = null;
        if(!StringUtils.isEmpty(req.getName())){
            courseList = courseDao.listByCourseName(req.getName());
        }
        if(!CollectionUtils.isEmpty(courseList)){
            courseIdList = new ArrayList<>();
            for(Course course:courseList){
                courseIdList.add(course.getId());
            }
        }
        if(!CollectionUtils.isEmpty(courseIdList)){
            c.andResourceIdIn(courseIdList);
        }

        if (ObjectUtil.isNotNull(req.getStatusId())) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        example.setOrderByClause(" id desc ");
        Page<ResourceRecommend> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<ResourceRecommendListRESP> respPage = PageUtil.transform(page, ResourceRecommendListRESP.class);
        for (ResourceRecommendListRESP resp : respPage.getList()) {
            Course course = courseDao.getById(resp.getResourceId());
            if (ObjectUtil.isNotNull(course)) {
                resp.setResourceName(course.getCourseName());
            }
        }
        return Result.success(respPage);
    }


    /**
     * 文库推荐添加
     *
     * @param resourceRecommendSaveREQ 文库推荐
     * @return 添加结果
     */
    public Result<String> save(ResourceRecommendSaveREQ resourceRecommendSaveREQ) {
        Course course = courseDao.getById(resourceRecommendSaveREQ.getResourceId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到文库");
        }
        ResourceRecommend resourceRecommend = dao.getByResourceId(resourceRecommendSaveREQ.getResourceId());
        if (ObjectUtil.isNotNull(resourceRecommend)) {
            return Result.error("文库推荐已添加");
        }
        ResourceRecommend record = BeanUtil.copyProperties(resourceRecommendSaveREQ, ResourceRecommend.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    public Result<String> saveBatch(ResourceRecommendSaveBatchREQ req) {
        if(StringUtils.isEmpty(req.getResourceIds())){
            return Result.error("ids不能为空");
        }
        String[] ids = req.getResourceIds().split(",");
        for(String idStr : ids){
            Long id = Long.valueOf(idStr);
            Course course = courseDao.getById(id);
            if (ObjectUtil.isNull(course)) {
               continue;
            }
            ResourceRecommend recommend = dao.getByResourceId(id);
            if(!ObjectUtils.isEmpty(recommend)){
                continue;
            }
            ResourceRecommend record = new ResourceRecommend();
            record.setResourceId(id);
            dao.save(record);
        }
        return Result.success("操作成功");
    }

    /**
     * 文库推荐查看
     *
     * @param id 主键ID
     * @return 文库推荐
     */
    public Result<ResourceRecommendViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), ResourceRecommendViewRESP.class));
    }


    /**
     * 文库推荐修改
     *
     * @param resourceRecommendEditREQ 文库推荐修改对象
     * @return 修改结果
     */
    public Result<String> edit(ResourceRecommendEditREQ resourceRecommendEditREQ) {
        ResourceRecommend record = BeanUtil.copyProperties(resourceRecommendEditREQ, ResourceRecommend.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 文库推荐修改
     *
     * @param resourceRecommendUpdateStatusREQ 文库推荐状态修改对象
     * @return 修改结果
     */
    public Result<String> updateStatus(ResourceRecommendUpdateStatusREQ resourceRecommendUpdateStatusREQ) {
        if (ObjectUtil.isNull(StatusIdEnum.byCode(resourceRecommendUpdateStatusREQ.getStatusId()))) {
            return Result.error("修改状态不正确");
        }
        ResourceRecommend viewRecord = dao.getById(resourceRecommendUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(viewRecord)) {
            return Result.error("文库推荐不存在");
        }
        ResourceRecommend record = BeanUtil.copyProperties(resourceRecommendUpdateStatusREQ, ResourceRecommend.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 文库推荐删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
