package com.roncoo.education.course.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseInfoPageBO implements Serializable {

	private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "讲师用户编号")
	private Long lecturerUserNo;
	/**
	 * 一级分类编号
	 */
	@ApiModelProperty(value = "一级分类编号")
	private Long categoryId1;
	/**
	 * 二级分类编号
	 */
	@ApiModelProperty(value = "二级分类编号")
	private Long categoryId2;
	/**
	 * 三级分类编号
	 */
	@ApiModelProperty(value = "三级分类编号")
	private Long categoryId3;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	/**
	 * 是否免费(1:免费，0:收费)
	 */
	@ApiModelProperty(value = "是否免费(1:免费，0:收费)")
	private Integer isFree;
	/**
	 * 会员是否免费(1:免费，0:收费)
	 */
	@ApiModelProperty(value = "会员是否免费(1:免费，0:收费)")
	private Integer isVipFree;
	/**
	 * 课程分类(1:普通课程;2:直播课程,3:试卷)
	 */
	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程，4:文库)", required = true)
	private Integer courseCategory;

	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private int pageCurrent = 1;
	/**
	 * 每页条数
	 */
	@ApiModelProperty(value = "每页条数")
	private int pageSize = 20;
}
