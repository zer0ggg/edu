package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.vod.model.v20170321.CreateUploadVideoRequest;
import com.aliyuncs.vod.model.v20170321.CreateUploadVideoResponse;
import com.aliyuncs.vod.model.v20170321.RefreshUploadVideoResponse;
import com.roncoo.education.common.aliyun.AliYunVodUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadUrlFile;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.AuthCourseVideoForListDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseVideoListDTO;
import com.roncoo.education.course.common.dto.auth.AuthWebUploadVideoGetUrlDTO;
import com.roncoo.education.course.common.dto.auth.AuthWebUploadVideoRefreshDTO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliYunVodVO;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigVodVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 课程视频信息
 *
 * @author wuyun
 */
@Component
public class AuthCourseVideoBiz extends BaseBiz {

    @Autowired
    private CourseVideoDao dao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseChapterAuditDao chapterAuditDao;
    @Autowired
    private CourseAuditDao courseAuditDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;

    /**
     * 章节视频库，课时视频库添加接口
     *
     * @param authCourseVideoSaveBO 视频保存参数
     */
    public Result<Integer> save(AuthCourseVideoSaveBO authCourseVideoSaveBO) {
        if (authCourseVideoSaveBO.getChapterId().equals(0L)) {
            return Result.error("章节ID不能为空");
        }
        // 根据章节编号查找章节信息
        CourseChapterAudit chapterInfoAudit = chapterAuditDao.getById(authCourseVideoSaveBO.getChapterId());
        if (ObjectUtil.isNull(chapterInfoAudit) || !StatusIdEnum.YES.getCode().equals(chapterInfoAudit.getStatusId())) {
            return Result.error("找不到章节信息");
        }
        CourseAudit courseAudit = courseAuditDao.getById(chapterInfoAudit.getCourseId());
        if (ObjectUtil.isNull(courseAudit) || !StatusIdEnum.YES.getCode().equals(courseAudit.getStatusId())) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(courseAudit.getLecturerUserNo())) {
            return Result.error("传入的useNo与该课程的讲师useNo不一致");
        }
        // 查找视频信息
        CourseVideo courseVideo = dao.getById(authCourseVideoSaveBO.getVideoId());
        if (ObjectUtil.isNull(courseVideo) || !StatusIdEnum.YES.getCode().equals(courseVideo.getStatusId())) {
            return Result.error("找不到视频信息");
        }
        courseVideo.setCourseId(chapterInfoAudit.getCourseId());
        courseVideo.setChapterId(chapterInfoAudit.getId());
        int result = dao.updateById(courseVideo);
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }

    /**
     * 章节视频库列出
     *
     * @param authCourseVideoBO 获取该章节的视频参数
     */
    public Result<AuthCourseVideoListDTO> listByChapterId(AuthCourseVideoBO authCourseVideoBO) {
        // 查找该章节下可用的视频信息集合
        List<CourseVideo> courseVideoList = dao.listByChapterIdAndStatusId(authCourseVideoBO.getChapterId(), StatusIdEnum.YES.getCode());
        AuthCourseVideoListDTO dto = new AuthCourseVideoListDTO();
        if (CollectionUtil.isNotEmpty(courseVideoList)) {
            List<AuthCourseVideoForListDTO> dtoList = ArrayListUtil.copy(courseVideoList, AuthCourseVideoForListDTO.class);
            dto.setList(dtoList);
        }
        return Result.success(dto);
    }

    /**
     * 视频删除
     *
     * @param authCourseVideoDeleteBO 视频删除参数
     * @author wuyun
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> updateById(AuthCourseVideoDeleteBO authCourseVideoDeleteBO) {
        if (authCourseVideoDeleteBO.getId() == null) {
            return Result.error("id不能为空");
        }
        CourseVideo courseVideo = dao.getById(authCourseVideoDeleteBO.getId());
        if (ObjectUtil.isNull(courseVideo)) {
            return Result.error("找不到该视频");
        }
        CourseAudit course = courseAuditDao.getById(courseVideo.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("传入的useNo与该课程的讲师useNo不一致");
        }
        ConfigPolyvVO sys = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sys)) {
            return Result.error("未找到系统配置信息");
        }
        if (StringUtils.isEmpty(sys.getPolyvUseid()) || StringUtils.isEmpty(sys.getPolyvSecretkey())) {
            return Result.error("useid或secretkey未配置");
        }

        List<CourseChapterPeriodAudit> periodAuditList = courseChapterPeriodAuditDao.listByVideoIdAndStatusId(authCourseVideoDeleteBO.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(periodAuditList)) {
            return Result.error("该视频已被使用，请先删除");
        }

        List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByVideoIdAndStatusId(authCourseVideoDeleteBO.getId(), StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(periodList)) {
            return Result.error("该视频已被使用，请先删除");
        }
        // 更改为冻结状态
        courseVideo.setStatusId(Constants.FREEZE);
        int result = dao.updateById(courseVideo);
        if (result > 0 && VodPlatformEnum.POLYV.getCode().equals(courseVideo.getVodPlatform())) {
            // 删除保利威视的视频
            PolyvUtil.deleteFile(courseVideo.getVideoVid(), sys.getPolyvUseid(), sys.getPolyvSecretkey());
            return Result.success(result);
        }
        return Result.error(ResultEnum.COURSE_DELETE_FAIL);
    }

    public Result<String> ref(AuthWebUploadVideoRefBO bo) {
        CourseChapterPeriodAudit periodInfoAudit = courseChapterPeriodAuditDao.getById(bo.getPeriodId());
        if (ObjectUtil.isNull(periodInfoAudit)) {
            return Result.error("获取不到课时信息");
        }

        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (StringUtils.isEmpty(configPolyvVO.getPolyvSecretkey()) || StringUtils.isEmpty(configPolyvVO.getPolyvWritetoken())) {
            return Result.error("未配置保利威信息!");
        }
        //获取水印logo
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.LOGO);

        Long videoId = IdWorker.getId();
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setId(videoId);
        courseVideo.setVodPlatform(VodPlatformEnum.POLYV.getCode());
        courseVideo.setCourseId(periodInfoAudit.getCourseId());
        courseVideo.setChapterId(periodInfoAudit.getChapterId());
        courseVideo.setVideoStatus(VideoStatusEnum.UPLOADING.getCode());
        courseVideo.setVideoName(bo.getVideoName());
        //前端直接上传保利威
        if (!StringUtils.isEmpty(bo.getVid())) {
            //章节视频库
            courseVideo.setVideoVid(bo.getVid());
            dao.save(courseVideo);
            return Result.success("上传成功");
        }
        //上传保利威
        UploadUrlFile uploadUrlFile = new UploadUrlFile();
        uploadUrlFile.setUrl(bo.getOssUrl());
        uploadUrlFile.setTitle(bo.getVideoName());
        uploadUrlFile.setDesc(bo.getVideoName());
        //异步处理
        uploadUrlFile.setAsync("true");
        //自定义数据为视频id，上传完成回调时会返回处理
        uploadUrlFile.setState(videoId.toString());
        //视频类型，用于区分是试卷还是普通课程
        uploadUrlFile.setTag(VideoTagEnum.COURSE.getCode());
        if (!StringUtils.isEmpty(sysConfigVO)) {
            uploadUrlFile.setWatermark(sysConfigVO.getConfigValue());
        }
        if (PolyvUtil.uploadUrlFile(uploadUrlFile, configPolyvVO.getPolyvWritetoken(), configPolyvVO.getPolyvSecretkey())) {
            //章节视频库
            courseVideo.setVideoBackupPlatform(VodPlatformEnum.ALI_YUN_VOD.getCode());
            courseVideo.setVideoOssId(bo.getOssUrl());
            dao.save(courseVideo);
            return Result.success("上传成功");
        } else {
            //如果上传保利威失败，则删除阿里云的视频
            Aliyun aliyun = BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class);
            AliyunUtil.delete(bo.getOssUrl(), aliyun);
            return Result.error("上传失败!");
        }
    }

    /**
     * 获取视频上传路径
     *
     * @param bo 获取参数
     * @return 获取结果
     */
    public Result<AuthWebUploadVideoGetUrlDTO> getUploadUrl(AuthWebUploadVideoGetUrlBO bo) {
        // 获取配置
        BeanValidateUtil.ValidationResult validationResult = BeanValidateUtil.validate(bo);
        if (!validationResult.isPass()) {
            return Result.error(validationResult.getErrMsgString());
        }
        ConfigVodVO sysVodVo = feignSysConfig.getVod();
        if (ObjectUtil.isNull(sysVodVo) || ObjectUtil.isNull(sysVodVo.getVodPlatform()) || ObjectUtil.isNull(sysVodVo.getVodVideoEncrypt())) {
            return Result.error("未设置平台视频配置");
        }
        ConfigAliYunVodVO sysVo = feignSysConfig.getAliYunVod();
        if (ObjectUtil.isNull(sysVo)) {
            return Result.error("获取视频配置失败");
        }
        if (StrUtil.isBlank(sysVo.getAliYunVodAccessKeyId()) || StrUtil.isBlank(sysVo.getAliYunVodAccessKeySecret())) {
            return Result.error("视频上传没有配置信息，请联系管理员");
        }

        // 校验课程
        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(bo.getPeriodId());
        if (ObjectUtil.isNull(periodAudit) || !StatusIdEnum.YES.getCode().equals(periodAudit.getStatusId())) {
            return Result.error("当前课时不存在或状态不可用");
        }
        CourseChapterAudit chapterAudit = chapterAuditDao.getById(periodAudit.getChapterId());
        if (ObjectUtil.isNull(chapterAudit) || !StatusIdEnum.YES.getCode().equals(chapterAudit.getStatusId())) {
            return Result.error("当前章节不存在或状态不可用");
        }
        CourseAudit courseAudit = courseAuditDao.getById(periodAudit.getCourseId());
        if (ObjectUtil.isNull(courseAudit) || !StatusIdEnum.YES.getCode().equals(courseAudit.getStatusId())) {
            return Result.error("当前课程不存在或状态不可用");
        }

        // 获取上传凭证和地址
        CreateUploadVideoRequest uploadVideoRequest = new CreateUploadVideoRequest();
        uploadVideoRequest.setTitle(bo.getVideoName().substring(0, bo.getVideoName().indexOf(".")));
        uploadVideoRequest.setFileName(bo.getVideoName());

        JSONObject messageCallback = new JSONObject();
        messageCallback.put("CallbackType", "http");
        messageCallback.put("CallbackURL", sysVo.getAliYunVodCallbackUrl());
        JSONObject userData = new JSONObject();
        userData.put("MessageCallback", messageCallback);
        uploadVideoRequest.setUserData(userData.toJSONString());
        CreateUploadVideoResponse uploadVideoResponse = AliYunVodUtil.createUploadVideo(uploadVideoRequest, sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
        if (ObjectUtil.isNull(uploadVideoResponse)) {
            return Result.error("获取上传地址失败");
        }

        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setId(IdWorker.getId());
        courseVideo.setCourseId(periodAudit.getCourseId());
        courseVideo.setChapterId(periodAudit.getChapterId());
        courseVideo.setVideoName(uploadVideoRequest.getTitle());
        courseVideo.setVideoStatus(VideoStatusEnum.UPLOADING.getCode());
        courseVideo.setVodPlatform(sysVodVo.getVodPlatform());
        courseVideo.setVideoBackupPlatform(VodPlatformEnum.ALI_YUN_VOD.getCode());
        courseVideo.setVideoBackupVid(uploadVideoResponse.getVideoId());
        dao.save(courseVideo);

        AuthWebUploadVideoGetUrlDTO dto = new AuthWebUploadVideoGetUrlDTO();
        dto.setVideoVid(uploadVideoResponse.getVideoId());
        dto.setUploadAuth(uploadVideoResponse.getUploadAuth());
        dto.setUploadAddress(uploadVideoResponse.getUploadAddress());
        return Result.success(dto);
    }

    /**
     * 刷新上传凭证
     *
     * @param bo 上传凭证
     * @return 刷新结果
     */
    public Result<AuthWebUploadVideoRefreshDTO> uploadRefresh(AuthWebUploadVideoRefreshBO bo) {
        BeanValidateUtil.ValidationResult validate = BeanValidateUtil.validate(bo);
        if (!validate.isPass()) {
            return Result.error(validate.getErrMsgString());
        }

        CourseVideo courseVideo = dao.getByVideoBackupPlatformAndVideoBackupVid(VodPlatformEnum.ALI_YUN_VOD.getCode(), bo.getVideoVid());
        if (ObjectUtil.isNull(courseVideo)) {
            return Result.error("视频信息不存在");
        }
        ConfigAliYunVodVO sysVo = feignSysConfig.getAliYunVod();
        if (ObjectUtil.isNull(sysVo)) {
            return Result.error("获取视频配置失败");
        }
        if (StrUtil.isBlank(sysVo.getAliYunVodAccessKeyId()) || StrUtil.isBlank(sysVo.getAliYunVodAccessKeySecret())) {
            return Result.error("视频上传没有配置信息，请联系管理员");
        }

        RefreshUploadVideoResponse response = AliYunVodUtil.refreshUploadVideo(bo.getVideoVid(), sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
        if (ObjectUtil.isNull(response)) {
            return Result.error("刷新视频上传凭证失败");
        }

        AuthWebUploadVideoRefreshDTO dto = new AuthWebUploadVideoRefreshDTO();
        dto.setVideoVid(response.getVideoId());
        dto.setUploadAuth(response.getUploadAuth());
        dto.setUploadAddress(response.getUploadAddress());
        return Result.success(dto);
    }
}
