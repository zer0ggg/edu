package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程用户学习日志-汇总
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseUserStudyLogStatisticalListRESP", description="课程用户学习日志列表-汇总")
public class CourseUserStudyLogStatisticalListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数组名")
    private String name;

    @ApiModelProperty(value = "学习时长")
    private String value;

    @ApiModelProperty(value = "课程总时长")
    private String courseLength;

    @ApiModelProperty(value = "学习进度")
    private Integer studyProcess = 0;

}
