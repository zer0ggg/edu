package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneSaveREQ", description="专区添加")
public class ZoneSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "位置(1电脑端，2微信端)不能为空")
    @ApiModelProperty(value = "位置(1电脑端，2微信端)",required = true)
    private Integer zoneLocation;

    @NotEmpty(message = "名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String zoneName;

    @NotNull(message = "排序不能为空")
    @ApiModelProperty(value = "排序",required = true)
    private Integer sort;

    @ApiModelProperty(value = "描述")
    private String zoneDesc;


}
