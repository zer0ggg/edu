package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryDownloadBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessoryListBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseAccessorySaveBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseAccessoryListDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseAccessoryBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 附件信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/course/accessory")
public class AuthCourseAccessoryController {

	@Autowired
	private AuthCourseAccessoryBiz biz;

	/**
	 * 附件列出接口
	 *
	 * @param authAccessoryListBO
	 * @author kyh
	 */
	@ApiOperation(value = "附件列出接口", notes = "附件列出接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<AuthCourseAccessoryListDTO> list(@RequestBody AuthCourseAccessoryListBO authAccessoryListBO) {
		return biz.list(authAccessoryListBO);
	}

	/**
	 * 附件下载接口
	 *
	 * @param authAccessoryDownloadBO
	 * @return
	 */
	@ApiOperation(value = "附件下载接口", notes = "附件下载接口")
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public Result<String> download(@RequestBody AuthCourseAccessoryDownloadBO authAccessoryDownloadBO) {
		return biz.download(authAccessoryDownloadBO);
	}

	/**
	 * 附件信息保存接口
	 *
	 * @param authAccessorySaveBO
	 * @author kyh
	 */
	@ApiOperation(value = "附件信息保存接口", notes = "附件信息保存接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<Integer> save(@RequestBody AuthCourseAccessorySaveBO authAccessorySaveBO) {
		return biz.save(authAccessorySaveBO);
	}

	/**
	 * 附件信息删除接口
	 *
	 * @param authAccessoryDeleteBO
	 * @author kyh
	 */
	@ApiOperation(value = "附件信息删除接口", notes = "附件信息删除接口")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthCourseAccessoryDeleteBO authAccessoryDeleteBO) {
		return biz.delete(authAccessoryDeleteBO);
	}

}
