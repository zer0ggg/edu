package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserPageBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserSaveBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseCommentUserPageDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseCommentUserSaveDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseCommentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 课程评论 UserApi接口
 *
 * @author Quanf
 * @date 2020-09-03
 */
@Api(tags = "API-AUTH-课程评论")
@RestController
@RequestMapping("/course/auth/courseComment")
public class AuthCourseCommentController {

    @Autowired
    private AuthCourseCommentBiz biz;

    @ApiOperation(value = "评论列表展示接口", notes = "查看我的评论或者回复的评论(courseUserNo和commentUserNo传一个即可，传commentUserNo是查看我的评论，传courseUserNo是回复的评论)")
    @PostMapping(value = "/list")
    public Result<Page<AuthCourseCommentUserPageDTO>> list(@RequestBody @Valid AuthCourseCommentUserPageBO authCourseCommentUserPageBO) {
        return biz.list(authCourseCommentUserPageBO);
    }

    @ApiOperation(value = "添加课程评论接口", notes = "对课程进行评论")
    @PostMapping(value = "/save")
    public Result<AuthCourseCommentUserSaveDTO> save(@RequestBody @Valid AuthCourseCommentUserSaveBO authCourseCommentUserSaveBO) {
        return biz.save(authCourseCommentUserSaveBO);
    }

    @ApiOperation(value = "删除课程评论接口", notes = "对自己评论进行删除(状态删除)")
    @PostMapping(value = "/delete")
    public Result<Integer> delete(@RequestBody @Valid AuthCourseCommentUserDeleteBO authCourseCommentUserDeleteBO) {
        return biz.delete(authCourseCommentUserDeleteBO);
    }
}
