package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.CourseChapterPeriodQO;
import com.roncoo.education.course.feign.vo.CourseChapterPeriodVO;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapter;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@Component
public class FeignCourseChapterPeriodBiz {

    @Autowired
    private CourseChapterPeriodDao dao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseDao courseDao;

    public Page<CourseChapterPeriodVO> listForPage(CourseChapterPeriodQO qo) {
        CourseChapterPeriodExample example = new CourseChapterPeriodExample();
        example.setOrderByClause(" id desc ");
        Page<CourseChapterPeriod> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
        return PageUtil.transform(page, CourseChapterPeriodVO.class);
    }

    public int save(CourseChapterPeriodQO qo) {
        CourseChapterPeriod record = BeanUtil.copyProperties(qo, CourseChapterPeriod.class);
        return dao.save(record);
    }

    public int deleteById(Long id) {
        return dao.deleteById(id);
    }

    public CourseChapterPeriodVO getById(Long id) {
        CourseChapterPeriod record = dao.getById(id);
        return BeanUtil.copyProperties(record, CourseChapterPeriodVO.class);
    }

    public int updateById(CourseChapterPeriodQO qo) {
        CourseChapterPeriod record = BeanUtil.copyProperties(qo, CourseChapterPeriod.class);
        return dao.updateById(record);
    }

    /**
     * 根据课时id获取课时、章节、课程信息
     *
     * @return
     */
    public CourseChapterPeriodVO getByCourseChapterPeriodId(CourseChapterPeriodQO qo) {
        CourseChapterPeriod record = dao.getById(qo.getId());
        if (ObjectUtil.isNull(record)) {
            return null;
        }
        CourseChapterPeriodVO vo = BeanUtil.copyProperties(record, CourseChapterPeriodVO.class);

        vo.setPeriodName(record.getPeriodName());
        CourseChapter courseChapter = courseChapterDao.getById(record.getChapterId());
        if (ObjectUtil.isNull(courseChapter)) {
            return vo;
        }
        vo.setChapterName(courseChapter.getChapterName());
        Course course = courseDao.getById(record.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return vo;
        }
        vo.setCourseName(course.getCourseName());
        return vo;
    }

    public List<CourseChapterPeriodVO> ListByCourseId(CourseChapterPeriodQO qo) {
        List<CourseChapterPeriod> list = dao.listByCourseId(qo.getCourseId());
        return ArrayListUtil.copy(list, CourseChapterPeriodVO.class);
    }

    public List<CourseChapterPeriodVO> listByIds(CourseChapterPeriodQO qo) {
        List<CourseChapterPeriod> list = dao.listByIds(qo.getIds());
        return ArrayListUtil.copy(list, CourseChapterPeriodVO.class);
    }
}