package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseChapterPeriodEditREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodListREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterPeriodListRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseChapterPeriodBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课时信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/chapter/period")
@Api(value = "course-课时信息", tags = {"course-课时信息"})
public class PcCourseChapterPeriodController {

    @Autowired
    private PcCourseChapterPeriodBiz biz;

    @ApiOperation(value = "课时信息列表", notes = "课时信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseChapterPeriodListRESP>> list(@RequestBody CourseChapterPeriodListREQ courseChapterPeriodListREQ) {
        return biz.list(courseChapterPeriodListREQ);
    }

    @ApiOperation(value = "课时信息添加", notes = "课时信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseChapterPeriodSaveREQ courseChapterPeriodSaveREQ) {
        return biz.save(courseChapterPeriodSaveREQ);
    }

    @ApiOperation(value = "课时信息查看", notes = "课时信息查看")
    @GetMapping(value = "/view")
    public Result<CourseChapterPeriodViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课时信息修改", notes = "课时信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseChapterPeriodEditREQ courseChapterPeriodEditREQ) {
        return biz.edit(courseChapterPeriodEditREQ);
    }

    @ApiOperation(value = "课时信息删除", notes = "课时信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
