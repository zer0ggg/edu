package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.BaseException;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.ResourceRecommendQO;
import com.roncoo.education.course.feign.vo.ResourceRecommendVO;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.ResourceRecommendDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 文库推荐
 *
 * @author wujing
 */
@Component
public class FeignResourceRecommendBiz extends BaseBiz {

    @Autowired
    private ResourceRecommendDao dao;
	@Autowired
	private CourseDao courseDao;


	public Page<ResourceRecommendVO> listForPage(ResourceRecommendQO qo) {
		ResourceRecommendExample example = new ResourceRecommendExample();
		Criteria c = example.createCriteria();
		example.setOrderByClause(" id desc ");
		Page<ResourceRecommend> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<ResourceRecommendVO> listForPage =  PageUtil.transform(page, ResourceRecommendVO.class);
		for (ResourceRecommendVO vo : listForPage.getList()) {
			Course course = courseDao.getById(vo.getResourceId());
			if (ObjectUtil.isNotNull(course)) {
				vo.setResourceName(course.getCourseName());
			}
		}
		return listForPage;
	}

	public int save(ResourceRecommendQO qo) {
		Course course = courseDao.getById(qo.getResourceId());
		if (ObjectUtil.isNull(course)) {
			throw new BaseException("找不到文库");
		}
		ResourceRecommend resourceRecommend = dao.getByResourceId(qo.getResourceId());
		if (ObjectUtil.isNotNull(resourceRecommend)) {
			throw new BaseException("文库推荐已添加");
		}
		ResourceRecommend record = BeanUtil.copyProperties(qo, ResourceRecommend.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public ResourceRecommendVO getById(Long id) {
		ResourceRecommend record = dao.getById(id);
		return BeanUtil.copyProperties(record, ResourceRecommendVO.class);
	}

	public int updateById(ResourceRecommendQO qo) {
		ResourceRecommend record = BeanUtil.copyProperties(qo, ResourceRecommend.class);
		return dao.updateById(record);
	}

}
