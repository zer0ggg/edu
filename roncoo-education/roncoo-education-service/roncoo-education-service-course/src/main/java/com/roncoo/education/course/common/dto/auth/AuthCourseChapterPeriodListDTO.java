package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课时集合信息
	 */
	@ApiModelProperty(value = "课时集合信息")
	private List<AuthCourseChapterPeriodDTO> userPeriodList = new ArrayList<>();
}
