package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.vod.upload.resp.UploadImageResponse;
import com.aliyuncs.kms.model.v20160120.GenerateDataKeyRequest;
import com.aliyuncs.kms.model.v20160120.GenerateDataKeyResponse;
import com.aliyuncs.vod.model.v20170321.GetTranscodeSummaryResponse;
import com.aliyuncs.vod.model.v20170321.SubmitTranscodeJobsRequest;
import com.aliyuncs.vod.model.v20170321.SubmitTranscodeJobsResponse;
import com.roncoo.education.common.aliyun.AliYunVodUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.enums.RedisPreEnum;
import com.roncoo.education.common.core.enums.VideoStatusEnum;
import com.roncoo.education.common.core.enums.VideoTagEnum;
import com.roncoo.education.common.core.enums.VodPlatformEnum;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadUrlFile;
import com.roncoo.education.course.feign.qo.AliYunVideoAnalysisCompleteQO;
import com.roncoo.education.course.feign.qo.AliYunVideoUploadComplateQO;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliYunVodVO;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigVodVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * 阿里云回调通知处理
 *
 * @author LYQ
 */
@Slf4j
@Component
public class FeignCallbackAliYunBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private CourseVideoDao courseVideoDao;

    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Boolean uploadComplete(AliYunVideoUploadComplateQO qo) {
        // 加锁
        String redisKey = RedisPreEnum.ALI_YUN_UPLOAD_COMPLETE_HANDLE_LOCK.getCode() + qo.getVideoId();
        boolean lock = myRedisTemplate.lock(redisKey);
        if (!lock) {
            log.error("阿里云上传处理，当前视频[VID:{}]处理中，不予处理", qo.getVideoId());
            return false;
        }

        try {
            CourseVideo courseVideo = courseVideoDao.getByVideoBackupPlatformAndVideoBackupVid(VodPlatformEnum.ALI_YUN_VOD.getCode(), qo.getVideoId());
            if (ObjectUtil.isNull(courseVideo)) {
                log.info("视频备份平台：{}--视频备份ID：{},找不到对应视频", VodPlatformEnum.ALI_YUN_VOD.getDesc(), qo.getVideoId());
                return false;
            }
            if (courseVideo.getCallbackHandleComplete()) {
                log.info("视频[VID:{}]已经处理完成，不能重复处理", qo.getVideoId());
                return true;
            }

            if (VodPlatformEnum.ALI_YUN_VOD.getCode().equals(courseVideo.getVodPlatform())) {
                // 对视频进行转码、加密等处理
                return aliYunUploadComplete(qo, courseVideo);
            }

            if (VodPlatformEnum.POLYV.getCode().equals(courseVideo.getVodPlatform())) {
                // 需要上传保利威
                return polyvVideoRemotePull(qo, courseVideo);
            }

            log.error("视频平台【编码：{}】没有实现相关的处理逻辑", courseVideo.getVodPlatform());
            return false;
        } finally {
            // 解锁redis
            myRedisTemplate.unlock(redisKey);
        }
    }

    /**
     * 解析完成
     *
     * @param qo 解析完成返回
     * @return 解析结果
     */
    public Boolean videoAnalysisComplete(AliYunVideoAnalysisCompleteQO qo) {
        CourseVideo courseVideo = courseVideoDao.getByVodPlatformAndVideoVid(VodPlatformEnum.ALI_YUN_VOD.getCode(), qo.getVideoId());
        if (ObjectUtil.isNull(courseVideo)) {
            return true;
        }

        if ("success".equals(qo.getStatus())) {
            String videoLength = DateUtil.format(DateUtil.offsetSecond(DateUtil.beginOfDay(new Date()), qo.getDuration().intValue()), DatePattern.NORM_TIME_PATTERN);
            CourseVideo updateCourseVideo = new CourseVideo();
            updateCourseVideo.setId(courseVideo.getId());
            updateCourseVideo.setVideoLength(videoLength);
            return courseVideoDao.updateById(updateCourseVideo) > 0;
        }
        return true;
    }

    /**
     * 阿里云上传完成处理
     *
     * @param qo          回调参数
     * @param courseVideo 课程视频信息
     * @return 处理结果
     */
    private boolean aliYunUploadComplete(AliYunVideoUploadComplateQO qo, CourseVideo courseVideo) {
        //获取水印logo
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.LOGO);

        ConfigVodVO sysVodVo = feignSysConfig.getVod();
        if (ObjectUtil.isNull(sysVodVo)) {
            log.error("系统点播配置获取失败");
            return false;
        }
        ConfigAliYunVodVO sysVo = feignSysConfig.getAliYunVod();
        if (ObjectUtil.isNull(sysVo)) {
            log.error("阿里云点播配置获取失败");
            return false;
        }

        // 查询是否已发起转码
        GetTranscodeSummaryResponse summaryResponse = AliYunVodUtil.getTranscodeSummary(qo.getVideoId(), sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
        if (ObjectUtil.isNull(summaryResponse)) {
            log.error("阿里云点播--查询视频[VID:{}]是否转码失败", qo.getVideoId());
            return false;
        }

        if (CollectionUtil.isNotEmpty(summaryResponse.getNonExistVideoIds())) {
            // 发起转码请求
            boolean isEncrypt = sysVodVo.getVodVideoEncrypt().equals(1);

            SubmitTranscodeJobsRequest jobsRequest = new SubmitTranscodeJobsRequest();
            jobsRequest.setTemplateGroupId(isEncrypt ? sysVo.getAliYunVodTranscodeEncryptTemplateGroupId() : sysVo.getAliYunVodTranscodeTemplateGroupId());
            jobsRequest.setVideoId(qo.getVideoId());

            // 覆盖参数
            UploadImageResponse uploadImageResponse = AliYunVodUtil.uploadWatermarkImage(sysConfigVO.getConfigValue(), sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
            JSONObject overrideParams = new JSONObject();
            JSONArray watermarks = new JSONArray();
            JSONObject watermark = new JSONObject();
            watermark.put("WatermarkId", sysVo.getAliYunVodWatermarkId());
            watermark.put("FileUrl", uploadImageResponse.getImageURL());
            watermarks.add(watermark);
            overrideParams.put("Watermarks", watermarks);
            jobsRequest.setOverrideParams(overrideParams.toJSONString());

            // 加密设置
            if (isEncrypt) {
                GenerateDataKeyRequest generateDataKeyRequest = new GenerateDataKeyRequest();
                generateDataKeyRequest.setKeyId(sysVo.getAliYunVodDataServiceKey());
                generateDataKeyRequest.setKeySpec("AES_128");
                GenerateDataKeyResponse generateDataKeyResponse = AliYunVodUtil.generateDataKey(generateDataKeyRequest, sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
                if (ObjectUtil.isNull(generateDataKeyResponse)) {
                    log.error("阿里云点播--视频[VID:{}]加密创建数据秘钥失败", qo.getVideoId());
                    return false;
                }
                // 加密配置，为JSON字符串，只有使用HLS标准加密时才需要此参数
                JSONObject encryptConfig = new JSONObject();
                encryptConfig.put("CipherText", generateDataKeyResponse.getCiphertextBlob());
                encryptConfig.put("DecryptKeyUri", sysVo.getAliYunVodPlayDecryptUrl() + "?Ciphertext=" + generateDataKeyResponse.getCiphertextBlob());
                encryptConfig.put("KeyServiceType", "KMS");
                jobsRequest.setEncryptConfig(encryptConfig.toJSONString());
            }

            SubmitTranscodeJobsResponse jobsResponse = AliYunVodUtil.submitTranscodeJobs(jobsRequest, sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
            if (ObjectUtil.isNull(jobsResponse)) {
                log.error("阿里云点播--视频[VID:{}]发起转码任务失败", qo.getVideoId());
                return false;
            }
            log.info("阿里云点播--视频[VID:{}]发起转码成功！\n转码返回信息：{}", qo.getVideoId(), JSON.toJSONString(jobsResponse));
        }

        // 更新视频信息
        CourseVideo updateCourseVideo = new CourseVideo();
        updateCourseVideo.setId(courseVideo.getId());
        updateCourseVideo.setVideoVid(qo.getVideoId());
        updateCourseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
        updateCourseVideo.setCallbackHandleComplete(true);
        int result = courseVideoDao.updateById(updateCourseVideo);
        return result > 0;
    }

    /**
     * 保利威远程拉取视频
     *
     * @param qo          回调参数
     * @param courseVideo 课程视频信息
     * @return 处理结果
     */
    private boolean polyvVideoRemotePull(AliYunVideoUploadComplateQO qo, CourseVideo courseVideo) {
        //获取水印logo
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.LOGO);

        // 获取保利威配置
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (StringUtils.isEmpty(configPolyvVO.getPolyvSecretkey()) || StringUtils.isEmpty(configPolyvVO.getPolyvWritetoken())) {
            return false;
        }

        UploadUrlFile uploadUrlFile = new UploadUrlFile();
        uploadUrlFile.setUrl(qo.getFileUrl());
        uploadUrlFile.setTitle(courseVideo.getVideoName());
        uploadUrlFile.setDesc(courseVideo.getVideoName());
        //异步处理
        uploadUrlFile.setAsync("true");
        //自定义数据为视频id，上传完成回调时会返回处理
        uploadUrlFile.setState(String.valueOf(courseVideo.getId()));
        //视频类型，用于区分是试卷还是普通课程
        uploadUrlFile.setTag(VideoTagEnum.COURSE.getCode());
        if (ObjectUtil.isNotNull(sysConfigVO)) {
            uploadUrlFile.setWatermark(sysConfigVO.getConfigValue());
        }
        log.info("保利威远程拉取视频文件参数：{}", JSON.toJSONString(uploadUrlFile));

        // 远程拉取
        boolean result = PolyvUtil.uploadUrlFile(uploadUrlFile, configPolyvVO.getPolyvWritetoken(), configPolyvVO.getPolyvSecretkey());
        if (result) {
            CourseVideo updateCourseVideo = new CourseVideo();
            updateCourseVideo.setId(courseVideo.getId());
            updateCourseVideo.setCallbackHandleComplete(true);
            int updateResult = courseVideoDao.updateById(updateCourseVideo);
            return updateResult > 0;
        }
        return false;
    }
}
