package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.druid.util.StringUtils;
import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.LiveStatusEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.common.polyv.live.request.PLChannelConvertRequest;
import com.roncoo.education.course.feign.qo.CrontabPlanPolyvQO;
import com.roncoo.education.course.feign.vo.CrontabPlanPolyvVO;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.CrontabPlanPolyvDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 直播转点存定时任务
 *
 * @author wujing
 */
@Component
public class FeignCrontabPlanPolyvBiz extends BaseController {

	@Autowired
	private CrontabPlanPolyvDao dao;
	@Autowired
	private CourseLiveLogDao courseLiveLogDao;

	@Autowired
	private IFeignSysConfig feignSysConfig;

	public Page<CrontabPlanPolyvVO> listForPage(CrontabPlanPolyvQO qo) {
		CrontabPlanPolyvExample example = new CrontabPlanPolyvExample();
		example.setOrderByClause(" id desc ");
		Page<CrontabPlanPolyv> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, CrontabPlanPolyvVO.class);
	}

	public int save(CrontabPlanPolyvQO qo) {
		CrontabPlanPolyv record = BeanUtil.copyProperties(qo, CrontabPlanPolyv.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CrontabPlanPolyvVO getById(Long id) {
		CrontabPlanPolyv record = dao.getById(id);
		return BeanUtil.copyProperties(record, CrontabPlanPolyvVO.class);
	}

	public int updateById(CrontabPlanPolyvQO qo) {
		CrontabPlanPolyv record = BeanUtil.copyProperties(qo, CrontabPlanPolyv.class);
		return dao.updateById(record);
	}

	public void handleScheduledTasks() {
		ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
		if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
			logger.error("保利威配置错误，参数={}", configPolyvVO);
		}
		CrontabPlanPolyvExample example = new CrontabPlanPolyvExample();
		example.setOrderByClause(" id asc ");
		List<CrontabPlanPolyv> list = dao.listForTimeAsc(example);
		if (CollectionUtil.isNotEmpty(list)) {
			CrontabPlanPolyv vo = list.get(0);
			// 转存成点播
			PLChannelConvertRequest request = new PLChannelConvertRequest();
			request.setChannelId(vo.getChannelId());
			request.setFileUrl(vo.getPlayback());
			request.setFileName(vo.getPeriodName());// 转存后为直播课时的名称
			// request.setCataid(SystemUtil.POLYV_CATEGORY_ID);// 转存的目录id
			request.setToPlayList("Y");// 存放到回放列表
			String result = PLChannelUtil.convert(request, configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvAppSecret());
			if ("success".equals(result)) {
				// 转存成功后删除定时任务信息
				dao.deleteById(vo.getId());
			}
			// 如果转存失败，且失败次数小于5，则统计失败次数
			if (StringUtils.isEmpty(result) && vo.getFailCount() < 5) {
				vo.setFailCount(vo.getFailCount() + 1);
				dao.updateById(vo);
			}
			// 如果失败次数错误5次，则直接处理为成功
			if (StringUtils.isEmpty(result) && vo.getFailCount() >= 5) {
				// 根据频道号和状态查找记录中当前状态的最后一条
				CourseLiveLog courseLiveLog = courseLiveLogDao.getByChannelIdAndLiveStatus(vo.getChannelId(), LiveStatusEnum.WAITSAVE.getCode());
				if (ObjectUtil.isNull(courseLiveLog)) {
					logger.error("获取不到当前频道的直播记录");
				}
				// 更改状态为结束
				courseLiveLog.setLiveStatus(LiveStatusEnum.END.getCode());
				courseLiveLogDao.updateById(courseLiveLog);
				// 删除当前定时任务
				dao.deleteById(vo.getId());
			}
		}
	}

}
