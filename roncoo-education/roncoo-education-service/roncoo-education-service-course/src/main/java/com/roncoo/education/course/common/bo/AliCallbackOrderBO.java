package com.roncoo.education.course.common.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 支付宝扫码付回调参数
 *
 * @author liaoh
 */
@Data
public class AliCallbackOrderBO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date notifyTime;

    private String notifyType;

    private String notifyId;

    private String appId;

    private String charset;

    private String version;

    private String signType;

    private String sign;

    private String tradeNo;

    private String outTradeNo;
}
