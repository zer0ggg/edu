package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 直播记录保存
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthCourseLiveLogPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    @ApiModelProperty(value = "课程ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long courseId;
    /**
     * 课程名称
     */
    @ApiModelProperty(value = "课程名称")
    private String courseName;
    /**
     * 章节名称
     */
    @ApiModelProperty(value = "章节名称")
    private String chapterName;

    @ApiModelProperty(value = "课时ID")
    private Long periodId;

    /**
     * 课时名称
     */
    @ApiModelProperty(value = "课时名称")
    private String periodName;
    /**
     * 频道号
     */
    @ApiModelProperty(value = "频道号")
    private String channelId;
    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 直播状态(1:未开播,2:正在直播,3:待生成回放,4:待转存,5:结束)
     */
    @ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:待生成回放,4:待转存,5:结束)")
    private Integer liveStatus;
    /**
     * 直播场景(alone:活动拍摄;ppt:三分屏课；largeInteractive：大班互动；smallClass：小班)
     */
    @ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏课；largeInteractive：大班互动；smallClass：小班)")
    private String scene;
    /**
     * 频道密码
     */
    @ApiModelProperty(value = "频道密码")
    private String channelPasswd;

    @ApiModelProperty(value = "直播地址")
    private String liveUrl;

    @ApiModelProperty(value = "直播平台")
    private Integer livePlatform;
}
