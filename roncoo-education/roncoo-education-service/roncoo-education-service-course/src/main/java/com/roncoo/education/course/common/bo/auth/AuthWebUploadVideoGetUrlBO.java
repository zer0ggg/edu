package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 网页端上传视频获取上传路径
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthWebUploadVideoGetUrlBO", description = "网页端上传视频获取上传路径")
public class AuthWebUploadVideoGetUrlBO implements Serializable {

    private static final long serialVersionUID = -8537280403855516113L;

    @NotNull(message = "课时不能为空")
    @ApiModelProperty(value = "课时id")
    private Long periodId;

    @NotBlank(message = "视频名称不能为空")
    @ApiModelProperty(value = "视频名称（必须带扩展名，且扩展名不区分大小写）")
    private String videoName;
}
