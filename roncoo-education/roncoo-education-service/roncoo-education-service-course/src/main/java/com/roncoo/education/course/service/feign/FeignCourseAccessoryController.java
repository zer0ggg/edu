package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignCourseAccessory;
import com.roncoo.education.course.feign.qo.CourseAccessoryQO;
import com.roncoo.education.course.feign.vo.CourseAccessoryVO;
import com.roncoo.education.course.service.feign.biz.FeignCourseAccessoryBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 附件信息
 *
 * @author wujing
 */
@RestController
public class FeignCourseAccessoryController extends BaseController implements IFeignCourseAccessory {

	@Autowired
	private FeignCourseAccessoryBiz biz;

	@Override
	public Page<CourseAccessoryVO> listForPage(@RequestBody CourseAccessoryQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseAccessoryQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@RequestBody Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseAccessoryQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public CourseAccessoryVO getById(@RequestBody Long id) {
		return biz.getById(id);
	}

	@Override
	public CourseAccessoryVO countDownload() {
		return biz.countDownload();
	}

	@Override
	public List<CourseAccessoryVO> sumByAll(@RequestBody CourseAccessoryQO qo) {
		return biz.sumByAll(qo);
	}

}
