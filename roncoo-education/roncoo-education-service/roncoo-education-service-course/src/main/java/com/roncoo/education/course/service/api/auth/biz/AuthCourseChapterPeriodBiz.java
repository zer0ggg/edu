package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterPeriodListBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterPeriodDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterPeriodListDTO;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapter;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@Component
public class AuthCourseChapterPeriodBiz extends BaseBiz {

    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;

    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    /**
     * 课时列出接口
     *
     * @param bo
     * @return
     * @author wuyun
     */
    public Result<AuthCourseChapterPeriodListDTO> listByChapterId(AuthCourseChapterPeriodListBO bo) {
        if (bo.getChapterId() == null) {
            return Result.error("章节ID不能为空");
        }
        CourseChapter courseChapter = courseChapterDao.getById(bo.getChapterId());
        if (ObjectUtil.isNull(courseChapter) || !StatusIdEnum.YES.getCode().equals(courseChapter.getStatusId())) {
            return Result.error("找不到章节信息");
        }
        Course course = courseDao.getById(courseChapter.getCourseId());
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        // 根据章节ID查询课时信息表
        List<CourseChapterPeriod> periodAuditList = courseChapterPeriodDao.listByChapterIdAndStatusId(bo.getChapterId(), StatusIdEnum.YES.getCode());
        AuthCourseChapterPeriodListDTO dto = new AuthCourseChapterPeriodListDTO();
        if (CollectionUtil.isNotEmpty(periodAuditList)) {
            List<AuthCourseChapterPeriodDTO> periodAuditDTOList = ArrayListUtil.copy(periodAuditList, AuthCourseChapterPeriodDTO.class);
            for (AuthCourseChapterPeriodDTO authCourseChapterPeriodDTO : periodAuditDTOList) {
                // 查找课程关联考试信息
                CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
                courseExamRefQO.setRefId(authCourseChapterPeriodDTO.getId());
                courseExamRefQO.setRefType(RefTypeEnum.CHAPTER.getCode());
                CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
                if (ObjectUtil.isNotNull(courseExamRefVO)) {
                    authCourseChapterPeriodDTO.setExamId(courseExamRefVO.getExamId());
                    authCourseChapterPeriodDTO.setCourseExamRefId(courseExamRefVO.getId());
                    authCourseChapterPeriodDTO.setExamName(courseExamRefVO.getExamName());
                }
            }
            dto.setUserPeriodList(periodAuditDTOList);
        }
        return Result.success(dto);
    }
}
