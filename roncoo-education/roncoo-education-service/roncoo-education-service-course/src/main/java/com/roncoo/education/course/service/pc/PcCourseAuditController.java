package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseAuditListRESP;
import com.roncoo.education.course.common.resp.CourseAuditViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程信息-审核 Pc接口
 *
 * @author LYQ
 */
@RestController
@RequestMapping("/course/pc/course/audit")
@Api(value = "course-课程信息-审核", tags = {"course-课程信息-审核"})
public class PcCourseAuditController {

    @Autowired
    private PcCourseAuditBiz biz;

    @ApiOperation(value = "课程信息-审核列表", notes = "课程信息-审核列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseAuditListRESP>> list(@RequestBody CourseAuditListREQ courseAuditListREQ) {
        return biz.list(courseAuditListREQ);
    }


    @ApiOperation(value = "课程信息-审核添加", notes = "课程信息-审核添加")
    @SysLog(value = "课程信息-审核添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseAuditSaveREQ courseAuditSaveREQ) {
        return biz.save(courseAuditSaveREQ);
    }

    @ApiOperation(value = "课程信息-审核查看", notes = "课程信息-审核查看")
    @GetMapping(value = "/view")
    public Result<CourseAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "课程信息-审核修改", notes = "课程信息-审核修改")
    @SysLog(value = "课程信息-审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseAuditEditREQ courseAuditEditREQ) {
        return biz.edit(courseAuditEditREQ);
    }


    @ApiOperation(value = "课程信息-审核修改状态", notes = "课程信息-审核修改状态")
    @SysLog(value = "课程信息-审核修改状态")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody CourseAuditUpdateStatusREQ courseAuditUpdateStatusREQ) {
        return biz.updateStatus(courseAuditUpdateStatusREQ);
    }


    @ApiOperation(value = "课程信息-审核修改上下架", notes = "课程信息-审核修改上下架")
    @SysLog(value = "课程信息-审核修改上下架")
    @PutMapping(value = "/update/putaway")
    public Result<String> updateIsPutaway(@RequestBody CourseAuditUpdateIsPutawayREQ courseAuditUpdateIsPutawayREQ) {
        return biz.updateIsPutaway(courseAuditUpdateIsPutawayREQ);
    }


    @ApiOperation(value = "课程信息-审核删除", notes = "课程信息-审核删除")
    @SysLog(value = "课程信息-审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }


    @ApiOperation(value = "课程信息-审核", notes = "课程信息-审核")
    @SysLog(value = "课程信息-审核")
    @PutMapping(value = "/audit")
    public Result<String> audit(@RequestBody CourseAuditREQ courseAuditREQ) {
        return biz.audit(courseAuditREQ);
    }
}
