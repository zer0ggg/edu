package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseUserStudyLogListREQ;
import com.roncoo.education.course.common.req.CourseUserStudyLogPeriodListREQ;
import com.roncoo.education.course.common.req.CourseUserStudyLogStatisticalREQ;
import com.roncoo.education.course.common.resp.CourseUserStudyLogListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyLogPeriodListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyLogStatisticalRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseUserStudyLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程用户学习日志 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/user/study/log")
@Api(value = "course-课程用户学习日志", tags = {"course-课程用户学习日志"})
public class PcCourseUserStudyLogController {

    @Autowired
    private PcCourseUserStudyLogBiz biz;
    
    @ApiOperation(value = "课程用户学习日志", notes = "课程用户学习日志")
    @PostMapping(value = "/list")
    public Result<Page<CourseUserStudyLogListRESP>> list(@RequestBody CourseUserStudyLogListREQ userOrderCourseRefStudyLogListREQ) {
        return biz.list(userOrderCourseRefStudyLogListREQ);
    }

    @ApiOperation(value = "课程用户学习-汇总", notes = "课程用户学习-汇总")
    @PostMapping(value = "/statistical")
    public Result<CourseUserStudyLogStatisticalRESP> statistical(@RequestBody CourseUserStudyLogStatisticalREQ courseUserStudyLogStatisticalREQ) {
        return biz.statistical(courseUserStudyLogStatisticalREQ);
    }


}
