package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseAccessoryEditREQ;
import com.roncoo.education.course.common.req.CourseAccessoryListREQ;
import com.roncoo.education.course.common.req.CourseAccessorySaveREQ;
import com.roncoo.education.course.common.resp.CourseAccessoryListRESP;
import com.roncoo.education.course.common.resp.CourseAccessoryViewRESP;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.CourseAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 附件信息
 *
 * @author wujing
 */
@Component
public class PcCourseAccessoryBiz extends BaseBiz {

    @Autowired
    private CourseAccessoryDao dao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseChapterAuditDao courseChapterAuditDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;

    /**
    * 附件信息列表
    *
    * @param courseAccessoryListREQ 附件信息分页查询参数
    * @return 附件信息分页查询结果
    */
    public Result<Page<CourseAccessoryListRESP>> list(CourseAccessoryListREQ courseAccessoryListREQ) {
        CourseAccessoryExample example = new CourseAccessoryExample();
        Criteria c = example.createCriteria();
        Page<CourseAccessory> page = dao.listForPage(courseAccessoryListREQ.getPageCurrent(), courseAccessoryListREQ.getPageSize(), example);
        Page<CourseAccessoryListRESP> respPage = PageUtil.transform(page, CourseAccessoryListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 附件信息添加
    *
    * @param req 附件信息
    * @return 添加结果
    */
    public Result<String> save(CourseAccessorySaveREQ req) {
        if (StringUtils.isEmpty(req.getAcName())) {
            return Result.error("附件名称不能为空");
        }
        if (StringUtils.isEmpty(req.getAcUrl())) {
            return Result.error("附件地址不能为空");
        }
        if (req.getRefId() == null) {
            return Result.error("关联ID不能为空");
        }
        if (req.getCourseCategory() == null) {
            return Result.error("课程分类不能为空");
        }
        if (req.getRefType() == null) {
            return Result.error("关联类型不能为空");
        }
        if (RefTypeEnum.COURSE.getCode().equals(req.getRefType())) {
            CourseAudit courseAudit = courseAuditDao.getById(req.getRefId());
            if (ObjectUtil.isNull(courseAudit) || !StatusIdEnum.YES.getCode().equals(courseAudit.getStatusId())) {
                return Result.error("找不到课程审核信息");
            }
        } else if (RefTypeEnum.CHAPTER.getCode().equals(req.getRefType())) {
            CourseChapterAudit courseChapterAudit = courseChapterAuditDao.getById(req.getRefId());
            if (ObjectUtil.isNull(courseChapterAudit) || !StatusIdEnum.YES.getCode().equals(courseChapterAudit.getStatusId())) {
                return Result.error("找不到章节审核信息");
            }
        } else {
            CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(req.getRefId());
            if (ObjectUtil.isNull(courseChapterPeriodAudit) || !StatusIdEnum.YES.getCode().equals(courseChapterPeriodAudit.getStatusId())) {
                return Result.error("找不到课时审核信息");
            }
        }
        CourseAccessory accessory = BeanUtil.copyProperties(req, CourseAccessory.class);
        int results = dao.save(accessory);
        if (results > 0) {
            return Result.success("保存成功");
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }


    /**
    * 附件信息查看
    *
    * @param id 主键ID
    * @return 附件信息
    */
    public Result<CourseAccessoryViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseAccessoryViewRESP.class));
    }


    /**
    * 附件信息修改
    *
    * @param courseAccessoryEditREQ 附件信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseAccessoryEditREQ courseAccessoryEditREQ) {
        CourseAccessory record = BeanUtil.copyProperties(courseAccessoryEditREQ, CourseAccessory.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 附件信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
