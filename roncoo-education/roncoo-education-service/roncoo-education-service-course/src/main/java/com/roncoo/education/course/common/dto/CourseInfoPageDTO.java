package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课程信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseInfoPageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "讲师用户编号")
	private Long lecturerUserNo;

	@ApiModelProperty(value = "讲师名称")
	private String lecturerName;

	@ApiModelProperty(value = "讲师头像")
	private String headImgUrl;

	@ApiModelProperty(value = "课程ID")
	private Long id;

	@ApiModelProperty(value = "课程名称")
	private String courseName;

	@ApiModelProperty(value = "课程封面")
	private String courseLogo;

	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;

	@ApiModelProperty(value = "原价")
	private BigDecimal courseOriginal;

	@ApiModelProperty(value = "优惠价")
	private BigDecimal courseDiscount;

	@ApiModelProperty(value = "购买人数")
	private Integer countBuy;

	@ApiModelProperty(value = "学习人数")
	private Integer countStudy;

	@ApiModelProperty(value = "总课时数")
	private Integer periodTotal;

	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4:文库)")
	private Integer courseCategory;

	@ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:回放)")
	private Integer liveStatus;

	@ApiModelProperty(value = "直播开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@ApiModelProperty(value = "直播结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

}
