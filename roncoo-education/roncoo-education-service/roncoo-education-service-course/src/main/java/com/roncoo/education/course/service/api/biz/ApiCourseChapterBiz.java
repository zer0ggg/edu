package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.course.common.bo.CourseChapterPageBO;
import com.roncoo.education.course.common.dto.CourseAccessoryDTO;
import com.roncoo.education.course.common.dto.CourseChapterPageDTO;
import com.roncoo.education.course.common.dto.CourseChapterPeriodListDTO;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterExample.Criteria;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApiCourseChapterBiz {

	@Autowired
	private CourseDao courseDao;
	@Autowired
	private CourseChapterDao dao;
	@Autowired
	private CourseChapterPeriodDao courseChapterPeriodDao;
	@Autowired
	private CourseAccessoryDao courseAccessoryDao;

	@Autowired
	private IFeignCourseExamRef feignCourseExamRef;

	public Result<Page<CourseChapterPageDTO>> listForPage(CourseChapterPageBO bo) {
		if (bo.getCourseId() == null) {
			return Result.error("课程ID不能为空");
		}
		Course course = courseDao.getById(bo.getCourseId());
		if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
			return Result.error("该课程不可用");
		}
		CourseChapterExample example = new CourseChapterExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(bo.getCourseId());
		c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
		example.setOrderByClause("sort asc,id asc");
		Page<CourseChapter> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		Page<CourseChapterPageDTO> listForPage = PageUtil.transform(page, CourseChapterPageDTO.class);
		for (CourseChapterPageDTO dto : listForPage.getList()) {
			// 查找课程关联考试信息
			CourseExamRefQO courseExamRefQO =  new CourseExamRefQO();
			courseExamRefQO.setRefId(dto.getId());
			courseExamRefQO.setRefType(RefTypeEnum.CHAPTER.getCode());
			CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
			if (ObjectUtil.isNotNull(courseExamRefVO)) {
				dto.setExamId(courseExamRefVO.getExamId());
				dto.setCourseExamRefId(courseExamRefVO.getId());
				dto.setExamName(courseExamRefVO.getExamName());
			}
			// 获取课时信息
			List<CourseChapterPeriod> list = courseChapterPeriodDao.listByChapterIdAndStatusId(dto.getId(), StatusIdEnum.YES.getCode());
			if (CollectionUtil.isNotEmpty(list)) {
				List<CourseChapterPeriodListDTO> periodList = ArrayListUtil.copy(list, CourseChapterPeriodListDTO.class);
				// 迭代获取课时试卷题目图片、试卷答案图片、附件附件
				period(periodList);
				// 返回课时集合
				dto.setPeriodList(periodList);
			}
		}
		return Result.success(listForPage);
	}

	private void period(List<CourseChapterPeriodListDTO> periodList) {
		for (CourseChapterPeriodListDTO periodDto : periodList) {
			// 查找课程关联考试信息
			CourseExamRefQO courseExamRefQO =  new CourseExamRefQO();
			courseExamRefQO.setRefId(periodDto.getId());
			courseExamRefQO.setRefType(RefTypeEnum.PERIOD.getCode());
			CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefTypeAudit(courseExamRefQO);
			if (ObjectUtil.isNotNull(courseExamRefVO)) {
				periodDto.setExamId(courseExamRefVO.getExamId());
				periodDto.setCourseExamRefId(courseExamRefVO.getId());
				periodDto.setExamName(courseExamRefVO.getExamName());
			}
			// 课时附件信息
			List<CourseAccessory> periodAccessoryList = courseAccessoryDao.listByRefIdAndRefTypeAndStatusId(periodDto.getId(), RefTypeEnum.PERIOD.getCode(), StatusIdEnum.YES.getCode());
			if (CollectionUtil.isNotEmpty(periodAccessoryList)) {
				periodDto.setAccessoryList(ArrayListUtil.copy(periodAccessoryList, CourseAccessoryDTO.class));
			}
		}
	}

}
