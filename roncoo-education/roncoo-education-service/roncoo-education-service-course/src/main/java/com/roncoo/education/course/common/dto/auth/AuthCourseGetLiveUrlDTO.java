package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 获取直播地址
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthCourseGetLiveUrlDTO", description = "获取直播地址")
public class AuthCourseGetLiveUrlDTO implements Serializable {

    private static final long serialVersionUID = -3526596268523585308L;

    @ApiModelProperty(value = "直播平台(1：保利威、2：欢拓)")
    private Integer livePlatform;

    @ApiModelProperty(value = "直播场景(alone：活动直播, ppt：三分屏， largeInteractive：大班互动)")
    private String liveScene;

    @ApiModelProperty(value = "直播参数")
    private String liveParam;

    @ApiModelProperty(value = "直播地址")
    private String liveUrl;

}
