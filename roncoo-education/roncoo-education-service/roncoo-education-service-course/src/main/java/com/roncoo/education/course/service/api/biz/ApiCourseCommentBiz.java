package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.course.common.bo.CourseCommentPageBO;
import com.roncoo.education.course.common.dto.CourseCommentPageDTO;
import com.roncoo.education.course.service.dao.CourseCommentDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample.Criteria;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程评论
 *
 * @author Quanf
 */
@Component
public class ApiCourseCommentBiz extends BaseBiz {

    @Autowired
    private CourseCommentDao dao;

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 课程评论
     */
    public Result<Page<CourseCommentPageDTO>> list(CourseCommentPageBO bo) {
        CourseCommentExample example = new CourseCommentExample();
        Criteria c = example.createCriteria();
        c.andParentIdEqualTo(0L);
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andCourseIdEqualTo(bo.getCourseId());
        example.setOrderByClause(" status_id desc, sort asc, id desc ");
        Page<CourseComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<CourseCommentPageDTO> dto = PageUtil.transform(page, CourseCommentPageDTO.class);
        // 用户默认头像
        SysConfigVO SysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        if (StringUtils.isEmpty(SysVO.getConfigValue())) {
            throw new BaseException("未设置平台域名");
        }
        String headImgUrl = SysVO.getConfigValue() + "/friend.jpg";
        for (CourseCommentPageDTO courseComment : dto.getList()) {
            CourseComment comment = dao.getById(courseComment.getId());
            if (ObjectUtil.isNotNull(comment)) {
                courseComment.setContent(comment.getContent());
                // 默认头像
                courseComment.setUserImg(headImgUrl);
                courseComment.setCourseUserImg(headImgUrl);
            }
            // 被评论者用户
            UserExtVO courseUser = feignUserExt.getByUserNo(courseComment.getCourseUserNo());
            if (ObjectUtil.isNotNull(courseUser)) {
                if (StringUtils.isEmpty(courseComment.getCourseNickname())) {
                    if (StringUtils.isEmpty(courseUser.getNickname())) {
                        courseComment.setCourseNickname(courseUser.getMobile().substring(0, 3) + "****" + courseUser.getMobile().substring(7));
                    } else {
                        courseComment.setCourseNickname(courseUser.getNickname());
                    }
                }
                if (StringUtils.hasText(courseUser.getHeadImgUrl())) {
                    courseComment.setCourseUserImg(courseUser.getHeadImgUrl());
                }
            }
            // 评论者用户
            UserExtVO user = feignUserExt.getByUserNo(courseComment.getUserNo());
            if (ObjectUtil.isNotNull(user)) {
                if (StringUtils.isEmpty(courseComment.getNickname())) {
                    if (StringUtils.isEmpty(user.getNickname())) {
                        courseComment.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
                    } else {
                        courseComment.setNickname(user.getNickname());
                    }
                }
                if (StringUtils.hasText(user.getHeadImgUrl())) {
                    courseComment.setUserImg(user.getHeadImgUrl());
                }
            }
            courseComment.setCourseCommentList(recursionList(courseComment.getId()));
        }
        return Result.success(dto);
    }


    /**
     * 展示第二级
     */
    private List<CourseCommentPageDTO> recursionList(Long parentId) {
        List<CourseCommentPageDTO> list = new ArrayList<>();
        List<CourseComment> courseCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        if (CollectionUtils.isNotEmpty(courseCommentList)) {
            // 用户默认头像
            SysConfigVO SysVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
            if (StringUtils.isEmpty(SysVO.getConfigValue())) {
                throw new BaseException("未设置平台域名");
            }
            String headImgUrl = SysVO.getConfigValue() + "/friend.jpg";
            for (CourseComment courseComment : courseCommentList) {
                CourseCommentPageDTO vo = BeanUtil.copyProperties(courseComment, CourseCommentPageDTO.class);
                CourseComment comment = dao.getById(vo.getId());
                if (ObjectUtil.isNotNull(comment)) {
                    vo.setContent(comment.getContent());
                    // 默认头像
                    vo.setUserImg(headImgUrl);
                    vo.setCourseUserImg(headImgUrl);
                }
                // 被评论者用户
                UserExtVO courseUser = feignUserExt.getByUserNo(vo.getCourseUserNo());

                if (ObjectUtil.isNotNull(courseUser)) {
                    if (StringUtils.isEmpty(courseComment.getCourseNickname())) {
                        if (StringUtils.isEmpty(courseUser.getNickname())) {
                            vo.setCourseNickname(courseUser.getMobile().substring(0, 3) + "****" + courseUser.getMobile().substring(7));
                        } else {
                            vo.setCourseNickname(courseUser.getNickname());
                        }
                    }
                    if (StringUtils.hasText(courseUser.getHeadImgUrl())) {
                        vo.setCourseUserImg(courseUser.getHeadImgUrl());
                    }
                }
                // 评论者用户
                UserExtVO user = feignUserExt.getByUserNo(vo.getUserNo());
                if (ObjectUtil.isNotNull(user)) {
                    if (StringUtils.isEmpty(courseComment.getNickname())) {
                        if (StringUtils.isEmpty(user.getNickname())) {
                            vo.setNickname(user.getMobile().substring(0, 3) + "****" + user.getMobile().substring(7));
                        } else {
                            vo.setNickname(user.getNickname());
                        }
                    }
                    if (StringUtils.hasText(user.getHeadImgUrl())) {
                        vo.setUserImg(user.getHeadImgUrl());
                    }
                }
                list.add(vo);
            }
        }
        return list;
    }
}
