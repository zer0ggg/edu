package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseIntroduceAuditEditREQ;
import com.roncoo.education.course.common.req.CourseIntroduceAuditListREQ;
import com.roncoo.education.course.common.req.CourseIntroduceAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseIntroduceAuditListRESP;
import com.roncoo.education.course.common.resp.CourseIntroduceAuditViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseIntroduceAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程介绍信息-审核 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/introduce/audit")
@Api(value = "course-课程介绍信息-审核", tags = {"course-课程介绍信息-审核"})
public class PcCourseIntroduceAuditController {

    @Autowired
    private PcCourseIntroduceAuditBiz biz;

    @ApiOperation(value = "课程介绍信息-审核列表", notes = "课程介绍信息-审核列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseIntroduceAuditListRESP>> list(@RequestBody CourseIntroduceAuditListREQ courseIntroduceAuditListREQ) {
        return biz.list(courseIntroduceAuditListREQ);
    }

    @ApiOperation(value = "课程介绍信息-审核添加", notes = "课程介绍信息-审核添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseIntroduceAuditSaveREQ courseIntroduceAuditSaveREQ) {
        return biz.save(courseIntroduceAuditSaveREQ);
    }

    @ApiOperation(value = "课程介绍信息-审核查看", notes = "课程介绍信息-审核查看")
    @GetMapping(value = "/view")
    public Result<CourseIntroduceAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程介绍信息-审核修改", notes = "课程介绍信息-审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseIntroduceAuditEditREQ courseIntroduceAuditEditREQ) {
        return biz.edit(courseIntroduceAuditEditREQ);
    }

    @ApiOperation(value = "课程介绍信息-审核删除", notes = "课程介绍信息-审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
