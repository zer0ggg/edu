package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterListBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterPageBO;
import com.roncoo.education.course.common.dto.auth.*;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 课时信息
 *
 * @author wujing
 */
@Component
public class AuthCourseChapterBiz extends BaseBiz {

    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterDao dao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseAccessoryDao courseAccessoryDao;
    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    @Autowired
    private IFeignCourseLog feignCourseLog;

    /**
     * 章节列出接口
     *
     * @return 章节信息
     * @author kyh
     */
    public Result<AuthCourseChapterListDTO> listByCourseId(AuthCourseChapterListBO bo) {
        if (bo.getCourseId() == null) {
            return Result.error("课程ID不能为空");
        }
        Course course = courseDao.getById(bo.getCourseId());
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            return Result.error("找不到课程信息");
        }
        if (!ThreadContext.userNo().equals(course.getLecturerUserNo())) {
            return Result.error("找不到课程信息");
        }
        // 根据课程ID查询章节信息表
        List<CourseChapter> chapterList = dao.listByCourseIdAndStatusId(bo.getCourseId(), StatusIdEnum.YES.getCode());
        AuthCourseChapterListDTO dto = new AuthCourseChapterListDTO();
        if (CollectionUtil.isNotEmpty(chapterList)) {
            List<AuthCourseChapterDTO> chapterDtoList = ArrayListUtil.copy(chapterList, AuthCourseChapterDTO.class);
            dto.setChapterList(chapterDtoList);
        }
        return Result.success(dto);
    }

    public Result<Page<AuthCourseChapterPageDTO>> listForPage(AuthCourseChapterPageBO bo) {
        if (bo.getCourseId() == null) {
            return Result.error("课程ID不能为空");
        }
        Course course = courseDao.getById(bo.getCourseId());
        if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
            return Result.error("找不到该课程信息");
        }
        if (!course.getLecturerUserNo().equals(ThreadContext.userNo())) {
            return Result.error("找不到该课程信息");
        }
        CourseChapterExample example = new CourseChapterExample();
        CourseChapterExample.Criteria c = example.createCriteria();
        c.andCourseIdEqualTo(bo.getCourseId());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause("sort asc,id asc");
        Page<CourseChapter> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        Page<AuthCourseChapterPageDTO> listForPage = PageUtil.transform(page, AuthCourseChapterPageDTO.class);
        for (AuthCourseChapterPageDTO dto : listForPage.getList()) {
            // 查找章节关联考试信息
            CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
            courseExamRefQO.setRefId(course.getId());
            courseExamRefQO.setRefType(RefTypeEnum.CHAPTER.getCode());
            CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefType(courseExamRefQO);
            if (ObjectUtil.isNotNull(courseExamRefVO)) {
                dto.setExamId(courseExamRefVO.getExamId());
            }
            // 获取课时信息
            List<CourseChapterPeriod> list = courseChapterPeriodDao.listByChapterIdAndStatusId(dto.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(list)) {
                List<AuthCourseChapterPeriodPageDTO> periodList = ArrayListUtil.copy(list, AuthCourseChapterPeriodPageDTO.class);
                // 迭代获取课时试卷题目图片、试卷答案图片、附件附件
                period(ThreadContext.userNo(), periodList);
                // 返回课时集合
                dto.setPeriodList(periodList);
            }
        }
        return Result.success(listForPage);
    }

    private void period(Long userNo, List<AuthCourseChapterPeriodPageDTO> periodList) {
        for (AuthCourseChapterPeriodPageDTO periodDto : periodList) {
            // 查找章节关联考试信息
            CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
            courseExamRefQO.setRefId(periodDto.getId());
            courseExamRefQO.setRefType(RefTypeEnum.CHAPTER.getCode());
            CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefType(courseExamRefQO);
            if (ObjectUtil.isNotNull(courseExamRefVO)) {
                periodDto.setExamId(courseExamRefVO.getExamId());
            }

            // 课时附件信息
            List<CourseAccessory> periodAccessoryList = courseAccessoryDao.listByRefIdAndRefTypeAndStatusId(periodDto.getId(), RefTypeEnum.PERIOD.getCode(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(periodAccessoryList)) {
                periodDto.setAccessoryList(ArrayListUtil.copy(periodAccessoryList, AuthCourseAccessoryDTO.class));
            }

            //人脸对比剩余次数
            CourseLogQO qo = new CourseLogQO();
            qo.setUserNo(userNo);
            qo.setPeriodId(periodDto.getId());
            CourseLogVO courseLogVO = feignCourseLog.getByUserNoAndPeriodIdLatest(qo);
            if (ObjectUtil.isNotEmpty(courseLogVO)) {
                periodDto.setResidueContrastTotal(courseLogVO.getResidueContrastTotal());
            } else {
                periodDto.setResidueContrastTotal(3);
            }
        }
    }

}
