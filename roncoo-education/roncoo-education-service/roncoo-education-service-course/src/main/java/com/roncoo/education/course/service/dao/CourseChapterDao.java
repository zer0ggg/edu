package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapter;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterExample;

import java.util.List;

public interface CourseChapterDao {
    int save(CourseChapter record);

    int deleteById(Long id);

    int updateById(CourseChapter record);

    CourseChapter getById(Long id);

    Page<CourseChapter> listForPage(int pageCurrent, int pageSize, CourseChapterExample example);

    /**
     * 根据章节编号、状态ID查找章节信息
     *
     * @param courseId
     * @param statusId
     */
    List<CourseChapter> listByCourseIdAndStatusId(Long courseId, Integer statusId);

    /**
     * 根据章节编号、状态ID、升序查找课程第一章节
     *
     * @param courseId
     * @param statusId
     * @author kyh
     */
    List<CourseChapter> listByCourseIdAndStatusIdAndSortAsc(Long courseId, Integer statusId);

    /**
     * 根据章节编号、状态ID、倒序序查找课程最后一章节
     *
     * @param courseId
     * @param statusId
     * @author kyh
     */
    List<CourseChapter> listByCourseIdAndStatusIdAndSortDesc(Long courseId, Integer statusId);

    /**
     * 根据id集合获取课程信息
     *
     * @param ids
     * @return
     */
    List<CourseChapter> listByIds(List<Long> ids);
}
