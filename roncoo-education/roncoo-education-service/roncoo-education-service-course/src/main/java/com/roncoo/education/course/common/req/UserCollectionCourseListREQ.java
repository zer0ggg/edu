package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户收藏课程
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserCollectionCourseListREQ", description="用户收藏课程列表")
public class UserCollectionCourseListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "收藏类型:1课程，2章节，3课时")
    private Integer collectionType;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
