package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * web端上传阿里云关联
 */
@Data
@Accessors(chain = true)
public class AuthWebUploadVideoRefBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ossurl", required = false)
    private String ossUrl;

    @ApiModelProperty(value = "保利威视频vid", required = false)
    private String vid;

    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;

    @ApiModelProperty(value = "视频名称", required = true)
    private String videoName;
}
