package com.roncoo.education.course.service.feign.biz;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.UserCollectionCourseQO;
import com.roncoo.education.course.feign.vo.UserCollectionCourseVO;
import com.roncoo.education.course.service.dao.UserCollectionCourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourse;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@Component
public class FeignUserCollectionCourseBiz {

	@Autowired
	private UserCollectionCourseDao dao;

	public Page<UserCollectionCourseVO> listForPage(UserCollectionCourseQO qo) {
		UserCollectionCourseExample example = new UserCollectionCourseExample();
		Criteria c = example.createCriteria();
		if (StringUtils.isNotEmpty(qo.getMobile())) {
			c.andMobileLike(PageUtil.like(qo.getMobile()));
		}
		if (qo.getCollectionType() != null) {
			c.andCollectionTypeEqualTo(qo.getCollectionType());
		}

		example.setOrderByClause(" id desc ");
		Page<UserCollectionCourse> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		return PageUtil.transform(page, UserCollectionCourseVO.class);
	}

	public int save(UserCollectionCourseQO qo) {
		UserCollectionCourse record = BeanUtil.copyProperties(qo, UserCollectionCourse.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public UserCollectionCourseVO getById(Long id) {
		UserCollectionCourse record = dao.getById(id);
		return BeanUtil.copyProperties(record, UserCollectionCourseVO.class);
	}

	public int updateById(UserCollectionCourseQO qo) {
		UserCollectionCourse record = BeanUtil.copyProperties(qo, UserCollectionCourse.class);
		return dao.updateById(record);
	}

}
