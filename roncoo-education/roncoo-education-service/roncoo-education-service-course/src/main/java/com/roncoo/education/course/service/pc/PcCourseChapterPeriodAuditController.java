package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditEditREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditListREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditListRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditSaveRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseChapterPeriodAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课时信息-审核 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/chapter/period/audit")
@Api(value = "course-课时信息-审核", tags = {"course-课时信息-审核"})
public class PcCourseChapterPeriodAuditController {

    @Autowired
    private PcCourseChapterPeriodAuditBiz biz;

    @ApiOperation(value = "课时信息-审核列表", notes = "课时信息-审核列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseChapterPeriodAuditListRESP>> list(@RequestBody CourseChapterPeriodAuditListREQ courseChapterPeriodAuditListREQ) {
        return biz.list(courseChapterPeriodAuditListREQ);
    }

    @ApiOperation(value = "课时信息-审核添加", notes = "课时信息-审核添加")
    @PostMapping(value = "/save")
    public Result<CourseChapterPeriodAuditSaveRESP> save(@RequestBody CourseChapterPeriodAuditSaveREQ courseChapterPeriodAuditSaveREQ) {
        return biz.save(courseChapterPeriodAuditSaveREQ);
    }

    @ApiOperation(value = "课时信息-审核查看", notes = "课时信息-审核查看")
    @GetMapping(value = "/view")
    public Result<CourseChapterPeriodAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课时信息-审核修改", notes = "课时信息-审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseChapterPeriodAuditEditREQ courseChapterPeriodAuditEditREQ) {
        return biz.edit(courseChapterPeriodAuditEditREQ);
    }

    @ApiOperation(value = "课时信息-审核删除", notes = "课时信息-审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
