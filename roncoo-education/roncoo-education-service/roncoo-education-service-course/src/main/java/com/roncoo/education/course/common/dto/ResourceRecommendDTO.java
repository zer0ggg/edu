package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 文库推荐
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ResourceRecommendDTO", description="文库推荐")
public class ResourceRecommendDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文库id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceId;

    @ApiModelProperty(value = "文库名称")
    private String resourceName;

    @ApiModelProperty(value = "Logo")
    private String resourceLogo;
}
