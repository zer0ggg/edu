package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.course.common.dto.auth.AuthCourseAuditSaveDTO;
import com.roncoo.education.course.common.es.EsCourse;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseAuditListRESP;
import com.roncoo.education.course.common.resp.CourseAuditViewRESP;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAuditExample.Criteria;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignMsg;
import com.roncoo.education.user.feign.qo.LecturerQO;
import com.roncoo.education.user.feign.qo.MsgQO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Component
public class PcCourseAuditBiz extends BaseBiz {

    @Autowired
    private CourseAuditDao dao;
    @Autowired
    private CourseIntroduceAuditDao courseIntroduceAuditDao;
    @Autowired
    private CourseChapterAuditDao courseChapterAuditDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;
    @Autowired
    private CourseAccessoryDao courseAccessoryDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignMsg feignMsg;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignCourseLog feignCourseLog;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 课程信息-审核列表
     *
     * @param courseAuditListREQ 课程信息-审核分页查询参数
     * @return 课程信息-审核分页查询结果
     */
    public Result<Page<CourseAuditListRESP>> list(CourseAuditListREQ courseAuditListREQ) {
        CourseAuditExample example = new CourseAuditExample();
        Criteria c = example.createCriteria();

        if (!StringUtils.isEmpty(courseAuditListREQ.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(courseAuditListREQ.getCourseName()));
        }
        if (ObjectUtil.isNotNull(courseAuditListREQ.getStatusId())) {
            c.andStatusIdEqualTo(courseAuditListREQ.getStatusId());
        }

        if (ObjectUtil.isNotNull(courseAuditListREQ.getCourseCategory())) {
            c.andCourseCategoryEqualTo(courseAuditListREQ.getCourseCategory());
        }

        if (ObjectUtil.isNotNull(courseAuditListREQ.getAuditStatus())) {
            c.andAuditStatusEqualTo(courseAuditListREQ.getAuditStatus());
        }
        if (ObjectUtil.isNotNull(courseAuditListREQ.getIsFree())) {
            c.andIsFreeEqualTo(courseAuditListREQ.getIsFree());
        }
        if (courseAuditListREQ.getIsPutaway() != null) {
            c.andIsPutawayEqualTo(courseAuditListREQ.getIsPutaway());
        }

        example.setOrderByClause(" sort asc, id desc ");
        Page<CourseAudit> page = dao.listForPage(courseAuditListREQ.getPageCurrent(), courseAuditListREQ.getPageSize(), example);
        Page<CourseAuditListRESP> respPage = PageUtil.transform(page, CourseAuditListRESP.class);
        if (CollectionUtil.isEmpty(respPage.getList())) {
            return Result.success(respPage);
        }
        // 获取所有讲师
        List<Long> lecturerUserNos = respPage.getList().stream().map(CourseAuditListRESP::getLecturerUserNo).collect(Collectors.toList());
        Map<Long, String> lecturerNameMap = new HashMap<>();
        LecturerQO lecturerQO = new LecturerQO();
        lecturerQO.setLecturerUserNos(lecturerUserNos);
        List<LecturerVO> lecturerVOList = feignLecturer.listByLecturerUserNos(lecturerQO);
        if (CollectionUtil.isNotEmpty(lecturerVOList)) {
            lecturerNameMap = new HashMap<>(lecturerVOList.stream().collect(Collectors.toMap(LecturerVO::getLecturerUserNo, LecturerVO::getLecturerName)));
        }

        Map<Long, CourseCategory> cacheMap = new HashMap<>();
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        for (CourseAuditListRESP resp : respPage.getList()) {

            // 课程类型为文库时返回章节ID
            if (CourseCategoryEnum.RESOURCES.getCode().equals(resp.getCourseCategory())) {
                List<CourseChapterAudit> chapterAuditList = courseChapterAuditDao.listByCourseId(resp.getId());
                if (ObjectUtil.isNotNull(chapterAuditList) && StatusIdEnum.YES.getCode().equals(chapterAuditList.get(0).getStatusId())) {
                    resp.setChapterId(chapterAuditList.get(0).getId());
                }
            }
            resp.setDomain(sysConfigVO.getConfigValue());
            // 设置讲师名称
            resp.setLecturerName(lecturerNameMap.get(resp.getLecturerUserNo()));
            if (resp.getCategoryId1() != null && resp.getCategoryId1() != 0L) {
                CourseCategory courseCategory = getCourseCategoryById(cacheMap, resp.getCategoryId1());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName1(courseCategory.getCategoryName());
                }
            }
            if (resp.getCategoryId2() != null && resp.getCategoryId2() != 0L) {
                CourseCategory courseCategory = getCourseCategoryById(cacheMap, resp.getCategoryId2());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName2(courseCategory.getCategoryName());
                }
            }
            if (resp.getCategoryId3() != null && resp.getCategoryId3() != 0L) {
                CourseCategory courseCategory = getCourseCategoryById(cacheMap, resp.getCategoryId3());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName3(courseCategory.getCategoryName());
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 课程信息-审核添加
     *
     * @param req 课程信息-审核
     * @return 添加结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> save(CourseAuditSaveREQ req) {
        //校验敏感词
        /*SensitiveUtil.init(feignSensitiveWordLibrary.listAll());
        String introduce = SensitiveUtil.getFindedFirstSensitive(bo.getIntroduce());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(introduce)) {
            return Result.error("存在敏感词{" + introduce + "}");
        }
        String courseName = SensitiveUtil.getFindedFirstSensitive(bo.getCourseName());
        if (!com.alibaba.druid.util.StringUtils.isEmpty(courseName)) {
            return Result.error("存在敏感词{" + courseName + "}");
        }*/

        // 判断是否是讲师
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(req.getLecturerUserNo());
        if (ObjectUtil.isNull(lecturerVO)) {
            return Result.error("请先申请成为讲师");
        }
        if (CourseCategoryEnum.LIVE.getCode().equals(req.getCourseCategory())) {
            if (IsLiveEnum.NO.getCode().equals(lecturerVO.getIsLive())) {
                return Result.error("请先申请直播功能");
            }
        }

        // 原价为0，课程也是免费
        if (req.getCourseOriginal().equals(BigDecimal.ZERO) || StringUtils.isEmpty(req.getCourseOriginal())) {
            req.setIsFree(IsFreeEnum.FREE.getCode());
        }

        if (IsFreeEnum.FREE.getCode().equals(req.getIsFree())) {
            // 课程免费就设置价格为0(原价、优惠价)
            req.setCourseOriginal(BigDecimal.valueOf(0));
            req.setCourseDiscount(BigDecimal.valueOf(0));
        } else {
            // 课程收费但价格为空
            if (req.getCourseOriginal() == null) {
                return Result.error("请输入价格");
            }
            if (req.getCourseDiscount() == null) {
                req.setCourseDiscount(BigDecimal.valueOf(0));
            }
            // 原价小于0
            if (req.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) < 0 || req.getCourseDiscount().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("售价不能为负数");
            }
            if (req.getCourseDiscount().compareTo(req.getCourseOriginal()) > 0) {
                return Result.error("会员价不能大于原价");
            }
        }

        CourseAudit record = BeanUtil.copyProperties(req, CourseAudit.class);
        record.setLecturerUserNo(lecturerVO.getLecturerUserNo());
        // 课程介绍
        if (StringUtils.hasText(req.getIntroduce())) {
            CourseIntroduceAudit courseIntroduceAudit = new CourseIntroduceAudit();
            courseIntroduceAudit.setIntroduce(req.getIntroduce());
            courseIntroduceAuditDao.save(courseIntroduceAudit);

            record.setIntroduceId(courseIntroduceAudit.getId());
        }
        record.setStatusId(StatusIdEnum.YES.getCode());
        record.setIsPutaway(IsPutawayEnum.YES.getCode());
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        record.setId(IdWorker.getId());

        // 成功返回参数
        AuthCourseAuditSaveDTO dto = BeanUtil.copyProperties(record, AuthCourseAuditSaveDTO.class);
        if (CourseCategoryEnum.RESOURCES.getCode().equals(req.getCourseCategory())) {
            // 课程分类为文库默认添加一个章节
            CourseChapterAudit courseChapterAudit = new CourseChapterAudit();
            courseChapterAudit.setCourseId(record.getId());
            courseChapterAudit.setChapterName("第一章");
            courseChapterAudit.setChapterOriginal(BigDecimal.valueOf(0));
            courseChapterAudit.setChapterDiscount(BigDecimal.valueOf(0));
            courseChapterAudit.setId(IdWorker.getId());
            courseChapterAuditDao.save(courseChapterAudit);
            dto.setChapterId(courseChapterAudit.getId());
        }
        if (dao.save(record) > 0) {
            if (CollectionUtils.isNotEmpty(req.getAccessoryList())) {
                // 添加附件信息
                for (CourseAccessorySaveREQ courseAccessorySaveREQ : req.getAccessoryList()) {
                    CourseAccessory courseAccessory = new CourseAccessory();
                    courseAccessory.setAcName(courseAccessorySaveREQ.getAcName());
                    courseAccessory.setAcUrl(courseAccessorySaveREQ.getAcUrl());
                    courseAccessory.setCourseCategory(req.getCourseCategory());
                    courseAccessory.setRefId(record.getId());
                    courseAccessory.setRefName(record.getCourseName());
                    courseAccessory.setRefType(RefTypeEnum.COURSE.getCode());
                    courseAccessoryDao.save(courseAccessory);
                }
            }
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 课程信息-审核查看
     *
     * @param id 主键ID
     * @return 课程信息-审核
     */
    public Result<CourseAuditViewRESP> view(Long id) {
        CourseAuditViewRESP resp = BeanUtil.copyProperties(dao.getById(id), CourseAuditViewRESP.class);
        if (ObjectUtil.isNull(resp)) {
            return Result.success(resp);
        }

        // 课程简介
        if (resp.getIntroduceId() != null && !resp.getIntroduceId().equals(0L)) {
            CourseIntroduceAudit courseIntroduceAudit = courseIntroduceAuditDao.getById(resp.getIntroduceId());
            resp.setIntroduce(courseIntroduceAudit.getIntroduce());
        }
        return Result.success(resp);
    }

    /**
     * 课程信息-审核修改
     *
     * @param req 课程信息-审核修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> edit(CourseAuditEditREQ req) {
        BeanValidateUtil.ValidationResult result = BeanValidateUtil.validate(req);
        if (!result.isPass()) {
            return Result.error(result.getErrMsgString());
        }
        if (IsFreeEnum.FREE.getCode().equals(req.getIsFree())) {
            req.setCourseOriginal(BigDecimal.ZERO);
            req.setCourseDiscount(BigDecimal.ZERO);
        }
        // 价格处理
        // 原价为0，试卷也是免费
        if (IsFreeEnum.FREE.getCode().equals(req.getIsFree())) {
            // 免费就设置价格为0(原价、优惠价)
            req.setCourseOriginal(BigDecimal.valueOf(0));
            req.setCourseDiscount(BigDecimal.valueOf(0));
        } else {
            // 收费但价格为空
            if (req.getCourseOriginal() == null) {
                return Result.error("请输入价格");
            }
            if (req.getCourseDiscount() == null) {
                req.setCourseDiscount(BigDecimal.valueOf(0));
            }
            // 价格不能小于0
            if (req.getCourseOriginal().compareTo(BigDecimal.valueOf(0)) < 0 || req.getCourseDiscount().compareTo(BigDecimal.valueOf(0)) < 0) {
                return Result.error("价格不能为负数");
            }
            if (req.getCourseDiscount().compareTo(req.getCourseOriginal()) > 0) {
                return Result.error("优惠价不能大于原价");
            }
        }
        CourseAudit record = BeanUtil.copyProperties(req, CourseAudit.class);
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());

        CourseIntroduceAudit introduceAudit = courseIntroduceAuditDao.getById(record.getIntroduceId());
        if (ObjectUtil.isNull(introduceAudit)) {
            return Result.error("找不到课程简介信息");
        }
        introduceAudit.setId(req.getIntroduceId());
        introduceAudit.setIntroduce(req.getIntroduce());
        courseIntroduceAuditDao.updateById(introduceAudit);

        if (dao.updateById(record) > 0) {
            if (CollectionUtils.isNotEmpty(req.getAccessoryList())) {

                // 查找该文库区课程的附件信息
                List<CourseAccessory> courseAccessoryList = courseAccessoryDao.listByRefIdAndStatusId(req.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isNotEmpty(courseAccessoryList)) {
                    for (CourseAccessory courseAccessory : courseAccessoryList) {
                        // 删除原有的附件信息
                        courseAccessoryDao.deleteById(courseAccessory.getId());
                    }
                }
                // 添加附件信息
                for (CourseAccessorySaveREQ courseAccessorySaveREQ : req.getAccessoryList()) {
                    CourseAccessory courseAccessory = new CourseAccessory();
                    courseAccessory.setAcName(courseAccessorySaveREQ.getAcName());
                    courseAccessory.setAcUrl(courseAccessorySaveREQ.getAcUrl());
                    courseAccessory.setCourseCategory(req.getCourseCategory());
                    courseAccessory.setRefId(req.getId());
                    courseAccessory.setRefType(RefTypeEnum.COURSE.getCode());
                    courseAccessoryDao.save(courseAccessory);
                }
            }
            return Result.success("编辑成功");
        }
        throw new BaseException("编辑失败");
    }

    /**
     * 修改状态
     *
     * @param courseAuditUpdateStatusREQ 修改状态参数
     * @return 修改结果
     */
    public Result<String> updateStatus(CourseAuditUpdateStatusREQ courseAuditUpdateStatusREQ) {
        CourseAudit courseAudit = dao.getById(courseAuditUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("课程不存在");
        }
        courseAudit.setStatusId(courseAuditUpdateStatusREQ.getStatusId());
        if (dao.updateById(courseAudit) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 修改是否上下架
     *
     * @param courseAuditUpdateIsPutawayREQ 修改是否上下架参数
     * @return 修改结果
     */
    public Result<String> updateIsPutaway(CourseAuditUpdateIsPutawayREQ courseAuditUpdateIsPutawayREQ) {
        CourseAudit courseAudit = dao.getById(courseAuditUpdateIsPutawayREQ.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("课程不存在");
        }
        courseAudit.setIsPutaway(courseAuditUpdateIsPutawayREQ.getIsPutaway());
        if (dao.updateById(courseAudit) > 0) {
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 课程信息-审核删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<String> audit(CourseAuditREQ courseAuditREQ) {
        CourseAudit courseAudit = dao.getById(courseAuditREQ.getId());
        if (ObjectUtil.isNull(courseAudit)) {
            return Result.error("课程不存在");
        }
        // 不成功
        if (!AuditStatusEnum.SUCCESS.getCode().equals(courseAuditREQ.getAuditStatus())) {
            CourseAudit audit = BeanUtil.copyProperties(courseAuditREQ, CourseAudit.class);
            if (dao.updateById(audit) > 0) {
                // 发送站内消息消息通知讲师
                CALLBACK_EXECUTOR.execute(new MsgSend(courseAudit, courseAuditREQ.getAuditStatus(), courseAuditREQ.getAuditOpinion()));
                return Result.success("审核成功");
            }
            return Result.error("审核失败");
        }
        // 根据课程ID查询课时信息集合
        List<CourseChapterPeriodAudit> periodAuditList = courseChapterPeriodAuditDao.listByCourseId(courseAudit.getId());

        // 2、对课程简介操作
        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            CourseIntroduceAudit courseIntroduceAudit = courseIntroduceAuditDao.getById(courseAudit.getIntroduceId());
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(courseAudit.getIntroduceId());
            if (ObjectUtil.isNull(courseIntroduceAudit)) {
                throw new BaseException("课程简介信息表不存在");
            }
            if (ObjectUtil.isNull(courseIntroduce)) {
                CourseIntroduce introduce = BeanUtil.copyProperties(courseIntroduceAudit, CourseIntroduce.class);
                courseIntroduceDao.save(introduce);
            } else {
                courseIntroduce = BeanUtil.copyProperties(courseIntroduceAudit, CourseIntroduce.class);
                courseIntroduceDao.updateById(courseIntroduce);
            }
        }

        // 3、对章节操作
        // 根据课程编号查找章节审核信息集合
        List<CourseChapterAudit> courseChapterAuditList = courseChapterAuditDao.listByCourseId(courseAudit.getId());
        if (CollectionUtils.isNotEmpty(courseChapterAuditList)) {
            chapterAudit(courseChapterAuditList);
        }

        // 4、对课时操作
        // 根据课程编号查找课时审核信息集合
        // 课程视频总时长
        String videoLength = "";
        List<CourseChapterPeriodAudit> courseChapterPeriodAuditList = courseChapterPeriodAuditDao.listByCourseId(courseAudit.getId());
        if (ObjectUtil.isNotNull(courseChapterPeriodAuditList)) {
            videoLength = periodAudit(courseChapterPeriodAuditList);
        }

        // 1、对课程操作
        Course info = BeanUtil.copyProperties(courseAudit, Course.class);
        // 如果课程信息表里面有数据就进行更新
        info.setGmtCreate(null);
        info.setGmtModified(null);
        // 设置总课时数
        if (CollectionUtils.isEmpty(periodAuditList)) {
            info.setPeriodTotal(0);
        } else {
            info.setPeriodTotal(periodAuditList.size());
        }
        info.setVideoLength(videoLength);

        Course course = courseDao.getById(courseAudit.getId());
        if (ObjectUtil.isNotNull(course)) {
            // 设置总课时数
            // 更新课程信息表
            courseDao.updateById(info);
        } else {
            // 如果课程信息表里面没数据就进行插入
            courseDao.save(info);
        }
        // 更改课程审核状态
        CourseAudit audit = BeanUtil.copyProperties(courseAuditREQ, CourseAudit.class);
        int resultNum = dao.updateById(audit);
        if (StringUtils.hasText(videoLength)) {
            // 异步更新相关视频时长
            CALLBACK_EXECUTOR.execute(new VideoLength(info.getId(), videoLength));
        }

        if (resultNum > 0) {
            try {
                // 查询讲师名称并插入es
                // 插入es或者更新es

                LecturerVO lecturerInfoVO = feignLecturer.getByLecturerUserNo(courseAudit.getLecturerUserNo());
                EsCourse esCourse = BeanUtil.copyProperties(courseAudit, EsCourse.class);
                if (ObjectUtil.isNotNull(lecturerInfoVO) && StrUtil.isNotBlank(lecturerInfoVO.getLecturerName()) && lecturerInfoVO.getStatusId().equals(StatusIdEnum.YES.getCode())) {
                    esCourse.setLecturerName(lecturerInfoVO.getLecturerName());
                }
                esCourse.setCourseSort(0);
                IndexQuery query = new IndexQueryBuilder().withObject(esCourse).build();
                elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsCourse.COURSE));
            } catch (Exception e) {
                logger.warn("elasticsearch更新数据失败", e);
                throw new BaseException("审核失败");
            }
            // 发送站内消息消息通知讲师
            CALLBACK_EXECUTOR.execute(new MsgSend(courseAudit, courseAuditREQ.getAuditStatus(), courseAuditREQ.getAuditOpinion()));
            return Result.success("审核成功");
        }

        return Result.error("审核失败");
    }

    /**
     * 获取课程分类
     *
     * @param cacheMap 缓存Map
     * @param id       分类ID
     * @return 课程分类
     */
    private CourseCategory getCourseCategoryById(Map<Long, CourseCategory> cacheMap, Long id) {
        CourseCategory courseCategory = cacheMap.get(id);
        if (ObjectUtil.isNotNull(courseCategory)) {
            return courseCategory;
        }

        courseCategory = courseCategoryDao.getById(id);
        if (ObjectUtil.isNotNull(courseCategory)) {
            cacheMap.put(id, courseCategory);
            return courseCategory;
        }
        return null;
    }

    /**
     * 章节信息审核
     *
     * @param courseChapterAuditList 章节信息审核列表
     */
    private void chapterAudit(List<CourseChapterAudit> courseChapterAuditList) {
        for (CourseChapterAudit courseChapterAudit : courseChapterAuditList) {
            // 根据章节编号查找章节审核信息
            CourseChapterAudit infoAudit = courseChapterAuditDao.getById(courseChapterAudit.getId());
            // 查找章节信息表是否存在该课时信息
            CourseChapter chapter = courseChapterDao.getById(courseChapterAudit.getId());

            if (ObjectUtil.isNotNull(chapter)) {
                // 存在就更新章节信息表数据
                chapter = BeanUtil.copyProperties(infoAudit, CourseChapter.class);
                chapter.setGmtCreate(null);
                chapter.setGmtModified(null);
                courseChapterDao.updateById(chapter);
            } else {
                // 如果章节不存在则插入章节信息
                chapter = BeanUtil.copyProperties(infoAudit, CourseChapter.class);
                chapter.setGmtCreate(null);
                chapter.setGmtModified(null);
                courseChapterDao.save(chapter);
            }

            // 更新审核状态
            infoAudit.setAuditStatus(AuditStatusEnum.SUCCESS.getCode());
            courseChapterAuditDao.updateById(infoAudit);
        }
    }

    /**
     * 课时信息审核
     *
     * @param periodAuditList 课时信息审核列表
     */
    private String periodAudit(List<CourseChapterPeriodAudit> periodAuditList) {
        BigDecimal countTime = BigDecimal.ZERO;
        for (CourseChapterPeriodAudit chapterPeriodAudit : periodAuditList) {
            // 根据课时编号查找课时信息
            CourseChapterPeriod chapterPeriod = courseChapterPeriodDao.getById(chapterPeriodAudit.getId());

            if (ObjectUtil.isNotNull(chapterPeriod)) {
                // 如果信息表存在就更新信息表信息
                chapterPeriod = BeanUtil.copyProperties(chapterPeriodAudit, CourseChapterPeriod.class);
                chapterPeriod.setGmtCreate(null);
                chapterPeriod.setGmtModified(null);
                courseChapterPeriodDao.updateById(chapterPeriod);
            } else {
                // 如果课时信息表不存在就插入信息
                chapterPeriod = BeanUtil.copyProperties(chapterPeriodAudit, CourseChapterPeriod.class);
                chapterPeriod.setGmtCreate(null);
                chapterPeriod.setGmtModified(null);
                courseChapterPeriodDao.save(chapterPeriod);
            }

            // 更新审核状态
            chapterPeriodAudit.setAuditStatus(AuditStatusEnum.SUCCESS.getCode());
            courseChapterPeriodAuditDao.updateById(chapterPeriodAudit);

            if (StringUtils.hasText(chapterPeriodAudit.getVideoLength())) {
                DateTime dateTime = DateUtil.parseTime(chapterPeriodAudit.getVideoLength());
                countTime = countTime.add(BigDecimal.valueOf(dateTime.getTime()));
            }
        }
        if (countTime.equals(BigDecimal.ZERO)) {
            return "";
        }
        //初始化Formatter的转换格式
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        // 返回课程视频总时长
        return formatter.format(countTime);
    }

    /**
     * 用户订单课程关联
     *
     * @param courseId    课程ID
     * @param videoLength 视频时长
     */
    private void userOrderCourseRef(Long courseId, String videoLength) {
        List<UserOrderCourseRef> list = userOrderCourseRefDao.listCourseId(courseId);
        for (UserOrderCourseRef ref : list) {
            // 课程视频总时长不存在 或 课程视频总时长不与原来不一致
            // 更新信息
            if (StringUtils.isEmpty(ref.getCourseLength()) || !ref.getCourseLength().equals(videoLength)) {
                ref.setCourseLength(videoLength);
               /* if (StringUtils.hasText(ref.getStudyLength())) {
                    // 更新观看进度
                    BigDecimal watchLength = new BigDecimal(ref.getStudyLength());
                    BigDecimal courseLength = new BigDecimal(DateUtil.getSecond(videoLength));
                    BigDecimal watchProgress = watchLength.divide(courseLength, 2, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100L));
                    double f1 = watchProgress.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                    ref.setStudyProcess(String.valueOf(f1));
                }*/
                userOrderCourseRefDao.updateById(ref);
            }
        }
    }

    /**
     * 异步发送通知讲师消息
     */
    class MsgSend implements Runnable {
        private final CourseAudit courseAudit;
        private final Integer auditStatus;
        private final String auditOpinion;

        public MsgSend(CourseAudit courseAudit, Integer auditStatus, String auditOpinion) {
            this.courseAudit = courseAudit;
            this.auditStatus = auditStatus;
            this.auditOpinion = auditOpinion;
        }

        @Override
        public void run() {
            try {
                MsgQO qo = new MsgQO();
                qo.setLecturerUserNo(courseAudit.getLecturerUserNo());
                qo.setCourseId(courseAudit.getId());
                qo.setCourseName(courseAudit.getCourseName());
                qo.setAuditStatus(auditStatus);
                qo.setAuditOpinion(auditOpinion);
                feignMsg.msgSendLecturer(qo);
            } catch (Exception e) {
                logger.error("异步更新时长失败", e);
            }
        }
    }

    /**
     * 异步更新时长
     */
    class VideoLength implements Runnable {
        private final Long courseId;
        private final String videoLength;

        public VideoLength(Long courseId, String videoLength) {
            this.courseId = courseId;
            this.videoLength = videoLength;
        }

        @Override
        public void run() {
            try {
                userOrderCourseRef(courseId, videoLength);

                CourseLogQO qo = new CourseLogQO();
                qo.setCourseId(courseId);
                feignCourseLog.updateByCourseId(qo);
            } catch (Exception e) {
                logger.error("异步更新时长失败", e);
            }
        }
    }


}



