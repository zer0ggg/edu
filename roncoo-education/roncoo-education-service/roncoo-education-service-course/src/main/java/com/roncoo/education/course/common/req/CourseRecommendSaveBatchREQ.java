package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 课程推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseRecommendSaveBatchREQ", description = "课程推荐批量添加")
public class CourseRecommendSaveBatchREQ implements Serializable {


    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类ID不能为空")
    @ApiModelProperty(value = "分类ID",required = true)
    private Long categoryId;

    @NotEmpty(message = "课程ids不能为空")
    @ApiModelProperty(value = "课程ids",required = true)
    private String courseIds;
}
