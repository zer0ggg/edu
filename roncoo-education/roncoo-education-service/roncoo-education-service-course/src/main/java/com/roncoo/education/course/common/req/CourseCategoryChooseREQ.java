package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程分类
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseCategoryChooseREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类类型(1点播，2直播，4文库)
     */
    @ApiModelProperty(value = "分类类型(1点播，2直播，4文库)", required = true)
    private Integer categoryType;
}
