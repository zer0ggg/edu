package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户收藏课程分类列表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserCollectionCourseDeleteBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 收藏id
	 */
	@ApiModelProperty(value = "收藏id")
	private Long collectionId;
	/**
	 * 收藏类型:1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "收藏类型:1课程，2章节，3课时", required = true)
	private Integer collectionType;



}
