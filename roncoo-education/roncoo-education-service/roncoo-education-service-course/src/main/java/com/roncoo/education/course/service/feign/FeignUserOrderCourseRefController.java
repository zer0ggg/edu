package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignUserOrderCourseRef;
import com.roncoo.education.course.feign.qo.UserOrderCourseRefQO;
import com.roncoo.education.course.feign.vo.UserOrderCourseRefVO;
import com.roncoo.education.course.service.feign.biz.FeignUserOrderCourseRefBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@RestController
public class FeignUserOrderCourseRefController extends BaseController implements IFeignUserOrderCourseRef {

    @Autowired
    private FeignUserOrderCourseRefBiz biz;

    @Override
    public Page<UserOrderCourseRefVO> listForPage(@RequestBody UserOrderCourseRefQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody UserOrderCourseRefQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@RequestBody Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody UserOrderCourseRefQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public UserOrderCourseRefVO getById(@RequestBody Long id) {
        return biz.getById(id);
    }

    @Override
    public UserOrderCourseRefVO getByUserNoAndRefIdAndCourseType(@RequestBody UserOrderCourseRefQO qo) {
        return biz.getByUserNoAndRefIdAndCourseType(qo);
    }

    @Override
    public UserOrderCourseRefVO getByUserNoAndRefId(UserOrderCourseRefQO qo) {
        return biz.getByUserNoAndRefId(qo);
    }

    @Override
    public int updateByUserNoAndRefId(@RequestBody UserOrderCourseRefQO userOrderCourseRefQO) {
        return biz.updateByUserNoAndRefId(userOrderCourseRefQO);
    }

}
