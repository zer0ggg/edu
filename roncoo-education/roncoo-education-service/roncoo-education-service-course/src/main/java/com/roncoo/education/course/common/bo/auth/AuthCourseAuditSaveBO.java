/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author WY
 */
@Data
@Accessors(chain = true)
public class AuthCourseAuditSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "一级分类ID", required = false)
    private Long categoryId1;

    @ApiModelProperty(value = "二级分类ID", required = false)
    private Long categoryId2;

    @ApiModelProperty(value = "三级分类ID", required = false)
    private Long categoryId3;

    @ApiModelProperty(value = "课程名称", required = true)
    private String courseName;

    @ApiModelProperty(value = "课程封面", required = true)
    private String courseLogo;

    @ApiModelProperty(value = "课程简介", required = true)
    private String introduce;

    @ApiModelProperty(value = "原价", required = true)
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价格", required = true)
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4:文库)", required = true)
    private Integer courseCategory;

    @ApiModelProperty(value = "SEO关键词", required = true)
    private String keywords;

    @ApiModelProperty(value = "SEO描述", required = true)
    private String description;

    @ApiModelProperty(value = "附件集合", required = false)
    private List<AuthCourseAccessoryBO> accessoryList;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}
