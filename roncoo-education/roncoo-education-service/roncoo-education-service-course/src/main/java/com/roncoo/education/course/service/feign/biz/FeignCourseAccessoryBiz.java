package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.enums.CourseCategoryEnum;
import com.roncoo.education.common.core.enums.FileTypeEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.feign.qo.CourseAccessoryQO;
import com.roncoo.education.course.feign.vo.CourseAccessoryVO;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.FileInfoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample.Criteria;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 附件信息
 *
 * @author wujing
 */
@Component
public class FeignCourseAccessoryBiz {

	@Autowired
	private CourseAccessoryDao dao;

	@Autowired
	private FileInfoDao fileInfoDao;

	public Page<CourseAccessoryVO> listForPage(CourseAccessoryQO qo) {
		CourseAccessoryExample example = new CourseAccessoryExample();
		Criteria c = example.createCriteria();
		if (StringUtils.hasText(qo.getRefName())) {
			c.andRefNameLike(PageUtil.like(qo.getRefName()));
		}
		if (qo.getCourseCategory() != null) {
			c.andCourseCategoryEqualTo(qo.getCourseCategory());
		}
		example.setOrderByClause(" id desc ");
		Page<CourseAccessory> page = dao.listForPage(qo.getPageCurrent(), qo.getPageSize(), example);
		Page<CourseAccessoryVO> listForPage = PageUtil.transform(page, CourseAccessoryVO.class);
		for (CourseAccessoryVO vo : listForPage.getList()) {
			FileInfo file = fileInfoDao.getById(vo.getId());
			if (ObjectUtil.isNotNull(file) && FileTypeEnum.ACCESSORY.getCode().equals(file.getFileType())) {
				vo.setFileSize(file.getFileSize());
			}
		}
		return listForPage;
	}

	public int save(CourseAccessoryQO qo) {
		CourseAccessory record = BeanUtil.copyProperties(qo, CourseAccessory.class);
		return dao.save(record);
	}

	public int deleteById(Long id) {
		return dao.deleteById(id);
	}

	public CourseAccessoryVO getById(Long id) {
		CourseAccessory record = dao.getById(id);
		return BeanUtil.copyProperties(record, CourseAccessoryVO.class);
	}

	public int updateById(CourseAccessoryQO qo) {
		CourseAccessory record = BeanUtil.copyProperties(qo, CourseAccessory.class);
		return dao.updateById(record);
	}

	public CourseAccessoryVO countDownload() {
		CourseAccessoryVO vo = new CourseAccessoryVO();
		// 总下载数
		int totalDownload = dao.totalDownload();
		vo.setTotalDownload(totalDownload);
		// 点播课程下载数
		int courseDownload = dao.courseDownload(CourseCategoryEnum.ORDINARY.getCode());
		vo.setCourseDownload(courseDownload);
		// 直播下载数
		int liveDownload = dao.courseDownload(CourseCategoryEnum.LIVE.getCode());
		vo.setLiveDownload(liveDownload);
		// 文库下载数
		int resourcesDownload = dao.courseDownload(CourseCategoryEnum.RESOURCES.getCode());
		vo.setResourcesDownload(resourcesDownload);
		return vo;
	}

	/**
	 * 去重查找所有附件信息
	 *
	 * @param qo
	 * @author kyh
	 */
	public List<CourseAccessoryVO> sumByAll(CourseAccessoryQO qo) {
		CourseAccessory record = BeanUtil.copyProperties(qo, CourseAccessory.class);
		List<CourseAccessory> list = dao.sumByAll(record);
		return ArrayListUtil.copy(list, CourseAccessoryVO.class);
	}

}
