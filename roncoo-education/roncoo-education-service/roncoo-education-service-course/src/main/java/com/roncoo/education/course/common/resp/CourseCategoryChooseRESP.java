package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 课程分类
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseCategoryChooseRESP", description = "课程分类列表")
public class CourseCategoryChooseRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程分类集合")
    private List<CourseCategoryChooseListRESP> list;
}
