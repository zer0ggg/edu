package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程分类
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseCategoryUpdateStatusREQ", description = "课程分类修改状态")
public class CourseCategoryUpdateStatusREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;
}
