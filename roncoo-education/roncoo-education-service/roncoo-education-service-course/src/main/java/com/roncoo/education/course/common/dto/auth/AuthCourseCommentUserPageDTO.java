package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 课程评论表
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
public class AuthCourseCommentUserPageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "评论时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    /**
     * 父ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "父ID")
    private Long parentId;
    /**
     * 课程ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "课程ID")
    private Long courseId;
    /**
     * 课程用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "被评论者用户编号")
    private Long courseUserNo;
    /**
     * 课程用户头像
     */
    @ApiModelProperty(value = "被评论者用户头像")
    private String courseUserImg;
    /**
     * 课程用户昵称
     */
    @ApiModelProperty(value = "被评论者用户昵称")
    private String courseNickname;
    /**
     * 评论者用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;
    /**
     * 评论者用户头像
     */
    @ApiModelProperty(value = "评论者用户头像")
    private String userImg;
    /**
     * 评论者用户昵称
     */
    @ApiModelProperty(value = "评论者用户昵称")
    private String nickname;
    /**
     * 评论内容
     */
    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "回复内容")
    private List<AuthCourseCommentUserPageDTO> commentList;
}
