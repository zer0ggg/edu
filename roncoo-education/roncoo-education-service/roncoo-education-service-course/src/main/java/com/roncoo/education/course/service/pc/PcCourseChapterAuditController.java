package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseChapterAuditEditREQ;
import com.roncoo.education.course.common.req.CourseChapterAuditListREQ;
import com.roncoo.education.course.common.req.CourseChapterAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterAuditListRESP;
import com.roncoo.education.course.common.resp.CourseChapterAuditViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseChapterAuditBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 章节信息-审核 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/chapter/audit")
@Api(value = "course-章节信息-审核", tags = {"course-章节信息-审核"})
public class PcCourseChapterAuditController {

    @Autowired
    private PcCourseChapterAuditBiz biz;

    @ApiOperation(value = "章节信息-审核列表", notes = "章节信息-审核列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseChapterAuditListRESP>> list(@RequestBody CourseChapterAuditListREQ courseChapterAuditListREQ) {
        return biz.list(courseChapterAuditListREQ);
    }

    @ApiOperation(value = "章节信息-审核添加", notes = "章节信息-审核添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseChapterAuditSaveREQ courseChapterAuditSaveREQ) {
        return biz.save(courseChapterAuditSaveREQ);
    }

    @ApiOperation(value = "章节信息-审核查看", notes = "章节信息-审核查看")
    @GetMapping(value = "/view")
    public Result<CourseChapterAuditViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "章节信息-审核修改", notes = "章节信息-审核修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseChapterAuditEditREQ courseChapterAuditEditREQ) {
        return biz.edit(courseChapterAuditEditREQ);
    }

    @ApiOperation(value = "章节信息-审核删除", notes = "章节信息-审核删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
