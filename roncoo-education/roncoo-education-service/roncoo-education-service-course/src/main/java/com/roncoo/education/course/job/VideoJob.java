package com.roncoo.education.course.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;

/**
 * 目前视频上传改用web直接上传，不再经过服务端
 * 定时任务-视频处理
 *
 * @author wuyun
 */
@Slf4j
@Component
@Deprecated
public class VideoJob extends IJobHandler {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseVideoDao dao;

    @Override
    @XxlJob(value = "videoJob")
    public ReturnT<String> execute(String param) {
        XxlJobLogger.log("开始执行[视频处理]");
        int videoSum = 0;

        File file = new File(SystemUtil.VIDEO_PATH);
        if (file.isDirectory()) {// isDirectory是否文件夹
            File[] files = file.listFiles();// listFiles是获取该目录下所有文件和目录的绝对路径
            for (File targetFile : files) {
                if (targetFile.isFile() && targetFile.exists()) {
                    if (!FileUtil.newerThan(targetFile, (System.currentTimeMillis() - 7200000))) {// 上传两个小时后的视频
                        try {
                            handleScheduledTasks(targetFile);
                            videoSum = videoSum + 1;
                        } catch (Exception e) {
                            log.error("视频定时任务处理失败", e);
                            return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[视频处理]--处理异常");
                        }
                    }
                }
            }
        }

        log.info("视频处理-定时任务完成，处理视频数={}", videoSum);
        XxlJobLogger.log("结束执行[视频处理]");
        return SUCCESS;
    }

    /**
     * 定时任务-视频处理
     *
     * @param targetFile
     * @author wuyun
     */
    public void handleScheduledTasks(File targetFile) {
        Long videoId = Long.valueOf(StrUtil.getPrefix(targetFile.getName()));
        CourseVideo courseVideo = dao.getById(videoId);
        if (ObjectUtil.isNull(courseVideo)) {
            if (targetFile.isFile() && targetFile.exists()) {
                if (targetFile.delete()) {
                    log.warn("删除本地文件成功");
                } else {
                    log.error("删除本地文件失败");
                }
            }
            return;
        }
        // 如果不是删除上传失败
        if (!VideoStatusEnum.FINAL.getCode().equals(courseVideo.getVideoStatus())) {
            if (VideoStatusEnum.SUCCES.getCode().equals(courseVideo.getVideoStatus())) {
                // 如果成功删除本地文件
                if (!targetFile.delete()) {
                    log.error("删除失败");
                }
            }
            return;
        }

        ConfigPolyvVO sys = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sys)) {
            try {
                throw new Exception("找不到系统配置信息");
            } catch (Exception e) {
                log.error("定时任务-视频处理失败", e);
            }
        }
        if (StringUtils.isEmpty(sys.getPolyvWritetoken())) {
            try {
                throw new Exception("writetoken没配置");
            } catch (Exception e) {
                log.error("定时任务-视频处理失败", e);
            }
        }
        // 修改状态为上传中
        courseVideo.setVideoStatus(VideoStatusEnum.UPLOADING.getCode());
        dao.updateById(courseVideo);

        // 上传保利威
        String videoVId = PolyvUtil.sdkUploadFile(targetFile.getName(), courseVideo.getVideoName(), SystemUtil.VIDEO_PATH, VideoTagEnum.COURSE.getCode(), null, sys.getPolyvUseid(), sys.getPolyvSecretkey());
        if (!StringUtils.isEmpty(videoVId)) {
            if (IsBackupEnum.YES.getCode().equals(feignSysConfig.getByConfigKey(SysConfigConstants.IS_BACKUP_ALIYUN).getConfigValue())) {
                // 2、异步上传到阿里云
                String videoOssId = AliyunUtil.uploadVideo(PlatformEnum.COURSE, targetFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
                courseVideo.setVideoOssId(videoOssId);
            }

            // 上传
            courseVideo.setVideoVid(videoVId);
            courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
            dao.updateById(courseVideo);

            // 更新课时审核视频信息
            List<CourseChapterPeriodAudit> periodAuditList = courseChapterPeriodAuditDao.listByVideoId(videoId);
            if (CollectionUtil.isNotEmpty(periodAuditList)) {
                for (CourseChapterPeriodAudit periodAudit : periodAuditList) {
                    periodAudit.setVideoVid(videoVId);
                    periodAudit.setIsVideo(IsVideoEnum.YES.getCode());
                    courseChapterPeriodAuditDao.updateById(periodAudit);
                }
            }

            // 更新课时视频信息
            List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByVideoId(videoId);
            if (CollectionUtil.isNotEmpty(periodList)) {
                for (CourseChapterPeriod period : periodList) {
                    period.setVideoVid(videoVId);
                    period.setIsVideo(IsVideoEnum.YES.getCode());
                    courseChapterPeriodDao.updateById(period);
                }
            }
            // 4、成功删除本地文件
            if (targetFile.isFile() && targetFile.exists()) {
                if (targetFile.delete()) {
                    log.warn("删除本地文件成功");
                } else {
                    log.error("删除本地文件失败");
                }
            }
            return;

        }

        log.warn("上传视频失败");
        // 上传失败修改状态为待上传
        courseVideo.setVideoStatus(VideoStatusEnum.FINAL.getCode());
        dao.updateById(courseVideo);

    }
}
