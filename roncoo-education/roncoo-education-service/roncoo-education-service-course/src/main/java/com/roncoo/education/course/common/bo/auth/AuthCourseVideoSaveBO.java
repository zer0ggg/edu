package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程视频信息
 *
 * @author wuyun
 */
@Data
@Accessors(chain = true)
public class AuthCourseVideoSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 视频Id
     */
    @ApiModelProperty(value = "视频Id")
    private Long videoId;
    /**
     * 章节ID（添加章节视频时传入）
     */
    @ApiModelProperty(value = "章节ID")
    private Long chapterId;
}
