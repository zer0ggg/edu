package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.CourseCommentDao;
import com.roncoo.education.course.service.dao.impl.mapper.CourseCommentMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 课程评论 服务实现类
 *
 * @author Quanf
 * @date 2020-09-03
 */
@Repository
public class CourseCommentDaoImpl implements CourseCommentDao {

    @Autowired
    private CourseCommentMapper mapper;

    @Override
    public int save(CourseComment record) {
        record.setId(IdWorker.getId());
        return this.mapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CourseComment record) {
        return this.mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public CourseComment getById(Long id) {
        return this.mapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CourseComment> listForPage(int pageCurrent, int pageSize, CourseCommentExample example) {
        int count = this.mapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<>(count, totalPage, pageCurrent, pageSize, this.mapper.selectByExample(example));
    }

    @Override
    public List<CourseComment> listByParentIdAndStatusId(Long parentId, Integer statusId) {
        CourseCommentExample example = new CourseCommentExample();
        Criteria c = example.createCriteria();
        c.andParentIdEqualTo(parentId);
        c.andStatusIdEqualTo(statusId);
        return this.mapper.selectByExample(example);
    }


}
