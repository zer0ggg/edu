package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.FileInfoDao;
import com.roncoo.education.course.service.dao.impl.mapper.FileInfoMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@Repository
public class FileInfoDaoImpl implements FileInfoDao {
    @Autowired
    private FileInfoMapper fileInfoMapper;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(FileInfo record) {
        record.setId(IdWorker.getId());
        return this.fileInfoMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.fileInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(FileInfo record) {
        return this.fileInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(FileInfo record, FileInfoExample example) {
        return this.fileInfoMapper.updateByExampleSelective(record, example);
    }

    @Override
    public FileInfo getById(Long id) {
        return this.fileInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<FileInfo> listForPage(int pageCurrent, int pageSize, FileInfoExample example) {
        int count = this.fileInfoMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<FileInfo>(count, totalPage, pageCurrent, pageSize, this.fileInfoMapper.selectByExample(example));
    }

    @Override
    public String count() {
        Map<String, Object> map = new HashMap<String, Object>();

        String sql = "select sum(file_size) as count from file_info";
        map = jdbcTemplate.queryForMap(sql);
        String fileSize = 0 + "B";
        String returnSize = 0 + "B";
        if (!StringUtils.isEmpty(map.get("count"))) {
            if (Long.parseLong(String.valueOf(map.get("count"))) == 0) {
                return "0B";
            } else {
                returnSize = unitConversion(fileSize, String.valueOf(map.get("count")));
            }
        }
        return returnSize;
    }

    @Override
    public String countByFileType(Integer fileType) {
        Map<String, Object> map = new HashMap<String, Object>();
        String sql = "select sum(file_size) as count from file_info where file_type = ?";
        map = jdbcTemplate.queryForMap(sql, fileType);
        String fileSize = "0B";
        String returnSize = "0B";
        if (!StringUtils.isEmpty(map.get("count"))) {
            if (Long.parseLong(String.valueOf(map.get("count"))) == 0) {
                return "0B";
            } else {
                returnSize = unitConversion(fileSize, String.valueOf(map.get("count")));
            }
        }
        return returnSize;
    }

    private String unitConversion(String fileSize, String fileSizeString) {
        DecimalFormat df = new DecimalFormat("#.00");
        if (Long.parseLong(fileSizeString) < 1024) {
            return df.format((double) Long.parseLong(fileSizeString)) + "B";
        } else if (Long.parseLong(fileSizeString) < 1048576) {
            return df.format((double) Long.parseLong(fileSizeString) / 1024) + "KB";
        } else if (Long.parseLong(fileSizeString) < 1073741824) {
            return df.format((double) Long.parseLong(fileSizeString) / 1048576) + "MB";
        } else {
            return df.format((double) Long.parseLong(fileSizeString) / 1073741824) + "GB";
        }
    }

}
