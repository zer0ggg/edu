package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.base.SysLog;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.ResourceRecommendListRESP;
import com.roncoo.education.course.common.resp.ResourceRecommendViewRESP;
import com.roncoo.education.course.service.pc.biz.PcResourceRecommendBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文库推荐 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/resource/recommend")
@Api(value = "course-文库推荐", tags = {"course-文库推荐"})
public class PcResourceRecommendController {

    @Autowired
    private PcResourceRecommendBiz biz;

    @ApiOperation(value = "文库推荐列表", notes = "文库推荐列表")
    @PostMapping(value = "/list")
    public Result<Page<ResourceRecommendListRESP>> list(@RequestBody ResourceRecommendListREQ resourceRecommendListREQ) {
        return biz.list(resourceRecommendListREQ);
    }


    @ApiOperation(value = "文库推荐添加", notes = "文库推荐添加")
    @SysLog(value = "文库推荐添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody ResourceRecommendSaveREQ resourceRecommendSaveREQ) {
        return biz.save(resourceRecommendSaveREQ);
    }


    @ApiOperation(value = "文库推荐批量添加", notes = "批量添加")
    @SysLog(value = "文库推荐批量添加")
    @PostMapping(value = "/save/batch")
    public Result<String> saveBatch(@RequestBody ResourceRecommendSaveBatchREQ req) {
        return biz.saveBatch(req);
    }

    @ApiOperation(value = "文库推荐查看", notes = "文库推荐查看")
    @GetMapping(value = "/view")
    public Result<ResourceRecommendViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }


    @ApiOperation(value = "文库推荐修改", notes = "文库推荐修改")
    @SysLog(value = "文库推荐修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody ResourceRecommendEditREQ resourceRecommendEditREQ) {
        return biz.edit(resourceRecommendEditREQ);
    }


    @ApiOperation(value = "文库推荐状态修改", notes = "文库推荐状态修改")
    @SysLog(value = "文库推荐状态修改")
    @PutMapping(value = "/update/status")
    public Result<String> updateStatus(@RequestBody ResourceRecommendUpdateStatusREQ resourceRecommendUpdateStatusREQ) {
        return biz.updateStatus(resourceRecommendUpdateStatusREQ);
    }


    @ApiOperation(value = "文库推荐删除", notes = "文库推荐删除")
    @SysLog(value = "文库推荐删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
