package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLogExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseLiveLogMapper {
    int countByExample(CourseLiveLogExample example);

    int deleteByExample(CourseLiveLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CourseLiveLog record);

    int insertSelective(CourseLiveLog record);

    List<CourseLiveLog> selectByExample(CourseLiveLogExample example);

    CourseLiveLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CourseLiveLog record, @Param("example") CourseLiveLogExample example);

    int updateByExample(@Param("record") CourseLiveLog record, @Param("example") CourseLiveLogExample example);

    int updateByPrimaryKeySelective(CourseLiveLog record);

    int updateByPrimaryKey(CourseLiveLog record);
}
