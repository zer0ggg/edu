package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterPeriodAuditSaveDTO;
import com.roncoo.education.course.common.dto.auth.AuthPeriodAuditListDTO;
import com.roncoo.education.course.common.dto.auth.AuthPeriodAuditVideoViewDTO;
import com.roncoo.education.course.common.dto.auth.AuthPeriodAuditViewDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseChapterPeriodAuditBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/course/chapter/period/audit")
public class AuthCourseChapterPeriodAuditController extends BaseController {

	@Autowired
	private AuthCourseChapterPeriodAuditBiz biz;

	/**
	 * 课时列出接口
	 *
	 * @param authCourseChapterPeriodAuditBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时列出接口", notes = "根据章节ID列出课时信息")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<AuthPeriodAuditListDTO> listByChapterId(@RequestBody AuthCourseChapterPeriodAuditBO authCourseChapterPeriodAuditBO) {
		return biz.listByChapterId(authCourseChapterPeriodAuditBO);
	}

	/**
	 * 课时查看接口
	 *
	 * @param authCourseChapterPeriodAuditViewBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时查看接口", notes = "课时查看接口")
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	public Result<AuthPeriodAuditViewDTO> view(@RequestBody AuthCourseChapterPeriodAuditViewBO authCourseChapterPeriodAuditViewBO) {
		return biz.view(authCourseChapterPeriodAuditViewBO);
	}

	/**
	 * 课时删除接口
	 *
	 * @param authCourseChapterPeriodAuditDeleteBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时删除接口", notes = "课时删除接口")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Result<Integer> delete(@RequestBody AuthCourseChapterPeriodAuditDeleteBO authCourseChapterPeriodAuditDeleteBO) {
		return biz.delete(authCourseChapterPeriodAuditDeleteBO);
	}

	/**
	 * 课时添加接口
	 *
	 * @param authCourseChapterPeriodAuditSaveBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时添加接口", notes = "课时添加接口")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result<AuthCourseChapterPeriodAuditSaveDTO> save(@RequestBody AuthCourseChapterPeriodAuditSaveBO authCourseChapterPeriodAuditSaveBO) {
		return biz.save(authCourseChapterPeriodAuditSaveBO);
	}

	/**
	 * 课时更新接口
	 *
	 * @param authCourseChapterPeriodAuditUpdateBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时更新接口", notes = "课时更新接口")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Result<Integer> update(@RequestBody AuthCourseChapterPeriodAuditUpdateBO authCourseChapterPeriodAuditUpdateBO) {
		return biz.update(authCourseChapterPeriodAuditUpdateBO);
	}

	/**
	 * 课时排序接口
	 *
	 * @param authCourseChapterPeriodAuditSortBO
	 * @author kyh
	 */
	@ApiOperation(value = "更新课时排序", notes = "更新课时排序")
	@RequestMapping(value = "/sort", method = RequestMethod.POST)
	public Result<Integer> sort(@RequestBody AuthCourseChapterPeriodAuditSortBO authCourseChapterPeriodAuditSortBO) {
		return biz.sort(authCourseChapterPeriodAuditSortBO);
	}

	/**
	 * 课时审核视频添加
	 *
	 * @param AuthCourseChapterPeriodAuditVideoSaveBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时审核视频添加", notes = "课时审核视频添加")
	@RequestMapping(value = "/video/save", method = RequestMethod.POST)
	public Result<Integer> videoSave(@RequestBody AuthCourseChapterPeriodAuditVideoSaveBO AuthCourseChapterPeriodAuditVideoSaveBO) {
		return biz.videoSave(AuthCourseChapterPeriodAuditVideoSaveBO);
	}

	/**
	 * 课时视频查看接口
	 *
	 * @param authCourseChapterPeriodAuditVideoViewBO
	 * @author kyh
	 */
	@ApiOperation(value = "课时视频查看接口", notes = "课时视频查看接口")
	@RequestMapping(value = "/video/view", method = RequestMethod.POST)
	public Result<AuthPeriodAuditVideoViewDTO> videoView(@RequestBody AuthCourseChapterPeriodAuditVideoViewBO authCourseChapterPeriodAuditVideoViewBO) {
		return biz.videoView(authCourseChapterPeriodAuditVideoViewBO);
	}

}
