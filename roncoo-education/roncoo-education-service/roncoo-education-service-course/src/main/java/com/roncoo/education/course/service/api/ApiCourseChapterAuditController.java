package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.CourseChapterAuditPageBO;
import com.roncoo.education.course.common.dto.CourseChapterAuditPageDTO;
import com.roncoo.education.course.service.api.biz.ApiCourseChapterAuditBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/course/api/course/chapter/audit")
public class ApiCourseChapterAuditController {

	@Autowired
	private ApiCourseChapterAuditBiz biz;

	/**
	 * 普通课程、直播、文库详情列出章节课时接口(管理员预览)
	 *
	 * @author kyh
	 */
	@ApiOperation(value = "普通课程、直播、文库详情列出章节课时接口(管理员预览)", notes = "普通课程、直播、文库详情根据课程ID列出章节课时接口(管理员预览)")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<CourseChapterAuditPageDTO>> listForPage(@RequestBody CourseChapterAuditPageBO courseChapterAuditPageBO) {
		return biz.listForPage(courseChapterAuditPageBO);

	}

}
