package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 直播记录表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseLiveLogPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 讲师用户编号
	 */
	@ApiModelProperty(value = "讲师用户编号", required = true)
	private Long lecturerUserNo;
	/**
	 * 是否未开播(1:未开播,2:直播结束)
	 */
	private Integer liveStatus = 2;
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private int pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数")
	private int pageSize = 20;
}
