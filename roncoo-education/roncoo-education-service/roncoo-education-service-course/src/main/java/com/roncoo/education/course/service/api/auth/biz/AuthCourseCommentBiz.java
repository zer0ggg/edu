package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserPageBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseCommentUserSaveBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseCommentUserPageDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseCommentUserSaveDTO;
import com.roncoo.education.course.service.dao.CourseCommentDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.Course;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseComment;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseCommentExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程评论
 *
 * @author Quanf
 */
@Component
public class AuthCourseCommentBiz extends BaseBiz {

    @Autowired
    private CourseCommentDao dao;
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 评论列表展示接口
     */
    public Result<Page<AuthCourseCommentUserPageDTO>> list(AuthCourseCommentUserPageBO bo) {
        CourseCommentExample example = new CourseCommentExample();
        Criteria c = example.createCriteria();
        if (bo.getCourseUserNo() != null) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getCourseUserNo());
            if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("用户异常");
            }
            c.andCourseUserNoEqualTo(bo.getCourseUserNo());
        }
        if (bo.getCommentUserNo() != null) {
            UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getCommentUserNo());
            if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
                return Result.error("用户异常");
            }
            c.andUserNoEqualTo(bo.getCommentUserNo());
        }
        c.andCourseCategoryEqualTo(bo.getCourseCategory());
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        example.setOrderByClause(" status_id desc, sort asc, gmt_create desc, id desc ");
        Page<CourseComment> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        logger.debug("------------------------------={}", page.getList());
        Page<AuthCourseCommentUserPageDTO> pageDto = PageUtil.transform(page, AuthCourseCommentUserPageDTO.class);
        for (AuthCourseCommentUserPageDTO dto : pageDto.getList()) {
            CourseComment comment = dao.getById(dto.getId());
            if (ObjectUtil.isNotNull(comment)) {
                if (StringUtils.hasText(comment.getContent())) {
                    dto.setContent(comment.getContent());
                }
            }
            // 查找被评论者信息
            UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getCourseUserNo());
            if (StringUtils.isEmpty(dto.getCourseNickname())) {
                if (StringUtils.isEmpty(userExtVO.getNickname())) {
                    dto.setCourseNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                } else {
                    dto.setCourseNickname(userExtVO.getNickname());
                }
            }
            dto.setCourseUserImg(userExtVO.getHeadImgUrl());
            // 查找评论者信息
            UserExtVO vo = feignUserExt.getByUserNo(dto.getUserNo());
            if (StringUtils.isEmpty(vo.getNickname())) {
                dto.setNickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
            } else {
                dto.setNickname(vo.getNickname());
            }
            dto.setUserImg(vo.getHeadImgUrl());

            dto.setCommentList(recursionList(dto.getId()));

        }
        return Result.success(pageDto);
    }


    /**
     * 展示第二级
     */
    private List<AuthCourseCommentUserPageDTO> recursionList(Long parentId) {
        List<AuthCourseCommentUserPageDTO> list = new ArrayList<>();
        List<CourseComment> courseCommentList = dao.listByParentIdAndStatusId(parentId, StatusIdEnum.YES.getCode());
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(courseCommentList)) {
            for (CourseComment courseComment : courseCommentList) {
                AuthCourseCommentUserPageDTO dto = BeanUtil.copyProperties(courseComment, AuthCourseCommentUserPageDTO.class);
                CourseComment comment = dao.getById(dto.getId());
                if (ObjectUtil.isNotNull(comment)) {
                    if (StringUtils.hasText(comment.getContent())) {
                        dto.setContent(comment.getContent());
                    }
                }
                // 查找被评论者信息
                UserExtVO userExtVO = feignUserExt.getByUserNo(dto.getCourseUserNo());
                if (StringUtils.isEmpty(dto.getCourseNickname())) {
                    if (StringUtils.isEmpty(userExtVO.getNickname())) {
                        dto.setCourseNickname(userExtVO.getMobile().substring(0, 3) + "****" + userExtVO.getMobile().substring(7));
                    } else {
                        dto.setCourseNickname(userExtVO.getNickname());
                    }
                }
                dto.setCourseUserImg(userExtVO.getHeadImgUrl());
                // 查找评论者信息
                UserExtVO vo = feignUserExt.getByUserNo(dto.getUserNo());
                if (StringUtils.isEmpty(vo.getNickname())) {
                    dto.setNickname(vo.getMobile().substring(0, 3) + "****" + vo.getMobile().substring(7));
                } else {
                    dto.setNickname(vo.getNickname());
                }
                dto.setUserImg(vo.getHeadImgUrl());
                list.add(dto);
            }
        }
        return list;

    }

    /**
     * 添加评论
     */
    public Result<AuthCourseCommentUserSaveDTO> save(AuthCourseCommentUserSaveBO bo) {
        // 被评论者用户信息
        UserExtVO userExtVO = feignUserExt.getByUserNo(bo.getCourseUserNo());
        if (ObjectUtil.isNull(userExtVO)) {
            return Result.error("被评论者用户教育信息不存在");
        }
        // 评论者用户信息
        UserExtVO vo = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(vo)) {
            return Result.error("评论者用户教育信息不存在");
        }
        // 根据课程ID查询课程信息
        Course course = courseDao.getById(bo.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("课程信息不存在");
        }
        // 添加评论
        CourseComment courseComment = new CourseComment();
        courseComment.setParentId(bo.getParentId());
        courseComment.setCourseId(bo.getCourseId());
        courseComment.setCourseName(course.getCourseName());
        courseComment.setCourseUserNo(bo.getCourseUserNo());
        courseComment.setUserNo(ThreadContext.userNo());
        courseComment.setContent(bo.getContent());
        courseComment.setUserIp(bo.getUserIp());
        courseComment.setUserTerminal(bo.getUserTerminal());
        courseComment.setCourseCategory(course.getCourseCategory());
        courseComment.setCourseNickname(userExtVO.getNickname());
        courseComment.setNickname(vo.getNickname());
        dao.save(courseComment);
        AuthCourseCommentUserSaveDTO dto = BeanUtil.copyProperties(courseComment, AuthCourseCommentUserSaveDTO.class);
        // 查找被评论者信息
        dto.setCourseUserImg(userExtVO.getHeadImgUrl());
        // 查找评论者信息
        dto.setUserImg(vo.getHeadImgUrl());
        return Result.success(dto);
    }

    /**
     * 删除评论
     */
    public Result<Integer> delete(AuthCourseCommentUserDeleteBO bo) {
        // 查询评论信息
        CourseComment courseComment = dao.getById(bo.getId());
        if (ObjectUtil.isNull(courseComment)) {
            return Result.error("该评论不存在");
        }
        // 本人删除
        if (!courseComment.getUserNo().equals(ThreadContext.userNo())) {
            return Result.error("没有权限");
        }
        // 查询博客信息
        Course course = courseDao.getById(bo.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("课程信息不存在");
        }
        courseComment.setStatusId(StatusIdEnum.NO.getCode());
        int resultNum = dao.updateById(courseComment);

        if (resultNum > 0) {
            // 只统计一级的评论，更新博客信息
            if (bo.getParentId() == 0) {
                // 删除该一级评论下的所有二级评论
                List<CourseComment> courseCommentList = dao.listByParentIdAndStatusId(bo.getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtils.isEmpty(courseCommentList)) {
                    return Result.success(resultNum);
                }
                for (CourseComment comment : courseCommentList) {
                    comment.setStatusId(StatusIdEnum.NO.getCode());
                    dao.updateById(comment);
                }
            }
        }
        return Result.success(resultNum);
    }

}
