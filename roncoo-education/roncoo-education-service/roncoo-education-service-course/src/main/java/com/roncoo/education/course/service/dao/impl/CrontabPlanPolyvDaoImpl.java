package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.CrontabPlanPolyvDao;
import com.roncoo.education.course.service.dao.impl.mapper.CrontabPlanPolyvMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CrontabPlanPolyvDaoImpl implements CrontabPlanPolyvDao {
    @Autowired
    private CrontabPlanPolyvMapper crontabPlanPolyvMapper;

    @Override
    public int save(CrontabPlanPolyv record) {
        record.setId(IdWorker.getId());
        return this.crontabPlanPolyvMapper.insertSelective(record);
    }

    @Override
    public int deleteById(Long id) {
        return this.crontabPlanPolyvMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(CrontabPlanPolyv record) {
        return this.crontabPlanPolyvMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByExampleSelective(CrontabPlanPolyv record, CrontabPlanPolyvExample example) {
        return this.crontabPlanPolyvMapper.updateByExampleSelective(record, example);
    }

    @Override
    public CrontabPlanPolyv getById(Long id) {
        return this.crontabPlanPolyvMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<CrontabPlanPolyv> listForPage(int pageCurrent, int pageSize, CrontabPlanPolyvExample example) {
        int count = this.crontabPlanPolyvMapper.countByExample(example);
        pageSize = PageUtil.checkPageSize(pageSize);
        pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
        int totalPage = PageUtil.countTotalPage(count, pageSize);
        example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
        example.setPageSize(pageSize);
        return new Page<CrontabPlanPolyv>(count, totalPage, pageCurrent, pageSize, this.crontabPlanPolyvMapper.selectByExample(example));
    }

	@Override
	public List<CrontabPlanPolyv> listForTimeAsc(CrontabPlanPolyvExample example) {
		  return this.crontabPlanPolyvMapper.selectByExample(example);
	}
}
