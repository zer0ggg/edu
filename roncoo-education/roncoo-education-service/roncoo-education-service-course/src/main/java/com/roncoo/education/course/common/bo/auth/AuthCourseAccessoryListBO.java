package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 附件下载
 *
 * @author kyh
 *
 */
@Data
@Accessors(chain = true)
public class AuthCourseAccessoryListBO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 关联ID
	 */
	@ApiModelProperty(value = "关联ID", required = true)
	private Long refId;

}
