package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 专区课程关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ZoneCourseListREQ", description="专区课程关联列表")
public class ZoneCourseListREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @NotNull(message = "专区编号不能为空")
    @ApiModelProperty(value = "专区编号",required = true)
    private Long zoneId;

    @ApiModelProperty(value = "位置")
    private Integer zoneLocation;

    @ApiModelProperty(value = "课程名称")
    private String courseName;
    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
