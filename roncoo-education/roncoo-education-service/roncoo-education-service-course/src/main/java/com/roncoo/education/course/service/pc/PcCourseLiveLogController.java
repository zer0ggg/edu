package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseLiveLogEditREQ;
import com.roncoo.education.course.common.req.CourseLiveLogListREQ;
import com.roncoo.education.course.common.req.CourseLiveLogSaveREQ;
import com.roncoo.education.course.common.resp.CourseLiveLogListRESP;
import com.roncoo.education.course.common.resp.CourseLiveLogViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseLiveLogBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 直播记录 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/live/log")
@Api(value = "course-直播记录", tags = {"course-直播记录"})
public class PcCourseLiveLogController {

    @Autowired
    private PcCourseLiveLogBiz biz;

    @ApiOperation(value = "直播记录列表", notes = "直播记录列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseLiveLogListRESP>> list(@RequestBody CourseLiveLogListREQ courseLiveLogListREQ) {
        return biz.list(courseLiveLogListREQ);
    }

    @ApiOperation(value = "直播记录添加", notes = "直播记录添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseLiveLogSaveREQ courseLiveLogSaveREQ) {
        return biz.save(courseLiveLogSaveREQ);
    }

    @ApiOperation(value = "直播记录查看", notes = "直播记录查看")
    @GetMapping(value = "/view")
    public Result<CourseLiveLogViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "直播记录修改", notes = "直播记录修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseLiveLogEditREQ courseLiveLogEditREQ) {
        return biz.edit(courseLiveLogEditREQ);
    }

    @ApiOperation(value = "直播记录删除", notes = "直播记录删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
