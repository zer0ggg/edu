package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfoExample;

public interface FileInfoDao {
    int save(FileInfo record);

    int deleteById(Long id);

    int updateById(FileInfo record);

    int updateByExampleSelective(FileInfo record, FileInfoExample example);

    FileInfo getById(Long id);

    Page<FileInfo> listForPage(int pageCurrent, int pageSize, FileInfoExample example);
    /**
     * 统计已用总空间
     *
     * @return
     */
    String count();


    /**
     * 根据文件类型统计已用总空间
     *
     * @param fileType
     * @return
     */
    String countByFileType(Integer fileType);

}