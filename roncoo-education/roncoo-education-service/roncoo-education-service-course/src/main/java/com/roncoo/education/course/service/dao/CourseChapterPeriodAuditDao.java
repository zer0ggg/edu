package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAuditExample;

import java.util.List;

public interface CourseChapterPeriodAuditDao {
	int save(CourseChapterPeriodAudit record);

	int deleteById(Long id);

	int updateById(CourseChapterPeriodAudit record);

	CourseChapterPeriodAudit getById(Long id);

	Page<CourseChapterPeriodAudit> listForPage(int pageCurrent, int pageSize, CourseChapterPeriodAuditExample example);

	/**
	 * 根据课程ID查询课时信息
	 *
	 * @param CourseId
	 */
	List<CourseChapterPeriodAudit> listByCourseId(Long CourseId);

	/**
	 * 根据章节ID修改课时排序
	 *
	 * @param sort
	 * @param periodId
	 */
	int updateSortByPeriodId(int sort, Long periodId);

	/**
	 * 根据视频Id查询课时审核信息
	 *
	 * @param videoId
	 * @author wuyun
	 */
	CourseChapterPeriodAudit getByVideoId(Long videoId);

	/**
	 * 根据课程编号删除课时信息
	 *
	 * @param courseId
	 * @return int
	 * @author wuyun
	 */
	int deleteByCourseId(Long courseId);

	/**
	 * 根据章节ID查询可用的课时信息集合
	 *
	 * @param chapterId
	 * @return statusId
	 * @author wuyun
	 */
	List<CourseChapterPeriodAudit> listByChapterIdAndStatusId(Long chapterId, Integer statusId);

	/**
	 * 根据审核状态、课时ID更新
	 *
	 * @param auditStatus
	 * @param periodId
	 * @author kyh
	 */
	int updateAuditStatusByPeriodId(Integer auditStatus, Long periodId);

	/**
	 * 根据视频ID获取课时信息
	 *
	 * @param videoId
	 * @author kyh
	 */
	List<CourseChapterPeriodAudit> listByVideoId(Long videoId);

	/**
	 * 根据视频ID、状态获取课时信息
	 *
	 * @param videoId
	 * @param statusId
	 * @author kyh
	 */
	List<CourseChapterPeriodAudit> listByVideoIdAndStatusId(Long videoId, Integer statusId);

	/**
	 * 根据videoId更新时长
	 * @param courseChapterPeriodAudit
	 * @return
	 */
	int updateByVideoId(CourseChapterPeriodAudit courseChapterPeriodAudit);

	/**
	 * 根据视频vid更新时长
	 * @param courseChapterPeriodAudit
	 * @return
	 */
	int updateByVid(CourseChapterPeriodAudit courseChapterPeriodAudit);
}
