package com.roncoo.education.course.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.enums.LiveStatusEnum;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.common.polyv.live.request.PLChannelConvertRequest;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.CrontabPlanPolyvDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;
import com.roncoo.education.course.service.feign.biz.FeignCallbackPolyvLiveBiz;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 定时任务-保利威视直播转点播处理
 *
 * @author LHR
 */
@Slf4j
@Component
public class PolyvLiveJob extends IJobHandler {

    @Autowired
    private CrontabPlanPolyvDao dao;
    @Autowired
    private CourseLiveLogDao courseLiveLogDao;
    @Autowired
    private IFeignSysConfig feignSysConfig;

    @Autowired
    private FeignCallbackPolyvLiveBiz feignCallbackPolyvLiveBiz;

    @Override
    @XxlJob(value = "polyvLiveJob")
    public ReturnT<String> execute(String param) throws Exception {

        XxlJobLogger.log("开始执行[保利威视直播转点播处理]");
        try {
            // 处理视频直播转点存
            handleScheduledTasks();
        } catch (Exception e) {
            log.error("定时任务-保利威视转存处理-执行出错", e);
            return new ReturnT<String>(IJobHandler.FAIL.getCode(), "执行[保利威视转存处理]--处理异常");
        }

        log.warn("保利威视转存处理-定时任务完成");
        XxlJobLogger.log("结束执行[保利威视直播转点播处理]");
        return SUCCESS;
    }

    private void handleScheduledTasks() {
        CrontabPlanPolyvExample example = new CrontabPlanPolyvExample();
        example.setOrderByClause(" id asc ");
        List<CrontabPlanPolyv> list = dao.listForTimeAsc(example);
        if (CollectionUtil.isNotEmpty(list)) {
            ConfigPolyvVO configPolyvVO =  feignSysConfig.getPolyv();
            if (ObjectUtil.isNull(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvUseid()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
                log.error("保利威参数配置错误，configPolyvVO={}", configPolyvVO);
                return;
            }

            CrontabPlanPolyv vo = list.get(0);
            // 根据频道号和状态查找记录中当前状态的最后一条
            CourseLiveLog courseLiveLog = courseLiveLogDao.getByChannelIdAndSessionId(vo.getChannelId(), vo.getSessionId());
            if (ObjectUtil.isNull(courseLiveLog)) {
                log.error("获取不到当前频道的直播记录");
            }

            // 转存成点播
            PLChannelConvertRequest request = new PLChannelConvertRequest();
            request.setAppId(configPolyvVO.getPolyvAppID());
            request.setUserId(configPolyvVO.getPolyvUseid());
            request.setChannelId(vo.getChannelId());
            request.setFileUrl(vo.getPlayback());
            request.setFileName(vo.getPeriodName());// 转存后为直播课时的名称
            request.setToPlayList("Y");// 存放到回放列表
            String result = PLChannelUtil.convert(request, configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvAppSecret());
            if ("success".equals(result)) {
                // 转存成功后删除定时任务信息
                dao.deleteById(vo.getId());
                if ("true".equals(feignSysConfig.getByConfigKey(SysConfigConstants.POLYV_LIVE_BACKUP).getConfigValue())) {
                    feignCallbackPolyvLiveBiz.uploadOSS(vo.getPlayback(), vo.getPeriodName(), courseLiveLog);
                }
                return;
            }
            // 如果转存失败，且失败次数小于5，则统计失败次数
            if (StringUtils.isEmpty(result) && vo.getFailCount() < 5) {
                vo.setFailCount(vo.getFailCount() + 1);
                dao.updateById(vo);
            }
            // 如果失败次数错误5次，则直接处理为成功
            if (StringUtils.isEmpty(result) && vo.getFailCount() >= 5) {
                // 更改状态为结束
                courseLiveLog.setLiveStatus(LiveStatusEnum.END.getCode());
                courseLiveLogDao.updateById(courseLiveLog);
                // 删除当前定时任务
                dao.deleteById(vo.getId());
            }
        }
    }
}
