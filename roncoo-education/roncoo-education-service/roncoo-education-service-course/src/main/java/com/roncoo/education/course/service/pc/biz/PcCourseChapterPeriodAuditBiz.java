package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.AuditStatusEnum;
import com.roncoo.education.common.core.enums.CourseCategoryEnum;
import com.roncoo.education.common.core.enums.LiveStatusEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditEditREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditListREQ;
import com.roncoo.education.course.common.req.CourseChapterPeriodAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditListRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditSaveRESP;
import com.roncoo.education.course.common.resp.CourseChapterPeriodAuditViewRESP;
import com.roncoo.education.course.service.dao.CourseAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAuditExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Component
public class PcCourseChapterPeriodAuditBiz extends BaseBiz {

    @Autowired
    private CourseChapterPeriodAuditDao dao;

    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseChapterAuditDao chapterAuditDao;
    @Autowired
    private CourseChapterPeriodAuditDao periodAuditDao;

    /**
    * 课时信息-审核列表
    *
    * @param req 课时信息-审核分页查询参数
    * @return 课时信息-审核分页查询结果
    */
    public Result<Page<CourseChapterPeriodAuditListRESP>> list(CourseChapterPeriodAuditListREQ req) {
        CourseChapterPeriodAuditExample example = new CourseChapterPeriodAuditExample();
        Criteria c = example.createCriteria();
        if (req.getChapterId() != null) {
            c.andChapterIdEqualTo(req.getChapterId());
        }
        if (req.getStatusId() != null) {
            c.andStatusIdEqualTo(req.getStatusId());
        }
        if (StringUtils.hasText(req.getPeriodName())) {
            c.andPeriodNameLike(PageUtil.like(req.getPeriodName()));
        }
        example.setOrderByClause("sort asc, id asc");
        Page<CourseChapterPeriodAudit> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseChapterPeriodAuditListRESP> respPage = PageUtil.transform(page, CourseChapterPeriodAuditListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课时信息-审核添加
    *
    * @param req 课时信息-审核
    * @return 添加结果
    */
    @Transactional(rollbackFor = Exception.class)
    public Result<CourseChapterPeriodAuditSaveRESP> save(CourseChapterPeriodAuditSaveREQ req) {
        if (req.getChapterId() == null) {
            return Result.error("chapterId不能为空");
        }
        if (org.apache.commons.lang.StringUtils.isEmpty(req.getPeriodName())) {
            return Result.error("课时名称不能为空");
        }
        if (req.getIsFree() == null) {
            return Result.error("isFree不能为空");
        }

        CourseChapterAudit chapterAudit = chapterAuditDao.getById(req.getChapterId());
        if (ObjectUtil.isNull(chapterAudit)) {
            return Result.error("找不到章节信息");
        }
        CourseAudit course = courseAuditDao.getById(chapterAudit.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }

        CourseChapterPeriodAudit record = BeanUtil.copyProperties(req, CourseChapterPeriodAudit.class);
        if (org.apache.commons.lang.StringUtils.isNotEmpty(req.getStartTime())) {
            record.setStartTime(DateUtil.parse(req.getStartTime()));
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(req.getEndTime())) {
            record.setEndTime(DateUtil.parse(req.getEndTime()));

        }
        record.setCourseId(chapterAudit.getCourseId());
        record.setAuditStatus(AuditStatusEnum.WAIT.getCode());
        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            record.setLiveStatus(LiveStatusEnum.NOT.getCode());
        }
        int results = periodAuditDao.save(record);
        if (results > 0) {
            // 更新课程审核表、章节审核表的审核状态为待审核
            chapterAuditDao.updateAuditStatusByChapterNo(AuditStatusEnum.WAIT.getCode(), record.getChapterId());
            courseAuditDao.updateAuditStatusBycourseId(AuditStatusEnum.WAIT.getCode(), record.getCourseId());
            // 再复制回dto进行返回
            CourseChapterPeriodAuditSaveRESP dto = BeanUtil.copyProperties(record, CourseChapterPeriodAuditSaveRESP.class);
            return Result.success(dto);
        }
        return Result.error(ResultEnum.COURSE_SAVE_FAIL);
    }


    /**
    * 课时信息-审核查看
    *
    * @param id 主键ID
    * @return 课时信息-审核
    */
    public Result<CourseChapterPeriodAuditViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseChapterPeriodAuditViewRESP.class));
    }


    /**
    * 课时信息-审核修改
    *
    * @param courseChapterPeriodAuditEditREQ 课时信息-审核修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseChapterPeriodAuditEditREQ courseChapterPeriodAuditEditREQ) {
        CourseChapterPeriodAudit record = BeanUtil.copyProperties(courseChapterPeriodAuditEditREQ, CourseChapterPeriodAudit.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课时信息-审核删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
