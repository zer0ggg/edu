package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 文库推荐
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ResourceRecommendSaveBatchREQ", description = "文库推荐批量添加")
public class ResourceRecommendSaveBatchREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文库ids", required = true)
    private String resourceIds;
}
