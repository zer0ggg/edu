package com.roncoo.education.course.service.api.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.RefTypeEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.ArrayListUtil;
import com.roncoo.education.course.common.bo.CourseChapterAuditPageBO;
import com.roncoo.education.course.common.dto.CourseAccessoryDTO;
import com.roncoo.education.course.common.dto.CourseChapterAuditPageDTO;
import com.roncoo.education.course.common.dto.CourseChapterPeriodAuditListDTO;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.CourseAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApiCourseChapterAuditBiz {

	@Autowired
	private CourseAuditDao courseAuditDao;
	@Autowired
	private CourseChapterAuditDao dao;
	@Autowired
	private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
	@Autowired
	private CourseAccessoryDao courseAccessoryDao;

	public Result<Page<CourseChapterAuditPageDTO>> listForPage(CourseChapterAuditPageBO bo) {
		if (bo.getCourseId() == null) {
			return Result.error("课程ID不能为空");
		}
		CourseAudit course = courseAuditDao.getById(bo.getCourseId());
		if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
			return Result.error("找不到该课程信息");
		}
		CourseChapterAuditExample example = new CourseChapterAuditExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(bo.getCourseId());
		c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
		example.setOrderByClause("sort asc,id asc");
		Page<CourseChapterAudit> page = dao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		Page<CourseChapterAuditPageDTO> listForPage = PageUtil.transform(page, CourseChapterAuditPageDTO.class);
		for (CourseChapterAuditPageDTO dto : listForPage.getList()) {
			// 获取课时信息
			List<CourseChapterPeriodAudit> list = courseChapterPeriodAuditDao.listByChapterIdAndStatusId(dto.getId(), StatusIdEnum.YES.getCode());
			if (CollectionUtil.isNotEmpty(list)) {
				List<CourseChapterPeriodAuditListDTO> periodList = ArrayListUtil.copy(list, CourseChapterPeriodAuditListDTO.class);
				// 迭代获取课时题目图片、答案图片、附件附件
				period(periodList);
				// 返回课时集合
				dto.setPeriodList(periodList);
			}
		}
		return Result.success(listForPage);
	}

	private void period(List<CourseChapterPeriodAuditListDTO> periodList) {
		for (CourseChapterPeriodAuditListDTO periodDto : periodList) {
			// 课时附件信息
			List<CourseAccessory> periodAccessoryList = courseAccessoryDao.listByRefIdAndRefTypeAndStatusId(periodDto.getId(), RefTypeEnum.PERIOD.getCode(), StatusIdEnum.YES.getCode());
			if (CollectionUtil.isNotEmpty(periodAccessoryList)) {
				periodDto.setAccessoryList(ArrayListUtil.copy(periodAccessoryList, CourseAccessoryDTO.class));
			}
		}
	}

}
