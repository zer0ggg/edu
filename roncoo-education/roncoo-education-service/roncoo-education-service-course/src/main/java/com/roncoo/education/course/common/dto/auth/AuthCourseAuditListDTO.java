package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseAuditListDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	@ApiModelProperty(value = "状态(1:正常，0:禁用)")
	private Integer statusId;

	@ApiModelProperty(value = "课程名称")
	private String courseName;

	@ApiModelProperty(value = "课程封面")
	private String courseLogo;

	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;

	@ApiModelProperty(value = "原价")
	private BigDecimal courseOriginal;

	@ApiModelProperty(value = "优惠价")
	private BigDecimal courseDiscount;

	@ApiModelProperty(value = "是否上架(1:上架，0:下架)")
	private Integer isPutaway;

	@ApiModelProperty(value = "课程分类：1普通,2直播,试卷")
	private Integer courseCategory;

	@ApiModelProperty(value = "章节ID(课程分类为试卷时)")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long chapterId;

	@ApiModelProperty(value = "审核状态(0:待审核,1:审核通过,2:审核不通过)")
	private Integer auditStatus;

	@ApiModelProperty(value = "是否可删除（1:可删除，0:不可删除）")
	private Integer isDelete;

    @ApiModelProperty(value = "排序")
    private Integer sort;

	@ApiModelProperty(value = "课程试卷关联主键id")
	private Long courseExamRefId;

	@ApiModelProperty(value = "试卷ID")
	private Long examId;

	@ApiModelProperty(value = "试卷名称")
	private String examName;
}
