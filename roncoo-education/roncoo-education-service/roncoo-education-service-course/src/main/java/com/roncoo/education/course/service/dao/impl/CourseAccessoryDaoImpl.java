package com.roncoo.education.course.service.dao.impl;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.course.service.dao.CourseAccessoryDao;
import com.roncoo.education.course.service.dao.impl.mapper.CourseAccessoryMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@Repository
public class CourseAccessoryDaoImpl implements CourseAccessoryDao {
	@Autowired
	private CourseAccessoryMapper courseaccessoryMapper;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int save(CourseAccessory record) {
		record.setId(IdWorker.getId());
		return this.courseaccessoryMapper.insertSelective(record);
	}

	@Override
	public int deleteById(Long id) {
		return this.courseaccessoryMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateById(CourseAccessory record) {
		return this.courseaccessoryMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByExampleSelective(CourseAccessory record, CourseAccessoryExample example) {
		return this.courseaccessoryMapper.updateByExampleSelective(record, example);
	}

	@Override
	public CourseAccessory getById(Long id) {
		return this.courseaccessoryMapper.selectByPrimaryKey(id);
	}

	@Override
	public Page<CourseAccessory> listForPage(int pageCurrent, int pageSize, CourseAccessoryExample example) {
		int count = this.courseaccessoryMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<>(count, totalPage, pageCurrent, pageSize, this.courseaccessoryMapper.selectByExample(example));
	}

	@Override
	public List<CourseAccessory> listByRefIdAndRefTypeAndStatusId(Long refId, Integer refType, Integer statusId) {
		CourseAccessoryExample example = new CourseAccessoryExample();
		Criteria c = example.createCriteria();
		c.andRefIdEqualTo(refId);
		c.andRefTypeEqualTo(refType);
		c.andStatusIdEqualTo(statusId);
		return this.courseaccessoryMapper.selectByExample(example);
	}

	@Override
	public List<CourseAccessory> listByRefIdAndStatusId(Long refId, Integer statusId) {
		CourseAccessoryExample example = new CourseAccessoryExample();
		Criteria c = example.createCriteria();
		c.andRefIdEqualTo(refId);
		c.andStatusIdEqualTo(statusId);
		return this.courseaccessoryMapper.selectByExample(example);
	}

	@Override
	public int totalDownload() {
		int count = 0;
		Map<String, Object> map;
		String sql = "select sum(download_count) as count from course_accessory";
		map = jdbcTemplate.queryForMap(sql);
		if (!StringUtils.isEmpty(map.get("count"))) {
			count = Integer.parseInt(String.valueOf(map.get("count")));
		}
		return count;
	}

	@Override
	public int courseDownload(Integer courseCategory) {
		int count = 0;
		Map<String, Object> map;
		String sql = "select sum(download_count) as count from course_accessory where course_category = ?";
		map = jdbcTemplate.queryForMap(sql, courseCategory);
		if (!StringUtils.isEmpty(map.get("count"))) {
			count = Integer.parseInt(String.valueOf(map.get("count")));
		}
		return count;
	}

	@Override
	public List<CourseAccessory> sumByAll(CourseAccessory courseAccessory) {
		if (courseAccessory.getCourseCategory() != null) {
			String sql = "select sum(download_count) as downloadCount, ref_type as refType, ref_name as refName from course_accessory where course_category = ? group by ref_id order by downloadCount desc limit 0,5";
			return queryForObjectList(sql, CourseAccessory.class, courseAccessory.getCourseCategory());
		}
		String sql = "select sum(download_count) as downloadCount, ref_type as refType, ref_name as refName from course_accessory group by ref_id order by downloadCount desc limit 0,5";
		return queryForObjectList(sql, CourseAccessory.class);
	}

	private <T> List<T> queryForObjectList(String sql, Class<T> clazz, Object... args) {
		return jdbcTemplate.query(sql, args, new BeanPropertyRowMapper<T>(clazz));
	}
}
