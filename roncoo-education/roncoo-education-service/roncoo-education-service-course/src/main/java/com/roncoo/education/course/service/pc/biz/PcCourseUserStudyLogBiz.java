package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.DateUtil;
import com.roncoo.education.course.common.req.CourseUserStudyLogListREQ;
import com.roncoo.education.course.common.req.CourseUserStudyLogStatisticalREQ;
import com.roncoo.education.course.common.resp.CourseUserStudyLogListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyLogStatisticalListRESP;
import com.roncoo.education.course.common.resp.CourseUserStudyLogStatisticalRESP;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.UserOrderCourseRefDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.qo.UserExtQO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 课程用户学习日志
 *
 * @author wujing
 */
@Component
public class PcCourseUserStudyLogBiz extends BaseBiz {

    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseDao courseDao;

    @Autowired
    private IFeignUserExt feignUserExt;

    /**
     * 列出学员学习记录
     *
     * @param req
     * @return
     */
    public Result<Page<CourseUserStudyLogListRESP>> list(CourseUserStudyLogListREQ req) {
        UserOrderCourseRefExample example = new UserOrderCourseRefExample();
        UserOrderCourseRefExample.Criteria c = example.createCriteria();
        if (req.getUserNo() != null) {
            c.andUserNoEqualTo(req.getUserNo());
        }
        if (req.getCourseId() != null) {
            c.andCourseIdEqualTo(req.getCourseId());
        }
        if (StringUtils.hasText(req.getCourseName())) {
            List<Course> courseList = courseDao.listByCourseName(req.getCourseName());
            if (CollectionUtil.isEmpty(courseList)) {
                return Result.success(new Page<>());
            } else {
                Set<Long> courseIds = courseList.stream().map(Course::getId).collect(Collectors.toSet());
                c.andCourseIdIn(new ArrayList<>(courseIds));
            }
        }
        if (StringUtils.hasText(req.getMobile())) {
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setMobile(req.getMobile());
            UserExtVO userExtVO = feignUserExt.getByMobile(userExtQO);
            if (ObjectUtil.isNotNull(userExtVO)) {
                c.andUserNoEqualTo(req.getUserNo());
            }
        }
        Page<UserOrderCourseRef> page = userOrderCourseRefDao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseUserStudyLogListRESP> respPage = PageUtil.transform(page, CourseUserStudyLogListRESP.class);
        if (respPage.getList().isEmpty()) {
            return Result.success(respPage);
        }
        Set<Long> courseIds = respPage.getList().stream().map(CourseUserStudyLogListRESP::getCourseId).collect(Collectors.toSet());
        // 根据课程ID集合获取课程信息
        List<Course> courseList = courseDao.listByIds(new ArrayList<>(courseIds));
        if (CollectionUtil.isEmpty(courseList)) {
            return Result.success(respPage);
        }
        Map<Long, String> courseListMap = courseList.stream().collect(Collectors.toMap(Course::getId, item -> item.getCourseName()));

        // 课时ID集合
        Set<Long> periodIds = respPage.getList().stream().map(CourseUserStudyLogListRESP::getLastStudyPeriodId).collect(Collectors.toSet());
        // 根据课时ID集合获取课时信息
        Map<Long, String> periodListMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(periodIds)) {
            List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByIds(new ArrayList<>(periodIds));
            if (CollectionUtil.isNotEmpty(periodList)) {
                periodListMap = periodList.stream().collect(Collectors.toMap(CourseChapterPeriod::getId, item -> item.getPeriodName()));
            }
        }

        Set<Long> userNoIds = respPage.getList().stream().map(CourseUserStudyLogListRESP::getUserNo).collect(Collectors.toSet());
        UserExtQO userExtQO = new UserExtQO();
        userExtQO.setUserNos(new ArrayList<>(userNoIds));
        // 根据用户编号获取用户信息
        List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
        Map<Long, String> userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item.getMobile()));
        for (CourseUserStudyLogListRESP resp : respPage.getList()) {
            String courseName = courseListMap.get(resp.getCourseId());
            if (StringUtils.hasText(courseName)) {
                resp.setCourseName(courseName);
            }
            String mobile = userExtVOMap.get(resp.getUserNo());
            if (StringUtils.hasText(mobile)) {
                resp.setMobile(mobile);
            }
            if (resp.getStudyLength() != null) {
                resp.setStringStudyLength(resp.getStudyLength().setScale(6, RoundingMode.HALF_UP).stripTrailingZeros().toString());
            }
            if (resp.getLastStudyPeriodId() != null && StringUtils.hasText(periodListMap.get(resp.getLastStudyPeriodId()))) {
                resp.setLastStudyPeriodName(periodListMap.get(resp.getLastStudyPeriodId()));
            }
            if (resp.getStudyProcess() > 100) {
                resp.setStudyProcess(100);
            }
        }
        return Result.success(respPage);
    }

    public Result<CourseUserStudyLogStatisticalRESP> statistical(CourseUserStudyLogStatisticalREQ req) {
        CourseUserStudyLogStatisticalRESP resp = new CourseUserStudyLogStatisticalRESP();

        // 总学习数
        String numberTotal = userOrderCourseRefDao.numbergetByUserNoAndCourseId(req.getUserNo(), req.getCourseId(), null, null);
        resp.setNumberTotal(numberTotal);
        // 本周总学习数
        String numberWeeks = userOrderCourseRefDao.numbergetByUserNoAndCourseId(req.getUserNo(), req.getCourseId(), DateUtil.getWeekStart() + " 00:00:00", DateUtil.getWeekEnd() + " 23:59:59");
        resp.setNumberWeeks(numberWeeks);

        // 获取周观看时长
        String weeksWatchLength = userOrderCourseRefDao.getWatchLengthTotalUserNoAndCourseIdAndGmtModified(req.getUserNo(), req.getCourseId(), DateUtil.getWeekStart() + " 00:00:00", DateUtil.getWeekEnd() + " 23:59:59");
        if (StringUtils.hasText(weeksWatchLength) && weeksWatchLength.contains(".")) {
            int indexOf = weeksWatchLength.indexOf(".");
            weeksWatchLength = weeksWatchLength.substring(0, indexOf);
        }
        resp.setWeeksWatchLength(cn.hutool.core.date.DateUtil.secondToTime(Integer.parseInt(weeksWatchLength)));
        // 获取总观看时长
        String watchLengthTotal = userOrderCourseRefDao.getWatchLengthTotalUserNoAndCourseIdAndGmtModified(req.getUserNo(), req.getCourseId(), null, null);
        if (StringUtils.hasText(watchLengthTotal) && watchLengthTotal.contains(".")) {
            int indexOf = watchLengthTotal.indexOf(".");
            watchLengthTotal = watchLengthTotal.substring(0, indexOf);
        }
        resp.setWatchLengthTotal(cn.hutool.core.date.DateUtil.secondToTime(Integer.parseInt(watchLengthTotal)));

        // 观看排行榜
        List<String> xAxis = new ArrayList<>();

        // 列表
        List<CourseUserStudyLogStatisticalListRESP> listRESPS = new ArrayList<>();
        List<UserOrderCourseRef> list = userOrderCourseRefDao.listRankingUserNoAndCourseId(req.getUserNo(), req.getCourseId(), req.getRanking());
        if (list.isEmpty()) {
            return Result.success(resp);
        }
        Map<Long, String> courseNameListMap = new HashMap<>();
        if (req.getUserNo() != null) {
            Set<Long> courseIds = list.stream().map(UserOrderCourseRef::getCourseId).collect(Collectors.toSet());
            // 根据课程ID集合获取课程信息
            List<Course> courseVOList = courseDao.listByIds(new ArrayList<>(courseIds));
            courseNameListMap = courseVOList.stream().collect(Collectors.toMap(Course::getId, item -> item.getCourseName()));
        }

        // 获取用户信息
        Map<Long, String> userExtVOMap = new HashMap<>();
        if (req.getCourseId() != null) {
            Set<Long> userNoIds = list.stream().map(UserOrderCourseRef::getUserNo).collect(Collectors.toSet());
            UserExtQO userExtQO = new UserExtQO();
            userExtQO.setUserNos(new ArrayList<>(userNoIds));
            // 根据用户编号获取用户信息
            List<UserExtVO> userExtVOList = feignUserExt.listByUserNos(userExtQO);
            userExtVOMap = userExtVOList.stream().collect(Collectors.toMap(UserExtVO::getUserNo, item -> item.getMobile()));
        }

        for (UserOrderCourseRef ref : list) {
            CourseUserStudyLogStatisticalListRESP courseUserStudyLogStatisticalListRESP = new CourseUserStudyLogStatisticalListRESP();
            if (req.getCourseId() != null) {
                String mobile = userExtVOMap.get(ref.getUserNo());
                if (StringUtils.hasText(mobile)) {
                    courseUserStudyLogStatisticalListRESP.setName(mobile);
                }
            } else {
                String courseName = courseNameListMap.get(ref.getCourseId());
                if (StringUtils.hasText(courseName)) {
                    courseUserStudyLogStatisticalListRESP.setName(courseName);
                }
            }
            courseUserStudyLogStatisticalListRESP.setCourseLength(ref.getCourseLength());
            if (ref.getStudyLength() != null) {
                courseUserStudyLogStatisticalListRESP.setValue(ref.getStudyLength().setScale(6, RoundingMode.HALF_UP).stripTrailingZeros().toString());
            }
            courseUserStudyLogStatisticalListRESP.setStudyProcess(ref.getStudyProcess());

            listRESPS.add(courseUserStudyLogStatisticalListRESP);
            xAxis.add(courseUserStudyLogStatisticalListRESP.getName());
        }
        resp.setList(listRESPS);
        resp.setAxis(xAxis);
        return Result.success(resp);
    }
}
