/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.core.tools.StrUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.FileInfoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.FileInfo;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * 上传接口
 *
 * @author wuyun
 */
@Component
public class ApiUploadBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private FileInfoDao fileInfoDao;

    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseVideoDao courseVideoDao;

    /**
     * 上传视频接口
     *
     * @author wuyun
     */
    public Result<String> uploadVideo(MultipartFile videoFile) {
        logger.warn("上传视频");
        // 视频上传
        if (videoFile == null || videoFile.isEmpty()) {
            return Result.error("请选择视频进行上传");
        }

        // 获取上传文件的原名
        String fileName = videoFile.getOriginalFilename();
        boolean fileStatus = true;
        List<String> fileTypes = Arrays.asList("avi", "mp4", "flv", "mpg", "mov", "asf", "3gp", "f4v", "wmv", "x-ms-wmv\n");
        for (String filetype : fileTypes) {
            // 上传文件的原名+小写+后缀
            if (fileName.toLowerCase().endsWith(filetype)) {
                fileStatus = false;
                break;
            }
        }
        if (fileStatus) {
            return Result.error("上传的视频类型不正确");
        }

        // 当作存储到本地的文件名，方便定时任务的处理
        Long videoId = IdWorker.getId();

        // 1、上传到本地
        File targetFile = new File(SystemUtil.VIDEO_PATH + videoId.toString() + "." + StrUtil.getSuffix(fileName));
        // targetFile.setLastModified(System.currentTimeMillis());

        // 判断文件目录是否存在，不存在就创建文件目录
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        try {
            videoFile.transferTo(targetFile);
        } catch (Exception e) {
            logger.error("上传到本地失败", e);
            return Result.error("上传文件出错，请重新上传");
        }

        // 新增课程视频表信息
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setId(videoId);
        courseVideo.setGmtCreate(null);
        courseVideo.setGmtModified(null);
        courseVideo.setVideoName(fileName);
        courseVideo.setVodPlatform(VodPlatformEnum.POLYV.getCode());
        courseVideo.setVideoStatus(VideoStatusEnum.WAIT.getCode());
        int result = courseVideoDao.save(courseVideo);

        if (result > 0) {
            CALLBACK_EXECUTOR.execute(() -> {
                // 获取系统配置信息
                ConfigPolyvVO sys = feignSysConfig.getPolyv();
                String videoVid = PolyvUtil.sdkUploadFile(targetFile.getName(), fileName, SystemUtil.VIDEO_PATH, VideoTagEnum.COURSE.getCode(), fileName, sys.getPolyvUseid(), sys.getPolyvSecretkey());
                if (videoVid == null) {
                    // 上传异常，不再进行处理，定时任务会继续进行处理
                    logger.warn("上传视频失败!");
                    return;
                }
                courseVideo.setVideoVid(videoVid);
                courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
                courseVideoDao.updateById(courseVideo);

                // 记录文件信息
                FileInfo fileInfo = new FileInfo();
                fileInfoDao.save(fileInfo);

                if (String.valueOf(IsBackupEnum.YES.getCode()).equals(feignSysConfig.getByConfigKey(SysConfigConstants.IS_BACKUP_ALIYUN).getConfigValue())) {
                    // 3、异步上传到阿里云
                    String videoOssId = AliyunUtil.uploadVideo(PlatformEnum.COURSE, targetFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
                    courseVideo.setVideoOssId(videoOssId);
                    courseVideoDao.updateById(courseVideo);
                    fileInfo.setFileUrl(videoOssId);
                    fileInfoDao.updateById(fileInfo);
                }


                // 更新课时审核视频信息
                List<CourseChapterPeriodAudit> periodAuditList = courseChapterPeriodAuditDao.listByVideoId(videoId);
                if (CollectionUtil.isNotEmpty(periodAuditList)) {
                    for (CourseChapterPeriodAudit periodAudit : periodAuditList) {
                        periodAudit.setVideoVid(courseVideo.getVideoVid());
                        periodAudit.setIsVideo(IsVideoEnum.YES.getCode());
                        courseChapterPeriodAuditDao.updateById(periodAudit);
                    }
                }

                // 更新课时视频信息
                List<CourseChapterPeriod> periodList = courseChapterPeriodDao.listByVideoId(videoId);
                if (CollectionUtil.isNotEmpty(periodList)) {
                    for (CourseChapterPeriod period : periodList) {
                        period.setVideoVid(courseVideo.getVideoVid());
                        period.setIsVideo(IsVideoEnum.YES.getCode());
                        courseChapterPeriodDao.updateById(period);
                    }
                }

                fileInfo.setFileName(courseVideo.getVideoName());
                fileInfo.setFileType(FileTypeEnum.VIDEO.getCode());
                fileInfo.setFileSize(videoFile.getSize());
                fileInfoDao.updateById(fileInfo);

                // 4、成功删除本地文件
                if (targetFile.isFile() && targetFile.exists()) {
                    if (!targetFile.delete()) {
                        logger.error("删除本地文件失败");
                    }
                }
            });
        } else {
            return Result.error("系统异常，请重试");
        }
        return Result.success(String.valueOf(videoId));
    }

    /**
     * 上传图片接口
     *
     * @author wuyun
     */
    public Result<String> uploadPic(MultipartFile picFile) {
        if (ObjectUtil.isNotNull(picFile) && !picFile.isEmpty()) {
            // 上传到阿里云
            String fileUrl = AliyunUtil.uploadPic(PlatformEnum.COURSE, picFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
            // 记录文件信息
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(picFile.getName());
            fileInfo.setFileUrl(fileUrl);
            fileInfo.setFileType(FileTypeEnum.PICTURE.getCode());
            fileInfo.setFileSize(picFile.getSize());
            fileInfoDao.save(fileInfo);
            return Result.success(fileUrl);
        }
        return Result.error("请选择上传的图片");
    }

    /**
     * 上传文档接口
     *
     * @author wuyun
     */
    public Result<String> uploadDoc(MultipartFile docFile) {
        if (ObjectUtil.isNotNull(docFile) && !docFile.isEmpty()) {
            // 上传到阿里云
            String fileUrl = AliyunUtil.uploadDoc(PlatformEnum.COURSE, docFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
            // 记录文件信息
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(docFile.getName());
            fileInfo.setFileUrl(fileUrl);
            fileInfo.setFileType(FileTypeEnum.ACCESSORY.getCode());
            fileInfo.setFileSize(docFile.getSize());
            fileInfoDao.save(fileInfo);
            return Result.success(fileUrl);
        }
        return Result.error("请选择上传的文件");

    }

}
