package com.roncoo.education.course.service.pc.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.IsFreeEnum;
import com.roncoo.education.common.core.enums.IsPutawayEnum;
import com.roncoo.education.common.core.enums.PlatformEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.course.common.es.EsCourse;
import com.roncoo.education.course.common.req.*;
import com.roncoo.education.course.common.resp.CourseFindBackRESP;
import com.roncoo.education.course.common.resp.CourseListRESP;
import com.roncoo.education.course.common.resp.CourseViewRESP;
import com.roncoo.education.course.feign.qo.CourseQO;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseExample.Criteria;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.vo.LecturerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程信息
 *
 * @author wujing
 */
@Component
public class PcCourseBiz extends BaseBiz {

    @Autowired
    private CourseDao dao;
    @Autowired
    private CourseCategoryDao courseCategoryDao;
    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private CourseAuditDao courseAuditDao;
    @Autowired
    private CourseIntroduceAuditDao courseIntroduceAuditDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 课程信息列表
     *
     * @param courseListREQ 课程信息分页查询参数
     * @return 课程信息分页查询结果
     */
    public Result<Page<CourseListRESP>> list(CourseListREQ courseListREQ) {
        CourseExample example = new CourseExample();
        Criteria c = example.createCriteria();

        if (CollectionUtil.isNotEmpty(courseListREQ.getNotInCourseIdList())) {
            c.andIdNotIn(courseListREQ.getNotInCourseIdList());
        }
        if (courseListREQ.getCategoryId1() != null) {
            c.andCategoryId1EqualTo(courseListREQ.getCategoryId1());
        }
        if (!StringUtils.isEmpty(courseListREQ.getCourseName())) {
            c.andCourseNameLike(PageUtil.like(courseListREQ.getCourseName()));
        }
        if (courseListREQ.getStatusId() != null) {
            c.andStatusIdEqualTo(courseListREQ.getStatusId());
        }
        if (courseListREQ.getCourseCategory() != null) {
            c.andCourseCategoryEqualTo(courseListREQ.getCourseCategory());
        }
        if (courseListREQ.getIsFree() != null) {
            c.andIsFreeEqualTo(courseListREQ.getIsFree());
        }
        if (courseListREQ.getIsPutaway() != null) {
            c.andIsPutawayEqualTo(courseListREQ.getIsPutaway());
        }
        example.setOrderByClause(" status_id desc, is_putaway desc, course_sort asc, id desc ");
        Page<Course> page = dao.listForPage(courseListREQ.getPageCurrent(), courseListREQ.getPageSize(), example);
        Page<CourseListRESP> respPage = PageUtil.transform(page, CourseListRESP.class);
        Map<Long, CourseCategory> courseCategoryCacheMap = new HashMap<>();
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.DOMAIN);
        for (CourseListRESP resp : respPage.getList()) {
            resp.setDomain(sysConfigVO.getConfigValue());
            if (resp.getCategoryId1() != null && resp.getCategoryId1() != 0) {
                CourseCategory courseCategory = getCourseCategoryById(courseCategoryCacheMap, resp.getCategoryId1());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName1(courseCategory.getCategoryName());
                }
            }
            if (resp.getCategoryId2() != null && resp.getCategoryId2() != 0) {
                CourseCategory courseCategory = getCourseCategoryById(courseCategoryCacheMap, resp.getCategoryId2());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName2(courseCategory.getCategoryName());
                }
            }
            if (resp.getCategoryId3() != null && resp.getCategoryId3() != 0) {
                CourseCategory courseCategory = getCourseCategoryById(courseCategoryCacheMap, resp.getCategoryId3());
                if (!StringUtils.isEmpty(courseCategory)) {
                    resp.setCategoryName3(courseCategory.getCategoryName());
                }
            }
        }
        return Result.success(respPage);
    }

    /**
     * 根据ID获取课程分类
     *
     * @param cacheMap 缓存Map
     * @param id       分类ID
     * @return 分类信息
     */
    private CourseCategory getCourseCategoryById(Map<Long, CourseCategory> cacheMap, Long id) {
        CourseCategory courseCategory = cacheMap.get(id);
        if (ObjectUtil.isNull(courseCategory)) {
            courseCategory = courseCategoryDao.getById(id);
            if (ObjectUtil.isNotNull(courseCategory)) {
                cacheMap.put(id, courseCategory);
            } else {
                cacheMap.put(id, new CourseCategory());
            }
        }
        return courseCategory;
    }


    /**
     * 课程信息添加
     *
     * @param courseSaveREQ 课程信息
     * @return 添加结果
     */
    public Result<String> save(CourseSaveREQ courseSaveREQ) {
        Course record = BeanUtil.copyProperties(courseSaveREQ, Course.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
     * 课程信息查看
     *
     * @param id 主键ID
     * @return 课程信息
     */
    public Result<CourseViewRESP> view(Long id) {
        CourseViewRESP resp = BeanUtil.copyProperties(dao.getById(id), CourseViewRESP.class);
        if (ObjectUtil.isNotNull(resp)) {
            // 根据id查找课程简介信息
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(resp.getIntroduceId());
            // 把课程简介带回课程信息
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                resp.setIntroduce(courseIntroduce.getIntroduce());
            }
        }
        return Result.success(resp);
    }


    /**
     * 课程信息修改
     *
     * @param courseEditREQ 课程信息修改对象
     * @return 修改结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> edit(CourseEditREQ courseEditREQ) {
        if (IsFreeEnum.FREE.getCode().equals(courseEditREQ.getIsFree())) {
            courseEditREQ.setCourseOriginal(BigDecimal.ZERO);
            courseEditREQ.setCourseDiscount(BigDecimal.ZERO);
        }

        Course course = dao.getById(courseEditREQ.getId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }

        Course record = BeanUtil.copyProperties(courseEditREQ, Course.class);
        if (record.getIntroduceId() != null && !record.getIntroduceId().equals(0L)) {
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(record.getIntroduceId());
            if (ObjectUtil.isNotNull(courseIntroduce)) {
                courseIntroduce.setId(courseEditREQ.getIntroduceId());
                courseIntroduce.setIntroduce(courseEditREQ.getIntroduce());
                courseIntroduceDao.updateById(courseIntroduce);
            }
        }
        dao.updateById(record);

        // 同步更新审核
        CourseAudit courseAudit = BeanUtil.copyProperties(courseEditREQ, CourseAudit.class);
        if (courseAudit.getIntroduceId() != null && !courseAudit.getIntroduceId().equals(0L)) {
            CourseIntroduceAudit courseIntroduceAudit = courseIntroduceAuditDao.getById(courseAudit.getIntroduceId());
            if (ObjectUtil.isNotNull(courseIntroduceAudit)) {
                courseIntroduceAudit.setId(courseEditREQ.getIntroduceId());
                courseIntroduceAudit.setIntroduce(courseEditREQ.getIntroduce());
                courseIntroduceAuditDao.updateById(courseIntroduceAudit);
            }
        }
        if (courseAuditDao.updateById(courseAudit) > 0) {
            // 插入es或者更新es
            EsCourse esCourse = BeanUtil.copyProperties(courseAudit, EsCourse.class);
            IndexQuery query = new IndexQueryBuilder().withObject(esCourse).build();
            elasticsearchRestTemplate.index(query, IndexCoordinates.of(EsCourse.COURSE));
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
     * 课程信息删除
     *
     * @param id ID主键
     * @return 删除结果
     */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 导入ES
     *
     * @param courseAddEsREQ 导入参数
     * @return 导入结果
     */
    public Result<String> addEs(CourseAddEsREQ courseAddEsREQ) {
        // 查询全部，删除全部索引重建
        List<Course> courseList = dao.listByStatusId(StatusIdEnum.YES.getCode());
        List<IndexQuery> queries = new ArrayList<>();
        try {
            // 查询讲师名称并插入es
            for (Course course : courseList) {
                EsCourse esCourse = BeanUtil.copyProperties(course, EsCourse.class);
                LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(course.getLecturerUserNo());
                if (ObjectUtil.isNotNull(lecturerVO) && !StringUtils.isEmpty(lecturerVO.getLecturerName())) {
                    esCourse.setLecturerName(lecturerVO.getLecturerName());
                }
                IndexQuery query = new IndexQueryBuilder().withObject(esCourse).build();
                queries.add(query);
            }
            elasticsearchRestTemplate.indexOps(EsCourse.class).delete();
            elasticsearchRestTemplate.bulkIndex(queries, IndexCoordinates.of(EsCourse.COURSE));
        } catch (Exception e) {
            logger.warn("elasticsearch更新数据失败", e);
            return Result.error("导入失败");
        }
        return Result.success("导入成功");
    }

    /**
     * 更新状态
     *
     * @param courseUpdateStatusREQ 课程状态更新对象
     * @return 更新结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> updateStatus(CourseUpdateStatusREQ courseUpdateStatusREQ) {
        Course course = dao.getById(courseUpdateStatusREQ.getId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        course.setStatusId(courseUpdateStatusREQ.getStatusId());
        dao.updateById(course);

        CourseAudit courseAudit = courseAuditDao.getById(courseUpdateStatusREQ.getId());
        if (courseAudit != null) {
            courseAudit.setStatusId(courseUpdateStatusREQ.getStatusId());
            courseAuditDao.updateById(courseAudit);
        }
        return Result.success("修改成功");
    }

    /**
     * 更新是否上架
     *
     * @param courseUpdateIsPutawayREQ 课程更新是否上下架
     * @return 更新结果
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<String> updateIsPutAway(CourseUpdateIsPutawayREQ courseUpdateIsPutawayREQ) {
        Course course = dao.getById(courseUpdateIsPutawayREQ.getId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到课程信息");
        }
        course.setIsPutaway(courseUpdateIsPutawayREQ.getIsPutaway());
        dao.updateById(course);

        CourseAudit courseAudit = courseAuditDao.getById(courseUpdateIsPutawayREQ.getId());
        if (courseAudit != null) {
            courseAudit.setIsPutaway(courseUpdateIsPutawayREQ.getIsPutaway());
            courseAuditDao.updateById(courseAudit);
        }
        return Result.success("修改成功");
    }

    public Result<String> upload(MultipartFile multipartFile) {
        String url = AliyunUtil.uploadPic(PlatformEnum.COURSE, multipartFile, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
        if (StrUtil.isNotBlank(url)) {
            return Result.success(url);
        }
        return Result.error("上传文件失败");
    }

    /**
     * 获取订单信息查找带回课程信息
     *
     * @param courseFindBackREQ 分页查询参数
     * @return 分页结果
     */
    public Result<Page<CourseFindBackRESP>> findBack(CourseFindBackREQ courseFindBackREQ) {
        courseFindBackREQ.setIsPutaway(IsPutawayEnum.YES.getCode());
        courseFindBackREQ.setStatusId(StatusIdEnum.YES.getCode());
        CourseQO qo = new CourseQO();
        qo.setCourseCategory(courseFindBackREQ.getCourseCategory());
        qo.setIsFree(courseFindBackREQ.getIsFree());
        qo.setCourseName(courseFindBackREQ.getCourseName());
        qo.setPageCurrent(courseFindBackREQ.getPageCurrent());
        qo.setPageSize(courseFindBackREQ.getPageSize());
        Result<Page<CourseListRESP>> resultPage = list(BeanUtil.copyProperties(courseFindBackREQ, CourseListREQ.class));
        return Result.success(PageUtil.transform(resultPage.getData(), CourseFindBackRESP.class));
    }
}
