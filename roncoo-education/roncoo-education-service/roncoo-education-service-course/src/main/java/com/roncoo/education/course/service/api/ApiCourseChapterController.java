package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.CourseChapterPageBO;
import com.roncoo.education.course.common.dto.CourseChapterPageDTO;
import com.roncoo.education.course.service.api.biz.ApiCourseChapterBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/course/api/course/chapter")
public class ApiCourseChapterController {

	@Autowired
	private ApiCourseChapterBiz biz;

	/**
	 * 普通课程、直播、试卷详情列出章节课时接口
	 *
	 * @param courseChapterListBO
	 * @author kyh
	 */
	@ApiOperation(value = "普通课程、直播、试卷详情列出章节课时接口", notes = "普通课程、直播、试卷详情根据课程ID列出章节课时接口")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<Page<CourseChapterPageDTO>> listForPage(@RequestBody CourseChapterPageBO courseChapterListBO) {
		return biz.listForPage(courseChapterListBO);

	}

}
