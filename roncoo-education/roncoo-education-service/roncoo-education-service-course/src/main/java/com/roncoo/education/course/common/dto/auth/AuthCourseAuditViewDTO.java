package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 课程信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseAuditViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;


	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;

	@ApiModelProperty(value = "一级分类ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId1;

	@ApiModelProperty(value = "二级分类ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId2;

	@ApiModelProperty(value = "三级分类ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId3;

	@ApiModelProperty(value = "课程名称")
	private String courseName;

	@ApiModelProperty(value = "课程封面")
	private String courseLogo;

	@ApiModelProperty(value = "课程介绍")
	private String introduce;

	@ApiModelProperty(value = "原价")
	private BigDecimal courseOriginal;

	@ApiModelProperty(value = "优惠价")
	private BigDecimal courseDiscount;

	@ApiModelProperty(value = "是否免费(1:免费, 0:收费)")
	private Integer isFree;

	@ApiModelProperty(value = "课程类型：(1普通,2直播,3试卷)")
	private Integer courseCategory;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}
