package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文件信息
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="FileInfoListREQ", description="文件信息列表")
public class FileInfoListREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "文件地址")
    private String fileUrl;

    @ApiModelProperty(value = "文件类型(1:附件;2;图片;3:视频)")
    private Integer fileType;

    @ApiModelProperty(value = "文件大小(B)")
    private Long fileSize;

    /**
    * 当前页
    */
    @ApiModelProperty(value = "当前页")
    private int pageCurrent = 1;

    /**
    * 每页记录数
    */
    @ApiModelProperty(value = "每页条数")
    private int pageSize = 20;
}
