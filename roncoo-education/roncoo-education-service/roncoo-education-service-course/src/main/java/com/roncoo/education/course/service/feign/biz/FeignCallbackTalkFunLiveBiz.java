package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.roncoo.education.common.core.aliyun.Aliyun;
import com.roncoo.education.common.core.aliyun.AliyunUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.common.core.tools.SysConfigConstants;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadUrlFile;
import com.roncoo.education.common.talk.TalkFunUtil;
import com.roncoo.education.common.talk.request.CourseGetRequest;
import com.roncoo.education.common.talk.request.CourseUpdateRequest;
import com.roncoo.education.common.talk.request.CourseVideoRequest;
import com.roncoo.education.common.talk.response.CourseGetResponse;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveDocumentAddQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLivePlaybackQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStartQO;
import com.roncoo.education.course.feign.qo.TalkFunCallbackLiveStopQO;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigTalkFunVO;
import com.roncoo.education.system.feign.vo.SysConfigVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 欢拓回调处理
 *
 * @author LYQ
 */
@Slf4j
@Component
public class FeignCallbackTalkFunLiveBiz extends BaseBiz {

    @Autowired
    private CourseLiveLogDao courseLiveLogDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;

    @Autowired
    private IFeignSysConfig feignSysConfig;

    /**
     * 直播开始
     *
     * @param qo 直播开始通知参数
     * @return 处理结果
     */
    public Boolean liveStart(TalkFunCallbackLiveStartQO qo) {
        // 获取直播记录(获取没开播的记录)
        CourseLiveLog liveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.TALK_FUN.getCode(), String.valueOf(qo.getCourseId()));
        if (ObjectUtil.isNull(liveLog)) {
            log.error("欢拓开启直播失败 ,找不到直播记录信息");
            return false;
        }
        if (!LiveStatusEnum.NOT.getCode().equals(liveLog.getLiveStatus())) {
            log.error("欢拓回调通知--开始直播，没有找到没开播的直播记录");
            return false;
        }

        CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriodAudit)) {
            log.error("欢拓开始直播失败，课时审核信息不存在或者状态不正确");
            return false;
        }
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod)) {
            log.error("欢拓开启直播失败，课时信息不存在或状态不正确");
            return false;
        }

        // 直播记录正在直播状态
        liveLog.setBeginTime(new Date());
        liveLog.setLiveStatus(LiveStatusEnum.NOW.getCode());
        courseLiveLogDao.updateById(liveLog);

        courseChapterPeriodAudit.setLiveStatus(LiveStatusEnum.NOW.getCode());
        courseChapterPeriodAuditDao.updateById(courseChapterPeriodAudit);

        courseChapterPeriod.setLiveStatus(LiveStatusEnum.NOW.getCode());
        courseChapterPeriodDao.updateById(courseChapterPeriod);
        return true;
    }

    /**
     * 直播结束
     *
     * @param qo 直播结束通知参数
     * @return 处理结果
     */
    public Boolean liveStop(TalkFunCallbackLiveStopQO qo) {
        // 获取直播记录(获取没开播的记录)
        CourseLiveLog liveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.TALK_FUN.getCode(), String.valueOf(qo.getCourseId()));
        if (ObjectUtil.isNull(liveLog)) {
            log.error("欢拓停止直播失败 ,找不到直播记录信息");
            return false;
        }
        if (!LiveStatusEnum.NOW.getCode().equals(liveLog.getLiveStatus())) {
            log.error("欢拓回调通知--停止直播，没有找到直播中的直播记录");
            return false;
        }

        CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriodAudit) || StatusIdEnum.NO.getCode().equals(courseChapterPeriodAudit.getStatusId())) {
            log.error("欢拓停止直播失败，课时审核信息不存在或者状态不正确");
            return false;
        }
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod) || StatusIdEnum.NO.getCode().equals(courseChapterPeriod.getStatusId())) {
            log.error("欢拓停止直播失败，课时信息不存在或状态不正确");
            return false;
        }

        // 修改直播课程时间，防止二次开播
        ConfigTalkFunVO configTalkFunVo = feignSysConfig.getTalkFun();
        CourseGetRequest getRequest = new CourseGetRequest();
        getRequest.setCourseId(qo.getCourseId());
        CourseGetResponse getResponse = TalkFunUtil.courseGet(getRequest, configTalkFunVo.getTalkFunOpenId(), configTalkFunVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(getResponse)) {
            log.error("欢拓获取直播课程信息失败，获取课程信息为空");
            return false;
        }
        CourseUpdateRequest updateRequest = new CourseUpdateRequest();
        updateRequest.setCourseId(getResponse.getCourseId());
        updateRequest.setCourseName(getResponse.getCourseName());
        updateRequest.setAccount(getResponse.getZhuBo().getThirdAccount());
        // 因为直播结束后30分钟内，还可以开始直播，所以接收到结束通知，直播时间整体向前偏移一个小时
        updateRequest.setStartTime(DateUtil.offsetHour(getResponse.getStartTime(), -1));
        updateRequest.setEndTime(DateUtil.offsetHour(new Date(), -1));
        updateRequest.setNickname(getResponse.getZhuBo().getNickname());
        Boolean result = TalkFunUtil.courseUpdate(updateRequest, configTalkFunVo.getTalkFunOpenId(), configTalkFunVo.getTalkFunOpenToken());
        if (!result) {
            log.error("欢拓直播修改课程信息失败");
            return false;
        }

        // 更改状态为待生成回放
        liveLog.setEndTime(new Date());
        liveLog.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
        courseLiveLogDao.updateById(liveLog);

        courseChapterPeriodAudit.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
        courseChapterPeriodAuditDao.updateById(courseChapterPeriodAudit);

        courseChapterPeriod.setLiveStatus(LiveStatusEnum.WAITCREATE.getCode());
        courseChapterPeriodDao.updateById(courseChapterPeriod);
        return true;
    }

    /**
     * 直播回放生成
     *
     * @param qo 直播回放生成通知参数
     * @return 处理结果
     */
    public Boolean livePlayback(TalkFunCallbackLivePlaybackQO qo) {
        log.warn("回放生成回调结果:{}", JSON.toJSONString(qo));
        ConfigTalkFunVO talkFunVo = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(talkFunVo) || StrUtil.isBlank(talkFunVo.getTalkFunOpenToken()) || StrUtil.isBlank(talkFunVo.getTalkFunOpenId())) {
            log.error("欢拓配置有误，参数：{}", talkFunVo);
            return false;
        }
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return false;
        }
        //获取水印logo
        SysConfigVO sysConfigVO = feignSysConfig.getByConfigKey(SysConfigConstants.LOGO);

        CourseLiveLog liveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.TALK_FUN.getCode(), String.valueOf(qo.getCourseId()));
        if (ObjectUtil.isNull(liveLog)) {
            log.error("欢拓直播回放生成，找不到直播记录");
            return false;
        }

        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(periodAudit)) {
            log.error("找不到课时[{}]审核信息", liveLog.getPeriodId());
            return false;
        }
        periodAudit.setPlayback(qo.getUrl());
        periodAudit.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        courseChapterPeriodAuditDao.updateById(periodAudit);

        CourseChapterPeriod period = courseChapterPeriodDao.getById(liveLog.getPeriodId());
        if (ObjectUtil.isNull(period)) {
            log.error("找不到课时[{}]信息", liveLog.getPeriodId());
            return false;
        }
        period.setPlayback(qo.getUrl());
        period.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        courseChapterPeriodDao.updateById(period);

        // 更改状态为待转存
        liveLog.setLiveStatus(LiveStatusEnum.WAITSAVE.getCode());
        liveLog.setFileUrl(qo.getUrl());
        courseLiveLogDao.updateById(liveLog);

        CourseVideoRequest courseVideoRequest = new CourseVideoRequest();
        courseVideoRequest.setCourseId(qo.getCourseId());
        CourseVideoRequest.Option option = new CourseVideoRequest.Option();
        option.setDownload(true);
        courseVideoRequest.setOption(option);
        List<String> liveBackUrlList = TalkFunUtil.courseVideo(courseVideoRequest, talkFunVo.getTalkFunOpenId(), talkFunVo.getTalkFunOpenToken());
        if (CollectionUtil.isEmpty(liveBackUrlList)) {
            log.error("欢拓获取音视频下载地址为空");
            return false;
        }
        String liveBackUrl = liveBackUrlList.get(0);

        // 开启了阿里云备份，且是普通直播才备份----三分屏需要重制课件后才备份
        String value = feignSysConfig.getByConfigKey(SysConfigConstants.POLYV_LIVE_BACKUP).getConfigValue();
        if ("true".equals(value)) {
            uploadOSS(liveBackUrl, period.getPeriodName(), liveLog);
        }

        // 转存成点播
        //上传保利威
        UploadUrlFile uploadUrlFile = new UploadUrlFile();
        uploadUrlFile.setUrl(liveBackUrl);
        uploadUrlFile.setTitle(liveLog.getPeriodName());
        uploadUrlFile.setDesc(liveLog.getPeriodName());
        //异步处理
        uploadUrlFile.setAsync("true");
        //自定义数据为直播id，上传完成回调时会返回处理
        uploadUrlFile.setState(String.valueOf(liveLog.getId()));
        //视频类型，用于区分是试卷、普通课程还是直播回放
        uploadUrlFile.setTag(VideoTagEnum.LIVE_PLAYBACK.getCode());
        if (!org.springframework.util.StringUtils.isEmpty(sysConfigVO)) {
            uploadUrlFile.setWatermark(sysConfigVO.getConfigValue());
        }
        return PolyvUtil.uploadUrlFile(uploadUrlFile, configPolyvVO.getPolyvWritetoken(), configPolyvVO.getPolyvSecretkey());
    }

    /**
     * 上传文档
     *
     * @param qo 上传文档通知参数
     * @return 处理结果
     */
    public Boolean liveDocumentAdd(TalkFunCallbackLiveDocumentAddQO qo) {
        // TODO 待定，回放暂时不处理
        return null;
    }

    /**
     * 上传阿里云OSS
     *
     * @param fileUrl       文件路径
     * @param fileName      文件名称
     * @param courseLiveLog 直播日志
     */
    private void uploadOSS(String fileUrl, String fileName, CourseLiveLog courseLiveLog) {
        if (StrUtil.isNotBlank(fileUrl)) {
            CALLBACK_EXECUTOR.execute(() -> {
                //上传阿里云
                String videoOssId = AliyunUtil.uploadVideo(PlatformEnum.COURSE, fileUrl, fileName, BeanUtil.copyProperties(feignSysConfig.getAliyun(), Aliyun.class));
                if (StringUtils.isEmpty(videoOssId)) {
                    log.warn("上传阿里云备份失败!");
                } else {
                    courseLiveLog.setVideoOssId(videoOssId);
                    courseLiveLogDao.updateById(courseLiveLog);
                }
            });
        } else {
            log.error("重制课程回调处理失败！");
        }
    }
}
