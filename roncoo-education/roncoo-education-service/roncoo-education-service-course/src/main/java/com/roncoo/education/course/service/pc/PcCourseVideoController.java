package com.roncoo.education.course.service.pc;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.req.CourseVideoEditREQ;
import com.roncoo.education.course.common.req.CourseVideoListREQ;
import com.roncoo.education.course.common.req.CourseVideoSaveREQ;
import com.roncoo.education.course.common.resp.CourseVideoListRESP;
import com.roncoo.education.course.common.resp.CourseVideoViewRESP;
import com.roncoo.education.course.service.pc.biz.PcCourseVideoBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程视频信息 Pc接口
 *
 * @author wujing
 */
@RestController
@RequestMapping("/course/pc/course/video")
@Api(value = "course-课程视频信息", tags = {"course-课程视频信息"})
public class PcCourseVideoController {

    @Autowired
    private PcCourseVideoBiz biz;

    @ApiOperation(value = "课程视频信息列表", notes = "课程视频信息列表")
    @PostMapping(value = "/list")
    public Result<Page<CourseVideoListRESP>> list(@RequestBody CourseVideoListREQ courseVideoListREQ) {
        return biz.list(courseVideoListREQ);
    }

    @ApiOperation(value = "课程视频信息添加", notes = "课程视频信息添加")
    @PostMapping(value = "/save")
    public Result<String> save(@RequestBody CourseVideoSaveREQ courseVideoSaveREQ) {
        return biz.save(courseVideoSaveREQ);
    }

    @ApiOperation(value = "课程视频信息查看", notes = "课程视频信息查看")
    @GetMapping(value = "/view")
    public Result<CourseVideoViewRESP> view(@RequestParam Long id) {
        return biz.view(id);
    }

    @ApiOperation(value = "课程视频信息修改", notes = "课程视频信息修改")
    @PutMapping(value = "/edit")
    public Result<String> edit(@RequestBody CourseVideoEditREQ courseVideoEditREQ) {
        return biz.edit(courseVideoEditREQ);
    }

    @ApiOperation(value = "课程视频信息删除", notes = "课程视频信息删除")
    @DeleteMapping(value = "/delete")
    public Result<String> delete(@RequestParam Long id) {
        return biz.delete(id);
    }
}
