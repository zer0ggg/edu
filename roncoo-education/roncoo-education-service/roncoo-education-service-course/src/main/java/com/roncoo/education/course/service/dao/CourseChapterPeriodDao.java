package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample;

import java.util.List;

public interface CourseChapterPeriodDao {
    int save(CourseChapterPeriod record);

    int deleteById(Long id);

    int updateById(CourseChapterPeriod record);

    CourseChapterPeriod getById(Long id);

    Page<CourseChapterPeriod> listForPage(int pageCurrent, int pageSize, CourseChapterPeriodExample example);

    List<CourseChapterPeriod> listByChapterId(Long chapterId);

    /**
     * 根据章节编号和状态查找可用的课时信息集合
     *
     * @param chapterId
     * @return
     * @author LHR
     */
    List<CourseChapterPeriod> listByChapterIdAndStatusId(Long chapterId, Integer statusId);

    /**
     * 根据视频id查找课时信息集合
     *
     * @param videoId
     * @return
     * @author wuyun
     */
    CourseChapterPeriod getByVideoId(Long videoId);

    /**
     * 根据课程ID、正在直播状态查找课时信息
     *
     * @param courseId
     * @param liveStatus
     * @author kyh
     */
    CourseChapterPeriod getByCourseIdAndLiveStatus(Long courseId, Integer liveStatus);

    /**
     * 根据课程ID、直播状态排序顺序列出课时信息
     *
     * @param courseId
     * @param liveStatus
     * @author kyh
     */
    List<CourseChapterPeriod> listByCourseIdAndLiveStatusAndSorAsc(Long courseId, Integer liveStatus);

    /**
     * 根据课程ID、状态,顺序排序查找课时信息
     *
     * @param chapterId
     * @param statusId
     * @author kyh
     */
    List<CourseChapterPeriod> listByChapterIdAndStatusIdAndSortAsc(Long chapterId, Integer statusId);

    /**
     * 根据课程ID、状态、倒序序查找章节最后一课时
     *
     * @param chapterId
     * @param statusId
     * @author kyh
     */
    List<CourseChapterPeriod> listByChapterIdAndStatusIdAndSortDesc(Long chapterId, Integer statusId);

    /**
     * 根据视频ID获取课时信息
     *
     * @param videoId
     * @author kyh
     */
    List<CourseChapterPeriod> listByVideoId(Long videoId);

    /**
     * 根据课时ID、状态获取课时信息
     *
     * @param videoId
     * @param statusId
     * @author kyh
     */
    List<CourseChapterPeriod> listByVideoIdAndStatusId(Long videoId, Integer statusId);

    /**
     * 根据vid更新时长
     *
     * @param courseChapterPeriod
     * @return
     */
    int updateByVideoId(CourseChapterPeriod courseChapterPeriod);

    /**
     * 根据课程ID获取课时信息
     *
     * @param courseId
     * @return
     */
    List<CourseChapterPeriod> listByCourseId(Long courseId);

    int updateByVid(CourseChapterPeriod courseChapterPeriod);

    /**
     * 根据课程ID, 状态获取课时信息
     *
     * @param courseId
     * @return
     */
    List<CourseChapterPeriod> listByCourseIdAndStatusId(Long courseId, Integer statusId);

    /**
     * 根据课时ID集合获取课时信息
     *
     * @param ids
     * @return
     */
    List<CourseChapterPeriod> listByIds(List<Long> ids);
}
