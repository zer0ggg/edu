package com.roncoo.education.course.common.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 章节信息(管理员预览)
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class CourseChapterAuditPageBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID", required = true)
	private Long courseId;
	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页", required = true)
	private Integer pageCurrent = 1;
	/**
	 * 每页记录数
	 */
	@ApiModelProperty(value = "每页记录数", required = true)
	private Integer pageSize = 20;

}
