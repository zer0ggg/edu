package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 章节图片信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPicDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "修改时间")
	private Date gmtModified;
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer sort;
	/**
	 * 课程id
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 章节id
	 */
	@ApiModelProperty(value = "章节ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long chapterId;
	/**
	 * 图片类型(1:试卷题目,2:试卷答案)
	 */
	@ApiModelProperty(value = "图片类型(1:试卷题目,2:试卷答案)")
	private Integer picType;
	/**
	 * 图片地址
	 */
	@ApiModelProperty(value = "图片地址")
	private String picUrl;
	/**
	 * 标题
	 */
	@ApiModelProperty(value = "标题")
	private String title;
}
