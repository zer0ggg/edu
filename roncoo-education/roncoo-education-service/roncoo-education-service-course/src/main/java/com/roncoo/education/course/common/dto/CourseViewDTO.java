package com.roncoo.education.course.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程信息
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class CourseViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;
	/**
	 * 讲师用户编码
	 */
	@ApiModelProperty(value = "讲师用户编码")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lecturerUserNo;

	@ApiModelProperty(value = "分类ID1")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId1;

	@ApiModelProperty(value = "分类ID2")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId2;

	@ApiModelProperty(value = "分类ID3")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long categoryId3;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	/**
	 * 课程封面
	 */
	@ApiModelProperty(value = "课程封面")
	private String courseLogo;
	/**
	 * 课程介绍ID
	 */
	@ApiModelProperty(value = "课程介绍ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long introduceId;

	@ApiModelProperty(value = "是否免费：1免费，0收费")
	private Integer isFree;

	@ApiModelProperty(value = "原价")
	private BigDecimal courseOriginal;

	@ApiModelProperty(value = "优惠价")
	private BigDecimal courseDiscount;

	@ApiModelProperty(value = "购买人数")
	private Integer countBuy;

	@ApiModelProperty(value = "学习人数")
	private Integer countStudy;

	@ApiModelProperty(value = "总课时数")
	private Integer periodTotal;

	@ApiModelProperty(value = "课程介绍")
	private String introduce;

	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,3:试卷)")
	private Integer courseCategory;

	@ApiModelProperty(value = "讲师信息")
	private LecturerDTO lecturer;

	@ApiModelProperty(value = "是否上架(1:上架，0:下架)")
	private Integer isPutaway;

	@ApiModelProperty(value = "SEO关键词")
	private String keywords;

	@ApiModelProperty(value = "SEO描述")
	private String description;

	@ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:回放)")
	private Integer liveStatus;

	@ApiModelProperty(value = "课时名称")
	private String periodName;

	@ApiModelProperty(value = "直播开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@ApiModelProperty(value = "直播结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	@ApiModelProperty(value = "课程试卷关联主键id")
	private Long courseExamRefId;

	@ApiModelProperty(value = "试卷ID")
	private Long examId;

	@ApiModelProperty(value = "试卷名称")
	private String examName;

	@ApiModelProperty(value = "课时附件信息集合")
	private List<CourseAccessoryDTO> accessoryList = new ArrayList<>();

    @ApiModelProperty(value = "分销比例")
    private BigDecimal chosenPercent;

}
