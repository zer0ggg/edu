package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.auth.*;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 课程信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/course")
public class AuthCourseController extends BaseController {

    @Autowired
    private AuthCourseBiz biz;

    @ApiOperation(value = "普通课程、直播、试卷详情接口(已登录)", notes = "用户登录后获取普通课程、直播、试卷详情信息接口")
    @RequestMapping(value = "/view", method = RequestMethod.POST)
    public Result<AuthCourseViewDTO> view(@RequestBody AuthCourseViewBO authCourseViewBO) {
        return biz.view(authCourseViewBO);
    }

    @ApiOperation(value = "课时播放获取sign值接口", notes = "课时播放获取sign值接口")
    @RequestMapping(value = "/sign", method = RequestMethod.POST)
    public Result<AuthCourseSignDTO> sign(@RequestBody AuthCourseSignBO authCourseSignBO) {
        return biz.sign(authCourseSignBO);
    }

    @ApiOperation(value = "获取直播链接接口", notes = "获取直播链接接口")
    @RequestMapping(value = "/getLiveUrl", method = RequestMethod.POST)
    public Result<AuthCourseGetLiveUrlDTO> getLiveUrl(@RequestBody AuthCourseGetUrlBO authCourseGetUrlBO) {
        return biz.getLiveUrl(authCourseGetUrlBO);
    }

    @ApiOperation(value = "获取直播回放链接接口", notes = "获取直播回放链接接口")
    @RequestMapping(value = "/getPlaybackUrl", method = RequestMethod.POST)
    public Result<String> getPlaybackUrl(@RequestBody AuthCourseGetUrlBO authCourseGetUrlBO) {
        return biz.getPlaybackUrl(authCourseGetUrlBO);
    }

    @ApiOperation(value = "列出直播课程信息接口", notes = "列出直播课程信息接口")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Result<Page<AuthCoursePageDTO>> page(@RequestBody AuthCoursePageBO authCoursePageBO) {
        return biz.page(authCoursePageBO);
    }

    @ApiOperation(value = "频道信息详情接口", notes = "频道信息详情接口")
    @RequestMapping(value = "/channel/view", method = RequestMethod.POST)
    public Result<AuthChannelViewDTO> channelView(@RequestBody AuthChannelViewBO authChannelViewBO) {
        return biz.channelview(authChannelViewBO);
    }

    @ApiOperation(value = "修改频道密码接口", notes = "修改频道密码接口")
    @RequestMapping(value = "/update/channel/passwd", method = RequestMethod.POST)
    public Result<Integer> updatePasswd(@RequestBody AuthUpdateChannelPasswdBO authUpdateChannelPasswdBO) {
        return biz.updateChannelPasswd(authUpdateChannelPasswdBO);
    }
}
