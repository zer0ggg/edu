package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课程用户学习日志
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseUserStudyLogDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课时vid
     */
    @ApiModelProperty(value = "视频VID", required = true)
    private String videoVid;
    /**
     * 最近学习课时ID
     */
    @ApiModelProperty(value = "最近学习课时ID")
    private Long lastStudyPeriodId;
}
