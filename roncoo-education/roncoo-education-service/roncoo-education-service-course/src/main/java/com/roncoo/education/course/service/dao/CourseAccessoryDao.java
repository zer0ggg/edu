package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessory;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseAccessoryExample;

import java.util.List;

public interface CourseAccessoryDao {
	int save(CourseAccessory record);

	int deleteById(Long id);

	int updateById(CourseAccessory record);

	int updateByExampleSelective(CourseAccessory record, CourseAccessoryExample example);

	CourseAccessory getById(Long id);

	Page<CourseAccessory> listForPage(int pageCurrent, int pageSize, CourseAccessoryExample example);

	/**
	 * 根据关联ID、关联类型、状态获取附件信息
	 *
	 * @param refId
	 * @param refType
	 * @param statusId
	 * @author kyh
	 */
	List<CourseAccessory> listByRefIdAndRefTypeAndStatusId(Long refId, Integer refType, Integer statusId);

	/**
	 * 根据关联ID、状态获取附件信息
	 *
	 * @param refId
	 * @param statusId
	 * @author kyh
	 */
	List<CourseAccessory> listByRefIdAndStatusId(Long refId, Integer statusId);

	/**
	 * 统计总下载数
	 *
	 * @return
	 *
	 * @author kyh
	 */
	int totalDownload();

	/**
	 * 根据课程分类统计下载数
	 *
	 * @param courseCategory
	 */
	int courseDownload(Integer courseCategory);

	/**
	 * 查找所附件信息
	 *
	 * @param courseAccessory
	 * @author kyh
	 */
	List<CourseAccessory> sumByAll(CourseAccessory courseAccessory);

}
