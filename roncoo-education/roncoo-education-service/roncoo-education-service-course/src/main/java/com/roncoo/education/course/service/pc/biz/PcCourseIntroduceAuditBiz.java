package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseIntroduceAuditEditREQ;
import com.roncoo.education.course.common.req.CourseIntroduceAuditListREQ;
import com.roncoo.education.course.common.req.CourseIntroduceAuditSaveREQ;
import com.roncoo.education.course.common.resp.CourseIntroduceAuditListRESP;
import com.roncoo.education.course.common.resp.CourseIntroduceAuditViewRESP;
import com.roncoo.education.course.service.dao.CourseIntroduceAuditDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceAuditExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseIntroduceAuditExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程介绍信息-审核
 *
 * @author wujing
 */
@Component
public class PcCourseIntroduceAuditBiz extends BaseBiz {

    @Autowired
    private CourseIntroduceAuditDao dao;

    /**
    * 课程介绍信息-审核列表
    *
    * @param courseIntroduceAuditListREQ 课程介绍信息-审核分页查询参数
    * @return 课程介绍信息-审核分页查询结果
    */
    public Result<Page<CourseIntroduceAuditListRESP>> list(CourseIntroduceAuditListREQ courseIntroduceAuditListREQ) {
        CourseIntroduceAuditExample example = new CourseIntroduceAuditExample();
        Criteria c = example.createCriteria();
        Page<CourseIntroduceAudit> page = dao.listForPage(courseIntroduceAuditListREQ.getPageCurrent(), courseIntroduceAuditListREQ.getPageSize(), example);
        Page<CourseIntroduceAuditListRESP> respPage = PageUtil.transform(page, CourseIntroduceAuditListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课程介绍信息-审核添加
    *
    * @param courseIntroduceAuditSaveREQ 课程介绍信息-审核
    * @return 添加结果
    */
    public Result<String> save(CourseIntroduceAuditSaveREQ courseIntroduceAuditSaveREQ) {
        CourseIntroduceAudit record = BeanUtil.copyProperties(courseIntroduceAuditSaveREQ, CourseIntroduceAudit.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课程介绍信息-审核查看
    *
    * @param id 主键ID
    * @return 课程介绍信息-审核
    */
    public Result<CourseIntroduceAuditViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseIntroduceAuditViewRESP.class));
    }


    /**
    * 课程介绍信息-审核修改
    *
    * @param courseIntroduceAuditEditREQ 课程介绍信息-审核修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseIntroduceAuditEditREQ courseIntroduceAuditEditREQ) {
        CourseIntroduceAudit record = BeanUtil.copyProperties(courseIntroduceAuditEditREQ, CourseIntroduceAudit.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课程介绍信息-审核删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
