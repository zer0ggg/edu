package com.roncoo.education.course.service.dao.impl.mapper;

import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyv;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CrontabPlanPolyvExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CrontabPlanPolyvMapper {
    int countByExample(CrontabPlanPolyvExample example);

    int deleteByExample(CrontabPlanPolyvExample example);

    int deleteByPrimaryKey(Long id);

    int insert(CrontabPlanPolyv record);

    int insertSelective(CrontabPlanPolyv record);

    List<CrontabPlanPolyv> selectByExample(CrontabPlanPolyvExample example);

    CrontabPlanPolyv selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") CrontabPlanPolyv record, @Param("example") CrontabPlanPolyvExample example);

    int updateByExample(@Param("record") CrontabPlanPolyv record, @Param("example") CrontabPlanPolyvExample example);

    int updateByPrimaryKeySelective(CrontabPlanPolyv record);

    int updateByPrimaryKey(CrontabPlanPolyv record);
}
