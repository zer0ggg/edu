package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 课程信息修改
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseEditREQ", description = "课程信息修改")
public class CourseEditREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "课程封面", required = true)
    private String courseLogo;

    @ApiModelProperty(value = "课程名称", required = true)
    private String courseName;

    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal courseOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal courseDiscount;

    @ApiModelProperty(value = "展示顺序", required = true)
    private Integer courseSort;

    @ApiModelProperty(value = "课程介绍ID", required = true)
    private Long introduceId;

    @ApiModelProperty(value = "课程简介")
    private String introduce;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer countStudy;

    @ApiModelProperty(value = "SEO关键词")
    private String keywords;

    @ApiModelProperty(value = "SEO描述")
    private String description;

}
