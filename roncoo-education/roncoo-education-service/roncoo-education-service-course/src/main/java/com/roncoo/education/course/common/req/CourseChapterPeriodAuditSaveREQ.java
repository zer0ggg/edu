package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课时信息-审核
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseChapterPeriodAuditSaveREQ", description = "课时信息-审核添加")
public class CourseChapterPeriodAuditSaveREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 章节ID
     */
    @ApiModelProperty(value = "章节ID", required = true)
    private Long chapterId;
    /**
     * 课时名称
     */
    @ApiModelProperty(value = "课时名称", required = true)
    private String periodName;
    /**
     * 课时描述
     */
    @ApiModelProperty(value = "课时描述", required = false)
    private String periodDesc;
    /**
     * 是否免费：1免费，0收费
     */
    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;
    /**
     * 直播开始时间
     */
    @ApiModelProperty(value = "直播开始时间")
    private String startTime;
    /**
     * 直播结束时间
     */
    @ApiModelProperty(value = "直播结束时间")
    private String endTime;
}
