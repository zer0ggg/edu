package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程用户学习日志-汇总
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="CourseUserStudyLogStatisticalREQ", description="课程用户学习日志列表-汇总")
public class CourseUserStudyLogStatisticalREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户编号(汇总用户时必传)")
    private Long userNo;


    @ApiModelProperty(value = "课程ID(汇总课程时必传)")
    private Long courseId;

    @ApiModelProperty(value = "排行数（默认展示前十）")
    private Integer ranking = 10;

}
