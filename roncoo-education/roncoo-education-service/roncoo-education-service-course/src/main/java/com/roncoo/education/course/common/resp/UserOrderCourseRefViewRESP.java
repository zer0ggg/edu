package com.roncoo.education.course.common.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户订单课程关联
 * </p>
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
@ApiModel(value="UserOrderCourseRefViewRESP", description="用户订单课程关联查看")
public class UserOrderCourseRefViewRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private Date gmtModified;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "讲师用户编号")
    private Long lecturerUserNo;

    @ApiModelProperty(value = "用户编号")
    private Long userNo;

    @ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4文库，5:试卷)")
    private Integer courseCategory;

    @ApiModelProperty(value = "课程类型：1课程，2章节，3课时")
    private Integer courseType;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "关联ID")
    private Long refId;

    @ApiModelProperty(value = "是否支付：1是，0:否")
    private Integer isPay;

    @ApiModelProperty(value = "是否学习：1是，0:否")
    private Integer isStudy;

    @ApiModelProperty(value = "订单号")
    private Long orderNo;

    @ApiModelProperty(value = "过期时间")
    private Date expireTime;
}
