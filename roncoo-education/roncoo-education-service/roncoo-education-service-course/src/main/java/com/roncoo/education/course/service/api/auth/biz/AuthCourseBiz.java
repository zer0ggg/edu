package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;
import com.roncoo.education.common.aliyun.AliYunVodCode;
import com.roncoo.education.common.aliyun.AliYunVodUtil;
import com.roncoo.education.common.cache.MyRedisTemplate;
import com.roncoo.education.common.core.base.*;
import com.roncoo.education.common.core.config.SystemUtil;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.*;
import com.roncoo.education.common.polyv.PolyvCode;
import com.roncoo.education.common.polyv.PolyvSign;
import com.roncoo.education.common.polyv.PolyvSignResult;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.live.PLChannelUtil;
import com.roncoo.education.common.polyv.live.result.PLChannelGetAccounts;
import com.roncoo.education.common.polyv.live.result.PLChannelGetResult;
import com.roncoo.education.common.polyv.live.result.PLChannelWatchAuthResult;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.common.talk.TalkFunUtil;
import com.roncoo.education.common.talk.request.CourseAccessRequest;
import com.roncoo.education.common.talk.request.CourseGetRequest;
import com.roncoo.education.common.talk.request.CourseVideoRequest;
import com.roncoo.education.common.talk.response.CourseAccessResponse;
import com.roncoo.education.common.talk.response.CourseGetResponse;
import com.roncoo.education.common.talk.response.CourseLaunchResponse;
import com.roncoo.education.common.talk.response.CoursePushRtmpUrlResponse;
import com.roncoo.education.course.common.bo.auth.*;
import com.roncoo.education.course.common.dto.CourseAccessoryDTO;
import com.roncoo.education.course.common.dto.CourseIntroduceDTO;
import com.roncoo.education.course.common.dto.auth.*;
import com.roncoo.education.course.common.req.LiveCallbackREQ;
import com.roncoo.education.course.service.dao.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseExample.Criteria;
import com.roncoo.education.data.feign.interfaces.IFeignCourseLog;
import com.roncoo.education.data.feign.qo.CourseLogQO;
import com.roncoo.education.data.feign.vo.CourseLogVO;
import com.roncoo.education.exam.feign.interfaces.IFeignCourseExamRef;
import com.roncoo.education.exam.feign.qo.CourseExamRefQO;
import com.roncoo.education.exam.feign.vo.CourseExamRefVO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigAliYunVodVO;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import com.roncoo.education.system.feign.vo.ConfigTalkFunVO;
import com.roncoo.education.user.feign.interfaces.IFeignChosen;
import com.roncoo.education.user.feign.interfaces.IFeignLecturer;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.interfaces.IFeignUserLecturerAttention;
import com.roncoo.education.user.feign.qo.UserLecturerAttentionQO;
import com.roncoo.education.user.feign.vo.ChosenVO;
import com.roncoo.education.user.feign.vo.LecturerVO;
import com.roncoo.education.user.feign.vo.UserExtVO;
import com.roncoo.education.user.feign.vo.UserLecturerAttentionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 课程信息
 *
 * @author wujing
 */
@Component
public class AuthCourseBiz extends BaseBiz {

    @Autowired
    private IFeignCourseLog feignCourseLog;
    @Autowired
    private IFeignUserLecturerAttention feignUserLecturerAttention;
    @Autowired
    private IFeignLecturer feignLecturer;
    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignUserExt feignUserExt;
    @Autowired
    private IFeignChosen feignChosen;
    @Autowired
    private IFeignCourseExamRef feignCourseExamRef;

    @Autowired
    private CourseAccessoryDao accessoryDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseChapterDao courseChapterDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseIntroduceDao courseIntroduceDao;
    @Autowired
    private UserOrderCourseRefDao userOrderCourseRefDao;
    @Autowired
    private CourseLiveLogDao courseLiveLogDao;
    @Autowired
    private CourseUserStudyLogDao courseUserStudyLogDao;
    @Autowired
    private CourseVideoDao courseVideoDao;

    @Autowired
    private UserCollectionCourseDao userCollectionCourseDao;
    @Autowired
    private MyRedisTemplate myRedisTemplate;

    public Result<AuthCourseSignDTO> sign(AuthCourseSignBO authCourseSignBO) {
        BeanValidateUtil.ValidationResult validationResult = BeanValidateUtil.validate(authCourseSignBO);
        if (!validationResult.isPass()) {
            return Result.error(validationResult.getErrMsgString());
        }
        // 查找用户信息
        UserExtVO userextVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userextVO)) {
            return Result.error("用户不存在，系统管理员");
        }
        if (StatusIdEnum.NO.getCode().equals(userextVO.getStatusId())) {
            return Result.error("该账号已被禁用，请联系客服");
        }
        // 课时信息
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(authCourseSignBO.getPeriodId());
        if (StringUtils.isEmpty(courseChapterPeriod)) {
            return Result.error("找不到课时信息");
        }
        // 章节信息
        CourseChapter courseChapter = courseChapterDao.getById(courseChapterPeriod.getChapterId());
        if (ObjectUtil.isNull(courseChapter)) {
            return Result.error("找不到章节信息");
        }
        // 课程信息
        Course course = courseDao.getById(courseChapterPeriod.getCourseId());
        if (ObjectUtil.isNull(course) || StatusIdEnum.NO.getCode().equals(course.getStatusId())) {
            // 课程不可以用
            return Result.error(ResultEnum.COURSE_DISABLE);
        }
        // 免费：课时免费，章节免费，课程免费
        if (IsFreeEnum.FREE.getCode().equals(courseChapterPeriod.getIsFree()) || IsFreeEnum.FREE.getCode().equals(courseChapter.getIsFree()) || IsFreeEnum.FREE.getCode().equals(course.getIsFree())) {
            // 如果课程免费，则保存用户课程关联信息
            if (IsFreeEnum.FREE.getCode().equals(course.getIsFree())) {
                saveUserOrderLiveCourseRef(course, userextVO);
            }
            // TODO 需要调整
            AuthCourseSignDTO authCourseSignDTO = getSign(authCourseSignBO, courseChapterPeriod);
            CALLBACK_EXECUTOR.execute(new StudyLog(authCourseSignBO, courseChapterPeriod, course, courseChapter.getChapterName(), userextVO.getMobile()));
            return Result.success(authCourseSignDTO);
        }
        // 课程会员免费、用戶是会员
        if (VipCheckUtil.checkVipIsFree(userextVO.getIsVip(), userextVO.getExpireTime(), course.getCourseDiscount())) {
            saveUserOrderLiveCourseRef(course, userextVO);
            // TODO 需要调整
            AuthCourseSignDTO authCourseSignDTO = getSign(authCourseSignBO, courseChapterPeriod);
            CALLBACK_EXECUTOR.execute(new StudyLog(authCourseSignBO, courseChapterPeriod, course, courseChapter.getChapterName(), userextVO.getMobile()));
            return Result.success(authCourseSignDTO);
        }

        UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(ThreadContext.userNo(), course.getId());
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            // 未购买或者没支付情况
            return Result.error("收费课程，请先购买");
        } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
            // 已过期情况
            logger.warn("购买课程已过期");
            return Result.error("收费课程，请先购买");
        }

        // 成功 TODO 需要调整
        AuthCourseSignDTO authCourseSignDTO = getSign(authCourseSignBO, courseChapterPeriod);
        CALLBACK_EXECUTOR.execute(new StudyLog(authCourseSignBO, courseChapterPeriod, course, courseChapter.getChapterName(), userextVO.getMobile()));

        return Result.success(authCourseSignDTO);
    }

    private void saveUserOrderLiveCourseRef(Course course, UserExtVO userExtVO) {
        // 保存用户课程关联信息
        UserOrderCourseRef ref = userOrderCourseRefDao.getByUserNoAndRefId(userExtVO.getUserNo(), course.getId());
        if (ObjectUtil.isNull(ref)) {
            UserOrderCourseRef userOrderCourseRef = new UserOrderCourseRef();
            userOrderCourseRef.setCourseCategory(course.getCourseCategory());
            userOrderCourseRef.setLecturerUserNo(course.getLecturerUserNo());
            userOrderCourseRef.setUserNo(userExtVO.getUserNo());
            userOrderCourseRef.setCourseType(CourseTypeEnum.COURSE.getCode());
            userOrderCourseRef.setCourseId(course.getId());
            userOrderCourseRef.setRefId(course.getId());
            userOrderCourseRef.setIsPay(IsPayEnum.NO.getCode());
            userOrderCourseRef.setIsStudy(IsStudyEnum.YES.getCode());
            userOrderCourseRef.setExpireTime(DateUtil.addYear(new Date(), 1));
            userOrderCourseRefDao.save(userOrderCourseRef);
        }
    }

    public Result<AuthCourseViewDTO> view(AuthCourseViewBO bo) {
        if (StringUtils.isEmpty(bo.getCourseId())) {
            return Result.error("courseId不能为空");
        }
        UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
            return Result.error("用戶异常");
        }
        // 查询课程信息
        Course course = courseDao.getById(bo.getCourseId());
        if (ObjectUtil.isNull(course) || StatusIdEnum.NO.getCode().equals(course.getStatusId())) {
            // 课程不可以用
            return Result.error(ResultEnum.COURSE_DISABLE);
        }
        if (CourseCategoryEnum.RESOURCES.getCode().equals(course.getCourseCategory())) {
            // 文库更新学习数
            course.setCountStudy(course.getCountStudy() + 1);
            courseDao.updateById(course);
        }
        AuthCourseViewDTO dto = BeanUtil.copyProperties(course, AuthCourseViewDTO.class);
        // 查找课程关联考试信息
        CourseExamRefQO courseExamRefQO = new CourseExamRefQO();
        courseExamRefQO.setRefId(course.getId());
        courseExamRefQO.setRefType(RefTypeEnum.COURSE.getCode());
        CourseExamRefVO courseExamRefVO = feignCourseExamRef.getByRefIdAndRefType(courseExamRefQO);
        if (ObjectUtil.isNotNull(courseExamRefVO)) {
            dto.setExamId(courseExamRefVO.getExamId());
        }
        // 如果为分销商品，设置分销比例和推荐码
        ChosenVO chosenVO = feignChosen.getByCourseId(course.getId());
        if (ObjectUtil.isNotNull(chosenVO) && StatusIdEnum.YES.getCode().equals(chosenVO.getStatusId())) {
            dto.setChosenPercent(chosenVO.getChosenPercent());
            dto.setReferralCode(userExtVO.getReferralCode());
        }
        if (course.getIntroduceId() != null && !course.getIntroduceId().equals(0L)) {
            // 查询课程介绍
            CourseIntroduce courseIntroduce = courseIntroduceDao.getById(course.getIntroduceId());
            dto.setIntroduce(BeanUtil.copyProperties(courseIntroduce, CourseIntroduceDTO.class).getIntroduce());
        }

        // 先假设为收费课程且用户未付款
        dto.setIsPay(IsPayEnum.NO.getCode());
        // 根据用户编号、课程ID获取 用户课程关联信息
        UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(ThreadContext.userNo(), bo.getCourseId());
        if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
            // 未购买或者没支付情况
            dto.setIsPay(IsPayEnum.NO.getCode());
        } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
            // 已过期情况
            dto.setIsPay(IsPayEnum.NO.getCode());
        } else {
            // 订单状态为已支付
            dto.setIsPay(IsPayEnum.YES.getCode());
        }
        // 会员课程免费，则直接设置为已支付
        if (VipCheckUtil.checkVipIsFree(userExtVO.getIsVip(), userExtVO.getExpireTime(), course.getCourseDiscount())) {
            dto.setIsPay(IsPayEnum.YES.getCode());
        }
        // 如果课程为免费课程则设置为已付费
        if (IsFreeEnum.FREE.getCode().equals(course.getIsFree())) {
            dto.setIsPay(IsPayEnum.YES.getCode());
        }

        // 查找最近学习的记录
        CourseLogQO record = new CourseLogQO();
        record.setUserNo(ThreadContext.userNo());
        record.setCourseId(bo.getCourseId());
        List<CourseLogVO> courseUserStudy = feignCourseLog.listByUserNoAndCourseId(record);
        if (courseUserStudy != null && CollectionUtil.isNotEmpty(courseUserStudy)) {
            CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(courseUserStudy.get(0).getPeriodId());
            if (ObjectUtil.isNotNull(courseChapterPeriod)) {
                if (IsPayEnum.YES.getCode().equals(dto.getIsPay()) || IsFreeEnum.FREE.getCode().equals(courseChapterPeriod.getIsFree())) {
                    AuthCourseUserStudyLogDTO logDTO = new AuthCourseUserStudyLogDTO();
                    logDTO.setLastStudyPeriodId(courseUserStudy.get(0).getPeriodId());
                    logDTO.setVideoVid(courseUserStudy.get(0).getVid());
                    dto.setAuthCourseUserStudyLog(logDTO);
                }
            }
        }
        // 查询讲师信息
        LecturerVO lecturerVO = feignLecturer.getByLecturerUserNo(dto.getLecturerUserNo());
        dto.setLecturer(BeanUtil.copyProperties(lecturerVO, AuthLecturerDTO.class));

        // 获取用户是否关注讲师
        UserLecturerAttentionQO userLecturerAttentionQO = new UserLecturerAttentionQO();
        userLecturerAttentionQO.setUserNo(ThreadContext.userNo());
        userLecturerAttentionQO.setLecturerUserNo(dto.getLecturerUserNo());
        UserLecturerAttentionVO userLecturerAttention = feignUserLecturerAttention.getByUserAndLecturerUserNo(userLecturerAttentionQO);
        if (ObjectUtil.isNull(userLecturerAttention)) {
            // 未关注
            dto.setIsAttentionLecturer(0);
        } else {
            // 已关注
            dto.setIsAttentionLecturer(1);
        }

        // 根据用户编号、课程ID获取收藏信息
        UserCollectionCourse userCollectionCourse = userCollectionCourseDao.getByUserAndCourseId(ThreadContext.userNo(), course.getId());
        if (ObjectUtil.isNull(userCollectionCourse)) {
            // 未关注
            dto.setIsCollectionCourse(0);
        } else {
            // 已关注
            dto.setIsCollectionCourse(1);
        }

        // 获取用户是否收藏课程

        if (CourseCategoryEnum.LIVE.getCode().equals(course.getCourseCategory())) {
            // 获取课程第一章
            List<CourseChapter> list = courseChapterDao.listByCourseIdAndStatusIdAndSortAsc(dto.getId(), StatusIdEnum.YES.getCode());
            if (CollectionUtil.isNotEmpty(list)) {
                // 获取课程第一章第一节课时
                List<CourseChapterPeriod> period = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortAsc(list.get(0).getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(period)) {
                    dto.setPeriodName(period.get(0).getPeriodName());
                    dto.setStartTime(period.get(0).getStartTime());
                }
            }
            // 获取课程第一章
            List<CourseChapter> listDesc = courseChapterDao.listByCourseIdAndStatusIdAndSortDesc(dto.getId(), StatusIdEnum.YES.getCode());
            // 获取课程第一章第一节课时
            if (CollectionUtil.isNotEmpty(listDesc)) {
                List<CourseChapterPeriod> periodDesc = courseChapterPeriodDao.listByChapterIdAndStatusIdAndSortDesc(listDesc.get(0).getId(), StatusIdEnum.YES.getCode());
                if (CollectionUtil.isNotEmpty(periodDesc)) {
                    dto.setEndTime(periodDesc.get(0).getEndTime());
                }
            }
            // 获取直播课程正在直播信息
            CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getByCourseIdAndLiveStatus(dto.getId(), LiveStatusEnum.NOW.getCode());
            if (ObjectUtil.isNotNull(courseChapterPeriod)) {
                dto.setIsFreePeriod(courseChapterPeriod.getIsFree());
                dto.setLiveStatus(courseChapterPeriod.getLiveStatus());
                dto.setPeriodId(courseChapterPeriod.getId());
            }
        }
        // 课时附件信息
        List<CourseAccessory> periodAccessoryList = accessoryDao.listByRefIdAndRefTypeAndStatusId(dto.getId(), RefTypeEnum.COURSE.getCode(), StatusIdEnum.YES.getCode());
        if (CollectionUtil.isNotEmpty(periodAccessoryList)) {
            dto.setAccessoryList(ArrayListUtil.copy(periodAccessoryList, CourseAccessoryDTO.class));
        }

        return Result.success(dto);
    }

    class StudyLog implements Runnable {
        private final AuthCourseSignBO authCourseSignBO;
        private final CourseChapterPeriod courseChapterPeriod;
        private final Course course;
        private final String mobile;
        private final String chapterName;

        public StudyLog(AuthCourseSignBO authCourseSignBO, CourseChapterPeriod courseChapterPeriod, Course course, String chapterName, String mobile) {
            this.authCourseSignBO = authCourseSignBO;
            this.courseChapterPeriod = courseChapterPeriod;
            this.course = course;
            this.mobile = mobile;
            this.chapterName = chapterName;
        }

        @Override
        public void run() {
            // 更新学习人数
            updateCount(course);

            // 学习日志与统计
            // studyCount(authCourseSignBO, courseChapterPeriod, course, chapterName, mobile);
        }

        /**
         * 更新学习人数
         */
        private Integer updateCount(Course course) {
            Course record = new Course();
            record.setId(course.getId());
            record.setCountStudy(course.getCountStudy() + 1);
            return courseDao.updateById(record);
        }

        /*private void studyCount(AuthCourseSignBO authCourseSignBO, CourseChapterPeriod courseChapterPeriod, Course course, String chapterName, String mobile) {
            // 查找课程用户关联表
            CourseLogQO record = new CourseLogQO();
            record.setUserNo(authCourseSignBO.getUserNo());
            record.setPeriodId(authCourseSignBO.getPeriodId());
            CourseLogVO courseUserStudy = feignCourseLog.getByUserNoAndPeriodId(record);
            // 如果不存在记录
            record.setUserNo(authCourseSignBO.getUserNo());
            record.setMobile(mobile);
            record.setCourseName(course.getCourseName());
            record.setCourseId(courseChapterPeriod.getCourseId());
            record.setChapterId(courseChapterPeriod.getChapterId());
            record.setChapterName(chapterName);
            record.setPeriodName(courseChapterPeriod.getPeriodName());
            record.setVid(courseChapterPeriod.getVideoVid());
            record.setVidLength(courseChapterPeriod.getVideoLength());
            if (ObjectUtil.isNull(courseUserStudy)) {
                feignCourseLog.save(record);
            } else {
                record.setId(courseUserStudy.getId());
                feignCourseLog.updateById(record);
            }
        }*/
    }

    public Result<AuthCourseGetLiveUrlDTO> getLiveUrl(AuthCourseGetUrlBO authCourseGetUrlBO) {
        BeanValidateUtil.ValidationResult validationResult = BeanValidateUtil.validate(authCourseGetUrlBO);
        if (!validationResult.isPass()) {
            return Result.error(validationResult.getErrMsgString());
        }

        UserExtVO userextVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userextVO) || StatusIdEnum.NO.getCode().equals(userextVO.getStatusId())) {
            return Result.error("用户异常请联系，系统管理员");
        }

        CourseLiveLog courseLiveLog = courseLiveLogDao.getByPeriodIdAndLiveStatus(authCourseGetUrlBO.getPeriodId(), LiveStatusEnum.NOW.getCode());
        if (ObjectUtil.isNull(courseLiveLog)) {
            return Result.error("该直播未开始或已结束");
        }

        Course course = courseDao.getById(courseLiveLog.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到该课程");
        }
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod)) {
            return Result.error("找不到该课时信息");
        }
        // 会员课程免费，则直接设置为已支付
        if (!VipCheckUtil.checkVipIsFree(userextVO.getIsVip(), userextVO.getExpireTime(), course.getCourseDiscount())) {
            if (IsFreeEnum.CHARGE.getCode().equals(course.getIsFree()) && IsFreeEnum.CHARGE.getCode().equals(courseChapterPeriod.getIsFree())) {
                UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(userextVO.getUserNo(), courseLiveLog.getCourseId());
                if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
                    return Result.error("收费课程，请先购买");
                } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
                    // 课程过期
                    return Result.error("收费课程，请先购买");
                }
            }
        }

        if (LivePlatformEnum.POLYV.getCode().equals(courseLiveLog.getLivePlatform())) {
            // 处理保利威
            return polyvLiveUrl(courseLiveLog, authCourseGetUrlBO);
        } else if (LivePlatformEnum.TALK_FUN.getCode().equals(courseLiveLog.getLivePlatform())) {
            // 欢拓处理逻辑
            return talkFunLiveUrl(authCourseGetUrlBO, courseLiveLog, userextVO, course, courseChapterPeriod);
        } else {
            logger.error("课时：{}---直播平台：{}，没有实现处理", courseLiveLog.getPeriodId(), courseLiveLog.getLivePlatform());
            return Result.error("直播异常");
        }


    }

    public Result<Page<AuthCoursePageDTO>> page(AuthCoursePageBO bo) {
        if (bo.getCourseCategory() == null) {
            return Result.error("课程分类不能为空");
        }
        CourseExample example = new CourseExample();
        Criteria c = example.createCriteria();
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        c.andIsPutawayEqualTo(IsPutawayEnum.YES.getCode());
        c.andLecturerUserNoEqualTo(ThreadContext.userNo());
        c.andCourseCategoryEqualTo(bo.getCourseCategory());
        example.setOrderByClause(" course_sort asc, id desc ");
        Page<Course> page = courseDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
        return Result.success(PageUtil.transform(page, AuthCoursePageDTO.class));
    }

    public Result<String> getPlaybackUrl(AuthCourseGetUrlBO authCourseGetUrlBO) {
        if (authCourseGetUrlBO.getPeriodId() == null) {
            return Result.error("periodId不能为空");
        }
        UserExtVO userextVO = feignUserExt.getByUserNo(ThreadContext.userNo());
        if (ObjectUtil.isNull(userextVO) || StatusIdEnum.NO.getCode().equals(userextVO.getStatusId())) {
            return Result.error("用户异常请联系，系统管理员");
        }
        CourseLiveLog courseLiveLog = courseLiveLogDao.getByPeriodId(authCourseGetUrlBO.getPeriodId());
        if (ObjectUtil.isNull(courseLiveLog)) {
            return Result.error("获取不到该直播记录");
        }

        Course course = courseDao.getById(courseLiveLog.getCourseId());
        if (ObjectUtil.isNull(course)) {
            return Result.error("找不到该课程");
        }
        CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        if (ObjectUtil.isNull(courseChapterPeriod)) {
            return Result.error("找不到该课时信息");
        }
        if (!LiveStatusEnum.END.getCode().equals(courseChapterPeriod.getLiveStatus())) {
            return Result.error("直播回放未生成");
        }

        // 会员课程免费，则直接设置为已支付
        if (!VipCheckUtil.checkVipIsFree(userextVO.getIsVip(), userextVO.getExpireTime(), course.getCourseDiscount())) {
            if (IsFreeEnum.CHARGE.getCode().equals(course.getIsFree()) && IsFreeEnum.CHARGE.getCode().equals(courseChapterPeriod.getIsFree())) {
                UserOrderCourseRef userOrderCourseRef = userOrderCourseRefDao.getByUserNoAndRefId(ThreadContext.userNo(), courseLiveLog.getCourseId());
                if (ObjectUtil.isNull(userOrderCourseRef) || IsPayEnum.NO.getCode().equals(userOrderCourseRef.getIsPay())) {
                    return Result.error("收费课程，请先购买");
                } else if (System.currentTimeMillis() > userOrderCourseRef.getExpireTime().getTime()) {
                    // 课程过期
                    return Result.error("收费课程，请先购买");
                }
            }
        }

        if (LivePlatformEnum.POLYV.getCode().equals(courseLiveLog.getLivePlatform())) {
            return getPolyvPlayback(courseLiveLog, courseChapterPeriod, authCourseGetUrlBO);
        } else if (LivePlatformEnum.TALK_FUN.getCode().equals(courseLiveLog.getLivePlatform())) {
            return getTalkFunPlayback(courseLiveLog);
        } else {
            return Result.error("直播信息异常");
        }
    }

    /**
     * 获取播放sign值
     *
     * @return 签名值
     */
    private AuthCourseSignDTO getSign(AuthCourseSignBO authCourseSignBO, CourseChapterPeriod courseChapterPeriod) {
        CourseVideo courseVideo = courseVideoDao.getById(courseChapterPeriod.getVideoId());
        if (ObjectUtil.isNull(courseVideo)) {
            logger.error("课程视频[ID:{}]不存在", courseChapterPeriod.getVideoId());
            throw new BaseException("课程视频不存在");
        }
        if (!VideoStatusEnum.SUCCES.getCode().equals(courseVideo.getVideoStatus())) {
            logger.error("课程视频[ID:{}]状态为[{}]未上传成功，无法播放", courseVideo.getId(), courseVideo.getVideoStatus());
            throw new BaseException("当前视频无法播放");
        }

        if (VodPlatformEnum.POLYV.getCode().equals(courseVideo.getVodPlatform())) {
            // 保利威
            return getPolyvPlaySign(authCourseSignBO, courseChapterPeriod);
        } else if (VodPlatformEnum.ALI_YUN_VOD.getCode().equals(courseVideo.getVodPlatform())) {
            // 阿里云点播
            return getAliYunPlaySource(authCourseSignBO, courseChapterPeriod);
        } else {
            logger.error("视频平台[CODE:{}]对应的播放业务逻辑暂未实现", courseVideo.getVodPlatform());
            throw new BaseException("播放异常，请联系管理员");
        }
    }

    public Result<AuthChannelViewDTO> channelview(AuthChannelViewBO bo) {
        if (bo.getPeriodId() == null) {
            return Result.error("课时id不能为空");
        }

        CourseLiveLog liveLog = courseLiveLogDao.getByPeriodIdAndLiveStatus(bo.getPeriodId(), LiveStatusEnum.NOT.getCode());
        if (ObjectUtil.isNull(liveLog)) {
            liveLog = courseLiveLogDao.getByPeriodIdAndLiveStatus(bo.getPeriodId(), LiveStatusEnum.NOW.getCode());
        }
        if (ObjectUtil.isNull(liveLog) || !liveLog.getLiveScene().equals(bo.getScene())) {
            return Result.error("课时该直播场景未设置开播");
        }

        // 获取直播信息
        if (LivePlatformEnum.POLYV.getCode().equals(liveLog.getLivePlatform())) {
            return getPolyvChannelView(liveLog);
        } else if (LivePlatformEnum.TALK_FUN.getCode().equals(liveLog.getLivePlatform())) {
            return getTalkFunChannelView(bo, liveLog);
        } else {
            return Result.error("视频直播设置异常，请联系管理员");
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> updateChannelPasswd(AuthUpdateChannelPasswdBO bo) {
        BeanValidateUtil.ValidationResult validationResult = BeanValidateUtil.validate(bo);
        if (!validationResult.isPass()) {
            return Result.error(validationResult.getErrMsgString());
        }

        CourseLiveLog liveLog = courseLiveLogDao.getByLivePlatformAndChannelId(LivePlatformEnum.POLYV.getCode(), bo.getChannelId());
        if (ObjectUtil.isNull(liveLog)) {
            return Result.error("直播频道不存在");
        }

        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || org.apache.commons.lang.StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || org.apache.commons.lang.StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return Result.error("保利威配置错误");
        }

        PLChannelGetResult plChannelGetResult = PLChannelUtil.getChannel(bo.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isEmpty(plChannelGetResult)) {
            return Result.error("获取不到该频道信息!");
        }

        //更新普通直播密码
        CourseLiveLog updateLiveLog = new CourseLiveLog();
        updateLiveLog.setId(liveLog.getId());
        updateLiveLog.setChannelPasswd(bo.getPasswd());
        int result = courseLiveLogDao.updateById(updateLiveLog);


        PLChannelUtil.passwdSetting(bo.getChannelId(), bo.getPasswd(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvUseid(), configPolyvVO.getPolyvAppSecret());
        if (result > 0) {
            return Result.success(result);
        }
        return Result.error(ResultEnum.USER_UPDATE_FAIL);
    }

    /**
     * 获取保利威直播观看地址
     *
     * @param courseLiveLog      课程直播记录
     * @param authCourseGetUrlBO 获取播放地址参数
     * @return 播放观看地址
     */
    private Result<AuthCourseGetLiveUrlDTO> polyvLiveUrl(CourseLiveLog courseLiveLog, AuthCourseGetUrlBO authCourseGetUrlBO) {
        // 保利威特有
        LiveCallbackREQ req = new LiveCallbackREQ();
        req.setChannelId(courseLiveLog.getChannelId());
        req.setPeriodId(courseLiveLog.getPeriodId());
        myRedisTemplate.setByJson(RedisPreEnum.LIVE_CHANNEL.getCode() + ThreadContext.userNo().toString(), req, 1, TimeUnit.MINUTES);

        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return Result.error("保利威配置错误");
        }
        if (StringUtils.isEmpty(configPolyvVO.getPolyvLiveDomain())) {
            return Result.error("直播域名不能为空");
        }

        PLChannelWatchAuthResult result = PLChannelUtil.watchAuthChannel(courseLiveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(result)) {
            return Result.error("找不到频道观看条件");
        }

        Long ts = System.currentTimeMillis();
        String url = SystemUtil.POLYV_LIVE_URL.replace("{domain}", configPolyvVO.getPolyvLiveDomain()).replace("{channelId}", courseLiveLog.getChannelId()).replace("{userid}", ThreadContext.userNo().toString()).replace("{ts}", ts.toString()).replace("{sign}", PLChannelUtil.getSign(result.getExternalKey(), ThreadContext.userNo().toString(), ts));

        AuthCourseGetLiveUrlDTO dto = new AuthCourseGetLiveUrlDTO();
        dto.setLivePlatform(LivePlatformEnum.POLYV.getCode());
        dto.setLiveScene(courseLiveLog.getLiveScene());
        dto.setLiveParam(courseLiveLog.getChannelId());
        dto.setLiveUrl(url);
        return Result.success(dto);
    }

    /**
     * 获取欢拓直播观看地址
     *
     * @param courseLiveLog 课程直播记录
     * @param userextVO     用户教育信息
     * @return 直播观看地址
     */
    private Result<AuthCourseGetLiveUrlDTO> talkFunLiveUrl(AuthCourseGetUrlBO bo, CourseLiveLog courseLiveLog, UserExtVO userextVO, Course course, CourseChapterPeriod courseChapterPeriod) {
        ConfigTalkFunVO sysVo = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(sysVo) || StrUtil.isBlank(sysVo.getTalkFunOpenId()) || StrUtil.isBlank(sysVo.getTalkFunOpenToken())) {
            return Result.error("直播参数配置有误");
        }
        CourseAccessRequest request = new CourseAccessRequest();
        request.setCourseId(Integer.parseInt(courseLiveLog.getChannelId()));
        request.setUid(String.valueOf(userextVO.getUserNo()));
        if (StrUtil.isNotBlank(userextVO.getNickname())) {
            request.setNickname(userextVO.getNickname());
        } else {
            request.setNickname(String.valueOf(userextVO.getUserNo()));
        }
        request.setRole("user");

        CourseAccessRequest.Option option = new CourseAccessRequest.Option();
        option.setTimes(1);
        option.setSsl(true);
        request.setOption(option);
        CourseAccessResponse response = TalkFunUtil.courseAccess(request, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(response)) {
            return Result.error("获取直播信息异常");
        }

        // 获取课程章节
        CourseChapter courseChapter = courseChapterDao.getById(courseChapterPeriod.getChapterId());
        if (ObjectUtil.isNull(courseChapter) || !StatusIdEnum.YES.getCode().equals(courseChapter.getStatusId())) {
            return Result.error("章节信息不存在或不可用");
        }

        CourseChapterPeriodAudit courseChapterPeriodAudit = courseChapterPeriodAuditDao.getById(courseChapterPeriod.getId());
        if (ObjectUtil.isNull(courseChapterPeriodAudit) || StatusIdEnum.NO.getCode().equals(courseChapterPeriodAudit.getStatusId())) {
            logger.error("找不到审核课时信息");
            return Result.error("找不到课时信息");
        }
        // 更新学习人数
        updateStudyNum(userextVO, course, courseChapter, courseChapterPeriodAudit, courseChapterPeriod);

        AuthCourseGetLiveUrlDTO dto = new AuthCourseGetLiveUrlDTO();
        dto.setLivePlatform(LivePlatformEnum.TALK_FUN.getCode());
        dto.setLiveScene(courseLiveLog.getLiveScene());
        dto.setLiveParam(response.getAccessToken());
        dto.setLiveUrl(response.getLiveUrl());
        return Result.success(dto);
    }

    /**
     * 获取保利威回放地址
     *
     * @param courseLiveLog       课程直播信息
     * @param courseChapterPeriod 课时信息
     * @param authCourseGetUrlBO  请求参数
     * @return 回放地址
     */
    private Result<String> getPolyvPlayback(CourseLiveLog courseLiveLog, CourseChapterPeriod courseChapterPeriod, AuthCourseGetUrlBO authCourseGetUrlBO) {
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvAppID()) || StringUtils.isEmpty(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return Result.error("保利威配置错误");
        }
        PLChannelWatchAuthResult result = PLChannelUtil.watchAuthChannel(courseLiveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(result)) {
            return Result.error("找不到频道观看条件");
        }

        LiveCallbackREQ req = new LiveCallbackREQ();
        req.setChannelId(courseLiveLog.getChannelId());
        req.setPeriodId(courseLiveLog.getPeriodId());
        myRedisTemplate.setByJson(RedisPreEnum.LIVE_CHANNEL.getCode() + ThreadContext.userNo().toString(), req, 1, TimeUnit.MINUTES);
        Long ts = System.currentTimeMillis();
        String url = SystemUtil.POLYV_PLAYBACK_URL.replace("{domain}", configPolyvVO.getPolyvLiveDomain()).replace("{channelId}", courseLiveLog.getChannelId()).replace("{channelId}", courseLiveLog.getChannelId()).replace("{vid}", courseChapterPeriod.getLiveVid()).replace("{userid}", ThreadContext.userNo().toString()).replace("{ts}", String.valueOf(ts)).replace("{sign}", PLChannelUtil.getSign(result.getExternalKey(), ThreadContext.userNo().toString(), ts));
        return Result.success(url);
    }

    /**
     * 获取欢拓直播回放地址
     *
     * @param courseLiveLog 课程直播记录
     * @return 直播回放地址
     */
    private Result<String> getTalkFunPlayback(CourseLiveLog courseLiveLog) {
        ConfigTalkFunVO sysVo = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(sysVo) || StrUtil.isBlank(sysVo.getTalkFunOpenId()) || StrUtil.isBlank(sysVo.getTalkFunOpenToken())) {
            return Result.error("直播参数配置有误");
        }
        CourseVideoRequest request = new CourseVideoRequest();
        request.setCourseId(Integer.parseInt(courseLiveLog.getChannelId()));

        CourseVideoRequest.Option option = new CourseVideoRequest.Option();
        option.setDownload(false);
        request.setOption(option);
        List<String> urlList = TalkFunUtil.courseVideo(request, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        return CollectionUtil.isEmpty(urlList) ? Result.error("获取播放地址失败") : Result.success(urlList.get(0));
    }

    /**
     * 更新学习人数
     *
     * @param userextVO                用户教育信息
     * @param course                   课程信息
     * @param courseChapter            章节信息
     * @param courseChapterPeriodAudit 课时审核信息
     * @param courseChapterPeriod      课时信息
     */
    private void updateStudyNum(UserExtVO userextVO, Course course, CourseChapter courseChapter, CourseChapterPeriodAudit courseChapterPeriodAudit, CourseChapterPeriod courseChapterPeriod) {
        // 更新课时学生人数
        courseChapterPeriod.setCountStudy(courseChapterPeriod.getCountStudy() + 1);
        courseChapterPeriodDao.updateById(courseChapterPeriod);

        // 更新课程学生人数
        course.setCountStudy(course.getCountStudy() + 1);
        courseDao.updateById(course);

        // 更新课时学生人数
        courseChapterPeriodAudit.setCountStudy(courseChapterPeriodAudit.getCountStudy() + 1);
        courseChapterPeriodAuditDao.updateById(courseChapterPeriodAudit);

        // 记录课程学习日志
        CourseUserStudyLog courseUserStudyLog = new CourseUserStudyLog();
        courseUserStudyLog.setCourseId(course.getId());
        courseUserStudyLog.setCourseName(course.getCourseName());
        courseUserStudyLog.setChapterId(courseChapter.getId());
        courseUserStudyLog.setChapterName(courseChapter.getChapterName());
        courseUserStudyLog.setPeriodId(courseChapterPeriod.getId());
        courseUserStudyLog.setPeriodName(courseChapterPeriod.getPeriodName());
        courseUserStudyLog.setUserNo(userextVO.getUserNo());
        courseUserStudyLog.setCourseCategory(CourseCategoryEnum.LIVE.getCode());
        courseUserStudyLogDao.save(courseUserStudyLog);
    }

    /**
     * 获取保利威频道信息
     *
     * @param liveLog 直播记录
     * @return 频道信息
     */
    private Result<AuthChannelViewDTO> getPolyvChannelView(CourseLiveLog liveLog) {
        ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
        if (ObjectUtil.isEmpty(configPolyvVO) || StrUtil.isBlank(configPolyvVO.getPolyvAppID()) || StrUtil.isBlank(configPolyvVO.getPolyvAppSecret())) {
            logger.error("保利威配置错误，参数={}", configPolyvVO);
            return Result.error("保利威配置错误");
        }

        PLChannelGetAccounts plChannelGetAccounts = PLChannelUtil.accounts(liveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(plChannelGetAccounts)) {
            return Result.error("获取频道助教信息失败");
        }
        PLChannelGetResult plChannelGetResult = PLChannelUtil.getChannel(liveLog.getChannelId(), configPolyvVO.getPolyvAppID(), configPolyvVO.getPolyvAppSecret());
        if (ObjectUtil.isNull(plChannelGetResult)) {
            return Result.error("获取频道信息失败");
        }
        AuthChannelViewDTO dto = new AuthChannelViewDTO();
        dto.setLivePlatform(LivePlatformEnum.POLYV.getCode());
        dto.setChannelId(liveLog.getChannelId());
        // 获取助教信息
        dto.setChannelPasswd(plChannelGetResult.getChannelPasswd());
        dto.setAssistantChannelId(plChannelGetAccounts.getAccount());
        dto.setAssistantPasswd(plChannelGetAccounts.getPasswd());
        dto.setPushFlowUrl(plChannelGetResult.getUrl());
        dto.setScene(plChannelGetResult.getScene());
        dto.setAssistantUrl("https://live.polyv.net/teacher.html");
        return Result.success(dto);
    }

    /**
     * 获取欢拓频道信息
     *
     * @param bo      查看请求参数
     * @param liveLog 直播记录
     * @return 频道信息
     */
    private Result<AuthChannelViewDTO> getTalkFunChannelView(AuthChannelViewBO bo, CourseLiveLog liveLog) {
        ConfigTalkFunVO sysVo = feignSysConfig.getTalkFun();
        if (ObjectUtil.isNull(sysVo) || StrUtil.isBlank(sysVo.getTalkFunOpenId()) || StrUtil.isBlank(sysVo.getTalkFunOpenToken())) {
            return Result.error("直播配置有误");
        }

        Integer courseId = Integer.parseInt(liveLog.getChannelId());

        // 获取外部推流地址
        CoursePushRtmpUrlResponse pushRtmpUrlResponse = TalkFunUtil.coursePushRtmpUrl(courseId, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(pushRtmpUrlResponse)) {
            logger.error("获取外部推流地址有误");
            return Result.error("获取直播地址有误");
        }

        // 获取主播授权登录路径
        CourseLaunchResponse launchResponse = TalkFunUtil.courseLaunch(courseId, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(launchResponse)) {
            logger.error("获取主播授权登录路径有误");
            return Result.error("获取直播地址有误");
        }

        // 获取助教授权登录地址
        CourseAccessRequest accessRequest = new CourseAccessRequest();
        accessRequest.setCourseId(courseId);
        accessRequest.setUid(String.valueOf(courseId));
        accessRequest.setNickname("助教");
        accessRequest.setRole("admin");
        CourseAccessRequest.Option option = new CourseAccessRequest.Option();
        option.setTimes(1);
        option.setSsl(true);
        CourseAccessResponse accessResponse = TalkFunUtil.courseAccess(accessRequest, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(accessResponse)) {
            return Result.error("助教信息获取失败");
        }

        CourseGetRequest getRequest = new CourseGetRequest();
        getRequest.setCourseId(Integer.parseInt(liveLog.getChannelId()));
        CourseGetResponse getResponse = TalkFunUtil.courseGet(getRequest, sysVo.getTalkFunOpenId(), sysVo.getTalkFunOpenToken());
        if (ObjectUtil.isNull(getResponse)) {
            return Result.error("直播信息获取失败");
        }

        // 返回直播频道信息
        AuthChannelViewDTO dto = new AuthChannelViewDTO();
        dto.setChannelId(liveLog.getChannelId());
        dto.setChannelPasswd(liveLog.getChannelPasswd());
        dto.setBid(getResponse.getBid());
        dto.setLivePlatform(LivePlatformEnum.TALK_FUN.getCode());

        // 外部直播推流地址
        dto.setScene(bo.getScene());
        dto.setWebLiveUrl(launchResponse.getUrl());
        dto.setPushFlowUrl(pushRtmpUrlResponse.getPushAddr());
        dto.setAssistantUrl(accessResponse.getLiveUrl());

        // 直播授权地址
        dto.setLiveUrl(launchResponse.getUrl());
        return Result.success(dto);
    }

    private AuthCourseSignDTO getPolyvPlaySign(AuthCourseSignBO authCourseSignBO, CourseChapterPeriod courseChapterPeriod) {
        ConfigPolyvVO sys = feignSysConfig.getPolyv();
        if (ObjectUtil.isNull(sys)) {
            logger.error("保利威点播--找不到系统配置信息");
            throw new BaseException("找不到系统配置信息");
        }
        if (StringUtils.isEmpty(sys.getPolyvUseid()) || StringUtils.isEmpty(sys.getPolyvSecretkey())) {
            logger.error("保利威点播--polyvUseid和polyvSecretkey未配置");
            throw new BaseException("useid或secretkey未配置");
        }
        PolyvSign polyvSign = new PolyvSign();
        polyvSign.setIp(authCourseSignBO.getIp());
        polyvSign.setUserNo(ThreadContext.userNo());
        polyvSign.setVid(courseChapterPeriod.getVideoVid());
        PolyvSignResult signResult = PolyvUtil.getSignForH5(polyvSign, sys.getPolyvUseid(), sys.getPolyvSecretkey());

        AuthCourseSignDTO dto = BeanUtil.copyProperties(signResult, AuthCourseSignDTO.class);
        dto.setVodPlatform(VodPlatformEnum.POLYV.getCode());
        PolyvCode polyvCode = new PolyvCode();
        polyvCode.setPeriodId(authCourseSignBO.getPeriodId());
        polyvCode.setUserNo(ThreadContext.userNo());
        polyvCode.setProblemId(0L);
        polyvCode.setExamId(0L);
        dto.setCode(PolyvUtil.getPolyvCode(polyvCode));

        //人脸对比剩余次数
        CourseLogQO courseLogQO = new CourseLogQO();
        courseLogQO.setUserNo(ThreadContext.userNo());
        courseLogQO.setPeriodId(authCourseSignBO.getPeriodId());
        CourseLogVO courseLogVO = feignCourseLog.getByUserNoAndPeriodIdLatest(courseLogQO);
        if (ObjectUtil.isNotEmpty(courseLogVO) && IsFaceContrasEnum.YES.getCode().equals(courseChapterPeriod.getIsFaceContras())) {
            dto.setResidueContrastTotal(courseLogVO.getResidueContrastTotal());
            dto.setResidueContrastTotal(3);
        }
        //如果课时不需要人脸对比则直接设置剩余次数为0次
        if (IsFaceContrasEnum.NO.getCode().equals(courseChapterPeriod.getIsFaceContras())) {
            dto.setResidueContrastTotal(0);
        }
        return dto;
    }

    private AuthCourseSignDTO getAliYunPlaySource(AuthCourseSignBO authCourseSignBO, CourseChapterPeriod courseChapterPeriod) {
        ConfigAliYunVodVO sysVo = feignSysConfig.getAliYunVod();
        if (ObjectUtil.isNull(sysVo)) {
            logger.error("阿里云点播--找不到阿里云点播配置");
            throw new BaseException("获取配置为空");
        }

        // 获取播放地址
        GetPlayInfoRequest getPlayInfoRequest = new GetPlayInfoRequest();
        getPlayInfoRequest.setVideoId(courseChapterPeriod.getVideoVid());
        getPlayInfoRequest.setStreamType("video");
        GetPlayInfoResponse getPlayInfoResponse = AliYunVodUtil.getPlayInfo(getPlayInfoRequest, sysVo.getAliYunVodAccessKeyId(), sysVo.getAliYunVodAccessKeySecret());
        if (ObjectUtil.isNull(getPlayInfoResponse)) {
            logger.error("阿里云点播--获取视频播放地址返回为空");
            throw new BaseException("获取播放信息失败");
        }
        if (CollectionUtil.isEmpty(getPlayInfoResponse.getPlayInfoList())) {
            logger.error("阿里云点播--视频[VID:{}]对应视频不存在", courseChapterPeriod.getVideoVid());
            throw new BaseException("视频信息不存在");
        }

        // 生成令牌
        String token = IdWorker.get32UUID();
        AliYunVodCode aliYunVodCode = new AliYunVodCode(ThreadContext.userNo(), courseChapterPeriod.getId(), null, null);
        myRedisTemplate.set(RedisPreEnum.ALI_YUN_VOD_PLAY.getCode() + token, JSON.toJSONString(aliYunVodCode), 10, TimeUnit.MINUTES);

        // 生成播放视频源
        JSONObject sourceJson = new JSONObject();
        List<GetPlayInfoResponse.PlayInfo> playInfoList = getPlayInfoResponse.getPlayInfoList();
        for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
            sourceJson.put(playInfo.getDefinition(), playInfo.getPlayURL() + "?token=" + token);
        }

        AuthCourseSignDTO dto = new AuthCourseSignDTO();
        dto.setVodPlatform(VodPlatformEnum.ALI_YUN_VOD.getCode());
        dto.setCoverUrl(getPlayInfoResponse.getVideoBase().getCoverURL());
        dto.setSource(sourceJson.toJSONString());

        //人脸对比剩余次数
        CourseLogQO courseLogQO = new CourseLogQO();
        courseLogQO.setUserNo(ThreadContext.userNo());
        courseLogQO.setPeriodId(authCourseSignBO.getPeriodId());
        CourseLogVO courseLogVO = feignCourseLog.getByUserNoAndPeriodIdLatest(courseLogQO);
        if (ObjectUtil.isNotEmpty(courseLogVO) && IsFaceContrasEnum.YES.getCode().equals(courseChapterPeriod.getIsFaceContras())) {
            dto.setResidueContrastTotal(courseLogVO.getResidueContrastTotal());
            dto.setResidueContrastTotal(3);
        }
        //如果课时不需要人脸对比则直接设置剩余次数为0次
        if (IsFaceContrasEnum.NO.getCode().equals(courseChapterPeriod.getIsFaceContras())) {
            dto.setResidueContrastTotal(0);
        }
        return dto;
    }

}
