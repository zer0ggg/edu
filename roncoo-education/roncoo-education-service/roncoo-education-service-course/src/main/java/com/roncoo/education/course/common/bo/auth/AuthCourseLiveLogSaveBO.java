package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 直播记录保存
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthCourseLiveLogSaveBO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "课时ID不能为空")
    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;

    @NotNull(message = "直播场景不能为空")
    @ApiModelProperty(value = "直播场景：alone 活动直播, ppt 三分屏, largeInteractive 大班互动", required = true)
    private String scene;

    @ApiModelProperty(value = "小班课类型： 1 1V1，2 1V6，3 1V16(默认)", required = true)
    private Integer smallType = 3;
}
