package com.roncoo.education.course.service.api;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.CourseCommentPageBO;
import com.roncoo.education.course.common.dto.CourseCommentPageDTO;
import com.roncoo.education.course.service.api.biz.ApiCourseCommentBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 课程评论 Api接口
 *
 * @author Quanf
 * @date 2020-09-03
 */
@Api(tags = "API-课程评论")
@RestController
@RequestMapping("/course/api/courseComment")
public class ApiCourseCommentController {

    @Autowired
    private ApiCourseCommentBiz biz;

    @ApiOperation(value = "课程评论信息展示接口", notes = "课程评论信息展示接口")
    @PostMapping(value = "/list")
    public Result<Page<CourseCommentPageDTO>> list(@RequestBody @Valid CourseCommentPageBO courseCommentPageBO) {
        return biz.list(courseCommentPageBO);
    }
}
