package com.roncoo.education.course.service.feign.biz;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.enums.*;
import com.roncoo.education.common.core.tools.IdWorker;
import com.roncoo.education.common.polyv.PolyvUtil;
import com.roncoo.education.common.polyv.UploadFileResult;
import com.roncoo.education.course.feign.qo.PolyvVideoQO;
import com.roncoo.education.course.service.dao.CourseChapterPeriodAuditDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseLiveLogDao;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodAudit;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseLiveLog;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.exam.feign.interfaces.IFeignExamProblem;
import com.roncoo.education.exam.feign.interfaces.IFeignExamVideo;
import com.roncoo.education.exam.feign.qo.ExamProblemQO;
import com.roncoo.education.exam.feign.qo.ExamVideoQO;
import com.roncoo.education.system.feign.interfaces.IFeignSysConfig;
import com.roncoo.education.system.feign.vo.ConfigPolyvVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 保利威直播回调
 *
 * @author System
 */
@Component
public class FeignCallbackPolyvBiz extends BaseBiz {

    @Autowired
    private IFeignSysConfig feignSysConfig;
    @Autowired
    private IFeignExamVideo feignExamVideo;
    @Autowired
    private IFeignExamProblem feignExamProblem;

    @Autowired
    private CourseChapterPeriodDao courseChapterPeriodDao;
    @Autowired
    private CourseVideoDao courseVideoDao;
    @Autowired
    private CourseChapterPeriodAuditDao courseChapterPeriodAuditDao;
    @Autowired
    private CourseLiveLogDao courseLiveLogDao;

    /**
     * 保利威视，视频上传回调接口
     * <p>
     * tag为视频类型 tag有可能为空，直播生成回放转存已经处理了时长(但针对三分屏的转存并不是)
     * state为视频id（当前端直接上传保利威时不存在该参数）
     *
     * @return 处理结果
     */
    public String callbackPolyvVideo(PolyvVideoQO qo) {
        logger.info("上传视频回调参数,参数={}", JSONUtil.toJsonStr(qo));
        if ("pass".equals(qo.getType())) {
            logger.warn("保利威视-上传视频-回调成功：{}", qo);
            ConfigPolyvVO configPolyvVO = feignSysConfig.getPolyv();
            if (ObjectUtil.isEmpty(configPolyvVO) || StringUtils.isEmpty(configPolyvVO.getPolyvSecretkey()) || StringUtils.isEmpty(configPolyvVO.getPolyvUseid())) {
                logger.error("保利威配置错误，参数={}", configPolyvVO);
                return "fail";
            }
            UploadFileResult uploadFileResult = PolyvUtil.getVideo(qo.getVid(), configPolyvVO.getPolyvSecretkey(), configPolyvVO.getPolyvUseid());
            if (ObjectUtil.isNull(uploadFileResult)) {
                logger.warn("获取不到该视频信息，vid={}", qo.getVid());
                return "fail";
            }

            if (VideoTagEnum.EXAM.getCode().equals(uploadFileResult.getTag())) {
                // 试卷视频上传
                return examUploadCallbackHandle(qo, uploadFileResult);
            } else if (VideoTagEnum.LIVE_PLAYBACK.getCode().equals(uploadFileResult.getTag())) {
                // 直播点转播
                return courseLivePlaybackHandle(qo, uploadFileResult);
            } else {
                // 课程视频、直播转存
                return courseUploadCallbackHandle(qo, uploadFileResult);
            }
        }
        return "success";
    }

    /**
     * 试卷视频上传回调处理
     *
     * @param qo               回调参数
     * @param uploadFileResult 上传文件结果
     * @return 处理结果
     */
    private String examUploadCallbackHandle(PolyvVideoQO qo, UploadFileResult uploadFileResult) {
        ExamVideoQO examVideo = new ExamVideoQO();
        examVideo.setVideoLength(uploadFileResult.getDuration());
        examVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());

        ExamProblemQO examProblem = new ExamProblemQO();
        examProblem.setVideoLength(uploadFileResult.getDuration());

        //state为空，前端上传视频保利威，不存在视频id, 通过vid更新时长
        if (StringUtils.isEmpty(qo.getState())) {
            examVideo.setVideoVid(qo.getVid());
            feignExamVideo.updateByVid(examVideo);

            examProblem.setVideoVid(qo.getVid());
            feignExamProblem.updateByVid(examProblem);
        } else {
            //前端上传阿里云回调流程(未存在视频vid、时长信息，需要通过视频id更新)
            examVideo.setId(Long.valueOf(qo.getState()));
            examVideo.setVideoVid(qo.getVid());
            feignExamVideo.updateById(examVideo);

            examProblem.setVideoId(Long.valueOf(qo.getState()));
            examProblem.setVideoVid(qo.getVid());
            feignExamProblem.updateByVideoId(examProblem);
        }

        return "success";
    }

    private String courseLivePlaybackHandle(PolyvVideoQO qo, UploadFileResult uploadFileResult) {

        //根据频道号和场次id获取直播记录
        CourseLiveLog courseLiveLog = courseLiveLogDao.getById(Long.valueOf(qo.getState()));

        Long vId = IdWorker.getId();
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());
        courseVideo.setChapterId(courseLiveLog.getChapterId());
        courseVideo.setCourseId(courseLiveLog.getCourseId());
        courseVideo.setId(vId);
        courseVideo.setVodPlatform(VodPlatformEnum.POLYV.getCode());
        courseVideo.setStatusId(StatusIdEnum.YES.getCode());
        courseVideo.setVideoLength(uploadFileResult.getDuration());
        courseVideo.setVideoName(uploadFileResult.getTitle());
        courseVideo.setVideoVid(uploadFileResult.getVid());
        courseVideoDao.save(courseVideo);

        CourseChapterPeriodAudit periodAudit = courseChapterPeriodAuditDao.getById(courseLiveLog.getPeriodId());
        periodAudit.setVideoId(vId);
        periodAudit.setIsVideo(IsVideoEnum.YES.getCode());
        periodAudit.setVideoVid(uploadFileResult.getVid());
        periodAudit.setVideoName(uploadFileResult.getTitle());
        periodAudit.setVideoLength(uploadFileResult.getDuration());
        periodAudit.setLiveStatus(LiveStatusEnum.END.getCode());
        //直播存放的id
        periodAudit.setLiveVid(uploadFileResult.getVid());
        courseChapterPeriodAuditDao.updateById(periodAudit);

        CourseChapterPeriod period = courseChapterPeriodDao.getById(courseLiveLog.getPeriodId());
        period.setVideoId(vId);
        period.setIsVideo(IsVideoEnum.YES.getCode());
        period.setVideoVid(uploadFileResult.getVid());
        period.setVideoName(uploadFileResult.getTitle());
        period.setVideoLength(uploadFileResult.getDuration());
        period.setLiveStatus(LiveStatusEnum.END.getCode());
        //直播存放的id
        period.setLiveVid(uploadFileResult.getVid());
        courseChapterPeriodDao.updateById(period);

        // 更改状态为结束
        courseLiveLog.setLiveStatus(LiveStatusEnum.END.getCode());
        int result = courseLiveLogDao.updateById(courseLiveLog);
        if (result > 0) {
            return "success";
        } else {
            logger.error("保利威视，转存成功回调失败");
            return "fail";
        }
    }

    /**
     * 课程视频、直播回放上传回调处理
     *
     * @param qo               回调参数
     * @param uploadFileResult 上传文件结果
     * @return 处理结果
     */
    private String courseUploadCallbackHandle(PolyvVideoQO qo, UploadFileResult uploadFileResult) {
        CourseVideo courseVideo = new CourseVideo();
        courseVideo.setVideoLength(uploadFileResult.getDuration());
        courseVideo.setVideoStatus(VideoStatusEnum.SUCCES.getCode());

        CourseChapterPeriodAudit courseChapterPeriodAudit = new CourseChapterPeriodAudit();
        courseChapterPeriodAudit.setVideoLength(uploadFileResult.getDuration());
        CourseChapterPeriod courseChapterPeriod = new CourseChapterPeriod();
        courseChapterPeriod.setVideoLength(uploadFileResult.getDuration());

        //state为空，前端上传视频保利威，不存在视频id, 通过vid更新时长
        if (StringUtils.isEmpty(qo.getState())) {
            //更新视频信息
            courseVideo.setVideoVid(qo.getVid());
            courseVideoDao.updateDurationByVid(courseVideo);

            //更新关联该视频的课时审核信息
            courseChapterPeriodAudit.setVideoVid(qo.getVid());
            courseChapterPeriodAuditDao.updateByVid(courseChapterPeriodAudit);

            // 更新关联该视频的课时信息
            courseChapterPeriod.setVideoVid(qo.getVid());
            courseChapterPeriodDao.updateByVid(courseChapterPeriod);
        } else {
            //前端上传阿里云回调流程(未存在视频vid、时长信息，需要通过视频id更新)

            // 更新视频信息
            courseVideo.setId(Long.valueOf(qo.getState()));
            courseVideo.setVideoVid(qo.getVid());
            courseVideoDao.updateById(courseVideo);

            // 更新关联该视频的课时审核信息
            courseChapterPeriodAudit.setVideoId(Long.valueOf(qo.getState()));
            courseChapterPeriodAudit.setVideoVid(qo.getVid());
            courseChapterPeriodAuditDao.updateByVideoId(courseChapterPeriodAudit);

            // 更新关联该视频的课时信息
            courseChapterPeriod.setVideoId(Long.valueOf(qo.getState()));
            courseChapterPeriod.setVideoVid(qo.getVid());
            courseChapterPeriodDao.updateByVideoId(courseChapterPeriod);
        }
        return "success";
    }

}
