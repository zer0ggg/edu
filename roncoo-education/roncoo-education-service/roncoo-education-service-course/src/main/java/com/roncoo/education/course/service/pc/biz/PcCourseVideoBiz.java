package com.roncoo.education.course.service.pc.biz;

import com.roncoo.education.common.core.base.BaseBiz;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.core.tools.BeanUtil;
import com.roncoo.education.course.common.req.CourseVideoEditREQ;
import com.roncoo.education.course.common.req.CourseVideoListREQ;
import com.roncoo.education.course.common.req.CourseVideoSaveREQ;
import com.roncoo.education.course.common.resp.CourseVideoListRESP;
import com.roncoo.education.course.common.resp.CourseVideoViewRESP;
import com.roncoo.education.course.service.dao.CourseVideoDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideo;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseVideoExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 课程视频信息
 *
 * @author wujing
 */
@Component
public class PcCourseVideoBiz extends BaseBiz {

    @Autowired
    private CourseVideoDao dao;

    /**
    * 课程视频信息列表
    *
    * @param req 课程视频信息分页查询参数
    * @return 课程视频信息分页查询结果
    */
    public Result<Page<CourseVideoListRESP>> list(CourseVideoListREQ req) {
        CourseVideoExample example = new CourseVideoExample();
        Criteria c = example.createCriteria();
        if (req.getChapterId() != null) {
            c.andChapterIdEqualTo(req.getChapterId());
        }
        c.andStatusIdEqualTo(StatusIdEnum.YES.getCode());
        Page<CourseVideo> page = dao.listForPage(req.getPageCurrent(), req.getPageSize(), example);
        Page<CourseVideoListRESP> respPage = PageUtil.transform(page, CourseVideoListRESP.class);
        return Result.success(respPage);
    }


    /**
    * 课程视频信息添加
    *
    * @param courseVideoSaveREQ 课程视频信息
    * @return 添加结果
    */
    public Result<String> save(CourseVideoSaveREQ courseVideoSaveREQ) {
        CourseVideo record = BeanUtil.copyProperties(courseVideoSaveREQ, CourseVideo.class);
        if (dao.save(record) > 0) {
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }


    /**
    * 课程视频信息查看
    *
    * @param id 主键ID
    * @return 课程视频信息
    */
    public Result<CourseVideoViewRESP> view(Long id) {
        return Result.success(BeanUtil.copyProperties(dao.getById(id), CourseVideoViewRESP.class));
    }


    /**
    * 课程视频信息修改
    *
    * @param courseVideoEditREQ 课程视频信息修改对象
    * @return 修改结果
    */
    public Result<String> edit(CourseVideoEditREQ courseVideoEditREQ) {
        CourseVideo record = BeanUtil.copyProperties(courseVideoEditREQ, CourseVideo.class);
        if (dao.updateById(record) > 0) {
            return Result.success("编辑成功");
        }
        return Result.error("编辑失败");
    }

    /**
    * 课程视频信息删除
    *
    * @param id ID主键
    * @return 删除结果
    */
    public Result<String> delete(Long id) {
        if (dao.deleteById(id) > 0) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }
}
