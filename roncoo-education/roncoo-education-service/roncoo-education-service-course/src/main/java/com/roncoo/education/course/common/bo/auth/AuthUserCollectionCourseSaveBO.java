package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用户收藏课程分类列表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserCollectionCourseSaveBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 收藏id
	 */
	@ApiModelProperty(value = "收藏id")
	private Long collectionId;
	/**
	 * 课程分类(1:普通课程;2:直播课程,4文库)
	 */
	@ApiModelProperty(value = "课程分类(1:普通课程;2:直播课程,4文库)", required = true)
	private Integer courseCategory;
	/**
	 * 收藏类型:1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "收藏类型:1课程，2章节，3课时", required = true)
	private Integer collectionType;

}
