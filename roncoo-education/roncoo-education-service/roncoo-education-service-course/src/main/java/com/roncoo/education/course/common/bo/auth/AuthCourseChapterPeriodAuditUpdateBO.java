package com.roncoo.education.course.common.bo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 课时信息-审核
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthCourseChapterPeriodAuditUpdateBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课时ID
     */
    @ApiModelProperty(value = "课时ID", required = true)
    private Long id;
    /**
     * 课时名称
     */
    @ApiModelProperty(value = "课时名称", required = true)
    private String periodName;
    /**
     * 课时描述
     */
    @ApiModelProperty(value = "课时描述", required = false)
    private String periodDesc;
    /**
     * 是否免费：1免费，0收费
     */
    @ApiModelProperty(value = "是否免费：1免费，0收费", required = true)
    private Integer isFree;
    /**
     * 直播开始时间
     */
    @ApiModelProperty(value = "直播开始时间")
    private String startTime;
    /**
     * 直播结束时间
     */
    @ApiModelProperty(value = "直播结束时间")
    private String endTime;
    /**
     * 是否需要人脸对比(0:否，1:是)
     */
    @ApiModelProperty(value = "是否需要人脸对比(0:否，1:是)")
    private Integer isFaceContras;
}
