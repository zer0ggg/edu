package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 直播回调使用
 */
@Data
@Accessors(chain = true)
public class LiveCallbackREQ implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "课时id", required = true)
    private Long periodId;

    @ApiModelProperty(value = "频道号", required = true)
    private String channelId;
}
