package com.roncoo.education.course.common.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 课程信息表
 *
 * @author wuyun
 */
@Document(indexName = EsCourse.COURSE)
public class EsCourse implements Serializable {
	
	public static final String COURSE = "edu_course";

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	private Long id;
	/**
	 * 创建时间
	 */
	private Date gmtCreate;
	/**
	 * 修改时间
	 */
	private Date gmtModified;
	/**
	 * 状态(1:正常，0:禁用)
	 */
	private Integer statusId;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 讲师名称
	 */
	private String lecturerName;
	/**
	 * 课程封面
	 */
	private String courseLogo;
	/**
	 * 课程名称
	 */
	private String courseName;
	/**
	 * 课程排序
	 */
	private Integer courseSort;
	/**
	 * 课程分类：1点播课程，2直播课程，3题库，4文库
	 */
	private Integer courseCategory;
	/**
	 * 原价
	 */
	private BigDecimal courseOriginal;
	/**
	 * 优惠价
	 */
	private BigDecimal courseDiscount;
	/**
	 * 是否免费(1:免费，0:收费)
	 */
	private Integer isFree;
	/**
	 * 是否上架(1:上架，0:下架)
	 */
	private Integer isPutaway;
	/**
	 * 学习人数
	 */
	private Integer countStudy;

	public EsCourse() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getCourseName() {
		return courseName;
	}

	public Integer getCourseCategory() {
		return courseCategory;
	}
	public void setCourseCategory(Integer courseCategory) {
		this.courseCategory = courseCategory;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Integer getIsFree() {
		return isFree;
	}

	public void setIsFree(Integer isFree) {
		this.isFree = isFree;
	}

	public Integer getCourseSort() {
		return courseSort;
	}

	public void setCourseSort(Integer courseSort) {
		this.courseSort = courseSort;
	}

	public String getLecturerName() {
		return lecturerName;
	}

	public void setLecturerName(String lecturerName) {
		this.lecturerName = lecturerName;
	}

	public String getCourseLogo() {
		return courseLogo;
	}

	public void setCourseLogo(String courselog) {
		this.courseLogo = courselog;
	}

	public BigDecimal getCourseOriginal() {
		return courseOriginal;
	}

	public void setCourseOriginal(BigDecimal courseOriginal) {
		this.courseOriginal = courseOriginal;
	}

	public BigDecimal getCourseDiscount() {
		return courseDiscount;
	}

	public void setCourseDiscount(BigDecimal courseDiscount) {
		this.courseDiscount = courseDiscount;
	}
	public Integer getCountStudy() {
		return countStudy;
	}

	public Integer getIsPutaway() {
		return isPutaway;
	}

	public void setIsPutaway(Integer isPutaway) {
		this.isPutaway = isPutaway;
	}

	public void setCountStudy(Integer countStudy) {
		this.countStudy = countStudy;
	}

	@Override
	public String toString() {
		return "EsCourse [id=" + id + ", gmtCreate=" + gmtCreate + ", gmtModified=" + gmtModified + ", statusId=" + statusId +", sort=" + sort + ", lecturerName=" + lecturerName + ", courseLogo=" + courseLogo + ", courseName=" + courseName + "courseCategory" + courseCategory + ", courseSort=" + courseSort + ", courseOriginal="
				+ courseOriginal + ", courseDiscount=" + courseDiscount + ", isFree=" + isFree + ", isPutaway" + isPutaway + ", countStudy=" + countStudy + "]";
	}

}
