package com.roncoo.education.course.service.dao;

import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommend;
import com.roncoo.education.course.service.dao.impl.mapper.entity.ResourceRecommendExample;

import java.util.List;

/**
 * <p>
 * 文库推荐 服务类
 * </p>
 *
 * @author wujing
 * @date 2020-03-02
 */
public interface ResourceRecommendDao {

    int save(ResourceRecommend record);

    int deleteById(Long id);

    int updateById(ResourceRecommend record);

    ResourceRecommend getById(Long id);

    Page<ResourceRecommend> listForPage(int pageCurrent, int pageSize, ResourceRecommendExample example);

    /**
     * 根据状态列出文库推荐
     * @param statusId
     * @return
     */
    List<ResourceRecommend> listByStatusId(Integer statusId);

    /**
     * 根据文库ID获取文库推荐
     * @param resourceId
     * @return
     */
    ResourceRecommend getByResourceId(Long resourceId);
}
