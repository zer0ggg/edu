package com.roncoo.education.course.common.bo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class UserCollectionCourseBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 状态(1:正常，0:禁用)
     */
    private Integer statusId;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 用户编号
     */
    private Long userNo;
    /**
     * 用户手机号码
     */
    private String mobile;
    /**
     * 课时id
     */
    private Long periodId;
    /**
     * 课时名称
     */
    private String periodName;
    /**
     * 章节编号
     */
    private Long chapterId;
    /**
     * 章节名称
     */
    private String chapterName;
    /**
     * 课程id
     */
    private Long courseId;
    /**
     * 课程名称
     */
    private String courseName;
    /**
     * 课程分类(1:普通课程;2:直播课程,3:试卷)
     */
    private Integer courseCategory;
    /**
     * 课程图片
     */
    private String courseImg;
    /**
     * 收藏类型:1课程，2章节，3课时
     */
    private Integer collectionType;
}
