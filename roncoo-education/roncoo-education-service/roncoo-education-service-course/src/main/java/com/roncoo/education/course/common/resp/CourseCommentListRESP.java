package com.roncoo.education.course.common.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 课程评论
 * </p>
 *
 * @author Quanf
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseCommentListRESP", description = "课程评论列表")
public class CourseCommentListRESP implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "评论时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime gmtModified;

    @ApiModelProperty(value = "状态(1:正常;0:禁用)")
    private Integer statusId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "课程ID")
    private Long courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseName;

    @ApiModelProperty(value = "被评论用户编号")
    private Long courseUserNo;

    @ApiModelProperty(value = "被评论者昵称")
    private String courseNickname;

    @ApiModelProperty(value = "评论者用户编号")
    private Long userNo;

    @ApiModelProperty(value = "评论者昵称")
    private String nickname;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论者IP")
    private String userIp;

    @ApiModelProperty(value = "评论者终端")
    private String userTerminal;

    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)")
    private Integer courseCategory;

    @ApiModelProperty(value = "机构号")
    private String orgNo;

    @ApiModelProperty(value = "回复内容")
    private List<CourseCommentListRESP> courseCommentList;

    @ApiModelProperty(value = "被评论者用户头像")
    private String courseUserImg;

    @ApiModelProperty(value = "评论者用户头像")
    private String userImg;
}
