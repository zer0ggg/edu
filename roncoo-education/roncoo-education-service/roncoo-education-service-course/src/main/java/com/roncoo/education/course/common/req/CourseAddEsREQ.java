package com.roncoo.education.course.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 课程信息一键导入ES
 * </p>
 *
 * @author LYQ
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "CourseAddEsREQ", description = "课程信息一键导入ES")
public class CourseAddEsREQ implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课程分类(1点播,2直播,4文库)", required = true)
    private Integer courseCategory;

}
