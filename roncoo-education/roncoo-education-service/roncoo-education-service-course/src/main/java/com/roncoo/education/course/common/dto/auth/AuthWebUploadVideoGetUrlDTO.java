package com.roncoo.education.course.common.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 网页端上传视频获取上传路径
 *
 * @author LYQ
 */
@Data
@ApiModel(value = "AuthWebUploadVideoGetUrlDTO", description = "网页端上传视频获取上传路径")
public class AuthWebUploadVideoGetUrlDTO implements Serializable {

    private static final long serialVersionUID = 2443593017097068265L;

    @ApiModelProperty(value = "视频Vid", required = true)
    private String videoVid;

    @ApiModelProperty(value = "上传地址", required = true)
    private String uploadAddress;

    @ApiModelProperty(value = "上传凭证", required = true)
    private String uploadAuth;

}
