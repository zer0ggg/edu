package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 直播记录查看
 *
 * @author kyh
 */
@Data
@Accessors(chain = true)
public class AuthCourseLiveLogViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	/**
	 * 章节名称
	 */
	@ApiModelProperty(value = "章节名称")
	private String chapterName;
	/**
	 * 课时名称
	 */
	@ApiModelProperty(value = "课时名称")
	private String periodName;
	/**
	 * 频道号
	 */
	@ApiModelProperty(value = "频道号(欢拓课程id)")
	private String channelId;
	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date beginTime;
	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	/**
	 * 直播状态(1:未开播,2:正在直播,3:结束,4:待生成回放,5:待转存)
	 */
	@ApiModelProperty(value = "直播状态(1:未开播,2:正在直播,3:结束,4:待生成回放,5:待转存)")
	private Integer liveStatus;

	@ApiModelProperty(value = "直播平台(1:保利威、2:欢拓)")
	private Integer livePlatform;

	/**
	 * web端开播url
	 */
	@ApiModelProperty(value = "web端开播url")
	private String webLiveUrl;
	/**
	 * 频道密码
	 */
	@ApiModelProperty(value = "频道密码")
	private String channelPasswd;
	/**
	 * 直播推流地址
	 */
	@ApiModelProperty(value = "直播推流地址")
	private String pushFlowUrl;
	/**
	 * 直播场景(alone:活动拍摄;ppt:三分屏)
	 */
	@ApiModelProperty(value = "直播场景(alone:活动拍摄;ppt:三分屏课)")
	private String scene;
	/**
	 * 助教频道
	 */
	@ApiModelProperty(value = "助教频道")
	private String assistantChannelId;
	/**
	 * 助教密码
	 */
	@ApiModelProperty(value = "助教密码")
	private String assistantPasswd;
	/**
	 * 助教链接
	 */
	@ApiModelProperty(value = "助教链接")
	private String assistantUrl;


	/**
	 * 	欢拓系统的主播id
	 */
	@ApiModelProperty(value = "欢拓系统的主播id")
	private Integer bid;

}
