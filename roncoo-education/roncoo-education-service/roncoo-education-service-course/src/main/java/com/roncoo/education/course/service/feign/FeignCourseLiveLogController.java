package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignLiveChannelLog;
import com.roncoo.education.course.feign.qo.CourseLiveLogQO;
import com.roncoo.education.course.feign.vo.CourseLiveLogVO;
import com.roncoo.education.course.service.feign.biz.FeignCourseLiveLogBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 直播记录表
 *
 * @author wujing
 */
@RestController
public class FeignCourseLiveLogController extends BaseController implements IFeignLiveChannelLog {

	@Autowired
	private FeignCourseLiveLogBiz biz;

	@Override
	public Page<CourseLiveLogVO> listForPage(@RequestBody CourseLiveLogQO qo) {
		return biz.listForPage(qo);
	}

	@Override
	public int save(@RequestBody CourseLiveLogQO qo) {
		return biz.save(qo);
	}

	@Override
	public int deleteById(@RequestBody Long id) {
		return biz.deleteById(id);
	}

	@Override
	public int updateById(@RequestBody CourseLiveLogQO qo) {
		return biz.updateById(qo);
	}

	@Override
	public CourseLiveLogVO getById(@RequestBody Long id) {
		return biz.getById(id);
	}

	@Override
	public CourseLiveLogVO getByChannelIdAndLiveStatus(@RequestBody CourseLiveLogQO qo) {
		return biz.getByChannelIdAndLiveStatus(qo);
	}

}
