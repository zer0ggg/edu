package com.roncoo.education.course.service.api.auth.biz;

import cn.hutool.core.util.ObjectUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.common.core.enums.CollectionTypeEnum;
import com.roncoo.education.common.core.enums.ResultEnum;
import com.roncoo.education.common.core.enums.StatusIdEnum;
import com.roncoo.education.common.service.thread.ThreadContext;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCourseDeleteBO;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCoursePageBO;
import com.roncoo.education.course.common.bo.auth.AuthUserCollectionCourseSaveBO;
import com.roncoo.education.course.common.dto.auth.AuthUserCollectionCoursePageDTO;
import com.roncoo.education.course.service.dao.CourseChapterDao;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.CourseDao;
import com.roncoo.education.course.service.dao.UserCollectionCourseDao;
import com.roncoo.education.course.service.dao.impl.mapper.entity.*;
import com.roncoo.education.course.service.dao.impl.mapper.entity.UserCollectionCourseExample.Criteria;
import com.roncoo.education.user.feign.interfaces.IFeignUserExt;
import com.roncoo.education.user.feign.vo.UserExtVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户收藏课程表
 *
 * @author wujing
 */
@Component
public class AuthUserCollectionCourseBiz {

	@Autowired
	private IFeignUserExt feignUserExt;

	@Autowired
	private CourseDao courseDao;
	@Autowired
	private CourseChapterDao courseChapterDao;

	@Autowired
	private CourseChapterPeriodDao courseChapterPeriodDao;
	@Autowired
	private UserCollectionCourseDao userCollectionCourseDao;

	/**
	 * 根据用户编号获取用户收藏课程信息
	 *
	 * @param bo
	 * @author kyh
	 */
	public Result<Page<AuthUserCollectionCoursePageDTO>> list(AuthUserCollectionCoursePageBO bo) {
		if (ThreadContext.userNo() == null) {
			return Result.error("userNo不能为空");
		}

		UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
		if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
			return Result.error("找不到用户信息");
		}
		UserCollectionCourseExample example = new UserCollectionCourseExample();
		Criteria c = example.createCriteria();
		c.andUserNoEqualTo(ThreadContext.userNo());

		if (bo.getCourseCategory() != null) {
			c.andCourseCategoryEqualTo(bo.getCourseCategory());
		}
		if (bo.getCollectionType() != null) {
			c.andCollectionTypeEqualTo(bo.getCollectionType());
		}
		Page<UserCollectionCourse> page = userCollectionCourseDao.listForPage(bo.getPageCurrent(), bo.getPageSize(), example);
		return Result.success(PageUtil.transform(page, AuthUserCollectionCoursePageDTO.class));
	}

	public Result<Integer> save(AuthUserCollectionCourseSaveBO bo) {
		if (ThreadContext.userNo() == null) {
			return Result.error("userNo不能为空");
		}
		if (bo.getCollectionType() == null) {
			return Result.error("CollectionType不能为空");
		}
		UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
		if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
			return Result.error("找不到用户信息");
		}
		UserCollectionCourse  userCollectionCourse;
		if (CollectionTypeEnum.COURSE.getCode().equals(bo.getCollectionType())) {
			// 收藏课程
			// 查找用户是否已收藏
			UserCollectionCourse collectionCourse = userCollectionCourseDao.getByUserAndCourseId(ThreadContext.userNo(), bo.getCollectionId());
			if (ObjectUtil.isNotNull(collectionCourse)) {
				return Result.error(ResultEnum.COLLECTION);
			}

			Course course = courseDao.getById(bo.getCollectionId());
			if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
				return Result.error("课程异常");
			}
			userCollectionCourse = new UserCollectionCourse();
			userCollectionCourse.setCourseId(bo.getCollectionId());
			userCollectionCourse.setCourseImg(course.getCourseLogo());
			userCollectionCourse.setCourseName(course.getCourseName());

		} else if (CollectionTypeEnum.CHAPTER.getCode().equals(bo.getCollectionType())) {
			// 收藏章节
			// 查找用户是否已收藏
			UserCollectionCourse userCollectionCourseChapter = userCollectionCourseDao.getByUserAndChapterId(ThreadContext.userNo(), bo.getCollectionId());
			if (ObjectUtil.isNotNull(userCollectionCourseChapter)) {
				return Result.error(ResultEnum.COLLECTION);
			}

			CourseChapter courseChapter = courseChapterDao.getById(bo.getCollectionId());
			if (ObjectUtil.isNull(courseChapter) || !StatusIdEnum.YES.getCode().equals(courseChapter.getStatusId())) {
				return Result.error("章节异常");
			}

			Course course = courseDao.getById(courseChapter.getCourseId());
			if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
				return Result.error("课程异常");
			}
			userCollectionCourse = new UserCollectionCourse();
			userCollectionCourse.setCourseId(courseChapter.getCourseId());
			userCollectionCourse.setCourseImg(course.getCourseLogo());
			userCollectionCourse.setCourseName(course.getCourseName());
			userCollectionCourse.setChapterId(courseChapter.getId());
			userCollectionCourse.setChapterName(courseChapter.getChapterName());

		} else {
			// 收藏课时
			// 查找用户是否已收藏
			UserCollectionCourse userCollectionCoursePeriod = userCollectionCourseDao.getByUserAndPeriodId(ThreadContext.userNo(), bo.getCollectionId());
			if (ObjectUtil.isNotNull(userCollectionCoursePeriod)) {
				return Result.error(ResultEnum.COLLECTION);
			}
			// 查找课时信息
			CourseChapterPeriod courseChapterPeriod = courseChapterPeriodDao.getById(bo.getCollectionId());
			if (ObjectUtil.isNull(courseChapterPeriod) || !StatusIdEnum.YES.getCode().equals(courseChapterPeriod.getStatusId())) {
				return Result.error("课时异常");
			}
			// 查找章节信息
			CourseChapter courseChapter = courseChapterDao.getById(courseChapterPeriod.getChapterId());
			if (ObjectUtil.isNull(courseChapter) || !StatusIdEnum.YES.getCode().equals(courseChapter.getStatusId())) {
				return Result.error("章节异常");
			}

			// 查找课程信息
			Course course = courseDao.getById(courseChapterPeriod.getCourseId());
			if (ObjectUtil.isNull(course) || !StatusIdEnum.YES.getCode().equals(course.getStatusId())) {
				return Result.error("课程异常");
			}
			userCollectionCourse = new UserCollectionCourse();
			userCollectionCourse.setCourseId(courseChapter.getCourseId());
			userCollectionCourse.setCourseImg(course.getCourseLogo());
			userCollectionCourse.setCourseName(course.getCourseName());
			userCollectionCourse.setChapterId(courseChapter.getId());
			userCollectionCourse.setChapterName(courseChapter.getChapterName());
			userCollectionCourse.setPeriodId(bo.getCollectionId());
			userCollectionCourse.setPeriodName(courseChapterPeriod.getPeriodName());
		}
		userCollectionCourse.setUserNo(ThreadContext.userNo());
		userCollectionCourse.setMobile(userExtVO.getMobile());
		userCollectionCourse.setCourseCategory(bo.getCourseCategory());
		userCollectionCourse.setCollectionType(bo.getCollectionType());
		int results = userCollectionCourseDao.save(userCollectionCourse);
		if (results > 0) {
			return Result.success(results);
		}
		return Result.error(ResultEnum.COURSE_SAVE_FAIL);
	}

	public Result<Integer> delete(AuthUserCollectionCourseDeleteBO bo) {
		if (ThreadContext.userNo() == null) {
			return Result.error("userNo不能为空");
		}
		if (bo.getCollectionId() == null) {
			return Result.error("收藏ID不能为空");
		}
		if (bo.getCollectionType() == null) {
			return Result.error("收藏类型不能为空");
		}
		UserExtVO userExtVO = feignUserExt.getByUserNo(ThreadContext.userNo());
		if (ObjectUtil.isNull(userExtVO) || !StatusIdEnum.YES.getCode().equals(userExtVO.getStatusId())) {
			return Result.error("找不到用户信息");
		}
		UserCollectionCourse userCollectionCourse;
		if (CollectionTypeEnum.COURSE.getCode().equals(bo.getCollectionType())) {
			userCollectionCourse = userCollectionCourseDao.getByUserAndCourseId(ThreadContext.userNo(), bo.getCollectionId());
		} else if (CollectionTypeEnum.CHAPTER.getCode().equals(bo.getCollectionType())) {
			userCollectionCourse = userCollectionCourseDao.getByUserAndChapterId(ThreadContext.userNo(), bo.getCollectionId());
		} else {
			userCollectionCourse = userCollectionCourseDao.getByUserAndPeriodId(ThreadContext.userNo(), bo.getCollectionId());
		}
		if (ObjectUtil.isNull(userCollectionCourse)) {
			return Result.error("已取消收藏");
		}
		int results = userCollectionCourseDao.deleteById(userCollectionCourse.getId());
		if (results > 0) {
			return Result.success(results);
		}
		return Result.error(ResultEnum.COURSE_DELETE_FAIL);
	}

}
