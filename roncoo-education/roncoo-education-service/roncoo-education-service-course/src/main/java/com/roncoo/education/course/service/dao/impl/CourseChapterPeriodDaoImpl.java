package com.roncoo.education.course.service.dao.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.PageUtil;
import com.roncoo.education.course.service.dao.CourseChapterPeriodDao;
import com.roncoo.education.course.service.dao.impl.mapper.CourseChapterPeriodMapper;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriod;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample;
import com.roncoo.education.course.service.dao.impl.mapper.entity.CourseChapterPeriodExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseChapterPeriodDaoImpl implements CourseChapterPeriodDao {
	@Autowired
	private CourseChapterPeriodMapper courseChapterPeriodMapper;

	@Override
	public int save(CourseChapterPeriod record) {
		return this.courseChapterPeriodMapper.insertSelective(record);
	}

	@Override
	public int deleteById(Long id) {
		return this.courseChapterPeriodMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int updateById(CourseChapterPeriod record) {
		record.setGmtCreate(null);
		record.setGmtModified(null);
		return this.courseChapterPeriodMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public CourseChapterPeriod getById(Long id) {
		return this.courseChapterPeriodMapper.selectByPrimaryKey(id);
	}

	@Override
	public Page<CourseChapterPeriod> listForPage(int pageCurrent, int pageSize, CourseChapterPeriodExample example) {
		int count = this.courseChapterPeriodMapper.countByExample(example);
		pageSize = PageUtil.checkPageSize(pageSize);
		pageCurrent = PageUtil.checkPageCurrent(count, pageSize, pageCurrent);
		int totalPage = PageUtil.countTotalPage(count, pageSize);
		example.setLimitStart(PageUtil.countOffset(pageCurrent, pageSize));
		example.setPageSize(pageSize);
		return new Page<CourseChapterPeriod>(count, totalPage, pageCurrent, pageSize, this.courseChapterPeriodMapper.selectByExample(example));
	}

	@Override
	public List<CourseChapterPeriod> listByChapterId(Long chapterId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		example.createCriteria().andChapterIdEqualTo(chapterId);
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public List<CourseChapterPeriod> listByChapterIdAndStatusId(Long chapterId, Integer statusId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andChapterIdEqualTo(chapterId);
		c.andStatusIdEqualTo(statusId);
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public CourseChapterPeriod getByVideoId(Long videoId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andVideoIdEqualTo(videoId);
		example.setOrderByClause(" sort asc ,id asc");
		List<CourseChapterPeriod> list = this.courseChapterPeriodMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public CourseChapterPeriod getByCourseIdAndLiveStatus(Long courseId, Integer liveStatus) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(courseId);
		c.andLiveStatusEqualTo(liveStatus);
		List<CourseChapterPeriod> list = this.courseChapterPeriodMapper.selectByExample(example);
		if (CollectionUtil.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<CourseChapterPeriod> listByCourseIdAndLiveStatusAndSorAsc(Long courseId, Integer liveStatus) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(courseId);
		c.andLiveStatusEqualTo(liveStatus);
		example.setOrderByClause(" sort asc ,id asc");
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public List<CourseChapterPeriod> listByChapterIdAndStatusIdAndSortAsc(Long chapterId, Integer statusId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andChapterIdEqualTo(chapterId);
		c.andStatusIdEqualTo(statusId);
		example.setOrderByClause(" sort asc ,id asc");
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public List<CourseChapterPeriod> listByChapterIdAndStatusIdAndSortDesc(Long chapterId, Integer statusId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andChapterIdEqualTo(chapterId);
		c.andStatusIdEqualTo(statusId);
		example.setOrderByClause(" sort asc ,id desc");
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public List<CourseChapterPeriod> listByVideoId(Long videoId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andVideoIdEqualTo(videoId);
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public List<CourseChapterPeriod> listByVideoIdAndStatusId(Long videoId, Integer statusId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andVideoIdEqualTo(videoId);
		c.andStatusIdEqualTo(statusId);
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

	@Override
	public int updateByVideoId(CourseChapterPeriod courseChapterPeriod) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andVideoIdEqualTo(courseChapterPeriod.getVideoId());
		return this.courseChapterPeriodMapper.updateByExampleSelective(courseChapterPeriod, example);
	}

    @Override
    public List<CourseChapterPeriod> listByCourseId(Long courseId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(courseId);
		return this.courseChapterPeriodMapper.selectByExample(example);
    }

	@Override
	public int updateByVid(CourseChapterPeriod courseChapterPeriod) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andVideoVidEqualTo(courseChapterPeriod.getVideoVid());
		return this.courseChapterPeriodMapper.updateByExampleSelective(courseChapterPeriod, example);
	}


	@Override
	public List<CourseChapterPeriod> listByCourseIdAndStatusId(Long courseId, Integer statusId) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andCourseIdEqualTo(courseId);
		c.andStatusIdEqualTo(statusId);
		return this.courseChapterPeriodMapper.selectByExample(example);
	}

    @Override
    public List<CourseChapterPeriod> listByIds(List<Long> ids) {
		CourseChapterPeriodExample example = new CourseChapterPeriodExample();
		Criteria c = example.createCriteria();
		c.andIdIn(ids);
		return this.courseChapterPeriodMapper.selectByExample(example);
    }
}
