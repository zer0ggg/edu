package com.roncoo.education.course.service.feign;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.course.feign.interfaces.IFeignFileInfo;
import com.roncoo.education.course.feign.qo.FileInfoQO;
import com.roncoo.education.course.feign.vo.FileInfoVO;
import com.roncoo.education.course.feign.vo.UsedSpaceVO;
import com.roncoo.education.course.service.feign.biz.FeignFileInfoBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件信息表
 *
 * @author wujing
 */
@RestController
public class FeignFileInfoController extends BaseController implements IFeignFileInfo{

	@Autowired
	private FeignFileInfoBiz biz;

	@Override
	public Page<FileInfoVO> listForPage(@RequestBody FileInfoQO qo){
		return biz.listForPage(qo);
	}

    @Override
	public int save(@RequestBody FileInfoQO qo){
		return biz.save(qo);
	}

    @Override
	public int deleteById(@RequestBody Long id){
		return biz.deleteById(id);
	}

    @Override
	public int updateById(@RequestBody FileInfoQO qo){
		return biz.updateById(qo);
	}

    @Override
	public FileInfoVO getById(@RequestBody Long id){
		return biz.getById(id);
	}

	@Override
	public UsedSpaceVO usedSpace() {
		return biz.usedSpace();
	}

}
