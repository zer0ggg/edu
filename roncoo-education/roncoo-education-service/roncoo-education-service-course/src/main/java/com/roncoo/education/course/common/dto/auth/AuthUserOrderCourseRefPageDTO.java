package com.roncoo.education.course.common.dto.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户订单课程关联表
 *
 * @author wujing
 */
@Data
@Accessors(chain = true)
public class AuthUserOrderCourseRefPageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 讲师用户编号
	 */
	@ApiModelProperty(value = "讲师用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lecturerUserNo;
	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long userNo;
	/**
	 * 课程类型(1:普通课程;2:直播课程,3:试卷)
	 */
	@ApiModelProperty(value = "课程类型(1:普通课程;2:直播课程,3:试卷)")
	private Integer courseCategory;
	/**
	 * 课程类型：1课程，2章节，3课时
	 */
	@ApiModelProperty(value = "课程类型：1课程，2章节，3课时")
	private Integer courseType;
	/**
	 * 课程ID
	 */
	@ApiModelProperty(value = "课程ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long courseId;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	/**
	 * 课程图片
	 */
	@ApiModelProperty(value = "课程图片")
	private String courseLogo;
	/**
	 * 关联ID
	 */
	@ApiModelProperty(value = "关联ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long refId;
	/**
	 * 关联名称
	 */
	@ApiModelProperty(value = "关联名称")
	private String refName;
	/**
	 * 是否支付：1是，0:否
	 */
	@ApiModelProperty(value = "是否支付：1是，0:否")
	private Integer isPay;
	/**
	 * 是否学习：1是，0:否
	 */
	@ApiModelProperty(value = "是否学习：1是，0:否")
	private Integer isStudy;
	/**
	 * 订单号
	 */
	@ApiModelProperty(value = "订单号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long orderNo;
	/**
	 * 最近学习课时ID
	 */
	@ApiModelProperty(value = "最近学习课时ID")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long lastStudyPeriodId;

	/**
	 * 最近学习课时名称
	 */
	@ApiModelProperty(value = "最近学习课时名称")
	private String periodName;
	/**
	 * 章节ID
	 */
	@ApiModelProperty(value = "章节ID")
	private Long chapterId;
	/**
	 * 章节名称
	 */
	@ApiModelProperty(value = "章节名称")
	private String chapterName;
	/**
	 * 观看时长（秒）
	 */
	@ApiModelProperty(value = "观看时长（秒）")
	private BigDecimal studyLength;
	/**
	 * 观看进度(0~100)
	 */
	@ApiModelProperty(value = "观看进度(0~100)")
	private Integer studyProcess;
	/**
	 * 学习时间
	 */
	@ApiModelProperty(value = "学习时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastStudyTime;
	/**
	 * 学习时间
	 */
	@ApiModelProperty(value = "学习时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date gmtCreate;

}
