package com.roncoo.education.course.service.api.auth;

import com.roncoo.education.common.core.base.BaseController;
import com.roncoo.education.common.core.base.Page;
import com.roncoo.education.common.core.base.Result;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterListBO;
import com.roncoo.education.course.common.bo.auth.AuthCourseChapterPageBO;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterListDTO;
import com.roncoo.education.course.common.dto.auth.AuthCourseChapterPageDTO;
import com.roncoo.education.course.service.api.auth.biz.AuthCourseChapterBiz;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 章节信息
 *
 * @author wujing
 */
@RestController
@RequestMapping(value = "/course/auth/course/chapter")
public class AuthCourseChapterController extends BaseController {

    @Autowired
    private AuthCourseChapterBiz biz;

    /**
     * 章节列出接口
     *
     * @param authCourseChapterListBO
     * @author kyh
     */
    @ApiOperation(value = "章节列出接口(频道选直播课程)", notes = "根据课程ID列出章节信息")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Result<AuthCourseChapterListDTO> listByCourseId(@RequestBody AuthCourseChapterListBO authCourseChapterListBO) {
        return biz.listByCourseId(authCourseChapterListBO);
    }

    /**
     * 普通课程、直播、试卷详情列出章节课时接口
     *
     * @param authCourseChapterPageBO
     * @author kyh
     */
    @ApiOperation(value = "普通课程、直播、试卷详情列出章节课时接口（已登录）", notes = "普通课程、直播、试卷详情根据课程ID列出章节课时接口")
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public Result<Page<AuthCourseChapterPageDTO>> listForPage(@RequestBody AuthCourseChapterPageBO authCourseChapterPageBO) {
        return biz.listForPage(authCourseChapterPageBO);
    }

}
