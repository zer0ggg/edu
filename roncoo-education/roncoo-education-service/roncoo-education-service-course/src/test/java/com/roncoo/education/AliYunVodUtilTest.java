package com.roncoo.education;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.aliyun.vod.upload.resp.UploadImageResponse;
import com.aliyuncs.vod.model.v20170321.CreateUploadAttachedMediaRequest;
import com.aliyuncs.vod.model.v20170321.CreateUploadAttachedMediaResponse;
import com.roncoo.education.common.aliyun.AliYunVodUtil;
import org.junit.Test;

import java.util.Date;

public class AliYunVodUtilTest {

    private final static String accessKeyId = "LTAI4FcgThpro9wTQ1uVznXZ";
    private final static String accessKeySecret = "HqJqo9s3vZyJPpHKXum6ANrmuI1PNX";
    private float bitrate;

    @Test
    public void createUploadAttachedMedia() {
        CreateUploadAttachedMediaRequest request = new CreateUploadAttachedMediaRequest();
        request.setBusinessType("watermark");
        request.setMediaExt("png");
        CreateUploadAttachedMediaResponse response = AliYunVodUtil.createUploadAttachedMedia(request, accessKeyId, accessKeySecret);
        System.out.println(JSON.toJSONString(response));
        //{"fileURL":"https://outin-09536bb2e03211eaabb800163e1a65b6.oss-cn-shanghai.aliyuncs.com/watermark/24C9B4424994488DA8878C3BACFAEC94-0-2.png","mediaId":"636375ff19df429796e6877c7f462867","mediaURL":"http://video.vcloud.roncoos.com/watermark/24C9B4424994488DA8878C3BACFAEC94-0-2.png","requestId":"96E6C846-AD84-41BA-9E08-47538DC778B9","uploadAddress":"eyJFbmRwb2ludCI6Imh0dHBzOi8vb3NzLWNuLXNoYW5naGFpLmFsaXl1bmNzLmNvbSIsIkJ1Y2tldCI6Im91dGluLTA5NTM2YmIyZTAzMjExZWFhYmI4MDAxNjNlMWE2NWI2IiwiRmlsZU5hbWUiOiJ3YXRlcm1hcmsvMjRDOUI0NDI0OTk0NDg4REE4ODc4QzNCQUNGQUVDOTQtMC0yLnBuZyJ9","uploadAuth":"eyJTZWN1cml0eVRva2VuIjoiQ0FJUzF3UjFxNkZ0NUIyeWZTaklyNWY3SXZMMjJxcEYyS2plTzBQcmhrb05WYjVLbTR2eW1qejJJSGhKZVhOdkJPMGV0ZjQrbVdCWTdQY1lsclVxRXNjZUdCR2VQWlVwdHNnSXFsbjZKcGZadjh1ODRZQURpNUNqUWNGdGl1NWZtcDI4V2Y3d2FmK0FVQTdHQ1RtZDVNd1lvOWJUY1RHbFFDWnVXLy90b0pWN2I5TVJjeENsWkQ1ZGZybC9MUmRqcjhsbzF4R3pVUEcyS1V6U24zYjNCa2hsc1JZZTcyUms4dmFIeGRhQXpSRGNnVmJtcUpjU3ZKK2pDNEM4WXM5Z0c1MTlYdHlwdm9weGJiR1Q4Q05aNXo5QTlxcDlrTTQ5L2l6YzdQNlFIMzViNFJpTkw4L1o3dFFOWHdoaWZmb2JIYTlZcmZIZ21OaGx2dkRTajQzdDF5dFZPZVpjWDBha1E1dTdrdTdaSFArb0x0OGphWXZqUDNQRTNyTHBNWUx1NFQ0OFpYVVNPRHREWWNaRFVIaHJFazRSVWpYZEk2T2Y4VXJXU1FDN1dzcjIxN290ZzdGeXlrM3M4TWFIQWtXTFg3U0IyRHdFQjRjNGFFb2tWVzRSeG5lelc2VUJhUkJwYmxkN0JxNmNWNWxPZEJSWm9LK0t6UXJKVFg5RXoycExtdUQ2ZS9MT3M3b0RWSjM3V1p0S3l1aDRZNDlkNFU4clZFalBRcWl5a1QwZ0ZncGZUSzFSemJQbU5MS205YmFCMjUvelcrUGREZTBkc1Znb1dWS0hwaUdXRzNSTE5uK3p0Sjl4YmtlRStzS1VsUDJScU0xcUdsRWp2b3NGVkZpSWVOOW5wUXcrdS9Mc3RCbksrYkcrV0M3dDRXTWs5OFBkOXBOTXJnbEdCcTc0M0xYTjVtT0U0eWJKTWZGcHlabnROVGN4RVVqdFNSdFEyK3VQZzM4TG5SY0ZubHl2Tmd3WGcxYnJqajd0SVpkRWo2SFRuU2tWWC9jRHc3V0NVVXVKK21SeEZOUzM1TG93VU9wNVIveFZhUEc0MEJHaFQzaGJhWmdXMFJxQUFRNEw3dW0wMERObk5NamxrNHNNRzBrOXZ4MC9BV2RHQ1VoeTlRNEd5andzdXcyWmlnUEJRQ21iN3FuMFV3aFlYVEZJR1VsdXZSMmt3cTduRjRtcnNZalp5WFF5L2FTa0dQelRGakh5Q3dZWWRQejF5WCsxWDl4MnFPQ1U4N1pHa3VJU2toNFZEdmdzVXloN25STWVhZlNncVJEMUFVTlZNV2ZGZkd6NTczT0oiLCJBY2Nlc3NLZXlJZCI6IlNUUy5OVE5pSEI3dWRvajU5ZVpiSlhZMWZ0Tll4IiwiRXhwaXJlVVRDVGltZSI6IjIwMjAtMDktMjVUMDI6MzE6NTRaIiwiQWNjZXNzS2V5U2VjcmV0IjoiSFh0U2Z2WDVSVktNMm1BR253NUJCbmoyOEtwYTN2YWNEOU5pMlF0SFJGV2YiLCJFeHBpcmF0aW9uIjoiMzYwMCIsIlJlZ2lvbiI6ImNuLXNoYW5naGFpIn0="}
    }

    @Test
    public void uploadWatermarkImage() throws Exception {
        //{"code":"Success","imageId":"3d50afb8af30403992b1daa46187e756","imageURL":"http://video.vcloud.roncoos.com/image/watermark/C67E7ED996A9408AA890B21A48C1A99C-6-2.png","message":"Success","requestId":"41185F4D-69B5-4B5A-B744-7A283EB4DAEA","success":true}
        String fileUrl = "https://static-dev.roncoo.com/course/6b3GshXhbUilwGMCgvWTfebhsVczX9I6.png";
        UploadImageResponse response = AliYunVodUtil.uploadWatermarkImage(fileUrl, accessKeyId, accessKeySecret);
        System.out.println(JSON.toJSONString(response));
    }

    @Test
    public void playVideo() {
        Double bitrate = Double.parseDouble("499");
        System.out.println(bitrate);
        System.out.println(bitrate);

        System.out.println(bitrate.longValue());
        String result = DateUtil.format(DateUtil.offsetSecond(DateUtil.beginOfDay(new Date()), bitrate.intValue()), DatePattern.NORM_TIME_PATTERN);
        System.out.println(result);
    }
}
