#### 端口设置
| 服务名称                    |     服务端口     |     xxl-job     |     sentinel    | 
| ------------------------- | --------------- | --------------- | --------------- | 
| app-sba                   |  8830           |        -        |        -        |
| app-job                   |  8840           |        -        |        -        |
| app-gateway               |  8880           |        -        |        -        |
| web                       |  3000           |        -        |        -        |
| service-data              |  8700           |       8701      |      8702       |
| service-system            |  8710           |        -        |      8712       |
| service-user              |  8720           |       8721      |      8722       |
| service-course            |  8730           |       8731      |      8732       |
| service-community         |  8740           |        -        |      8742       |
| service-marketing         |  8750           |        -        |      8752       |
| service-exam              |  8760           |       8761      |      8762       |
-----------------------------------------------------------------------------------
#### 地址
管理后台：http://localhost/admin           18800000000/123456  
分布式调度：http://localhost:8840/job       admin/123456  
应用监控：http://localhost:8830/sba         admin/123456     
