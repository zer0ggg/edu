import { getNavList, getWebsite, aboutList } from '~/api/system.js'
import { getUserInfo } from '~/api/user'
import { messageNun } from '~/api/account/user.js'

export default {
  async nuxtServerInit({ dispatch }, context) {
    const { store, req, redirect } = context;
    store.commit('GET_TOKEN_SERVER', req)
    await dispatch('GET_ABOUT')
    await dispatch('GET_NAV', context)
    await dispatch('GET_WEBINFO');
    // 判断是否移动端访问

    const deviceAgent = req.headers['user-agent'].toLowerCase();
    const agentID = deviceAgent.match(/(iphone|ipod|ipad|android)/);
    if (agentID && store.state.webInfo.h5Domain) {
      console.info('移动端访问')
      redirect(store.state.webInfo.h5Domain)
    }
    if (store.state.tokenInfo) {
      const res = await getUserInfo({}, store.state.tokenInfo)
      if ((res.code >= 300 && res.code <= 400) || res.code === 999) {
        redirect({ path: '/login?t=login' })
      }
      if (res.data) {
        store.commit('SET_USER', res.data)
      }
    }
  },
  GET_ABOUT(store) { // 获取关联信息
    return new Promise((resolve, reject) => {
      aboutList().then((res) => {
        if (res.code === 200) {
          const now = new Date()
          const data = { time: now.getTime(), list: res.data.websiteNavList }
          store.state.aboutList = data
        }
        resolve(res)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  // 获取当前用户未读站内信数量
  GET_USERMSGNUM: (store) => {
    messageNun().then((res) => {
      if (res.code === 200) {
        store.state.userMsgNum = res.data.num || 0
      }
    })
  },
  GET_NAV(store) { // 获取导航信息
    return new Promise((resolve, reject) => {
      getNavList({ platShow: 1 }).then((res) => {
        if (res.code === 200) {
          store.state.navList = res.data
        }
        resolve(res)
      }).catch((error) => {
        reject(error)
      })
    })
  },

  GET_WEBINFO(store) { // 获取网站信息
    return new Promise((resolve, reject) => {
      getWebsite().then((res) => {
        if (res.code === 200) {
          store.state.webInfo = res.data
          store.state.clientData.name = res.data.title
        }
        resolve(res)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  GET_USERINFO(store, cb) {
    return new Promise((resolve, reject) => {
      getUserInfo()
        .then((res) => {
          if (res.code === 200) {
            resolve(res)
            store.commit('SET_USER', res.data)
            if (cb) {
              cb(store)
            }
          } else {
            if (cb) {
              cb(store, res.code)
            }
            store.commit('SIGN_OUT')
            reject(res)
          }
        }).catch((msg) => {
          reject(msg)
          store.commit('SIGN_OUT')
          // console.log(msg)
        })
    })
  },
  REDIRECT_LOGIN(store) {
    store.commit('SET_TEMPORARYURL')
    store.commit('SIGN_OUT')
    this.$router.push({ name: 'login' })
  },
  REDIRECT_ERR(store, context, err) {
    context.error({ message: err.msg, statusCode: err.code || 404 })
  }
}
