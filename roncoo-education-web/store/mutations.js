import cookie from '../utils/cookies'
import { removeStore, getStore, setStore, setSession, getSession } from '../utils/storage'
const tokenName = 'RC_WEB_TOKEN'

export default {
  INIT_WEB: (state) => {
    const token = cookie.getInClient(tokenName)
    if (token) {
      const userInfo = JSON.parse(getStore('RcUserInfo'))
      state.tokenInfo = token
      state.userInfo = userInfo
    } else {
      state.userInfo = ''
      removeStore('RcUserInfo')
    }
  },
  SET_ITEMS: (state, { key, value }) => {
    state[key] = value
  },
  // 开始loading
  LOADING_START: (state) => {
    state.loading = true
  },
  // 结束loading
  LOADING_FINISH: (state) => {
    state.loading = false
  },
  // 记录token
  SET_TOKEN: (state, token) => {
    state.tokenInfo = token
    cookie.setInClient({ key: tokenName, val: token })
    // setStore('tokenInfo', info.info)
  },
  // 服务端获取token
  GET_TOKEN_SERVER: (state, req) => {
    const cook = cookie.getInServer(req)
    state.tokenInfo = cook[tokenName]
  },
  // 记录当前url
  SET_TEMPORARYURL: (state, data) => {
    const uri = window.location.href
    if (uri && uri.indexOf('/login') === -1) {
      setSession('temporaryUrl', uri)
    }
  },

  // 获取临时url
  GET_TEMPORARYURL: (state) => {
    let uri = getSession('temporaryUrl')
    if (uri) {
      if (uri.indexOf('/login') !== -1) {
        uri = '/'
      }
      state.temporaryUrl = uri
    }
  },
  // 隐藏二维码
  HIDE_EWM: (state) => {
    state.changeEwm = Math.random()
  },
  // 记录用户信息
  SET_USER: (state, data) => {
    if (data) {
      data.token = state.tokenInfo
      state.userInfo = data
      if (process.client) {
        setStore('RcUserInfo', data)
      }
    }
    // Vue.set(state.users, id, user || false) /* false means user not found */
  },
  // 退出登录
  SIGN_OUT: (state) => {
    state.userInfo = ''
    state.tokenInfo = ''
    cookie.delInClient(tokenName)
    removeStore('RcUserInfo')
  },
  // 修改时间格式
  CHANGE_TIME(state, data) {
    let timeData;
    if (data.gmtModified) {
      timeData = new Date(data.gmtModified)
    } else {
      timeData = new Date()
    }
    // console.log(timeData)
    const year = timeData.getFullYear()
    const mon = (timeData.getMonth() + 1).toString().padStart(2, '0')
    const day = timeData.getDate().toString().padStart(2, '0')
    const hours = timeData.getHours().toString().padStart(2, '0')
    const min = timeData.getMinutes().toString().padStart(2, '0')
    const sec = timeData.getSeconds().toString().padStart(2, '0')
    const dateStr = year + '-' + mon + '-' + day + ' ' + hours + ':' + min + ':' + sec
    data.gmtModified = dateStr
  }
}
