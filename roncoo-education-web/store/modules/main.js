import { setStore, getStore, removeStore } from '../../utils/storage'
import {
  getClassList
} from '~/api/course.js'

import {
  getAdvList,
  getZoneList
} from '~/api/system.js'

export default {
  namespaced: true,
  state: {
    referralCode: ''
  },
  mutations: {
    SET_REFERRAL(state, code) {
      state.referralCode = code
    }
  },
  actions: {
    // 获取轮播图
    getAdvList(store, params = {}) {
      return getAdvList(params)
    },

    // 课程分类列表接口
    getClassList(store) {
      return new Promise((resolve, reject) => {
        getClassList().then((res) => {
          resolve(res)
        }).catch((error) => {
          reject(error)
        })
      })
    },

    // 专区课程列表接口
    getZoneList(store) {
      return new Promise((resolve, reject) => {
        getZoneList({ zoneLocation: 1 }).then((res) => {
          resolve(res)
        }).catch((error) => {
          console.log('error=>', error)
          reject(error)
        })
      })
    },
    // 保存推荐码
    SetReferral(store, code) {
      store.commit('SET_REFERRAL', code)
      setStore('referralCode', code)
    },
    // 移除推荐码
    RemoveReferralCode(store) {
      store.commit('SET_REFERRAL', '')
      removeStore('referralCode')
    },
    // 获取推荐码
    GetReferralCode(store) {
      return new Promise((resolve) => {
        if (store.state.referralCode) {
          resolve(store.state.referralCode)
        } else if (getStore('referralCode')) {
          resolve(getStore('referralCode'))
        }
      })
    }
  }
}
