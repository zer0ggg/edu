import { courseDetail, userCourseDetail } from '~/api/course.js'
export default {
  namespaced: true,
  actions: {
    // 获取题库详情
    GET_LIBRARYDETAIL(store, data) {
      return new Promise((resolve, reject) => {
        let _this = data.dataObj
        if (data.vm) {
          _this = data.vm
        }
        courseDetail(_this.params).then((res) => {
          if (res.code === 200) {
            _this.courseInfo = res.data
            if (_this.courseInfo.isFree) {
              _this.isFree = true
            }
            _this.acList = res.data.accessoryList || []
            _this.courseName = res.data.courseName
            _this.lecturerInfo = res.data.lecturer
            resolve(res)
          } else {
            reject(res)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取登录题库详情
    GET_USERLIBRARYDETAIL(store, data) {
      return new Promise((resolve, reject) => {
        let _this = data.dataObj
        if (data.vm) {
          _this = data.vm
        }
        userCourseDetail(_this.params, _this.tokenInfo).then((res) => {
          console.log(res)
          if (res.code === 200) {
            _this.courseInfo = res.data
            _this.isPay = res.data.isPay
            _this.acList = res.data.accessoryList || []
            _this.courseName = res.data.courseName
            _this.lecturerInfo = res.data.lecturer
            if (res.data.isFree === 1 || res.data.isPay === 1) {
              _this.isFree = true
            }
            resolve(res)
          } else if (res.code >= 300 && res.code <= 400) {
            resolve(res)
          } else {
            reject(res)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    }
  }
}
