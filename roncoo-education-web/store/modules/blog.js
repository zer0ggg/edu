// import LRU from 'lru-cache'
import {
  blogTab,
  blogList,
  blogDetail,
  bolggerInfo,
  commentList,
  changeBlogger,
  infoZone,
  hotZone
} from '~/api/blog/community.js'
import { userBolggerInfo, blogView } from '~/api/blog/user'

// const CACHED = new LRU()
// const ttl = 10 * 60 * 1000 // 缓存时间
export default {
  namespaced: true,
  mutations: {
    // 修改时间格式
    CHANGE_TIME(state, data) {
      const timeData = new Date(data.gmtModified)
      const year = timeData.getFullYear()
      const mon = (timeData.getMonth() + 1).toString().padStart(2, '0')
      const day = timeData.getDate().toString().padStart(2, '0')
      const hours = timeData.getHours().toString().padStart(2, '0')
      const min = timeData.getMinutes().toString().padStart(2, '0')
      const sec = timeData.getSeconds().toString().padStart(2, '0')
      const dateStr = year + '-' + mon + '-' + day + ' ' + hours + ':' + min + ':' + sec
      data.gmtModified = dateStr
    }
  },
  actions: {
    CHANGE_BLOGGER(store) {
      changeBlogger({
        isShowErrTip: false
      }).then((res) => {})
    },
    // 获取首页标签
    GET_TABLIST(store, dataObj) {
      let fixedList = [
        { labelNo: '1', labelName: '最新博客' },
        { labelNo: '3', labelName: '热门博客' },
        { labelNo: '2', labelName: '关注' }
      ]
      if (dataObj.question) {
        fixedList = [
          { labelNo: '', labelName: '全部' }
        ]
      }
      let tabList = []
      return new Promise((resolve, reject) => {
        blogTab({ labelType: dataObj.labelType || 1 }).then((res) => {
          if (res.code === 200) {
            tabList = res.data.list || []
            if (!dataObj.client) {
              tabList = fixedList.concat(tabList)
            }
            resolve(tabList)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取博客列表
    GET_BLOGLIST(store, params) {
      return new Promise((resolve, reject) => {
        params.articleType = params.articleType || 1
        blogList(params).then((res) => {
          if (res.code === 200) {
            const pageObj = res.data
            if (pageObj.list.length) {
              pageObj.list.forEach((item) => {
                store.commit('CHANGE_TIME', item)
              })
            }
            resolve(pageObj)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取登录后博客详情
    GET_USERBLOGDETAIL(store, params) {
      return new Promise((resolve, reject) => {
        blogView(params, store.rootState.tokenInfo).then((res) => {
          // console.log(res)
          // console.log('GET_USERBLOGDETAIL===')
          if (res.code === 200) {
            store.commit('CHANGE_TIME', res.data)
            resolve(res)
          } else if (res.code >= 300 && res.code <= 400) {
            store.rootState.userInfo = ''
            store.rootState.tokenInfo = ''
            resolve(res)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取博客详情
    GET_BLOGDETAIL(store, params) {
      return new Promise((resolve, reject) => {
        blogDetail(params).then((res) => {
          // console.log(res)
          // console.log('GET_BLOGDETAIL===')
          if (res.code === 200) {
            store.commit('CHANGE_TIME', res.data)
            const blogInfo = res.data
            resolve(blogInfo)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取博主信息
    GET_BOLGGERINFO(store, params) {
      return new Promise((resolve, reject) => {
        bolggerInfo(params).then((res) => {
          // console.log(res)
          // console.log('GET_BOLGGERINFO===')
          if (res.code === 200) {
            const bolggerInfo = res.data
            resolve(bolggerInfo)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 登录获取博主信息
    GET_USERBOLGGERINFO(store, params) {
      return new Promise((resolve, reject) => {
        userBolggerInfo(params, store.rootState.tokenInfo).then((res) => {
          // console.log(res)
          // console.log('GET_USERBOLGGERINFO===')
          if (res.code === 200) {
            resolve(res)
          } else if (res.code >= 300 && res.code <= 400) {
            store.rootState.userInfo = ''
            store.rootState.tokenInfo = ''
            resolve(res)
          } else {
            console.log(res)
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取评论列表
    GET_COMMENTLIST(store, params) {
      return new Promise((resolve, reject) => {
        commentList(params).then((res) => {
          // console.log(res.data)
          // console.log('GET_COMMENTLIST===')
          if (res.code === 200) {
            res.data.list.forEach((item) => {
              item.gmtModified = item.gmtCreate
              store.commit('CHANGE_TIME', item)
            })
            const pageObj = res.data
            resolve(pageObj)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取资讯专区
    GET_ZONEINFO(store, params) {
      return new Promise((resolve, reject) => {
        infoZone(params).then((res) => {
          // console.log(res.data)
          // console.log('GET_ZONEINFO===')
          if (res.code === 200) {
            resolve(res.data)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 热门推荐
    GET_HOTINFO(store, params) {
      return new Promise((resolve, reject) => {
        hotZone(params).then((res) => {
          console.log(res.data)
          console.log('GET_HOTINFO===')
          if (res.code === 200) {
            resolve(res.data)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    }
  }
}
