import {
  questionsList,
  questionsDetail,
  questionsComment,
  userQuestionsDetail
} from '~/api/question.js'
export default {
  namespaced: true,
  actions: {
    // 获取问答列表
    GET_QUESTIONSLIST(store, params) {
      return new Promise((resolve, reject) => {
        // console.log(data.params)
        // console.log('===')
        questionsList(params).then((res) => {
          if (res.code === 200) {
            const pageObj = res.data
            if (pageObj.list.length) {
              pageObj.list.forEach((item) => {
                store.commit('CHANGE_TIME', item, { root: true })
              })
            }
            resolve(pageObj)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取登录后博客详情
    GET_USERQUESTIONDETAIL(store, params) {
      return new Promise((resolve, reject) => {
        userQuestionsDetail(params, store.rootState.tokenInfo).then((res) => {
          if (res.code === 200) {
            store.commit('CHANGE_TIME', res.data, { root: true })
            resolve(res)
          } else if (res.code >= 300 && res.code <= 400) {
            store.rootState.userInfo = ''
            store.rootState.tokenInfo = ''
            resolve(res)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取问答详情
    GET_QUESTIONDETAIL(store, params) {
      return new Promise((resolve, reject) => {
        questionsDetail(params).then((res) => {
          if (res.code === 200) {
            store.commit('CHANGE_TIME', res.data, { root: true })
            const blogInfo = res.data
            resolve(blogInfo)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    },
    // 获取评论列表
    GET_COMMENTLIST(store, params) {
      return new Promise((resolve, reject) => {
        questionsComment(params).then((res) => {
          if (res.code === 200) {
            res.data.list.forEach((item) => {
              store.commit('CHANGE_TIME', item, { root: true })
            })
            const pageObj = res.data
            resolve(pageObj)
          } else {
            reject(res.msg)
          }
        }).catch((msg) => {
          reject(msg)
        })
      })
    }
  }
}
