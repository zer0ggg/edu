import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import main from './modules/main.js'
import library from './modules/library.js'
import blog from './modules/blog.js'
import question from './modules/question.js'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      loading: false, // loading页面
      temporaryUrl: '', // 临时url
      count: false,
      webInfo: null, // 站点信息
      tokenInfo: '', // token信息
      aboutList: null, // about列表
      navList: { time: 0, list: [] }, // 导航信息
      clientData: { tokenName: 'RC_WEB_TOKEN' }, // 机构信息
      changeEwm: 1, // 隐藏小程序二维码
      openVip: false,
      userInfo: null,
      userMsgNum: 0 // 用户未读站内信数
    }),
    mutations: mutations,
    actions: actions,
    modules: {
      main,
      library,
      blog,
      question
    }
  })
}

export default createStore
