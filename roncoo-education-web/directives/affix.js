// @/directives/affix.js

// 导入 dom 处理工具
import dom from '@/utils/dom'

export default {
  bind(el) {
    dom.css(el, {
      zIndex: '200'
    })
  },
  inserted(el, { value }) {
    if (!process.client) return
    const elOffsetTop = Number.parseFloat(dom.offset(el).top); // 当前元素距离屏幕顶部的距离值
    const elOffsetleft = Number.parseFloat(dom.offset(el).left || 0); // 当前元素距离屏幕顶部的距离值
    const elTop = (+el.getAttribute('exceedTop')) || elOffsetTop
    const top = el.getAttribute('top') || '86'
    // 滚动处理
    el._affix_scroll_handle = function (e) {
      const scrollInstance = window.pageYOffset || window.scrollY;
      let position = 'static';
      if (scrollInstance >= elTop) {
        position = 'fixed'
      } else {
        position = 'static'
      }
      dom.css(el, {
        position,
        left: elOffsetleft + 'px',
        top: top + 'px'
      })
    }
    // 点击处理（点击被绑指令元素时，自动滚动到顶部）
    el._affix_click_handle = function () {
      window.scrollTo(0, 0);
    }
    window.addEventListener('scroll', el._affix_scroll_handle);
    el.addEventListener('click', el._affix_click_handle)
  },
  unbind(el) {
    el._affix_scroll_handle && window.removeEventListener('scroll', el._affix_scroll_handle)
    el._affix_click_handle && el.removeEventListener('click', el._affix_click_handle)
  }
}
