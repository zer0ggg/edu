// import conf from '../config'
// import cookie from '../utils/cookies'
// import Vue from 'vue'

// const getInServer = function (req, key) {
//   const serviceCookie = {}
//   req.cookie.split(';').forEach(function (val) {
//     const parts = val.split('=')
//     serviceCookie[parts[0].trim()] = (parts[1] || '').trim()
//   })
//   return serviceCookie[key]
// }

// export default function ({ $axios, redirect }) {
//   const baseURL = `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 3000}/`
//   $axios.setBaseURL(baseURL)
//   if (process.client) {
//     $axios.setHeader('token', cookie.getInClient(conf.CLIENT.tokenName))
//   }
//   $axios.onRequest(config => {
//     console.log('config', config)
//     if (process.server) {
//       const token = getInServer(config.headers.common, conf.CLIENT.tokenName)
//       config.headers.common.token = token
//     }
//   })
//   $axios.onResponse(response => {
//     // 对响应数据做点什么
//     if (response.data.code === 200) {
//       return Promise.resolve(response.data)
//     } else {
//       if (response.data.code >= 300 && response.data.code <= 400) {
//         redirect('/login')
//       }
//       if (process.client) {
//         const d = JSON.stringify(response.config.data || response.config.params)
//         if (d.isShowErrTip !== false) {
//           // 过滤同时多个接口报token错误 会出现多个提示bug
//           const title = localStorage.getItem('___errmsg')
//           const time = localStorage.getItem('___errmsgTime')
//           const newtime = (new Date()).getTime()
//           if (title !== response.data.msg || (newtime - time) > 2000) {
//             localStorage.setItem('___errmsg', response.data.msg)
//             localStorage.setItem('___errmsgTime', newtime)
//             Vue.prototype.$msgBox(response.data.msg);
//           }
//         }
//       }
//       console.warn(response.data)
//       return Promise.reject(response.data)
//     }
//   })
//   $axios.onError(error => {
//     const code = parseInt(error.response && error.response.status)
//     if (code >= 300 && code <= 400) {
//       redirect('/login')
//     }
//   })
// }
