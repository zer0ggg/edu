import Vue from 'vue'
import affix from '@/directives/affix'

Vue.directive('affix', affix)
