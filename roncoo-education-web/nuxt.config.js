const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const pkg = require('./package')
const config = require('./config/conf')

module.exports = {
  mode: 'universal',
  router: {
    middleware: ['checkuser'],
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'indexPath',
        path: '/index',
        component: resolve(__dirname, 'pages/index.vue')
      })
    }
  },
  telemetry: false,

  /*
     ** Headers of the page
     */
  head: {
    title: pkg.name,
    meta: [
      {charset: 'utf-8'},
      {name: 'renderer', content: 'webkit'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {'http-equiv': 'content-type', content: 'text/html;charset=utf-8'}
    ]
  },

  /*
     ** Customize the progress-bar color
     */
  loading: {color: '#fff'},

  /*
     ** Global CSS
     */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'vue-datetime/dist/vue-datetime.css',
    'wangeditor/release/wangEditor.min.css',
    '@/assets/css/main.scss'
  ],

  /*
     ** Plugins to load before mounting the App
     */
  plugins: [
    {src: '~/plugins/element-ui', ssr: true},
    '~/plugins/axios',
    '~/plugins/message',
    '~/plugins/dragging',
    '~/plugins/datetime',
    '~/plugins/lockr',
    '~/plugins/jsonp',
    '~/plugins/directives'
  ],

  /*
     ** Nuxt.js modules
     */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@gauseen/nuxt-proxy',
    '@nuxtjs/proxy',
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
     ** Axios module configuration
     */

  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxy: true
  },
  proxyTable: {
    '/apis': {
      target: config.baseUrl,
      ws: false,
      pathRewrite: {
        '^/apis': '' // 重写接口
      }
    }
  },
  env: {
    NODE_ENV: process.env.NODE_ENV || 'production'
  },
  /*
     ** Build configuration
     */
  build: {
    //publicPath: '',
    vendor: [],
    maxChunkSize: 300000,
    // transpile: [/^element-ui/],
    /*
         ** You can extend webpack config here
         */
    // babel: {
    //   plugins: [
    //     [
    //       'component',
    //       {
    //         libraryName: 'element-ui',
    //         styleLibraryName: 'theme-chalk'
    //       }
    //     ]
    //   ],
    //   comments: true
    // },
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    // 过滤日志
    plugins: [
      new UglifyJsPlugin({
        uglifyOptions: {
          // warnings: false
          compress: {
            warnings: false,
            pure_funcs: process.env.NODE_ENV === 'production' ? ['console.log'] : []
            // drop_console: true
          }
        },
        sourceMap: true,
        cache: true,
        parallel: true
      })
    ]
  }
}
