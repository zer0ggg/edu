import http from './request.js'

// 课程分类列表接口
export const getClassList = (params = {}) => {
  return http().post('/course/api/course/category/list', params)
}

// 专区课程列表接口
export const getZoneList = (params = {}) => {
  return http().post('/course/api/zone/course/list', params)
}

// 课程列表
export const courseList = (params = {}) => {
  return http().post('/course/api/course/list', params)
}

// 课程详情
export const courseDetail = (params = {}) => {
  return http().post('/course/api/course/view', params)
}

// 登录课程详情
export const userCourseDetail = (params = {}, token) => {
  return http(token).post('/course/auth/course/view', params)
}

// 获取sign
export const chapterSign = (params = {}) => {
  return http().post('/course/auth/course/sign', params)
}

// 获取章节
export const chapterDetail = (params = {}) => {
  return http().post('/course/api/course/chapter/list', params)
}

// 附件下载
export const downAc = (params = {}) => {
  return http().post('/course/auth/course/accessory/download', params)
}

// 直播链接
export const liveUrl = (params = {}) => {
  return http().post('/course/auth/course/getLiveUrl', params)
}
// 直播回放链接
export const playback = (params = {}) => {
  return http().post('/course/auth/course/getPlaybackUrl', params)
}

// 课程搜索
export const searchCourse = (params = {}) => {
  return http().post('/course/api/course/search/list', params)
}

// 课程推荐
export const recommendList = (params = {}) => {
  return http().post('/course/api/course/recommend/list', params)
}

// 用户收藏课程保存
export const collectionSave = (params = {}) => {
  return http().post('/course/auth/user/collection/course/save', params)
}

// 用户收藏课程取消
export const removeCollection = (params = {}) => {
  return http().post('/course/auth/user/collection/course/delet', params)
}
// 热门文库推荐
export const resourceRecommend = (params = {}) => {
  return http().post('/course/api/resource/recommend/list', params)
}
// 评论添加
export const courseCommentSave = (params = {}) => {
  return http().post('/course/auth/courseComment/save', params)
}

// 登录后显示评论列表
export const authCourseCommentList = (params = {}) => {
  return http().post('/course/auth/courseComment/list', params)
}

// 未登录显示评论列表
export const courseCommentList = (params = {}) => {
  return http().post('/course/api/courseComment/list', params)
}

// 删除评论
export const courseCommentDelete = (params = {}) => {
  return http().post('/course/auth/courseComment/delete', params)
}

// 查询个人看课时的进度
export const userCourseProgress = (params = {}) => {
  return http().post('/data/auth/course/log/course/progress', params)
}
