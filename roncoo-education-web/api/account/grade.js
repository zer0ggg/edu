import http from '../request.js'
// 添加班级
export const saveGrade = (params = {}) => {
  return http().post('/exam/auth/grade/info/save', params)
}

// 班级信息修改
export const updateGrade = (params = {}) => {
  return http().put('/exam/auth/grade/info/update', params)
}

// 分页列出--讲师创建的班级
export const getGradeList = (params = {}) => {
  return http().post('/exam/auth/grade/info/page', params)
}

// 解散班级
export const disbandGrade = (params = {}) => {
  return http().post('/exam/auth/grade/info/disband', params)
}

// 学生加入班级
export const studentAddGrade = (params = {}) => {
  return http().post('/exam/auth/grade/student/grade/add', params)
}

// 学生退出班级
export const studentQuitGrade = (params = {}) => {
  return http().put('/exam/auth/grade/student/quit', params)
}

// 踢出班级学生
export const GradeOutstudent = (params = {}) => {
  return http().put('/exam/auth/grade/student/kick/out', params)
}

// 设置或取消管理员
export const updateGradeAdmin = (params = {}) => {
  return http().put('/exam/auth/grade/student/update/admin', params)
}

// 班级学生分页列出
export const gradeStudentList = (params = {}) => {
  return http().post('/exam/auth/grade/student/page', params)
}

// 学生申请记录分页列出
export const applyRecordList = (params = {}) => {
  return http().post('/exam/auth/grade/apply/record/page', params)
}

// 学生申请记录审核
export const auditRecord = (params = {}) => {
  return http().put('/exam/auth/grade/apply/record/audit', params)
}

// 学生申请记录未审核总数
export const auditRecordTotal = (params = {}) => {
  return http().post('/exam/auth/grade/apply/record/summary', params)
}

// 批量学生申请记录审核
export const auditRecordList = (params = {}) => {
  return http().put('/exam/auth/grade/apply/record/audit/batch', params)
}

// 学生申请记录审核提示列表
export const ignoreAuditRecordTipList = (params = {}) => {
  return http().put('exam/auth/grade/apply/record/ignore', params)
}

//
export const auditRecordTipList = (params = {}) => {
  return http().post('exam/auth/grade/apply/record/tips', params)
}

// 学生我的班级列表
export const myGradeList = (params = {}) => {
  return http().post('exam/auth/grade/info/grade/list/all', params)
}

// 布置学生考试
export const saveGradeExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/save', params)
}

// 班级考试学生列表
export const getGradeExamStudent = (params = {}) => {
  return http().post('/exam/auth/grade/exam/get/student/ids', params)
}

// 编辑班级考试
export const editGradeExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/edit', params)
}

// 学生考试提示列表
export const relationTips = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/tips', params)
}

// 学生考试忽略查看
export const ignoreRelationTips = (params = {}) => {
  return http().put('exam/auth/grade/exam/student/relation/ignore', params)
}

//  讲师的班级试卷列表
export const GradeAdminExamList = (params = {}) => {
  return http().post('exam/auth/grade/exam/admin/page', params)
}

// 学生的的班级试卷列表
export const StudentGradeExamList = (params = {}) => {
  return http().post('/exam/auth/grade/exam/student/relation/page', params)
}

// 编辑学生名字
export const GradeEditStudent = (params = {}) => {
  return http().put('exam/auth/grade/student/edit', params)
}

// 班级试卷补交接口
export const compensate = (params = {}) => {
  return http().post('exam/auth/grade/exam/edit/compensate/status', params)
}

// 班级试卷补交接口
export const GradeRemoveExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/del', params)
}
// 是否班级管理员
export const isGradeAdmin = (params = {}) => {
  return http().put('exam/auth/grade/student/isAdmin', params)
}

// 获取考试记录详情
export const examRelationView = (params = {}, token) => {
  return http(token).post('exam/auth/grade/exam/student/relation/view', params)
}

// 继续考试
export const continueExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/continue', params)
}

// 开始考试
export const beginExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/begin', params)
}

// 考试保存答案
export const saveExamAnswer = (params = {}) => {
  return http().post('exam/auth/grade/student/exam/answer/save', params)
}

// 提交考试
export const submitExam = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/submit/audit', params)
}

// 查看系统评分详情
export const relationExamScore = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/view/score', params)
}

// 查看试卷答案
export const getStudentAnswer = (params = {}) => {
  return http().post('exam/auth/grade/student/exam/answer/view', params)
}

// 获取当前考试记录的学生列表
export const getExamStudentList = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/list', params)
}

// 评卷完成接口
export const completeAuditExam = (params = {}) => {
  return http().put('exam/auth/grade/exam/student/relation/audit/complete', params)
}

// 评卷接口
export const auditExamWnswer = (params = {}) => {
  return http().post('exam/auth/grade/student/exam/answer/sys/audit', params)
}

// 班级考试情况（学员成绩列表）
export const examResult = (params = {}) => {
  return http().post('exam/auth/grade/exam/student/relation/page/grade', params)
}
