import http from '../request.js'

// 修改用户信息
export const updateUserEducationInf = (params = {}) => {
  return http().post('user/auth/user/ext/update', params)
}
// 获取讲师信息
export const getLecturerInfo = (params = {}) => {
  return http().post('user/auth/lecturer/audit/view', params)
}
// 修改讲师信息recommendList
export const updataLecturerInfo = (params = {}) => {
  return http().post('user/auth/lecturer/audit/update', params)
}
// 修改密码
export const updatePassword = (params = {}) => {
  return http().post('user/api/user/update/password', params)
}
// 银行卡信息
export const cardInfo = (params = {}) => {
  return http().post('user/auth/user/account/view', params)
}
// 申请提现
export const getCash = (params = {}) => {
  return http().post('user/auth/user/account/extract/log/save', params)
}
// 讲师订单收益
export const teacherOrderList = (params = {}) => {
  return http().post('user/auth/order/info/lecturer', params)
}
// 讲师提现记录
export const teacherCashList = (params = {}) => {
  return http().post('user/auth/user/account/extract/log/list', params)
}
// 绑定银行卡
export const bindCard = (params = {}) => {
  return http().post('user/auth/user/account/update', params)
}
// 关注讲师列表
export const attentionList = (params = {}) => {
  return http().post('user/auth/lecturer/attention/list', params)
}
// 用戶取消关注讲师
export const attentionDelete = (params = {}) => {
  return http().post('user/auth/lecturer/attention/delete', params)
}
// 学习记录
export const studyList = (params = {}) => {
  return http().post('/data/auth/course/log/list', params)
}
// 频道详情
export const channelInfo = (params = {}) => {
  return http().post('user/auth/lecturer/channel/view', params)
}
// 站内信分页
export const messageList = (params = {}) => {
  return http().post('user/auth/msg/user/list', params)
}
// 站内信详情
export const messageInfo = (params = {}) => {
  return http().post('user/auth/msg/user/read', params)
}
// 站内信数量
export const messageNun = (params = {}) => {
  return http().post('user/auth/msg/user/num', params)
}

// 设置所有站内信已读
export const readAllMessage = (params = {}) => {
  return http().post('/user/auth/msg/user/read/all', params)
}

// 我的推荐
export const recommendList = (params = {}) => {
  return http().post('user/auth/user/recommended/list', params)
}
// 分享图片生成
export const sharingRecommended = (params = {}) => {
  return http().post('/user/auth/user/ext/sharing/recommended', params)
}
// 活体认证
export const verifyFace = (params = {}) => {
  return http().post('/user/auth/user/ext/face/contras/code', params)
}
// 检测活体认证结果
export const checkVerifyFace = (params = {}) => {
  return http().post('/user/auth/user/ext/face/contras/pass', params)
}
