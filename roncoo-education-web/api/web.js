import http from './request.js'

// 校验图片验证码
export const checkImgCode = (params) => {
  return http().post('/web/user/api/send/sms/verify/code', params)
}

// 获取手机验证码
export const getMobileCode = (params = {}) => {
  return http().post('/web/user/api/send/sms/log', params)
}
// 校验手机验证码
export const checkCode = (params = {}) => {
  return http().post('/user/api/user/verify/send/code', params)
}
