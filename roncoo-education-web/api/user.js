import http from './request.js'

// 用户登录
export const userLogin = (params = {}) => {
  return http().post('/user/api/user/login/password', params)
}
// 用户登录
export const userMsgLogin = (params = {}) => {
  return http().post('/user/api/user/login/code', params)
}

// 用户注册
export const register = (params = {}) => {
  return http().post('/user/api/user/register', params)
}

// 用户修改密码接口
export const updatePassword = (params = {}) => {
  return http().post('/user/api/user/update/password', params)
}

// 获取手机验证码
export const getMobileCode = (params = {}) => {
  return http().post('/user/api/user/send/code', params)
}

// 获取用户信息
export const getUserInfo = (params = {}, token = '') => {
  return http(token).post('/user/auth/user/ext/view', params)
}

// 申请讲师
export const teacherEnter = (params = {}) => {
  return http().post('/user/api/lecturer/audit/save', params)
}

// 申请直播
export const getLive = (params = {}) => {
  return http().post('/user/auth/lecturer/audit/applyLive', params)
}

// 查看讲师信息
export const getTeacherInfo = (params = {}) => {
  return http().post('/user/auth/lecturer/view', params)
}

// 修改频道密码
export const updateChannelPasswd = (params = {}) => {
  return http().post('/course/auth/course/update/channel/passwd', params)
}

// 修改频道信息
export const channelUpdate = (params = {}) => {
  return http().post('/user/auth/lecturer/update/channel', params)
}

// 关注讲师
export const attentionSave = (params = {}) => {
  return http().post('/user/auth/lecturer/attention/save', params)
}

// 判断用户与代理是否存在接口
export const checkUserAgent = (params = {}) => {
  return http().post('/agent/api/agent/check', params)
}

// 记录用户学习记录
export const saveCourseLog = (params = {}) => {
  return http().post('/data/auth/course/log/save', params)
}

// 查看讲师信息
export const getLecturer = (params = {}) => {
  return http().post('/user/api/lecturer/view', params)
}
// 查看生成认证二维码
export const verifyFace = (params = {}) => {
  return http().post('/user/auth/user/ext/face/contras/code', params)
}

// 认证失败
export const verifyFail = (params = {}) => {
  return http().post('/user/auth/user/ext/face/contras/fail', params)
}

// 添加表单
export const saveUserResearch = (params = {}) => {
  return http().post('/user/auth/user/research/save', params)
}

// 编辑表单
export const editUserResearch = (params = {}) => {
  return http().post('/user/auth/user/research/edit', params)
}

// 获取表单
export const getUserResearch = (params = {}) => {
  return http().post('/user/auth/user/research/view', params)
}
