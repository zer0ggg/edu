import http from './request.js'

// 获取头部导航信息接口
export const getAdvList = (params = {}) => {
  return http().post('/system/api/adv/list', params)
}

// 获取头部导航信息接口
export const getNavList = (params = {}) => {
  return http().post('/system/api/nav/bar/list', params)
}

// 会员列表
export const vipList = (params = {}) => {
  return http().post('system/api/vip/set/list', params)
}

// 获取站点信息接口
export const getWebsite = (params = {}) => {
  return http().post('/system/api/website/get', params)
}

// 友情链接
export const friendLink = (params = {}) => {
  return http().post('/system/api/website/link', params)
}

// 关于我们列表
export const aboutList = (params = {}) => {
  return http().post('/system/api/website/nav/list', params)
}

// 关于我们详情
export const aboutInfo = (params = {}) => {
  return http().post('/system/api/website/nav/article/get', params)
}

// 获取首页专区列表（广告下面的课程）
export const getZoneList = (params = {}) => {
  return http().post('system/api/zone/list', params)
}
// 获取ali上传配置
export const getUploadConfig = (params) => {
  return http().post('/system/auth/upload/aliyun', params)
}
// 获取polyv上传sign
export const getPolyvVideoSign = (params) => {
  return http().post('/system/auth/upload/polyv', params)
}
// 生成跳小程序二维码
export const referralCode = (params) => {
  return http().post('/system/api/website/referral/code', params)
}
