import * as axios from 'axios'
import { Message } from 'element-ui';
import cookie from '../utils/cookies'
const tokenName = 'RC_WEB_TOKEN'
let serverUrl = '';
const createHttp = (token) => {
  const options = {
    timeout: 10000
  }
  const head = {}
  // 需要全路径才能工作
  if (process.server) {
    if (token) {
      head.token = token
    }
    options.baseURL = `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 3000}/`
  }
  if (process.client) {
    head.token = cookie.getInClient(tokenName)
  }
  options.headers = head
  // 添加请求拦截器
  const http = axios.create(options)
  http.interceptors.request.use(function (config) {
    let url = '';
    if (config.url[0] === '/') {
      url = '/apis' + config.url
    } else {
      url = '/apis/' + config.url
    }
    config.url = url
    serverUrl = url
    if (process.server) {
      console.info('request to: ' + serverUrl)
    }
    return config
  }, function (error) {
    // 对请求错误做些什么
    console.warn(error)
    if (process.client) {
      return Promise.reject(error)
    }
  })
  http.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if (response.data.code === 200) {
      return Promise.resolve(response.data)
    } else {
      console.info('request to: ' + serverUrl)
      console.warn(JSON.stringify(response.data))

      const d = JSON.parse(response.config.data || response.config.params)
      if (d._No_dispose) {
        return Promise.resolve(response.data)
      }

      if (response.data.code >= 300 && response.data.code <= 400) {
        console.log('request login')
        if (process.client) { // 客户端请求接口token 过期让他重新登录
          window.location.href = '/login?t=login'
        } else {
          console.info(JSON.stringify(response.data))
        }
      }
      if (process.client) {
        const d = JSON.parse(response.config.data || response.config.params)
        if (d.isShowErrTip !== false) {
          // 过滤同时多个接口报token错误 会出现多个提示bug
          const title = localStorage.getItem('___errmsg')
          const time = localStorage.getItem('___errmsgTime')
          const newtime = (new Date()).getTime()
          if (title !== response.data.msg || (newtime - time) > 2000) {
            localStorage.setItem('___errmsg', response.data.msg)
            localStorage.setItem('___errmsgTime', newtime)
            Message.error(response.data.msg);
          }
          return Promise.reject(response.data)
        } else {
          return Promise.resolve(response.data)
        }
      } else {
        return Promise.resolve(response.data)
      }
    }
  }, function (error) {
    // 对响应错误做点什么
    if (process.client) {
      return Promise.reject(error)
    } else {
      console.info('request to: ' + serverUrl)
      console.info(JSON.stringify(error))
      return Promise.resolve(error.response.data)
    }
  })
  return http
}

export default createHttp
