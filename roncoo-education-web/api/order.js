import http from './request.js'

// 创建订单
export const orderSave = (params = {}) => {
  return http().post('user/auth/order/info/pay', params)
}

// 创建试卷订单
export const orderSaveExam = (params = {}) => {
  return http().post('exam/auth/exam/pay', params)
}

// 订单信息
export const orderInfo = (params = {}) => {
  return http().post('user/auth/order/info/view', params)
}
