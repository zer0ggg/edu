import http from './request'

// 问答列表
export const questionsList = (params = {}) => {
  return http().post('/community/api/questions/list', params)
}

// 搜索问答
export const searchQuestions = (params = {}) => {
  return http().post('/community/api/questions/search/list', params)
}
// 添加问答信息接口
export const addQuestion = (params = {}) => {
  return http().post('/community/auth/questions/save', params)
}

// 问答详情接口
export const questionsDetail = (params = {}) => {
  return http().post('/community/api/questions/view', params)
}

// 已登录问答详情接口
export const userQuestionsDetail = (params = {}, token) => {
  return http(token).post('/community/auth/questions/view', params)
}

// 问答评论展示接口
export const questionsComment = (params = {}) => {
  return http().post('/community/api/questions/comment/list', params)
}

// 添加评论信息接口
export const addQuestionComment = (params = {}) => {
  return http().post('/community/auth/questions/comment/save', params)
}

// 点赞、收藏问答
export const likeOrCollQuestion = (params = {}) => {
  return http().post('/community/auth/questions/user/record/save', params)
}

// 取消点赞、收藏
export const delLikeOrCollQuestion = (params = {}) => {
  return http().post('/community/auth/questions/user/record/delete', params)
}

// 我的问答
export const myQuestionList = (params = {}) => {
  return http().post('/community/auth/questions/list', params)
}

// 修改问答信息接口
export const updateQuestion = (params = {}) => {
  return http().post('/community/auth/questions/update', params)
}

// 删除问答信息接口
export const deleteMyQuestion = (params = {}) => {
  return http().post('/community/auth/questions/delete', params)
}

// 收藏的问答
export const myQuestionCollection = (params = {}) => {
  return http().post('/community/auth/questions/user/record/list', params)
}

// 我的问答评论列表
export const myQuestionComment = (params = {}) => {
  return http().post('/community/auth/questions/comment/list', params)
}
