import http from './request'

// 课程详情
export const courseDetail = (params = {}) => {
  return http().post('/course/api/course/audit/view', params)
}
// 获取课程章节
export const chapterDetail = (params = {}) => {
  return http().post('/course/api/course/chapter/audit/list', params)
}
