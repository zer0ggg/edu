import http from '../request.js'
// 首页标签
export const blogTab = (params = {}) => {
  return http().post('community/api/label/list', params)
}
// 首页博客列表
export const blogList = (params = {}) => {
  return http().post('community/api/blog/list', params)
}
// 博客详情
export const blogDetail = (params = {}) => {
  return http().post('community/api/blog/view', params)
}
// 博客评论
export const commentList = (params = {}) => {
  return http().post('community/api/blog/comment/list', params)
}
// 未登录博主信息
export const bolggerInfo = (params = {}) => {
  return http().post('community/api/blogger/view', params)
}
// 变成博主
export const changeBlogger = (params = {}) => {
  return http().post('community/auth/blogger/save', params)
}
// 搜索
export const searchBlog = (params = {}) => {
  return http().post('community/api/blog/search/list', params)
}
// 推荐博主
export const recommendBlogger = (params = {}) => {
  return http().post('community/auth/blogger/recommend/list', params)
}
// 资讯专区
export const infoZone = (params = {}) => {
  return http().post('community/api/article/zone/category/list', params)
}
// 资讯专区列表
export const infoZoneList = (params = {}) => {
  return http().post('community/api/article/zone/ref/list', params)
}
// 资讯推荐
export const hotZone = (params = {}) => {
  return http().post('community/api/article/recommend/list', params)
}

// 热门博客
export const hotBlog = (params = {}) => {
  return http().post('/community/api/blog/list', params)
}
