import http from '../request.js'
// 关注博客列表
export const activeBolgList = (params = {}) => {
  return http().post('community/auth/blog/attention/list', params)
}
// 登录博主信息
export const userBolggerInfo = (params = {}, token) => {
  return http(token).post('community/auth/blogger/view', params)
}
// 保存博客
export const addBlog = (params = {}) => {
  return http().post('community/auth/blog/save', params)
}
// 修改博客
export const changeBlog = (params = {}) => {
  return http().post('community/auth/blog/update', params)
}
// 博主博客列表
export const userBlogList = (params = {}) => {
  return http().post('community/auth/blog/list', params)
}
// 博主查看博客
export const blogView = (params = {}, token) => {
  return http(token).post('community/auth/blog/view', params)
}
// 点赞或收藏
export const likeOrColl = (params = {}) => {
  return http().post('community/auth/blog/user/record/save', params)
}
// 取消点赞或收藏
export const delLikeOrColl = (params = {}) => {
  return http().post('community/auth/blog/user/record/delete', params)
}
// 我的关注
export const myActive = (params = {}) => {
  return http().post('community/auth/blogger/user/record/list', params)
}
// 我的粉丝
export const myFans = (params = {}) => {
  return http().post('community/auth/blogger/user/record/fans/list', params)
}
// 我的收藏
export const myCollection = (params = {}) => {
  return http().post('community/auth/blog/user/record/collection/list', params)
}
// 取消收藏或点赞
export const closeCollOrLike = (params = {}) => {
  return http().post('community/auth/blog/user/record/delete', params)
}
// 关注博主
export const activeBlogger = (params = {}) => {
  return http().post('community/auth/blogger/user/record/save', params)
}
// 放进回收站
export const addRemove = (params = {}) => {
  return http().post('community/auth/blog/stand', params)
}
// 彻底删除博客
export const removeBlog = (params = {}) => {
  return http().post('community/auth/blog/delete', params)
}
// 添加评论
export const addComment = (params = {}) => {
  return http().post('community/auth/blog/comment/save', params)
}
// 博主评论列表
export const userCommentList = (params = {}) => {
  return http().post('community/auth/blog/comment/list', params)
}
// 删除评论
export const deleteComment = (params = {}) => {
  return http().post('community/auth/blog/comment/delete', params)
}
// 博主个人中心信息
export const selfInfo = (params = {}) => {
  return http().post('community/auth/blogger/myself', params)
}
// 博主个人中心信息更新
export const setBloggerInfo = (params = {}) => {
  return http().post('community/auth/blogger/update', params)
}
