import * as axios from 'axios'
import cookie from '../utils/cookies'
const tokenName = 'RC_WEB_TOKEN'

const createUpload = (url, params = {}, cb) => {
  const cancelToken = axios.CancelToken
  const source = cancelToken.source();
  const config = {
    onUploadProgress: (progressEvent) => {
      const complete = (progressEvent.loaded / progressEvent.total * 100 | 0)
      if (cb) {
        cb(complete, source)
      }
    },
    headers: {
      'Content-Type': 'multipart/form-data',
      token: cookie.getInClient(tokenName)
    },
    cancelToken: source.token,
    timeout: 1000 * 60 * 90
  }
  return new Promise((resolve, reject) => {
    axios.post(url + '?token=' + cookie.getInClient(tokenName), params, config).then((res) => {
      resolve(res.data)
    }).catch((error) => {
      if (axios.isCancel(error)) { // 主要是这里
        console.log('取消成功')
      }
      reject(error)
    })
  })
}
// 上传图片
export const uploadPic = (params = {}, cb) => {
  return createUpload('/apis/system/auth/upload/pic', params, cb)
}
// 上传文档
export const uploadDoc = (params = {}, cb) => {
  return createUpload('/apis/system/auth/upload/doc', params, cb)
}

// 上传试题
export const uploadProblem = (params = {}, cb) => {
  return createUpload('/apis/exam/auth/exam/problem/upload/excel', params, cb)
}

// 上传班级导入学员
export const uploadGradeStudent = (params = {}, cb) => {
  return createUpload('/apis/exam/auth/grade/student/upload/excel', params, cb)
}

// 上传视频
export const uploadResVideo = (params = {}, cb) => {
  return createUpload('/apis/course/api/upload/video', params, cb)
}

// 试卷上传视频
export const uploadExamResVideo = (params = {}, cb) => {
  return createUpload('/apis/exam/api/upload/video', params, cb)
}
