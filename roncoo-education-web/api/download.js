// 导出
import { formDownloadFile } from '~/utils/frmDownload.js'

export const studyExport = (params = {}) => {
  formDownloadFile('/data/auth/course/log/export', params, 'post')
}
