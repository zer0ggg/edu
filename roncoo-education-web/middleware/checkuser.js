export default function (context) {
  const { store, req, redirect, route } = context
  if (process.server) {
    store.commit('GET_TOKEN_SERVER', req)
  }
  if (process.client) {
    store.commit('INIT_WEB')
  }
  if (route.path.includes('account') && !store.state.tokenInfo) {
    redirect('/login')
  }
}
