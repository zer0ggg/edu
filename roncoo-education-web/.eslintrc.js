module.exports = {
    root: true,
    env: {
      browser: true,
      node: true
    },
    parserOptions: {
      parser: 'babel-eslint'
    },
    extends: [
      // '@nuxtjs',
      'plugin:nuxt/recommended'
    ],
    // add your custom rules here
    rules: {
      'nuxt/no-cjs-in-config': 'off',
      'arrow-parens': 'off',
      'prefer-const': 2,
      'indent': 'off',
      'semi': 'off',
      'vue/no-v-html': 'off',
      "vue/singleline-html-element-content-newline": "off",
      "vue/html-closing-bracket-newline": "off",
      'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'vue/attributes-order': 'off',
      "vue/html-indent": "off",
      "vue/html-closing-bracket-newline": "off",
      "vue/html-self-closing": "off"
    }
  }

