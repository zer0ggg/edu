if (process.env.NODE_ENV === 'development') {
  // 开发环境
  module.exports = {
    baseUrl: 'http://dev.school.roncoos.com/gateway'
  }
} else {
  // 生产环境
  module.exports = {
    baseUrl: 'http://localhost/gateway'
  }
}
