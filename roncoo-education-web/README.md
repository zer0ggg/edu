# roncoo-education-web
> 领课教育系统前端门户工程

## Build Setup
``` 
# install dependencies
$ yarn install

# serve with hot reload at localhost:3001
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```
浏览器访问 [http://localhost:3001](http://localhost:3001)

## 发布
```
# 构建生产环境
npm run build:modern

# pm2 运行
pm2 start npm --name roncoo-education-web -- run start:modern
```

## docker
```
//构建
docker build -t web .
//运行
docker run -d --name education-web -p 80:3000 web
```
