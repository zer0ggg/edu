import OSS from 'ali-oss'
import { getUploadConfig } from '@/api/system.js'
export default {
  data() {
    return {
      aliClient: undefined, // 阿里上传SDK实例
      aliResumeClient: undefined, // 阿里续传SDK实例
      tempCheckpoint: undefined, // 续传对象
      directoryPath: '',
      uploading: false,
      uploadStatus: [],
      uploadList: []
    }
  },
  filters: {
    uploadStatus(val) {
      if (val === 2) {
        return '正在上传'
      } else if (val === 3) {
        return '上传成功'
      } else if (val === 4) {
        return '上传失败'
      } else if (val === 5) {
        return '上传暂停'
      } else if (val === 6) {
        return '正在保存'
      } else if (val === 7) {
        return '保存成功'
      } else if (val === 8) {
        return '保存失败'
      } else {
        return '等待上传'
      }
    }
  },
  mounted() {
    this.initOssConfig()
  },
  methods: {
    receiveFile(files) {
      console.log(files)
      if (files.length) {
        for (let i = 0; i < files.length; i++) {
          const file = {
            name: files[i].name,
            status: 1,
            progress: 0 // 上传进度
          }
          console.log(this.uploadStatus)

          this.uploadStatus.push(file);
          this.uploadList.push(files[i])
        }
        // 开始上传
        this.uploading = true;
        this.startUpload()
      }
    },
    uploadUploading() {
      let is = false;
      this.uploadStatus.forEach((e, i) => {
        if (e.status !== 7) {
          is = true;
        }
      })
      this.uploading = is
    },
    startUpload() {
      let uploadNum = 0;
      const maxUploadNum = 5;
      console.log(this.uploadStatus);
      this.uploadStatus.forEach((e, i) => {
        if (e.status === 2) {
          uploadNum++;
        }
      })
      this.uploadStatus.forEach((e, i) => {
        if (e.status === 1) {
          if (uploadNum < maxUploadNum) {
            uploadNum++;
            e.status = 2
            this.multipartUpload(i)
          }
        }
      })
    },
    // 暂停上传
    stopAllUpload() {
      this.aliClient.cancel()
      this.aliResumeClient.cancel()
    },
    randomString(len = 32) {
      const $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
      const maxPos = $chars.length;
      let pwd = '';
      for (let i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
      }
      return pwd;
    },
    // 初始化上传配置
    initOssConfig() {
      getUploadConfig({}).then(res => {
        if (res.code === 200) {
          if (res.data.directory) {
            this.directoryPath = res.data.directory + '/'
            this.aliyunOssUrl = res.data.aliyunOssUrl
          }
          this.bucket = res.data.aliyunOssBucket
          const ossConfig = {
            // region以杭州为例（oss-cn-hangzhou），其他region按实际情况填写。
            region: res.data.endPoint,
            // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
            accessKeyId: res.data.aliyunAccessKeyId,
            accessKeySecret: res.data.aliyunccessKeySecret,
            bucket: res.data.aliyunOssBucket
          }
          this.aliClient = new OSS(ossConfig);
          this.aliResumeClient = new OSS(ossConfig);

        }
      }).catch(msg => {
        this.$msgBox({
          content: msg.msg
        })
      })
    },
    // 暂停上传
    stopUpload(int = 0) {
      this.uploadStatus[int].status = 5
      this.uploadStatus.forEach((e, i) => {
        e.status = 5
      })
      this.aliClient.cancel()
      this.aliResumeClient.cancel()
    },
    // 继续上传
    resumeUpload(int) {
      this.uploadStatus[int].status = 2
      if (this.uploadStatus[int] && this.uploadStatus[int].isAliUpload) {
        this.aliResumeUpload(int)
      } else {
        this.multipartUpload(int)
      }
    },
    // ali-oss上传
    multipartUpload(fileIndex) {
      const nextIndex = fileIndex + 1
      const file = this.uploadList[fileIndex]
      const stat = this.uploadStatus[fileIndex]
      const that = this
      const fileType = file.name.split('.')
      // stat.fileName = this.randomString() + '.' + file.type.substr(file.type.indexOf('/') + 1)
      stat.fileName = this.randomString() + '.' + fileType[fileType.length - 1]
      // object-key可以自定义为文件名（例如file.txt）或目录（例如abc/test/file.txt）的形式，实现将文件上传至当前Bucket或Bucket下的指定目录。
      this.aliClient.multipartUpload(this.directoryPath + stat.fileName, file, {
        progress: function (p, checkpoint) {
          // 断点记录点。浏览器重启后无法直接继续上传，您需要手动触发上传操作。
          that.tempCheckpoint = checkpoint;
          stat.progress = parseInt(p * 100)
          stat.status = 2
        },
        // parallel: 5, // 分片数量
        // partSize: 1024 * 1024 * 40, // 分片大小
        meta: { year: 2020, people: 'test' },
        mime: file.type
      }).then(result => {
        stat.status = 3
        const url = result.res.requestUrls[0]
        if (this.fileType === 3) {
          // 视频原样返回url
          let _end = url.indexOf('?')
          if (_end === -1) {
            _end = undefined
          }
          that.savaVideo(
            {
              file: file,
              ossUrl: url.substr(0, _end)
            }, fileIndex);
        } else {
          that.savaVideo(
            {
              file: file,
              ossUrl: this.aliyunOssUrl + result.name
            }, fileIndex);
        }
        if (nextIndex < this.uploadList.length) {
          this.multipartUpload(nextIndex)
        } else {
          this.uploading = false;
        }
      }).catch(error => {
        console.log('error', error)
        if (error.status === 0) {
          stat.status = 5
        } else {
          stat.status = 4
        }
        if (nextIndex < this.uploadList.length) {
          this.multipartUpload(nextIndex)
        } else {
          this.uploading = false;
        }
      })
    },
    // ali-oss续传
    aliResumeUpload(fileIndex) {
      const file = this.uploadList[fileIndex]
      const stat = this.uploadStatus[fileIndex]
      const that = this
      const fileType = file.name.split('.')
      // stat.fileName = this.randomString() + '.' + file.type.substr(file.type.indexOf('/') + 1)
      stat.fileName = this.randomString() + '.' + fileType[fileType.length - 1]
      // object-key可以自定义为文件名（例如file.txt）或目录（例如abc/test/file.txt）的形式，实现将文件上传至当前Bucket或Bucket下的指定目录。
      this.aliResumeClient.multipartUpload(this.directoryPath + stat.fileName, file, {
        progress: function (p, checkpoint) {
          // 断点记录点。浏览器重启后无法直接继续上传，您需要手动触发上传操作。
          that.tempCheckpoint = checkpoint;
          // console.log(checkpoint, p)
          stat.progress = parseInt(p * 100)
          stat.status = 2
        },
        checkpoint: that.tempCheckpoint,
        meta: { year: 2020, people: 'test' },
        mime: file.type
      }).then(result => {
        stat.status = 3
        const url = result.res.requestUrls[0]
        // 视频原样返回url
        let _end = url.indexOf('?')
        if (_end === -1) {
          _end = undefined
        }
        that.savaVideo(
          {
            file: file,
            ossUrl: url.substr(0, _end)
          }, fileIndex);
        this.uploading = false;
      }).catch(error => {
        if (error.status === 0) {
          stat.status = 5
        } else {
          stat.status = 4
        }
      })
    }
  }
}
