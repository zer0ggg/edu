import {
  searchQuestions, questionsList
} from '~/api/question.js'
export default {
  methods: {
    // 问答列表
    getQuestionsList(params, context) {
      let list = []
      let method = questionsList
      if (params.title) {
        method = searchQuestions
      }
      // console.log(method)
      method(params).then((res) => {
        // console.log(res)
        // 如果是加载更多
        if (context) {
          list = this.pageObj.list
          if (context.loadMore) {
            context.loadMore = false
          }
        }
        this.pageObj = res.data
        this.pageObj.list = list.concat(this.pageObj.list)
        this.pageObj.list.forEach((item) => {
          this.$store.commit('CHANGE_TIME', item)
        })
      }).catch(() => {
        if (context && context.loadMore) {
          context.loadMore = false
        }
        this.pageObj = {
          list: [],
          pageCurrent: '',
          pageSize: '',
          totalCount: '',
          totalPage: ''
        }
      })
    },
    // 加载更多
    loadList(context) {
      this.params.pageCurrent++
      this.getQuestionsList(this.params, context)
    }
  }
}
