// import VueCookie from 'vue-cookie'
import Cookies from 'js-cookie'
export default {
  // 获取服务端cookie
  getInServer(req) {
    const serviceCookie = {}
    req && req.headers.cookie && req.headers.cookie.split(';').forEach(function (val) {
      const parts = val.split('=')
      serviceCookie[parts[0].trim()] = (parts[1] || '').trim()
    })
    return serviceCookie
  },
  // 获取客户端cookie
  getInClient(key) {
    const tokenInfo = Cookies.get(key)
    return tokenInfo
  },
  // 获取客户端cookie
  setInClient({ key, val }) {
    Cookies.set(key, val, { expires: 1 })
  },
  // 删除客户端cookie
  delInClient(key) {
    Cookies.remove(key)

    const option = {};
    // 因为总域名可能会有登录token
    option.domain = '.roncoos.com';
    Cookies.remove(key, option)
  }
}
