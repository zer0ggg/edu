import { blogList, searchBlog } from '~/api/blog/community.js'
import { activeBolgList } from '~/api/blog/user.js'
export default {
  methods: {
    // 博客列表
    getBlogList(params, context) {
      let list = []
      let method = blogList
      if (!params.tagsName && !params.sections && !params.bloggerUserNo) {
        method = activeBolgList
      }
      if (params.title) {
        method = searchBlog
      }
      method(params).then((res) => {
        // 如果是加载更多
        if (context) {
          list = this.pageObj.list
          if (context.loadMore) {
            context.loadMore = false
          }
        }
        this.pageObj = res.data
        this.pageObj.list = list.concat(this.pageObj.list)
        this.pageObj.list.forEach((item) => {
          this.$store.commit('CHANGE_TIME', item)
        })
      }).catch(() => {
        if (context && context.loadMore) {
          context.loadMore = false
        }
        this.pageObj = {
          list: [],
          pageCurrent: '',
          pageSize: '',
          totalCount: '',
          totalPage: ''
        }
      })
    },
    // 加载更多
    loadList(context) {
      this.params.pageCurrent++
      this.getBlogList(this.params, context)
    }
  }
}
