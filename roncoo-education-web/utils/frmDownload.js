import cookie from '../utils/cookies'
const tokenName = 'RC_WEB_TOKEN'
/**
 * 导出excel
 * @param url
 * @param data
 * @param method
 */
export function formDownloadFile(url, data, method) {
  let inputs = '<input type="hidden" name="token" value="' + cookie.getInClient(tokenName) + '" />';
  for (const obj in data) {
    inputs += '<input type="hidden" name="' + obj + '" value="' + data[obj] + '" />';
  }
  const $form = document.createElement('form');
  $form.method = method || 'post'
  $form.action = '/apis' + url + '?token=' + cookie.getInClient(tokenName)
  $form.innerHTML = inputs
  document.body.appendChild($form); // 这一行必须，iframe挂在到dom树上才会发请求
  $form.submit();
  $form.remove();
}

export function downloadFile(url) {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none'; // 防止影响页面
  iframe.style.height = 0; // 防止影响页面
  iframe.src = url;
  document.body.appendChild(iframe); // 这一行必须，iframe挂在到dom树上才会发请求
  // 5分钟之后删除（onload方法对于下载链接不起作用，就先抠脚一下吧）
  setTimeout(() => {
    iframe.remove();
  }, 5 * 60 * 1000);
}
