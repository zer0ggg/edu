# roncoo-education-uni-app

#### 安装
```
npm install
```

#### 开发
```
npm run serve
```

#### H5编译
```
npm run build:h5
```

#### docker
```
//构建
docker build -t h5 .
//运行
docker run -d --name education-h5 -p 80:80 h5
```
