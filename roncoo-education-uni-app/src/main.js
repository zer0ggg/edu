import Vue from 'vue'
import App from './App'
import pagesObj from "uni-pages?{\"type\":\"style\"}";
const title = pagesObj.globalStyle.navigationBarTitleText || pagesJson.globalStyle.navigationBarTitleText


Vue.config.productionTip = true

Vue.mixin({
	onLoad() {
		const website = uni.getStorageSync('website')
		const pagesList = getCurrentPages();
		const curPage = pagesList[pagesList.length - 1];
		if (pagesObj.pages[curPage.route]) {
			const d = pagesObj.pages[curPage.route];
			if (d) {
				if (!d.navigationBarTitleText || (title == d.navigationBarTitleText)) {
					uni.setNavigationBarTitle({
						title: website.midTitle
					});
				}
			}
		} else {
			for (let i = 0; i < pagesJson.pages.length; i++) {
				const e = pagesJson.pages[i];
				if (e.path === curPage.route) {
					if (!e.style.navigationBarTitleText || (title == e.style.navigationBarTitleText)) {
						uni.setNavigationBarTitle({
							title: website.midTitle
						});
					}
				}
			}
		}
		// #ifdef  H5
		if (website.ico) {
			const icon = document.getElementById('pageIco');
			if (icon) {
				icon.href = website.ico
			}
		}
		// #endif
	},
	methods: {
		setData: function(obj, callback) {
			let that = this;
			const handleData = (tepData, tepKey, afterKey) => {
				tepKey = tepKey.split('.');
				tepKey.forEach(item => {
					if (tepData[item] === null || tepData[item] === undefined) {
						let reg = /^[0-9]+$/;
						tepData[item] = reg.test(afterKey) ? [] : {};
						tepData = tepData[item];
					} else {
						tepData = tepData[item];
					}
				});
				return tepData;
			};
			const isFn = function(value) {
				return typeof value == 'function' || false;
			};
			Object.keys(obj).forEach(function(key) {
				let val = obj[key];
				key = key.replace(/\]/g, '').replace(/\[/g, '.');
				let front, after;
				let index_after = key.lastIndexOf('.');
				if (index_after != -1) {
					after = key.slice(index_after + 1);
					front = handleData(that, key.slice(0, index_after), after);
				} else {
					after = key;
					front = that;
				}
				if (front.$data && front.$data[after] === undefined) {
					Object.defineProperty(front, after, {
						get() {
							return front.$data[after];
						},
						set(newValue) {
							front.$data[after] = newValue;
							that.$forceUpdate();
						},
						enumerable: true,
						configurable: true
					});
					front[after] = val;
				} else {
					that.$set(front, after, val);
				}
			});
			// this.$forceUpdate();
			isFn(callback) && this.$nextTick(callback);
		}
	}
});

App.mpType = 'app'

const app = new Vue({
	...App
})


app.$mount()
