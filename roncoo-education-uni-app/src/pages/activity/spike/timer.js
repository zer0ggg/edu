module.exports = {
  timeState: function (val) {
    var stateList = ['即将开始', '距结束', '已结束', '明日开始'];
    return stateList[val];
  },
  isNextStyle: function (info) {
    if (info.isNext || info.isSpike === 3) {
      return 'c_yellow';
    }

    return 'c_fff';
  },
  isStart: function (info, actInfo) {
    var style = 'background: rgba(51, 51, 51, 1)';

    if (info.isSpike === 1) {
      style = 'background: rgba(213, 20, 35, 1)';
    } else if (actInfo.seckillTimeBarImg) {
      style = 'background: url(+ actInfo.seckillTimeBarImg +)';
    } else if (actInfo.seckillTimeBarColor) {
      style = 'background:' + actInfo.seckillTimeBarColor;
    }

    return style;
  },
  zoneTitleStyle: function (actInfo) {
    if (actInfo.zoneTitleColor) {
      return 'color:' + actInfo.zoneTitleColor;
    }

    return 'color: #fff';
  },
  actStyle: function (actInfo) {
    var style = 'background: rgba(251, 119, 125, 1)';

    if (actInfo.actBackImg) {
      style = 'background: url(+ actInfo.actBackImg +)';
    } else if (actInfo.actBackColor) {
      style = 'background: ' + actInfo.actBackColor;
    }

    return style;
  }
};