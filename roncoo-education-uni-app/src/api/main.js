import req from "../utils/request";
export const advList = (params = {}) => {
  return req.post('/system/api/adv/list', params);
};

export const zoneList = (params = {}) => {
  return req.post('/system/api/zone/list', params);
};

// 会员列表
export const vipList = (params = {}) => {
  return req.post('/system/api/vip/set/list', params)
}
