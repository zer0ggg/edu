import req from "../utils/request"; // 活动列表

// 首页标签
export const blogTab = (params = {}) => {
  return req.post('/community/api/label/list', params)
}

// 首页博客列表
export const blogList = (params = {}) => {
  return req.post('/community/api/blog/list', params)
}


// 登录后博主查看博客
export const blogView = (params = {}) => {
  return req.post('/community/auth/blog/view', params)
}

// 博客详情
export const blogDetail = (params = {}) => {
  return req.post('/community/api/blog/view', params)
}

// 点赞或收藏博客
export const likeOrColl = (params = {}) => {
  return req.post('/community/auth/blog/user/record/save', params)
}
// 取消点赞或收藏博客
export const delLikeOrColl = (params = {}) => {
  return req.post('/community/auth/blog/user/record/delete', params)
}

// 取消点赞或收藏博客
export const searchBlog = (page, pagesize, params = {}) => {
  return req.post('/community/api/blog/search/list', {
	  pageCurrent: page,
	  pageSize: pagesize,
	  ...params
  })
}

