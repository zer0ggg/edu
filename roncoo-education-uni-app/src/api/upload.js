import createUpload from "../utils/upload"; // 上传图片

export const uploadPic = (filePath, params = {}, cb) => {
  return createUpload('/system/auth/upload/pic', 'picFile', filePath, params, cb);
}; // 上传文档

export const uploadDoc = (params = {}, cb) => {
  return createUpload('/system/auth/upload/doc', params, cb);
}; // 上传视频

export const uploadResVideo = (params = {}, cb) => {
  return createUpload('/course/api/upload/video', params, cb);
};