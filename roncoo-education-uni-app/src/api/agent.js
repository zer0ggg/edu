import req from "../utils/request"; // 生成推荐二维码

export const referralCode = (params = {}) => {
  return req.post('/user/auth/user/ext/referral/code', params);
}; // 我的下级推荐商户

export const merchantList = (params = {}) => {
  return req.post('/agent/auth/agent/list', params);
}; // 检测用户与代理商关系

export const checkAgent = (params = {}) => {
  return req.post('/agent/api/agent/check', params);
}; // 绑定用户与代理商关系

export const bindAgent = (params = {}) => {
  return req.post('/user/api/user/agent/register', params);
}; // 代理收益分页列表

export const agentProfitList = (params = {}) => {
  return req.post('/agent/auth/profit/list', params);
};