import config from "../config/index"; // fileType: picFile|docFile|videoFile

const createHttp = (url, fileType = 'picFile', filePath, params, cb) => {
  const userInfo = uni.getStorageSync('userInfo');
  const data = {
    name: fileType,
    url: config.baseUrl + url,
    filePath: filePath,
    formData: params
  };

  if (!!userInfo) {
    data.header = {
      token: userInfo.token
    };
  }

  return request(data, cb);
};

const request = (params, cb) => {
  var promise = new Promise((resolve, reject) => {
    uni.showLoading({
      title: '正在上传'
    });
    const task = uni.uploadFile({ ...params,
      success: res => {
        uni.hideLoading();
        let result = JSON.parse(res.data);

        if (result.code === 200) {
          resolve(result.data);
        } else {
          reject(result);

          if (!!result.msg) {
            uni.showToast({
              title: result.msg,
              icon: 'none'
            });
          }
        }
      },
      fail: msg => {
        uni.hideLoading();
        uni.showToast({
          title: '上传失败，请稍后重试',
          icon: 'none'
        });
        reject(msg);
      }
    });

    if (cb) {
      cb(task);
    }
  });
  return promise;
};

export default createHttp;