import config from "../config/index";
import { login, logout } from "./auth";
const createHttp = {
  post: (url, params) => {
    const token = uni.getStorageSync('userToken') || '';
    const baseUrl = config.baseUrl
    const data = {
      method: 'POST',
      url: baseUrl + url,
      data: params
    };

    if (token) {
      data.header = {
        token: token
      };
    }
    return request(data);
  },
  put: (url, params) => {
    const token = uni.getStorageSync('userToken') || '';
    const baseUrl = config.baseUrl
    const data = {
      method: 'put',
      url: baseUrl + url,
      data: params
    };

    if (token) {
      data.header = {
        token: token
      };
    }
    return request(data);
  }
};

const request = (params, cb) => {

  var promise = new Promise((resolve, reject) => {
    if (params.data && !params.data.noLoading) {
      uni.showLoading({
        title: '加载中'
      });
    }
	
    uni.request({
      ...params,
      success: res => {
        uni.hideLoading();
        let result = res.data;
        if (result.code === 200) {
          resolve(result.data);
        } else if (result.code >= 300 && result.code <= 400) {
          uni.setStorage({
            'key': 'userInfo',
            'data': null
          });
          // #ifdef MP-WEIXIN
          login();
          return;
          // #endif
		  uni.showToast({
		    title: result.msg,
		    icon: 'none'
		  });
          logout();
		  setTimeout(()=>{
			  login();
		  })
        } else {
          reject(result);
          if (!!result.msg) {
            uni.showToast({
              title: result.msg,
              icon: 'none'
            });
          }
        }
      },
      fail: msg => {
        uni.hideLoading();
        uni.showToast({
          title: '数据加载失败，请稍后重试',
          icon: 'none'
        });
        reject(msg);
      }
    });
  });
  return promise;
};

export default createHttp;
