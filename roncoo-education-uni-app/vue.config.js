//vue.config.js
const TransformPages = require('uni-read-pages')
//默认值
const CONFIG={
    cli:false,      //当前是否为脚手架初始化的项目
    includes:['path','aliasPath','style']	    //需要获取包涵的字段
}
const tfPages = new TransformPages(CONFIG)
module.exports = {
    configureWebpack: {
        plugins: [
            new tfPages.webpack.DefinePlugin({
                ROUTES: JSON.stringify(tfPages.routes),
                pagesJson: JSON.stringify(tfPages.pagesJson),
            })
        ]
    }
}