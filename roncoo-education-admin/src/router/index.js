import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
const asyncRouterMap = []
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: {title: '概况总览', icon: 'dashboard'}
    }]
  },

  {path: '*', redirect: '/404', hidden: true},
  {
    path: '/',
    component: Layout,
    redirect: 'dashboard', // 设置登陆系统默认页面
    children: [
      {path: 'iframe', component: () => import('@/views/iframe/index')},
      {path: 'redirect/:path*', name: 'redirect', component: () => import('@/views/redirect/index')},
      ...asyncRouterMap
    ]
  },
  {
    path: '/user/order/info',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'orderInfoList',
        component: () => import('@/views/user/orderInfo/list'),
        meta: {title: '课程订单'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/orderInfo/add'),
        meta: {title: '添加订单信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/orderInfo/edit'),
        meta: {title: '编辑订单信息'}
      }
    ]
  },
  {
    path: '/user/chosen',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/chosen/list'),
        meta: {title: '课程订单'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/orderInfo/add'),
        meta: {title: '添加订单信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/orderInfo/edit'),
        meta: {title: '编辑订单信息'}
      }
    ]
  },
  {
    path: '/system/zone',
    component: Layout,
    children: [
      {
        path: 'pc/list',
        component: () => import('@/views/system/zone/pc/list'),
        meta: {title: '电脑端专区设置'}
      },
      {
        path: 'app/list',
        component: () => import('@/views/system/zone/app/list'),
        meta: {title: '移动端专区设置'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/zone/add'),
        meta: {title: '添加专区'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/zone/edit'),
        meta: {title: '编辑专区'}
      }
    ]
  },
  {
    path: '/system/zoneRef',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/zoneRef/list'),
        meta: {title: '专区关联'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/zoneRef/add'),
        meta: {title: '添加专区关联'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/zoneRef/edit'),
        meta: {title: '编辑专区关联'}
      }
    ]
  },
  {
    path: '/system/adv',
    component: Layout,
    children: [
      {
        path: 'top/pc/list',
        component: () => import('@/views/system/adv/top/pc/list'),
        meta: {title: '顶部电脑端管理'}
      },
      {
        path: 'top/app/list',
        component: () => import('@/views/system/adv/top/app/list'),
        meta: {title: '顶部App移动端管理'}
      },
      {
        path: 'carousel/pc/list',
        component: () => import('@/views/system/adv/carousel/pc/list'),
        meta: {title: '轮播电脑端管理'}
      },
      {
        path: 'carousel/app/list',
        component: () => import('@/views/system/adv/carousel/app/list'),
        meta: {title: '轮播App移动端管理'}
      },
      {
        path: 'message/pc/list',
        component: () => import('@/views/system/adv/message/pc/list'),
        meta: {title: '资讯轮播电脑端'}
      }
    ]
  },
  {
    path: '/system/nav/bar',
    component: Layout,
    children: [
      {
        path: 'pc/list',
        component: () => import('@/views/system/navBar/pc/list'),
        meta: {title: '头部导航列表'}
      },
      {
        path: 'app/list',
        component: () => import('@/views/system/navBar/app/list'),
        meta: {title: '头部导航列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/navBar/add'),
        meta: {title: '添加头部导航'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/navBar/edit'),
        meta: {title: '编辑头部导航'}
      }
    ]
  },
  {
    path: '/system/website/link',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/websiteLink/list'),
        meta: {title: '站点友情链接列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/websiteLink/add'),
        meta: {title: '添加站点友情链接'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/websiteLink/edit'),
        meta: {title: '编辑站点友情链接'}
      }
    ]
  },
  {
    path: '/system/website/nav',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/websiteNav/list'),
        meta: {title: '站点导航列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/websiteNav/add'),
        meta: {title: '添加站点导航'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/websiteNav/edit'),
        meta: {title: '编辑站点导航'}
      },
      {
        path: 'article',
        component: () => import('@/views/system/websiteNav/article'),
        meta: {title: '编辑站点文章'}
      }
    ]
  },
  {
    path: '/course/course',
    component: Layout,
    children: [
      {
        path: 'record/list',
        component: () => import('@/views/course/course/record/list'),
        meta: {title: '点播课程列表'}
      },
      {
        path: 'record/edit',
        component: () => import('@/views/course/course/record/edit'),
        meta: {title: '点播课程编辑'}
      },
      {
        path: 'record/study/log',
        component: () => import('@/views/course/course/record/studyLog'),
        meta: {title: '学习记录'}
      },
      {
        path: 'live/list',
        component: () => import('@/views/course/course/live/list'),
        meta: {title: '直播课程列表'}
      },
      {
        path: 'live/edit',
        component: () => import('@/views/course/course/live/edit'),
        meta: {title: '直播课程编辑'}
      },
      {
        path: 'resource/list',
        component: () => import('@/views/course/course/resource/list'),
        meta: {title: '文库列表'}
      },
      {
        path: 'resource/edit',
        component: () => import('@/views/course/course/resource/edit'),
        meta: {title: '文库编辑'}
      }
    ]
  },
  {
    path: '/course',
    component: Layout,
    children: [
      {
        path: 'record/study/log',
        component: () => import('@/views/course/course/record/studyLog'),
        meta: {title: '课程学习记录'}
      },
      {
        path: 'record/study/log/period',
        component: () => import('@/views/course/course/record/studyLog/period'),
        meta: {title: '课时学习记录'}
      }
    ]
  },
  {
    path: '/course/course/audit',
    component: Layout,
    children: [
      {
        path: 'record/list',
        component: () => import('@/views/course/courseAudit/record/list'),
        meta: {title: '点播课程审核'}
      },
      {
        path: 'record/add',
        component: () => import('@/views/course/courseAudit/record/add'),
        meta: {title: '点播审核添加'}
      },
      {
        path: 'record/edit',
        component: () => import('@/views/course/courseAudit/record/edit'),
        meta: {title: '点播课程审核编辑'}
      },
      {
        path: 'live/list',
        component: () => import('@/views/course/courseAudit/live/list'),
        meta: {title: '直播课程审核'}
      },
      {
        path: 'live/add',
        component: () => import('@/views/course/courseAudit/live/add'),
        meta: {title: '直播审核添加'}
      },
      {
        path: 'live/edit',
        component: () => import('@/views/course/courseAudit/live/edit'),
        meta: {title: '直播课程审核编辑'}
      },
      {
        path: 'resource/list',
        component: () => import('@/views/course/courseAudit/resource/list'),
        meta: {title: '文库审核'}
      },
      {
        path: 'resource/add',
        component: () => import('@/views/course/courseAudit/resource/add'),
        meta: {title: '文库审核添加'}
      },
      {
        path: 'resource/edit',
        component: () => import('@/views/course/courseAudit/resource/edit'),
        meta: {title: '文库审核编辑'}
      },
      {
        path: 'resource/period/add',
        component: () => import('@/views/course/courseAudit/resource/period/add'),
        meta: {title: '文库审核添加'}
      },
      {
        path: 'resource/period',
        component: () => import('@/views/course/courseAudit/resource/period'),
        meta: {title: '文件管理'}
      }
    ]
  },
  {
    path: '/course/course/category',
    component: Layout,
    children: [
      {
        path: 'home/list',
        component: () => import('@/views/course/courseCategory/home/list'),
        meta: {title: '首页推荐'}
      },
      {
        path: 'record/list',
        component: () => import('@/views/course/courseCategory/record/list'),
        meta: {title: '直播课程分类'}
      },
      {
        path: 'live/list',
        component: () => import('@/views/course/courseCategory/live/list'),
        meta: {title: '点播课程分类'}
      },
      {
        path: 'resource/list',
        component: () => import('@/views/course/courseCategory/resource/list'),
        meta: {title: '文库分类'}
      }
    ]
  },
  {
    path: '/course/course/recommend',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/course/courseRecommend/list'),
        meta: {title: '课程推荐'}
      }
    ]
  },
  {
    path: '/course/course/comment',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/course/course/comment/list'),
        meta: {title: '课程评论列表'}
      }
    ]
  },
  {
    path: '/course/course/resource/recommend',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/course/resourceRecommend/list'),
        meta: {title: '文库推荐'}
      }
    ]
  },
  {
    path: '/exam/exam',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/exam/exam/list'),
        meta: {title: '试卷列表'}
      },
      {
        path: 'edit',
        component: () => import('@/views/exam/exam/edit'),
        meta: {title: '试卷编辑'}
      },
      {
        path: 'audit/list',
        component: () => import('@/views/exam/examAudit/list'),
        meta: {title: '试卷审核列表'}
      },
      {
        path: 'audit/edit',
        component: () => import('@/views/exam/examAudit/edit'),
        meta: {title: '试卷审核编辑'}
      }
    ]
  },
  {
    path: '/exam/problem',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/exam/examProblem/list'),
        meta: {title: '试题列表'}
      },
      {
        path: 'edit',
        component: () => import('@/views/exam/examProblem/edit'),
        meta: {title: '试题编辑'}
      }
    ]
  },
  {
    path: '/exam/recommend',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/exam/examRecommend/list'),
        meta: {title: '试卷推荐列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/exam/examRecommend/add'),
        meta: {title: '试卷推荐添加'}
      },
      {
        path: 'edit',
        component: () => import('@/views/exam/examRecommend/edit'),
        meta: {title: '试卷推荐编辑'}
      }
    ]
  },
  {
    path: '/exam/exam/category',
    component: Layout,
    children: [
      {
        path: 'edit',
        component: () => import('@/views/exam/examCategory/edit'),
        meta: {title: '分类编辑'}
      },
      {
        path: 'add',
        component: () => import('@/views/exam/examCategory/add'),
        meta: {title: '分类添加'}
      },
      {
        path: 'examList/course/list',
        component: () => import('@/views/exam/examCategory/examList/course'),
        meta: {title: '科目'}
      },
      {
        path: 'examList/year/list',
        component: () => import('@/views/exam/examCategory/examList/year'),
        meta: {title: '年份'}
      },
      {
        path: 'examList/source/list',
        component: () => import('@/views/exam/examCategory/examList/source'),
        meta: {title: '来源'}
      },
      {
        path: 'examList/difficulty/list',
        component: () => import('@/views/exam/examCategory/examList/difficulty'),
        meta: {title: '难度'}
      },
      {
        path: 'problemList/course/list',
        component: () => import('@/views/exam/examCategory/problemList/course'),
        meta: {title: '科目'}
      },
      {
        path: 'problemList/year/list',
        component: () => import('@/views/exam/examCategory/problemList/year'),
        meta: {title: '年份'}
      },
      {
        path: 'problemList/source/list',
        component: () => import('@/views/exam/examCategory/problemList/source'),
        meta: {title: '来源'}
      },
      {
        path: 'problemList/difficulty/list',
        component: () => import('@/views/exam/examCategory/problemList/difficulty'),
        meta: {title: '难度'}
      },
      {
        path: 'problemList/top/list',
        component: () => import('@/views/exam/examCategory/problemList/top'),
        meta: {title: '题类'}
      },
      {
        path: 'problemList/emphasis/list',
        component: () => import('@/views/exam/examCategory/problemList/emphasis'),
        meta: {title: '考点'}
      }
    ]
  },
  {
    path: '/exam/exam/user/category',
    component: Layout,
    children: [
      {
        path: 'examList',
        component: () => import('@/views/exam/examUserCategory/examList'),
        meta: {title: '讲师试卷分类列表'}
      },
      {
        path: 'subjectList',
        component: () => import('@/views/exam/examUserCategory/problemList'),
        meta: {title: '讲师试题分类列表'}
      },
      {
        path: 'edit',
        component: () => import('@/views/exam/examUserCategory/edit'),
        meta: {title: '试卷分类编辑'}
      }
    ]
  },
  {
    path: '/exam/grade',
    component: Layout,
    children: [
      {
        path: 'info',
        component: () => import('@/views/exam/grade/info'),
        children: [
          {
            path: 'list',
            component: () => import('@/views/exam/grade/info/list'),
            meta: {title: '班级管理'}
          }
        ]
      }
    ]
  },
  {
    path: '/community/blogger',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/community/blogger/list'),
        meta: {title: '博主信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/community/blogger/add'),
        meta: {title: '添加博主信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/community/blogger/edit'),
        meta: {title: '编辑博主信息'}
      }
    ]
  },
  {
    path: '/community/blog',
    component: Layout,
    children: [
      {
        path: 'blog/list',
        component: () => import('@/views/community/blog/blog/list'),
        meta: {title: '博客信息列表'}
      },
      {
        path: 'blog/edit',
        component: () => import('@/views/community/blog/blog/edit'),
        meta: {title: '博客信息编辑'}
      },
      {
        path: 'message/list',
        component: () => import('@/views/community/blog/message/list'),
        meta: {title: '资讯列表'}
      },
      {
        path: 'message/add',
        component: () => import('@/views/community/blog/message/add'),
        meta: {title: '资讯添加'}
      },
      {
        path: 'message/edit',
        component: () => import('@/views/community/blog/message/edit'),
        meta: {title: '资讯编辑'}
      },
      {
        path: 'comment/list',
        component: () => import('@/views/community/blogComment/list'),
        meta: {title: '评论列表'}
      }
    ]
  },
  {
    path: '/community/questions',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/community/questions/list'),
        meta: {title: '问答信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/community/questions/add'),
        meta: {title: '添加问答信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/community/questions/edit'),
        meta: {title: '编辑问答信息'}
      }
    ]
  },
  {
    path: '/community/questions/comment',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/community/questionsComment/list'),
        meta: {title: '问答评论列表'}
      }
    ]
  },
  {
    path: '/community/article/recommend',
    component: Layout,
    children: [
      {
        path: 'blog/list',
        component: () => import('@/views/community/articleRecommend/blog/list'),
        meta: {title: '文章推荐列表'}
      },
      {
        path: 'message/list',
        component: () => import('@/views/community/articleRecommend/message/list'),
        meta: {title: '文章推荐列表'}
      }
    ]
  },
  {
    path: '/community/label',
    component: Layout,
    children: [
      {
        path: 'blog/list',
        component: () => import('@/views/community/label/blog/list'),
        meta: {title: '博客标签列表'}
      },
      {
        path: 'message/list',
        component: () => import('@/views/community/label/message/list'),
        meta: {title: '资讯标签列表'}
      },
      {
        path: 'questions/list',
        component: () => import('@/views/community/label/questions/list'),
        meta: {title: '问答标签列表'}
      }
    ]
  },
  {
    path: '/community/article/zone/ref',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/community/articleZoneRef/list'),
        meta: {title: '资讯列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/community/articleZoneRef/add'),
        meta: {title: '资讯设置'}
      }
    ]
  },
  {
    path: '/community/article/zone/category',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/community/articleZoneCategory/list'),
        meta: {title: '资讯专区电脑端设置'}
      },
      {
        path: 'add',
        component: () => import('@/views/community/articleZoneCategory/add'),
        meta: {title: '添加文章专区首页分类'}
      },
      {
        path: 'edit',
        component: () => import('@/views/community/articleZoneCategory/edit'),
        meta: {title: '编辑文章专区首页分类'}
      }
    ]
  },
  {
    path: '/user/lecturer',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/lecturer/list'),
        meta: {title: '讲师信息列表'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/lecturer/edit'),
        meta: {title: '编辑讲师信息'}
      }
    ]
  },
  {
    path: '/user/lecturer/audit',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/lecturerAudit/list'),
        meta: {title: '讲师信息-审核列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/lecturerAudit/add'),
        meta: {title: '添加讲师信息-审核'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/lecturerAudit/edit'),
        meta: {title: '编辑讲师信息-审核'}
      }
    ]
  },
  {
    path: '/user/channel',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/channel/list'),
        meta: {title: '频道列表'}
      }
    ]
  },
  {
    path: '/user/user/account/extract/log',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userAccountExtractLog/list'),
        meta: {title: '用户账户提现日志列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userAccountExtractLog/add'),
        meta: {title: '添加用户账户提现日志'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userAccountExtractLog/edit'),
        meta: {title: '编辑用户账户提现日志'}
      }
    ]
  },
  {
    path: '/user/user/ext',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userExt/list'),
        meta: {title: '用户教育信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userExt/add'),
        meta: {title: '添加用户教育信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userExt/edit'),
        meta: {title: '编辑用户教育信息'}
      }
    ]
  },
  {
    path: '/user/user/recommend',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userRecommended/list'),
        meta: {title: '用户推荐'}
      }
    ]
  },
  {
    path: '/system/sharing/template',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/sharingTemplate/list'),
        meta: {title: '分享图片模板列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/sharingTemplate/add'),
        meta: {title: '添加分享图片模板设置'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/sharingTemplate/edit'),
        meta: {title: '编辑分享图片模板设置'}
      }
    ]
  },
  {
    path: '/user/user/lecturer/attention',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userLecturerAttention/list'),
        meta: {title: '用户关注讲师列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userLecturerAttention/add'),
        meta: {title: '添加用户关注讲师'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userLecturerAttention/edit'),
        meta: {title: '编辑用户关注讲师'}
      }
    ]
  },
  {
    path: '/course/user/collection/course',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/course/userCollectionCourse/list'),
        meta: {title: '用户收藏课程列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/course/userCollectionCourse/add'),
        meta: {title: '添加用户收藏课程'}
      },
      {
        path: 'edit',
        component: () => import('@/views/course/userCollectionCourse/edit'),
        meta: {title: '编辑用户收藏课程'}
      }
    ]
  },
  {
    path: '/system/vip/set',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/vipSet/list'),
        meta: {title: '会员设置列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/vipSet/add'),
        meta: {title: '添加会员设置'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/vipSet/edit'),
        meta: {title: '编辑会员设置'}
      }
    ]
  },
  {
    path: '/user/msg',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/msg/list'),
        meta: {title: '站内信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/msg/add'),
        meta: {title: '添加站内信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/msg/edit'),
        meta: {title: '编辑站内信息'}
      }
    ]
  },
  {
    path: '/user/user/msg',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userMsg/list'),
        meta: {title: '站内信用户记录列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userMsg/add'),
        meta: {title: '添加站内信用户记录'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userMsg/edit'),
        meta: {title: '编辑站内信用户记录'}
      }
    ]
  },
  {
    path: '/user/user/log/login',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userLogLogin/list'),
        meta: {title: '用户错误登录日志列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userLogLogin/add'),
        meta: {title: '添加用户错误登录日志'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userLogLogin/edit'),
        meta: {title: '编辑用户错误登录日志'}
      }
    ]
  },
  {
    path: '/user/user/log/sms',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userLogSms/list'),
        meta: {title: '用户发送短信日志列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userLogSms/add'),
        meta: {title: '添加用户发送短信日志'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userLogSms/edit'),
        meta: {title: '编辑用户发送短信日志'}
      }
    ]
  },
  {
    path: '/user/user/order/log',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userOrderLog/list'),
        meta: {title: '用户订单日志信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/userOrderLog/add'),
        meta: {title: '添加用户订单日志信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/userOrderLog/edit'),
        meta: {title: '编辑用户订单日志信息'}
      }
    ]
  },
  {
    path: '/data/order/log',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/orderLog/list'),
        meta: {title: '订单日志列表'}
      }
    ]
  },
  {
    path: '/data/user/log/login',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'userLogList',
        component: () => import('@/views/data/userLog/list'),
        meta: {title: '用户日志列表'}
      }
    ]
  },
  {
    path: '/data/order/stat/courser',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/orderStatCourser/list'),
        meta: {title: '订单课程统计'}
      },
      {
        path: 'summary',
        component: () => import('@/views/data/orderStatCourser/summary'),
        meta: {title: '订单课程统计汇总'}
      }
    ]
  },
  {
    path: '/data/order/stat/lecturer',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/orderStatLecturer/list'),
        meta: {title: '订单讲师统计列表'}
      },
      {
        path: 'summary',
        component: () => import('@/views/data/orderStatLecturer/summary'),
        meta: {title: '订单讲师统计汇总'}
      }
    ]
  },
  {
    path: '/data/sys/log',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/sysLog/list'),
        meta: { title: '后台操作日志' }
      }
    ]
  },
  {
    path: '/data/course/log',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/courseLog/list'),
        meta: {title: '用户学习记录'}
      }
    ]
  },
  {
    path: '/data/course/stat',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/courseStat/list'),
        meta: {title: '课程学习统计'}
      }
    ]
  },
  {
    path: '/data/course/stat/user',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/data/courseStatUser/list'),
        meta: {title: '课程用户统计'}
      }
    ]
  },
  {
    path: '/user/pay/channel',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/payChannel/list'),
        meta: {title: '支付渠道信息列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/payChannel/add'),
        meta: {title: '添加支付渠道信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/payChannel/edit'),
        meta: {title: '编辑支付渠道信息'}
      }
    ]
  },
  {
    path: '/user/pay/rule',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/payRule/list'),
        meta: {title: '支付路由列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/payRule/add'),
        meta: {title: '添加支付路由'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/payRule/edit'),
        meta: {title: '编辑支付路由'}
      }
    ]
  },
  {
    path: '/sys',
    component: Layout,
    children: [
      {
        path: 'user/list',
        component: () => import('@/views/system/sysUser/list'),
        meta: {title: '用户管理'}
      },
      {
        path: 'role/list',
        component: () => import('@/views/system/sysRole/list'),
        meta: {title: '角色管理'}
      },
      {
        path: 'menu/list',
        component: () => import('@/views/system/sysMenu/list'),
        meta: {title: '菜单权限管理'}
      }
    ]
  },
  {
    path: '/system/sys/config',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/sysConfig/list'),
        meta: {title: '参数配置列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/sysConfig/add'),
        meta: {title: '添加参数配置'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/sysConfig/edit'),
        meta: {title: '编辑参数配置'}
      }
    ]
  },
  {
    path: '/system/sys/param/config',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/sysParamConfig/list'),
        meta: {title: '参数配置列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/sysParamConfig/add'),
        meta: {title: '添加参数配置'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/sysParamConfig/edit'),
        meta: {title: '编辑参数配置'}
      }
    ]
  },
  {
    path: '/system/sys/sensitive/word/library',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/sensitiveWordLibrary/list'),
        meta: {title: '参数配置列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/sensitiveWordLibrary/add'),
        meta: {title: '添加参数配置'}
      }
    ]
  },
  {
    path: '/user/msg/template',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/msgTemplate/list'),
        meta: {title: '电脑端专区设置'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/msgTemplate/add'),
        meta: {title: '添加专区'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/msgTemplate/edit'),
        meta: {title: '编辑专区'}
      }
    ]
  },
  {
    path: '/marketing',
    component: Layout,
    children: [
      {
        path: 'seckill/list',
        component: () => import('@/views/marketing/seckill/list'),
        meta: {title: '秒杀'}
      },
      {
        path: 'seckill/course',
        component: () => import('@/views/marketing/seckill/course'),
        meta: {title: '秒杀课程'}
      },
      {
        path: 'coupon/list',
        component: () => import('@/views/marketing/coupon/list'),
        meta: {title: '优惠券'}
      },
      {
        path: 'zone/list',
        component: () => import('@/views/marketing/zone/list'),
        meta: {title: '专区'}
      },
      {
        path: 'course/list',
        component: () => import('@/views/marketing/course/list'),
        meta: {title: '课程'}
      }
    ]
  },
  {
    path: '/system/app/message',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/system/appMessage/list'),
        meta: {title: 'APP推送列表'}
      },
      {
        path: 'add',
        component: () => import('@/views/system/appMessage/add'),
        meta: {title: '添加APP推送'}
      },
      {
        path: 'edit',
        component: () => import('@/views/system/appMessage/edit'),
        meta: {title: '编辑APP推送'}
      }
    ]
  },
  {
    path: '/user/user/research',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/userResearch/list'),
        meta: {title: '调研信息'}
      },
      {
        path: 'view',
        component: () => import('@/views/user/userResearch/view'),
        meta: {title: '调研信息查看'}
      }
    ]
  },
  {
    path: '/user/chosen',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/chosen/list'),
        meta: {title: '课程订单'}
      },
      {
        path: 'add',
        component: () => import('@/views/user/orderInfo/add'),
        meta: {title: '添加订单信息'}
      },
      {
        path: 'edit',
        component: () => import('@/views/user/orderInfo/edit'),
        meta: {title: '编辑订单信息'}
      }
    ]
  },
  {
    path: '/user/distribution',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/distribution/list'),
        meta: {title: '分销管理'}
      }
    ]
  },
  {
    path: '/user/distribution/profit',
    component: Layout,
    children: [
      {
        path: 'list',
        component: () => import('@/views/user/distributionProfit/list'),
        meta: {title: '代理分润记录'}
      }
    ]
  },
  {
    path: '/exam/course/exam/ref',
    component: Layout,
    children: [
      {
        path: 'record/list',
        component: () => import('@/views/exam/courseExamRef/record/list'),
        meta: {title: '随堂练习'}
      },
      {
        path: 'live/list',
        component: () => import('@/views/exam/courseExamRef/live/list'),
        meta: {title: '随堂练习'}
      },
      {
        path: 'resource/list',
        component: () => import('@/views/exam/courseExamRef/resource/list'),
        meta: {title: '随堂练习'}
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
