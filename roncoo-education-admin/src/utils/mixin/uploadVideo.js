import OSS from 'ali-oss'
import { getUploadConfig, getPolyvVideoSign } from '@/api/system.js'
export default {
  data() {
    return {
      polyvClient: undefined, // 保利威SDK实例
      aliClient: undefined, // 阿里上传SDK实例
      aliResumeClient: undefined, // 阿里续传SDK实例
      tempCheckpoint: undefined, // 续传对象
      directoryPath: '',
      aliyunOssUrl: '',
      uploading: false,
      bucket: '',
      uploadStatus: [],
      uploadList: []
    }
  },
  filters: {
    uploadStatus(val) {
      if (val === 2) {
        return '正在上传'
      } else if (val === 3) {
        return '上传成功'
      } else if (val === 4) {
        return '上传失败'
      } else if (val === 5) {
        return '上传暂停'
      } else if (val === 6) {
        return '正在保存'
      } else if (val === 7) {
        return '保存成功'
      } else if (val === 8) {
        return '保存失败'
      } else {
        return '等待上传'
      }
    }
  },
  mounted() {
    this.initOssConfig()
  },
  methods: {
    startUpload(files) {
      for (let i = 0; i < files.length; i++) {
        const file = {
          name: files[i].name,
          status: 1,
          progress: 0 // 上传进度
        }

        this.uploadStatus.push(file);
        this.uploadList = files;
      }
      // 开始上传
      // console.log(this.uploadList)
      this.uploading = true;
      if (this.polyvClient) {
        this.polyvUpload(0)
      } else {
        this.multipartUpload(0)
      }
    },
    randomString(len = 32) {
      const $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
      const maxPos = $chars.length;
      let pwd = '';
      for (let i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
      }
      return pwd;
    },
    // 初始化上传配置
    initOssConfig() {
      getUploadConfig({}).then(res => {
        if (res.code === 200) {
          if (res.data.directory) {
            this.directoryPath = res.data.directory + '/'
            this.aliyunOssUrl = res.data.aliyunOssUrl
          }
          if (this.fileType === 3) {
            this.bucket = res.data.ossBucketVideo
          } else {
            this.bucket = res.data.aliyunOssBucket
          }
            const ossConfig = {
            // region以杭州为例（oss-cn-hangzhou），其他region按实际情况填写。
            region: res.data.endPoint,
            // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
            accessKeyId: res.data.aliyunAccessKeyId,
            accessKeySecret: res.data.aliyunccessKeySecret,
            bucket: this.bucket
          }
          this.aliClient = new OSS(ossConfig);
          this.aliResumeClient = new OSS(ossConfig);
        }
      }).catch(msg => {
        this.$msgBox({
          content: msg.msg
        })
      })
    },
    // 初始化保利威上传sdk
    initPolyv() {
      const PlvVideoUpload = require('@polyv/vod-upload-js-sdk').default;
      this.polyvClient = new PlvVideoUpload({
        events: {
          Error: (err) => { // 错误事件回调
            console.log(err);
          },
          UploadComplete: () => {} // 全部上传任务完成回调
        }
      });
      this.getPolyvVideoSign()
    },
    // 暂停上传
    stopUpload(int = 0) {
      this.uploadList[int].status = 5
      if (this.polyvClient) {
        this.polyvClient.stopAll()
      } else {
        this.aliClient.cancel()
        this.aliResumeClient.cancel()
      }
    },
    // 继续上传
    resumeUpload(int) {
      this.uploadList[int].status = 2
      if (this.polyvClient) {
        this.polyvClient.resumeFile(this.uploadStatus[int].uploadId)
      } else {
        this.aliResumeUpload(int)
      }
    },
    // 获取保利威上传sign
    getPolyvVideoSign() {
      getPolyvVideoSign({}).then(res => {
        this.polyvClient.updateUserData({
          userid: res.data.userid, // Polyv云点播账号的ID
          ptime: res.data.ptime, // 时间戳，注意：系统时间不正确会导致校验失败
          sign: res.data.sign, // 是根据将secretkey和ts按照顺序拼凑起来的字符串进行MD5计算得到的值
          hash: res.data.hash // 是根据将ts和writeToken按照顺序拼凑起来的字符串进行MD5计算得到的值
        });
        setTimeout(() => {
          this.getPolyvVideoSign()
        }, 1000 * 60 * 2)
      })
    },
    // polyv上传
    polyvUpload(fileIndex) {
      const that = this
      const file = that.uploadList[fileIndex]
      const stat = that.uploadStatus[fileIndex]
      const fileSetting = {
        title: undefined, // 标题
        desc: undefined, // 描述
        cataid: undefined, // 上传分类目录ID
        tag: 'course', // 标签
        luping: 1, // 是否开启视频课件优化处理，对于上传录屏类视频清晰度有所优化：0为不开启，1为开启
        keepsource: 0, // 是否源文件播放（不对视频进行编码）：0为编码，1为不编码
        state: undefined // 用户自定义数据，如果提交了该字段，会在视频上传完成事件回调时透传返回。
      }
      const uploader = that.polyvClient.addFile(
        file, // file 为待上传的文件对象
        {
          FileStarted: function (uploadInfo) { // 文件开始上传回调
            console.log('文件上传开始: ' + uploadInfo.fileData.title);
          },
          FileProgress: function (uploadInfo) { // 文件上传过程返回上传进度信息回调
            stat.status = 2
            stat.progress = parseInt(uploadInfo.progress * 100)
            console.log('文件上传中: ' + (uploadInfo.progress * 100).toFixed(2) + '%');
          },
          FileStopped: function (uploadInfo) { // 文件暂停上传回调
            stat.status = 5
            console.log('文件上传停止: ' + uploadInfo.fileData.title);
          },
          FileSucceed: function (uploadInfo) { // 文件上传成功回调
            stat.status = 3
            console.log('文件上传成功: ', uploadInfo.fileData);
            if (that.uploadSuccess) {
              that.uploadSuccess(uploadInfo.fileData, fileIndex)
            }
          },
          FileFailed: function (uploadInfo) { // 文件上传失败回调
            stat.status = 4
            that.uploading = false
            console.log('文件上传失败: ' + uploadInfo.fileData.title);
          }
        },
        fileSetting
      );
      const uploaderid = uploader.id;
      stat.uploadId = uploaderid
      this.polyvClient.resumeFile(uploaderid);
    },
    // ali-oss上传
    multipartUpload(fileIndex) {
      const nextIndex = fileIndex + 1
      const file = this.uploadList[fileIndex]
      const stat = this.uploadStatus[fileIndex]
      const that = this
      const fileType = file.name.split('.')
      // stat.fileName = this.randomString() + '.' + file.type.substr(file.type.indexOf('/') + 1)
      stat.fileName = this.randomString() + '.' + fileType[fileType.length - 1]
      // object-key可以自定义为文件名（例如file.txt）或目录（例如abc/test/file.txt）的形式，实现将文件上传至当前Bucket或Bucket下的指定目录。
      this.aliClient.multipartUpload(this.directoryPath + stat.fileName, file, {
        progress: function (p, checkpoint) {
          // 断点记录点。浏览器重启后无法直接继续上传，您需要手动触发上传操作。
          that.tempCheckpoint = checkpoint;
          stat.progress = parseInt(p * 100)
          stat.status = 2
        },
        // parallel: 5, // 分片数量
        // partSize: 1024 * 1024 * 40, // 分片大小
        meta: { year: 2020, people: 'test' },
        mime: file.type
      }).then(result => {
        stat.status = 3
        const url = result.res.requestUrls[0]
        if (this.fileType === 3) {
          // 视频原样返回url
          let _end = url.indexOf('?')
          if (_end === -1) {
            _end = undefined
          }
          that.savaVideo(
            {
              file: file,
              ossUrl: url.substr(0, _end)
            }, fileIndex);
        } else {
          that.savaVideo(
            {
              file: file,
              ossUrl: this.aliyunOssUrl + result.name
            }, fileIndex);
        }
        if (nextIndex < this.uploadList.length) {
          this.multipartUpload(nextIndex)
        } else {
          this.uploading = false;
        }
      }).catch(error => {
        console.log('error', error)
        if (error.status === 0) {
          stat.status = 5
        } else {
          stat.status = 4
        }
        if (nextIndex < this.uploadList.length) {
          this.multipartUpload(nextIndex)
        } else {
          this.uploading = false;
        }
      })
    },
    // ali-oss续传
    aliResumeUpload(fileIndex) {
      const file = this.uploadList[fileIndex]
      const stat = this.uploadStatus[fileIndex]
      const that = this
      const fileType = file.name.split('.')
      // stat.fileName = this.randomString() + '.' + file.type.substr(file.type.indexOf('/') + 1)
      stat.fileName = this.randomString() + '.' + fileType[fileType.length - 1]
      // object-key可以自定义为文件名（例如file.txt）或目录（例如abc/test/file.txt）的形式，实现将文件上传至当前Bucket或Bucket下的指定目录。
      this.aliResumeClient.multipartUpload(this.directoryPath + stat.fileName, file, {
        progress: function (p, checkpoint) {
          // 断点记录点。浏览器重启后无法直接继续上传，您需要手动触发上传操作。
          that.tempCheckpoint = checkpoint;
          // console.log(checkpoint, p)
          stat.progress = parseInt(p * 100)
          stat.status = 2
        },
        checkpoint: that.tempCheckpoint,
        meta: { year: 2020, people: 'test' },
        mime: file.type
      }).then(result => {
        stat.status = 3
        const url = result.res.requestUrls[0]
        if (this.fileType === 3) {
          // 视频原样返回url
          let _end = url.indexOf('?')
          if (_end === -1) {
            _end = undefined
          }
          that.savaVideo(
            {
              file: file,
              ossUrl: url.substr(0, _end)
            }, fileIndex);
        } else {
          that.savaVideo(
            {
              file: file,
              ossUrl: this.aliyunOssUrl + result.name
            }, fileIndex);
        }
        this.uploading = false;
      }).catch(error => {
        if (error.status === 0) {
          stat.status = 5
        } else {
          stat.status = 4
        }
      })
    }
  }
}
