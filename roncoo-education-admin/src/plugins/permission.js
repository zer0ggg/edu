import permissionData from '@/router/permission.json5'
import store from '../store'
const permissionArray = {}

const checkPermission = function(name) {
  const view = permissionArray[this.$route.path]
  if (store.getters.userPermission.indexOf(view[name]) !== -1) {
    return true
  } else {
    return false
  }
}

export default function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  permissionData.map(item => {
    permissionArray[item.path] = item.rules
  })
  Vue.prototype.checkPermission = checkPermission
}
