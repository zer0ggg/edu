import request from '@/utils/request'

// 课程信息--添加
export function courseSave(data = {}) {
  return request({
    url: '/course/pc/course/save',
    method: 'post',
    data: data
  })
}

// 课程分类--
export function getCategory(data = {}) {
  return request({
    url: '/course/pc/course/category/choose',
    method: 'post',
    data: data
  })
}

// 课程信息--分页
export function coursePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程信息--查找带回
export function courseFindBack(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/find/back',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 课程信息--查看
export function courseView(id) {
  return request.get(`/course/pc/course/view?id=${id}`)
}

// 课程信息--删除
export function courseDelete(id) {
  return request.delete(`/course/pc/course/delete?id=${id}`)
}

// 课程信息--修改
export function courseEdit(data = {}) {
  return request({
    url: '/course/pc/course/edit',
    method: 'put',
    data: data
  })
}

// 课程信息--状态修改
export function courseStatusUpdate(data = {}) {
  return request({
    url: '/course/pc/course/update/status',
    method: 'put',
    data: data
  })
}

// 课程信息--上下架修改
export function courseIsPutawayUpdate(data = {}) {
  return request({
    url: '/course/pc/course/update/putaway',
    method: 'put',
    data: data
  })
}

// 课程信息--一键导入ES
export function courseImportEs(courseCategory) {
  return request({
    url: '/course/pc/course/es/add',
    method: 'post',
    data: {
      courseCategory: courseCategory
    }
  })
}

// 附件信息--添加
export function courseAccessorySave(data = {}) {
  return request({
    url: '/course/pc/course/accessory/save',
    method: 'post',
    data: data
  })
}

// 附件信息--分页
export function courseAccessoryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/accessory/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 附件信息--查看
export function courseAccessoryView(id) {
  return request.get(`/course/pc/course/accessory/view?id=${id}`)
}

// 附件信息--删除
export function courseAccessoryDelete(id) {
  return request.delete(`/course/pc/course/accessory/delete?id=${id}`)
}

// 附件信息--修改
export function courseAccessoryEdit(data = {}) {
  return request({
    url: '/course/pc/course/accessory/edit',
    method: 'put',
    data: data
  })
}

// 课程信息-审核--添加
export function courseAuditSave(data = {}) {
  return request({
    url: '/course/pc/course/audit/save',
    method: 'post',
    data: data
  })
}

// 课程信息-审核--分页
export function courseAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程信息-审核--查看
export function courseAuditView(id) {
  return request.get(`/course/pc/course/audit/view?id=${id}`)
}

// 课程信息-审核--删除
export function courseAuditDelete(id) {
  return request.delete(`/course/pc/course/audit/delete?id=${id}`)
}

// 课程信息-审核--修改
export function courseAuditEdit(data = {}) {
  return request({
    url: '/course/pc/course/audit/edit',
    method: 'put',
    data: data
  })
}

// 课程信息-审核-修改状态
export function courseAuditUpdateStatus(data = {}) {
  return request({
    url: '/course/pc/course/audit/update/status',
    method: 'put',
    data: data
  })
}

// 课程信息-审核-修改状态
export function courseAuditUpdateIsPutaway(data = {}) {
  return request({
    url: '/course/pc/course/audit/update/putaway',
    method: 'put',
    data: data
  })
}

// 课程信息-审核-审核功能
export function courseAuditAudit(data = {}) {
  return request({
    url: '/course/pc/course/audit/audit',
    method: 'put',
    data: data
  })
}

// 课程分类--添加
export function courseCategorySave(data = {}) {
  return request({
    url: '/course/pc/course/category/save',
    method: 'post',
    data: data
  })
}

// 课程分类--分页
export function courseCategoryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/category/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程分类--查看
export function courseCategoryView(id) {
  return request.get(`/course/pc/course/category/view?id=${id}`)
}

// 课程分类--删除
export function courseCategoryDelete(id) {
  return request.delete(`/course/pc/course/category/delete?id=${id}`)
}

// 课程分类--修改
export function courseCategoryEdit(data = {}) {
  return request({
    url: '/course/pc/course/category/edit',
    method: 'put',
    data: data
  })
}

// 课程分类--修改状态
export function courseCategoryUpdateStatus(id, statusId) {
  return request({
    url: '/course/pc/course/category/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 章节信息--添加
export function courseChapterSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/save',
    method: 'post',
    data: data
  })
}

// 章节信息--分页
export function courseChapterPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 章节信息--查看
export function courseChapterView(id) {
  return request.get(`/course/pc/course/chapter/view?id=${id}`)
}

// 章节信息--删除
export function courseChapterDelete(id) {
  return request.delete(`/course/pc/course/chapter/delete?id=${id}`)
}

// 章节信息--修改
export function courseChapterEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/edit',
    method: 'put',
    data: data
  })
}

// 章节信息-审核--添加
export function courseChapterAuditSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/audit/save',
    method: 'post',
    data: data
  })
}

// 章节信息-审核--分页
export function courseChapterAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 章节信息-审核--查看
export function courseChapterAuditView(id) {
  return request.get(`/course/pc/course/chapter/audit/view?id=${id}`)
}

// 章节信息-审核--删除
export function courseChapterAuditDelete(id) {
  return request.delete(`/course/pc/course/chapter/audit/delete?id=${id}`)
}

// 章节信息-审核--修改
export function courseChapterAuditEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/audit/edit',
    method: 'put',
    data: data
  })
}

// 课时信息--添加
export function courseChapterPeriodSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/save',
    method: 'post',
    data: data
  })
}

// 课时信息--分页
export function courseChapterPeriodPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/period/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课时信息--查看
export function courseChapterPeriodView(id) {
  return request.get(`/course/pc/course/chapter/period/view?id=${id}`)
}

// 课时信息--删除
export function courseChapterPeriodDelete(id) {
  return request.delete(`/course/pc/course/chapter/period/delete?id=${id}`)
}

// 课时信息--修改
export function courseChapterPeriodEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/edit',
    method: 'put',
    data: data
  })
}

// 课时信息-审核--添加
export function courseChapterPeriodAuditSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/audit/save',
    method: 'post',
    data: data
  })
}

// 课时信息-审核--分页
export function courseChapterPeriodAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/period/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课时信息-审核--查看
export function courseChapterPeriodAuditView(id) {
  return request.get(`/course/pc/course/chapter/period/audit/view?id=${id}`)
}

// 课时信息-审核--删除
export function courseChapterPeriodAuditDelete(id) {
  return request.delete(`/course/pc/course/chapter/period/audit/delete?id=${id}`)
}

// 课时信息-审核--修改
export function courseChapterPeriodAuditEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/audit/edit',
    method: 'put',
    data: data
  })
}

// 课时图片--添加
export function courseChapterPeriodPicSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/pic/save',
    method: 'post',
    data: data
  })
}

// 课时图片--分页
export function courseChapterPeriodPicPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/period/pic/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课时图片--查看
export function courseChapterPeriodPicView(id) {
  return request.get(`/course/pc/course/chapter/period/pic/view?id=${id}`)
}

// 课时图片--删除
export function courseChapterPeriodPicDelete(id) {
  return request.delete(`/course/pc/course/chapter/period/pic/delete?id=${id}`)
}

// 课时图片--修改
export function courseChapterPeriodPicEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/pic/edit',
    method: 'put',
    data: data
  })
}

// 课时图片审核--添加
export function courseChapterPeriodPicAuditSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/pic/audit/save',
    method: 'post',
    data: data
  })
}

// 课时图片审核--分页
export function courseChapterPeriodPicAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/period/pic/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课时图片审核--查看
export function courseChapterPeriodPicAuditView(id) {
  return request.get(`/course/pc/course/chapter/period/pic/audit/view?id=${id}`)
}

// 课时图片审核--删除
export function courseChapterPeriodPicAuditDelete(id) {
  return request.delete(`/course/pc/course/chapter/period/pic/audit/delete?id=${id}`)
}

// 课时图片审核--修改
export function courseChapterPeriodPicAuditEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/period/pic/audit/edit',
    method: 'put',
    data: data
  })
}

// 章节图片信息--添加
export function courseChapterPicSave(data = {}) {
  return request({
    url: '/course/pc/course/chapter/pic/save',
    method: 'post',
    data: data
  })
}

// 章节图片信息--分页
export function courseChapterPicPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/chapter/pic/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 章节图片信息--查看
export function courseChapterPicView(id) {
  return request.get(`/course/pc/course/chapter/pic/view?id=${id}`)
}

// 章节图片信息--删除
export function courseChapterPicDelete(id) {
  return request.delete(`/course/pc/course/chapter/pic/delete?id=${id}`)
}

// 章节图片信息--修改
export function courseChapterPicEdit(data = {}) {
  return request({
    url: '/course/pc/course/chapter/pic/edit',
    method: 'put',
    data: data
  })
}

// 课程介绍信息--添加
export function courseIntroduceSave(data = {}) {
  return request({
    url: '/course/pc/course/introduce/save',
    method: 'post',
    data: data
  })
}

// 课程介绍信息--分页
export function courseIntroducePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/introduce/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程介绍信息--查看
export function courseIntroduceView(id) {
  return request.get(`/course/pc/course/introduce/view?id=${id}`)
}

// 课程介绍信息--删除
export function courseIntroduceDelete(id) {
  return request.delete(`/course/pc/course/introduce/delete?id=${id}`)
}

// 课程介绍信息--修改
export function courseIntroduceEdit(data = {}) {
  return request({
    url: '/course/pc/course/introduce/edit',
    method: 'put',
    data: data
  })
}

// 课程介绍信息-审核--添加
export function courseIntroduceAuditSave(data = {}) {
  return request({
    url: '/course/pc/course/introduce/audit/save',
    method: 'post',
    data: data
  })
}

// 课程介绍信息-审核--分页
export function courseIntroduceAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/introduce/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程介绍信息-审核--查看
export function courseIntroduceAuditView(id) {
  return request.get(`/course/pc/course/introduce/audit/view?id=${id}`)
}

// 课程介绍信息-审核--删除
export function courseIntroduceAuditDelete(id) {
  return request.delete(`/course/pc/course/introduce/audit/delete?id=${id}`)
}

// 课程介绍信息-审核--修改
export function courseIntroduceAuditEdit(data = {}) {
  return request({
    url: '/course/pc/course/introduce/audit/edit',
    method: 'put',
    data: data
  })
}

// 直播记录--添加
export function courseLiveLogSave(data = {}) {
  return request({
    url: '/course/pc/course/live/log/save',
    method: 'post',
    data: data
  })
}

// 直播记录--分页
export function courseLiveLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/live/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 直播记录--查看
export function courseLiveLogView(id) {
  return request.get(`/course/pc/course/live/log/view?id=${id}`)
}

// 直播记录--删除
export function courseLiveLogDelete(id) {
  return request.delete(`/course/pc/course/live/log/delete?id=${id}`)
}

// 直播记录--修改
export function courseLiveLogEdit(data = {}) {
  return request({
    url: '/course/pc/course/live/log/edit',
    method: 'put',
    data: data
  })
}

// 课程推荐--添加
export function courseRecommendSave(categoryId, courseId) {
  return request({
    url: '/course/pc/course/recommend/save',
    method: 'post',
    data: { categoryId: categoryId, courseId: courseId }
  })
}

// 课程推荐--批量添加
export function courseRecommendSaveBatch(categoryId, courseIds) {
  return request({
    url: '/course/pc/course/recommend/save/batch',
    method: 'post',
    data: { categoryId: categoryId, courseIds: courseIds }
  })
}

// 课程推荐--分页
export function courseRecommendPage(params, categoryId, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/recommend/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, categoryId, ...params }
  })
}

// 课程推荐--分页
export function courseRecommendCoursePage(params, categoryId, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/recommend/courseList',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, categoryId, ...params }
  })
}

// 课程推荐--查看
export function courseRecommendView(id) {
  return request.get(`/course/pc/course/recommend/view?id=${id}`)
}

// 课程推荐--删除
export function courseRecommendDelete(id) {
  return request.delete(`/course/pc/course/recommend/delete?id=${id}`)
}

// 课程推荐--修改
export function courseRecommendEdit(data = {}) {
  return request({
    url: '/course/pc/course/recommend/edit',
    method: 'put',
    data: data
  })
}

// 课程用户关联--添加
export function courseUserStudySave(data = {}) {
  return request({
    url: '/course/pc/course/user/study/save',
    method: 'post',
    data: data
  })
}

// 课程用户关联--分页
export function courseUserStudyPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/user/study/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程用户关联--查看
export function courseUserStudyView(id) {
  return request.get(`/course/pc/course/user/study/view?id=${id}`)
}

// 课程用户关联--删除
export function courseUserStudyDelete(id) {
  return request.delete(`/course/pc/course/user/study/delete?id=${id}`)
}

// 课程用户关联--修改
export function courseUserStudyEdit(data = {}) {
  return request({
    url: '/course/pc/course/user/study/edit',
    method: 'put',
    data: data
  })
}

// 课程用户学习日志--添加
export function courseUserStudyLogSave(data = {}) {
  return request({
    url: '/course/pc/course/user/study/log/save',
    method: 'post',
    data: data
  })
}

// 课程用户学习日志--分页
export function courseUserStudyLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/user/study/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程用户学习日志--查看
export function courseUserStudyLogView(id) {
  return request.get(`/course/pc/course/user/study/log/view?id=${id}`)
}

// 课程用户学习日志--删除
export function courseUserStudyLogDelete(id) {
  return request.delete(`/course/pc/course/user/study/log/delete?id=${id}`)
}

// 课程用户学习日志--修改
export function courseUserStudyLogEdit(data = {}) {
  return request({
    url: '/course/pc/course/user/study/log/edit',
    method: 'put',
    data: data
  })
}

// 课程视频信息--添加
export function courseVideoSave(data = {}) {
  return request({
    url: '/course/pc/course/video/save',
    method: 'post',
    data: data
  })
}

// 课程视频信息--分页
export function courseVideoPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/course/video/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程视频信息--查看
export function courseVideoView(id) {
  return request.get(`/course/pc/course/video/view?id=${id}`)
}

// 课程视频信息--删除
export function courseVideoDelete(id) {
  return request.delete(`/course/pc/course/video/delete?id=${id}`)
}

// 课程视频信息--修改
export function courseVideoEdit(data = {}) {
  return request({
    url: '/course/pc/course/video/edit',
    method: 'put',
    data: data
  })
}

// 定时任务计划--添加
export function crontabPlanSave(data = {}) {
  return request({
    url: '/course/pc/crontab/plan/save',
    method: 'post',
    data: data
  })
}

// 定时任务计划--分页
export function crontabPlanPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/crontab/plan/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 定时任务计划--查看
export function crontabPlanView(id) {
  return request.get(`/course/pc/crontab/plan/view?id=${id}`)
}

// 定时任务计划--删除
export function crontabPlanDelete(id) {
  return request.delete(`/course/pc/crontab/plan/delete?id=${id}`)
}

// 定时任务计划--修改
export function crontabPlanEdit(data = {}) {
  return request({
    url: '/course/pc/crontab/plan/edit',
    method: 'put',
    data: data
  })
}

// 直播转点存定时任务--添加
export function crontabPlanPolyvSave(data = {}) {
  return request({
    url: '/course/pc/crontab/plan/polyv/save',
    method: 'post',
    data: data
  })
}

// 直播转点存定时任务--分页
export function crontabPlanPolyvPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/crontab/plan/polyv/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 直播转点存定时任务--查看
export function crontabPlanPolyvView(id) {
  return request.get(`/course/pc/crontab/plan/polyv/view?id=${id}`)
}

// 直播转点存定时任务--删除
export function crontabPlanPolyvDelete(id) {
  return request.delete(`/course/pc/crontab/plan/polyv/delete?id=${id}`)
}

// 直播转点存定时任务--修改
export function crontabPlanPolyvEdit(data = {}) {
  return request({
    url: '/course/pc/crontab/plan/polyv/edit',
    method: 'put',
    data: data
  })
}

// 文件信息--添加
export function fileInfoSave(data = {}) {
  return request({
    url: '/course/pc/file/info/save',
    method: 'post',
    data: data
  })
}

// 文件信息--分页
export function fileInfoPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/file/info/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 文件信息--查看
export function fileInfoView(id) {
  return request.get(`/course/pc/file/info/view?id=${id}`)
}

// 文件信息--删除
export function fileInfoDelete(id) {
  return request.delete(`/course/pc/file/info/delete?id=${id}`)
}

// 文件信息--修改
export function fileInfoEdit(data = {}) {
  return request({
    url: '/course/pc/file/info/edit',
    method: 'put',
    data: data
  })
}

// 文库推荐--添加
export function resourceRecommendSave(data = {}) {
  return request({
    url: '/course/pc/resource/recommend/save',
    method: 'post',
    data: data
  })
}

// 文库推荐--批量添加
export function resourceRecommendSaveBatch(data = {}) {
  return request({
    url: '/course/pc/resource/recommend/save/batch',
    method: 'post',
    data: data
  })
}

// 文库推荐--分页
export function resourceRecommendPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/resource/recommend/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 文库推荐--查看
export function resourceRecommendView(id) {
  return request.get(`/course/pc/resource/recommend/view?id=${id}`)
}

// 文库推荐--删除
export function resourceRecommendDelete(id) {
  return request.delete(`/course/pc/resource/recommend/delete?id=${id}`)
}

// 文库推荐--修改
export function resourceRecommendEdit(data = {}) {
  return request({
    url: '/course/pc/resource/recommend/edit',
    method: 'put',
    data: data
  })
}

// 文库推荐--状态修改
export function resourceRecommendUpdateStatus(id, statusId) {
  return request({
    url: '/course/pc/resource/recommend/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// --添加
export function undoLogSave(data = {}) {
  return request({
    url: '/course/pc/undo/log/save',
    method: 'post',
    data: data
  })
}

// --分页
export function undoLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/undo/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// --查看
export function undoLogView(id) {
  return request.get(`/course/pc/undo/log/view?id=${id}`)
}

// --删除
export function undoLogDelete(id) {
  return request.delete(`/course/pc/undo/log/delete?id=${id}`)
}

// --修改
export function undoLogEdit(data = {}) {
  return request({
    url: '/course/pc/undo/log/edit',
    method: 'put',
    data: data
  })
}

// 用户收藏课程--添加
export function userCollectionCourseSave(data = {}) {
  return request({
    url: '/course/pc/user/collection/course/save',
    method: 'post',
    data: data
  })
}

// 用户收藏课程--分页
export function userCollectionCoursePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/user/collection/course/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 用户收藏课程--查看
export function userCollectionCourseView(id) {
  return request.get(`/course/pc/user/collection/course/view?id=${id}`)
}

// 用户收藏课程--删除
export function userCollectionCourseDelete(id) {
  return request.delete(`/course/pc/user/collection/course/delete?id=${id}`)
}

// 用户收藏课程--修改
export function userCollectionCourseEdit(data = {}) {
  return request({
    url: '/course/pc/user/collection/course/edit',
    method: 'put',
    data: data
  })
}

// 用户订单课程关联--添加
export function userOrderCourseRefSave(data = {}) {
  return request({
    url: '/course/pc/user/order/course/ref/save',
    method: 'post',
    data: data
  })
}

// 用户订单课程关联--分页
export function userOrderCourseRefPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/user/order/course/ref/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 用户订单课程关联--查看
export function userOrderCourseRefView(id) {
  return request.get(`/course/pc/user/order/course/ref/view?id=${id}`)
}

// 用户订单课程关联--删除
export function userOrderCourseRefDelete(id) {
  return request.delete(`/course/pc/user/order/course/ref/delete?id=${id}`)
}

// 用户订单课程关联--修改
export function userOrderCourseRefEdit(data = {}) {
  return request({
    url: '/course/pc/user/order/course/ref/edit',
    method: 'put',
    data: data
  })
}

// 专区--添加
export function zoneSave(data = {}) {
  return request({
    url: '/course/pc/zone/save',
    method: 'post',
    data: data
  })
}

// 专区--分页
export function zonePage(params, zoneLocation, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/zone/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, zoneLocation: zoneLocation, ...params }
  })
}

// 专区--查看
export function zoneView(id) {
  return request.get(`/course/pc/zone/view?id=${id}`)
}

// 专区--删除
export function zoneDelete(id) {
  return request.delete(`/course/pc/zone/delete?id=${id}`)
}

// 专区--修改
export function zoneEdit(data = {}) {
  return request({
    url: '/course/pc/zone/edit',
    method: 'put',
    data: data
  })
}

// 专区课程关联--添加
export function zoneCourseSave(zoneId, courseId) {
  return request({
    url: '/course/pc/zone/course/save',
    method: 'post',
    data: { zoneId: zoneId, courseId: courseId }
  })
}

// 专区课程关联--分页
export function zoneCoursePage(params, zoneId, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/zone/course/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, zoneId, ...params }
  })
}

// 专区课程未关联--分页
export function zoneCourseRefPage(params, zoneId, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/course/pc/zone/course/unref/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, zoneId, ...params }
  })
}

// 专区课程关联--查看
export function zoneCourseView(id) {
  return request.get(`/course/pc/zone/course/view?id=${id}`)
}

// 专区课程关联--删除
export function zoneCourseDelete(id) {
  return request.delete(`/course/pc/zone/course/delete?id=${id}`)
}

// 专区课程关联--修改
export function zoneCourseEdit(data = {}) {
  return request({
    url: '/course/pc/zone/course/edit',
    method: 'put',
    data: data
  })
}

// 用户学习记录--分页
export function studyLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
      url: '/course/pc/course/user/study/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}
// 用户学习记录--分页
export function studyLogPeriodPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/course/log/period/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 订单讲师统计
export function courseUserStudyLogStatistical(data = {}) {
  return request({
    url: '/course/pc/course/user/study/log/statistical',
    method: 'post',
    data: data
  })
}

// 课程评论列表
export function courseCommentList(data = {}) {
  return request({
    url: '/course/pc/course/comment/list',
    method: 'post',
    data: data
  })
}

// 添加课程评论列表
export function AddCourseCommentList(data = {}) {
  return request({
    url: '/course/pc/course/comment/save',
    method: 'post',
    data: data
  })
}

// 修改课程评论列表
export function editCourseCommentList(data = {}) {
  return request({
    url: '/course/pc/course/comment/edit',
    method: 'put',
    data: data
  })
}

// 修改课程评论列表
export function deleteCourseComment(data = {}) {
  return request({
    url: '/course/pc/course/comment/delete?id=' + data.id,
    method: 'DELETE'
  })
}
