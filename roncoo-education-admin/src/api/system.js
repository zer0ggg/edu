import request from '@/utils/request'

// 上传图片
export function uploadPic(data = {}) {
  return request({
    url: `/course/api/upload/pic`,
    method: 'post',
    data: data
  })
}

// 广告信息--添加
export function advSave(data = {}) {
  return request({
    url: '/system/pc/adv/save',
    method: 'post',
    data: data
  })
}

// 广告信息--分页
export function advPage(params, platShow, advLocation, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/adv/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, platShow: platShow, advLocation: advLocation, ...params }
  })
}

// 广告信息--查看
export function advView(id) {
  return request.get(`/system/pc/adv/view?id=${id}`)
}

// 广告信息--删除
export function advDelete(id) {
  return request.delete(`/system/pc/adv/delete?id=${id}`)
}

// 广告信息--清空缓存
export function advDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/adv/delete/redis',
    method: 'put',
    data: data
  })
}

// 广告信息--修改
export function advEdit(data = {}) {
  return request({
    url: '/system/pc/adv/edit',
    method: 'put',
    data: data
  })
}

// 广告信息--修改
export function advUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/adv/update/status',
    method: 'put',
    data: data
  })
}

// 头部导航--添加
export function navBarSave(data = {}) {
  return request({
    url: '/system/pc/nav/bar/save',
    method: 'post',
    data: data
  })
}

// 头部导航--分页
export function navBarPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/nav/bar/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 头部导航--查看
export function navBarView(id) {
  return request.get(`/system/pc/nav/bar/view?id=${id}`)
}

// 头部导航--删除
export function navBarDelete(id) {
  return request.delete(`/system/pc/nav/bar/delete?id=${id}`)
}

// 头部导航--修改
export function navBarEdit(data = {}) {
  return request({
    url: '/system/pc/nav/bar/edit',
    method: 'put',
    data: data
  })
}

// 头部导航--清空缓存
export function navBarDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/nav/bar/delete/redis',
    method: 'put',
    data: data
  })
}

// 平台信息--添加
export function platformSave(data = {}) {
  return request({
    url: '/system/pc/platform/save',
    method: 'post',
    data: data
  })
}

// 平台信息--分页
export function platformPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/platform/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 平台信息--查看
export function platformView(id) {
  return request.get(`/system/pc/platform/view?id=${id}`)
}

// 平台信息--删除
export function platformDelete(id) {
  return request.delete(`/system/pc/platform/delete?id=${id}`)
}

// 平台信息--修改
export function platformEdit(data = {}) {
  return request({
    url: '/system/pc/platform/edit',
    method: 'put',
    data: data
  })
}

// 行政区域--添加
export function regionSave(data = {}) {
  return request({
    url: '/system/pc/region/save',
    method: 'post',
    data: data
  })
}

// 行政区域--分页
export function regionPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/region/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 行政区域--查看
export function regionView(id) {
  return request.get(`/system/pc/region/view?id=${id}`)
}

// 行政区域--删除
export function regionDelete(id) {
  return request.delete(`/system/pc/region/delete?id=${id}`)
}

// 行政区域--修改
export function regionEdit(data = {}) {
  return request({
    url: '/system/pc/region/edit',
    method: 'put',
    data: data
  })
}

// 系统配置--添加
export function sysSave(data = {}) {
  return request({
    url: '/system/pc/sys/save',
    method: 'post',
    data: data
  })
}

// 系统配置--分页
export function sysPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统配置--查看
export function sysView() {
  return request.get(`/system/pc/sys/view`)
}

// 系统配置--删除
export function sysDelete(id) {
  return request.delete(`/system/pc/sys/delete?id=${id}`)
}

// 系统配置--修改
export function sysEdit(data = {}) {
  return request({
    url: '/system/pc/sys/edit',
    method: 'put',
    data: data
  })
}

// --添加
export function undoLogSave(data = {}) {
  return request({
    url: '/system/pc/undo/log/save',
    method: 'post',
    data: data
  })
}

// --分页
export function undoLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/undo/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// --查看
export function undoLogView(id) {
  return request.get(`/system/pc/undo/log/view?id=${id}`)
}

// --删除
export function undoLogDelete(id) {
  return request.delete(`/system/pc/undo/log/delete?id=${id}`)
}

// --修改
export function undoLogEdit(data = {}) {
  return request({
    url: '/system/pc/undo/log/edit',
    method: 'put',
    data: data
  })
}

// 会员设置--添加
export function vipSetSave(data = {}) {
  return request({
    url: '/system/pc/vip/set/save',
    method: 'post',
    data: data
  })
}

// 会员设置--分页
export function vipSetPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/vip/set/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 会员设置--查看
export function vipSetView(id) {
  return request.get(`/system/pc/vip/set/view?id=${id}`)
}

// 会员设置--删除
export function vipSetDelete(id) {
  return request.delete(`/system/pc/vip/set/delete?id=${id}`)
}

// 会员设置--修改
export function vipSetEdit(data = {}) {
  return request({
    url: '/system/pc/vip/set/edit',
    method: 'put',
    data: data
  })
}

// 会员设置状态--修改
export function vipSetUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/vip/set/update/status',
    method: 'put',
    data: data
  })
}

// 站点信息--添加
export function websiteSave(data = {}) {
  return request({
    url: '/system/pc/website/save',
    method: 'post',
    data: data
  })
}

// 站点信息--分页
export function websitePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/website/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 站点信息--查看
export function websiteView() {
  return request.get(`/system/pc/website/view`)
}

// 站点信息--删除
export function websiteDelete(id) {
  return request.delete(`/system/pc/website/delete?id=${id}`)
}

// 站点信息--修改
export function websiteEdit(data = {}) {
  return request({
    url: '/system/pc/website/edit',
    method: 'put',
    data: data
  })
}

// 站点友情链接--添加
export function websiteLinkSave(data = {}) {
  return request({
    url: '/system/pc/website/link/save',
    method: 'post',
    data: data
  })
}

// 站点友情链接--分页
export function websiteLinkPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/website/link/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 站点友情链接--查看
export function websiteLinkView(id) {
  return request.get(`/system/pc/website/link/view?id=${id}`)
}

// 站点友情链接--删除
export function websiteLinkDelete(id) {
  return request.delete(`/system/pc/website/link/delete?id=${id}`)
}

// 站点友情链接--修改
export function websiteLinkEdit(data = {}) {
  return request({
    url: '/system/pc/website/link/edit',
    method: 'put',
    data: data
  })
}

// 站点友情链接--清空缓存
export function websiteLinkDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/website/link/delete/redis',
    method: 'put',
    data: data
  })
}

// 站点导航--添加
export function websiteNavSave(data = {}) {
  return request({
    url: '/system/pc/website/nav/save',
    method: 'post',
    data: data
  })
}

// 站点导航--分页
export function websiteNavPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/website/nav/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 站点导航--查看
export function websiteNavView(id) {
  return request.get(`/system/pc/website/nav/view?id=${id}`)
}

// 站点导航--删除
export function websiteNavDelete(id) {
  return request.delete(`/system/pc/website/nav/delete?id=${id}`)
}

// 站点导航--修改
export function websiteNavEdit(data = {}) {
  return request({
    url: '/system/pc/website/nav/edit',
    method: 'put',
    data: data
  })
}

// 站点导航--修改
export function websiteNavDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/website/nav/delete/redis',
    method: 'put',
    data: data
  })
}

// 站点导航文章--添加
export function websiteNavArticleSave(data = {}) {
  return request({
    url: '/system/pc/website/nav/article/save',
    method: 'post',
    data: data
  })
}

// 站点导航文章--分页
export function websiteNavArticlePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/website/nav/article/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 站点导航文章--查看
export function websiteNavArticleView(id) {
  return request.get(`/system/pc/website/nav/article/view?id=${id}`)
}

// 站点导航文章--通过导航id查看
export function websiteNavArticleViewByNavId(id) {
  return request.get(`/system/pc/website/nav/article/view/navId?id=${id}`)
}

// 站点导航文章--删除
export function websiteNavArticleDelete(id) {
  return request.delete(`/system/pc/website/nav/article/delete?id=${id}`)
}

// 站点导航文章--修改
export function websiteNavArticleEdit(data = {}) {
  return request({
    url: '/system/pc/website/nav/article/edit',
    method: 'put',
    data: data
  })
}

// 站点导航文章--修改或保存
export function updateOrSaveByNavId(data = {}) {
  return request({
    url: '/system/pc/website/nav/article/update/navId',
    method: 'post',
    data: data
  })
}

// 参数配置--添加
export function sysConfigSave(data = {}) {
  return request({
    url: '/system/pc/sys/config/save',
    method: 'post',
    data: data
  })
}

// 参数配置--分页
export function sysConfigPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/config/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 参数配置--查看
export function sysConfigView(id) {
  return request.get(`/system/pc/sys/config/view?id=${id}`)
}

// 参数配置--删除
export function sysConfigDelete(id) {
  return request.delete(`/system/pc/sys/config/delete?id=${id}`)
}

// 参数配置--修改
export function sysConfigEdit(data = {}) {
  return request({
    url: '/system/pc/sys/config/edit',
    method: 'put',
    data: data
  })
}
// 参数配置--清空缓存
export function sysConfigDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/sys/config/delete/redis',
    method: 'put',
    data: data
  })
}

// 参数配置状态--修改
export function sysConfigUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/sys/config/update/status',
    method: 'put',
    data: data
  })
}

// 系统菜单--添加
export function sysMenuSave(data = {}) {
  return request({
    url: '/system/pc/sys/menu/save',
    method: 'post',
    data: data
  })
}

// 系统菜单--分页
export function sysMenuPermission(id = 0) {
  return request({
    url: `/system/pc/sys/menu/list?parentId=${id}`,
    method: 'get'
  })
}

// 系统菜单--查看
export function sysMenuView(id) {
  return request.get(`/system/pc/sys/menu/view?id=${id}`)
}

// 系统可用菜单--列出
export function sysMenuAvailableList() {
  return request.get(`/system/pc/sys/menu/available/list`)
}

// 系统菜单--删除
export function sysMenuDelete(id) {
  return request.delete(`/system/pc/sys/menu/delete?id=${id}`)
}

// 系统菜单--修改
export function sysMenuEdit(data = {}) {
  return request({
    url: '/system/pc/sys/menu/edit',
    method: 'put',
    data: data
  })
}

// 系统菜单--状态修改
export function sysMenuUpdateStatus(id, statusId) {
  return request({
    url: '/system/pc/sys/menu/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 系统权限--添加
export function sysPermissionSave(data = {}) {
  return request({
    url: '/system/pc/sys/permission/save',
    method: 'post',
    data: data
  })
}

// 系统权限--分页
export function sysPermissionPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/permission/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统权限--查看
export function sysPermissionView(id) {
  return request.get(`/system/pc/sys/permission/view?id=${id}`)
}

// 系统权限--删除
export function sysPermissionDelete(id) {
  return request.delete(`/system/pc/sys/permission/delete?id=${id}`)
}

// 系统权限--修改
export function sysPermissionEdit(data = {}) {
  return request({
    url: '/system/pc/sys/permission/edit',
    method: 'put',
    data: data
  })
}

// 系统权限--状态修改
export function sysPermissionUpdateStatus(id, statusId) {
  return request({
    url: '/system/pc/sys/permission/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 系统角色--添加
export function sysRoleSave(data = {}) {
  return request({
    url: '/system/pc/sys/role/save',
    method: 'post',
    data: data
  })
}

// 系统角色已分配权限--获取
export function sysRoleListSelectMenuAndPermission(id) {
  return request.get(`/system/pc/sys/role/select?id=${id}`);
}

// 系统角色系统角色菜单和权限--分配
export function sysRoleAllocationMenuAndPermission(data = {}) {
  return request({
    url: '/system/pc/sys/role/allocation',
    method: 'post',
    data: data
  });
}

// 系统角色--分页
export function sysRolePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/role/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统角色--查看
export function sysRoleView(id) {
  return request.get(`/system/pc/sys/role/view?id=${id}`)
}

// 系统角色--删除
export function sysRoleDelete(id) {
  return request.delete(`/system/pc/sys/role/delete?id=${id}`)
}

// 系统角色--修改
export function sysRoleEdit(data = {}) {
  return request({
    url: '/system/pc/sys/role/edit',
    method: 'put',
    data: data
  })
}

// 系统角色--状态修改
export function sysRoleUpdateStatus(id, statusId) {
  return request({
    url: '/system/pc/sys/role/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 系统角色菜单--添加
export function sysRoleMenuSave(data = {}) {
  return request({
    url: '/system/pc/sys/role/menu/save',
    method: 'post',
    data: data
  })
}

// 系统角色菜单--分页
export function sysRoleMenuPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/role/menu/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统角色菜单--查看
export function sysRoleMenuView(id) {
  return request.get(`/system/pc/sys/role/menu/view?id=${id}`)
}

// 系统角色菜单--删除
export function sysRoleMenuDelete(id) {
  return request.delete(`/system/pc/sys/role/menu/delete?id=${id}`)
}

// 系统角色菜单--修改
export function sysRoleMenuEdit(data = {}) {
  return request({
    url: '/system/pc/sys/role/menu/edit',
    method: 'put',
    data: data
  })
}

// 系统角色权限--添加
export function sysRolePermissionSave(data = {}) {
  return request({
    url: '/system/pc/sys/role/permission/save',
    method: 'post',
    data: data
  })
}

// 系统角色权限--分页
export function sysRolePermissionPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/role/permission/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统角色权限--查看
export function sysRolePermissionView(id) {
  return request.get(`/system/pc/sys/role/permission/view?id=${id}`)
}

// 系统角色权限--删除
export function sysRolePermissionDelete(id) {
  return request.delete(`/system/pc/sys/role/permission/delete?id=${id}`)
}

// 系统角色权限--修改
export function sysRolePermissionEdit(data = {}) {
  return request({
    url: '/system/pc/sys/role/permission/edit',
    method: 'put',
    data: data
  })
}

// 系统用户--添加
export function sysUserSave(data = {}) {
  return request({
    url: '/system/pc/sys/user/save',
    method: 'post',
    data: data
  })
}

// 系统用户--分配角色
export function sysUserAllocationRole(data = {}) {
  return request({
    url: '/system/pc/sys/user/allocation',
    method: 'post',
    data: data
  })
}

// 系统用户--查看已分配角色
export function sysUserListSelectRole(id) {
  return request.get(`/system/pc/sys/user/select/role/list?id=${id}`)
}

// 系统用户--分页
export function sysUserPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/user/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统用户--查看
export function sysUserView(id) {
  return request.get(`/system/pc/sys/user/view?id=${id}`)
}

// 系统用户--删除
export function sysUserDelete(id) {
  return request.delete(`/system/pc/sys/user/delete?id=${id}`)
}

// 系统用户--修改
export function sysUserEdit(data = {}) {
  return request({
    url: '/system/pc/sys/user/edit',
    method: 'put',
    data: data
  })
}

// 系统用户--修改
export function sysUserMenu(data = {}) {
  return request({
    url: '/system/pc/sys/user/menu',
    method: 'post',
    data: data
  })
}

// 系统用户状态--修改
export function sysUserUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/sys/user/update/status',
    method: 'put',
    data: data
  })
}

// 系统用户密码--修改
export function sysUserUpdatePassword(data = {}) {
  return request({
    url: '/system/pc/sys/user/update/password',
    method: 'put',
    data: data
  })
}

// 当前用户密码--修改
export function sysUserUpdatePasswordCurrent(data = {}) {
  return request({
    url: '/system/pc/sys/user/update/password/current',
    method: 'put',
    data: data
  })
}

// 获取权限方法DEMO, 没有传参想从TOKEN直接取值转发到Controller通过实体接收，需要传空参数
// export function sysUserGetMenu() {
//   return request({
//     url: `/system/pc/sys/user/permission`,
//     method: 'post',
//     data: {}
//   })
// }

// 系统用户角色--添加
export function sysUserRoleSave(data = {}) {
  return request({
    url: '/system/pc/sys/user/role/save',
    method: 'post',
    data: data
  })
}

// 系统用户角色--分页
export function sysUserRolePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sys/user/role/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 系统用户角色--查看
export function sysUserRoleView(id) {
  return request.get(`/system/pc/sys/user/role/view?id=${id}`)
}

// 系统用户角色--删除
export function sysUserRoleDelete(id) {
  return request.delete(`/system/pc/sys/user/role/delete?id=${id}`)
}

// 系统用户角色--修改
export function sysUserRoleEdit(data = {}) {
  return request({
    url: '/system/pc/sys/user/role/edit',
    method: 'put',
    data: data
  })
}

// 获取枚举
export function enumList(enumName) {
  return request({
    url: `/system/pc/api/enum/list/${enumName}`,
    method: 'get'
  })
}

// 专区表
// 专区--添加
export function zoneSave(data = {}) {
  return request({
    url: '/system/pc/zone/save',
    method: 'post',
    data: data
  })
}

// 专区--分页
export function zonePage(params, zoneLocation, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/zone/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, zoneLocation: zoneLocation, ...params }
  })
}

// 专区--查看
export function zoneView(id) {
  return request.get(`/system/pc/zone/view?id=${id}`)
}

// 专区--删除
export function zoneDelete(id) {
  return request.delete(`/system/pc/zone/delete?id=${id}`)
}

// 专区--清空缓存
export function zoneDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/zone/delete/redis',
    method: 'put',
    data: data
  })
}

// 专区--修改
export function zoneEdit(data = {}) {
  return request({
    url: '/system/pc/zone/edit',
    method: 'put',
    data: data
  })
}

// 专区--修改状态
export function zoneUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/zone/update/status',
    method: 'put',
    data: data
  })
}

// 专区关联
// 专区关联--分页
export function zoneRefPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/zone/ref/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 专区关联--添加
export function zoneRefSave(data = {}) {
  return request({
    url: '/system/pc/zone/ref/save',
    method: 'post',
    data: data
  })
}

// 专区关联--批量添加
export function zoneRefSaveBatch(data = {}) {
  return request({
    url: '/system/pc/zone/ref/save/batch',
    method: 'post',
    data: data
  })
}

// 专区关联--删除
export function zoneRefDelete(id) {
  return request.delete(`/system/pc/zone/ref/delete?id=${id}`)
}

// 专区关联--修改
export function zoneRefEdit(data = {}) {
  return request({
    url: '/system/pc/zone/ref/edit',
    method: 'put',
    data: data
  })
}

// 专区关联--修改
export function zoneRefUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/zone/ref/update/status',
    method: 'put',
    data: data
  })
}

// 专区关联--查看
export function zoneRefView(id) {
  return request.get(`/system/pc/zone/ref/view/${id}`)
}

// APP推送--分页
export function appMessagePage(params, zoneLocation, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/app/message/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, zoneLocation: zoneLocation, ...params }
  })
}

// APP推送--查看
export function appMessageView(id) {
  return request.get(`/system/pc/app/message/view?id=${id}`)
}

// APP推送--删除
export function appMessageDelete(id) {
  return request.delete(`/system/pc/app/message/delete?id=${id}`)
}

// APP推送--添加
export function appMessageSave(data = {}) {
  return request({
    url: '/system/pc/app/message/save',
    method: 'post',
    data: data
  })
}

// APP推送--发送
export function appMessageSend(data = {}) {
  return request({
    url: '/system/pc/app/message/send',
    method: 'post',
    data: data
  })
}

// APP推送--修改
export function appMessageEdit(data = {}) {
  return request({
    url: '/system/pc/app/message/edit',
    method: 'put',
    data: data
  })
}

// 敏感词管理--分页
export function sensitiveWordLibraryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sensitive/word/library/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 敏感词管理--添加
export function sensitiveWordLibrarySave(data = {}) {
  return request({
    url: '/system/pc/sensitive/word/library/save',
    method: 'post',
    data: data
  })
}

// 敏感词管理--删除
export function sensitiveWordLibraryDelete(id) {
  return request.delete(`/system/pc/sensitive/word/library/delete?id=${id}`)
}

// 敏感词管理--更新
export function sensitiveWordLibraryUpdate(data = {}) {
  return request({
    url: '/system/pc/sensitive/word/library/update',
    method: 'put',
    data: data
  })
}

// 敏感词管理--清空缓存
export function sensitiveWordLibraryDeleteRedis(data = {}) {
  return request({
    url: '/system/pc/sensitive/word/library/delete/redis',
    method: 'put',
    data: data
  })
}

// 友情链接(不登陆访问)
export const friendLink = (params = {}) => {
  return request.post('/system/api/website/link', params)
}

// 获取站点信息接口(不登陆访问)
export const getWebsite = (params = {}) => {
  return request.post('/system/api/website/get', params)
}

// 分享图片模板列表--分页
export function sharingTemplatePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/system/pc/sharing/template/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 分享图片模板列表--添加
export function sharingTemplateSave(data = {}) {
  return request({
    url: '/system/pc/sharing/template/save',
    method: 'post',
    data: data
  })
}

// 分享图片模板列表--修改
export function sharingTemplateEdit(data = {}) {
  return request({
    url: '/system/pc/sharing/template/edit',
    method: 'put',
    data: data
  })
}

// 分享图片模板--删除
export function sharingTemplateDelete(id) {
  return request.delete(`/system/pc/sharing/template/delete?id=${id}`)
}

// 分享图片模板状态--修改
export function sharingTemplateUpdateStatus(data = {}) {
  return request({
    url: '/system/pc/sharing/template/update/status',
    method: 'put',
    data: data
  })
}

export function txtImport(data, cb) {
  // 上传方法
  const formData = new FormData()
  formData.append('file', data.file)
  const config = {
    onUploadProgress: progressEvent => {
      const videoUploadPercent = Number((progressEvent.loaded / progressEvent.total * 100).toFixed(2))
      // 计算上传进度
      if (cb) {
        cb(videoUploadPercent)
      }
    }
  }
  return request.post('/system/pc/sensitive/word/library/txt/import', formData, config)
}

// 获取ali上传配置
export function getUploadConfig(data = {}) {
  return request({
    url: '/system/pc/upload/aliyun',
    method: 'post',
    data: data
  })
}

// 获取polyv上传sign
export function getPolyvVideoSign(data = {}) {
  return request({
    url: '/system/upload/api/update/sign',
    method: 'post',
    data: data
  })
}
