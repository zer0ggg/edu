import request from '@/utils/request'

// 订单日志--分页
export function orderLogPage(params, pageCurrent = 1, pageSize = 20) {
    return request({
        url: '/data/pc/order/log/list',
        method: 'post',
        data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
    })
}

// 订单课程统计--分页
export function orderStatCourserPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/order/stat/courser/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 订单讲师统计--分页
export function orderStatLecturerPage(params, pageCurrent = 1, pageSize = 20) {
    return request({
        url: '/data/pc/order/stat/lecturer/list',
        method: 'post',
        data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
    })
}

// 用户登录日志--分页
export function userLogPage(params, pageCurrent = 1, pageSize = 20) {
    return request({
        url: '/data/pc/user/log/list',
        method: 'post',
        data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
    })
}

// 订单课程统计
export function courserStatistical(data = {}) {
  return request({
    url: '/data/pc/order/stat/courser/statistical',
    method: 'post',
    data: data
  })
}

// 订单讲师统计
export function lecturerStatistical(data = {}) {
  return request({
    url: '/data/pc/order/stat/lecturer/statistical',
    method: 'post',
    data: data
  })
}

// 课程日志列表--分页
export function courseLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/course/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程统计列表--分页
export function courseStatPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/course/stat/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程统计用户列表--分页
export function courseStatUserPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/course/stat/user/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 后台操作日志列表--分页
export function sysLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/sys/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 用户学习记录--分页
export function studyLogPeriodPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/data/pc/course/log/period/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}
