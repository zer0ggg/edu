// 课程信息--分页
import request from '@/utils/request'

// 当天订单汇总--查看
export function orderStatView() {
  return request.post(`/data/pc/order/stat/view`, {})
}

// 订单课程当天统计
export function orderCourserToday() {
  return request.post(`/data/pc/order/stat/courser/today`, {})
}

// 订单讲师当天统计
export function orderLecturerToday() {
  return request.post(`/data/pc/order/stat/lecturer/today`, {})
}

// 获取当前时间与后十天的时间登录统计
export function orderLogDaysStat() {
  return request.post(`/data/pc/order/log/days/stat`, {})
}

// 当天汇总登录--查看
export function userStatView() {
  return request.post(`/data/pc/user/stat/view`, {})
}

// 用户省统计列表
export function userStatProvinceList() {
  return request.post(`/data/pc/user/stat/province/list`, {})
}

// 获取当前时间与后七天的时间登录统计
export function userLogDaysStat() {
  return request.post(`/data/pc/user/log/days/stat`, {})
}

