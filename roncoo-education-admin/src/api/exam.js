import request from '@/utils/request';
import {getToken} from '@/utils/auth'

// 试卷审核表
// 试卷审核--分页
export function examAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/exam/info/audit/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 试卷审核--状态修改
export function examAuditStatusUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/info/audit/update/status',
    method: 'put',
    data: data
  })
}

// 试卷审核--状态修改
export function examAuditView(id) {
  return request.get(`/exam/pc/exam/info/audit/view?id=${id}`)
}

// 试卷审核--上下架修改
export function examAuditIsPutawayUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/info/audit/put/away',
    method: 'put',
    data: data
  })
}

// 试卷审核--修改
export function examAuditEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/info/audit/edit',
    method: 'put',
    data: data
  })
}

// 试卷审核--审核
export function examAuditAudit(data = {}) {
  return request({
    url: '/exam/pc/exam/info/audit/audit',
    method: 'put',
    data: data
  })
}

// 试卷表
// 试卷--分页
export function examPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/exam/info/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 试卷--状态修改
export function examStatusUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/info/update/status',
    method: 'put',
    data: data
  })
}

// 试卷--查看
export function examView(id) {
  return request.get(`/exam/pc/exam/info/view?id=${id}`)
}

// 试卷--上下架修改
export function examIsPutawayUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/info/put/away',
    method: 'put',
    data: data
  })
}

// 试卷--修改
export function examEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/info/update',
    method: 'put',
    data: data
  })
}

// 试卷信息--一键导入ES
export function examImportEs() {
  return request({
    url: '/exam/pc/exam/info/es/add',
    method: 'post'
  })
}

// 试题表
// 试题--分页
export function examProblemPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/exam/problem/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 试题--状态修改
export function examProblemStatusUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/problem/update/status',
    method: 'put',
    data: data
  })
}

// 试题--查看
export function examProblemView(id) {
  return request.get(`/exam/pc/exam/problem/view?id=${id}`)
}

// 试题--修改
export function examProblemEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/problem/edit',
    method: 'put',
    data: data
  })
}

// 分类列表接口(试卷、试题修改使用)
export function categoryListEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/category/list/edit',
    method: 'post',
    data: data
  })
}

export function uploadExcel(data, cb) {
  // 上传方法
  const formData = new FormData()
  formData.append('file', data.file)
  const config = {
    onUploadProgress: progressEvent => {
      const videoUploadPercent = Number((progressEvent.loaded / progressEvent.total * 100).toFixed(2))
      // 计算上传进度
      if (cb) {
        cb(videoUploadPercent)
      }
    }
  }
  return request.post('/exam/pc/exam/problem/upload/excel?token=' + getToken(), formData, config)
}

// 试卷分类表
// 试卷分类--分页
export function examCategoryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/exam/category/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 试卷分类--查看
export function examCategoryView(id) {
  return request.get(`/exam/pc/exam/category/view?id=${id}`)
}

// 试卷分类--修改
export function examCategoryEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/category/edit',
    method: 'put',
    data: data
  })
}

// 试卷分类--修改状态
export function examCategoryUpdateStatus(data = {}) {
  return request({
    url: '/exam/pc/exam/category/update/status',
    method: 'put',
    data: data
  })
}

// 试卷分类--添加
export function examCategorySave(data = {}) {
  return request({
    url: '/exam/pc/exam/category/save',
    method: 'post',
    data: data
  })
}

// 试题分类--删除
export function examCategoryDelete(id) {
  return request.delete(`/exam/pc/exam/category//delete?id=${id}`)
}

// 试题分类--修改
export function examCategoryUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/category/update',
    method: 'put',
    data: data
  })
}

// 用户分类表
// 用户分类--分页
export function examUserCategoryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/user/exam/category/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 用户分类--查看
export function examUserCategoryView(id) {
  return request.get(`/exam/pc/user/exam/category/view?id=${id}`)
}

// 用户分类--修改
export function examUserCategoryEdit(data = {}) {
  return request({
    url: '/exam/pc/user/exam/category/update',
    method: 'put',
    data: data
  })
}

// 用户分类--添加
export function examUserCategorySave(data = {}) {
  return request({
    url: '/exam/pc/user/exam/category/save',
    method: 'post',
    data: data
  })
}

// 用户分类--删除
export function examUserCategoryDelete(id) {
  return request.delete(`/exam/pc/user/exam/category/delete?id=${id}`)
}

// 用户分类--修改
export function examUserCategoryUpdate(data = {}) {
  return request({
    url: '/exam/pc/user/exam/category/update',
    method: 'put',
    data: data
  })
}

// 试卷推荐
// 试卷推荐--分页
export function examRecommendPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/exam/recommend/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 试卷推荐--查看
export function examRecommendView(id) {
  return request.get(`/exam/pc/user/exam/category/view/${id}`)
}

// 试卷推荐--修改
export function examRecommendEdit(data = {}) {
  return request({
    url: '/exam/pc/exam/recommend/update',
    method: 'put',
    data: data
  })
}

// 试卷推荐--添加
export function examRecommendSave(data = {}) {
  return request({
    url: '/exam/pc/exam/recommend/save',
    method: 'post',
    data: data
  })
}

// 试卷推荐--批量添加
export function examRecommendSaveBatch(data = {}) {
  return request({
    url: '/exam/pc/exam/recommend/save/batch',
    method: 'post',
    data: data
  })
}

// 试卷推荐--删除
export function examRecommendDelete(id) {
  return request.delete(`/exam/pc/exam/recommend/delete?id=${id}`)
}

// 试卷推荐--修改
export function examRecommendUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/recommend/edit',
    method: 'put',
    data: data
  })
}

// 试卷推荐--修改状态
export function examRecommendStatusUpdate(data = {}) {
  return request({
    url: '/exam/pc/exam/recommend/update/status',
    method: 'put',
    data: data
  })
}

// 班级信息
// 班级信息--分页
export function gradeInfoPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/grade/info/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 班级信息--添加
export function gradeInfoSave(data = {}) {
  return request({
    url: '/exam/pc/grade/info/save',
    method: 'post',
    data: data
  })
}

// 班级信息--查看
export function gradeInfoView(id) {
  return request.get(`/exam/pc/grade/info/view?id=${id}`)
}

// 班级信息--修改
export function gradeInfoUpdate(data = {}) {
  return request({
    url: '/exam/pc/grade/info/edit',
    method: 'put',
    data: data
  })
}

// 班级信息--修改
export function gradeInfoUpdateStatus(id) {
  return request({
    url: `/exam/pc/grade/info/update/status?id=${id}`,
    method: 'put'
  })
}

// 班级信息--删除
export function gradeInfoDelete(id) {
  return request({
    url: `/exam/pc/grade/info/delete?id=${id}`,
    method: 'delete'
  })
}

// 班级学生
// 班级学生--分页
export function gradeStudentPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/grade/student/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 班级学生--添加
export function gradeStudentSave(data = {}) {
  return request({
    url: '/exam/pc/grade/student/save',
    method: 'post',
    data: data
  })
}

// 班级学生--修改
export function gradeStudentUpdate(data = {}) {
  return request({
    url: '/exam/pc/grade/student/edit',
    method: 'put',
    data: data
  })
}

// 班级学生--踢出
export function gradeStudentView(id) {
  return request.get(`/exam/pc/grade/student/view?id=${id}`)
}

// 班级学生--踢出
export function gradeStudentDelete(id) {
  return request({
    url: `/exam/pc/grade/student/delete?id=${id}`,
    method: 'delete'
  })
}

// 班级考试
// 班级考试--分页
export function gradeExamPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/grade/exam/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 班级考试--查看
export function gradeExamView(id) {
  return request.get(`/exam/pc/grade/exam/view?id=${id}`)
}

// 班级考试--修改
export function gradeExamUpdate(data = {}) {
  return request({
    url: '/exam/pc/grade/exam/edit',
    method: 'put',
    data: data
  })
}

// 班级考试学生
// 班级考试学生--分页
export function gradeExamStudentPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/grade/exam/student/relation/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}
export function gradeStudentSPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/grade/exam/student/relation/page/grade',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 课程试卷关联--分页
export function courseExamRefPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/exam/pc/course/exam/ref/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}
