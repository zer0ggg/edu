import request from '@/utils/request'

export function uploadPic(data, cb) {
  // 上传方法
  const formData = new FormData()
  formData.append('file', data.file)
  const config = {
    onUploadProgress: progressEvent => {
      const videoUploadPercent = Number((progressEvent.loaded / progressEvent.total * 100).toFixed(2))
      // 计算上传进度
      if (cb) {
        cb(videoUploadPercent)
      }
    }
  }
  return request.post('/course/pc/course/upload', formData, config)
}
