import request from '@/utils/request'
import {getToken} from '@/utils/auth'

// 分销信息列表--添加
export function distributionSave(data = {}) {
  return request({
    url: '/user/pc/distribution/save',
    method: 'post',
    data: data
  })
}

// 分销信息--修改
export function distributionEdit(data = {}) {
  return request({
    url: '/user/pc/distribution/edit',
    method: 'put',
    data: data
  })
}

// 分销信息列表--分页
export function distributionPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/distribution/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 分销信息列表--删除
export function distributionDelete(id) {
  return request.delete(`/user/pc/distribution/delete?id=${id}`)
}

// 代理分润记录列表--分页
export function distributionProfitPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/distribution/profit/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 讲师信息--添加
export function lecturerSave(data = {}) {
  return request({
    url: '/user/pc/lecturer/save',
    method: 'post',
    data: data
  })
}

// 讲师信息--分页
export function lecturerPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/lecturer/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 讲师信息--查看
export function lecturerView(id) {
  return request.get(`/user/pc/lecturer/view?id=${id}`)
}

// 获取讲师中心跳转路径
export function getRedirectLecturerCenterUrl(lecturerUserNo) {
  return request.get(`/user/pc/lecturer/direct?lecturerUserNo=${lecturerUserNo}`)
}

// 讲师信息--删除
export function lecturerDelete(id) {
  return request.delete(`/user/pc/lecturer/delete?id=${id}`)
}

// 讲师信息--修改
export function lecturerEdit(data = {}) {
  return request({
    url: '/user/pc/lecturer/edit',
    method: 'put',
    data: data
  })
}

// 讲师信息--修改
export function lecturerUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/lecturer/update/status',
    method: 'put',
    data: data
  })
}

// 讲师分成设置
export function lecturerProportionSet(data = {}) {
  return request({
    url: '/user/pc/lecturer/proportion/set',
    method: 'put',
    data: data
  })
}

// 讲师信息-审核--添加
export function lecturerAuditSave(data = {}) {
  return request({
    url: '/user/pc/lecturer/audit/save',
    method: 'post',
    data: data
  })
}

// 讲师信息-审核--分页
export function lecturerAuditPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/lecturer/audit/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 讲师信息-审核--查看
export function lecturerAuditView(id) {
  return request.get(`/user/pc/lecturer/audit/view?id=${id}`)
}

// 讲师信息-审核--删除
export function lecturerAuditDelete(id) {
  return request.delete(`/user/pc/lecturer/audit/delete?id=${id}`)
}

// 讲师信息-审核--修改
export function lecturerAuditEdit(data = {}) {
  return request({
    url: '/user/pc/lecturer/audit/edit',
    method: 'put',
    data: data
  })
}

// 讲师信息-审核--审核
export function lecturerAuditAudit(data = {}) {
  return request({
    url: '/user/pc/lecturer/audit/audit',
    method: 'post',
    data: data
  })
}

// 讲师信息-审核--校验手机
export function lecturerAuditCheckMobile(lecturerMobile) {
  return request.post(`/user/pc/lecturer/audit/checkUserAndLecturer?lecturerMobile=${lecturerMobile}`)
}

// 站内信息--添加
export function msgSave(data = {}) {
  return request({
    url: '/user/pc/msg/save',
    method: 'post',
    data: data
  })
}

// 站内信息--分页
export function msgPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/msg/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 站内信息--查看
export function msgView(id) {
  return request.get(`/user/pc/msg/view?id=${id}`)
}

// 站内信息--删除
export function msgDelete(id) {
  return request.delete(`/user/pc/msg/delete?id=${id}`)
}

// 站内信息--发送
export function msgSend(id) {
  return request.post(`/user/pc/msg/push?id=${id}`)
}

// 站内信息--修改
export function msgEdit(data = {}) {
  return request({
    url: '/user/pc/msg/edit',
    method: 'put',
    data: data
  })
}

// 消息模板--添加
export function msgTemplateSave(data = {}) {
  return request({
    url: '/user/pc/msg/template/save',
    method: 'post',
    data: data
  })
}

// 消息模板--分页
export function msgTemplatePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/msg/template/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 消息模板--查看
export function msgTemplateView(id) {
  return request.get(`/user/pc/msg/template/view?id=${id}`)
}

// 消息模板--删除
export function msgTemplateDelete(id) {
  return request.delete(`/user/pc/msg/template/delete?id=${id}`)
}

// 消息模板--修改
export function msgTemplateEdit(data = {}) {
  return request({
    url: '/user/pc/msg/template/edit',
    method: 'put',
    data: data
  })
}

// 订单信息--添加
export function orderInfoSave(data = {}) {
  return request({
    url: '/user/pc/order/info/save',
    method: 'post',
    data: data
  })
}

// 订单信息--手工录单
export function orderInfoManual(data = {}) {
  return request({
    url: '/user/pc/order/info/manual',
    method: 'post',
    data: data
  })
}

// 订单信息--分页
export function orderInfoPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/order/info/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 订单信息用户--分页
export function orderInfoUserPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/order/info/user/page',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 订单信息--查看
export function orderInfoView(id) {
  return request.get(`/user/pc/order/info/view?id=${id}`)
}

// 订单信息--查单
export function orderInfoQuery(orderNo) {
  return request.get(`/user/pc/order/info/query?orderNo=${orderNo}`)
}

// 订单信息--查看
export function orderInfoUnbind(id) {
  return request.post(`/user/pc/order/info/unbind?id=${id}`)
}

// 订单信息--修改
export function orderInfoEdit(data = {}) {
  return request({
    url: '/user/pc/order/info/edit',
    method: 'put',
    data: data
  })
}

// 订单信息--改价
export function orderInfoPrice(data = {}) {
  return request({
    url: '/user/pc/order/info/update/price',
    method: 'put',
    data: data
  })
}

// 订单日志信息--添加
export function orderLogSave(data = {}) {
  return request({
    url: '/user/pc/order/log/save',
    method: 'post',
    data: data
  })
}

// 订单日志信息--分页
export function orderLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/order/log/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 订单日志信息--查看
export function orderLogView(id) {
  return request.get(`/user/pc/order/log/view?id=${id}`)
}

// 订单日志信息--删除
export function orderLogDelete(id) {
  return request.delete(`/user/pc/order/log/delete?id=${id}`)
}

// 订单日志信息--修改
export function orderLogEdit(data = {}) {
  return request({
    url: '/user/pc/order/log/edit',
    method: 'put',
    data: data
  })
}

// 支付渠道信息--添加
export function payChannelSave(data = {}) {
  return request({
    url: '/user/pc/pay/channel/save',
    method: 'post',
    data: data
  })
}

// 支付渠道信息--分页
export function payChannelPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/pay/channel/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 支付渠道信息--查看
export function payChannelView(id) {
  return request.get(`/user/pc/pay/channel/view?id=${id}`)
}

// 支付渠道信息--删除
export function payChannelDelete(id) {
  return request.delete(`/user/pc/pay/channel/delete?id=${id}`)
}

// 支付渠道信息--修改
export function payChannelEdit(data = {}) {
  return request({
    url: '/user/pc/pay/channel/edit',
    method: 'put',
    data: data
  })
}

// 可用支付渠道
export function payChannelListUsable() {
  return request({
    url: '/user/pc/pay/channel/list/usable',
    method: 'get'
  })
}

// 支付渠道状态--修改
export function payChannelUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/pay/channel/update/status',
    method: 'put',
    data: data
  })
}

// 支付路由--添加
export function payRuleSave(data = {}) {
  return request({
    url: '/user/pc/pay/rule/save',
    method: 'post',
    data: data
  })
}

// 支付路由--分页
export function payRulePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/pay/rule/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 支付路由--查看
export function payRuleView(id) {
  return request.get(`/user/pc/pay/rule/view?id=${id}`)
}

// 支付路由--删除
export function payRuleDelete(id) {
  return request.delete(`/user/pc/pay/rule/delete?id=${id}`)
}

// 支付路由--修改
export function payRuleEdit(data = {}) {
  return request({
    url: '/user/pc/pay/rule/edit',
    method: 'put',
    data: data
  })
}

// 支付路由状态--修改
export function payRuleUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/pay/rule/update/status',
    method: 'put',
    data: data
  })
}

// --添加
export function undoLogSave(data = {}) {
  return request({
    url: '/user/pc/undo/log/save',
    method: 'post',
    data: data
  })
}

// --分页
export function undoLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/undo/log/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// --查看
export function undoLogView(id) {
  return request.get(`/user/pc/undo/log/view?id=${id}`)
}

// --删除
export function undoLogDelete(id) {
  return request.delete(`/user/pc/undo/log/delete?id=${id}`)
}

// --修改
export function undoLogEdit(data = {}) {
  return request({
    url: '/user/pc/undo/log/edit',
    method: 'put',
    data: data
  })
}

// 用户基本信息--添加
export function userSave(data = {}) {
  return request({
    url: '/user/pc/user/save',
    method: 'post',
    data: data
  })
}

// 用户基本信息--分页
export function userPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户基本信息--查看
export function userView(id) {
  return request.get(`/user/pc/user/view?id=${id}`)
}

// 用户基本信息--删除
export function userDelete(id) {
  return request.delete(`/user/pc/user/delete?id=${id}`)
}

// 用户基本信息--修改
export function userEdit(data = {}) {
  return request({
    url: '/user/pc/user/edit',
    method: 'put',
    data: data
  })
}

// 用户账户信息--添加
export function userAccountSave(data = {}) {
  return request({
    url: '/user/pc/user/account/save',
    method: 'post',
    data: data
  })
}

// 用户账户信息--分页
export function userAccountPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/account/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户账户信息--查看
export function userAccountView(id) {
  return request.get(`/user/pc/user/account/view?id=${id}`)
}

// 用户账户信息--删除
export function userAccountDelete(id) {
  return request.delete(`/user/pc/user/account/delete?id=${id}`)
}

// 用户账户信息--修改
export function userAccountEdit(data = {}) {
  return request({
    url: '/user/pc/user/account/edit',
    method: 'put',
    data: data
  })
}

// 用户账户提现日志--添加
export function userAccountExtractLogSave(data = {}) {
  return request({
    url: '/user/pc/user/account/extract/log/save',
    method: 'post',
    data: data
  })
}

// 用户账户提现日志--分页
export function userAccountExtractLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/account/extract/log/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户账户提现日志--查看
export function userAccountExtractLogView(id) {
  return request.get(`/user/pc/user/account/extract/log/view?id=${id}`)
}

// 用户账户提现日志--删除
export function userAccountExtractLogDelete(id) {
  return request.delete(`/user/pc/user/account/extract/log/delete?id=${id}`)
}

// 用户账户提现日志--修改
export function userAccountExtractLogEdit(data = {}) {
  return request({
    url: '/user/pc/user/account/extract/log/edit',
    method: 'put',
    data: data
  })
}

// 用户账户提现日志--批量标记
export function userAccountExtractLogBatchAudit(ids) {
  return request.post(`/user/pc/user/account/extract/log/batch/audit?ids=${ids}`)
}

// 用户教育信息--添加
export function userExtSave(data = {}) {
  return request({
    url: '/user/pc/user/ext/save',
    method: 'post',
    data: data
  })
}

// 用户教育信息--分页
export function userExtPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/ext/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户教育信息--查看
export function userExtView(userNo) {
  return request.get(`/user/pc/user/ext/view?userNo=${userNo}`)
}

// 用户教育信息--删除
export function userExtDelete(id) {
  return request.delete(`/user/pc/user/ext/delete?id=${id}`)
}

// 用户教育信息--重置错误次数
export function userExtResetError(userNo) {
  return request.post(`/user/pc/user/ext/reset/error?userNo=${userNo}`)
}

// 用户教育信息--解绑小程序
export function userExtUnbind(userNo) {
  return request.post(`/user/pc/user/ext/unbind?userNo=${userNo}`)
}

// 用户教育信息--修改
export function userExtEdit(data = {}) {
  return request({
    url: '/user/pc/user/ext/edit',
    method: 'put',
    data: data
  })
}

// 用户教育信息状态--修改
export function userExtUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/user/ext/update/status',
    method: 'put',
    data: data
  })
}

export function userExtUploadExcel(data, cb) {
  // 上传方法
  const formData = new FormData()
  formData.append('file', data.file)
  const config = {
    onUploadProgress: progressEvent => {
      const videoUploadPercent = Number((progressEvent.loaded / progressEvent.total * 100).toFixed(2))
      // 计算上传进度
      if (cb) {
        cb(videoUploadPercent)
      }
    }
  }
  return request.post('/user/pc/user/ext/upload/excel?token=' + getToken(), formData, config)
}

// 用户关注讲师--添加
export function userLecturerAttentionSave(data = {}) {
  return request({
    url: '/user/pc/user/lecturer/attention/save',
    method: 'post',
    data: data
  })
}

// 用户关注讲师--分页
export function userLecturerAttentionPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/lecturer/attention/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户关注讲师--查看
export function userLecturerAttentionView(id) {
  return request.get(`/user/pc/user/lecturer/attention/view?id=${id}`)
}

// 用户关注讲师--删除
export function userLecturerAttentionDelete(id) {
  return request.delete(`/user/pc/user/lecturer/attention/delete?id=${id}`)
}

// 用户关注讲师--修改
export function userLecturerAttentionEdit(data = {}) {
  return request({
    url: '/user/pc/user/lecturer/attention/edit',
    method: 'put',
    data: data
  })
}

// 用户错误登录日志--添加
export function userLogLoginSave(data = {}) {
  return request({
    url: '/user/pc/user/log/login/save',
    method: 'post',
    data: data
  })
}

// 用户错误登录日志--分页
export function userLogLoginPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/log/login/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户错误登录日志--查看
export function userLogLoginView(id) {
  return request.get(`/user/pc/user/log/login/view?id=${id}`)
}

// 用户错误登录日志--删除
export function userLogLoginDelete(id) {
  return request.delete(`/user/pc/user/log/login/delete?id=${id}`)
}

// 用户错误登录日志--修改
export function userLogLoginEdit(data = {}) {
  return request({
    url: '/user/pc/user/log/login/edit',
    method: 'put',
    data: data
  })
}

// 用户修改日志--添加
export function userLogModifiedSave(data = {}) {
  return request({
    url: '/user/pc/user/log/modified/save',
    method: 'post',
    data: data
  })
}

// 用户修改日志--分页
export function userLogModifiedPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/log/modified/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户修改日志--查看
export function userLogModifiedView(id) {
  return request.get(`/user/pc/user/log/modified/view?id=${id}`)
}

// 用户修改日志--删除
export function userLogModifiedDelete(id) {
  return request.delete(`/user/pc/user/log/modified/delete?id=${id}`)
}

// 用户修改日志--修改
export function userLogModifiedEdit(data = {}) {
  return request({
    url: '/user/pc/user/log/modified/edit',
    method: 'put',
    data: data
  })
}

// 用户发送短信日志--添加
export function userLogSmsSave(data = {}) {
  return request({
    url: '/user/pc/user/log/sms/save',
    method: 'post',
    data: data
  })
}

// 用户发送短信日志--分页
export function userLogSmsPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/log/sms/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户发送短信日志--统计
export function userLogSmsCount() {
  return request({
    url: '/user/pc/user/log/sms/count',
    method: 'post'
  })
}

// 用户发送短信日志--查看
export function userLogSmsView(id) {
  return request.get(`/user/pc/user/log/sms/view?id=${id}`)
}

// 用户发送短信日志--发送短信
export function userLogSmsSend(mobile) {
  return request.get(`/user/pc/user/log/sms/send?mobile=${mobile}`)
}

// 用户发送短信日志--删除
export function userLogSmsDelete(id) {
  return request.delete(`/user/pc/user/log/sms/delete?id=${id}`)
}

// 用户发送短信日志--修改
export function userLogSmsEdit(data = {}) {
  return request({
    url: '/user/pc/user/log/sms/edit',
    method: 'put',
    data: data
  })
}

// 站内信用户记录--添加
export function userMsgSave(data = {}) {
  return request({
    url: '/user/pc/user/msg/save',
    method: 'post',
    data: data
  })
}

// 站内信用户记录--分页
export function userMsgPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/msg/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 站内信用户记录--查看
export function userMsgView(id) {
  return request.get(`/user/pc/user/msg/view?id=${id}`)
}

// 站内信用户记录--删除
export function userMsgDelete(id) {
  return request.delete(`/user/pc/user/msg/delete?id=${id}`)
}

// 站内信用户记录--修改
export function userMsgEdit(data = {}) {
  return request({
    url: '/user/pc/user/msg/edit',
    method: 'put',
    data: data
  })
}

// 用户订单--添加
export function userOrderSave(data = {}) {
  return request({
    url: '/user/pc/user/order/save',
    method: 'post',
    data: data
  })
}

// 用户订单--分页
export function userOrderPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/order/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户订单--统计
export function userOrderCount(data = {}) {
  return request({
    url: '/user/pc/user/order/count',
    method: 'post',
    data: data
  })
}

// 用户订单--查看
export function userOrderView(id) {
  return request.get(`/user/pc/user/order/view?id=${id}`)
}

// 用户订单--删除
export function userOrderDelete(id) {
  return request.delete(`/user/pc/user/order/delete?id=${id}`)
}

// 用户订单--手工录单
export function userOrderManual(data = {}) {
  return request({
    url: '/user/pc/user/order/manual',
    method: 'post',
    data: data
  })
}

// 用户订单--会员解绑
export function userOrderUnbind(id) {
  return request.post(`/user/pc/user/order/unbind?id=${id}`)
}

// 用户订单--查单
export function userOrderQuery(orderNo) {
  return request.get(`/user/pc/user/order/query?orderNo=${orderNo}`)
}

// 用户订单--修改
export function userOrderEdit(data = {}) {
  return request({
    url: '/user/pc/user/order/edit',
    method: 'put',
    data: data
  })
}

// 用户订单日志信息--添加
export function userOrderLogSave(data = {}) {
  return request({
    url: '/user/pc/user/order/log/save',
    method: 'post',
    data: data
  })
}

// 用户订单日志信息--分页
export function userOrderLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/order/log/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户订单日志信息--查看
export function userOrderLogView(id) {
  return request.get(`/user/pc/user/order/log/view?id=${id}`)
}

// 用户订单日志信息--删除
export function userOrderLogDelete(id) {
  return request.delete(`/user/pc/user/order/log/delete?id=${id}`)
}

// 用户订单日志信息--修改
export function userOrderLogEdit(data = {}) {
  return request({
    url: '/user/pc/user/order/log/edit',
    method: 'put',
    data: data
  })
}

// 频道信息--分页
export function channelPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/liveChannel/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 频道信息--修改状态
export function channelUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/liveChannel/update/status',
    method: 'put',
    data: data
  })
}

// 频道信息--删除
export function deleteChannel(data = {}) {
  return request({
    url: '/user/pc/liveChannel/delete',
    method: 'post',
    data: data
  })
}

// 分销信息--分页
export function chosenPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/chosen/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 分销信息--添加
export function chosenSave(data = {}) {
  return request({
    url: '/user/pc/chosen/save',
    method: 'post',
    data: data
  })
}

// 分销信息--删除
export function chosenDelete(id) {
  return request.delete(`/user/pc/chosen/delete?id=${id}`)
}

// 分销信息--修改
export function chosenUpdateStatus(data = {}) {
  return request({
    url: '/user/pc/chosen/update/status',
    method: 'put',
    data: data
  })
}

// 分销信息--查看
export function chosenView(id) {
  return request.get(`/user/pc/chosen/view?id=${id}`)
}

// 分销信息--修改
export function chosenEdit(data = {}) {
  return request({
    url: '/user/pc/chosen/edit',
    method: 'put',
    data: data
  })
}

// 用户推荐--分页
export function recommendedPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/recommended/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户调研信息--分页
export function userResearchPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/user/pc/user/research/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 用户调研信息--查看
export function userResearchView(id) {
  return request.get(`/user/pc/user/research/view?id=${id}`)
}
