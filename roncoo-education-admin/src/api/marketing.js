import request from '@/utils/request'

// 活动信息表--添加
export function actSave(data = {}) {
  return request({
    url: '/marketing/pc/act/save',
    method: 'post',
    data: data
  })
}

// 活动信息表--分页
export function actPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 活动信息表--查看
export function actView(id) {
  return request.get(`/marketing/pc/act/view?id=${id}`)
}

// 活动信息表--删除
export function actDelete(id) {
  return request.delete(`/marketing/pc/act/delete?id=${id}`)
}

// 活动信息表--修改
export function actEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/edit',
    method: 'put',
    data: data
  })
}

// 活动信息表--复制
export function actCopy(data = {}) {
  return request({
    url: '/marketing/pc/act/copy',
    method: 'post',
    data: data
  })
}

// 活动信息--状态
export function actUpdateStatus(data = {}) {
  return request({
    url: '/marketing/pc/act/update/status',
    method: 'put',
    data: data
  })
}

// 优惠券活动--添加
export function actCouponSave(data = {}) {
  return request({
    url: '/marketing/pc/act/coupon/save',
    method: 'post',
    data: data
  })
}

// 优惠券活动--分页
export function actCouponPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/coupon/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 优惠券活动--查看
export function actCouponView(id) {
  return request.get(`/marketing/pc/act/coupon/view?id=${id}`)
}

// 优惠券活动--删除
export function actCouponDelete(id) {
  return request.delete(`/marketing/pc/act/coupon/delete?id=${id}`)
}

// 优惠券活动--修改
export function actCouponEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/coupon/edit',
    method: 'put',
    data: data
  })
}

// 优惠券用户关联--添加
export function actCouponUserSave(data = {}) {
  return request({
    url: '/marketing/pc/act/coupon/user/save',
    method: 'post',
    data: data
  })
}

// 优惠券用户关联--分页
export function actCouponUserPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/coupon/user/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 优惠券用户关联--查看
export function actCouponUserView(id) {
  return request.get(`/marketing/pc/act/coupon/user/view?id=${id}`)
}

// 优惠券用户关联--删除
export function actCouponUserDelete(id) {
  return request.delete(`/marketing/pc/act/coupon/user/delete?id=${id}`)
}

// 优惠券用户关联--修改
export function actCouponUserEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/coupon/user/edit',
    method: 'put',
    data: data
  })
}

// 课程秒杀活动--添加
export function actSeckillSave(data = {}) {
  return request({
    url: '/marketing/pc/act/seckill/save',
    method: 'post',
    data: data
  })
}

// 课程秒杀活动--分页
export function actSeckillPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/seckill/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 课程秒杀活动--查看
export function actSeckillView(id) {
  return request.get(`/marketing/pc/act/seckill/view?id=${id}`)
}

// 课程秒杀活动--删除
export function actSeckillDelete(id) {
  return request.delete(`/marketing/pc/act/seckill/delete?id=${id}`)
}

// 课程秒杀活动--修改
export function actSeckillEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/seckill/edit',
    method: 'put',
    data: data
  })
}

// 活动专区--添加
export function actZoneSave(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/save',
    method: 'post',
    data: data
  })
}

// 活动专区--分页
export function actZonePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/zone/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 活动专区--查看
export function actZoneView(id) {
  return request.get(`/marketing/pc/act/zone/view?id=${id}`)
}

// 活动专区--删除
export function actZoneDelete(id) {
  return request.delete(`/marketing/pc/act/zone/delete?id=${id}`)
}

// 活动专区--修改
export function actZoneEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/edit',
    method: 'put',
    data: data
  })
}

// 活动专区--状态
export function actZoneUpdateStatus(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/update/status',
    method: 'put',
    data: data
  })
}

// 秒杀专区--复制
export function actZoneCopy(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/copy',
    method: 'post',
    data: data
  })
}

// 活动专区课程--添加
export function actZoneCourseSave(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/course/save',
    method: 'post',
    data: data
  })
}

// 活动专区课程--分页
export function actZoneCoursePage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/marketing/pc/act/zone/course/list',
    method: 'post',
    data: {pageCurrent: pageCurrent, pageSize: pageSize, ...params}
  })
}

// 活动专区课程--查看
export function actZoneCourseView(id) {
  return request.get(`/marketing/pc/act/zone/course/view?id=${id}`)
}

// 活动专区课程--删除
export function actZoneCourseDelete(id) {
  return request.delete(`/marketing/pc/act/zone/course/delete?id=${id}`)
}

// 活动专区课程--修改
export function actZoneCourseEdit(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/course/edit',
    method: 'put',
    data: data
  })
}

// 活动专区课程--修改状态
export function actZoneCourseUpdateStatus(data = {}) {
  return request({
    url: '/marketing/pc/act/zone/course/update/status',
    method: 'put',
    data: data
  })
}

