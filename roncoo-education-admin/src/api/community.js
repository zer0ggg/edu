import request from '@/utils/request'

// 文章推荐--添加
export function articleRecommendSave(data = {}) {
  return request({
    url: '/community/pc/article/recommend/save',
    method: 'post',
    data: data
  })
}

// 文章推荐--添加
export function articleRecommendSaveBatch(data = {}) {
  return request({
    url: '/community/pc/article/recommend/save/batch',
    method: 'post',
    data: data
  })
}

// 文章推荐--分页
export function articleRecommendPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/article/recommend/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 文章推荐--查看
export function articleRecommendView(id) {
  return request.get(`/community/pc/article/recommend/view?id=${id}`)
}

// 文章推荐--删除
export function articleRecommendDelete(id) {
  return request.delete(`/community/pc/article/recommend/delete?id=${id}`)
}

// 文章推荐--修改
export function articleRecommendEdit(data = {}) {
  return request({
    url: '/community/pc/article/recommend/edit',
    method: 'put',
    data: data
  })
}

// 文章推荐--状态修改
export function articleRecommendUpdateStatus(id, statusId) {
  return request({
    url: '/community/pc/article/recommend/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 文章专区首页分类--添加
export function articleZoneCategorySave(data = {}) {
  return request({
    url: '/community/pc/article/zone/category/save',
    method: 'post',
    data: data
  })
}

// 文章专区首页分类--分页
export function articleZoneCategoryPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/article/zone/category/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 文章专区首页分类--查看
export function articleZoneCategoryView(id) {
  return request.get(`/community/pc/article/zone/category/view?id=${id}`)
}

// 文章专区首页分类--删除
export function articleZoneCategoryDelete(id) {
  return request.delete(`/community/pc/article/zone/category/delete?id=${id}`)
}

// 文章专区首页分类--修改
export function articleZoneCategoryEdit(data = {}) {
  return request({
    url: '/community/pc/article/zone/category/edit',
    method: 'put',
    data: data
  })
}

// 文章专区关联--批量添加
export function articleZoneRefSaveBatch(data = {}) {
  return request({
    url: '/community/pc/article/zone/ref/save/batch',
    method: 'post',
    data: data
  })
}

// 文章专区关联--添加
export function articleZoneRefSave(data = {}) {
  return request({
    url: '/community/pc/article/zone/ref/save',
    method: 'post',
    data: data
  })
}

// 文章专区关联--分页
export function articleZoneRefPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/article/zone/ref/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 文章专区关联--查看
export function articleZoneRefView(id) {
  return request.get(`/community/pc/article/zone/ref/view?id=${id}`)
}

// 文章专区关联--删除
export function articleZoneRefDelete(id) {
  return request.delete(`/community/pc/article/zone/ref/delete?id=${id}`)
}

// 文章专区关联--修改状态
export function articleZoneUpdateStatus(data = {}) {
  return request({
    url: '/community/pc/article/zone/ref/updateStatusId',
    method: 'put',
    data: data
  })
}

// 文章专区关联--修改
export function articleZoneRefEdit(data = {}) {
  return request({
    url: '/community/pc/article/zone/ref/edit',
    method: 'put',
    data: data
  })
}

// 博客信息--添加
export function blogSave(data = {}) {
  return request({
    url: '/community/pc/blog/save',
    method: 'post',
    data: data
  })
}

// 博客信息--分页
export function blogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/blog/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 博客信息--查看
export function blogView(id) {
  return request.get(`/community/pc/blog/view?id=${id}`)
}

// 博客信息--删除
export function blogDelete(id) {
  return request.delete(`/community/pc/blog/delete?id=${id}`)
}

// 博客信息--修改
export function blogEdit(data = {}) {
  return request({
    url: '/community/pc/blog/edit',
    method: 'put',
    data: data
  })
}

// 博客信息--修改
export function blogUpdateStatus(id, statusId) {
  return request({
    url: '/community/pc/blog/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 课程信息--一键导入ES
export function blogImportEs(articleType) {
  return request({
    url: '/community/pc/blog/add/es',
    method: 'post',
    data: {
      articleType: articleType
    }
  })
}

// 博客评论--添加
export function blogCommentSave(data = {}) {
  return request({
    url: '/community/pc/blog/comment/save',
    method: 'post',
    data: data
  })
}

// 博客评论--分页
export function blogCommentPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/blog/comment/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 博客评论--查看
export function blogCommentView(id) {
  return request.get(`/community/pc/blog/comment/view?id=${id}`)
}

// 博客评论--删除
export function blogCommentDelete(id) {
  return request.delete(`/community/pc/blog/comment/delete?id=${id}`)
}

// 博客评论--状态删除
export function blogCommentStatusDel(data = {}) {
  return request.post(`/community/pc/blog/comment/del`, data)
}

// 博客评论--修改
export function blogCommentEdit(data = {}) {
  return request({
    url: '/community/pc/blog/comment/edit',
    method: 'put',
    data: data
  })
}

// 博客与用户关联--添加
export function blogUserRecordSave(data = {}) {
  return request({
    url: '/community/pc/blog/user/record/save',
    method: 'post',
    data: data
  })
}

// 博客与用户关联--分页
export function blogUserRecordPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/blog/user/record/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 博客与用户关联--查看
export function blogUserRecordView(id) {
  return request.get(`/community/pc/blog/user/record/view?id=${id}`)
}

// 博客与用户关联--删除
export function blogUserRecordDelete(id) {
  return request.delete(`/community/pc/blog/user/record/delete?id=${id}`)
}

// 博客与用户关联--修改
export function blogUserRecordEdit(data = {}) {
  return request({
    url: '/community/pc/blog/user/record/edit',
    method: 'put',
    data: data
  })
}

// 博主信息--添加
export function bloggerSave(data = {}) {
  return request({
    url: '/community/pc/blogger/save',
    method: 'post',
    data: data
  })
}

// 博主信息--添加
export function bloggerBatchSave(data = {}) {
  return request({
    url: '/community/pc/blogger/batch/save',
    method: 'post',
    data: data
  })
}

// 博主信息--分页
export function bloggerPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/blogger/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 博主信息--查看
export function bloggerView(id) {
  return request.get(`/community/pc/blogger/view?id=${id}`)
}

// 博主信息--删除
export function bloggerDelete(id) {
  return request.delete(`/community/pc/blogger/delete?id=${id}`)
}

// 博主信息--修改
export function bloggerEdit(data = {}) {
  return request({
    url: '/community/pc/blogger/edit',
    method: 'put',
    data: data
  })
}

// 博主与用户关注关联--添加
export function bloggerUserRecordSave(data = {}) {
  return request({
    url: '/community/pc/blogger/user/record/save',
    method: 'post',
    data: data
  })
}

// 博主与用户关注关联--分页
export function bloggerUserRecordPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/blogger/user/record/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 博主与用户关注关联--查看
export function bloggerUserRecordView(id) {
  return request.get(`/community/pc/blogger/user/record/view?id=${id}`)
}

// 博主与用户关注关联--删除
export function bloggerUserRecordDelete(id) {
  return request.delete(`/community/pc/blogger/user/record/delete?id=${id}`)
}

// 博主与用户关注关联--修改
export function bloggerUserRecordEdit(data = {}) {
  return request({
    url: '/community/pc/blogger/user/record/edit',
    method: 'put',
    data: data
  })
}

// 标签--添加
export function labelSave(data = {}) {
  return request({
    url: '/community/pc/label/save',
    method: 'post',
    data: data
  })
}

// 标签--分页
export function labelPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/label/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 标签--分页，纯名称查出
export function labelPageOnlyName(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/label/list/only/name',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 标签--查看
export function labelView(id) {
  return request.get(`/community/pc/label/view?id=${id}`)
}

// 标签--查看
export function labelListByType(labelType) {
  return request.get(`/community/pc/label/get/type?labelType=${labelType}`)
}

// 标签--删除
export function labelDelete(id) {
  return request.delete(`/community/pc/label/delete?id=${id}`)
}

// 标签--修改
export function labelEdit(data = {}) {
  return request({
    url: '/community/pc/label/edit',
    method: 'put',
    data: data
  })
}

// 标签--状态修改
export function labelUpdateStatus(id, statusId) {
  return request({
    url: '/community/pc/label/update/status',
    method: 'put',
    data: {
      id: id,
      statusId: statusId
    }
  })
}

// 问答信息--添加
export function questionsSave(data = {}) {
  return request({
    url: '/community/pc/questions/save',
    method: 'post',
    data: data
  })
}

// 问答信息--分页
export function questionsPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/questions/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 问答信息--查看
export function questionsView(id) {
  return request.get(`/community/pc/questions/view?id=${id}`)
}

// 问答信息--删除
export function questionsDelete(id) {
  return request.delete(`/community/pc/questions/delete?id=${id}`)
}

// 问答信息--修改
export function questionsEdit(data = {}) {
  return request({
    url: '/community/pc/questions/edit',
    method: 'put',
    data: data
  })
}

// 问答信息--修改状态
export function questionsUpdateStatusId(data = {}) {
  return request({
    url: '/community/pc/questions/updateStatusId',
    method: 'put',
    data: data
  })
}

// 问答信息--一键导入ES
export function questionsImportEs() {
  return request({
    url: '/community/pc/questions/add/es',
    method: 'post'
  })
}

// 问答评论--添加
export function questionsCommentSave(data = {}) {
  return request({
    url: '/community/pc/questions/comment/save',
    method: 'post',
    data: data
  })
}

// 问答评论--分页
export function questionsCommentPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/questions/comment/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 问答评论--查看
export function questionsCommentView(id) {
  return request.get(`/community/pc/questions/comment/view?id=${id}`)
}

// 问答评论--删除
export function questionsCommentDelete(id) {
  return request.delete(`/community/pc/questions/comment/delete?id=${id}`)
}

// 问答评论--修改
export function questionsCommentEdit(data = {}) {
  return request({
    url: '/community/pc/questions/comment/edit',
    method: 'put',
    data: data
  })
}

// 问题与用户关联--添加
export function questionsUserRecordSave(data = {}) {
  return request({
    url: '/community/pc/questions/user/record/save',
    method: 'post',
    data: data
  })
}

// 问题与用户关联--分页
export function questionsUserRecordPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/questions/user/record/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// 问题与用户关联--查看
export function questionsUserRecordView(id) {
  return request.get(`/community/pc/questions/user/record/view?id=${id}`)
}

// 问题与用户关联--删除
export function questionsUserRecordDelete(id) {
  return request.delete(`/community/pc/questions/user/record/delete?id=${id}`)
}

// 问题与用户关联--修改
export function questionsUserRecordEdit(data = {}) {
  return request({
    url: '/community/pc/questions/user/record/edit',
    method: 'put',
    data: data
  })
}

// --添加
export function undoLogSave(data = {}) {
  return request({
    url: '/community/pc/undo/log/save',
    method: 'post',
    data: data
  })
}

// --分页
export function undoLogPage(params, pageCurrent = 1, pageSize = 20) {
  return request({
    url: '/community/pc/undo/log/list',
    method: 'post',
    data: { pageCurrent: pageCurrent, pageSize: pageSize, ...params }
  })
}

// --查看
export function undoLogView(id) {
  return request.get(`/community/pc/undo/log/view?id=${id}`)
}

// --删除
export function undoLogDelete(id) {
  return request.delete(`/community/pc/undo/log/delete?id=${id}`)
}

// --修改
export function undoLogEdit(data = {}) {
  return request({
    url: '/community/pc/undo/log/edit',
    method: 'put',
    data: data
  })
}

