import request from '@/utils/request'

export function apiLogin(params = {}) {
  // 密码登录接口
  return request({
    url: '/system/pc/api/sys/user/login',
    method: 'post',
    data: params
  })
}

export function getLoginSign(params = {}) {
  return request({
    url: '/system/pc/api/sys/user/authenticate',
    method: 'post',
    data: params
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function getUserPermission() {
  return request({
    url: '/system/pc/sys/user/permission',
    method: 'post',
    data: {}
  })
}
