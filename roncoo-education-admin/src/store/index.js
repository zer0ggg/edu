import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import permission from './modules/permission'
import app from './modules/app'
import settings from './modules/settings'
import opts from './modules/opts'
import user from './modules/user'
import menu from './modules/menu'
import tags from './modules/tags'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    menu,
    opts,
    tags,
    permission
  },
  getters
})

export default store
