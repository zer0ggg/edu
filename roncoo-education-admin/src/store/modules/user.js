import {apiLogin, getLoginSign} from '@/api/login'
import {getToken, setToken, removeToken} from '@/utils/auth'
import md5 from '@/utils/md5'
import {resetRouter} from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    svgIconList: [
      {
        'icon': 'ad',
        'label': 'ad'
      },
      {
        'icon': 'agent',
        'label': 'agent'
      },
      {
        'icon': 'app',
        'label': 'app'
      },
      {
        'icon': 'blog',
        'label': 'blog'
      },
      {
        'icon': 'carousel',
        'label': 'carousel'
      },
      {
        'icon': 'category',
        'label': 'category'
      },
      {
        'icon': 'cay',
        'label': 'cay'
      },
      {
        'icon': 'chosen',
        'label': 'chosen'
      },
      {
        'icon': 'community',
        'label': 'community'
      },
      {
        'icon': 'count',
        'label': 'count'
      },
      {
        'icon': 'coupon',
        'label': 'coupon'
      },
      {
        'icon': 'course',
        'label': 'course'
      },
      {
        'icon': 'dashboard',
        'label': 'dashboard'
      },
      {
        'icon': 'data',
        'label': 'data'
      },
      {
        'icon': 'du',
        'label': 'du'
      },
      {
        'icon': 'exam',
        'label': 'exam'
      },
      {
        'icon': 'example',
        'label': 'example'
      },
      {
        'icon': 'eye-open',
        'label': 'eye-open'
      },
      {
        'icon': 'eye',
        'label': 'eye'
      },
      {
        'icon': 'form',
        'label': 'form'
      },
      {
        'icon': 'h5',
        'label': 'h5'
      },
      {
        'icon': 'inc',
        'label': 'inc'
      },
      {
        'icon': 'info',
        'label': 'info'
      },
      {
        'icon': 'lecturer',
        'label': 'lecturer'
      },
      {
        'icon': 'lib',
        'label': 'lib'
      },
      {
        'icon': 'link',
        'label': 'link'
      },
      {
        'icon': 'live',
        'label': 'live'
      },
      {
        'icon': 'login-pwd',
        'label': 'login-pwd'
      },
      {
        'icon': 'login-user',
        'label': 'login-user'
      },
      {
        'icon': 'login',
        'label': 'login'
      },
      {
        'icon': 'logo.png',
        'label': 'logo.png'
      },
      {
        'icon': 'marketing',
        'label': 'marketing'
      },
      {
        'icon': 'message',
        'label': 'message'
      },
      {
        'icon': 'msg',
        'label': 'msg'
      },
      {
        'icon': 'navBar',
        'label': 'navBar'
      },
      {
        'icon': 'nested',
        'label': 'nested'
      },
      {
        'icon': 'number',
        'label': 'number'
      },
      {
        'icon': 'often',
        'label': 'often'
      },
      {
        'icon': 'order',
        'label': 'order'
      },
      {
        'icon': 'orderInfo',
        'label': 'orderInfo'
      },
      {
        'icon': 'orderLog',
        'label': 'orderLog'
      },
      {
        'icon': 'password',
        'label': 'password'
      },
      {
        'icon': 'pay',
        'label': 'pay'
      },
      {
        'icon': 'pc',
        'label': 'pc'
      },
      {
        'icon': 'point',
        'label': 'point'
      },
      {
        'icon': 'quest',
        'label': 'quest'
      },
      {
        'icon': 'question',
        'label': 'question'
      },
      {
        'icon': 'recommend',
        'label': 'recommend'
      },
      {
        'icon': 'res',
        'label': 'res'
      },
      {
        'icon': 'resource',
        'label': 'resource'
      },
      {
        'icon': 'seckill',
        'label': 'seckill'
      },
      {
        'icon': 'share',
        'label': 'share'
      },
      {
        'icon': 'sub',
        'label': 'sub'
      },
      {
        'icon': 'subject',
        'label': 'subject'
      },
      {
        'icon': 'suc',
        'label': 'suc'
      },
      {
        'icon': 'svg-icon-info',
        'label': 'svg-icon-info'
      },
      {
        'icon': 'sysLog',
        'label': 'sysLog'
      },
      {
        'icon': 'system',
        'label': 'system'
      },
      {
        'icon': 'table',
        'label': 'table'
      },
      {
        'icon': 'ti',
        'label': 'ti'
      },
      {
        'icon': 'topAD',
        'label': 'topAD'
      },
      {
        'icon': 'tree',
        'label': 'tree'
      },
      {
        'icon': 'user',
        'label': 'user'
      },
      {
        'icon': 'userLog',
        'label': 'userLog'
      },
      {
        'icon': 'userOrder',
        'label': 'userOrder'
      },
      {
        'icon': 'video',
        'label': 'video'
      },
      {
        'icon': 'vod',
        'label': 'vod'
      },
      {
        'icon': 'website',
        'label': 'website'
      },
      {
        'icon': 'websiteLink',
        'label': 'websiteLink'
      },
      {
        'icon': 'websiteNav',
        'label': 'websiteNav'
      },
      {
        'icon': 'zi',
        'label': 'zi'
      },
      {
        'icon': 'zone',
        'label': 'zone'
      }
    ]
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({commit}, userInfo) {
    console.log('userInfo', userInfo)
    const loginName = userInfo.userName.trim()
    const loginPassword = userInfo.password
    const data = userInfo
    data.loginName = loginName
    data.loginPassword = loginPassword
    return new Promise((resolve, reject) => {
      apiLogin(data).then(response => {
        const {data} = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // user login
  getLoginSign({commit}, userInfo) {
    const userName = userInfo.userName.trim()
    const timestamp = (new Date()).getTime()
    const random = parseInt(Math.random() * 10000)
    const sign = ['login', userName, timestamp, random].join('lingke')
    return new Promise((resolve, reject) => {
      getLoginSign({
        loginName: userName,
        sign: md5.hexMD5(sign),
        signatureNonce: random,
        timestamp: timestamp
      }).then(response => {
        resolve({
          sign: response.data,
          signatureNonce: random,
          timestamp: timestamp
        })
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({commit, state}) {
    return new Promise((resolve, reject) => {
      commit('SET_NAME', 'name')
      commit('SET_AVATAR', 'avatar')
      resolve({})
      // getInfo(state.token).then(response => {
      //   const { data } = response
      //
      //   if (!data) {
      //     reject('Verification failed, please Login again.')
      //   }
      //
      //   const { name, avatar } = data
      //
      //   commit('SET_NAME', name)
      //   commit('SET_AVATAR', avatar)
      //   resolve(data)
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // user logout
  logout({commit, state}) {
    return new Promise((resolve, reject) => {
      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // logout(state.token).then(() => {
      //   removeToken() // must remove  token  first
      //   resetRouter()
      //   commit('RESET_STATE')
      //   resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({commit}) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

